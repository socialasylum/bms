/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.skin = 'bootstrapck'


    config.filebrowserBrowseUrl = '/public/ckfinder2.4/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/public/ckfinder2.4/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = '/public/ckfinder2.4/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl = '/public/ckfinder2.4/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/public/ckfinder2.4/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = '/public/ckfinder2.4/core/connector/php/connector.php?command=QuickUpload&type=Flash';



    // config.extraPlugins = 'maxheight';

config.toolbar = [
 //   { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-','Templates' ] },
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ], itemsems: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], itemsems: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    // { name: 'folderrms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'language' ] },
    { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
    { name: 'insert', items: [ 'Image', 'Table', 'Smiley', 'SpecialChar' ] },
    // '/',
    { newame: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
    {   name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools',    items: [ 'Maximize', ] },
    { name: 'others', items: [ '-'  ] }
];

config.toolbar_Basic =
[
	['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink']
];



};
