var module = {}

module.filetree;

module.indexInit = function ()
{
	global.createDataTable('#modTbl');
}

module.getSelectNodes = function ()
{
	var selNodes = 	module.filetree.getSelectedNodes();

	var files = []
	
	$(selNodes).each(function(i, n){
		
		var href = n.data.href;
		
		files.push(href);
	});
	
	return files;
}

module.editInit = function ()
{
	module.filetree = $('#tree').fancytree({
		checkbox: true,
		select: function(e, o)
		{

			if (o.node.isSelected()) module.addLib(o.node.data.path);
			else module.removeLib(o.node.data.path);
		},
		
		deactivate: function (e, o)
		{
			global.log(o);	
		},
		click: function (e, data)
		{


		}
	}).fancytree('getTree');
	
	global.slimScroll('#tree');
	
	
	global.ckeditor($('#description'));
	
	//CKEDITOR.replace('description');

    $('#saveBtn').click(function(e){
        module.checkEditForm();
    });


    $('#newWidgetBtn').click(function(e){
        module.loadWidgetModal();
    });

    $('#cancelBtn').cancelButton('/module');

    /*
    if ($('#compTbl').exists())
    {
        $('#compTbl').dataTable();
    }
    */

    if ($('#widgetTbl').exists())
    {
        //$('#widgetTbl').dataTable();
    }
    
    
}

module.editModule = function (b, id)
{
	$(b).disableSpin();
	redirect('/module/edit/' + id);
}

module.addLib = function (href)
{
	var $well = $("<div>", {class:'well', style:"opacity:0" });
	

	$well.append(href);

	
	$('#selectedModules').append($well);
	
	$well.data('href', href);
	
	$well.velocity({ opacity:1 });


	console.log($well.data('href'));
	
	return $well;
}

module.removeLib = function (href)
{

	$('#selectedModules .well').each(function(i, w){
		
		var u = $(w).data('href').toString();
		
		if (u == href)
		{
			$(w).velocity({ opacity: 0 }, {
				complete:function()
				{
					$(this).remove();
				}
			});
			
			return true;
		}
	});
	
	return true;
}

module.checkEditForm = function ()
{

    if ($('#name').val() == '')
    {
        global.renderAlert("Please enter a module name!");
        $('#name').focus();
        return false;
    }

    if ($('#url').val() == '')
    {
        global.renderAlert("Please enter a module url!");
        $('#url').focus();
        return false;
    }

    if ($('#namespace').val() == '')
    {
        global.renderAlert("Please enter a module namespace!");
        $('#namespace').focus();
        return false;
    }


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);

    $('#saveBtn').attr('disabled', 'disabled');

    $.post("/module/save", $('#editForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
            	redirect('/module?site-success=' + escape(data.msg));
                //window.location = '/module?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                $('#saveBtn').removeAttr('disabled');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
                $('#saveBtn').removeAttr('disabled');
            }
    }, 'json');
}

module.loadWidgetModal = function (widgetId)
{
    $('#widgetModal').modal('toggle');

    global.ajaxLoader('#widgetModal');

    var url = '';

    if (widgetId !== undefined) url = '/' + widgetId;

    $.get("/module/widgetmodal/" + $('#id').val() + url, function(data){
        $('#widgetModal').html(data);
    });
}

module.saveWidget = function ()
{
    $.post("/module/savewidget", $('#widgetForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/module/edit/' + $('#id').val() + '?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'widgetAlert');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error', 'widgetAlert');
            }
    }, 'json');
}
