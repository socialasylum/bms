var crm = {}




crm.indexInit = function ()
{
	global.createDataTable('#crmTbl');

	return true;
}

crm.editInit = function ()
{
	$('select').selectpicker();
	
    crm.inputEditCheck();

    $('#addEmailBtn').click(function(e){
        crm.addEmail();
    });

    $('#addAddressBtn').click(function(e){
        crm.addAddress();
    });

    $('#addPhoneBtn').click(function(e){
        crm.addPhone();
    });

    $('#saveBtn').click(function(e){
        crm.checkEditForm();
    });

    $('#deleteBtn').click(function(e){
        crm.deleteContact();
    });

    $('#cancelBtn').cancelButton('/crm');

    CKEDITOR.replace('contactNote');
    CKEDITOR.replace('callNote');



    // $('#callTime').timespinner();

    $('#callDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });


    $('#industry').change(function(){
        if ($('#industry :selected').val() == '0')
        {
            $('#otherIndustryFormGroup').show(global.showEffect);
        }
        else
        {
            $('#otherIndustry').val(''); // clears value
            $('#otherIndustryFormGroup').hide(global.hideEffect);
        }
    });

    crm.addSortable();
    crm.phoneSortable();
    crm.emailSortable();
    

}

crm.addEmail = function ()
{
    var html = '';

    html = $('#emailSortList').find('li').html();

    $('#emailSortList').append("<li>" + html  + "</li>");
    $('#emailSortList').find('input').last().effect('highlight');

    $('#emailSortList input').last().val('');
    $('#emailSortList button').last().removeAttr('disabled');

    crm.emailSortable();

    crm.updateDelBtn('#emailSortList');
}

crm.updateDelBtn = function (div)
{

    $(div).find("button").each(function(index, item){

        if (index == 0)
        {
            $(item).attr('disabled', 'disabled');
        }
        else
        {
            $(item).removeAttr('disabled');
        }

    });

}

crm.addAddress = function ()
{
    var html = '';

    // html = $('#master-address').html();
    html = $('#addSortList').find('li').html();

    $('#addSortList').append('<li>' + html + '</li>');

    $('#addSortList').find('.panel').last().effect('highlight');
    $('#addSortList input#address_description').last().val('');
    
    $('#addSortList textarea').last().val('');
    
    $('#addSortList input#address_line_1').last().val('');
    $('#addSortList input#address_line_2').last().val('');
    $('#addSortList input#address_line_3').last().val('');
    
    $('#addSortList input#city').last().val('');
    $('#addSortList input#postalCode').last().val('');

    $('#addSortList select').last().val('');
    // $('#address-container button').last().removeAttr('disabled');

    crm.addSortable();
    
    crm.updateDelBtn('#addSortList');
}

crm.addPhone = function ()
{
    var html = '';

    // html = $('#master-phone').html();
    html = $('#phoneSortList').find('li').html();

    $('#phoneSortList').append('<li>' + html + '</li>');
    $('#phoneSortList').find('select').last().effect('highlight');
    $('#phoneSortList').find('input').last().effect('highlight');


    $('#phoneSortList input').last().val('');
    $('#phoneSortList button').last().removeAttr('disabled');
    $('#phoneSortList select').last().val('');

    crm.phoneSortable();

    crm.updateDelBtn('#phoneSortList');
    // crm.inputEditCheck();

}

crm.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert('Please enter a name for this contact');
        $('#name').focus();
        return false;
    }

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['contactNote'].getData();
    $('#contactNote').val(str);

    // $('#saveBtn').attr('disabled', 'disabled');

    $.post("/crm/save", $('#editForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            window.location = '/crm/edit/' + data.id + '?site-success=' + escape(data.msg);
            // global.renderAlert(data.msg, 'alert-success');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            $('#saveBtn').removeAttr('disabled');
            return false;
        }


    }, 'json');
}

crm.editContact = function (el, id)
{
    $(el).location('/crm/edit/' + id);
}

crm.deleteEmail = function (item)
{
    // var test = $(item).parent().parent().parent().parent().attr('class');

    if (confirm("Are you sure you wish to remove this e-mail address?"))
    {

        $(item).parent().parent().parent().parent().parent().hide(global.hideEffect, 500, function(){
            $(this).html('');

            crm.updateDelBtn('#emailSortList');
        });
    }
    // $(item).parent().parent().parent().parent().html('');

}

crm.deletePhone = function (item)
{

    if (confirm("Are you sure you wish to remove this phone number?"))
    {
        $(item).parent().parent().parent().parent().hide(global.hideEffect, 500, function(){
            $(this).html('');

            crm.updateDelBtn('#phoneSortList');

        }); //hides element first
        // $(item).parent().parent().parent().parent().html(''); // removes the html
    }
}

crm.deleteAddress = function (item)
{

    if (confirm("Are you sure you wish to remove this address?"))
    {
        $(item).parent().parent().hide(global.hideEffect, 500, function(){
            $(this).html('');

            crm.updateDelBtn('#phoneSortList');
        }); //hides element first

    }
}

crm.inputEditCheck = function ()
{
    // console.log('edit check called');
    $('#main-container').inputedit({
        change: function(item)
        {

            $('#edited').val('1');
            $('input[name=edited]').val('1');

            // alert('change!! in function' + $(item).attr('name'));
        }
    });

}

crm.deleteContact = function ()
{
    if (confirm("Are you sure you want to delete this CRM contact?"))
    {
        $('#deleteBtn').disableSpin();

        $.post("/crm/deletecontact", { contact:$('#id').val(), cgi_token:global.CSRF_hash  }, function(data){

            if (data.status == 'SUCCESS')
            {
            	redirect('/crm?site-success=' + encodeURI(data.msg));
            }
            else if (data.status == 'ALERT')
            {
				Warning(data.msg);
                $('#deleteBtn').enableSpin();
                return false;
            }
            else
            {
            	Danger(data.msg);
                $('#deleteBtn').enableSpin();
                return false;
            }


        }, 'json');
    }
}

crm.addSortable = function ()
{
    $('#addSortList').sortable({
        stop: function (event, ui)
        {
            crm.updateDelBtn('#addSortList');
        }
    });
}

crm.emailSortable = function ()
{
    $('#emailSortList').sortable({
        stop: function (event, ui)
        {
            crm.updateDelBtn('#emailSortList');
        }
    });
}

crm.phoneSortable = function ()
{
    $('#phoneSortList').sortable({
        stop: function (event, ui)
        {
            crm.updateDelBtn('#phoneSortList');
        }
    });
}
