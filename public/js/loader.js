// check for existing jQuery
var jQuery = window.jQuery,

// check for old versions of jQuery
oldjQuery = jQuery && !!jQuery.fn.jquery.match(/^1\.[0-4](\.|$)/),
localJqueryPath = '/public/js/jquery-1.11.1.min',
paths = {
  		jquery: 'jquery-1.11.1.min',
		async: 'requirejs-async/src/async',
  		"datatables": "/public/js/datatables/media/js/jquery.dataTables.min",
  		'socketio': 'https://cdn.socket.io/socket.io-1.0.0',
  		'ckeditor-core':'/public/ckeditor4.2.2/ckeditor',
  		'ckeditor-jquery':'/public/ckeditor4.2.2/adapters/jquery'
  },
noConflict;

// check for jQuery 
if (!jQuery || oldjQuery)
{
    // load if it's not available or doesn't meet min standards
    paths.jquery = localJqueryPath;
    noConflict = !!oldjQuery;
}
else
{
    // register the current jQuery
    define('jquery', [], function() { return jQuery; });
}

require.config({
	waitSeconds: 15,
	baseUrl: "/public/js",
	paths: paths,
	shim: {
	    "datatables": {
	        "deps": ['jquery']
	    },
	    'soocketio': {
		    exports: 'io'
	    },
	    'ckeditor-jquery': {
		    deps:['jquery', 'ckeditor-core']
	    }
	}
});



// require jquery usign the path config
// define(["jquery", "datatables", 'socketio', 'async!https://maps.googleapis.com/maps/api/js?v=3.exp'], function ($, datatables, io) {
define(["jquery", "datatables"], function ($, datatables, io) {
	// deal with jQuery versions if necessary
    if (noConflict) $.noConflict(true);
    
    //console.log($);
	$(window).trigger('js.init.ready');

	// loads jquery UI first
	var files = 
	[
		'/public/bootstrap/dist/js/bootstrap.min.js',
		'/public/js/jquery.functions.js/jquery-number.js/jquery.number.min.js',
		'/public/js/jquery-ui-1.11.0/jquery-ui.min.js',
		'/public/js/jquery.functions.js/jquery.functions.js',
		'/public/js/anystretch/jquery.anystretch.min.js',
		'/public/js/velocity/jquery.velocity.min.js',
		'/public/js/ajax-progress/js/jquery.ajax-progress.js'
	];
	
	require(files, function (){
		require([
			$.min('/public/js/ajax.js'),
			$.min('/public/js/global.js'),
			$.min('/public/js/bs.js'),
			//$.min('/public/js/msg_socket.js'),
			$.min('/public/js/jquery.functions.js/jquery.dialog.js'),
			$.min('/public/js/jquery.functions.js/jquery.alerts.js')
			], function (){
		
			var preview = GETParam('preview');
		

			if (preview == undefined && preview !== null)
			{
				global.loadJS();
			
				//ms.load(io);
			}

		});
	});
});
