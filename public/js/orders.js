var orders = {}

orders.indexInit = function ()
{
    if ($('#ordersTbl').exists())
    {
        $('#ordersTbl').dataTable({ "bStateSave": true });
    }
}

orders.detailsInit = function ()
{
    $('#saveBtn').click(function(e){
        orders.updateOrder();
    });

    CKEDITOR.replace('note');
    
    $('#cancelBtn').cancelButton('/orders');
}

orders.viewDetails = function (order)
{
    window.location = '/orders/details/' + order;
}

orders.updateOrder = function ()
{
    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['note'].getData();
    $('#note').val(str);

    $('#saveBtn').attr('disabled', 'disabled');

    $.post("/orders/update", $('#detailsForm').serialize(), function(data){
       if (data.status == 'SUCCESS')
        {
            window.location = '/orders/details/' + $('#id').val() + '?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');
            $('#saveBtn').removeAttr('disabled');
        }
    }, 'json');
}
