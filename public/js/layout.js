var layout = {};

layout.headingSel = $('#preview-body h1, #preview-body h2, #preview-body h3, #preview-body h4, #preview-body h5, #preview-body h6');

layout.index = {};


layout.index.init = function ()
{
	
	layout.index.updatePreview();

	$('select').selectpicker();

	$("#bgImg").uploader({
		uploadUrl: '/layout/upload',
		extraParams: { type: 'background-image' },
		CSRF:
		{
			enabled: true,
			key: 'cgi_token', 
			val: global.CSRF_hash
		},
		successFunction: function (d, fd)
		{
			$('#background-image').val(d.file);
			layout.index.updatePreview();
		}
	}).data('uploader');

	/*
	$("#contentBG").uploader({
		uploadUrl: '/layout/upload',
		extraParams: { type: 'contentBgImg' },
		CSRF:
		{
			enabled: true,
			key: 'cgi_token', 
			val: global.CSRF_hash
		},
		successFunction: function (d, fd)
		{
			$('#contentBgImg').val(d.file);
			layout.index.updatePreview();
		}
	}).data('uploader');
	*/
	$('#fontWeight,#headingWeight').inputSpinner({
		min:100,
		max:900,
		increment:100,
		onChange: function(val)
		{
			//$('#preview-body').css('font-weight', val);
		}
	});
	
	$('#borderThickness').inputSpinner({
		min:0,
		max:50,
		increment:1,
		onChange: function(val)
		{
			//$('#preview-body').css('font-weight', val);
		}
	});
    
    
    layout.index.slider($('#bgOpacitySlide'), $('#bgOpacity'), 0, 100, $('#opacityPercent'), '', '%');
    
    layout.index.slider($('#containerBGOpacitySlide'), $('#containerBGOpacity'), 0, 100, $('#containerOpacityPercent'), '', '%');
        
    layout.index.slider($('#contentBGOpacitySlide'), $('#contentBGOpacity'), 0, 100, $('#contentOpacityPercent'), '', '%');
    layout.index.slider($('#contentBorderRadiusSlide'), $('#contentBorderRadius'), 0, 180, $('#contentRadiusDisplay'), '', 'px');
    
    //if ($('#background-image').val().length > 0) layout.index.setPreviewBG($('#background-image').val());
    
    $('#saveBtn').click(function(){
	    layout.save();
    });
    
    $('#delBGImgBtn').click(function(e){
	    layout.index.deleteImg($('#background-image').val(), 'background-image', $(this), function(d){
		    //layout.index.setPreviewBG(null);
		    
		    $('#background-image').val('');
	    });
    });
    

    layout.index.setColorPicker($('#containerBGColor'));

    layout.index.setColorPicker($('#fontColor'));
    
    layout.index.setColorPicker($('#background-color'));

    layout.index.setColorPicker($('#headingColor'));

    layout.index.setColorPicker($('#contentBgColor'));

    layout.index.setColorPicker($('#borderColor'));
    layout.index.setColorPicker($('#footerBgColor'));      

      
    $('#compileBtn').click(function(e){
    	layout.save(function(data){
	    	layout.index.compile();    	
    	});

    });
    
    $('#resetBtn').click(function(e){
	    layout.index.reset();
    });
    
    $('#preview-panel').affixTop(65);
    
    $('#background-image').on('change', function(e){
	    if ($(this).val().length > 0)
	    {
		    $('#delBGImgBtn').removeAttr('disabled');
	    }
	    else
	    {
		    $('#delBGImgBtn').attr('disabled', 'disabled');
	    }
    });
	
	/*
	$('#bootstrapTheme').on('change click', function(e){
		layout.index.updatePreview();
	});
	*/
	
	layout.index.setPreviewHighlight();
	
    return true;
}

layout.index.setPreviewHighlight = function ()
{
	$('#layout-panels > .panel').each(function(i, panel){
		var $panel = $(panel);
		
		var selector = $panel.data('selector');
		
		if (selector !== undefined && selector.length > 0)
		{
			$panel.mouseenter(function(e){
				$($('#preview-panel').contents().find('html ' + selector)).effect('highlight');
				
			});
		}
	});
	
	return true;
}

layout.index.setPreviewBG = function (file)
{
	if (file == undefined || file.length <= 0)
	{
		$('#preview-body').css('background-image', 'none');
		return true;	
	}
	
	$('#preview-body').css('background-image', "url('/public/uploads/layout/" + $('#company').val() + '/' + file + "')");

	return true;
}

layout.index.setColorPicker = function ($el, cf)
{
	var preset = $el.val();
	
	$el.ColorPicker({
		color:preset,
        onSubmit: function(hsb, hex, rbg, el){
			layout.colorPickerChange($el, hex);
			
			//layout.index.updatePreview();
			
			if (cf !== undefined && typeof cf == 'function') cf(hex);
        },
        onChange: function(hsbc, hex, rgb){
			layout.colorPickerChange($el, hex);
			
			//layout.index.updatePreview();
			
			if (cf !== undefined && typeof cf == 'function') cf(hex);
        }
    });
    
    if (preset !== undefined && preset.length > 0)
    {
		layout.colorPickerChange($el, preset);
		
		//layout.index.updatePreview();
		
		if (cf !== undefined && typeof cf == 'function') cf(preset);
    }
    
	return true;
}

layout.index.slider = function ($el, $input, min, max, $lbl, prefix, suffix, slideFunction)
{
	if (min == undefined) min = 0;
	if (max == undefined) max = 100;
	if (prefix == undefined) prefix = '';
	if (suffix == undefined) suffix = '';
    
    var initVal = (suffix == '%') ? ($input.val() * 100) : $input.val();
    var initLbl = (suffix == '%') ? ($input.val() * 100) : $input.val();
    
    if ($lbl !== undefined) $lbl.text(prefix + initLbl + suffix);
    
    var actualValue;
    
	$el.slider({
        value: initVal,
        min: min,
        max: max,
        slide: function (event, ui){
           	
           	if ($lbl !== undefined) $lbl.text(prefix + ui.value + suffix);
           	
           	if (suffix == '%') actualValue = (ui.value == 0) ? 0 : (parseInt(ui.value) / 100);
           	else actualValue = ui.value; 
           	
		   	$input.val(actualValue);
		   	
		   	if (slideFunction !== undefined && typeof slideFunction == 'function') slideFunction(event, ui);
		   	
		   //$('#preview-body').css('opacity', (parseInt(ui.value) / 100));
        }
    });
    

}

layout.index.deleteImg = function (file, type, b, sf)
{
	if (type == undefined) type = null;
	
	if (confirm("Are you sure you wish to delete this image?"))
	{
		if (b !== undefined) $(b).disableSpin();
	
		$.post('/layout/deleteImg', { file:file, type:type, cgi_token:global.CSRF_hash }, function(data){
			if (b !== undefined) $(b).enableSpin();
			
			if (data.status == 'SUCCESS')
			{
				Success(data.msg);
				
				if (sf !== undefined && typeof sf == 'function') sf(data);
			}
			else
			{
				Danger(data.msg);
			}
		}, 'json');
	}
}

layout.index.reset = function ()
{
	if (confirm("Are you sure you wish to reset?"))
	{
		$('#resetBtn').disableSpin({ spinClass: 'fa fa-spin fa-history danger' });
	
		$.getJSON('/layout/reset', function(data){
			$('#resetBtn').enableSpin();
			
			if (data.status == 'SUCCESS')
			{
				$.unloadCSS('layout.css');
			
				Success(data.msg);
			}
			else
			{
				Danger(data.msg)
			}
		});		
	}
	
	return true;
}

layout.index.compile = function ()
{
	
	$('#compileBtn').disableSpin({ spinClass: 'fa fa-spin fa-cog' });
	
	var layout = '/public/uploads/layout/' + $('#company').val() + '/layout.css&' + $.randomString();
		
	$.getJSON('/layout/compile', function(data){
	
		$('#compileBtn').enableSpin();
		
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
			
			$.unloadCSS('layout.css');
			
			$.loadCSS(layout, true);
		}
		else if (data.status == 'WARNING')
		{
			Warning(data.msg, undefined, { clearTimeoutSeconds:7 });
		}
		else
		{
			Danger(data.msg, undefined, { clearTimeoutSeconds:7 });
		}
		
	});
	
	return true;
}

layout.removeLayoutCSS = function ()
{
	$('script').each(function(i, el){
		var $el = $(el);
		
		if ($el.indexOf('layout.css'))
		{
			$el.remove();
			return true;
		}
	});
	
	return true;
}

layout.index.updatePreview = function ()
{
	document.getElementById('preview-panel').contentDocument.location.reload(true);
	
	var r = $.randomString();

	var preview = '/public/uploads/layout/' + $('#company').val() + '/preview.css&' + r;

	// unload preview CSS
	//$.unloadCSS(preview);
	
	// loads new preview CSS
	//$.loadCSS(preview, true);
	
	return true;
}

layout.colorPickerChange = function ($el, hex)
{
	// gets inverse color
	
	var ic = inverseColor('#' + hex);
	
	$el.css('background-color', '#' + hex);
	$el.css('color', ic);
	$el.attr('hex', hex);
	$el.val(hex);
	
	return true;
}

layout.save = function (sf)
{
	$('#saveBtn').disableSpin();
	
	$.post('/layout/save_values', $('#layoutForm').serialize(), function(data){
		
		$('#saveBtn').enableSpin();
		
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
			
			if (sf !== undefined && typeof sf == 'function') sf(data);
			
			// unloads layout if set
			//$.unloadCSS('layout.css');
			
			layout.index.updatePreview();
		}
		else
		{
			Danger(data.msg);
		}
		
	}, 'json');
}