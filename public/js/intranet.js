var intranet = {}

intranet.portlet = 0;
intranet.dashboard;

intranet.widgetsPerRow = 6;
intranet.widgetsPerCol = 6;

intranet.widgetPadding = 5;
intranet.widgetDimW = 0;
intranet.widgetDimH = 0;
intranet.loader;

intranet.hideFooter = false;

intranet.gridSize = [ 4, 3 ];

intranet.misplacedWidgets = Array();

intranet.saveRequest; // stores ajax request for saving dashboard so it can be canceled


intranet.jsLoaded = []




$(window).on('global.post.jsloaded', function(e,d){
	if (window.location.pathname == '/intranet' || 	window.location.pathname == '/intranet/')
	{
			intranet.indexInit();
	}
});

intranet.indexInit = function ()
{
	var display = 'dashboard';
	
	$(window).on('preprocess.redirect', function(e, url){
		e.preventDefault();
		window.noredirect = true;
		
		intranet.openPage(url);
		
	});
	
	$(window).on('pre.new.active.widget', function(e, n){
		
	
		var $oNode = $('.dashboard-widget.active');
		//console.log($oNode);
		if ($oNode !== undefined)
		{
			$oNode.removeClass('active');
			intranet.hideToolbar($oNode);
			
			intranet.destroySlimScroll($('.display-scroll'));
		}
	});
	
	intranet.trackWidgetClick();


	intranet.adjustScreen(function(h){

		//intranet.loadDashboard();

	}, true);	

	$(window).resize(function(){

		intranet.adjustScreen(function(h){
			//intranet.resizeGrid();		
		});
	});

	
	if ($('#site-alert').exists())
	{
		$('#site-alert').remove();
	}
	
	$('#compressWidgetBtn').hide();
	
	$('#dashLockBtn').click(function(e){
		intranet.lockDash();
	});
	
	//intranet.homeBtnLoadDash();

	if (hashSet('url') !== undefined && display == 'page') intranet.openPage(hashVal(url));
	else if (display == 'dashboard') intranet.loadDashboard();

	intranet.checkHideFooter();
	intranet.listenDomChanges();

	return true;
}

intranet.homeBtnLoadDash = function ()
{
	$('#navHomeBtn i').attr('class', 'fa fa-dashboard');
	
	intranet.showHomeBtn();
	
	$('#navHomeBtn').click(function(e){
		e.preventDefault();
		
		intranet.loadDashboard();	
	});
}


intranet.homeBtn = function ()
{
	$('#navHomeBtn i').attr('class', 'fa fa-home');
	
	intranet.showHomeBtn();

	$('#navHomeBtn').click(function(e){
		e.preventDefault();
		
		intranet.loadDashboard();	
	});
}

intranet.hideHomeBtn = function ()
{
	
	if ($('#navHomeBtn').is(':visible') == false) return true;
	
	$('#navHomeBtn').velocity({ opacity:0 }, {
		complete: function ()
		{
			$(this).hide();
			global.adjustNavbar();
		}
	});
	
	return true;
};

intranet.showHomeBtn = function ()
{
	if ($('#navHomeBtn').is(':visible')) return true;
	
	$('#navHomeBtn').css('display', '');
	
	$('#navHomeBtn').velocity({ opacity:1 }, {
		complete:function ()
		{
			$(this).removeAttr('style');
			global.adjustNavbar();
		}
	});

	return true;
};

intranet.listenDomChanges = function ()
{
	$('.wrapper').on('DOMAttrModified', function(e){
				
		if ($(this).css('overflow') !== 'visible') $(this).css('overflow', 'visible');
	});

	$('body').on('DOMAttrModified', function(e){
				
		//if ($(this).css('overflow') !== 'visible') $(this).css('overflow', 'visible');
	});
}

intranet.loadDashboard = function ()
{
	$('.widgetBtn').show();

	// updates search
	$('#searchForm').removeAttr('action')
		.submit(function(e){
			e.preventDefault();
			
			var url = '/search?q=' + encodeURI($('#q').val());
			
			intranet.openPage(url);
		});

	$('#dashLockBtn').velocity('fadeIn', {
		complete:function(){
			global.adjustNavbar();
		}
	});

	intranet.hideHomeBtn();

	// scrols user back to top of screen
	$('body').velocity("scroll", { duration: 500, delay:400 });

	hashUnsetVar('url'); // unsets hash
	
	hashSetVar('display', 'dashboard');
	
	intranet.destroySlimScroll($('.wrapper'));

	$('.footer').velocity('fadeOut');
		
	//$(window).unbind('resize');
	
	var $wrapper = $("<div>", { class: 'wrapper' });
	
	//$('body').css('background', "url('/public/images/pattern.png') repeat");
	
	$wrapper.css('position', 'absolute')
		.css('display', 'block')
		.css('padding', 0);
	
	var $container = $('<div>', { id:'main-container', class:'container-fluid' });
	
	$container.css('padding', 0)
		.css('display', '')
		.css('background', 'none')
		.css('margin', 0)
		.css('opacity', 0);
	
	$wrapper.append($container);
	
	$('.wrapper').velocity('fadeOut', {
		complete:function()
		{
			$(this).remove();
			
			$wrapper.insertAfter($('#main-top-nav'));
	
			
			intranet.getDashboardHtml(function(html){
				
				global.backgroundStretch();
						
				$container.html(html);
				
				$('.dashboard-widget-body').css('height', "calc(100% - 30px)");
				
				intranet.adjustScreen(function(h){
							
					intranet.createSlimScroll($wrapper);
					
					intranet.createPortGrid();
					
					intranet.initializeHtml();
					
					//intranet.setNodeRefreshBtn();
	
					
	
					$(window).resize(function(){
						intranet.adjustScreen(function(h){
							intranet.resizeGrid();		
						});
					});

					
				}, true);
								
			});


		}
	});
	
	return true;

}

intranet.lockDash = function ()
{
	$('#dashLockBtn i').attr('class', 'fa fa-unlock');
	
	$('#dashLockBtn').unbind('click');
	
	$('#dashLockBtn').click(function(e){
		intranet.unlockDash();
	});
	
	$('.dashboard-widget-header').unbind('hover');
	
	$('.dashboard-widget-header').hover(function(e){
		$(this).css('cursor', 'auto');
	});

	intranet.destroySlimScroll($('.wrapper'));
		
	intranet.dashboard.disable_resize();
	intranet.dashboard.disable();	
}


intranet.unlockDash = function ()
{
	$('#dashLockBtn i').attr('class', 'fa fa-lock');
	
	$('#dashLockBtn').unbind('click');
	
	$('#dashLockBtn').click(function(e){
		intranet.lockDash();
	});
	
	$('.dashboard-widget-header').unbind('hover');
		
	$('.dashboard-widget-header').hover(function(e){
		$(this).css('cursor', 'move');
	});


	intranet.createSlimScroll($('.wrapper'));
		
	intranet.dashboard.enable_resize();
	intranet.dashboard.enable();	
}

intranet.getDashboardHtml = function (sf)
{
	$.get('/intranet/dashboard', function(data){
		$(window).trigger("dashboard.html.loaded", data);
		
		if (sf !== undefined && typeof sf == 'function') sf(data);
	});
	
	return true;
}


intranet.calcScreenDim = function (h)
{
//	intranet.widgetDimW = ($('#dashboard-grid').width() / intranet.widgetsPerRow) - (intranet.widgetPadding * 2);
//	intranet.widgetDimH = (h / intranet.widgetsPerCol) - (intranet.widgetPadding * 2) - 15;

	intranet.widgetDimW = 320;
	intranet.widgetDimH = 320;
		
	
	var dim = { width:intranet.widgetDimW, height:intranet.widgetDimH };
	
	//global.log(dim);
	
	return dim;
	
}

intranet.findEmptyNode = function ()
{
	var node;
	
	$('#gridUL > li').each(function(index, n){
		var $n = $(n);
		var url = $n.data('url');
		
		if (url == undefined || url == '')
		{
			node = $n;
			return true;
		}
	});
	
	
	return node;
}


intranet.trackWidgetClick = function ($nodes)
{
	if ($nodes == undefined) $nodes = $('#gridUL > li > div.dashboard-widget');


	$nodes.each(function(i, w){
		var $w = $(w);
		var $display = $w.find('.display');
		
		$w.unbind('click');
		
		
		$w.click(function(e){
		
			var active = $w.hasClass('active');	
			
			if (active == true)
			{
				//$w.removeClass('active');				
			}
			else
			{
				$(window).trigger('pre.new.active.widget', $w);
				
				intranet.activateNode($w);

			}

		});
	});

	$(window).on('postprocess.redirect', function(ev, url)
	{
		ev.preventDefault();
		//global.log(ev, true);
		console.log(url);
		
	});
}

intranet.activateNode = function ($node)
{
	intranet.createSlimScroll($node.find('.display-scroll'));
	intranet.showToolbar($node);
	$node.addClass('active');	
}

intranet.deactivateNodes = function ($node)
{
	intranet.destroySlimScroll($node.find('.display-scroll'));
	intranet.hideToolbar($node);
	$node.remoeClass('active');
	
	return true;
}

intranet.openPage = function (url, $node, type, options)
{
	if (type == undefined) type = '';
	
	if (options == undefined) options = {};
	
	var defaults = 
	{
		x:2,
		y:2
	};
	
	options = $.extend(true, {}, defaults, options);
	
	if (url == undefined)
	{
		Warning('URL is undefined! Unable to load the page');
		return false;	
	}

	var dashboard = (intranet.dashboard == undefined) ? false : true;
	
	if (type.toUpperCase() == 'WIDGET') dashboard = true;
	if (type.toUpperCase() == 'LINK') redirect(url);
	if (type.toUpperCase() == 'PAGE') dashboard = false;
	
	var orgUrl = url;
	
	url = $.addGETParam('dynamic', undefined, url);


	if (dashboard)
	{
				
		if ($node == undefined)
		{
			$node = intranet.findEmptyNode();
		
			if ($node == undefined)
			{
				$node = intranet.addWidget(url, undefined, { x:options.x, y:options.y });
			}
		}

		intranet.trackWidgetClick($node.find('.dashboard-widget'));
		
		$node.find("#widgetUrl").val(orgUrl);
		
		intranet.fetchWidgetHtml(url, function(data){
							
			var d = intranet.parsePageResponse(data);			
			
			
			$node.data('containerClass', d.meta.containerClass);
			$node.data('containerStyle', d.meta.containerStyle);
						
			if (d.meta.headcss !== undefined && d.meta.headcss.length > 0) $.loadCSSScripts(d.meta.headcss);

			intranet.setLinks($node);
				

			/*
			intranet.loadPageJS(d, function (){

			});
			
			*/

		}, $node);

		

		
		//intranet.renderWidgetProgressBar($node, 1);
		

	}
	else
	{
		hashSetVar('url', url);
		
		hashSetVar('display', 'page');
		
		
		if ($('#dashLockBtn').is(':visible')) $('#dashLockBtn').velocity('fadeOut');
	
		// scrols user back to top of screen
		$('body').velocity("scroll", { duration: 500, delay:400 });		


		intranet.clearScreen(function($container){
				
			intranet.fetchWidgetHtml(url, function(data){
				
				var d = intranet.parsePageResponse(data);
				
				if (d.meta.headcss !== undefined && d.meta.headcss.length > 0) $.loadCSSScripts(d.meta.headcss);
				
				$container.html(d.html);
	
				intranet.loadPageJS(d, function (){
					intranet.setLinks($container);
				});
						
			});
					
		});
	}



}

// sets layout to org state before anything is loaded
intranet.clearScreen = function (sf)
{
	$(window).unbind('resize');
	
	var $wrapper = $("<div>", { class: 'wrapper' });
	
	intranet.homeBtnLoadDash();

	
	$wrapper.css('position', 'relative')
		.css('display', 'block');
	
	var $container = $('<div>', { id:'main-container', class:'container' });
	
	$container.css('padding-top', '15px')
		.css('padding-left', '15px')
		.css('padding-right', '15px')
		.css('display', '');
	
	$wrapper.append($container);
			
	$('.wrapper').velocity('fadeOut', {
		complete:function(){
	
			$(this).remove();
			
			if (sf !== undefined && typeof sf == 'function') sf($container);
		}
	});
	

	$wrapper.insertAfter('#main-top-nav');
	
	if (!intranet.hideFooter) $('.footer').velocity('fadeIn');
	
	//$('body').css('background-image', 'none')
	//	.css('background-color', '#F4F5F5');

	return true;
}

intranet.trackAnchorClicks = function ()
{
	$('a').unbind('click');
	
	$('a').click(function(e){
		e.preventDefaut();

		var url = $(this).attr('href');
		
		intranet.openPage(url);
		
	});
}

// when a user sets a widget to active - 
intranet.moveUserToNode = function ($node)
{
	if ($node == undefined) throw new Error("Node is undefined");
	
}

// loads a JS file
intranet.loadJSFile = function (url, successFunction)
{
	if (url == undefined) throw new exception("url is undefined!");
	
	$.ajax({
		url: url,
		dataType: 'script',
		success: function (data, textStatus, jqxhr)
		{
			if (successFunction !== undefined && successFunction == 'function') successFunction(data, textStatus, jqxhr);
		},
		error: function ()
		{
			throw new Exception("Unable to fetch Javascript file: " + url);
		}
	});
	
	return true;
}

intranet.setLinks = function ($node)
{
	var as = $node.find('a');
	as.each(function(index, a){
		var $a = $(a);

		//console.log(this);

		var url = $a.attr('href');
		
		if (url == undefined) return true;

		var firstChar = url.substr(0, 1);
		
		if (firstChar == '#') return true;
		
		if (url.indexOf("javascript:") < 0)
		{

		$a.attr('href', "javascript:void(0);");
		
		$a.click(function(e){
			e.preventDefault();
			//intranet.openPage(url, $node);
			
			if (url == undefined) return false;
			if (url.substr(0, 1) == '#') return false;
			

				console.log("Going to: " + url);
				
				intranet.openPage(url, $node);

		});
	}		
		
	});
	

	
	return true;
}

intranet.expandWidget = function (b)
{
	intranet.saveDashboard(function(data){
		var $node = $(b).parent().parent().parent();
	
		var $li = $node.parent();
		
		intranet.destroyGrid();
		
		intranet.openPage($li.data('url'), $li, 'page');
		
		/*
		$('#compressWidgetBtn').velocity('fadeIn', {
		    complete:function()
		    {
			    global.adjustNavbar(); 
		    }
	    });
	    */
	});

	
	return true;
	
	intranet.saveDashboard();
	
	var h = $('#main-container').outerHeight(); + 'px'
	var w = $('#main-container').outerWidth() + 'px';
	
	intranet.homeBtnLoadDash();
	
	//intranet.destroySlimScroll($('.wrapper'));
		
	//$.log('height: ' + h + ' | w: ' + w);
	
	intranet.destroySlimScroll($('.display-scroll'));


	
	var $display = $(b).parent().parent().find('.display');
	
	var $clone = $display.clone();
	
	//var $wrapper = $("<div>", {id: "temp-wrapper"});

	var $container = $("<div>", {id: "temp-container", class:'container'});
	var $wrapper = $container;
		
	var $div = $("<div>", {class: "expand-display", style: "padding-left:0px;padding-right:0px;"});
	
	//$wrapper.append($container);
	
	$container.append($div);
	
	$div.append($clone.html());

	//$div.append("<div id='temp-container' class='container'>" + $clone.html() + "</div>")

	//$div.appendTo($wrapper);
	
	$wrapper.css('background', '#FFFFFF')
		.css('padding-top', '15px')
		.css('opacity', 0);
		
	
	//$clone.re.css('opacity', 0);
		//.css('padding-left', '15px')
		//.css('padding-right', '15px');


   
    var cw = $('#compressWidgetBtn').outerWidth();
    var qw = $('#q').width();
    
    $('#q').velocity({ width: (qw - cw) + 'px' });
    $('#compressWidgetBtn').velocity('fadeIn', {
	    complete:function()
	    {
		    global.adjustNavbar(); 
	    }
    });
    
	

	
	
	$wrapper.insertBefore('#main-container');

	
	$('#main-container').velocity({ marginLeft:'130%' }, {
		complete:function ()
		{
		
			
			$(this).hide();

			intranet.destroyGrid();

			if (!intranet.hideFooter)
			{
				$('.footer').appendTo('.wrapper');
	
				$('.footer').velocity('fadeIn');
			}
			
			//global.adjustNavbar(); 


			$('body').css('background', '#F4F5F5');
			
			/*
			$('body').velocity({ background:'#F4F5F5' }, {
				delay:100,
				duration:800
			});
			*/
			//intranet.createSlimScroll($('.wrapper'));

			
			$wrapper.velocity('fadeIn', {
				delay:100,
				duration:800,
				complete:function ()
				{
					

					//$('body').css('background', '#FFFFFF');
					
					global.adjustNavbar(); 
				}
			})
		}
	});
	
	
	

	
	//$('.wrapper').removeAttr('style');
	
	var $li = $node.parent();
	
	//.velocity({ width:w, height:h });
	
	//console.log($display);	

}

intranet.compressWidget = function (b)
{
	
	
	//$('body').velocity({ background:"url('/public/images/pattern.png') repeat" });
	$('body').css('background', '');

	$('.wrapper').css('background', 'none');
	

	//intranet.destroySlimScroll($('.wrapper'));


		
    var cw = $('#compressWidgetBtn').outerWidth();
    var qw = $('#q').width();
    
    $('#q').velocity({ width: (qw + cw) + 'px' });

	$('#compressWidgetBtn').velocity('fadeOut');

	$('.footer').velocity('fadeOut');
	
	$('.footer').insertAfter('.wrapper');


	$('#temp-container').velocity('fadeOut', {
		complete: function ()
		{
			
		//	intranet.createSlimScroll($('.wrapper'));
				
			$('#main-container')
				.show()
				.velocity({ marginLeft:'0' }, {
					complete:function()
					{
						intranet.adjustScreen(function(h){
	
							intranet.createPortGrid();

							//intranet.bindTextFocus();
							intranet.setNodeRefreshBtn();
							
							intranet.createSlimScroll($('.display-scroll'));
									
							//intranet.resizeGrid();
							
							intranet.refreshWidgetDisplayHtml();						
						});


					}
				});
			
			//global.adjustNavbar(); 
			

			
			$('#temp-container').remove();
		}
	})
}

intranet.refreshWidgetDisplayHtml = function ($node)
{
	if ($node == undefined) $node = $('#gridUL > li .dashboard-widget');
	
	$node.each(function(index, n){
		$n = $(n);
		
		var $display = $n.find('.display');
		var $displayScroll = $n.find('.display-scroll');
		
		$display.redraw();
	});
}

// goes throu all ports and intializes their display when page is fresh loaded
intranet.initializeHtml = function ($nodes)
{
	if ($nodes == undefined) $nodes = $('#gridUL > li');

	$nodes.each(function(index, n){
		var $n = $(n);
		var url = $n.data('url');
		var purl = url; // parsed URL
		
		purl = purl.replace('?dynamic&', '?'); // first param of many
		purl = purl.replace('&dynamic&', '&'); // in middle many params
		purl = purl.replace('?dynamic', ''); // only param
		purl = purl.replace('&dynamic&', ''); // last param
		
		intranet.trackWidgetClick($n.find('.dashboard-widget'));
		
		$n.find("#widgetUrl").val(purl);
				
		if (url == undefined) return false;
		if (url == '') return false;
		
		intranet.fetchWidgetHtml(url, function(){
			
		}, $n);
		
	});
}

intranet.fetchWidgetHtml = function (url, successFunction, $node)
{
	// saves widget history
	if ($node !== undefined) intranet.addWidgetHistory($node, url);

	$.when(
	$.ajax({
		method: 'GET',
		url: url,
		progress: function (e)
		{
			if(e.lengthComputable) {
				//calculate the percentage loaded
				var pct = (e.loaded / e.total) * 100;
			
				//log percentage loaded
				if ($node !== undefined) intranet.setWidgetProgressBar($node, pct);
			}
			else
			{
				//this usually happens when Content-Length isn't set
				console.warn('Content Length not reported!');
			}	
		},
		success: function (html)
		{
			if ($node !== undefined) intranet.renderNodeDisplay($node, url, html);
			
			if (successFunction !== undefined && typeof successFunction == 'function') successFunction(html);
			
		},
		error: function ()
		{
			
		}
	})
	).done(function (){
		return  true;		
	});

	return false;
}

intranet.addWidgetHistory = function ($node, url)
{
	
	var history = $node.data('history');
	
	if (history == undefined) history = [];
	
	history.push(url);
	
	$node.data('history', history);
	
	/*
	$.post('/intranet/addWidgetHistory', { widget:1, url:url, cgi_token: global.CSRF_hash }, function(data){
		if (data.status == "ERROR")
		{
			Danger(data.msg);
		}
	}, 'json');
	*/
	//console.log($node.data('history'));
	return true;
}

/**
* splits the response between JSON object data and HTML
*
* @return Object - { meta, html }
*/
intranet.parsePageResponse = function (data)
{
	if (data == undefined) throw new Error("node display data is undefined!");
	
	var splitter = "<!-- HTML_BEGIN -->";
	
	var meta = jQuery.parseJSON(data.substring(0, data.indexOf(splitter)));
	
	//global.log(meta);
	
	//global.log(meta, true);
	var html = data.substring(data.indexOf(splitter));
	
	//global.log(html);
	
	return { meta: meta, html: html };
}

/**
* processes the javascript int he head of page response
*
* @return array - array of src's that were loaded
*/
intranet.loadPageJS = function (d, sf)
{
	var js = [];

	// load headlibs
	if (d.meta.headlibs !== undefined)
	{
		$(d.meta.headlibs).each(function(k, src){
			if (isJS(src))
			{
				//intranet.jsLoaded.push(src);

				js.push(src);
			}
		});
	}
	
	/*
	if (d.meta.civars.headscript !== undefined)
	{
		var srcs = $(d.meta.civars.headscript).getSrcs();

		$(srcs).each(function(index, src){
			
			if ($.isJS(src))
			{
				js.push(src);	
			}
			
		});
	}
	*/

	if (js.length > 0)
	{
		$.requireJS(js, function(rdata){

			eval(d.meta.civars.onload);
			
			if (sf !== undefined && typeof sf == 'function') sf();
		});
	}
	else
	{
		eval(d.meta.civars.onload);
		if (sf !== undefined && typeof sf == 'function') sf();
	}

	return js;
}

intranet.renderNodeDisplay = function ($node, url, data)
{
	if ($node == undefined) throw new Error("No is undefined!");
	if (url == undefined) throw new Error("URL is undefined!");
	if (data == undefined) throw new Error("node display data is undefined!");

	$node.data('url', url);

	var $display = $($node.find('.display'));

	intranet.renderWidgetProgressBar($node, 0);
	
	var d = intranet.parsePageResponse(data);
	
	/*
	var splitter = "<!-- HTML_BEGIN -->";
	
	var meta = jQuery.parseJSON(data.substring(0, data.indexOf(splitter)));
	//global.log(meta, true);
	var html = data.substring(data.indexOf(splitter));
	*/
	
	intranet.setWidgetTitle($node, d.meta.pageTitle);

	
	var js = intranet.loadPageJS(d);
	
	$node.data('js', js);
	
	/*
	var js = [];
	
	// load headlibs
	if (d.meta.headlibs !== undefined)
	{
		$(d.meta.headlibs).each(function(k, src){
			if ($.isJS(src))
			{
				js.push(src);	
			} 
			//$.loadJS(src);
		});
	}
	
	if (d.meta.civars.headscript !== undefined)
	{
		var srcs = $(d.meta.civars.headscript).getSrcs();

		$(srcs).each(function(index, src){
			
			if ($.isJS(src))
			{
				//console.log("HEADSCRIPT: " + src);
				js.push(src);	
			}
			
		});
	
		//$(srcs).loadJSSrcs();
	}
	
	if (js.length > 0)
	{
		// assigns array to node
		$node.data('js', js);
	
		//global.log(js);
		$.requireJS(js, function(rdata){
			eval(d.meta.civars.onload);

			//global.log(d);
		});
	}
	*/	

	
	$display.html(d.html);
	
	intranet.setLinks($node);

}

intranet.setWidgetTitle = function ($node, title)
{
	$node.find('span.widget-name').html(title);
}

intranet.createIframe = function ($node)
{
	var iframeHtml = $('<iframe />'); 

	$node.find('.display').append(iframeHtml);

	var iframe = $node.find('.display iframe');	
	
	
	$(iframe).load(function(){
		intranet.setNodeRefreshBtn($node);
		
		//intranet.renderWidgetProgressBar($node, 0);
	});
	//alert($(iframe).prop('tagName'));
	$(iframe).bind('DOMMouseScroll', function(){
			global.log('scoll1');
		intranet.hideToolbar($node);
	});

	$(iframe).bind('mousewheel', function(){
	global.log('scoll2');
		intranet.hideToolbar($node);
	});

	
	return iframe;
}


intranet.createSlimScroll = function ($display)
{
	if ($display == undefined) $display = $('#gridUL > li .dashboard-widget .display-scroll');
	//global.log($node, true);
	
	$display.each(function(index, el){
		var $el = $(el);
				
		//global.log('created slimscrollbar -');
		//global.log($el);
		
		$el.perfectScrollbar({
			wheelSpeed: 20,
			wheelPropagation: true,
			minScrollbarLength: 20,
			includePadding: true
		}).css('overflow', 'hidden');		
	});

}


intranet.destroySlimScroll = function ($display)
{
	if ($display == undefined) throw new Error('No display defined!');

	$display.each(function(index, el){
		var $el = $(el);
		
		$el.perfectScrollbar('destroy');
		
		$el.removeClass('ps-container'); // removes slimscroll style
		
		//global.log('destroyed slimscrollbar -');
		//global.log($el);
		
		//$el.css('overflow', '');
	});
	
	return true;
}

intranet.updateSlimScroll = function ($display)
{
	if ($display == undefined) $display = $('#gridUL > li .dashboard-widget .display-scroll');
	
	$display.each(function(index, el){
		var $el = $(el);
		
		$el.perfectScrollbar('update');
	});

}

intranet.createPortGrid = function ()
{
	// get container width

	//var dim = ($('#dashboard-grid').width() / intranet.widgetsPerRow) - (intranet.widgetPadding * 2);
	
	//global.log('WIDGET DIM: ' + dim);

	intranet.dashboard = $('#dashboard-grid #gridUL').gridster({
		widget_base_dimensions:[ intranet.widgetDimW,intranet.widgetDimH ],
		//widget_base_dimensions:[ 300, 200 ],
		widget_margins: [intranet.widgetPadding, intranet.widgetPadding],
		min_cols:4,
		widget_selector: "li[data-type='portlet']",
		//max_rows: 3,
		autogrow_cols: true,
		max_size_x: 4,
		max_size_y: 1,
		resize: {
			enabled: true,
			stop: function (e, ui, $widget)
			{
				
				//global.log($widget);
				
				//intranet.findInvalidCells();
				$display = $widget.find('.display-scroll');
				
				//var sizex = $widget.data('sizex');				
				//var sizey = $widget.data('sizey');

								
				//global.log(sizex + ' x ' + sizey);


				intranet.saveDashboard();
				/*
				intranet.dashboard.resize_widget($widget, w, h, function(){
					//intranet.saveDashboard();
				});
				*/
				
				//this.$player.coords().update({ height: newHeight, width: w });
				

				
				intranet.updateSlimScroll($('.dashboard-widget.active .display-scroll'));
				intranet.updateSlimScroll($('.wrapper'));
				
				//console.log('resize Wrapper Width: ' + intranet.dashboard.wrapper_width);
			}
		},
		draggable:{
			stop: function ()
			{
				$display = $(this).find('.display-scroll');
				
				intranet.saveDashboard();

				intranet.updateSlimScroll($('.dashboard-widget.active .display-scroll'));
				intranet.updateSlimScroll($('.wrapper'));
				//intranet.findInvalidCells();
				//intranet.updateSlimScroll($display);
				//$('#dashboard-grid ul').css
				//console.log('drag Wrapper Width: ' + intranet.dashboard.wrapper_width);
				//intranet.dashboard.set_dom_grid_width(5);
			},
			start: function ()
			{
				//console.log(JSON.stringify(this.$player.coords()));
				var coords = this.$player.coords();
				
			
				//console.log(JSON.stringify(coords.coords.width));
				
				//this.$player.coords().update({ width: intranet.widgetDimW });
				
				//console.log(JSON.stringify(coords.coords.width));

			}
		}
	}).data('gridster');
	
	//intranet.loader.loader.setPercent(100);
}

/*
intranet.findInvalidCells = function ()
{

	intranet.misplacedWidgets = Array();


	$('#dashboard-grid ul').find('li').each(function(index, widget){

		var c = $(widget).data('coords');
		
		if (c !== undefined)
		{
			if (c.grid.row > intranet.gridSize[1] || c.grid.col > intranet.gridSize[0])
			{
				intranet.misplacedWidgets.push(widget);
			}
			//console.log(c.grid.row);
			//console.log(c.grid.col);		
		}

	});
	

	global.log('Mis Placed Widgets: ' + intranet.misplacedWidgets.length);
	global.log(JSON.stringify(intranet.misplacedWidgets));
}
*/
intranet.removeWidget = function (b)
{
	var el = $(b).parent();
	
	intranet.dashboard.remove_widget(el);
	
}

intranet.resizeGrid = function ()
{

	if (intranet.dashboard == undefined) return false;
		
	intranet.dashboard.options.widget_base_dimensions = [ intranet.widgetDimW,intranet.widgetDimH ];

	intranet.dashboard.generate_grid_and_stylesheet();
	intranet.dashboard.recalculate_faux_grid();


	return true;
}

intranet.destroyGrid = function ()
{
	intranet.dashboard.destroy();
	
	intranet.dashboard = undefined;
	
	$(window).unbind('.gridster');

	return true;
}

// should the user some how scroll, will push them back up tot op
intranet.keepScrollTop = function ()
{
	$(window).scroll(function(e){
		intranet.ScrollToTop();
	});
}

// single user to scroll to top
intranet.ScrollToTop = function ()
{
	if ($(window).scrollTop() > 0)
	{
		$('body').velocity("scroll", { duration: 500, delay:400 });

		return true;
	}
	
	return false;
}

intranet.adjustScreen = function (callback, fadeIn)
{
	if (fadeIn == undefined) fadeIn = false;
	
	var wh = $(window).height();
	
	//global.log("Window Height: " + wh);
	
	var navHeight = $('#main-top-nav').outerHeight();

	//global.log("Nav Height: " + navHeight);
	
	var finalH = (wh - navHeight);
	
	//$('#main-container').height(finalH);

	$('.wrapper').css('position', 'absolute');
	$('.wrapper').velocity({
		background:'#FFFFFF'
		//backgroundImage: "url('/public/images/pattern.png')"
	},{
		duration:200
	});
	$('#main-container').velocity({ height: finalH  }, {
		duration: 200,
		complete: function()
		{
			intranet.calcScreenDim(finalH);
							
			if (typeof callback == 'function')
			{
				callback(finalH);
			}
			
				$('.wrapper').velocity({ borderBottom:'none' }, {
					complete:function()
					{
						$(this).css('box-shadow', 'none');
					}
				});
		}
	});

	if (fadeIn)
	{
		$('#main-container').velocity({ opacity:1 });		
	}

	
	
	$('.wrapper')
		.css('margin-bottom', 0)
		.css('padding-bottom', 0);
		
	//$('body').css('overflow', 'hidden');
}


intranet.addWidget = function (url, htmlOnly, options)
{
	if (url == undefined) url = '';
	if (htmlOnly == undefined) htmlOnly = false;

	if (options == undefined) options = {};
	
	var defaults = 
		{
			x:2,
			y:2
		};
	
	options = $.extend(true, {}, defaults, options);

	var widgetHtml = $('#widgetHtml').html();
	//var clone = $.parseHTML(widgetHtml);
	
	var clone = $(widgetHtml);
	//var clone = $.parseHTML(widgetHtml);

	clone.attr('class', '').removeAttr('style');

	clone.data('url', url)
		.data('sizex', options.x)
		.data('sizey', options.y);



	// only load HTML widget and not add it to active 
	if (!htmlOnly)
	{
		var nextPosition = intranet.dashboard.next_position(options.x, options.y);
		
		
		var portlet = intranet.getPortCnt();
		
		portlet++;
		
		clone.data('row', nextPosition.row)
			.data('col', nextPosition.col);
		
		//intranet.blurToolbars();
		
		intranet.dashboard.add_widget(clone, options.x, options.y);
		
		intranet.saveDashboard();
		
		//intranet.trackWidgetClick($(clone).find('.dashboard-widget'));
	
		//intranet.bindTextFocus(clone);
		
		//intranet.createSlimScroll(clone.find('.display-scroll'));
		intranet.setNodeRefreshBtn(clone);
	}
	
	return clone;
}


intranet.deleteWidget = function (b)
{
	var $node = $(b).parent().parent();
	
	$node.velocity({ opacity:0 }, {
		complete:function(){
			intranet.dashboard.remove_widget($(this).parent(), function(){

				var cnt = intranet.getPortCnt();

				if (cnt <= 0) intranet.openPage('/intranet/welcome', undefined, 'WIDGET', { x:2, y:1 });

				intranet.updateSlimScroll($('.wrapper'));
				intranet.saveDashboard();
			});
		}
	});

	return true;
}

intranet.saveDashboard = function (sf)
{
	if (intranet.saveRequest !== undefined)
	{
		intranet.saveRequest.abort();
	}
	
	$nodes = $('#gridUL > li.gs-w');
	
	if ($nodes.length < 1) return false;

	var allData = { ports: {}, cgi_token: global.CSRF_hash  }
	
	
	$nodes.each(function(index, n){
		var $n = $(n);
		
		var data = $n.data();
		
		allData.ports[index] = { 
			row: data.row ,
			col: data.col,
			x: data.sizex,
			y: data.sizey,
			url: data.url
		};
	});

	intranet.saveRequest = $.ajax({
		url: '/intranet/savedashboard',
		type:'post',
		data: allData,
		dataType: 'json',
		success: function(data)
		{
			if (data.status == 'SUCCESS')
			{
				if (sf !== undefined && typeof sf == 'function') sf(data);	
			}
		}
	});

	return allData;
}

/*
intranet.postDashboardPositions = function (data)
{
	global.log("POSTING");
	global.log(data);
}
*/


intranet.bindTextFocus = function ($node)
{
	if ($node == undefined) $node = $('#gridUL li .dashboard-widget');
	

	
	$node.each(function(index,el){
		var $el = $(el);
		var $toolbar = $el.find('.dashboard-widget-toolbar');
		var $widgetUrl = $toolbar.find('.widgetUrl');
		
		var $btn = $toolbar.find('#delReBtn');

		$widgetUrl.focus(function(){
			$btn.html("<i class='fa fa-arrow-circle-right'></i>");

			$btn.unbind('click');
			$widgetUrl.unbind('keypress');
		
			$widgetUrl.bind('keypress', function(e){
				var code = e.keyCode || e.which;
				
				if (code == 13)
				{
					intranet.openPage($widgetUrl.val(), $node);
					//intranet.updateIframeUrl($el, $widgetUrl.val());
				}
			});
			
			$btn.click(function(){

				//intranet.updateIframeUrl($el, $widgetUrl.val());
				
				intranet.setRefreshBtn($node, $btn);
			});
		});
		
		/*
		$widgetUrl.blur(function(){

		});
		*/
	});
}

intranet.setRefreshBtn = function ($node, $btn)
{
	$btn.html("<i class='fa fa-refresh'></i>");
	
	$btn.unbind('click');
	
	$btn.click(function(){
		intranet.refreshWidgetDisplay($node);
	
		//intranet.refreshIframe($node);
	});
}

intranet.refreshWidgetDisplay = function ($node)
{
	if ($node == undefined) throw new Exeption("No $node defined!");
	
	
	$node.each(function(index, el){
		var $el = $(el);
		
		
		
		//var url = $el.parent('li').attr('data-url');
		var url = $el.parent('li').data('url');
		
		
		intranet.renderWidgetProgressBar($el, 1);

		//url = url + "?dynamic";

		intranet.fetchWidgetHtml(url, function(html){
		
			$node.data('url', url);
		
			//$node.attr('data-url', url);
			intranet.renderWidgetProgressBar($node, 0);

			$node.find('.display').html(html);
		
	
	
		}, $node);
	});

	
}

intranet.setNodeRefreshBtn = function ($node)
{
	if ($node == undefined) $node = $('#gridUL li .dashboard-widget');
	
	$node.each(function(index, el){
		var $el = $(el);
		intranet.setRefreshBtn($el, $el.find('#delReBtn'));		
	});
	

}

intranet.showToolbar = function ($nodes)
{
	if ($nodes == undefined) throw new Exeption("No $node defined!");
	
	$nodes.each(function(i, n){
		var $n = $(n);
		
		var $toolbar = $(n).find('.dashboard-widget-toolbar');
					
		if ($toolbar.height() <= 0)
		{
			$toolbar.velocity({ height:'40px', opacity:'0.95' });
		}
	});
	
	
}

intranet.updateIframeUrl = function ($node, url)
{

	if ($node == undefined) return false;

	url = url.replace('http://', '');
	url = url.replace('https://', '');	

	var $iframe = $node.find('iframe');
	
	if ($iframe.length == 0)
	{
		$iframe = intranet.createIframe($node);
	}
	
	url = 'http://' + url;
	console.log(url);
	
	$node.data('url', url);

	
	$iframe.attr('src', url);
	//intranet.renderWidgetProgressBar($node, 1);
	
	//$('#widget-link').remove();

	//$iframe.removeAttr('name');
	
	return url;
}

intranet.renderWidgetProgressBar = function ($node, op)
{
	if ($node == undefined) throw new Exeption("No $node defined!");
	
	if (op == undefined) op = 1;
	
	var pb = $node.find('.progress');

	pb.velocity({ opacity: op });

	return true;
}

intranet.setWidgetProgressBar = function ($node, percent)
{
	if (percent == undefined) percent = 0;
	
	if ($node == undefined) throw new Exeption("No $node defined!");
		
	$node.find('.progress-bar').css('width', percent + '%');
	
	return true;
}



intranet.refreshIframe = function ($node)
{
	if ($node == undefined) throw new Exeption("No $node defined!");
	
	// reload iframe
	//$node.find('iframe').contentWindow.location.reload();
	
	
}

intranet.blurToolbars = function ($node)
{
	if ($node == undefined) $node = $('#gridUL li .dashboard-widget .dashboard-widget-toolbar');

	/*
	$node.each(function(index, el){
		$(el).foggy();	
	});
	*/
}

intranet.hideToolbar = function ($node, includeActive)
{
	if (includeActive == undefined) includeActive = true;
	
	if ($node == undefined) throw new Exeption("No $node defined!");
	
	//console.log(includeActive);
	
	$node.each(function(i, n){
		var $n = $(n);
		
		var $toolbar = $n.find('.dashboard-widget-toolbar');
			
		if ($toolbar.height() > 0)
		{
			if ($n.hasClass('active'))
			{
				if (includeActive == true)
				{
					console.log('ATIVE');
					console.log($n);
				}	
			}
			
			$toolbar.velocity({ height:'0px', opacity:'0' });	
			
		}	
	});
	

}

intranet.getPortCnt = function ($node)
{
	var maxCnt = 0;
	
	/*
	$('#dashboard-grid ul > li > input').find("input").each(function(index, item){
		if ($(item).attr('type') == 'hidden')
		{
			var portlet = parseInt($(item).attr('portlet'));
			
			if (portlet > maxCnt) maxCnt = portlet;
		}	
	});
	*/
	
	return $('#dashboard-grid ul > li').length;
}

intranet.loginInit = function ()
{
	if ($('#email').val().length <= 0) $('#email').focus();
	else $('#password').focus();
	
	$('#rememberMeBtn').click(function(e){
		var v = (parseInt($('#rememberMe').val()) == 1) ? true : false;
		if (v)
		{
			$('#rememberMeBtn i')
				.addClass('fa-square-o')
				.removeClass('fa-check-square-o');

				$('#rememberMe').val('0');
		}
		else
		{
			$('#rememberMeBtn i')
				.removeClass('fa-square-o')
				.addClass('fa-check-square-o');
				
			$('#rememberMe').val('1');
		}
		
	});
	
	/*
	$('#loginBtn').click(function(e){
		$(this).disableSpin();
	
		return true;
	});
	*/
	if ($('#submitFPBtn').exists())
	{
	    $('#submitFPBtn').click(function(e){
	        intranet.checkForgotPasswordForm();
	    });
	}
}

intranet.loginSubmit = function ()
{
	
	$('#loginBtn').disableSpin();
	
	return true;
}

intranet.landingInit = function ()
{

    $('#portSort').sortable({
        placeholder: 'widget highlight span6',
        stop: function (event, ui)
        {
            intranet.savePortletOrder(event, ui);
        }
    });

	$('li.widget').contextMenu({
		target: 'li',
		debug: global.debug,
		ajax:
		{
			dataType: 'html'
		},
		data:
		{
			url: '/intranet/landingrightclick'
		},
		selected: function(e)
		{
			
		}
	});

	/*
        $.rightClickMenu({
            selector: 'li.widget',
            src: '/intranet/landingrightclick',
            selected: function (port, menu)
            {
                if ($(menu).attr('action') == 'DELETE')
                {
                    intranet.clearPortlet(port);
                }
                else
                {
                    intranet.loadWidget($(menu).attr('url'), $(menu).attr('id') ,$(port).val());
                }


            }
        });
		*/
    intranet.portletInit();

    intranet.companyReqCheck();

    $('#reqcheckModal').modal({
        show: false,
        backdrop: 'static',
        keyboard:false
    });

    $('#reqcheckModal').on('hide.bs.modal', function (){
        // clears HTML in modal
        $('#reqcheckModal').html('');
    });
}

intranet.resetpasswordInit = function ()
{
    $('#submitBtn').click(function(e){
        intranet.checkResetPasswdForm();
    });
}

intranet.widgetRightClick = function (e, item, id)
{
    global.clearRightClick();

    var parentOffset = item.parent().offset();
    //or $(this).offset(); if you really just want the current element's offset
    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;

    // $('body').append("<div id='right-click-menu' style=\"top:" + String(e.pageY)  + "px;left:" + String(e.pageX) + "px;\">" + html + "</div>");
    $('body').append("<div id='right-click-menu' style=\"top:" + String(e.pageY)  + "px;left:" + String(e.pageX) + "px;\"></div>");

    global.ajaxLoader('#right-click-menu');


    intranet.portlet = item.attr('value');
    
    $('#right-click-menu').html($('#portRightClick').html());

    /*
    $.get("/intranet/widgetrightclick", function(data){
        $('#right-click-menu').html(data);
    });
    */
    // $('#rightClickDD').dropdown('toggle');

}

intranet.loadWidget = function (url, id, portlet, save)
{

    if (portlet == undefined) portlet = intranet.portlet;

    if (save == undefined) save = true;

    global.ajaxLoader('#widget_' + portlet);

    $.get(url, function(data){
        $('#widget_' + portlet).html(data);
        $('#widget_' + portlet).attr('widget', id); // sets wiget ID in LI

        global.clearRightClick();

        global.slimScroll('#widget_' + portlet + ' #widget-content', { height: '300px' });



        // any type of widget init
        if ($('#usersWidgetTable').exists())
        {
            $('#usersWidgetTable').dataTable();
        }

        if ($('#annTbl').exists())
        {
            $('#annTbl').dataTable();
        }

        // for territory map widget
        if ($('#widgetMap').exists())
        {
            // console.log('init map');
            var map = gm.initialize('widgetMap', undefined, undefined, undefined, false);
            gm.loadSavedMarkers(true, false, false);
        }

        if (save == true)
        {
            intranet.saveWidgetPorlet(portlet, id);
        }
    });
}

intranet.saveWidgetPorlet = function (portlet, widget)
{
    $.post("/intranet/savewidgetportlet", { portlet: portlet, widget: widget, cgi_token: global.CSRF_hash }, function(data){
        if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-error');
        }
    }, 'json');

}

intranet.portletInit = function ()
{
    $('#portSort').find("input").each(function(index, item)
    {
        if ($(item).attr('type') == 'hidden')
        {
            // url is defined - load widget
            if ($(item).attr('url') !== '')
            {
                intranet.portlet = $(item).attr('portlet');
                intranet.loadWidget($(item).attr('url'), $(item).attr('value'), $(item).attr('portlet'), false);
            }
            else
            {

                $('#widget_' + $(item).attr('portlet')).html($('#noWidgetHtml').html());

            }
        }
    });


}

intranet.savePortletOrder = function (event, ui)
{
    var cnt = 1;
    $('#portSort').find("li").each(function(index, item){

        if ($(item).attr('id') != undefined)
        {
            // console.log(" LI ID: " + $(item).attr('id') + ':' + $(item).attr('widget'));
            // will now get port ID if one is assigned

            if ($(item).attr('widget') > 0)
            {
                $.post("/intranet/saveportorder", { port: cnt, widget: $(item).attr('widget'), cgi_token:global.CSRF_hash }, function(data){
                    //code...
                });
            }

            cnt++;
        }
    });
    /*
    var data = $("#portSort").sortable('serialize');

    $.post("/intranet/saveportorder", data + '&' + $.param({ 'cgi_token': global.CSRF_hash }), function(data){
    }, 'json');
    */
}

intranet.buildPortRightlClick = function ()
{

    widgets = new Array();

    $('#hiddenWidgetInputs').find("input").each(function(index, item){

        if ($(item).attr('type') == 'hidden')
        {
            var id = String($(item).val());
            var name = String($(item).attr('widgetName'));

            var ob = { id: {'name': name }};

            widgets.push(ob);
        }
    });

    // console.log(JSON.stringify(widgets));

    // var widgetItems = widgets.join();

    var widgetItems = "{ " + widgets.join(", ") + " } ";

    /*
    if (widgets.length > 0)
    {
        for (var i = 0; i <= widgets.length; i++)
        {
            // jQuery.extend(true, widgetItems, widgets[i]);
        };
    }
    */
    // alert(widgetsFinal);


    $.contextMenu({
        selector: 'li.widget',
        callback: function (key, options){
            alert(key);
            // console.log('KEY: ' + key);
            // console.log('options: ' + options);
        },

        items: $.contextMenu.fromMenu($('#html5menu'))
    });

}

intranet.clearPortlet = function (port)
{

    if (confirm("Are you sure you wish to remove this widget?"))
    {
        $.post("/intranet/clearportlet", { port: $(port).val(), cgi_token:global.CSRF_hash }, function(data){

            if (data.status == 'SUCCESS')
            {
                $(port).html($('#noWidgetHtml').html());
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
        }, 'json');
    }
}



// checks if company and departments have been created
intranet.companyReqCheck = function ()
{

    $.getJSON("/intranet/reqcheck", function(data){

        if (data.status == 'SUCCESS')
        {
            // no changes were made, basically nothing needs to happen right now
        }
        else if (data.status == 'SELECT_POSITION' || data.status == 'SELECT_DEPARTMENT')
        {


            $('#reqcheckModal').html(data.html);
            $('#reqcheckModal').modal('show');
        }
        else if (data.status == 'ALERT')
        {
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    });

}

intranet.checkDepartmentSelect = function (b)
{
    if ($('#reqcheckModal #department :selected').val() == '')
    {
        global.renderAlert('Please select a department', undefined, 'selDepAlert');
        return false;
    }

    $(b).attr('disabled', 'disabled');

    $.post("/department/assign", $('#selDepForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            // hides modal, reruns check
            $('#reqcheckModal').modal('hide');
            intranet.companyReqCheck();

            // window.location = '/intranet/landing';
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'selDepAlert');
            $(b).removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'selDepAlert');
            $(b).removeAttr('disabled');
            return false;
        }

    }, 'json');

}


/*
 * checks postion select in modal on landing page
 * during intial setup process
 */
intranet.checkPositionSelect = function (b)
{
    if ($('#reqcheckModal #position :selected').val() == '')
    {
        global.renderAlert('Please select a position', undefined, 'selPosAlert');
        return false;
    }

    $(b).attr('disabled', 'disabled');

    $.post("/position/assign", $('#selPosForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            // hides modal, reruns check
            $('#reqcheckModal').modal('hide');
            intranet.companyReqCheck();
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'selPosAlert');
            $(b).removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'selPosAlert');
            $(b).removeAttr('disabled');
            return false;
        }

    }, 'json');

}

intranet.checkForgotPasswordForm = function ()
{
    if ($('#fpEmail').val() == '')
    {
    	Warning('Please enter an email address!');
        $('#fpEmail').effect(global.effect);
        $('#fpEmail').focus();
        return false;
    }

    $.post("/intranet/forgotpassword", { fpEmail: $('#fpEmail').val(), cgi_token:global.CSRF_hash  }, function(data){

            if (data.status == 'SUCCESS')
            {
                $('#passwordModal').modal('hide');
                $('#fpEmail').val(''); // clears email value

				Success(data.msg);
            }
            else
            {
            	Danger(data.msg);
            }
    }, 'json');
    
    return true;
}

intranet.checkResetPasswdForm = function ()
{
    if ($('#password').val() == '')
    {
        global.renderAlert("Please enter a password!");
        $('#password').focus();
        $('#password').effect(global.effect);
        return false;
    }

    if ($('#confirmPassword').val() == '')
    {
        global.renderAlert("Please confirm the password!");
        $('#confirmPassword').focus();
        $('#confirmPassword').effect(global.effect);
        return false;
    }

    if ($('#password').val() != $('#confirmPassword').val())
    {
        global.renderAlert("Passwords do not match!");
        $('#password').focus();
        $('#password').effect(global.effect);
        return false;
    }

    $('#submitBtn').attr('disabled', 'disabled');

    $.post("/intranet/processpasswordrequest", $('#resetPasswordForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = "/intranet/login?site-success=" + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                $('#submitBtn').removeAttr('disabled');
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                $('#submitBtn').removeAttr('disabled');
                return false;
            }

    }, 'json');


}

intranet.editMenu = function (url)
{
	redirect(url);
	
	return true;
};

intranet.checkHideFooter = function ()
{
	var hideFooter = $('body').attr('data-hideFooter');

	if (hideFooter == undefined || hideFooter == '0') hideFooter = false;
	else if (hideFooter == '1') hideFooter = true;
	
	intranet.hideFooter = hideFooter;
	
	return hideFooter;
};
