var articles = {}

articles.viewInit = function ()
{
    CKEDITOR.replace('comment');

    $('#submitCmtBtn').click(function(e){
        blog.leaveComment('articles');
    });

    $('#returnBtn').click(function(e){

        window.location = '/articles';

        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-arrow-left');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').addClass('fa-spin');
    });
}

articles.articleClick = function (b)
{
    window.location = '/articles/view/' + $(b).attr('articleID');

    $(b).attr('disabled', 'disabled');
    $(b).find('i').removeClass('fa-arrow-right');
    $(b).find('i').addClass('fa-spinner');
    $(b).find('i').addClass('fa-spin');
}

articles.readMore = function (b)
{
    window.location = '/articles/view/' + $(b).attr('articleID');
    articles.setReadMoreBtnDisabled($(b).attr('articleID'));
}

articles.setReadMoreBtnDisabled = function (articleID)
{
    $('#readMoreBtn' + String(articleID)).attr('disabled', 'disabled');
    $('#readMoreBtn' + String(articleID)).find('i').removeClass('fa-arrow-right');
    $('#readMoreBtn' + String(articleID)).find('i').addClass('fa-spinner');
    $('#readMoreBtn' + String(articleID)).find('i').addClass('fa-spin');
}
