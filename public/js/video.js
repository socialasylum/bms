var video = {}

video.indexInit = function ()
{

    folders.folderInit('#video-container', 'video');

    $('#youtubeBtn').click(function(e){
        video.checkYoutubeForm();
    });

    video.setFileDraggable(false);

    $('#video-container').find("li").each(function(index, item)
    {
            // videos
            if ($(item).attr('itemType') == '2')
            {
                // if they double click on a article
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    window.location = "/video/view/" + $(item).attr('value') + '/' + $('#folder').val() + '/' + $('#rootFolder').val();
                });
            }
    });


    $('#createFolderBtn').click(function(e){
        folders.createFolder();
    });

    $('#helpBtn').click(function(e){
        global.viewModKBArticle($('#module').val());
    });
}

video.editInit = function ()
{
    CKEDITOR.replace('description');

    $('#saveBtn').click(function(e){
        video.checkEditForm();
    });

    $('#cancelBtn').cancelButton('/video');
}

video.checkYoutubeForm = function ()
{
    if ($('#youtubeUrl').val() == '')
    {
        global.renderAlert("please enter a YouTube URL", undefined, 'youtubeAlert');
        $('#youtubeUrl').focus();
        $('#youtubeUrl').effect(global.effect);
        return false;
    }

    $('#closeYoutubeModalBtn').attr('disabled', 'disabled');
    $('#youtubeBtn').attr('disabled', 'disabled');

    $.post("/video/saveyoutubeurl", $('#youtubeForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            window.location = '/video/index/' + $('#folder').val() + '/0?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'youtubeAlert');
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'youtubeAlert');
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
    }, 'json');
}

video.setFileDraggable = function(clearPreviousSortable)
{
        if (clearPreviousSortable == undefined) clearPreviousSortable = false; // deafults to false

        if (clearPreviousSortable == true)
        {
            // removes previous functionality
            $('#fileList').sortable('destroy');
        }

        // goes through each li and for folders makes it so they are not disbled
        $('#fileList').find("li").each(function(index, item)
        {
            // folders
            if ($(item).attr('itemType') == '1')
            {
                $(item).removeClass('ui-state-disabled');
            }
        });

        $('#fileList').selectable({
            filter: "li",
            selected: function (event, ui)
            {
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }

                    $(ui.selected).draggable({
                    revert: true,
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#fileList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });

}

video.checkEditForm = function ()
{
    if ($('#title').val() == '')
    {
        global.renderAlert('Please enter a title for this video!');
        return false;
    }

    $('#saveBtn').attr('disabled', 'disabled');
    $('#editForm').submit();
}
