var reporting = {}


reporting.indexInit = function ()
{
    // global.noRightClick();
    // $(document).bind("contextmenu",function(e){ return false; });

    folders.folderInit('#report-container', 'reporting');
    global.setFileDraggable(false);

    // goes through each li in report-container and makes draggable/droppable
    $('#report-container').find("li").each(function(index, item)
    {
            // documents
            if ($(item).attr('itemType') == '2')
            {
                // if they double click on a document
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    window.location = "/reporting/view/" + $(item).attr('value') + '/' + $('#folder').val() + '/' + $('#rootFolder').val();
                });

                var id = $(item).attr('value');

                // when the user right clicks on the screen
                $(item).mousedown(function(e){
                    switch (e.which)
                    {
                        case 3:
                        // formbuilder.indexRightClick(e, $(item), id);
                        break;
                    }
                });
            }

            // datasets
            if ($(item).attr('itemType') == '3')
            {
                // if they double click on a document
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    window.location = "/reporting/createds/" + $(item).attr('value') + '/' + $('#folder').val();
                });
            }
    });

    $.rightClickMenu({
        selector: '.item',
        src: '/reporting/rightclick/' + $('#folder').val(),
        selected: function (item, action)
        {
            if (action == 'edit')
            {
                if ($(item).attr('itemType') == '2')
                {
                    // report
                    window.location = '/reporting/edit/' + $(item).val();
                }
                else if ($(item).attr('itemType') == '3')
                {
                    // dataset
                    window.location = '/reporting/createds/' + $(item).val() + '/' + $('#folder').val()
                }
            }
            // alert($(item).attr('itemID'));
        }
    });

}

reporting.editInit = function ()
{
    $('#saveBtn').click(function(e){
        reporting.checkEditForm();
    });

    $('#cancelBtn').cancelButton('/reporting');

}

reporting.createdsInit = function ()
{
    reporting.renderTblTree();

    $('#saveBtn').click(function(e){
        reporting.saveDataset();
    });

    $('#execBtn').click(function(e){
        reporting.checkVariables();
    });

    $('#cancelBtn').cancelButton('/reporting');

    // executes query if editing
    if (parseInt($('#id').val()) > 0)
    {
        reporting.execSQL();
    }

    /*
    CKEDITOR.replace('query');
    CKEDITOR.config.extraPlugins = 'syntaxhighlight';
    CKEDITOR.config.syntaxhighlight_lang = 'SQL';
    CKEDITOR.config.syntaxhighlight_hideGutter = true;
    CKEDITOR.config.syntaxhighlight_hideControls = true;
    */

}

reporting.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert('Please enter a report name!');
        return false;
    }

    $('#saveBtn').attr('disabled', 'disabled');
    $('#cancelBtn').attr('disabled', 'disabled');

    $.post("/reporting/save", $('#editForm').serialize(), function(data){

            if (data.status == 'SUCCESS')
            {
                window.location = '/reporting?site-success=' + escape(data.msg);
                // global.renderAlert(data.msg, 'alert-success');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }

    }, 'json');
}


reporting.containerIndexResize = function ()
{
    // var h = $(window).height() - 265;
    var h = $(window).height() - 200;

    $('.report-container').css('min-height', h + 'px');
}


reporting.renderTblTree = function ()
{
    global.ajaxLoader('#folder-tree');

    $.get("/reporting/foldertree", function(data){
        $('#folder-tree').html(data);
        $('#folder-tree').dynatree({
            onClick: function(node, event)
            {

                // msg.currentFolder = node.data.key;

                // msg.getFolderContent(node.data.key);
            }
        });

        // msg.setTreeDroppable();

        global.slimScroll('#free-tree');
    });
}

reporting.execSQL = function ()
{

    global.renderAlert();

    global.ajaxLoader('#resultsDisplay');

    // reporting.checkVariables($('#query').val());

    $.post("/reporting/execsql", $('#dsForm').serialize(), function(data){
        $('#resultsDisplay').html(data);
    });
}

reporting.checkVariables = function ()
{
    var query = $('#query').val();

    // n represents number of variables found in SQL statement
    var n = query.replace(/[^@]/g, "").length;

    if (n > 0)
    {
        $.post("/reporting/variables", { query: $('#query').val(), cgi_token:global.CSRF_hash }, function(data){
            $('#varModal').html(data);

            $('#varModal').modal('show');

            $('#varExecBtn').click(function(e){
                reporting.execSQL();

                $('#varModal').modal('hide');
                $('#varModal').html('');

            });
        });
    }
    else
    {
        reporting.execSQL();
    }
}

reporting.saveDataset = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert('Please enter a name for this dataset!');
        $('#name').effect('highlight', function(){ $(this).focus();  });
        return false;
    }


    if ($('#query').val() == '')
    {
        global.renderAlert('Please enter a SQL statement for this dataset!');
        $('#query').effect('highlight', function(){ $(this).focus();  });
        return false;
    }

    $.post("/reporting/saveds", $('#dsForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            window.location = "/reporting/index/" + $('#folder').val();
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}
