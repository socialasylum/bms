var search = {}

search.indexInit = function ()
{
    search.updateCnts();

    if ($('#crmTblSearch').exists())
    {
        $('#crmTblSearch').dataTable();
    }

    if ($('#userSearchTbl').exists())
    {
        $('#userSearchTbl').dataTable();
    }

    if ($('#msgSearchTbl').exists())
    {
        $('#msgSearchTbl').dataTable();
    }
}

search.userClick = function (id)
{
    window.location = '/user/edit/' + id;
}

search.viewKBArticle = function (id)
{
    window.location = '/kb/view/' + id;
}

search.viewCrmContact = function (contact)
{
    window.location = '/crm/edit/' + contact
}

search.viewForm = function (form)
{
    window.location = '/formbuilder/view/' + form;
}

search.viewVideo = function (video)
{
    window.location = '/video/view/' + video;
}


search.updateCnts = function ()
{
    $('#formCntDisplay').text($('#totalFormCnt').val());
    $('#KBCntDisplay').text($('#totalKBCnt').val());
    
    $('#CRMCntDisplay').text($('#totalCRMCnt').val());
    
    $('#userCntDisplay').text($('#totalUserCnt').val());
    
    // hides top part of table if no users to show
    if ($('#userSearchTbl').exists() && $('#totalUserCnt').val() == 0)
    {
        $('#userSearchTbl').hide(global.hideEffect);
    }


    $('#msgCntDisplay').text($('#totalMsgCnt').val());

    // hides top part of table if no messages to show
    if ($('#msgSearchTbl').exists() && $('#totalMsgCnt').val() == 0)
    {
        $('#msgSearchTbl').hide(global.hideEffect);
    }

    
    $('#docCntDisplay').text($('#totalDocCnt').val());
    
    $('#videoCntDisplay').text($('#totalVideoCnt').val());

}
