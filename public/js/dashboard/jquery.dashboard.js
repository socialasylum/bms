
/*
* Author: William Gallios
* Created: 6/11/2014
* Description: used to setup doasbhard elements and widgets


// Example:

		intranet.dashboard = $('#main-container').Dashboard({
			debug: true,
			styleSheetPath: '/public/js/dashboard/'
		}).data('Dashboard');
		

		intranet.dashboard.addWidget({
			title: "Another Widget",
			dragHandle: '.dashboard-widget-header',
			draggable: true
		});
		
		
*/

;(function($) {

	'use strict';
	
	var wHtml = "<div class='dashboard-widget'>"+
	"<div class='dashboard-widget-header'><span class='widget-name'>Widget Name</span> <button class='close-widget'>&times</button></div>" +
	"<div class='dashboard-widget-body'></div>" +
	"<div class='dashboard-widget-footer'></div>" +
	"</div> <!-- /.widget -->";
	
	var defaults = 
	{
		debug: false,
		containerID: 'dashboard-container',
		ctonainerClass: 'dashboard-container',
		styleSheetPath: '/jquery.dashboard/',
		styleSheet: 'jquery.dashboard.css',
		checkStyleSheet: true,
		loadStyleSheet: true,
		velocity: true,
		widgetHtml: wHtml
	};
	
	
	var widgetDefaults =
	{
		title: '',
		draggable: true,
		width:300,
		height:300,
		max_width: 0,
		dragHandle: '.dashboard-widget-header'
	}

	function Dashboard (el, options)
	{
		this.options = $.extend(true, {}, defaults, options);		
		this.$el = $(el);

		
		// checks if the velocity plugin is enabled
		var vel = (jQuery().velocity) ? true : false;
		
		if (vel == false) this.options.velocity = false;

		// cash widgetHtml
		this.init();


		return this;
	}	

	var fn = Dashboard.prototype; // Prototypes
	
	Dashboard.widgets = [];

    	
	
    fn.init = function ()
    {
		// checks if the drag plugin is loaded
		var vel = (jQuery().velocity) ? true : false;

		if (vel)
		{
			this.options.velocity = true; // enables velocity if loaded
		}
    
	    var containerHtml = "<div id='" + this.options.containerID + "' class='" + this.options.ctonainerClass +  "'></div>";
		
		this.container = $.parseHTML(containerHtml);
		
		this.$el.append(this.container);
		
		
		
		var cssLoaded = this.checkStyleSheetLoaded();
		
		if (!cssLoaded)
		{
			if (this.options.loadStyleSheet) this.loadCSS();
		}
		
		
		//this.makeDraggable();
		
		
		
		return true;
    };
    
    fn.addWidget = function (woptions)
    {
    	
    	
		//if (woptions == undefined) woptions = {};
	    //console.log('Add Widget');
	    
	    var widget = $.parseHTML(this.options.widgetHtml);
	    
	    // element will fade in
	    if (this.options.velocity) $(widget).css('opacity', 0);
	    
	    woptions = $.extend(widget, widgetDefaults, woptions);

	    $(widget).data(woptions);
	    
	    var $this = this;
	    
	    $(widget).find('span.widget-name').html(woptions.title);
	    
	    
	    $(widget).find('button.close-widget').bind('click', function(){
		    $this.closeWidget(widget);
	    });
	    
	    $(this.container).append(widget);
	  
	    var $node = $(this.container).find('.dashboard-widget').last();

		// adds node to widget array
		Dashboard.widgets.push($node);
		

	    if (this.options.velocity) $node.velocity('fadeIn');
	    
	    if (woptions.draggable)
	    {
	    	$node.draggable({ 
	    		handle: woptions.dragHandle,
	    		scroll: true,
	    		snap: true,
	    		refreshPositions: true,
	    		containment: '#' + this.options.containerID
	    	});
	    }
	    
	    $node.resizable({
		    minHeight: 100,
		    minWidth: 200,
		    grid: 10,
		    ghost: true,
		     autoHide: true 
	    });
	    
	   //this.makeWidgetDraggable(widget);
	    
	  
	    

	    
	    
	    
	    return true;
    };
     

	// checks if the style sheet for plugin is loaded
	fn.checkStyleSheetLoaded = function ()
	{
		var cssLoaded = false;
		
		for (var i = 0; i < document.styleSheets.length; i++)
		{
			var CSShref = document.styleSheets[i].href;
			
			if (CSShref == null) continue;
			
			var n = CSShref.indexOf(this.options.styleSheet);
			
			if (n > 0)
			{
				cssLoaded = true;
				break;
			}
		}
		
		if (cssLoaded) return true;

		return false;
	}
	
	// loads CSS Files in Head
	fn.loadCSS = function ()
	{
		$('head').append( $("<link rel='stylesheet' type='text/css' />").attr('href', this.options.styleSheetPath + this.options.styleSheet) );
		
		return true;
	}
	
	/*
	fn.makeWidgetDraggable = function (w)
	{
		global.log(w);
		$(w).draggable({ handle: w.dragHandle });   
		
		return true;
	}
*/
	fn.closeWidget = function (widget)
	{
		if (this.options.velocity)
		{
			$(widget).velocity('fadeOut', {
				complete:function(){
					$(this).remove();
				}
			});			
		}
		else
		{
			$(widget).remove();			
		}

		console.log('close widget');

		return true;
	}

	// jquery Adapter
    $.fn.Dashboard = function (options)
    {
	    return this.each(function(){
		     if (!$(this).data('Dashboard'))
		     {
		     	
		     	var inst = new Dashboard( this, options )

			 	

			    $(this).data('Dashboard', inst);
		     }
	    });
    };
    
    $.Dashboard = fn;
    

	
     
 
})(jQuery);


