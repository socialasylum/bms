var app = {}

app.indexInit = function ()
{

    $('#appTbl').dataTable();
    $('#openTbl').dataTable();

}

app.postpositionInit = function ()
{
    $('#startDate').datepicker({
        dateFormat:"yy-mm-dd"
    });

    $('#endDate').datepicker({
        dateFormat:"yy-mm-dd"
    });


    CKEDITOR.replace('description');

    $('#postBtn').click(function(e){
        app.checkPostForm();
    });

    $('#cancelBtn').click(function(e){
        if (confirm("Are you sure you wish to cancel?"))
        {
            window.location = "/app";
            $('#cancelBtn').attr('disabled', 'disabled');
        }
    });

    if ($('#position').exists())
    {
        $('#position').change(function(){
            app.getPositionDescription($('option:selected', this).val());
        });
    }
}

app.getPositionDescription = function(id)
{
    if (id == undefined || id == '')
    {
        $('#position-description').html('');
        return true;
    }

    global.ajaxLoader('#position-description');

    $.get("/app/getpositiondesc/" + id, function(data){
        $('#position-description').html(data);
    });
}

app.checkPostForm = function ()
{
    if ($('#position').exists())
    {
        if ($('#position :selected').val() == '')
        {
            global.renderAlert('Please select a position!');
            $('#position').focus();
            return false;
        }
    }

    if ($('#title').exists())
    {
        if ($('#title').val() == '')
        {
            global.renderAlert('Please enter a job title for this post');
            $('#title').focus();
            return false;
        }
    }

    if ($('#locationId').exists())
    {
        if ($('#locationId :selected').val() == '')
        {
            global.renderAlert('Please select a location!');
            $('#locationId').focus();
            return false;
        }
    }


    if ($('#locationTxt').exists())
    {
        if ($('#locationTxt').val() == '')
        {
            global.renderAlert('Please enter the location for this post');
            $('#locationTxt').focus();
            return false;
        }
    }
    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);

    $('#postBtn').attr('disabled', 'disabled');

    $.post("/app/postopenposition", $('#createForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                $('#cancelBtn').attr('disabled', 'disabled');

                window.location = "/app?site-success=" + escape("Position has been posted!");

            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                $('#postBtn').removeAttr('disabled');
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                $('#postBtn').removeAttr('disabled');
                return false;
            }
    }, 'json');
}
