/*
	intranet.loader = $('body').loader({
		percent: 50,
		backdrop: true,
		debug: true,
		fadeoutDelay: 0,
		updated: function (percent)
		{
			global.log("Loader has been updated! " + percent);
		},
		complete: function ()
		{
			global.log("Loader has finished, executed complete callback funciton");
		}
	});
*/

;(function($){
    $.fn.loader = function (options)
    {
    	var this_ = this;

		
    	if (options.backdrop == undefined) options.backdrop = true;
    	if (options.size == undefined) options.size = 80;
    	if (options.percent == undefined) options.percent = 0;
    	if (options.debug == undefined) options.debug = false;
    	if (options.fadeoutDelay == undefined) options.fadeoutDelay = 400;

    	 
		// get height of container
		var containerHeight = this.outerHeight();
		
		var h = $(window).height();
		var w = $(window).width();
				
		
		if (options.debug) console.log(this.prop("tagName"));
		
		var positionCSS = (this.prop("tagName") == 'BODY') ? "position:absolute;" : '';

		
    	var html = "<div class='loader-container' style=\"opacity:0;" + positionCSS + "\">"+
    	"<div class='loader shadow'><canvas id='dyn-ajax-loader'></canvas></div>" +
    	"</div>";
    	
		var backdropHtml = $("<div>", { class:'loader-backdrop' });
		
		if (this.prop("tagName") == 'BODY') $(backdropHtml).css('position', 'absolute');
		
		$(backdropHtml).width(w);
		$(backdropHtml).height(h);

    	if (options.backdrop) this.prepend(backdropHtml);
    	this.prepend(html);
    	
    	
    	
    	var $node = this.find('.loader-container');
    	var $backdrop = this.find('.loader-backdrop');
    	    	
    	//this.node = $node;
    	
    	$node.velocity({ opacity:1 });
	    
	    var clLoader = $('#dyn-ajax-loader').ClassyLoader({
	    	animate: false,
	    	percentage: 0,
	    	width:options.size,
	    	diameter: 30,
	    	//fontFamily: 'Open Sans',
	    	fontSize: '16px',
	    	height:options.size
		});
		

		$.fn.loader.setPercent = function (percent)
		{	
			clLoader.setPercent(percent).show();

			
			if (typeof options.updated == 'function') options.updated(percent);

			if (percent >= 100)
			{
				if (options.debug) console.log("Trigger Hide Function now");
				
				if (typeof options.complete == 'function') options.complete();
				
				if (options.debug) console.log("Setting timeout: " + options.fadeoutDelay);
				
				setTimeout(function(){
					this_.loader.clearLoader();	
				}, options.fadeoutDelay);
				
			}
				
			return this_;
		}
		
		$.fn.loader.clearLoader = function ()
		{
			if (options.debug) console.log('hiddding loading!');
			
			$node.velocity({ opacity:0 });
			$backdrop.velocity({ opacity: 0 }, {
				complete:function(){
			
					$node.remove();
					$backdrop.remove();		
				}
			});
			
		}
		
		return this;
	}

	
})(jQuery);