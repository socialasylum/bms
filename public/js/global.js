var global = {}


global.CSRF_token = 'token';

global.CSRF_hash = '';

global.showEffect = 'highlight';
global.hideEffect = 'highlight';

global.effect = 'highlight';

global.logoutTimeout = 3600000; // 1 hour
global.logoutTimer;

global.logoutWarn = 600000; // 10 minutes
global.warnTimer;

global.notTimeoutSeconds = 60000; // millisecond until timeout checks again for msgs, notifcations etc.
global.notificationTimeout;
global.msgNotificationTimeout;
global.subNotificationTimeout;

global.logged_in = false;
global.userid;
global.admin = false;
global.email;
global.company = 0;

global.debug = true;


global.trackingMouse = false;
global.mx;
global.my;

global.$sel;
global.bgh = 0;

global.onDashboard = false;


global.navanimrun = false;

/**
 * Jquery functions
 */


jQuery.fn.cancelButton = function(location)
{
	

    this.click(function(e){
        if (confirm("Are you sure you wish to cancel?"))
        {
			$(this).disableSpin();
    
        	redirect(location);
           // window.location = location;
            this.setAttribute('disabled', 'disabled');
        }
    });
};


$(function(){
	// user is on dashboard, sets global flag
	if (window.location.pathname == '/intranet' || 	window.location.pathname == '/intranet/')
	{
		global.onDashboard = true;
	}
});

global.loadJS = function ()
{
	var userid = $('body').data('userid');
	var company = $('body').data('company');

	if (userid !== undefined) global.userid = userid;
	if (company !== undefined) global.company = company;
	
	bs.init();
	//global.backgroundStretch();

	$(window).trigger('global.pre.jsloaded');

	var headlibs = $('body').data('javascript');

	if (headlibs == undefined)
	{
		//global.log("HEAD LIBS Undefined");
	}
	else
	{
		require(headlibs, function(d){
			global.postJSloaded();
			global.setLogoutTimer();
			var onload = $('body').data('init');
			eval(onload);
			$(window).trigger('global.post.jsloaded', d);
		});
	}
	
	if ($('body').data('token') !== undefined)
	{
		global.CSRF_hash = $('body').data('token');
		
		ajax.setup(global.CSRF_hash);
	}

    // if hidden input exists for CSRF token, gets value
    if ($('#' + global.CSRF_token).exists())
    {
        global.CSRF_hash = $('#' + global.CSRF_token).val();

		ajax.setup(global.CSRF_hash);
    }

	if ($('#dash-actions button').exists()) $('#dash-actions button').tooltip({container: 'body'});
	
	if ($('#rebuildCSSBtn').exists())
	{
		$('#rebuildCSSBtn').click(function(e){
			global.rebuildCSS();
		});
	}
    
    if ($('#numusers').exists())
    {
        $('#numusers').on('keyup', function(e) {
            $('#numusersspan').text($(this).val());
            $('#totalprice').text($(this).val() * 5);
            $('#price').val($(this).val() * 5);
        });
    }
    
	return true;
};



global.validateUser = function (sf)
{
	$.getJSON('/intranet/validate/' + global.userid, function(data){
		if (data.status = 'SUCCESS')
		{
			global.email = data.email;
		
			if (sf !== undefined && typeof sf == 'function') sf(data);
		}
	});
}


global.setLogoutTimer = function ()
{
	// sets 1 hourtime until they are logged out
	global.logoutTimer = setTimeout(function(){
		global.logout();
	}, global.logoutTimeout);


	global.warnTimer = setTimeout(function(){
		if ($('#logoutWarnModal').exists()) $('#logoutWarnModal').modal('show');
	}, (global.logoutTimeout - global.logoutWarn));

	return true;
}


// resets logout timer
global.stayLoggedIn = function ()
{
	clearTimeout(global.logoutTimer);
	clearTimeout(global.warnTimer);
	$('#logoutWarnModal').modal('hide');
	return true;
};

global.postJSloaded = function ()
{
    if ($('#lockModal').exists())
    {
        $('#lockModal').modal({
            backdrop: 'static',
            keyboard:false,
            show: false
        });

        $('#lockModal').on('shown.bs.modal', function (){
            global.modalBackdropLock();
        });
    }

	if ($('#logoutWarnModal').exists())
	{
		$('#logoutWarnModal').on('hidden.bs.modal', function (e) {
			global.setLogoutTimer();

		});
	}

    if ($('#searchModal').exists())
    {

        // when access model shown event is triggered
        $('#searchModal').on('shown', function () {
            $('#modalSearchForm #q').focus();
        });

        $('body').keypress(function(event){

            // console.log(event.which); // used for testing to find keys

            // shift + ~
            if (event.which == 126)
            {
                $('#searchModal').modal('toggle');
            }

            // alt + l
            if (event.which = 188)
            {
                // global.lockSite();
            }
        });
    }

    if ($('#navLoginBtn').exists())
    {
        $('#navLoginBtn').removeAttr('disabled');

        $('#navLoginBtn').click(function(e){
            $(this).attr('disabled', 'disabled');
            window.location = "/intranet/login";
        });
    }

    
    if ($('#navLoggedInBtn').exists())
    {
        $('#navLoggedInBtn').removeAttr('disabled');

        $('#navLoggedInBtn').click(function(e){
            window.location = '/intranet/landing';

            $(this).attr('disabled', 'disabled');
            $(this).find('i').removeClass('fa-user');
            $(this).find('i').addClass('fa-spinner');
            $(this).find('i').addClass('fa-spin');

        });
    }

    global.adjustNavbar();

    // executes function on resizing of window
    $(window).resize(function(){
        global.adjustNavbar();
    });

    $(window).load(function(){

        // re-adjusts nav bar once page is completely loaded
        global.adjustNavbar();

        global.adjustLogo();
    });

	if ($('#createCompBtn').exists())
	{
		$('#createCompBtn').click(function(e){
			$('#createCompModal').modal('show');
		});
	}

    if (global.userid !== undefined && parseInt(global.userid) > 0)
    {
    	global.validateUser(function(d){
    	
		    //global.checkNotifications();
	        
	        global.checkNewMessages();
	        
	        //global.checkSubscription();		    	
    	});
    }
    
    if ($('div.signupcontainer').exists())
    {
        window.onhashchange = navMove;
        $(document).ready(function() {
            if (navPieces.indexOf(location.hash) >= 0)
            {
                navMove();
            }
            else
            {
                location.hash = navPieces[0];
            }
            $('.nextbutton').on('click', function() {
                if (signup.checkSignUpForm())
                {
                    location.hash = navPieces[navPieces.indexOf(location.hash) + 1];
                }
            });
            $('.backbutton').on('click', function() {
                location.hash = navPieces[navPieces.indexOf(location.hash) - 1];
            });
            $('.submitbutton').on('click', function() {
                global.processSignup('#signupform');
            });
        });
    }
};

var navPieces = ['#about', '#newusers', '#payment', '#confirm'];

function navMove()
{
    $('.domainname').text($('#emaildomain').val());
    $('.stepwizard-step button.btn-primary').removeClass('btn-primary').addClass('btn-default');
    var index = navPieces.indexOf(location.hash);
    $('#step-' + (index + 1)).removeClass('btn-default').addClass('btn-primary');
    $('.navbuttons').addClass('hide');
    if (index == 0)
    {
        $('.nextbutton').removeClass('hide');
        $('.backbutton').addClass('hide');
    }
    else if (index !== (navPieces.length - 1))
    {
        $('.nextbutton').removeClass('hide');
        $('.backbutton').removeClass('hide');
    }
    else
    {
        $('.nextbutton').addClass('hide');
        $('.submitbutton').removeClass('hide');
    }
    $('#newemaildomain').text('@' + $('#emaildomain').val());
    var leftAmount = index * 1170;
    $('.inner-nav .container').css('display', 'none');
    $(location.hash).fadeIn('slow');
}

global.processSignup = function (id)
{
    $.post('/signup/createaccount', $(id).serialize(), function(data) {
        window.location = '/msg';
    }, 'json');
};

global.addCommas = function(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\lengthd+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ','             + '$2');
    }

    return x1 + x2;
}




global.trackMouse = function ()
{
	if (global.trackingMouse) return true;
	
	$(document).mousemove(function(event) {
		global.mx = event.pageX;
    	global.my = event.pageY;
    	
    	//t.debug("X: " + t.mx + " Y: " + t.my);
	});
	
	global.trackingMouse = true;
	
	return true;
}

/*
 * renders a site wide alert
 *
 * @param String msg - msg to be displayed
 * @param String type (optional) - type of message to be displayed, default is blank, alternate types: 'alert-success', 'alert-error' or 'alert-info'
 * @param String id (optional) - specifcy custom div ID to display the error
 */

global.renderAlert = function(msg, type, id)
{

    var header = "Alert!";

    if (id == undefined)
    {
        id = "site-alert";

		$('html').velocity("scroll");

        //$("html, body").animate({ scrollTop: 0 }, "slow");
    }

    if (msg == '' || msg == undefined)
    {
        $("#" + id).html('');
        return;
    }

	var $e = $('#' + id);

    if (type == undefined)
    {
        type = 'alert-warning';
    }


	if (type == 'alert-warning') $e.Warning(msg);
	if (type == 'alert-error') $e.Danger(msg);
	if (type == 'alert-danger') $e.Danger(msg);
	if (type == 'alert-info') $e.Info(msg);
	if (type == 'alert-success') $e.Success(msg);


return true;

	/*
    // patch for bootstrap 3
    if (type == 'alert-error') type = 'alert-danger';

    //$("#" + id).html("<div class='ui-widget'><div class='ui-state-error ui-corner-all' style=\"padding: 0 .7em;\"><p><span class='ui-icon ui-icon-alert' style=\"float: left; margin-right: .3em;\"></span><strong>Alert:</strong> "+msg+"</p></div></div>");

    if (type == 'alert-danger') header = "<i class='fa fa-times-circle-o'></i> Error";
    if (type == 'alert-info') header = "<i class='fa fa-exclamation-circle'></i> Information";
    if (type == 'alert-success') header = "<i class='fa fa-thumbs-up'></i> Success";

    $('#' + id).html("<div class='alert " + type + "'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4>" + header + "</h4> " + msg +"</div>");
return true;

	*/

}


global.selectAllToggle = function(divId, sel)
{

    if (sel == undefined)
    {
        sel = true;
    }

    $('#' + divId).find("input").each(function(index, item){
        if ($(item).attr('type') == 'checkbox')
        {
            $(item).prop('checked', sel);
        }
    });
}

/**
 * checks if an email address is valid
 */
global.checkEmail = function(inputvalue)
{
    var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;

    if(pattern.test(inputvalue))
    {
        return true;
    }
    else
    {
        return false;
    }
}

global.ajaxLoader = function(divId, prepend)
{

	$(divId).ajaxLoader();
	/*
	var $loader = $("<div>", { class:'row global-ajax-loader', style: "margin:50px 0;display:none;" });
	
	
    var html = "<div class='well col-lg-2 col-lg-offset-4 col-m-2 col-m-offset-4 col-s-2 col-s-offset-4 col-xs-2 col-xs-offset-4' align='center' style=\"min-width:150px;\">" +
        "<img src='/public/images/loader.gif'> Loading..." +
        "</div>";

	$loader.html(html);
        
	if (prepend !== undefined && prepend == true) $(divId).prepend($loader);
	else $(divId).html($loader);
	
	$loader.velocity('slideDown', { duration:50 });
	return true;
	*/
	
}

global.removeAjaxLoaders = function ()
{
	//$('.global-ajax-loader').remove();
	
	$('.ajax-loader').remove();
}


global.containerResize = function (sel, offset)
{
    if (offset == undefined) offset = 400;

    var h = $(window).height() - offset;

    sel.css('min-height', h + 'px');
}


global.clearRightClick = function()
{
    // clears previous right click if showing
    if ($('#right-click-menu').exists())
    {
        $('#right-click-menu').remove();
    }
}

global.slimScroll = function (div, options)
{

    if (options == undefined) options = { height: '100%' } 

	/*
    $(div).slimScroll({
        height: options.height,
        scrollBy: '10px',
        wheelStep: 1
    });
    */
    
	$(div).perfectScrollbar({
		wheelSpeed: 20,
		wheelPropagation: true,
		minScrollbarLength: 20,
		includePadding: true
	}).css('overflow', 'hidden');	
}

global.noRightClick = function ()
{
    $(document).bind("contextmenu",function(e){ return false; });
}

global.clearRightClick = function ()
{

    // removes right click context menu
    $('body').click(function(e){

        if ($(e.target)[0].tagName != 'A')
        {
            // clears previous right click if showing
            if ($('#right-click-menu').exists())
            {
                $('#right-click-menu').remove();
            }
        }
    });
}

global.setFileDraggable = function()
{
        $('#fileList').selectable({
            filter: "li",
            selected: function (event, ui)
            {

                global.clearRightClick();

                /*
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }
                */

                    $(ui.selected).draggable({
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#fileList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });

}

/**
 * checks if the user has any documents that are required to sign/view
 *
 */
global.checkRequiredDocs = function ()
{
    // checks if they are already on the newhire page before checking

    // var pattern = "/^newhire/";
    
    var pattern = new RegExp("docs");

    var pathname = window.location.pathname;

    // console.log('path: '+ pathname);

    var check = pattern.test(pathname);

    // console.log('pattern check: ' + String(check));

    if (check == false)
    {

        $.getJSON("/docs/checkrequire", function(data){
            if (data.status == 'SUCCESS')
            {
                if (data.msg == 'TRUE')
                {
                    if (confirm("There are documents that you are required to sign. Do you wish to sign them now?"))
                    {

                    }
                    else
                    {
                        // global.
                    }
                    // window.location = '/newhire';
                }
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                return false;
            }
        });

    }
}


global.logout = function ()
{
    $.getJSON("/intranet/logout/1", function(data){

        if (data.status == 'CLOCKIN')
        {
            $('#logoutModal').html(data.html);


			Warning(data.msg);
            //global.renderAlert(data.msg, undefined, 'logoutAlert');


            $('#logoutClockOutBtn').click(function(e){
                global.addTimePunch();

                $(this).attr('disabled', 'disabled');
                $('#logoutAnyBtn').attr('disabled', 'disabled');
                $('#logoutCancelBtn').attr('disabled', 'disabled');
            });

            $('#logoutAnyBtn').click(function(e){
                global.forceLogout();

                $(this).attr('disabled', 'disabled');
            });

            $('#logoutModal').modal('show');
        }
        if (data.status == 'SUCCESS')
        {
            // global.forceLogout();
            //window.location = '/intranet/login?site-alert=' + escape(data.msg) + '&email=' + escape(data.email) + '&company=' + data.company;
            redirect('/intranet/login?site-alert=' + encodeURI(data.msg) + '&email=' + encodeURI(global.email) + '&company=' + global.company);            
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
            return false;
        }
    });
}

/**
 * function used to clock out when logging out
 */

global.addTimePunch = function ()
{
    $.post("/clockinout/addpunch", $('#punchForm').serialize(), function(data){

        $('#punchMsg').html('');

        if (data.status == 'SUCCESS')
        {
            $('#logoutModal').modal('hide');
            $('#logoutModal').html('');

            global.logout();
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'logoutAlert');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'logoutAlert');
            return false;
        }

        // closes modal
        // setTimeout(function(){ clockinout.closePunchModal(); }, 5000);

    }, 'json');

}

global.forceLogout = function ()
{
    window.location = '/intranet/logout/0/1';
}


global.lockSite = function ()
{
    $('#lockModal').modal('show');
}

/**
 * used to make backdrop of modal darker to conceal background
 */
global.modalBackdropLock = function ()
{
    var bd = $('.modal-backdrop');

    bd.addClass('modal-hide');
}


global.checkNotifications = function ()
{
    $.getJSON("/intranet/notifications", function(data){

        if (data.status == 'NONE')
        {
            // no messages, remove classes etc.
            $('#notificationLi').addClass('notification');

            // resets HTML
            $('#notificationLi').html(data.html);

        }
        else if (data.status == 'SUCCESS')
        {
            $('#notificationLi').removeClass('notification');
            $('#notificationLi').html(data.html);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');

            // resets HTML 
            //$('#msgNotLi').html("<a href='javascript:void(0);'><i class='fa fa-exclamation-circle'></i></a>");
        }


        global.adjustNavbar(); // readjusts nav bar to ensure eveyrthing fits ok

        // sets a timer to execute the function again after a set amount of time  (check variable above)
        global.notificationTimeout = setTimeout('global.checkNotifications()', global.notTimeoutSeconds);
    });
}

global.checkNewMessages = function ()
{
    $.getJSON("/msg/notifications", function(data){

        if (data.status == 'NOMSG')
        {
            // no messages, remove classes etc.
            $('#msgNotLi').addClass('notification');

            // resets HTML
            // $('#msgNotLi').html("<a href='javascript:void(0);'><i class='fa fa-envelope'></i></a>");
            $('#msgNotLi').html(data.html);

        }
        else if (data.status == 'SUCCESS')
        {
            $('#msgNotLi').removeClass('notification');
            $('#msgNotLi').html(data.html);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');

            // resets HTML 
            $('#msgNotLi').html("<a href='javascript:void(0);'><i class='fa fa-envelope'></i></a>");
        }


        global.adjustNavbar(); // readjusts nav bar to ensure eveyrthing fits ok

        // sets a timer to execute the function again after a set amount of time  (check variable above)
        // console.log('timer being set for checking new messages'); // unncomment for debugging
    });
}

global.checkSubscription = function (setTimeoutFunc, redirect)
{

    if (setTimeoutFunc == undefined) setTimeoutFunc = true;
    if (redirect == undefined) redirect = true;

    var pathname = window.location.pathname;

    var regex = new RegExp(/\/companysettings/gi);

    var match = regex.test(pathname);

    if (match == false)
    {
        // they are not on the company settings page and must be 
        // re-directed in order to fix their subscirption
        $.getJSON("/intranet/checksubscription", function(data){
            
            // if ($('#BrainTreeSubscriptionID').val(data.status);

            if (data.status == 'EXEMPT')
            {
                // do nothing pretty much
            }
            else if (data.status == 'ALERT')
            {
                if (redirect == true)
                {
                    if (global.admin == true)
                    {
                        window.location = '/companysettings?site-alert=' + escape(data.msg);
                    }
                    else
                    {
                        window.location = '/intranet/login?site-alert=' + escape(data.msg);
                    }
                }
            }
            else
            {
            }

            if(setTimeoutFunc == true) global.subNotificationTimeout = setTimeout('global.checkSubscription()', global.notTimeoutSeconds);
        });
    }
}

global.composeNewMsg = function ()
{
    $.get("/msg/compose/1", function(data){
        //code...
    });
}

global.viewMsg = function (c, msg)
{
	var read = $(c).data('read');
	
	if (read == false)
	{
		var inboxCnt = parseInt($('#inboxCnt').html());
	
		$('#inboxCnt').html((inboxCnt - 1));
	
		if ($('#menuInboxCnt').exists())
		{
			var mic = parseInt($('#menuInboxCnt').html());
			$('#menuInboxCnt').html((mic - 1));
		}
	
	
		$(c).data('read', true);
		$(c).removeClass('unread');
	}
	
	var url = '/msg/preview/' + msg + '?window';
	
	var h = $(window).height();
	var w = $(window).width();
	
	h = (h * 0.8);
	w = (w * 0.8);
		
	msg.composeWindow = window.open(url, '_blank','width=' + w + ',height=' + h + ',toolbar=no, scrollbars=no, menubar=no, status=no, location=no,titlebar=no');

	return true;
}

global.adjustNavbar = function ()
{
    if ($('#main-nav').exists())
    {
        var navWidth = $('#main-nav').outerWidth();

        var container = $('#main-nav').parent().parent().innerWidth();

        var brand = $('#main-nav').parent().parent().find('.navbar-brand').outerWidth();

		var dashIcons = ($('#dash-actions').exists()) ? $('#dash-actions').outerWidth() : 0;

        // console.log($('#main-nav').parent().parent().attr('class'));

        // console.log('navwidth: ' + navWidth);
        // console.log('Brand width: ' + brand);
        // console.log('container Width: ' + container);

        var diff = container - (navWidth + brand + dashIcons);

        $('#q').width(diff - 160);

    }

}

global.adjustLogo = function ()
{
    // first get height of main navbar
    var navH = $('#main-top-nav').height();

    if(!$('#headerLogo').exists())
    {
        // no logo image being displayed (just txt)
        return false;
    }

    var logoH = $('#headerLogo').height();

    // console.log('Navbar height: ' + navH);
    // console.log('Logo height: ' + logoH);

    // now get logo height

    var buffer = (navH - logoH) / 2;

    // console.log("Buffer: "+  buffer);

    $('#headerLogo').css('padding-top', buffer);
}

/**
 * global function used to calculate the final price
 * this will ensure the algorithm is always the same
 */
global.calculatePricing = function (numUsers, moduleTotal)
{

    total = (moduleTotal * numUsers) + 100;

    return total;
}

global.toggleCheckboxBtn = function (b)
{
    if ($(b).hasClass('btn-success'))
    {
        $(b).removeClass('btn-success');
        $(b).addClass('btn-default');
    }
    else
    {
        $(b).removeClass('btn-default');
        $(b).addClass('btn-success');
    }
}

global.viewModKBArticle = function (module)
{
    if (module == undefined || module == 0)
    {
        Warning("Unable to view Knowledge Base Article as module ID is empty!");
        return false;
    }


    if (confirm("Are sure you wish to view the Knowledge Base Article for this module?"))
    {
    	redirect('/kb/view?module=' + encodeURI(module));
    }
}

global.setCursorWaiting = function ()
{
    $('body').css('cursor', 'wait');

    $('body').hover(function(){
        
        $('body').css('cursor', 'wait');

    });
}

/**
 *  Used for select all checkboxes in a table (or div)
 *
 */
global.tblChkboxSelectAllToggle = function (sel, div)
{
    // get table

    $(div).find("input").each(function(index, item){
        if ($(sel).prop('checked') == true)
        {
            // $(item).attr('checked', 'checked');
            $(item).prop('checked', true);
        }
        else
        {
            // $(item).removeAttr('checked');
            $(item).prop('checked', false);
        }
    });

}

global.checkCreateCompForm = function (b)
{
	if ($('#companyName').val() == '')
	{
		global.renderAlert("Please enter the company name!", undefined, 'createCompAlert');
		$('#companyName').focus();
		$('#companyName').effect(global.effect);
		return false;
	}

	if ($('#posName').val() == '')
	{
		global.renderAlert("Please enter the default position name!", undefined, 'createCompAlert');
		$('#posName').focus();
		$('#posName').effect(global.effect);
		return false;
	}

	if ($('#depName').val() == '')
	{
		global.renderAlert("Please enter the default department name!", undefined, 'createCompAlert');
		$('#depName').focus();
		$('#depName').effect(global.effect);
		return false;
	}
	
	$(b).attr('disabled', 'disabled');
	$(b).find('i').removeClass('fa-save');
	$(b).find('i').addClass('fa-spin');
	$(b).find('i').addClass('fa-spinner');
	
	$('#createCompCancelBtn').attr('disabled', 'disabled');	
	
	$.post('/company/create', $('#createCompForm').serialize() , function(data){
		if (data.status == 'SUCCESS')
		{
			global.renderAlert("New company has been created!", 'alert-success');
		}
		else
		{
			global.renderAlert("Unable to create new company!", 'alert-danger');

		}
		
		// reset buttons
		$('#createCompCancelBtn').removeAttr('disabled');
		$(b).removeAttr('disabled', 'disabled');
		$(b).find('i').addClass('fa-save');
		$(b).find('i').removeClass('fa-spin');
		$(b).find('i').removeClass('fa-spinner');
		
		// hides form		
		$('#createCompModal').modal('hide');
				
	}, 'json');
}

global.slimScrollContent = function ()
{
	var maxh = $(window).height();
	
	var navh = $('#main-top-nav').outerHeight();
	
	var footh = $('.footer').outerHeight();
	
	//console.log(navh + footh);
	var h = maxh - (navh + footh + 15);
	
	$('.wrapper').slimScroll({
		height: h + 'px',
		wheelStep: 5
	})
}


global.createDataTable = function (sel, options)
{
	if (!$(sel).exists()) return false;
	
	if (options == undefined) options = {}
	
	var dt = $(sel).dataTable($.extend(true, {}, options, { stateSave: true }));
	
	$('.dataTables_length select').selectpicker({
	    width:100
    });
    
	
	return dt;
	
}

global.composeMsg = function ()
{		
	return global.window('/msg/compose?window');
}

global.window = function (url)
{
	var h = $(window).height();
	var w = $(window).width();
	
	h = (h * 0.8);
	w = (w * 0.8);
		
	return window.open(url, '_blank','width=' + w + ',height=' + h + ',toolbar=no, scrollbars=no, menubar=no, status=no, location=no,titlebar=no');
}

global.allMsgs = function (b)
{
	$(b).disableSpin();
	
	redirect("/msg");
}

global.log = function (msg)
{
	if (global.debug)
	{
		console.log(msg);
	}
}

global.setAutoHide = function ()
{

	
	var lastScrollTop = 0;

	$(window).scroll(function(e){
		   var st = $(this).scrollTop();
		   if (st > lastScrollTop){
		       // downscroll code
		      global.hideNavBar(0);
		   } else {
		      // upscroll code
		      global.showNavBar();
		   }
		   lastScrollTop = st;
	});
	
	global.hideNavBar(1000);
	
}

global.hideNavBar = function (delay, duration)
{
	if (delay == undefined) delay = 1000;
	if (duration == undefined) duration = 600;
	
	var $sel = $('#main-top-nav');
		
	
	if ($sel.height() < 1) return false;
	
	if ($('body').data('autohideRunning') == true) return false;
	

	
	$sel.css('overflow', 'hidden')
		.css('min-height', '0px');
	
	$('body').data('autohideRunning', true);
	
	$('body').velocity({ paddingTop:'0px' }, { delay:delay, duration:duration,
		complete:function ()
		{
			$('body').data('autohideRunning', false);	
		}
	});
	
	$sel.velocity({ height:'0px' }, { delay:delay, duration:duration });

}

global.showNavBar = function ()
{
	if ($('body').data('autohideRunning') == true)
	{
		global.log("Show nav alraedy running!");
		return false;	
	} 

	$('body').data('autohideRunning', true);
	
	var $sel = $('#main-top-nav');
	

	$('body').velocity({ paddingTop:'50px' });
	
	$sel.velocity({ height:'50px' }, {
		complete: function ()
		{
			$('body').data('autohideRunning', false);
			
				$sel.css('overflow', '')
					.css('min-height', '');
		

		}
	});

}

// admin function to rebuild layout CSS
global.rebuildCSS = function ()
{
	$('#rebuildCSSBtn').disableSpin({ spinClass: 'fa fa-spin fa-cog' });
	
	$.getJSON('/layout/rebuildCSS', function(data){
		$('#rebuildCSSBtn').enableSpin();
	
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
		}
		else
		{
			Danger(data.msg);
		}
	});
}

/*
global.backgroundStretch = function ()
{
	if (global.$sel !== undefined)
	{
		global.$sel.resize();
		return true;
	}

	var bg = $('body').data('bg');
	
	if (bg == undefined) return false;

	var hideFooter = $('body').attr('data-hideFooter');

	if (hideFooter == undefined || hideFooter == '0')
	{		
		global.$sel = $('.wrapper').backstretch(bg);
				
		$('.wrapper').resize(function (e){

			
		});


	}
	else
	{
		$.backstretch(bg);
		global.$sel = null;
		//global.bgH = $('body').outerHeight();
	}
	



	return true;
}
*/

/**
*
*/
global.ckeditor = function ($sel, options, ready)
{
	if (options == undefined) options = {};

	var defaults = { width:"auto", height:"auto" };
	
	options = $.extend(true, {}, defaults, options);

	require(['jquery', 'ckeditor-jquery'], function (){
		
		$sel.ckeditor(function(txtarea){
			if (ready !== undefined && typeof ready == 'function') ready(txtarea);
			
		}, options);
		
		
	});

	return true;
}