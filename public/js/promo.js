var promo = {}

promo.indexInit = function ()
{
	global.createDataTable('#promoTbl');
}

promo.edit = function (b, id)
{
	$(b).disableSpin();
	redirect('/promo/edit/' + id);
}

promo.editInit = function ()
{
	$('select').selectpicker();
	
    CKEDITOR.replace('description');

    $('#startDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#endDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#saveBtn').click(function(e){
        promo.checkEditForm();
    });
    
    $('#cancelBtn').cancelButton('/promo');

}

promo.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert("Please enter a promo name!");
        $('#name').effect(globa.effect);
        $('#name').focus();
        return false;
    }

    $('#editForm').submit();
    $('#saveBtn').attr('disabled', 'disabled');
}
