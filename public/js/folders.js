var folders = {}

folders.createUrl;

folders.successCreate = function(){};

folders.build = function ()
{
	$('body').append("<div id='folderPropModal' class='modal fade'></div>");
    $('body').append("<div id='createFolderModal' class='modal fade'></div>");
    
    folders.setCreateUrl();
    
}

folders.successCreate = function (id, folder)
{
	if (confirm("Folder has been successfully created. The page needs to be reloaded. Do you wish to reload now?"))
    {
        // reload the page
        location.reload(true);
    }
}


folders.folderInit = function (sel, namespace)
{
    // console.log('initializing folders');

	/*
    $.rightClickMenu({
        selector: '.folder',
        src: '/folder/rightclick/' + $('#folder').val(),
        selected: function (item, menu, action)
        {
            if (action == 'SETTINGS')
            {
                folders.loadPropertiesModal($(item).attr('value'));
            }

            if (action == 'DELETE')
            {
                if (confirm("Are you for real sure you want to delete this folder?"))
                {
                    folders.deleteFolder($(item).attr('value'));
                }
            }
        }
    });
	*/
	
    $(sel).find("li").each(function(index, item)
    {
        if ($(item).attr('itemType') == '1')
        {

                // if they double click on a folder
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    window.location = '/' + namespace  + '/index/' + $(item).attr('value') + "/" + $('#rootFolder').val();
                });


                // folders are a drop zone
                $(item).droppable({
                    over: function (event, ui)
                    {
                        $(this).addClass('folder-highlight');
                    },
                    out: function (event, ui)
                    {
                        $(this).removeClass('folder-highlight');
                    },
                    drop: function (event, ui)
                    {
                        // console.log('drop');
                        folders.moveSelected(event, ui, $(this).attr('value'));
                        $(this).removeClass('folder-highlight');
                    },
                    helper: function()
                    {

                    }
                });
        }
    });
}

folders.loadPropertiesModal = function (folder)
{
    $.get("/folder/properties/" + folder, function(data){
        $('#folderPropModal').html(data);

        $('#folderPropModal').modal('show');

        folders.viewableModsInit();

    });
}

folders.saveProperties = function (b)
{
    folders.serializeModules();

    if ($('#folderPropForm #name').val() == '')
    {
        global.renderAlert('Please enter a name for the folder!', undefined, 'folderPropAlert');
        return false;
    }

    $(b).attr('disabled', 'disabled');

    $.post("/folder/saveproperties", $('#folderPropForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            $('#folderPropModal').modal('hide');

            // checks if folder named was changed
            // if ($('#currentName').val() !== $('#name').val())
            // {
                // if folder named was changed, promptes them to reload page
                if (confirm("Folder has been successfully updated. The page needs to be reloaded. Do you wish to reload now?"))
                {
                    // reload the page
                    location.reload(true);
                }
            // }
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'folderPropAlert');
            $(b).removeAttr('disabled', 'disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'folderPropAlert');
            $(b).removeAttr('disabled', 'disabled');
            return false;
        }


    }, 'json');

}

folders.createFolder = function ()
{
    $.get("/folder/create", function(data){
    
        $('#createFolderModal').html(data);
        $('#createFolderModal').modal('show');

        if ($('#folder').exists())
        {
            $('#createFolderModal #folder').val($('#folder').val());
        }

    });
}


folders.setCreateUrl = function (url)
{
	if (url == undefined) url = "/folder/createfolder";
	folders.createUrl = url;
};

folders.checkCreateForm = function (b)
{	
    if ($('#createFolderModal #folderName').val() == '')
    {
        global.renderAlert("Please enter a folder name!", undefined, "createFolderModal #folderAlert");
        $('#createFolderModal #folderName').effect(global.effect);
        $('#createFolderModal #folderName').focus();

        return false;
    }

    $(b).attr('disabled', 'disabled');

    $.post(folders.createUrl, $('#createFolderModal #createFolderForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
        	$(window).trigger('foldercraeted', data);
            $('#createFolderModal').modal('hide');
			folders.successCreate(data.id, data.name);
            msg.reRenderTree();
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'createFolderModal #folderAlert');
            $(b).removeAttr('disabled', 'disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'createFolderModal #folderAlert');
            $(b).removeAttr('disabled', 'disabled');
            return false;
        }


    }, 'json');
}

folders.renderFolderTree = function (div, selected, onClickFunction, namespace, includeFiles)
{
    if (selected == undefined) selected = 0;
    if (namespace == undefined) namespace = '';
    if (includeFiles == undefined) includeFiles = 0;
    
    var company = 0;

    if ($('#company').exists()) company = $('#company').val();

    var namespaceUrl = '';

    if (namespace !== '')
    {
        namespaceUrl = "?namespace=" + escape(namespace);
    }


    $.get("/folder/tree/" + selected + '/' + company + '/' + includeFiles + namespaceUrl, function(data){
        $(div).html(data);
        
	$(div).fancytree({
        activate: function(e, o)
        {
			if (onClickFunction !== undefined && typeof onClickFunction == 'function') onClickFunction(o.data.key);
        }
    }).fancytree('getTree');
    

        // global.slimScroll('#free-tree');
    });

}


folders.moveSelected = function (event, ui, folder)
{
    // alert($('#draggingContainer').html());
    $('#draggingContainer').find("li").each(function(index, item){

        // drop function for folders
        if ($(this).attr('itemType') == 1)
        {
            folders.moveFolder(undefined, undefined, folder, $(this).attr('value'));
        }

        // drop function for documents
        if ($(this).attr('itemType') == 2)
        {
            folders.moveItemToFolder(undefined, undefined, folder, $(this).attr('value'), $(this).attr('tbl'), $(this).attr('col'));
        }
    });
}

folders.moveItemToFolder = function (event, ui, folder, itemId, tbl, col)
{
    if (ui == undefined)
    {
        var id = itemId;
    }
    else
    {
        var id = $(ui.draggable).attr('value');
    }

    if (col == undefined) col = '';

    $.post("/folder/addItemToFolder", { id: id, folder: folder, tbl:tbl, col:col, cgi_token:global.CSRF_hash  }, function(data){
        if (data.status == 'SUCCESS')
        {
            if (ui == undefined)
            {
                $("#item_" + itemId).hide();
            }
            else
            {
                $(ui.draggable).hide();
            }
        }
        else if (data.stats == 'ALERT')
        {
            global.renderAlert(data.msg);
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}

folders.moveFolder = function (event, ui, folder, itemId)
{
    if (ui == undefined)
    {
        var id = itemId;
    }
    else
    {
        var id = $(ui.draggable).attr('value');
    }


    $.post("/folder/movefolder", { id: id, folder: folder, cgi_token: global.CSRF_hash }, function(data){
        if (data.status == 'SUCCESS')
        {
            if (ui == undefined)
            {
                $("#folder_" + itemId).hide();
            }
            else
            {
                $(ui.draggable).hide();
            }
        }
        else if (data.stats == 'ALERT')
        {
            global.renderAlert(data.msg);
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
            return false;
        }
    }, 'json');
}

folders.serializeModules = function ()
{
    // clears hidden input div
    $('#modViewInputs').html('');


    $('#tabViewable').find("button").each(function(index, item)
    {
        if ($(item).hasClass('btn-success'))
        {
            var id = $(item).attr('value');

            $('#modViewInputs').append("<input type='hidden' name='module[]' id='module_" + id + "' value='" + id + "'>");
        }
    });
}


folders.viewableModsInit = function ()
{

    $('#modViewInputs').find("input").each(function(index, item)
    {
        var id = $(item).val();

        if ($('#moduleBtn_' + id).exists())
        {
            $('#moduleBtn_' + id).removeClass('btn-default');
            $('#moduleBtn_' + id).addClass('btn-success');
        }
    });
}

folders.deleteFolder = function (id)
{
    $.post("/folder/deletefolder", { folder:id, cgi_token:global.CSRF_hash }, function(data){

        if (data.status == 'SUCCESS')
        {
            $('#createFolderModal').modal('hide');

            global.renderAlert(data.msg, 'alert-success');

            if (confirm("Folder has been successfully deleted. The page needs to be reloaded in order to reflect this change. Do you wish to reload now?"))
            {
                // reload the page
                location.reload(true);
            }
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
            return false;
        }


    }, 'json');
}
