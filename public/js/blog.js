var blog = {}

blog.indexInit = function ()
{

}

blog.editInit = function ()
{
	$('select').selectpicker();
	
    CKEDITOR.replace('body');

    $('#cancelBtn').cancelButton('/blog');

	
	
    $('#publishDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#saveBtn').click(function(e){
        blog.checkEditForm();
    });

    $('#addTagBtn').click(function(e){
        blog.checkTag();
    });


    var onClickFunction = function(folder)
    {
    	//console.log("Category: " + folder);
        $('#category').val(folder);
    }


	folders.renderFolderTree('#folderTree', $('#category').val(), onClickFunction);

    if ($('#id').exists())
    {
        blog.renderTags();
    }
}

blog.viewInit = function ()
{
    CKEDITOR.replace('comment');

    $('#submitCmtBtn').click(function(e){
        blog.leaveComment();
    });

    $('#returnBtn').click(function(e){
		redirect("/blog");


		$(this).disableSpin();
    });

}

blog.checkEditForm = function ()
{

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['body'].getData();
    $('#body').val(str);

    if ($('#title').val() == '')
    {
        global.renderAlert('Please enter a blog title!');
        return false;
    }

    if ($('#publishDate').val() == '')
    {
        global.renderAlert('Please select a publish date!');
        return false;
    }

	$('#saveBtn').attr('disabled', 'disabled');
	$('#blogForm').submit();
	

	/*
    $.post("/blog/save", $('#blogForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {

            $('#saveBtn').attr('disabled', 'disabled');

            window.location = '/blog/index/' + $('#page').val() + '?site-success=' + escape(data.msg);

        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
	*/
}

blog.leaveComment = function (namespace)
{
    if (namespace == undefined) namespace = 'blog';

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['comment'].getData();
    $('#comment').val(str);

    if ($('#comment').val() == '')
    {
        global.renderAlert("Please enter a comment!", undefined, 'commentAlert');
        return false;
    }

    if ($('#userid').val() == '0')
    {
        // user is not logged in, will check name and email

        if($('#name').val() == '')
        {
            global.renderAlert('Please enter your name!', undefined, 'commentAlert');
            $('#name').focus();
            $('#name').effect(global.effect);
            return false;
        }

        if($('#email').val() == '')
        {
            global.renderAlert('Please enter your e-mail address!', undefined, 'commentAlert');
            $('#email').focus();
            $('#email').effect(global.effect);
            return false;
        }
    }

    if (confirm("Are you sure you wish to submit this comment?"))
    {
        $.post("/" + namespace + "/savecomment", $('#commentForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                $('#submitCmtBtn').attr('disabled', 'disabled');

                window.location = '/' + namespace + '/view/' + $('#id').val() + '?site-success=' + escape(data.msg);
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error', 'commentAlert');
                return false;
            }
        }, 'json');
    }

}

blog.checkTag = function ()
{
    if ($('#tag').val() == '')
    {
        global.renderAlert("Please enter a tag!", undefined, 'tagAlert');
        $('#tag').effect(global.effect);
        $('#tag').focus();
        return false;
    }

    $('#tagInputContainer').append("<input type='hidden' name='tag[]' id='tag' value=\"" + $('#tag').val() + "\">");

    $('#tag').val('');
    $('#tag').effect(global.effect);
    $('#tag').focus();

    blog.renderTags();
}

blog.renderTags = function ()
{
    var html = '';


    $('#tagInputContainer').find("input").each(function(index, item){

        var str = String($(item).attr('value'));

        var tag = str.replace("'", "\\\'");

        html += "<label class='label label-success' onclick=\"blog.removeTag(this, '" + tag + "');\">" + $(item).attr('value') + "</label>";
    });
    
    $('#tagDisplay').html(html);
}

blog.removeTag = function (tag, txt)
{
    $(tag).hide(global.hideEffect);

    var removed = false;

    $('#tagInputContainer').find("input").each(function(index, item){
        if  ($(item).val() == txt)
        {
            $(item).remove();
            removed = true;
            return true;
        }
    });


    if (removed == true) return true;

    return false;
}

blog.deleteComment = function (id)
{
    if (confirm("Are you sure you wish to delete this comment?"))
    {
        $.post("/blog/deletecomment", { comment:id, cgi_token:global.CSRF_hash }, function(data){

            if (data.status == 'SUCCESS')
            {
                window.location = '/blog/view/' + $('#id').val() + '?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                return false;
            }
        }, 'json');
    }
}

blog.setSpamComment = function (id)
{
    if (confirm("Are you sure you wish to set this comment as spam  (this will remove it from the site)?"))
    {
        $.post("/blog/setspamcomment", { comment:id, cgi_token:global.CSRF_hash }, function(data){

            if (data.status == 'SUCCESS')
            {
                window.location = '/blog/view/' + $('#id').val() + '?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                return false;
            }
        }, 'json');
    }
}

blog.readMore = function (b)
{
	redirect('/blog/view/' + $(b).attr('articleID'));
    //window.location = '/blog/view/' + $(b).attr('articleID');
    blog.setReadMoreBtnDisabled($(b).attr('articleID'));
}

blog.setReadMoreBtnDisabled = function (articleID)
{
	$('#readMoreBtn' + String(articleID)).disableSpin();
	return true;
}

blog.editBlog = function (b)
{
	$(b).disableSpin();
	
    redirect('/blog/edit/' + $(b).attr('blogID') + '/0/' + $('#page').val());
    
    return true;
}
