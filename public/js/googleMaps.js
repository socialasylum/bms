var gm = {}

gm.map;
gm.markers = [];
gm.circles = [];


gm.initialize = function (mapDivID, lat, lng, zoom, allowAddPoint)
{
    if (mapDivID == undefined) mapDivID = 'map';
    
    if (lat == undefined || lat == 0) lat = 38.18719164156577;
    if (lng == undefined || lng == 0) lng = -452.900921875;
    if (zoom == undefined || zoom == 0) zoom = 4;

    if (allowAddPoint == undefined) allowAddPoint = true;

    // console.log("map zoom: " + zoom);


    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: parseInt(zoom),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };

    gm.map = new google.maps.Map(document.getElementById(mapDivID),mapOptions);

    if (allowAddPoint == true)
    {
        google.maps.event.addListener(gm.map, 'click', function(e){
            gm.addPoint(e.latLng);
        });
    }

	$(window).trigger('gmaps.api.loaded');

    return gm.map;
}

gm.initAsync = function ()
{
    var mapOptions = {
        center: new google.maps.LatLng(38.18719164156577, -452.900921875),
        zoom: parseInt(4),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };


    gm.map = new google.maps.Map(document.getElementById('map'), mapOptions);



	$(window).trigger('gmaps.api.loaded');

    return gm.map;
}

gm.postInit = function (allowAddPoint)
{
    if (allowAddPoint == undefined) allowAddPoint = true;
    
    if (allowAddPoint == true)
    {
        google.maps.event.addListener(gm.map, 'click', function(e){
            gm.addPoint(e.latLng);
		});
    }
        
	return true;
}


gm.addPoint = function (location, includeCircle, radius, draggable, editable, circleColor, opacity)
{
    if (includeCircle == undefined) includeCircle = true;

    if (radius == undefined) radius = 25000;

    if (draggable == undefined) draggable = true;
    if (editable == undefined) editable = true;

    if (circleColor == undefined || circleColor == '') circleColor = 'red';
    if (opacity == undefined || opacity == '') opacity = 0.8;

    var marker = new google.maps.Marker({
        position:location,
        draggable:draggable,
        animation: google.maps.Animation.DROP,
        map:gm.map
    });

    if (includeCircle == true)
    {

        // var circleColor = "red";
        // var opacity = 0.8;

        // checks for color
        if ($('#googleMapCircleColor').exists())
        {
            circleColor = '#' + $('#googleMapCircleColor').attr('hex');
        }

        // checks for custom opacity setting
        if ($('#googleMapCircleOpacity').exists())
        {
            opacity = parseInt($('#googleMapCircleOpacity').val())  / 100;
        }


        var circle = new google.maps.Circle({
            center: location,
            map:gm.map,
            radius:parseInt(radius),
            strokeColor:circleColor,
            // strokeColor:"red",
            strokeOpacity:opacity,
            strokeWeight:2,
            // fillColor:"red",
            fillColor:circleColor,
            editable:editable
        });

        circle.bindTo('center', marker, 'position');

    }

    if (editable == true)
    {
        // only if editable will they be able to remove markers
        google.maps.event.addListener(marker, 'rightclick', function(e){
            this.setMap(null);
            this.setVisible(false);

            if (includeCircle == true)
            {
                circle.setMap(null);
            }
        });
    }
    gm.markers.push(marker);

    if (includeCircle == true)
    {
        gm.circles.push(circle);
    }
}


gm.loadSavedMarkers = function (includeCircle, draggable, editable, div)
{
    if (div == undefined) div = '#savedMarkers';

    if ($(div).exists())
    {
        $(div).find("input").each(function(index, item)
        {
            // console.log('rad: ' + $(item).attr('radius'));
            gm.addPoint(new google.maps.LatLng($(item).attr('lat'),$(item).attr('lng')), includeCircle, $(item).attr('radius'), draggable, editable, $(item).attr('color'), $(item).attr('opacity'));
        });
    }
}

