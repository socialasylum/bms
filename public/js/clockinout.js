var clockinout = {}


clockinout.indexInit = function ()
{
    clockinout.loadHistory();

    $('#addPunchBtn').click(function(e){
        clockinout.addTimePunch();
    });
}

clockinout.addTimePunch = function ()
{
    $('#punchModal').modal('show');

    $.post("/clockinout/addpunch", $('#punchForm').serialize(), function(data){

        $('#punchMsg').html('');

        if (data.status == 'SUCCESS')
        {
            $('#punchMsg').html("Time punch added: " + data.msg);
	
			$('body').velocity("scroll");
        }
        else if (data.status == 'ALERT')
        {
        	Warning(data.msg);
            return false;
        }
        else
        {
        	Danger(data.msg);
            return false;
        }

        // closes modal
        setTimeout(function(){ clockinout.closePunchModal(); }, 5000);

        clockinout.loadHistory();

    }, 'json');
}

clockinout.closePunchModal = function ()
{
    $('#punchModal').modal('hide');
}

clockinout.loadHistory = function ()
{
    $.get("/clockinout/history", function(data){
        $('#historyDisplay').html(data);
        
        global.createDataTable('#punchTbl');
    });


}
