var formbuilder = {}

formbuilder.indexInit = function ()
{
    // global.noRightClick();
    // $(document).bind("contextmenu",function(e){ return false; });

    global.setFileDraggable(false);

    folders.folderInit('#form-container', 'formbuilder');

    // goes through each span1 div in doc-container and makes draggable/droppable
    $('#form-container').find("li").each(function(index, item)
    {

            // documents
            if ($(item).attr('itemType') == '2')
            {
                // if they double click on a document
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    // window.location = "/docs/view/" + $(item).attr('value') + "?folder=" + $('#folder').val() + "&menuId=" + $('#menuId').val();
                    window.location = "/formbuilder/view/" + $(item).attr('value') + '/' + $('#folder').val() + '/' + $('#rootFolder').val();
                });

                // var id = $(item).attr('value');

                // when the user right clicks on the screen
                /*
                $(item).mousedown(function(e){
                    switch (e.which)
                    {
                        case 3:
                        formbuilder.indexRightClick(e, $(item), id);
                        break;
                    }
                });
                */
            }

    });



    $.rightClickMenu({
        selector: '.form',
        src: '/formbuilder/rightclick/' + $(this).attr('value'),
        selected: function (item, menu, action)
        {
            if (action == 'EDIT')
            {
                window.location = '/formbuilder/edit/' + $(item).attr('value') + '/' + $('#folder').val();
            }

            if (action == 'DELETE')
            {
                // alert('delete!');
            }
            // alert($(item).attr('itemID'));
            // intranet.loadWidget($(item).attr('url'), $(item).attr('id') ,$(port).val());
        }
    });


    $('#createFormBtn').click(function(e){
        // formbuilder.checkNewFormForm();
    });

    $('#newFolderBtn').click(function(e){
        folders.createFolder();
    });


    if ($('#submittedFormsTbl').exists())
    {
        $('#submittedFormsTbl').dataTable();
    }
}

formbuilder.editInit = function ()
{
    // global.noRightClick();
    $(document).bind("contextmenu",function(e){ return false; });

    global.renderAlert('This form does not require any approval', 'alert-info', 'approval-display');

    $('#saveBtn').click(function(e){
        formbuilder.saveForm();
    });

    $('#approvals').change(function(){

        // clears current display
        $('#approval-display').html('');
        // global.ajaxLoader('#approval-display');


        // sets hidden count back to 0
        $('#applvl').val('0');
        $('input[name=applvl]').val('0');


        var lvls = parseInt($('#approvals :selected').val());
    
        formbuilder.loadApprovalLevel(lvls);
        });

    // This property tells CKEditor to not activate every element with contenteditable=true element.
    CKEDITOR.disableAutoInline = true;

    CKEDITOR.replace('instructions',{
        toolbar:
        [
            [ 'Source' ],
            { name: 'basicstyles', items: [ 'Bold', 'Italic'] }
        ]
    });

    formbuilder.createDropZone();

    formbuilder.makeElementsDraggable();
    formbuilder.renderDynamicForm();

    if ($('#approvals :selected').val() > 0)
    {
        // form does have approval levels set

        formbuilder.loadApprovalLevel($('#approvals :selected').val());

    }



    // CKEDITOR.inline('formTitle');
}

formbuilder.checkNewFormForm = function ()
{
    if ($('#formTitle').val() == '')
    {
        global.renderAlert("Please enter a form name!", undefined, 'formAlert');
        return false;
    }

    $('#createFormBtn').attr('disabled', 'disabled');

    window.location = "/formbuilder/edit?title=" + escape($('#formName').val());
}

formbuilder.loadApprovalLevel = function (lvl)
{

    var url = '';

    if ($('#id').exists() && $('#id').val() > 0)
    {
        url = '/' + $('#id').val();
    }

    $.get("/formbuilder/addapplvl/" + String(lvl) + url, function(data){
        $('#approval-display').html(data);

        $('#applvl').val(lvl);
        $('input[name=applvl]').val(lvl);
    });
}

formbuilder.saveForm = function ()
{

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['instructions'].getData();
    $('#instructions').val(str);

    $.post("/formbuilder/save", $('#formForm').serialize() + "&" + $.param({ 'render': $('#formHtmlContainer').html(), 'title': $('#formTitle').text(), 'briefIns': $('#bInst').html() }), function(data){

        if (data.status == 'SUCCESS')
        {
            window.location = '/formbuilder?site-success=' + escape("Form has been saved!");
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}

formbuilder.removeElement = function (btn, item)
{
    if (confirm("Are you sure you wish to remove this element?"))
    {
        $(btn).attr('disabled', 'disabled');

        formbuilder.closeRightClick();

        $(item).parent().hide('fade', undefined, 400, function(){
            $(this).html('');

            formbuilder.saveDynamicForm();
        });
    }
}

formbuilder.checkNewFolderForm = function()
{
    if ($('#folderName').val() == '')
    {
        global.renderAlert("Please enter a folder name!", undefined, 'folderAlert');
        return false;
    }


    $.post("/formbuilder/createfolder", $('#createFolderForm').serialize(), function(data){

        var folder = '';

        if (data.status == 'SUCCESS')
        {
            // clears alert
            global.renderAlert(undefined, undefined, 'folderAlert');

            $('#createFolderModal').modal('hide');
            $('#folderName').val('');

            if ($('#folder').val() != '')
            {
                folder = '/' + $('#folder').val();
            }


            window.location = "/formbuilder/index" + folder;
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'folderAlert');
            $('#createFolderBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'folderAlert');
            $('#createFolderBtn').removeAttr('disabled');
            return false;
        }

    }, 'json');
}

formbuilder.indexRightClick = function(e, item, id)
{
    global.clearRightClick();

    var html = "<div class='dropdown'>" +
        "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"formbuilder.editForm(" + id + ");\"><i class='fa fa-pencil'></i> Edit</a></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"window.open('/formbuilder/preview/" + id + "', '_blank');\"><i class='fa fa-print'></i> Print</a></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.getProperties(" + id + ");\"><i class='fa fa-cog'></i> Properties</a></li>" +
        "<li class='divider'></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"formbuilder.deleteForm(" + id + ");\"><i class='fa fa-trash-o'></i> Delete</a></li>" +
        "</ul>" +
        "</div>";


    // right click menu for folders
    if (item.attr('itemType') == 1)
    {
    var html = "<div class='dropdown'>" +
        "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"formbuilder.deleteForm(" + id + ");\"><i class='fa fa-trash'></i> Delete</a></li>" +
        "</ul>" +
        "</div>";


    }

    var parentOffset = item.parent().offset();
    //or $(this).offset(); if you really just want the current element's offset
    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;

    $('body').append("<div id='right-click-menu' style=\"top:" + String(e.pageY)  + "px;left:" + String(e.pageX) + "px;\">" + html + "</div>");
}

formbuilder.editForm = function (id)
{
     window.location = "/formbuilder/edit/" + id + "?folder=" + $('#folder').val();
}


formbuilder.viewInit = function ()
{
    $('#saveCustFormBtn').click(function(e){
        formbuilder.submitForm();
    });

    $('#resetBtn').click(function(e){
        $('#formbuilderForm').trigger('reset');
    });
    
}

formbuilder.viewsubmittedInit = function ()
{
    formbuilder.populateForm();
    
    $('#cancelBtn').cancelButton('/formbuilder');
}

formbuilder.completeInit = function ()
{
    $('#returnBtn').click(function(e){
        $(this).attr('disabled', 'disabled');
        window.location = '/formbuilder';
    });
}

formbuilder.submitForm = function ()
{
    $.post("/formbuilder/saveformpost", $('#formbuilderForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/formbuilder/complete/' + $('#id').val();
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}


formbuilder.createDropZone = function()
{
    $('#formDropZone').droppable({
        over: function (event, ui)
        {
            $(this).addClass('folder-highlight');
        },
        out: function (event, ui)
        {
            $(this).removeClass('folder-highlight');
        },
        drop: function (event, ui)
        {

            $(this).removeClass('folder-highlight');

            formbuilder.dropElements();
        }
    });
}

formbuilder.makeElementsDraggable = function (clearPreviousSortable)
{
    if (clearPreviousSortable == undefined) clearPreviousSortable = false; // deafults to false

    if (clearPreviousSortable == true)
    {
        // removes previous functionality
        // $('#form-elements').sortable('destroy');
    }

        $('#elementList').selectable({
            filter: "li",
            selected: function (event, ui)
            {
                /*
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }
                */
                    $(ui.selected).draggable({
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#elementList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });
}


formbuilder.dropElements = function ()
{

    var html = '';

    $('#draggingContainer').find("li").each(function(index, item){
        html += "<li>" + $(item).html() + "</li>";
    });

    formbuilder.addElement(html);
}

formbuilder.addElement = function (html)
{
    $.post("/formbuilder/addelement", { html: html, cgi_token: global.CSRF_hash  }, function(data){
        if (data.status == 'SUCCESS')
        {
            formbuilder.renderDynamicForm();
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}


/**
 * saves the HTML as is during the session
 */
formbuilder.saveDynamicForm = function ()
{
    $.post("/formbuilder/updatedynform", { html: $('#formHtmlContainer').html(), cgi_token:global.CSRF_hash }, function(data){
        if (data.status == 'SUCCESS')
        {
            // do nothing
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false
        }
    }, 'json');


}


formbuilder.renderDynamicForm = function ()
{
    $.get("/formbuilder/renderdynamic", function(data){
        $('#form-content').html(data);

        formbuilder.rightClickFormElements();

        formbuilder.setFormElementsDraggable();

        /*
        $('#form-content').find('div').each(function(index, item){
            if ($(item).attr('class') == 'form-group')
            {
                // console.log($(item).attr('class'));
                $(item).attr('onclick', "formbuilder.showProperties(this);");
            }
        });
        */
    });
}

formbuilder.rightClickFormElements = function ()
{

    $('#formHtmlContainer').find("div").each(function(index, item)
    {
        // console.log('CLASS: ' + $(item).attr('class'));

        if ($(item).attr('class') == 'form-group')
        {
            // when the user right clicks on 1 of the form elements  elements
            $(item).mousedown(function(e){
                switch (e.which)
                {
                    case 3:
                    formbuilder.editRightClick(e, $(item));
                    break;
                }
            });
        }

    });
}


formbuilder.editRightClick = function(e, item)
{

    var parentOffset = item.parent().offset();
    //or $(this).offset(); if you really just want the current element's offset
    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;

    $('#formPropPanel').css('top', (e.pageY - 220));
    $('#formPropPanel').css('left', e.pageX);
    // $('#formPropPanel').show('highlight');
    $('#formPropPanel').show();


    global.ajaxLoader('#formPropPanel');


    $.get("/formbuilder/getelproperties/" + $(item).attr('options'), function(data){
        $('#formPropPanel').html(data);

        formbuilder.populateProperties(item);

    
        $('#formPropPanel #saveBtn').click(function(e){
            formbuilder.applyProperties(item);
        });

        $('#formPropPanel #delBtn').click(function(e){
            formbuilder.removeElement(this, item);
        });

        $('#formPropPanel #codeGroup').change(function(e){
            formbuilder.loadCodePreview($(this).val());
        });


        formbuilder.customOptionsSortable();

        $('#formPropPanel #addCustomBtn').click(function(e){
            formbuilder.addCustomerOption();
        });
    });

}


formbuilder.populateProperties = function (item)
{
    // alert($(item).html()); // alert for troubleshooting

    var lbl = $(item).find('label').text();
    var name = $(item).find('input').attr('name');
    var id = $(item).find('input').attr('id');
    var ph = $(item).find('input').attr('placeholder');
    var cl = $(item).find('input').attr('class');

    // checks for textarea attritbutes
    if (name == undefined) name = $(item).find('textarea').attr('name');
    if (id == undefined) id = $(item).find('textarea').attr('id');
    if (cl == undefined) cl = $(item).find('textarea').attr('class');

    // will now check for select attritbutes
    if (name == undefined) name = $(item).find('select').attr('name');
    if (id == undefined) id = $(item).find('select').attr('id');
    if (cl == undefined) cl = $(item).find('select').attr('class');


    $('#formPropPanel #lbl').val(lbl);
    $('#formPropPanel #name').val(name);
    $('#formPropPanel #id').val(id);
    $('#formPropPanel #placeholder').val(ph);
    $('#formPropPanel #class').val(cl);

    // gets option type for inputs
    if ($('optionType').exists() && $('#formPropPanel #optionType').exists())
    {
        $('#formPropPanel #optionType').val($('#optionType').val());
    }
}


formbuilder.applyProperties = function (item)
{
    var lbl = $('#formPropPanel #lbl').val();
    var name = $('#formPropPanel #name').val();
    var id = $('#formPropPanel #id').val();
    var ph = $('#formPropPanel #placeholder').val();
    var cl = $('#formPropPanel #class').val();


    $(item).find('label').text(lbl);
    $(item).find('input').attr('name', name);
    $(item).find('input').attr('id', id);
    $(item).find('input').attr('placeholder', ph);
    $(item).find('input').attr('class', cl);

    // updates textarea's
    $(item).find('textarea').attr('name', name);
    $(item).find('textarea').attr('id', id);
    $(item).find('textarea').attr('class', cl);

    // updates select's
    $(item).find('select').attr('name', name);
    $(item).find('select').attr('id', id);
    $(item).find('select').attr('class', cl);

    // console.log('#formPropPanel #optionType: ' + $('#formPropPanel #optionType').exists());
    // console.log('$(item).find(\'#optionType\'): ' + $(item).parent().parent().find('#optionType').exists());

    // sets option type
    if ($('#formPropPanel #optionType').exists() == true && $(item).parent().parent().find('#optionType').exists() == true)
    {
        $(item).find('#optionType').val($('#formPropPanel #optionType').val());
        formbuilder.applyOptions(item);
    }

    formbuilder.closeRightClick();

    formbuilder.saveDynamicForm();
}

/**
 * applys the options type 
 */
formbuilder.applyOptions = function (item)
{
    var optionType = $(item).parent().parent().find('#optionType').val();

    if (optionType == 1) // they are using codes
    {

    }
    else if (optionType == 2) // using a dataset
    {

    }
    else if (optionType == 3) // custom key->value set
    {
        var selectOptions = '';
        var cnt = 0;

        $('#optionsSortList').find("input").each(function(index, item)
        {
            if ((cnt % 2) == 0)
            {
                selectOptions += "<option value='" + $(item).val() + "'>";
            }
            else
            {
                selectOptions += $(item).val() + "</option>";
            }
            console.log('text (' + $(item).attr('id')+ ')' + $(item).val());

            cnt++;
        });
    }

    console.log("select Options: " + selectOptions);

    // console.log(optionType);
}


formbuilder.addCustomOption = function ()
{

}

formbuilder.closeRightClick = function ()
{
    $('#formPropPanel').hide('fade', undefined, 400, function(){
        $('#formPropPanel').html('');
    });

}

formbuilder.clearFormHtml = function ()
{
    if (confirm("Are you sure you wish to clear this entire form?"))
    {
        $.get("/formbuilder/clearformhtml", function(data){
            formbuilder.renderDynamicForm();
        });
    }
}

formbuilder.showProperties = function (el)
{
    console.log($(el).attr('class'));
}


formbuilder.setFormElementsDraggable = function ()
{
    $('#formSort').sortable({
        stop: function (event, ui)
        {
            formbuilder.saveDynamicForm();
        }
    });
}

formbuilder.cancelForm = function ()
{
    if (confirm("Are you sure you wish to cancel?\n\nAll changes will be lost!"))
    {
        window.location = '/formbuilder';
    }
}

formbuilder.deleteForm = function (id)
{
    if (confirm("Are you sure you wish to delete this from?"))
    {
        $.post("/formbuilder/delete", { id: id, cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/formbuilder?site-success=' + escape(data.msg);
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
        }, 'json');
    }
}


formbuilder.setOptionType = function (type)
{
    $('#optionType').val(type);
}


formbuilder.customOptionsSortable = function ()
{
    $('#optionsSortList').sortable({
        stop: function (event, ui)
        {
            formbuilder.updateDelBtn('#optionsSortList');
        }
    });
}

formbuilder.addCustomerOption = function ()
{
    var html = '';

    html = $('#optionsSortList').find('li').html();

    $('#optionsSortList').append('<li>' + html + '</li>');

    formbuilder.updateDelBtn('#optionsSortList');
}

formbuilder.removeCustomOption = function (b)
{
    var item = $(b).parent().parent().parent();

    item.hide(global.hideEffect);
}


formbuilder.updateDelBtn = function (div)
{

    $(div).find("button").each(function(index, item){

        if (index == 0)
        {
            $(item).attr('disabled', 'disabled');
        }
        else
        {
            $(item).removeAttr('disabled');
        }

    });
}

formbuilder.viewSubmittedForm = function (id)
{
    window.location = '/formbuilder/viewsubmitted/' + id+ '/' + $('#folder').val();
}

formbuilder.populateForm = function ()
{
    // get raw json encoded post data
    var post = $('#formPostData').text();

    // console.log(post);

    // will nwo parse json data
    var obj = jQuery.parseJSON(post);


    $.each(obj, function(key, val){

        // console.log(key + ' : '  + val);

        if ($('#' + key).exists())
        {
            $('#' + key).val(val);
        }

    });


}

formbuilder.approveLvl = function (form, lvl, final)
{
    if (confirm("Are you sure you wish to approve this?"))
    {
        formbuilder.setSignLvlApproval(form, lvl, 1, final);
    }
}

formbuilder.denylvl = function (form, lvl, final)
{
    if (confirm("Are you sure you wish to deny this?"))
    {
        formbuilder.setSignLvlApproval(form, lvl, 0, final);
    }
}

formbuilder.setSignLvlApproval = function (form, lvl, decision, final)
{
    $.post("/formbuilder/setsignlvl", { id: form, lvl: lvl, decision: decision, final: final, cgi_token:global.CSRF_hash }, function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/formbuilder/viewsubmitted/' + form + '/' + $('#folder').val() + '?site-success=' + escape(data.msg);
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}

formbuilder.loadCodePreview = function (group)
{
    global.ajaxLoader('#codeGroupPreview');

    $.get("/formbuilder/codegrouppreview/" + group, function(data){
        $('#codeGroupPreview').html(data);
    });
}
