(function($){
    $.rightClickMenu = function (options)
    {

        var showing = false;

        var html = '';

        // var el = $(this);

        // gets HTML for menu
        if (options.src !== undefined)
        {
            $.get(options.src, function(data){
                html = data;
            });
        }

        $('body').mousedown(function(e){
    
            if (e.which == '1')
            {
                // clears previous right click if showing
                if (showing == true)
                {
                    // alert('clear it');
                    $('#right-click-menu').html('');
                    showing = false;
                }
            }
        });


        // when the user right clicks on the screen
        $(options.selector).mousedown(function(e){

            // console.log(e.which);
            switch (e.which)
            {
                case 3:

                    $(document).bind("contextmenu",function(e){ return false; });


                    if (!$('#right-click-menu').exists())
                    {
                        $('body').append("<div id='right-click-menu'></div>");
                    }

                    $('#right-click-menu').css('top', String(e.pageY) + 'px');
                    $('#right-click-menu').css('left', String(e.pageX) + 'px');

                    $('#right-click-menu').html(html);

                    showing = true;


                    // $('#right-click-menu').show();

                    var h = $('#right-click-menu').find('.submenu').parent().hover(function(e){
                        $(this).find('.submenu').css('visibility', 'visible');
                    },
                        function ()
                        {
                            $(this).find('.submenu').css('visibility', 'hidden');
                        }
                    );

                    var el = this;


                    // var portlet = $(options.selector).val();


                    $('#right-click-menu').find('li').mousedown(function(e){
                        switch (e.which)
                        {
                            case 1:

                                // alert($(this).attr('action'));
                                options.selected(el, this, $(this).attr('action'));
                                $('#right-click-menu').html(''); // clears HTML
                            break;
                        }
                    });
                break;
            }
        });

    }
})(jQuery);



