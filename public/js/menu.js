var menu = {}

menu.indexInit = function ()
{
    // menu.loadCompanyPanes();


    var id = $('#company').val();

    $('#createMenuHeaderBtn').click(function(e){
        menu.createMenuItem();
    });


    // when access model hide event is triggered
    $('#accessModal').on('hidden.bs.modal', function () {
        menu.savePositionAccess();
    });


    // when permission model hide event is triggered
    $('#modPermModal').on('hidden.bs.modal', function () {
        menu.saveModulePermissionAccess();
    });


    // makes modules reservoir draggable
    menu.setModulesDraggable();

    menu.createDropZone(id, $('.module-drop-zone'));

        // click for create page button
        $('#menuPageBtn').click(function(e){
            $('#createMenuPageModal').modal('toggle');
        });

        // click function for creating a new header
        $('#menuHeaderBtn').click(function(e){

            $('#menuHeaderForm #name').val('');

            $('#createMenuHeaderModal').modal('toggle');
            $('#menuHeaderForm #id').val($(this).attr('value'));
            $('#menuHeaderForm input[name=id]').val($(this).attr('value'));

            $('#menuHeaderForm #createMenuHeaderBtn').click(function(e){
                menu.createMenuItem();
            });

        });


        // makes header wells sortable
        $('#headers-container #menu-li').sortable({
            stop: function (event, ui)
            {
                menu.saveHeaderSort(event, ui, id);
            }
        });


		$("[data-toggle='tooltip']").tooltip();

		//$("[data-toggle='tooltip']:first-child").tooltip('open');

	return true;
}

menu.loadCompanyPanes = function ()
{
    $('.tab-content').find(".tab-pane").each(function(index, item)
    {
        menu.loadMenuContent($(item).attr('value'));
    });
}

menu.loadMenuContent = function (id)
{
    global.ajaxLoader('#tab' + id + '-display');

    $.get("/menu/menuContent/" + id, function(data){
        $('#tab' + id + '-display').html(data);

        // makes header wells sortable
        $('#tab' + id + '-display #headers-container #menu-li').sortable({
            stop: function (event, ui)
            {
                menu.saveHeaderSort(event, ui, id);
            }
        });

        // makes modules reservoir draggable
        menu.setModulesDraggable();

        menu.createDropZone(id, $('#tab' + id + '-display #headers-container .menu-header .module-drop-zone'));

        // click for create link button
        $('#tab' + id + '-display #headers-container .menu-header #menuLinkBtn').click(function(e){

            $('#menuLinkForm #name').val('');
            $('#menuLinkForm #link').val('');

            $('#createMenuLinkModal').modal('toggle');

            $('#menuLinkForm #id').val(id);
            $('#menuLinkForm input[name=id]').val(id);

            $('#menuLinkForm #parentMenu').val($(this).attr('parentmenu'));
            $('#menuLinkForm input[name=parentMenu]').val($(this).attr('parentmenu'));

            $('#createMenuLinkModal #createMenuLinkBtn').click(function(e){
                menu.createMenuLink();
            });
        });

        // click for create page button
        $('#tab' + id + '-display #headers-container .menu-header #menuPageBtn').click(function(e){
            $('#createMenuPageModal').modal('toggle');
        });

        // click function for creating a new header
        $('#tab' + id + '-display #menuHeaderBtn').click(function(e){

            $('#menuHeaderForm #name').val('');

            $('#createMenuHeaderModal').modal('toggle');
            $('#menuHeaderForm #id').val($(this).attr('value'));
            $('#menuHeaderForm input[name=id]').val($(this).attr('value'));

            $('#menuHeaderForm #createMenuHeaderBtn').click(function(e){
                menu.createMenuItem();
            });

        });

    });
}

menu.createMenuItem = function ()
{
    if ($('#menuHeaderForm #name').val() == '')
    {
        global.renderAlert('Please enter the header name!', undefined, 'menuHeaderAlert');
        $('#menuHeaderForm #name').focus();
        return false;
    }

    $.post("/menu/createMenuItem", $('#menuHeaderForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                // menu.loadMenuContent($('#menuHeaderForm #id').val());
                $('#createMenuHeaderModal').modal('hide');
                window.location = '/menu';
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'menuHeaderAlert');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error', 'menuHeaderAlert');
            }
    }, 'json');
}


/**
 * makes modules in rservoir draggable
 */
menu.setModulesDraggable = function(clearPreviousSortable)
{
        if (clearPreviousSortable == undefined) clearPreviousSortable = false; // deafults to false

        if (clearPreviousSortable == true)
        {
            // removes previous functionality
            $('#fileList').sortable('destroy');
        }

        // goes through each li and for folders makes it so they are not disbled
        $('#fileList').find("li").each(function(index, item)
        {
            // console.log('test: ' + $(this).attr('id'));
            // folders
            if ($(item).attr('itemType') == '1')
            {
                $(item).removeClass('ui-state-disabled');
            }
        });

        $('#fileList').selectable({
            filter: "li",
            selected: function (event, ui)
            {
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }

                    $(ui.selected).draggable({
                    scroll:true,
                    scrollSensitivity: 100,
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#fileList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });
}

menu.createDropZone = function(company, sel)
{
    sel.droppable({
        over: function (event, ui)
        {
            $(this).addClass('module-drop-zone-hover');
        },
        out: function (event, ui)
        {
            $(this).removeClass('module-drop-zone-hover');
        },
        drop: function (event, ui)
        {
            menu.moveSelected(event, ui, $(this).attr('value'), company, $(this).attr('value'));

        }
    });
}


menu.moveSelected = function (event, ui, folder, company, parentMenu)
{
    $('#draggingContainer').find("li").each(function(index, item){

        menu.addModuleToMenu(company, $(this).attr('value'), parentMenu);

    });
}

menu.addModuleToMenu = function (company, module, parentMenu)
{
    $.post("/menu/createMenuItem", { id: company, module: module, parentMenu: parentMenu,  cgi_token: global.CSRF_hash   }, function(data){
            if (data.status == 'SUCCESS')
            {

                window.location = '/menu';
                // $('#tab' + company + '-display #mod' + company).hide();
                // menu.loadMenuContent(company);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');

}

menu.saveHeaderSort = function (event, ui, id)
{
    var sortArray = $('#headers-container #menu-li').sortable('toArray');

    $.post("/menu/saveHeaderOrder", { company: id, order: sortArray, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                // do nothing
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');
}

menu.createMenuLink = function ()
{
    if ($('#menuLinkForm #name').val() == '')
    {
        global.renderAlert('Please enter a link name!', undefined, 'menuLinkAlert');
        $('#menuLinkForm #name').focus();
        return false;
    }

    if ($('#menuLinkForm #url').val() == '')
    {
        global.renderAlert('Please enter the URL address!', undefined, 'menuLinkAlert');
        $('#menuLinkForm #url').focus();
        return false;
    }

    $.post("/menu/createMenuItem", $('#menuLinkForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                menu.loadMenuContent($('#menuLinkForm #id').val());
                $('#createMenuLinkModal').modal('hide');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'menuLinkAlert');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error', 'menuLinkAlert');
            }
    }, 'json');
}


menu.createMenuPage = function ()
{
    if ($('#menuPageForm #name').val() == '')
    {
        global.renderAlert('Please enter a page name!', undefined, 'menuLinkAlert');
        $('#menuLinkForm #name').focus();
        return false;
    }

    $.post("/menu/createMenuItem", $('#menuPageForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                menu.loadMenuContent($('#menuLinkForm #id').val());
                $('#createMenuLinkModal').modal('hide');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'menuPageAlert');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error', 'menuPageAlert');
            }
    }, 'json');
}

menu.deleteMenuItem = function (id, company)
{
    if (confirm("Are you sure you wish to delete this menu item?"))
    {
        $.post("/menu/deletemenuitem", { id: id, company: company, cgi_token:global.CSRF_hash }, function(data){
                if (data.status == 'SUCCESS')
                {
                    window.location = '/menu?site-success=' + escape(data.msg);
                    // menu.loadMenuContent($('#menuLinkForm #id').val());
                    // global.renderAlert(data.msg, 'alert-success');
                }
                else if (data.status == 'ALERT')
                {
                    global.renderAlert(data.msg);
                }
                else if (data.status == 'ERROR')
                {
                    global.renderAlert(data.msg, 'alert-error');
                }
        }, 'json');
    }
}

menu.loadAccessModal = function (id)
{

    global.ajaxLoader('#access-display');

    $('#accessModal').modal('toggle');

    $.get("/menu/access/" + id, function(data){
        $('#access-display').html(data);
    });
}

menu.savePositionAccess = function ()
{
    $.post("/menu/saveaccess", $('#accessForm').serialize(), function(data){

    });
}

menu.loadModulePermissions = function (module)
{

    // clears any previous HTML
    $('#modPermDisplay').html('');

    $('#newModPermBtn').unbind();


    // global.ajaxLoader('#modPermDisplay');

    $.get("/menu/modulepermissions/" + module, function(data){
        $('#modPermModal').modal('toggle');
        $('#modPermDisplay').html(data);

		//$("input[type='checkbox']").prettyCheckable();


        $('#newModPermBtn').click(function(e){
            menu.loadEditPermModule(module);
        });

    });
}

menu.loadEditPermModule = function (module, id)
{
    $('#modPermModal').modal('hide');
    // $('#modPermModal').modal('destroy');
    $('#editPermModal').modal('show');

    global.ajaxLoader('#editPermDisplay');
    var idDisplay = "";

    if (id !== undefined)
    {
        idDisplay = "/" + String(id);
    }

    $('#saveEditPermBtn').unbind();

    $.get("/menu/editpermission/" + module + idDisplay, function(data){
        $('#editPermDisplay').html(data);

        $('#saveEditPermBtn').click(function(e){
            menu.checkEditPermForm();
        });

    });
}

menu.checkEditPermForm = function ()
{
    if ($('#editPermForm #bit').val() == '')
    {
        global.renderAlert("Please enter a bit (up to 64 bit INT)!", undefined, 'editPermAlert');
        return false;
    }

    if ($('#editPermForm #title').val() == '')
    {
        global.renderAlert("Please enter a tite for this permission!", undefined, 'editPermAlert');
        return false;
    }

    var module = $('#editPermForm #module').val();

    $.post("/menu/savemodpermission", $('#editPermForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                $('#editPermModal').modal('hide');

                menu.loadModulePermissions(module);

                global.renderAlert(data.msg, 'alert-success', 'modPermAlert');

            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'editPermAlert');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error', 'editPermAlert');
            }
    }, 'json');

}

menu.saveModulePermissionAccess = function ()
{
    $.post("/menu/savemodpermaccess", $('#modPermForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                // global.renderAlert(data.msg, 'alert-success');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');
}

menu.deleteMenuHeader = function (id)
{

    if (confirm("Are you sure you wish to delete this menu header? This will delete and links and pages as well as unassign any modules that are associated with this menu header!"))
    {
        $.post("/menu/deleteheader", { id:id, cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = "/menu?site-success=" + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
        }, 'json');
    }

}
