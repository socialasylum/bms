var apply = {}


apply.step13Init = function ()
{
    apply.addEdu();
}

apply.step14Init = function ()
{

    if ($('#jobCnt').val() == 0)
    {
        apply.addJob();
    }
    else
    {
        /*
        for (var i = 1; i <= parseInt($('#jobCnt').val()); i++)
        {
            apply.addJob(i);
        }
        */
    }

}

apply.addEdu = function (next)
{

    var cnt = parseInt($('#schoolCnt').val());

    if (next == undefined) next = cnt + 1;

    global.ajaxLoader('#edu-display');

    $.get("/apply/addEdu/" + String(next), function(data){
        $('#edu-display-' + String(next)).html(data);

        $('#schoolCnt').val(next);
        $('input[name=schoolCnt]').val(next);
    });
}

apply.removeEdu = function (cnt)
{
    $('#edu-wrapper-' + String(cnt)).hide();
}


apply.addJob = function (next)
{
    var cnt = parseInt($('#jobCnt').val());

    if (next == undefined) next = cnt + 1;

    global.ajaxLoader('#job-display');

    $.get("/apply/addJob/" + String(next), function(data){
        $('#job-display-' + String(next)).html(data);


        $('#from_' + next).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('#to_' + next).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('#jobCnt').val(next);
        $('input[name=jobCnt]').val(next);
    });
}

apply.removeJob = function (cnt)
{
    $('#job-wrapper-' + String(cnt)).hide();
}

apply.finishInit = function ()
{

    $('#agree').click(function(e){
        if ($(this).prop('checked') == true)
        {
            $('#submitBtn').removeAttr('disabled');
        }
        else
        {
            $('#submitBtn').attr('disabled', 'disabled');
        }
    });

    $('#submitBtn').click(function(e){
        window.location = '/apply/submit/' + $('#id').val();
        $(this).attr('disabled', 'disabled');
    });
}

apply.confirmationInit = function ()
{
    $('#returnBtn').click(function(e){
        window.location = '/apply/index/' + $('#id').val();
        $(this).attr('disabled', 'disabled');
    });
}
