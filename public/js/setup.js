var setup = {};

setup.index = {};
setup.console = {};
setup.console.delimeter = '$';

setup.index.init = function ()
{
	
	$('select').selectpicker();

	global.slimScroll('#console-output');
	
	$('#consoleInput').val('').focus();
		
		$('#consoleInput').bind('keypress', function(e){

			var $m = $(this);

			var code = e.keyCode || e.which;
			
			if (code == 13 && !e.shiftKey)
			{
					
				e.preventDefault();
			
				setup.console.cmd($m.val());
				
				//if ($m.val().trim() !== '') setup.console.cmd($m.val());
			}
		});

	$('#runBtn').click(function(e){
		setup.index.run();
	});
	
	// if user clicks on terminal focuses input
	$('#console-output').click(function(e){
		$('#consoleInput').focus();
	});
		
	return true;
}


setup.console.cmd = function (cmd, clearInput, noDelim, send)
{
	if (clearInput == undefined) clearInput = true;
	if (noDelim == undefined) noDelim = false;
	if (send == undefined) send = true;
	
	if (clearInput) $('#consoleInput').val('');
	
	var html = $.parseHTML($('#consoleOutputHtml').html());
	
	if (noDelim) $(html).find('.input-group-addon').remove();
	
	$(html).find('.consoleOutput').append(cmd);
	
	if (cmd.toUpperCase() == 'CLEAR')
	{
		$('#console-output').html('');
		
		send = false;
	}
	else $('#console-output').append(html);
	
	if (send && cmd.trim() !== '') setup.console.process(cmd);
	
	$('#console-output').scrollTop( $('#console-output').prop( "scrollHeight" ) );
	$('#console-output').perfectScrollbar('update');
	
	return true;
};

setup.console.process = function (cmd)
{
	$.post('/setup/cmd', { cmd: cmd, cgi_token: global.CSRF_hash }, function(data){
	
		data = data.nl2br();
		
		setup.console.cmd(data, false, true, false);	
	});
};

setup.index.run = function ()
{
	var valid = $('#setupForm').validateForm();
	
	if (!valid) return false;
	
	$('#runBtn').disableSpin({ spinClass: 'fa fa-spin fa-cog' });
	
	setup.console.cmd('Running Setup process...', false, true, false);

	var data = $('#setupForm').serialize();

	$('.panel').disable();

	$.post('/setup/run', data, function(data){
		$('#runBtn').enableSpin();
		$('.panel').enable();
			
		if (data.response !== undefined) setup.console.cmd(data.response.nl2br(), false, true, false);
		
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
			
			setup.console.cmd('Setup was successful. Now redirecting...', false, true, false);
		}
		else
		{
			Danger(data.msg);
			setup.console.cmd('Setup Failed! ' + data.msg, false, true, false);
		}
		
	}, 'json');
};

// when running setup, expands console
setup.console.expand = function ()
{
		
	var rowW = $('#setupForm .row').width();
	
	var w = $('#dbPanel').outerWidth();

	$('#dbPanel, #infoPanel').width(w);

	$('body').css('overflow-x', 'hidden');
	
	$('#dbPanel').velocity({ marginLeft:'-180%' }, { complete:function(){
		$(this).hide();
	} });

	$('#infoPanel').velocity({ marginLeft:'-180%' }, { delay:100, complete:function(){
		$(this).hide();
		// hides panel col'

		//$('#setupForm .row > div:first-child').hide();
		$('body').css('overflow-x', '');
	} });
	
	
	$('#setupForm .row > div:first-child').velocity({ width:0, padding:0 }, { delay:500, duration:600 });
	
	// expands terminal col
	$('#setupForm .row > div:last-child').velocity({ width:rowW }, { delay: 500, duration:620 });
	
	return true;
};


setup.console.contract = function ()
{
	
			
	$('body').css('overflow-x', 'hidden');
	

	
	$('#setupForm .row > div:first-child').velocity('reverse', { duration:420, complete:function(){
		$(this).removeAttr("style");	
		
	}});
	
	$('#setupForm .row > div:last-child').velocity('reverse', { duration:420, complete:function(){
		$(this).removeAttr('style');
	} });	
	
	
	$('#dbPanel').velocity({ marginLeft:0 }, { delay:620, begin:function(){
		$(this).show();
	}, 
	complete:function(){
		//$(this).removeAttr('style');
	} });

	$('#infoPanel').velocity({ marginLeft:0 }, { delay:720, begin:function(){
		$(this).show();
				
	},
	complete:function(){
		//$(this).removeAttr('style');
		$('body').css('overflow-x', '');
	
		$('#dbPanel, #infoPanel').css('width', '');
	} });

	return true;
};