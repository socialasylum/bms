var pricing = {}

pricing.indexInit = function ()
{
    /*
    $('#mod-container-list').find("input").each(function(index, item)
    {
        $(this).click(function(e){
            pricing.calculate();
        });
    });
    */

   // DONT FORGET to update slider in companysettings.js
    $('#slider').slider({
        range: 'min',
        min: 5,
        max: 100,
        step: 5,
        value: 5,
        slide: function (event, ui)
        {
            $('#numUsers').text(ui.value);

            // sets hidden input how many users were selected for use later
            $('#users').val(ui.value);
            $('input[name=users]').val(ui.value);

            pricing.calculate();
        }
    });


    $('#signUpBtn').removeAttr('disabled');

    $('#signUpBtn').click(function(e){
        pricing.signUp();
    });

    pricing.loadPopovers();
}

pricing.calculate = function ()
{

    // first gets the number of users user has selected
    var users = parseInt($('#users').val());

    var total = 0;
    var moduleTotal = 0;

    $('#mod-container-list').find("div").each(function(index, item)
    {
        if ($(this).hasClass('package-selected'))
        {
            moduleTotal = moduleTotal + parseFloat($(this).attr('ppu'));
        } 
    });



    // total = total * parseInt(users) + 100;
    total = global.calculatePricing(users, moduleTotal);

    // total = global.addCommas(total);

    $('#est-display').html(total.toFixed(2));
}

pricing.selectModule = function (sel, module)
{
    if (sel.hasClass('package-selected'))
    {
        // un-set module
        sel.removeClass('package-selected');
    }
    else
    {
        // set module
        sel.addClass('package-selected');
    }

    pricing.calculate();
}

// audrey wrote the "n" in function -- 2013-07-07
pricing.signUp = function ()
{

    var modules = "";

    $('#mod-container-list').find("div").each(function(index, item)
    {
        if ($(this).hasClass('package-selected'))
        {
            modules += "&module[]=" + $(this).attr('value');

            // modules.push($(this).attr('value'));
        }
    });


    window.location = '/signup?users=' + $('#users').val() + modules;

    $('#signUpBtn').attr('disabled', 'disabled');
}

pricing.loadPopovers = function ()
{
    $('#unvModContainer').find("a").each(function(index, item)
    {
        // console.log('Loading Popover: ' + $(item).attr('title') + ", ID: " + $(item).attr('id'));

        $(item).popover({
            html: true
        });
    });
}
