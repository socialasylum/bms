var departments = {}

departments.indexInit = function ()
{
	global.createDataTable('#departments');

}

departments.editInit = function ()
{
	$('select').selectpicker();

    $('#saveBtn').click(function(e){
        departments.checkEditForm();
    });

    $('#cancelBtn').cancelButton('/department');
}

departments.checkEditForm = function ()
{


    if ($('#name').val() == '')
    {
    	Warning('Please enter a department name!');
        $('#name').focus();
        return false;
    }


    if ($('#active :selected').val() == '')
    {
    	Warning("Please select the status of the department!");
        $('#active').focus();
        return false;
    }

    $('#saveBtn').disableSpin();

    $.post("/department/save", $('#editForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                redirect('/department?site-success=' + encodeURI(data.msg));
            }
            else if (data.status == 'ALERT')
            {
            	Warning(data.msg);
                $('#saveBtn').enableSpin();
            }
            else if (data.status == 'ERROR')
            {
            	Danger(data.msg);
                $('#saveBtn').enableSpin();
            }
    }, 'json');
}


