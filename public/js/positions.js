var positions = {}


positions.indexInit = function ()
{
	global.createDataTable('#positionsTable');
	
	return true;
}

/*
positions.createInit = function ()
{
    CKEDITOR.replace('description');

    $('#createBtn').click(function(e){
        positions.checkCreateForm();
    });

    $('#cancelBtn').click(function(e){
        if (confirm("Are you sure you wish to cancel?"))
        {
            window.location = "/position";
            $('#cancelBtn').attr('disabled', 'disabled');
        }
    });
}

*/

positions.editInit = function ()
{
    CKEDITOR.replace('description');

    $('#saveBtn').click(function(e){
        positions.updatePosition();
    });

    $('#cancelBtn').click(function(e){
        if (confirm("Are you sure you wish to cancel?"))
        {
        	redirect("/position")

            $('#cancelBtn').attr('disabled', 'disabled');
        }
    });
    
    return true;
}

positions.checkCreateForm = function()
{

    if ($('#name').val() == '')
    {
        global.renderAlert("Please enter a position name!");
        $('#name').focus();
        return false;
    }

    if ($('#shortName').val() == '')
    {
        global.renderAlert("Please enter a short name!");
        $('#shortName').focus();
        return false;
    }

    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);

    $('#createBtn').attr('disabled', 'disabled');

    $.post("/position/createNewPosition", $('#positionForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/position?site-success=' + escape('Position has been created!');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                $('#createBtn').removeAttr('disabled');
            }
            else if (data.status == 'ERROR')
            {
            	Danger(data.msg);
                $('#createBtn').removeAttr('disabled');
            }
    }, 'json');
}

positions.editPosition = function(b)
{
	$(b).disableSpin();
	
	redirect('/position/edit/' + $(b).data('id'));

	return false;	
}

positions.updatePosition = function ()
{

    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);

    $('#saveBtn').disableSpin();

    $.post("/position/updateposition", $('#editForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
            	Success(data.msg);
            }
            else if (data.status == 'ALERT')
            {
            	Warning(data.msg);
            }
            else if (data.status == 'ERROR')
            {
            	Danger(data.msg);
            }

            $('#saveBtn').enableSpin();
            
    }, 'json');
    
    return true;
}

positions.deletePosition = function (id, company)
{
    if (confirm("Are you sure you wish to delete this position?"))
    {
        $.post("/position/deleteposition", { id: id, company: company }, function(data){
                if (data.status == 'SUCCESS')
                {
                	redirect("/positions/index?site-success=" + encodeURI(data.msg));
                }
                else if (data.status == 'ALERT')
                {
                	Warning(data.msg);
                }
                else if (data.status == 'ERROR')
                {
                	Danger(data.msg);
                }
        }, 'json');
    }
}
