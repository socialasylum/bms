var msg = {}

msg.userSearchData;
msg.currentFolder;
msg.currentFolderName; // text name of folder viewing content
msg.selectedMsgs = [];
msg.syncRate;
msg.syncTimeout;
msg.syncCheckInterval;
msg.syncInProgress = false;
msg.composeck; // variable for compose ckeditor
msg.tree;
msg.composeWindow;
msg.ba;
msg.previewID = 0;
msg.folderContentMenu;
msg.treeContextMenu;
msg.folderEnd = false;
msg.ftdW;
msg.socket;
msg.syncRequest;
msg.folderRequest;
msg.previewRequest;
msg.noPasswdAccounts;
msg.draft = {};
msg.draft.interval = 10000;
msg.draftuid = 0;
msg.navMoved = false; // flag for compose and preview for when the sub navbar is moved to top
msg.lastID = []; // array that holds the last ID's displayed in folder - both internal and external
msg.lastIDOverall = 99999999;

//msg.lastInternalID = 0; // last ID in folder content in which to load more previous messages
//msg.lastExternalID = 0; // last ID for external msg 


msg.indexInit = function()
{
	msg.renderFolderTree();
	msg.getFolderContent('Inbox', 'Inbox');
	
    //global.noRightClick();
    folders.build();
	
	global.trackMouse();
	    
    norightclick();

	msg.getNoPasswdAccounts();

    $(window).resize(function(){
        msg.containerResize();
    });

    msg.containerResize();
    
    
    // sets the URL to the function which will create the folders
	folders.setCreateUrl('/msg/createfolder');

	folders.successCreate = function (folder, name)
	{
		msg.sync();
	};

    $('#createFolderBtn').click(function(e){
    	folders.createFolder();
    });


    // removes right click context menu
    $('body').click(function(e){

        if ($(e.target)[0].tagName != 'A')
        {
           global.clearRightClick();
        }
    });

    $('#refreshBtn').click(function(e){
        msg.sync();
    });
	msg.setSyncTimeout();
	msg.setRightClick();
    msg.createScroll('#msg-preview-scroll');
    msg.createScroll('#tree-scroll');
	msg.ftdW = $('#folder-tree-display').outerWidth();
	msg.widthAdjust();
	msg.buildContentScroll();
	$(window).on('socket.data-received', function (e, d){
		Success("Message has been sent");
	});
	$(window).resize(function(){
		msg.widthAdjust();
	});
};

msg.buildContentScroll = function ()
{

    msg.createScroll('#folder-content-scroll');
    
	$('#folder-content-scroll').scroll(function(){

	   if ($('#folder-content-scroll').scrollTop() + $('#folder-content-scroll').height() > $('#msg-folder-content').height() - 100)
	   {
		   // load more msgs if viewing folders
		   if (msg.currentFolder != undefined && parseInt(msg.currentFolder) > 0) msg.getFolderContent(msg.currentFolder, msg.currentFolderName, true);
		   
		   
	     	
	   }
	});
}

msg.setRightClick = function ()
{
	msg.folderContentMenu = $('#msg-list-display').contextMenu({
		target:'.msg',
		debug: global.debug,
		ajax:
		{
			dataType: 'json'
		},
		data:
		{
			url: '/msg/rclick'
		},
		selected: function(e, t, a, id)
		{

			if (a == 'delete')
			{
				msg.moveMsgs(msg.selectedMsgs, $('#trash').val(), function (){
					msg.reRenderTree();
					msg.getFolderContent(msg.currentFolder, msg.currentFolderName);
				});

			}
			
			if (a == 'folder')
			{

				msg.moveMsgs(msg.selectedMsgs, id, function (){
					msg.reRenderTree();
					//msg.getFolderContent(msg.currentFolder);
				});
				
			}
		}
	}).data('contextMenu');
	
	$('#msg-list-display').on('element.rightclick', function(e, el){
		if ($(el).hasClass('ui-selected'))
		{
			
		}
		else
		{
			$('blockquote').removeClass('ui-selected');
			
			msg.selectedMsgs = [];
			
			$(el).addClass('ui-selected');
		
			msg.selectedMsgs.push($(el).attr('value'));
		}
	});

	msg.treeContextMenu = $('#folder-tree').contextMenu({
		target:'.ui-fancytree li',
		debug: global.debug,
		data:
		{
			obj:[
				{name: 'Delete', action: 'delete', iconClass:'fa fa-trash-o' }
				//,{name: 'Settings', action: 'settings', iconClass:'fa fa-cog' }
			]
		},
		selected: function(e, t, a, id)
		{
			if (a == 'move')
			{
				
			}

			if (a == 'delete')
			{
				if (t.ftnode.data.editable == undefined) return false;
				
				//if (t.ftnode.data.editable)
				//{
					msg.deleteFolder(t.ftnode.data.name, t.ftnode.key);
				//}
			}
			
		}
	}).data('contextMenu');
};


msg.settingsInit = function ()
{
    $('#saveBtn').click(function(e){
        msg.saveSettings();
    });

    $('#cancelBtn').cancelButton('/msg');

    CKEDITOR.replace('sig');

    if ($('#accountsTbl').exists())
    {
		global.createDataTable('#accountsTbl');
    }
    
    if ($('#smtpTbl').exists())
    {
    	global.createDataTable('#smtpTbl');
    }
}

msg.compose = function (url, to)
{
	if (url == undefined) url = '/msg/compose?window';
	
	if (to !== undefined && parseInt(to) > 0) url += '&to=' + to;
	
	var h = $(window).height();
	var w = $(window).width();
	
	h = (h * 0.8);
	w = (w * 0.8);

	msg.composeWindow = window.open(url, '_blank','width=' + w + ',height=' + h + ',toolbar=no, scrollbars=no, menubar=no, status=no, location=no,titlebar=no');
	
	return true;
}

msg.composeInit = function ()
{
	window.opener = self;
	
	//msg.composeWindowResize();
	
	$('select').selectpicker();
	

	msg.getMsgTo(undefined, undefined, undefined, function (){
		msg.composeWindowResize(window.opener, function (ckHeight){
			global.ckeditor($('#msgBody'), { height:ckHeight + 'px' });
		});
	});
	
	$(window).on('socket.data-received', function (e, d){
		//$(window.parent).Success("Message has been sent");
		window.close();
	});


    $(window).resize(function(){
       msg.composeWindowResize(window.opener);
    });


    $('#sendBtn').click(function(e){
        msg.sendMessage(this);
    });

	$('#cancelBtn').click(function(){
		Confirm("Are you sure you wish to cancel?", function (){
			//$(window.parent).Warning("Message was cancelled");
			
			window.close();
			

		});
	});

    $('#add').autocomplete({
        source: "/msg/usersearch",
        minLength: 3,
        select: function(event, ui)
        {
            msg.getMsgTo(ui.item.id, undefined, $('#sendType :selected').val(), function(){
                msg.composeWindowResize(window.opener);
            });
        },
        close: function(event, ui)
        {
        }
    });

    // if nothing was chosen from autocompelete, will add address
    $('#addBtn').click(function(e){
        console.log('clicked');
        msg.getMsgTo($('#add').val(), undefined, $('#sendType :selected').val(), function(){
            msg.composeWindowResize(window.opener);
        });
        $('#add').val(''); // clears value
        $('#add').focus(); // refocues add
        $('#add').effect(global.effect); // effect to get users attention
    });
    msg.draft.startTimer();
};

msg.preview = function ()
{
	$(window).resize(function(){
		msg.previewResize();		
	});
	
	msg.previewResize();
	
	$('#email-body').ajaxLoader();
	
	var id = $('#id').val();
	
	$.get('/msg/body/' + id, function (html){
	
		$('#email-body').ajaxLoader.clear();
		
		var parsed = $('#parsed').val();

		if (parsed == 1)
		{
			$('#email-body').replaceWith(html);
		}
		else
		{
			html = msg.scrubEmailBody(html);
			$('#email-body').replaceWith(msg.scrubEmailBody(html));
		}
	});
	msg.createScroll('.preview-scroll');
};


msg.previewResize = function (compFunction)
{

	if (!msg.navMoved)
	{
		$('.navbar.subnav').insertBefore('.wrapper');
	
		msg.navMoved = true;
	}

	var h = $(window).height();
	var navHeight = $('.navbar.subnav').outerHeight();
	
	$('#site-wrapper').css('padding-top', navHeight);
	

	
	//clog("H: " + h);
	//clog($('.navbar.subnav').outerHeight());

	var ch = h - navHeight;
	
	$('.wrapper')
		.css('height', ch)
		.css('overflow', 'hidden');
	
	/*
	
	var mpch = ch - 40;
	
	//$('.msg-preview').velocity({ height: mpch + 'px'  });
	
	
	$('#main-container')
		.css('padding', '10px')
		.css('position', 'relative')
		.css('height', ch);
		/*
		.velocity({ height: ch }, {
			complete:function ()
			{
				if (compFunction !== undefined && typeof compFunction == 'function') compFunction();
			}
		});
		*/
}

msg.reply = function ()
{
	if (msg.previewID == 0) return false;
	
	var url = '/msg/compose?reply=' + msg.previewID + '&folder=' + msg.currentFolderName;
	
	if ($('#account').exists()) url += '&account=' + $('#account').val();
	
	url += '&window';
	
	global.window(url);
	
	return true;
}


msg.forward = function ()
{
	if (msg.previewID == 0) return false;
	
	var url = '/msg/compose?forward=' + msg.previewID;
	
	if ($('#account').exists()) url += '&account=' + $('#account').val();
	
	url += '&window';
		
	global.window(url);
	
	return true;
}


msg.widthAdjust = function ()
{
	var cW = $('.msg-container').width();
	
	var treeW = $('#folder-tree-display').outerWidth();
	var listW = $('#msg-list-display').outerWidth();

	var w = (cW - (treeW + listW) - 1);
	//clog($('.msg-container').width() + ' : ' + (treeW + listW + ));
	//$('#msg-preview').velocity({ width:w });
	$('#msg-preview').width(w);
	return true;
	
}

msg.containerResize = function ()
{
	$('.wrapper').css('position', 'absolute').css('overflow', 'hidden');

	var wh = $(window).height();
	
	$('body').height(wh);
	$('body').css('overflow', 'hidden');
	
	var navHeight = $('#main-top-nav').outerHeight();
	
	var h = (wh - navHeight);
 

	$('.wrapper').css('height', h);

    //$('#msg-list-display, #msg-preview, #folder-tree-display, .msg-container').velocity({ height: (h - 55) }, { complete:function(){
    $('.msg-container').velocity({ height: (h - 50) }, { complete:function(){
    	bs.resize();
    }});

 
	$('#folder-content-scroll, #msg-preview-scroll, #tree-scroll').perfectScrollbar('update');
}

msg.composeWindowResize = function (wo, compFunction)
{
	var h = $(wo).height();
	
	$('body').css('overflow', 'hidden');
	
	var navHeight = $('.subnav').outerHeight();
	
	var formH = $('#composeForm').outerHeight();
	
	var w = $('#composeForm').width();
		
	var toH = $('#msg-to-container').outerHeight();
	var ccH = $('#compose-controls').outerHeight();

	var ch = h - (navHeight + 1);
	
	$('.wrapper').css('height', ch);
	
	
	$('#main-container')
		//.css('padding', '10px')
		.css('position', 'relative');
		//.height(ch);

    //var formIH = $('#composeForm').innerHeight();
				
	var ckh = h - (toH + ccH + navHeight + 120);
    
	if (compFunction !== undefined && typeof compFunction == 'function') compFunction(ckh);
    
	return true;
}


msg.containerIndexResize = function ()
{
    // var h = $(window).height() - 265;
    var h = $(window).height() - 200;

    $('.msg-container').css('min-height', h + 'px');
}

msg.renderFolderTree = function (sf)
{
	msg.tree = $('#folder-tree').fancytree({
	    source: {
		    url: '/msg/foldertree',
		    cache: false,
		    complete: function ()
		    {
				msg.treeContextMenu.refreshTargets();
				msg.setTreeDroppable();
				
				if (sf !== undefined && typeof sf == 'function') sf();
		    }
	    },
    	expanded: true,
    	collapse: function ()
    	{
	        msg.widthAdjust();
    	},
    	expand: function ()
    	{
        	msg.widthAdjust();	
    	},
        activate: function(e, o)
        {
			msg.widthAdjust();
			
			msg.lastInternalID = 0;
			msg.lastExternalID = 0;
			
			msg.folderEnd = false;
			
	        msg.getFolderContent(o.node.key, o.node.data.name, false);
        }
    }).fancytree('getTree');

	return true;
}
/*
msg.getTreeHtml = function (sf)
{
    $.get("/msg/foldertree", function(data){
    	if (sf !== undefined && typeof sf == 'function') sf(data);
	});
	
	return true;
}
*/

msg.reRenderTree = function (sf)
{
	if (msg.tree == undefined)
	{
		msg.renderFolderTree(sf);
		
		return false;
	}

	msg.tree.reload().done(function(){
		msg.treeContextMenu.refreshTargets();
			
		msg.setTreeDroppable();
		
		if (sf !== undefined && typeof sf == 'function') sf();
	});
	
	return true;
}

msg.getFolderContent = function (id, name, append)
{
	if (msg.folderRequest !== undefined) msg.folderRequest.abort();
	
	if (msg.folderEnd) return false;

	if (name == undefined) throw new Error("Folder name is not defined!");

	if (append == undefined) append = false;
	
	var sel = '#msg-folder-content';
	
	var $sel = $(sel);
	
	// clears selectable
	if ($('#msg-folder-content').selectable('instance') !== undefined) $('#msg-folder-content').selectable('destroy');
	
	msg.currentFolderName = name;
	msg.currentFolder = id;

	if (!append)
	{
		msg.clearID();
		$('#msg-folder-content').empty();
		$('#folder-content-scroll').scrollTop(0);
	}

    $('#msg-folder-content').ajaxLoader(false, append);
    
    var post =
    {
	    id: id,
	    name: name,
	    accountIDs: msg.lastID,
	    lastIDOverall: msg.lastIDOverall
    };
    

	msg.folderRequest = $.post('/msg/foldercontent', post, function(data){
    	msg.folderRequest = undefined;
		if (data.length <= 0)
		{
			msg.folderEnd = true;
			
			if (!append) $('#msg-folder-content').Info('This folder is empty', 'No Messages', { clearTimeoutSeconds: 10, zIndex: 1 });
		}
		
		$('#msg-folder-content').ajaxLoader.clear();
    	
    	$sel.find('.alert').remove(); 
    	
        $sel.append(data);
		
		msg.setLastID();
		
		msg.setFolderContentDraggable();
		
        // loads preview for first message
        if (!append)
        {
        	var first = $('#msg-folder-content > blockquote').first().attr('value');
        	
	        if (first !== undefined) msg.getPreview(first);
        }

        
        msg.folderContentMenu.refreshTargets();
                	
		$.alerts.htmlAlerts();
                	
        $('#folder-content-scroll').perfectScrollbar('update');
    });
};

msg.setLastID = function ()
{	
	$('#msg-folder-content > blockquote').each(function(i, el){

		var $el = $(el);

		var account = parseInt($el.data('account'));
		var id = parseInt($el.attr('value'));
		var extID = parseInt($el.data('extmsgid'));
		var addAccount = true;
					
		if (id == undefined) return false;
		if (account == undefined) return false;
		if (extID == undefined) return false;


		if (id < msg.lastIDOverall) msg.lastIDOverall = id;

		for (var i = 0; i < msg.lastID.length; i++)
		{

			if (msg.lastID[i]['account'] == account)
			{
				addAccount = false;
				if (extID > msg.lastID[i]['id']) msg.lastID[i]['id'] = extID;
				break;
			}
		}
		
		if (addAccount) msg.lastID.push({ account: account, id: extID });
		
	});
	
	return true;
};

msg.clearID = function ()
{
	msg.lastID = [];
	msg.lastIDOverall = 99999999;
};

msg.getMsgTo = function (to, deleteToKey, sendType, sf)
{
    $('#msg-to-display').ajaxLoader();

    var url = "";

    if (to !== undefined)
    {
        url = "?to=" + String(to);

    }

    if (deleteToKey !== undefined)
    {
        url = "?deleteKey=" + deleteToKey;
    }

    if (sendType !== undefined) url = url + "&sendType=" + String(sendType);

    $.get("/msg/msgcomposeto" + url, function(data){
        $('#msg-to-display').html(data);
        $('#add').val('');
        if (sf !== undefined && typeof sf == 'function') sf(data);
        
    });
}

msg.typeaheadUserSelect = function (item)
{
    var id = 0;

    for (var i = 0; i < msg.userSearchData.options.length; i++)
    {
        if (item == msg.userSearchData.options[i])
        {
            id = msg.userSearchData.id[i];
            break;
        }
    }

    // if id was found
    if (id > 0)
    {
        msg.getMsgTo(id, undefined, $('#sendType :selected').val());
    }

}


msg.sendMessage = function (b)
{
    $(b).disableSpin();
    	
    $.post("/msg/send", $('#composeForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
        	//ms.emit('msg-sent', { userid: });
	        $(window).trigger("socket.data-received");
	        	        	
        	if (data.users !== undefined)
        	{
        		if (ms.connected) msg.notifyUsers(data.users);
				else $(window).trigger("socket.data-received"); // not connect, triggers data response anyways
        	}
        	else
        	{

        	}
        	
            //window.location = '/msg?site-success=' + escape("Message has been sent!");
        }
        else if (data.status == 'ALERT')
        {

			Warning(data.msg);
        	
    	    $(b).enableSpin();
        	
        }
        else if (data.status == 'ERROR')
        {
        	Danger(data.msg);

    	    $(b).enableSpin();
        }
    }, 'json');
}

msg.notifyUsers = function (users)
{
	ms.emit('msg-sent', users);
	
	return true;
}

msg.getPreview = function (id)
{
	if (id === undefined)
	{
		Danger("Unable to load message (Message ID is empty)");
		return false;
	}

	$blockExt = $("#msg-folder-content > blockquote[data-external='true'][value='" + id + "']");
	$blockInt = $("#msg-folder-content > blockquote[data-external='false'][value='" + id + "']");
	
	var ext = ($blockExt.length <= 0) ? false : true;
	var $block = ($blockExt.length <= 0) ? $blockInt : $blockExt;

	//if (msg.syncRequest !== undefined) return false;
	//if (msg.folderRequest !== undefined) return false;
	if (msg.previewRequest !== undefined) msg.previewRequest.abort();
	
	var parsed = $block.data('parsed');
	if (parsed == undefined) parsed = false;
	
	$('#msg-preview-scroll').scrollTop(0);
	
	$('#msg-preview-content').ajaxLoader();

	//var urlAdd = '?account=' + $block.data('account') + '&folder=' + msg.currentFolder;
	
    msg.previewRequest = $.get("/msg/preview/" + msg.currentFolderName + '/' + id, function(data){

    	msg.previewRequest = undefined;
	    msg.previewID = id;
		
		$('#msg-preview-content').ajaxLoader.clear(function () {


			if (parsed == false)
			{

				var emailBody = $(data).find('#email-body').html();
	
				$(data).find('#email-body').empty();
			
				emailBody = msg.scrubEmailBody(emailBody);
								
				$(data).find('#email-body').replaceWith(emailBody);
	
				$block.data('parsed', true);
			}
			
			$('#msg-preview-content').prepend(data);
			
	    	msg.toggleActions(false, id);
			
	        // if message is unread - will set it in DB to read
	        if ($('#subject' + id).hasClass('unread'))
	        {
	        }
	
	        $('#subject' + id).removeClass('unread');
	        
	        $('#msg_' + id).removeClass('unread');
	
			$('#msg-preview-scroll').perfectScrollbar('update');
		});
    });
};

// scrubs an email body removing unwanted tags
msg.scrubEmailBody = function (data, id)
{
	if (id == undefined) id = 0;
	
	var $html = $("<div>").append(data);
	var parsedHtml;
		
	$html.find('style').remove();
	$html.find('a').attr('target', '_blank');
	
	var parsedHtml = $html.html();
	
	return $html.html();
};

/*
* @param id - msg ID


msg.setNavMovemsg = function (id)
{
	$('#moveDrop .dropdown-menu > li > a').each(function(i, a){
		$a = $(a);
		
		var folder = $a.data('folder');
		
		
		$a.attr('onclick', "msg.previewMoveMsg(" + id + ", " + folder + ")");

	});
	
	return true;
};
*/
msg.toggleActions = function (hide)
{
	if (hide == undefined) hide = false;
	
	
	var m = (hide) ? 'fadeOut' : 'fadeIn';

	if (hide && $('#replyBtn').css('display') == 'none') return false;
	else if (!hide && $('#replyBtn').css('display') !== 'none') return false;

	$('#replyBtn, #forwardBtn, #moveDrop').velocity(m);
	
	return true;
}

msg.previewMoveMsg = function (m, folder)
{
	msg.moveMsg(m, folder, function(){
		msg.reRenderTree();
		msg.getFolderContent(msg.currentFolder);
	});
}

msg.createScroll = function (div)
{
	$(div).perfectScrollbar({
		wheelSpeed: 20,
		wheelPropagation: true,
		minScrollbarLength: 20,
		includePadding: true
	}).css('overflow', 'hidden')
	.css('position', 'relative');
}

msg.setMsgRead = function ()
{

}

msg.setFolderContentDraggable = function ()
{
	// clears selectable
	if ($('#msg-folder-content').selectable('instance') !== undefined) $('#msg-folder-content').selectable('destroy');

        // clears msg select array
        msg.selectedMsgs = [];

        var sel = $('#msg-folder-content').selectable({
            filter: "blockquote",
            selecting: function (event, ui)
            {
            	if ($(ui.selecting).attr('value') !== undefined)
            	{
            		msg.selectedMsgs.push($(ui.selecting).attr('value'));
            	}

            },
            selected: function (event, ui)
            {
                    $(ui.selected).draggable({
                    cursor: "move",
                    cursorAt:
                    {
	                    top:5,
	                    left:5
                    },
                    helper: function(){
						
						
						var $drag = $('<div/>').attr('class', 'drag-email-container');
						
						$('body').append($drag);
						var html = "<span><i class='fa fa-envelope'></i></span>";

												
						$drag.append(html);
						
						return $drag;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            },
            unselecting: function (event, ui)
            {

				msg.removeSelectedMsg($(ui.unselecting).attr('value'));

            },
            stop: function (event, ui)
            {
				if (msg.selectedMsgs.length > 0) msg.getPreview(msg.selectedMsgs[0]);
            }
        });
        

}

msg.removeSelectedMsg = function (id)
{
	var a = [];

	for (var i = 0; i <= msg.selectedMsgs.length; i++)
	{
		if (msg.selectedMsgs[i] == undefined) continue;
		
		if (id !== msg.selectedMsgs[i])
		{
			a.push(msg.selectedMsgs[i]);
		}
	}
	
	msg.selectedMsgs = a;
	
	return true;
};

msg.getSelCnt = function ()
{
    alert(msg.selectedMsgs.length);
}

msg.setTreeDroppable = function()
{
    // goes through each span1 div in doc-container and makes draggable/droppable
    $('.fancytree-container li').each(function(index, item)
    {

            // folders are a drop zone
            
            if ($(item).data('draggable')) $(item).droppable('destroy');
            
            $(item).droppable({
                over: function (event, ui)
                {
                    $(this).addClass('folder-highlight');
                },
                out: function (event, ui)
                {
                    $(this).removeClass('folder-highlight');
                },
                drop: function (e, ui)
                {
                    console.log(e.target.ftnode.key);
					if (e.target.ftnode.key !== undefined)
					{
						msg.moveMsgsToFolder(e.target.ftnode.key);					
					}

					
                    // docs.moveSelected(event, ui, $(this).attr('value'));
                    $(this).removeClass('folder-highlight');
                },
                helper: function()
                {

                }
            });
    });
};

msg.folderContentRightClick = function (e)
{
    global.clearRightClick();

    // var parentOffset = item.parent().offset();
    
    var parentOffset = $('#msg-list-display').parent().offset();
    
    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;

    $('body').append("<div id='right-click-menu' style=\"top:" + String(e.pageY)  + "px;left:" + String(e.pageX) + "px;\"></div>");

    $('#right-click-menu').html($('#msgRightClickDisplay').html());
}

msg.moveMsgsToFolder = function (folder)
{
    for (var i = 0; i < msg.selectedMsgs.length; i++)
    {
        msg.moveMsg(msg.selectedMsgs[i], folder);
    }

    msg.reRenderTree();

    global.clearRightClick();
};



msg.moveMsg = function (msg, folder, sf)
{
    $.post("/msg/movemsg", { msg: msg, folder: folder, cgi_token: global.CSRF_hash }, function(data){
        if (data.status == 'SUCCESS')
        {
            //$('#msg_' + msg).hide();
            $('#msg_' + msg).velocity('slideUp');
            if (sf !== undefined && typeof sf == 'function') sf();
        }
        else if (data.status == 'ALERT')
        {
            Warning("Unable to move message!");
            return false;
        }
        else
        {
            return false;
        }
    }, 'json');
}

msg.emptyTrash = function ()
{
	Confirm("Are you sure you wish to empty the trash folder?", function (){
		$.getJSON("/msg/emptytrash", function(data){
			if (data.status == 'SUCCESS')
			{
				Success(data.msg);
	        	msg.reRenderTree();
	        					
			}
			else if (data.status == 'WARNING')
			{
				Warning(data.msg);
			}
			else
			{
				Danger(data.msg);
			}

		});
    	
	});
	
    
}


msg.saveSettings = function ()
{


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['sig'].getData();
    $('#sig').val(str);

    $.post("/msg/savesettings", $('#msgSettingsForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');
        }
    }, 'json');
}


msg.addaccountInit = function ()
{
	$('select').selectpicker();

    $('#saveBtn').click(function(e){
        msg.checkAddAccountForm();
    });

    $('#cancelBtn').cancelButton('/msg/settings');


    $('#type').change(function(e){
        msg.checkOutgoingField();
    });

    msg.checkOutgoingField();
}

msg.checkOutgoingField = function ()
{
    if ($('#type :selected').val() == 1 || $('#type :selected').val() == 2)
    {
        $('#smtpServerFormGroup').show(global.showEffect);
        $('#yournameFormGroup').show(global.showEffect);
        $('#emailAddressFormGroup').show(global.showEffect);
    }
    else
    {
        $('#smtpServerFormGroup').hide(global.hideEffect);
        $('#yournameFormGroup').hide(global.hideEffect);
        $('#emailAddressFormGroup').hide(global.hideEffect);
    }
}

msg.checkAddAccountForm = function ()
{
    if ($('#mailbox').val() == '')
    {
        global.renderAlert('Please enter a server!');
        $('#mailbox').focus();
        $('#mailbox').effect(global.effect);
        return false;
    }

    $('#saveBtn').attr('disabled');

    $.post("/msg/saveaddaccount", $('#accountForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/msg/settings?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
    }, 'json');
}

msg.sync = function ()
{
	if (msg.syncRequest !== undefined) msg.syncRequest.abort();
	
	// sync check is already running
	if (msg.syncCheckInterval !== undefined) return false;
	
    // clears timeout to ensure it doesn't run again while syncing
    clearTimeout(msg.syncTimeout);

    $('#refreshBtn').attr('disabled', 'disabled');
    $('#refreshIcon').addClass('fa-spin');

	//if (msg.tree !== undefined) $('#folder-tree').fancytree('disable');
	
	// if showing folder content, will disable drag functionality
	//if ($('#msg-folder-content > blockquote').length > 0) $('#msg-folder-content').selectable('destroy');

	$.ajax({
		url:'/msg/sync',
		dataType:'json',
		method:'get',
		success: function(data)
		{

			if (data.status == 'STARTED')
			{

			}
			else if (data.status == 'SYNCHRONIZING')
			{

			}

			msg.syncInProgress = true;				
			msg.checkSync();

		},
		error: function (e)
		{
			Danger("There was an error syncornizing your accounts!<br><br>" + e);
			msg.setSyncTimeout();
		},
		complete: function (data)
		{

            msg.syncRequest = undefined;
			
			// reenables drag
			//if ($('#msg-folder-content > blockquote').length > 0) msg.setFolderContentDraggable();
			
        	//if (msg.tree !== undefined) $('#folder-tree').fancytree('enable');
			//msg.reRenderTree();
		}
	});
}

msg.checkSync = function ()
{
	if (msg.syncCheckInterval !== undefined) return false;

	msg.syncCheckInterval = setInterval(function(){
		$.ajax({
			url: '/msg/check_sync',
			method: 'get',
			dataType: 'json',
			success: function (data)
			{
				if (data.status == 'SUCCESS')
				{
					// accounts have been synched
					if (data.gearmanStatus == 'STARTED')
					{
						// process is still running
					}
					else
					{
						$('#refreshBtn').removeAttr('disabled');
						$('#refreshIcon').removeClass('fa-spin');
						
						msg.syncInProgress = false;
						
						clearInterval(msg.syncCheckInterval);
						
						msg.syncCheckInterval = undefined;
						msg.setSyncTimeout();
						
						msg.reRenderTree();
					}

					if (data.gearmanStatus == 'SUCCESS')
					{
						Success("<i class='fa fa-check'></i> Accounts have been synchronized");
					}
					
					if (data.gearmanStatus == 'ERROR') Danger(data.gearmanResponse.error);
					
					if (data.gearmanStatus == 'WARNING') Danger(data.gearmanResponse);
					
				}
				else
				{
					Warning(data.msg);
				}
			},
			error: function (data) { Danger(data); }
		});
				
	}, 10000);

	return true;
}

msg.setSyncTimeout = function ()
{
	if (!$('#syncRate').exists()) throw new Error('No Sync Rate');
	else msg.syncRate = parseInt($('#syncRate').val());

    if (msg.syncRate > 0)
    {
        msg.syncTimeout = setTimeout(function(){ msg.sync() }, msg.syncRate);

		return true;
    }

	return false;
}

msg.editAccount = function (id, b)
{
    window.location = '/msg/addaccount/'+ id;
    $(b).attr('disabled', 'disabled');
}

msg.deleteFolder = function (folderName, folder)
{
	Confirm("Are you sure you wish to delete the folder " + folderName + "?", function(e){
		$.post('/msg/deletefolder', { folder: folderName, cgi_token: global.CSRF_hash }, function(data){
			if (data.status == 'SUCCESS')
			{
				msg.reRenderTree();
				msg.sync();
			}
		}, 'json');
	});
}

msg.moveMsgs = function (selectedMsgs, newFolder, sf)
{

	$(selectedMsgs).each(function(i, m){
		$('#msg_' + m).velocity('slideUp');
	});

	$.post('/msg/movemsgs', { msgs: selectedMsgs, folder: newFolder, cgi_token: global.CSRF_hash }, function(data){
		if (data.status == 'SUCCESS')
		{
			if (sf !== undefined && typeof sf == 'function') sf();
		}
	}, 'json');
}


msg.deleteExternalAccount = function (b, id)
{
	Confirm("Are you sure you wish to delete this account?", function() {
		if (result == true)
		{
			$(b).disableSpin();
			
			$.post('/msg/removeaccount', { id: id, cgi_token: global.CSRF_hash }, function(data){
				if (data.status == 'SUCCESS')
				{
					var tr = $(b).parent().parent();
					
					$(tr).velocity({ opacity:0, height:0 }, {
						complete:function(){
							$(this).remove();
						}
					});
				}
				else
				{
					Danger(data.msg);
					$(b).disableSpin();
				}
			}, 'json');		
		}
	}); 

}


// parses hidden input of accounts that do not havea a saved passwd
msg.getNoPasswdAccounts = function ()
{
	if (!$('#noPasswd').exists()) return false;
	
	msg.noPasswdAccounts = $.parseJSON($('#noPasswd').val());
	
	return msg.noPasswdAccounts;
}

// if account does not have password, this is a prompt to enter it
msg.accountPW = function (email)
{
	if (email == undefined) email = '';
	else email = 'for ' + email;
	
	var html = "<p class='lead'>Please enter your password " + email + "</p>" +
	"<input type='password' class='form-control' name='accountPasswd' id='accountPasswd'>";

	Dialog(html, {
		header: "Account Password",
		backdrop: { show:false },
		deny: { show: true },
		ready: function(dialog){
			$(dialog).find('#accountPasswd').focus();
		}
	});
	
	return true;
}

// saves a message draft.
msg.draft.save = function ()
{
	$.ajax({
		url: '/msg/send/true/' + msg.draftuid,
		data: $('#composeForm').serialize(),
		method: 'post',
		dataType: 'json',
		success: function (data)
		{
			//Info("Draft saved");
            $('#saveDraftTxt').fadeIn('slow', function() {
                window.setTimeout(function() { $('#saveDraftTxt').fadeOut('slow'); }, 3000);
            });
            msg.draftuid = data.msguid;
		},
		error: function (data) { Danger("Unable to save draft!<br>" + Data); },
		complete: function (data)
		{
	
		}
	});

	return true;
};

msg.draft.startTimer = function ()
{
	if (msg.draft.timeout !== undefined) return false;
	
	msg.draft.timeout = setInterval(function () {
		msg.draft.save();
	}, msg.draft.interval);

	return true;
};