var leads = {};

leads.indexInit = function ()
{
	global.createDataTable('#sentTbl');
	global.createDataTable('#receivedTbl');
	global.createDataTable('#chapterTbl');

    if ($('#sentToMeMap').exists())
    {
        gm.initialize('sentToMeMap', undefined, undefined, undefined, false);

        gm.loadSavedMarkers(false, false, false, '#savedReceivedMarkers');
    }

}

leads.editInit = function ()
{

    $('#leadDate').datepicker({
        dateFormat:"yy-mm-dd"
    });

    $('#addAddressBtn').click(function(e){
        leads.addAddress();
    });

    $('#addPhoneBtn').click(function(e){
        leads.addPhone();
    });

    $('#addEmailBtn').click(function(e){
        leads.addEmail();
    });


    $('#saveBtn').click(function(e){
        leads.checkEditForm();
    });

    $('#cancelBtn').cancelButton('/leads');

    CKEDITOR.replace('details');


    leads.addSortable();
    leads.phoneSortable();
    leads.emailSortable();
}


leads.addEmail = function ()
{
    var html = '';

    html = $('#emailSortList').find('li').html();

    $('#emailSortList').append("<li>" + html  + "</li>");
    $('#emailSortList').find('input').last().effect(global.effect);

    $('#emailSortList input').last().val('');
    $('#emailSortList button').last().removeAttr('disabled');

    leads.emailSortable();

    leads.updateDelBtn('#emailSortList');
}


leads.addPhone = function ()
{
    var html = '';

    // html = $('#master-phone').html();
    html = $('#phoneSortList').find('li').html();

    $('#phoneSortList').append('<li>' + html + '</li>');
    $('#phoneSortList').find('select').last().effect(global.effect);
    $('#phoneSortList').find('input').last().effect(global.effect);


    $('#phoneSortList input').last().val('');
    $('#phoneSortList button').last().removeAttr('disabled');
    $('#phoneSortList select').last().val('');

    leads.phoneSortable();

    leads.updateDelBtn('#phoneSortList');

}

leads.addSortable = function ()
{
    $('#addSortList').sortable({
        stop: function (event, ui)
        {
            leads.updateDelBtn('#addSortList');
        }
    });
}

leads.emailSortable = function ()
{
    $('#emailSortList').sortable({
        stop: function (event, ui)
        {
            leads.updateDelBtn('#emailSortList');
        }
    });
}

leads.phoneSortable = function ()
{
    $('#phoneSortList').sortable({
        stop: function (event, ui)
        {
            leads.updateDelBtn('#phoneSortList');
        }
    });
}

leads.addAddress = function ()
{
    var html = '';

    // html = $('#master-address').html();
    html = $('#addSortList').find('li').html();

    $('#addSortList').append('<li>' + html + '</li>');

    $('#addSortList').find('.panel').last().effect('highlight');
    $('#addSortList input#address_description').last().val('');
    
    $('#addSortList textarea').last().val('');
    
    $('#addSortList input#address_line_1').last().val('');
    $('#addSortList input#address_line_2').last().val('');
    $('#addSortList input#address_line_3').last().val('');
    
    $('#addSortList input#city').last().val('');
    $('#addSortList input#postalCode').last().val('');

    $('#addSortList select').last().val('');
    // $('#address-container button').last().removeAttr('disabled');

    leads.addSortable();

    leads.updateDelBtn('#addSortList');
}


leads.updateDelBtn = function (div)
{

    $(div).find("button").each(function(index, item){

        if (index == 0)
        {
            $(item).attr('disabled', 'disabled');
        }
        else
        {
            $(item).removeAttr('disabled');
        }

    });

}

leads.deleteEmail = function (item)
{
    $(item).parent().parent().parent().parent().parent().hide('highlight', 500, function(){
        $(this).html('');

        leads.updateDelBtn('#emailSortList');
    });
}

leads.deletePhone = function (item)
{
    $(item).parent().parent().parent().parent().hide('highlight', 500, function(){
        $(this).html('');

        leads.updateDelBtn('#phoneSortList');

    }); //hides element first
}

leads.deleteAddress = function (item)
{
    $(item).parent().parent().hide('highlight', 500, function(){
        $(this).html('');

        leads.updateDelBtn('#phoneSortList');
    }); //hides element first
}



leads.checkEditForm = function ()
{
    /*
    if ($('#companyName').val() == '')
    {
        global.renderAlert("Please enter a company name!");
        $('#companyName').effect('highlight');
        $('#companyName').focus();
        return false;
    }
    */

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['details'].getData();
    $('#details').val(str);



    $('#saveBtn').attr('disabled', 'disabled');

    $.post("/leads/save", $('#leadForm').serialize(), function(data){
        
        if (data.status == 'SUCCESS')
        {
            window.location = '/leads/edit/' + data.id + '?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
            $('#saveBtn').removeAttr('disabled');
            return false;
        }


    },'json');
}

leads.edit = function (id)
{
    window.location = '/leads/edit/' + id;
}
