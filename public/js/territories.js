var territories = {}

territories.map;

territories.indexInit = function ()
{
	global.createDataTable('#territories');

	gm.initialize('map', undefined, undefined, undefined, true);
	gm.loadSavedMarkers(true, false, false);

	$(window).on('gmaps.api.loaded', function(e){
		//gm.postInit(true);
		
		
	});
}

territories.editInit = function ()
{
    $('#saveBtn').click(function(e){
        territories.checkEditForm();
    });

    $('#cancelBtn').click(function(e){
        if (confirm("Are you sure you wish to cancel?"))
        {
        	$(this).disableSpin();
        	redirect('/territories');
        }
    });

    territories.map = gm.initialize('map', $('#mapCenterLat').val(), $('#mapCenterLng').val(), $('#mapZoom').val());

    gm.loadSavedMarkers(true, true);

    var preloadColor = $('#googleMapCircleColor').val();

    // console.log('preload color: ' + preloadColor);

    $('#googleMapCircleColor').ColorPicker({
        onSubmit: function(hsb, hex, rbg, el){
            $('#googleMapCircleColor').css('background-color', '#' + hex);
            $('#googleMapCircleColor').attr('hex', hex);
            $('#googleMapCircleColor').val(hex);
        },
        onChange: function(hsbc, hex, rgb){
            $('#googleMapCircleColor').css('background-color', '#' + hex);
            $('#googleMapCircleColor').attr('hex', hex);
            $('#googleMapCircleColor').val(hex);
        }
    });

    if (preloadColor !== '')
    {
        $('#googleMapCircleColor').ColorPickerSetColor(preloadColor);
    }

    $('#googleMapCircleOpacitySlider').slider({
        value: 80,
        min: 1,
        max: 100,
        slide: function (event, ui){
            $('#opacityPercent').text(ui.value + '%');
            $('#googleMapCircleOpacity').val(ui.value);
        }
    });
}


territories.createInit = function ()
{
    $('#saveBtn').click(function(e){
        territories.checkCreateForm();
    });

    $('#cancelBtn').click(function(e){
        if (confirm("Are you sure you wish to cancel?"))
        {
        	$(this).disableSpin();
        	
            redirect("/territories");
        }
    });
}

territories.checkCreateForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert("Please enter a territory name!");
        $('#name').focus();
        return false;
    }

    if ($('#positionOwner :selected').val() == '')
    {
        global.renderAlert("Please select the position that will oversee this territory!");
        $('#positionOwner').focus();
        return false;
    }

    $('#saveBtn').attr('disabled', 'disabled');

    $.post("/territories/createterritory", $('#createForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/territories/edit/' + data.msg + '?site-success=' + escape("Territory has been created!");
            $('#cancelBtn').attr('disabled', 'disabled');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#saveBtn').removeAttr('disabled');
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-error');
            $('#saveBtn').removeAttr('disabled');
        }
    }, 'json');
}


territories.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
    	Warning('Please enter a territory name!');
        $('#name').focus();
        return false;
    }

    if ($('#positionOwner :selected').val() == '')
    {
    	Warning('Please select the position that will oversee this territory!');
        $('#positionOwner').focus();
        return false;
    }

    $('#saveBtn').disableSpin();

    $.post("/territories/save", $('#editForm').serialize() + '&' + $.param({ 'mapCenterLat': gm.map.getCenter().lat(), 'mapCenterLng': gm.map.getCenter().lng() , 'mapZoom':gm.map.getZoom() }), function(data){
        if (data.status == 'SUCCESS')
        {
            territories.saveMarkers();

			Success(data.msg);
        }
        else if (data.status == 'ALERT')
        {
        	Warning(data.msg);
        }
        else if (data.status == 'ERROR')
        {
        	Danger(data.msg);
        }

        $('#saveBtn').enableSpin();
        
    }, 'json');
}

territories.editTerritory = function (b)
{
	$(b).disableSpin();
	
	redirect('/territories/edit/' + $(b).data('id'));

	return false;
}

territories.saveMarkers = function ()
{

    // var markers = gm.markers.serializeArray();

    for (var i = 0; i < gm.markers.length; i++) 
    {
        if(gm.markers[i].getVisible() == true)
        {
            $.post("/territories/savemarker", { territory: $('#id').val(), lat:gm.markers[i].getPosition().lat(), lng:gm.markers[i].getPosition().lng(),radius:gm.circles[i].getRadius(), color: $('#googleMapCircleColor').val(), opacity: $('#googleMapCircleOpacity').val(), cgi_token:global.CSRF_hash } , function(data){
             if (data.status == 'ERROR')
                {
                	Danger(data.msg);
                    return false;
                }
            }, 'json');
        }

        /*
        console.log('lat['+i+']: ' + gm.markers[i].getPosition().lat());
        console.log('long['+i+']: ' + gm.markers[i].getPosition().lng());
        console.log('Visible['+i+']: ' + gm.markers[i].getVisible());
        console.log('radius['+i+']: ' + gm.circles[i].getRadius());
        */
    };
}

/*
territories.loadSavedMarkers = function (draggable, editable)
{

    if ($('#savedMarkers').exists())
    {
        $('#savedMarkers').find("input").each(function(index, item)
        {
            // console.log('rad: ' + $(item).attr('radius'));
            gm.addPoint(new google.maps.LatLng($(item).attr('lat'),$(item).attr('lng')), $(item).attr('radius'), draggable, editable);
        });
    }
}


territories.mapCenter = function ()
{

    console.log('CENTER: ' + gm.map.getCenter());
    console.log('CENTER LAT: ' + gm.map.getCenter().lat());
    console.log('CENTER LNG: ' + gm.map.getCenter().lng());
    console.log('ZOOM: ' + gm.map.getZoom());

}
*/


