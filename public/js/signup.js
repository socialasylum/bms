var signup = {}

signup.indexInit = function ()
{

    $('#submitBtn').click(function(e){
        signup.checkSignUpForm();
    });

    signup.moduleInit();

    $('#editPlanBtn').click(function(e){
        $(this).attr('disabled', 'disabled');
        window.location = "/pricing";
    });

    signup.calculate();


    $('#industry').change(function(){
        if ($('#industry :selected').val() == '0')
        {
            $('#otherIndustryFormGroup').show(global.showEffect);
        }
        else
        {
            $('#otherIndustry').val(''); // clears value
            $('#otherIndustryFormGroup').hide(global.hideEffect);
        }
    });
}

signup.checkSignUpForm = function ()
{
    if (location.hash == '#about')
    {
        if ($('#cname').val() == '')
        {
            global.renderAlert('Please enter the company name!');
            $('#cname').focus();
            return false;
        }

        if ($('#phone').val() == '')
        {
            global.renderAlert('Please enter a phone number!');
            $('#phone').focus();
            return false;
        }
        if ($('#emaildomain').val() == '')
        {
            global.renderAlert('Please enter your email domain!');
            $('#emaildomain').focus();
            return false;
        }
        if (!$('#terms').prop('checked'))
        {
            global.renderAlert('Please agree to the terms and conditions.');
            $('#terms').focus();
            return false;
        }
    }
    
    else if (location.hash == '#newusers')
    {
        if ($('#email').val() == '')
        {
            global.renderAlert('Please enter your new email address');
            $('#email').focus();
            return false;
        }
        
        if ($('#firstName').val() == '')
        {
            global.renderAlert('Please enter your first name');
            $('#firstName').focus();
            return false;
        }

        if ($('#lastName').val() == '')
        {
            global.renderAlert('Please enter your last name!');
            $('#lastName').focus();
            return false;
        }

        if ($('#password').val() == '')
        {
            global.renderAlert('Please create a password!');
            $('#password').focus();
            return false;
        }

        if ($('#password2').val() == '')
        {
            global.renderAlert('Please confirm your password!');
            $('#confirmPassword').focus();
            return false;
        }

        if ($('#password').val() !== $('#password2').val())
        {
            global.renderAlert('Your passwords did not match');
            // clears password fields
            $('#password').val('');
            $('#password2').val('');
            $('#password').focus();
            return false;
        }
    }

    else if (location.hash == '#payment')
    {
        if ($('#cardname').val() == '')
        {
            global.renderAlert('Please enter the card holders name.');
            $('#ccName').focus();
            return false;
        }

        if ($('#cardnum').val() == '')
        {
            global.renderAlert('Please enter the card number.');
            $('#ccNumber').focus();
            return false;
        }

        if ($('#ccexpm').val() == '' || $('#ccexpy').val() == '')
        {
            global.renderAlert('Please select the card expiration');
            $('#ccexpm').focus();
            return false;
        }

        if ($('#cvv').val() == '')
        {
            global.renderAlert('Please enter the card CVV number (usually on the back; amex on the front).');
            $('#cvv').focus();
            return false;
        }
    }

    else
    {
        $('#submitBtn').attr('disabled', 'disabled');
        $('#resetBtn').attr('disabled', 'disabled');

        $.post("/signup/createaccount", $('#signupForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/intranet/login?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                window.location = '/intranet/login?site-alert=' + escape(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-danger');
                // window.location = '/signup?site-error=' + escape(data.msg);

                $('#submitBtn').removeAttr('disabled');
                $('#resetBtn').removeAttr('disabled');
            }
        }, 'json');
    }
    
    return true;
};

signup.moduleInit = function()
{
    $('#well').load('home.html #moduleBio');

    $('#appPro').click(function(){
    getmodType("appPro");
    });

    $('#empOn').click(function(){
    getmodType("empOn");
    });

    $('#docMan').click(function(){
    getmodType("docMan");
    });

    $('#repSer').click(function(){
    getmodType("repSer");
    });

    $('#tecSup').click(function(){
    getmodType("tecSup");
    });

    $('#locMan').click(function(){
    getmodType("locMan");
    });

    $('#terDiv').click(function(){
    getmodType("terDiv");
    });

    $('#more').click(function(){
    getmodType("more");
    });


}


function getmodType(type)
{
    $.get("/signup/moduledivs/" + type, function(data){
    $('#moduleBio').html(data);	
    });
}


signup.calculate = function ()
{

    // first gets the number of users user has selected
    var users = $('#users').val();

    var total = 0;

    $('#mod-hidden-list').find("input").each(function(index, item)
    {
        total = total + parseFloat($(this).attr('ppu'));
    });


    total = total * parseInt(users) + 100;

    $('#totalDisplay').html(total.toFixed(2));
}

