var newhire = {}

newhire.indexInit = function ()
{
    newhire.renderDocTree();
}

newhire.settingsInit = function ()
{
    newhire.loadDays($('#days :selected').val());

    $('#days').change(function(e){
        newhire.savetdays();
        newhire.loadDays($("option:selected", this).val());
    });
}


/**
 * used on index page to render dyna tree of documents
 */
newhire.renderDocTree = function ()
{
    $.get("/newhire/doctree", function(data){
        $('#doc-tree').html(data);
        $('#doc-tree').dynatree({
            onClick: function(node, event)
            {

            }
        });

        global.slimScroll('#doc-tree');
    });
}




newhire.loadDays = function (days)
{

    if (days == undefined)
    {
        days = $('#days :selected').val();
    }

    global.ajaxLoader('#dayDisplay');

    $.get("/newhire/tdays/" + String(days), function(data){
        $('#dayDisplay').html(data);

        newhire.loadTypeahead();

        newhire.loadFileSortable();
    });
}

newhire.savetdays = function ()
{
    $.post('/newhire/updatedays', { days: $('#days :selected').val(), cgi_token: global.CSRF_hash }, function(data){

            if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
    }, 'json');
}


newhire.loadFileSortable = function ()
{
    $('#dayDisplay').find('ul').each(function(index, item){
        $(item).sortable({
            placeholder: "ui-state-highlight file-sortting",
            update: function (event, ui)
            {
                var data = $(this).sortable('serialize');

                newhire.updateDayOrder(data, $(this).attr('day'));
            }
        });
    });
}

newhire.updateDayOrder = function (data, day)
{
    $.post("/newhire/updateorder", data + "&" + $.param({ 'cgi_token': global.CSRF_hash, 'day' : day }), function(data){
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success', 'day' + day + 'alerts');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'day' + day + 'alerts');
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'day' + day + 'alerts');
            return false;
        }
    }, 'json');
}

newhire.loadTypeahead = function ()
{

    $('#dayDisplay').find("input").each(function(index, item){

        var day = $(item).attr('day');

        if ($(item).attr('type') == 'text')
        {

            // console.log($(item).attr('id'));


        $(item).autocomplete({
            source: "/newhire/docsearch",
            minLength: 3,
            select: function(event, ui)
            {
                newhire.addDocument(ui.item.id, $(this).attr('day'));

                // alert(JSON.stringify(ui));
                // $("#StoreID").val(ui.item.id);
                // store.getStoreData();
            }
        });

            /*
            $(item).typeahead({
                name: 'Documents Forms',
                prefetch: '/newhire/docsearch',
                limit:10,
                engine: Hogan
            });


            $(item).bind('typeahead:selected', function (obj, datum){
                alert(JSON.stringify(obj));
                alert(JSON.stringify(datum));
            });
            */
            
            /*
               $(item).typeahead({
                // source: is where it pulls the list of users to search from
                source: function(query, process)
                {
                        return $.getJSON("/newhire/docsearch", { query: query }, function(data){

                        // adds options to list
                        retur process(data.options);
                    });
                },
                // updater: once an item is chosen, executes function
                updater: function (item)
                {
                    newhire.addDocument(item, day);
                },
                minLength: 3
            });
            */
        }
    });
}

newhire.addDocument = function (id, day)
{
    $.post("/newhire/add", { id: id, day: day, cgi_token:global.CSRF_hash }, function(data){

            if (data.status == 'SUCCESS')
            {
                newhire.loadDays();
            }
            if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
    
    }, 'json');

}
