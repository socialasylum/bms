
var cms = {};

cms.animation = {};

cms.animation.duration = 500;

cms.tree;
cms.index = {};
cms.editor = {};
cms.file = {};

cms.uploader;

// array of extensions that are able to be opened
cms.openable = ['HTML', 'HTM', 'CSS', 'lESS', 'JS', 'TXT', 'HTACCESS'];

// array of extensions to load image viewer
cms.images = ['BMP', 'GIF', 'PNG', 'JPG', 'JPEG'];

cms.index.init = function ()
{
	cms.index.sitemap();
	cms.index.uploader();
	
	
	//global.ckeditor('content');

	return true;
};

cms.index.sitemap = function ()
{
	global.slimScroll('#sitemap');
	
	cms.tree = $('#sitemap').fancytree({
		click: function(e, o)
		{
			var dir = o.node.data.dir;
			
			if (!dir)
			{
				cms.file.open(o.node.data.local + o.node.data.file, o.node.data.extension);
			}
			
			//if (dir == undefined) return false;

			cms.index.setUploadPath(o.node.data.local, o.node.data.path);
		},
		expand: function ()
		{
			//global.backgroundStretch();
		},
		collapse: function ()
		{
			//global.backgroundStretch();
		}
	}).fancytree('getTree');;

	return true;
};


cms.index.reloadSitemap = function ()
{
	$.get('/cms/sitemap', function (html){
		
		$('#file-tree').remove();
		
		$('#sitemap').prepend(html);
		
		cms.tree.reload().done(function(){

		});
	});
	return true;
};

cms.index.setUploadPath = function (path, apath)
{
	$('#upload').data('path', path);

	cms.uploader.setParams({ path: path });

	$('.path-lbl-container').find('span').velocity('fadeOut', {
		duration:100,
		complete: function ()
		{
			$('.path-lbl-container').find('span').remove();

			if (path.length == 0) path = '&nbsp;';

			// now add new path spans
			var chunks = path.split('/');

			if (chunks == undefined) return false;

			
			for (var i = 0; i < chunks.length; i++)
			{
				var $span = $('<span>', { class: 'label label-info', style:"opacity:0;" });
				
				$span.html(chunks[i]);
				
				$('.path-lbl-container').append($span);
			}
			
			
			$('.path-lbl-container').find('span').velocity('fadeIn', { 
				duration:100, 
				complete:function(){ $('.path-lbl-container').find('span').removeAttr('style') } 
				});
			
		}
	});

	return true;
}

cms.index.uploader = function ()
{
	var url = $('#upload').data('path');

	cms.uploader = $("#upload").uploader({
		uploadUrl: '/cms/upload',
		dataType: 'json',
		extraParams: { path: $('#upload').data('path') },
		CSRF:
		{
			enabled: true,
			key: 'cgi_token', 
			val: global.CSRF_hash
		},
		successFunction: function (d, fd)
		{
			if (d.status == 'SUCCESS')
			{
				Success(d.msg);
				cms.index.reloadSitemap();			
			}
			else
			{
				Warning(d.msg);
			}

		},
		errorFunction: function (d, fd)
		{
			Danger(d.msg);
		}
	}).data('uploader');
};

cms.file.open = function (file, ext)
{

	if (cms.openable.indexOf(ext) >= 0)
	{
		cms.editor.web(file);
		cms.file.getFileContents(file);
	}
	
	// open image viewer
	if (cms.images.indexOf(ext) >= 0)
	{
		
	}	
};


cms.file.getFileContents = function (file, sf)
{

	$.post('/cms/open', { file:file, cgi_token: global.CSRF_hash }, function(html){

	if (html.isJSON())
	{
		var d = jQuery.parseJSON(html);
		
		if (d.status == 'ERROR') Danger(d.msg, undefined, { timeout:5 });
	}
	else
	{
		if (sf !== undefined && typeof sf == 'function') sf(html);
	}
	
	});	
}



/**
* HTML Web Editor
*/
cms.editor.web = function (file)
{
	
	cms.file.getFileContents(file, function(html){

		var html = $.parseHtml(html);

		$('#editor').velocity('slideUp', {
			duration: cms.animation.duration,
			complete:function ()
			{
				$(this).empty();
				
				$head = $('<textarea>', { class:'form-control', id:'head', name:'head', rows:4 });

				$editor = $('<textarea>', { class:'form-control', id:'content', name:'body', rows:7 });

				$head.val($(html.head).html());
				$editor.val($(html.body).html());
			
				$(this).append($head);
				$(this).append($editor);
				
				// creates ckeditor
				global.ckeditor($editor, {}, function (txtarea){
					
					$('#editor').velocity('slideDown', { delay:200, duration:cms.animation.duration });				
				});
			}
		});
			
	});
};

cms.editor.placeholder = function ()
{
	var html = $('#editorPlaceHolderHtml').html();
	
	$('#editor').velocity('slideUp', {
		duration: cms.animation.duration,
		complete: function ()
		{
			//cms.editor.unload();
			
			$(this).empty()
				.html(html)
				.velocity('slideDown', { duration:cms.animation.duration });
		}
	});	
};

//unload any resources, ckeditor etc
cms.editor.unload = function ()
{
};