var item = {}

item.tabCount = 0;

item.indexInit = function ()
{

    folders.folderInit('#item-container', 'item');

    item.setFileDraggable(false);

    $('#item-container').find("li").each(function(index, item)
    {
            // items
            if ($(item).attr('itemType') == '2')
            {
                // if they double click on a article
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');

                    redirect("/item/edit/" + $(item).attr('value') + '/' + $('#folder').val() + '/' + $('#rootFolder').val());

                    global.setCursorWaiting();
                });
            }
    });

	/*
    $.rightClickMenu({
        selector: '.item',
        src: '/item/rightclick/' + $(this).attr('value'),
        selected: function (item, menu, action)
        {
            if (action == 'EDIT')
            {
                window.location = '/item/edit/' + $(item).attr('itemID') + '/' + $('#folder').val()
            }

            if (action == 'DELETE')
            {
                // alert('delete!');
            }
            // alert($(item).attr('itemID'));
            // intranet.loadWidget($(item).attr('url'), $(item).attr('id') ,$(port).val());
        }
    });
	*/
    $('#createCatBtn').click(function(e){
        folders.createFolder();
    });
}

item.editInit = function ()
{

    $('#cancelBtn').cancelButton('/item');
    CKEDITOR.replace('description');

    $('#saveBtn').click(function(e){
        item.checkEditForm();
    });

    $('#addItemOption').click(function(e){
        item.addItemOption(this);
    });


    $('#uploadImgBtn').CGIUploader({
        uploaded: function(file)
        {
            $('#uploadedImgs').append("<input type='hidden' name='img[]' id='img' value=\"" + file + "\">");
            item.renderUploadedImgs();

            // console.log('FILE: ' + file);
        }
    });

    item.renderUploadedImgs();

    item.createDeleteDropZone();


    // $('.delete-drop-zone').hide();
}

item.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert('Please enter an item name!');
        $('#name').focus();
        $('#name').effect('highlight');
        return false;
    }


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);

    $.post("/item/save", $('#editForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            window.location = '/item/edit/' + data.id + '/' + $('#folder').val() + '?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
            $('#saveBtn').removeAttr('disabled');
            return false;
        }

    }, 'json');
}

item.addItemOption = function (i)
{

    item.tabCount++;

    $('ul#itemOptionsNav').children('li').last().prev().after("<li><a href='#optionTab" + String(item.tabCount) + "' data-toggle='tab'><i class='fa fa-cog'></i> <span id='optionTabLbl" + String(item.tabCount)  + "'>Option</span></a></li>");

    var html = "<div class='tab-pane' id='optionTab" + String(item.tabCount) + "'>" + $('#itemOption').html() + "</div>";


    // alert(html);

    $('#tab-content').append(html);


    $('#optionTab' + String(item.tabCount)).find('input#option').attr('value', item.tabCount);
    // $('#optionTab' + String(item.tabCount)).find('input#optionName').attr('id', 'optionName_' + item.tabCount);

                                                                              
    // $('#optionTab' + String(item.tabCount)).find('input#optionName').attr('name', 'optionName[' + item.tabCount + '][]');
    
    // var c = $('#optionTab' + String(item.tabCount)).find('input#optionName').attr('name');

    $('#optionTab' + String(item.tabCount)).find('input#optionName').attr('name', 'optionName[' + item.tabCount + ']');

    // sets value for optionCnt to so it knows which tab its working with
    $('#optionTab' + String(item.tabCount)).find('input#optionName').attr('optionCnt', item.tabCount);



    $('#optionTab' + String(item.tabCount)).find('input#optionName').change().keyup(function()
                                                                              {
                                                                                  var tc = $(this).attr('optionCnt');
                                                                                  // console.log(tc + ":" + $(this).val());
                                                                                  $('#optionTabLbl' + tc).text($(this).val());
                                                                              });



    $('#optionTab' + String(item.tabCount)).find('#optionChoice').attr('name', 'optionChoice[' + item.tabCount + '][]');

    $('#optionTab' + String(item.tabCount)).find('#type_0').last().attr('name', 'type[' + item.tabCount + ']');
    $('#optionTab' + String(item.tabCount)).find('#type_1').last().attr('name', 'type[' + item.tabCount + ']');
}

item.addOption = function (b)
{
    var html = '';

    html = $('#master-option').html();

    $(b).parent().parent().find('#options-container').append(html)
    $(b).parent().parent().find('#options-container').find('input').last().effect('highlight');
    $(b).parent().parent().find('#options-container').find('input').last().val('');

    $(b).parent().parent().find('#options-container').find('button').last().removeAttr('disabled');

    
    var tc = $(b).parent().parent().parent().find('input#option').attr('value');
    
    // $(b).parent().parent().parent().find('input#optionName').attr('name', 'optionName[' + tc + '][]');
    // $(b).parent().parent().parent().find('input#optionName').attr('id', 'optionName_' + tc);


    $(b).parent().parent().find('#options-container').find('input').attr('name', 'optionChoice[' + tc + '][]');

}


item.deleteOption = function (b)
{
    $(b).parent().parent().parent().parent().hide('highlight', 500, function(e){
        $(this).html('');
    });
}

item.renderUploadedImgs = function ()
{
    global.ajaxLoader('#uploadedImgDisplay');

    var data = $('#editForm').serialize();

    $.post("/item/renderimgs", $('#editForm').serialize(), function(data){
        $('#uploadedImgDisplay').html(data);
        
        item.setImgSort();
    });
}

item.setImgSort = function ()
{
    $('#imgSortList').sortable({
        revert: true,
        placeholder: 'yt-primary',
        start: function (event, ui)
        {
            $('.delete-drop-zone').show(global.showEffect);
        },
        stop: function (event, ui)
        {
            // users.updateYoutubeVideoOrder();
            $('.delete-drop-zone').hide(global.hideEffect);
        }
    });
}


item.setFileDraggable = function(clearPreviousSortable)
{
        if (clearPreviousSortable == undefined) clearPreviousSortable = false; // deafults to false

        if (clearPreviousSortable == true)
        {
            // removes previous functionality
            $('#fileList').sortable('destroy');
        }

        // goes through each li and for folders makes it so they are not disbled
        $('#fileList').find("li").each(function(index, item)
        {
            // folders
            if ($(item).attr('itemType') == '1')
            {
                $(item).removeClass('ui-state-disabled');
            }
        });

        $('#fileList').selectable({
            filter: "li",
            selected: function (event, ui)
            {
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }

                    $(ui.selected).draggable({
                    revert: true,
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#fileList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });

}


item.createDeleteDropZone = function()
{
    $('.delete-drop-zone').droppable({
        over: function (event, ui)
        {
            $(this).addClass('folder-highlight');
        },
        out: function (event, ui)
        {
            $(this).removeClass('folder-highlight');
        },
        drop: function (event, ui)
        {
            item.deleteImage(ui.draggable);
        }
    });
}

item.deleteImage = function (item)
{
    var fileName = $(item).attr('fileName');

    if (confirm("Are you sure you wish to delete this image?"))
    {
        $.post("/item/deleteitemimg", { item: $('#id').val(), fileName: fileName, cgi_token:global.CSRF_hash }, function(data){

            if (data.status == 'SUCCESS')
            {
                global.renderAlert(data.msg, 'alert-success');
                // item.deleteHiddenImgInput(fileName);
     
                var imgRemoved = false;

                $('#uploadedImgs').find("input").each(function(index, item)
                {
                    if ($(item).attr('type') == 'hidden')
                    {
                        // console.log('Checking Filename: ' + $(item).attr('value'));
                        // only checks hidden inputs
                        if ($(item).attr('value') == fileName)
                        {
                            // found the hidden input and will now remove
                            $(item).remove();
                            imgRemoved = true;
                            // return true;
                        }
                    }
                });


                // return true;
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                // return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                // return false;
            }

            item.renderUploadedImgs();

        }, 'json');
    }
}

item.deleteItem = function (b, id)
{
    $(b).attr('disabled', 'disabled');

    if (confirm("Are you sure you wish to delete this item?"))
    {
        $.post("/item/deleteitem", { id: id, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/item?site-success=' + escape(data.msg);
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                $(b).removeAttr('disabled');
            }
        }, 'json');
    }
    else
    {
        $(b).removeAttr('disabled');
    }
}

/*
item.deleteHiddenImgInput = function (fileName)
{
    var imgRemoved = false;

    $('#uploadedImgs').find("input").each(function(index, item)
    {
        if ($(item).attr('type') == 'hidden')
        {
            console.log('Checking Filename: ' + $(item).attr('value'));
            // only checks hidden inputs
            if ($(item).attr('value') == fileName)
            {
                // found the hidden input and will now remove
                $(item).remove();
                imgRemoved = true;
                return true;
            }
        }
    });

    if (imgRemoved == true)
    {
        item.renderUploadedImgs();
        return true;
    }

    return false;
}
*/
