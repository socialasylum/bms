var users = {}

users.uploader;

users.email = {};

users.cpwRunning = false;

users.email.tree;

users.email.testAjax;

users.indexInit = function ()
{
	$('select').selectpicker();

	global.createDataTable('#usersTable');

    $('#invUserBtn').click(function(e){
        users.inviteUser();
    });

	$('#invUserModal').on('shown.bs.modal', function (e) {

	});


    $('#invUserForm #email').autocomplete({
        source: "/user/autocomplete",
        minLength: 3,
        select: function(event, ui)
        {
            $('#addedUser').val(ui.item.id);
            $('#invUserBtn').removeAttr('disabled');
            // newhire.addDocument(ui.item.id, $(this).attr('day'));
        }
    });
    

}

users.createInit = function()
{
    $('#createBtn').click(function(e){
        users.checkCreateForm();
    });

    $('#cancelBtn').cancelButton('/user');

}

users.checkCreateForm = function()
{

    if ($('#firstName').val() == '')
    {
        global.renderAlert("Please enter a first name!");
        $('#firstName').focus();
        return;
    }

    if ($('#lastName').val() == '')
    {
        global.renderAlert("Please enter a last name!");
        $('#lastName').focus();
        return;
    }

    if ($('#position :selected').val() == '')
    {
        global.renderAlert("Please select a position!");
        $('#position').focus();
        return false;
    }

    if ($('#email').val() == '')
    {
        global.renderAlert("Please enter an email address!");
        $('#email').focus();
        return;
    }

    if ($('#password').val() == '')
    {
        global.renderAlert("Please enter a password!");
        $('#password').focus();
        return;
    }

    if ($('#confirmPassword').val() == '')
    {
        global.renderAlert("Please confirm the password!");
        $('#confirmPassword').focus();
        return;
    }

    if ($('#password').val() != $('#confirmPassword').val())
    {
        global.renderAlert("Passwords do not match!");
        $('#password').focus();
        return;
    }

    $('#createBtn').attr('disabled', 'disabled');

    $.post("/user/createNewUser", $('#createForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            redirect('/user');
        }
        else if (data.status == 'ALERT')
        {
        	Warning(data.msg);
        }
        else if (data.status == 'ERROR')
        {
        	Danger(data.msg);
        }
    }, 'json');
}


users.editUser = function(id)
{
    redirect('/user/edit/' + id);
}

users.editInit = function ()
{

	users.uploader = $("input[type='file']").uploader({
		uploadUrl: '/user/externalimgupload/' + $('#id').val(),
		CSRF:
		{
			enabled: true,
			key: 'cgi_token', 
			val: global.CSRF_hash
		},
		successFunction: function (d, fd)
		{
			$('#uploadedProfileImg').val(d.file);
			users.updateProfileImg(d.file);
		}
	}).data('uploader');

	$("#password").strength({
		strengthClass: 'form-control strength',
		strengthMeterClass: 'pw_strength_meter',
		strengthButtonClass: 'button_strength'
	});

	$('#password').on('strength.update', function(e, total, totalPer){
		var display = $('#confirmPWFG').css('display');
		var $cpw = $('#confirmPWFG div .pw_strength_meter');
		
		if (total > 1 && display == 'none' && users.cpwRunning == false)
		{
			if ($cpw.exists()) $cpw.remove();
			
			var m = $('#passwordFG div .pw_strength_meter').clone();
			
			$('#confirmPWFG div').append(m);
			
			users.cpwRunning = true;
			
			$('#confirmPWFG').velocity('slideDown', { complete:function(){
				users.cpwRunning = false;
			}});
		}
		else if (total <= 1 && display !== 'none' && users.cpwRunning == false)
		{
			users.cpwRunning = true;
			
			$('#confirmPWFG').velocity('slideUp', { complete:function(){
				
				$('#confirmPassword').val('');
				
				if ($cpw.exists()) $cpw.remove();
				
				users.cpwRunning = false;
			}});
		} 
	})
	.on('change keyup keydown', function(e){
		$('#confirmPassword').val('');
	});

	if ($('.album-container .nt-container').exists())
	{
		$('.album-container .nt-container').nailthumb({width:228,height:150});		
	}

    $('#saveBtn').click(function(e){
        users.checkEditForm();
    });

    $('#addYTBtn').click(function(e){
        users.checkYouTubeUrl();
    });

    if ($('#deleteUserBtn').exists())
    {
        $('#deleteUserBtn').click(function(e){
            users.deleteUser(this, $('#id').val(), $('#firstName').val() + ' ' + $('#lastName').val());
        });
    }

    $('#closeCardModelBtn').click(function(e){
    
        if (confirm("Are you sure you wish to cancel adding a new card?"))
        {
            $('#NCsubmitBtn').attr('disabled', 'disabled');

            $(this).attr('disabled', 'disabled');
            $(this).find('i').removeClass('fa-times');
            $(this).find('i').addClass('fa-spinner');
            $(this).find('i').addClass('fa-spin');


            $('#newCardModal').modal('hide');
        }
    });

    $('#newCardModal').on('hidden.bs.modal', function (e){

        // re-enables buttons
        $('#NCsubmitBtn').removeAttr('disabled');
        $('#closeCardModelBtn').removeAttr('disabled');

        $('#closeCardModelBtn').find('i').removeClass('fa-spinner');
        $('#closeCardModelBtn').find('i').removeClass('fa-spin');
        $('#closeCardModelBtn').find('i').addClass('fa-save');


        $('#NCsubmitBtn').find('i').removeClass('fa-spinner');
        $('#NCsubmitBtn').find('i').removeClass('fa-spin');
        $('#NCsubmitBtn').find('i').addClass('fa-save');
    });
	
	$('select').selectpicker();

    $('#loginAsBtn').click(function(e){
        users.loginAs();
    });

    $('#addCard').click(function(){
        var userid = $('#id').val();
        $('.modal-body #userid').val(userid);
        $('#newCardModal').modal('show');
    });

    $('#NCsubmitBtn').click(function(e){
        users.checkNewCardForm();
        // users.addCard();
    });

    if ($('#cancelSubBtn').exists())
    {
        $('#cancelSubBtn').click(function(e){
            users.cancelSubPrompt(this);
        });
    }

    if ($('#exemptBtn').exists())
    {
        $('#exemptBtn').click(function(e){
            users.toggleExempt();
        });
    }

    if ($('#createSubBtn').exists())
    {
        $('#createSubBtn').click(function(e){
            users.createSubscription(this);
        });
    }

    if ($('#companyAssignTbl').exists())
    {
        // $('#companyAssignTbl').dataTable({ "bStateSave": true });
    }


    if ($('#tranTbl').exists())
    {
        $('#tranTbl').dataTable({ "bStateSave": true });
	}

    if ($('#cardTbl').exists())
    {
        $('#cardTbl').dataTable({ "bStateSave": true });
    }

    if ($('#formTbl').exists())
    {
        $('#formTbl').dataTable({ "bStateSave": true });
    }

    if ($('#docTbl').exists())
    {
        $('#docTbl').dataTable({ "bStateSave": true });
    }

	global.ckeditor($('#bio'));
    //CKEDITOR.replace('bio');
	
	if ($('#masonry-container').exists())
	{
		$.requireJS(["/public/js/masonry.pkgd.min.js"], function (d){
			$('#masonry-container').masonry({
				columnWidth: 10,
				itemSelector: '.item'
			}).masonry('bindResize');
	
			//$('#userTabs li a').
			
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				e.target // activated tab
				e.relatedTarget // previous tab
				$('#masonry-container').masonry('layout');		
			});	
		});
		

	}
	// renders images in stream
	//users.renderStreamImgPreviews();

	//users.renderYoutubeVideos();

    
    // $('#userTabs li:eq(5) a').tab('show');
    $('#userTabs li:eq(' + $('#tab').val() + ') a').tab('show');

    $('#helpBtn').click(function(e){
        global.viewModKBArticle($('#module').val());
    });

    if ($('#completeBilling').exists())
    {
        if ($('#completeBilling').val() == '1')
        {
            users.toggleCompleteBillingmodal();
        }
    }
    
    if ($('#newAcctBtn').exists())
    {
	    $('#newAcctBtn').click(function(e){
		   users.email.add(this); 
	    });
    }
    
    users.tabChange();
    
    global.ckeditor($('#signature'));
};


users.tabChange = function ()
{
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		e.target // activated tab
		e.relatedTarget // previous tab


		if ($(e.target).attr('href') == '#tabEmail')
		{
			if ($('#emailDD').exists())
			{
				$('#emailDD').velocity('fadeIn');
			}
		}
		else
		{
			if ($('#emailDD').exists()) $('#emailDD').velocity('fadeOut');
			
			//if ($('#delAcctBtn').exists()) users.email.setDelBtn(undefined);
			
			$('#saveBtn').unbind('click');
		
			$('#saveBtn').click(function(e){
				users.checkEditForm();
			});

		}
	
	});	
}

users.cancelEdit = function (b)
{

	Confirm("Are you sure you wish to cancel?", function(e){		
		$('#cancelBtn').disableSpin();
		redirect('/user');
	});
	/*
	Dialog("Are you sure you wish to cancel?", {
		header: "Cancel",
		accept:{
			close: true,
			clickFunction: function(e)
			{
				$('#cancelBtn').disableSpin();
				
				redirect('/user');
			}
		},
		deny:{
			show: true,
			clickFunction: function(e)
			{

			}
		}
	});
	*/

}

users.checkEditForm = function (sf)
{
	var id = parseInt($('#id').val());
	var valid = $('#editForm').validateForm();
	if (!valid) return false;
    
    // forces tabs back to first tab
    $('#userTabs a:first').tab('show');
    var companyAssigned = false;

	$('#saveBtn').disableSpin();
	$('.subnav').disable();

    // updates textarea with latest content from CKeditor before saving
    //var str = CKEDITOR.instances['bio'].getData();
    //$('#bio').val(str);

	$.post('/user/update', $('#editForm').serialize(), function(data){
		
		if (data.status == 'SUCCESS')
		{
			if (sf !== undefined && typeof sf == 'function') sf(data);
			
			if (id !== undefined && id > 0)
			{
				//editing a user
				Success(data.msg);
				
				$('.subnav').enable();
				$('#saveBtn').enableSpin();
			}
			else
			{
				redirect("/user/edit/" + data.id + '?site-success=' + encodeURI('User has been created'));	
			}

		}
		else
		{
			Danger(data.msg);
			$('.subnav').enable();
			$('#saveBtn').enableSpin();
		}
	}, 'json');

	return true;
};

users.deleteUser = function (b, id, name)
{
	Confirm("Are you sure you wish to delete " + name + "?", function(e){		
		$('.subnav').disable();

    	$(b).disableSpin();


        $.post("/user/deleteuser", { id: id, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                redirect('/user?site-success=' + escape(data.msg));
                return true;

            }
            else if (data.status == 'ALERT')
            {
				Warning(data.msg);
				
				$('.subnav').enable();
				
				$(b).enableSpin();
				
                return false;
            }
            else if (data.status == 'ERROR')
            {
            	Danger(data.msg);
				
				$('.subnav').enable();
				
				$(b).enableSpin();
				
                return false;
            }
        }, 'json');
	});

	return true;
}

users.inviteUser = function ()
{
    if ($('#invUserForm #email').val() == '')
    {
        global.renderAlert("Please enter an e-mail address", undefined, 'invUserAlert');
        return false;
    }

    if ($('#invUserForm #position :selected').val() == '')
    {
        global.renderAlert('Please select a position', undefined, 'invUserAlert');
        return false;
    }

    if ($('#invUserForm #department :selected').val() == '')
    {
        global.renderAlert('Please select a department', undefined, 'invUserAlert');
        return false;
    }

    $('#invUserBtn').attr('disabled', 'disabled');

    $.post("/user/invite", $('#invUserForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                // global.renderAlert(data.msg, 'alert-success');

                // clears alert
                // global.renderAlert('', undefined, 'invUserAlert');

                // clears text from text input field
                // $('#invUserForm #email').val('');

                // hides modal
                // $('#invUserModal').modal('hide');

                redirect('/user?site-success=' + escape(data.msg));
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'invUserAlert');
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-danger', 'invUserAlert');
            }

    }, 'json');
}


users.checkYouTubeUrl = function ()
{
    if ($('#youTubeUrl').val() == '')
    {
        global.renderAlert("Please enter a url.");
        $('#youTubeUrl').effect(global.effect);
        $('#youTubeUrl').focus();
    }

    $.post("/user/addyoutubeurl", { user: $('#id').val(), url:$('#youTubeUrl').val(), cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                global.renderAlert(data.msg);

                $('#youTubeUrl').val('');
                $('#youTubeUrl').effect(global.effect);

                users.renderYoutubeVideos();

            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');
}

users.renderYoutubeVideos = function ()
{
	if ($('#YTVideoDisplay').exists())
	{
	    global.ajaxLoader('#YTVideoDisplay');
	
	    $.get("/user/youtubevideos/" + $('#id').val(), function(data){
	        $('#YTVideoDisplay').html(data);
	        users.ytVideoSort();
	    });
    }
}

/**
 * makes youtube videos sortable
 */
users.ytVideoSort = function ()
{
    $('#ytVideoSortList').sortable({
        placeholder: 'yt-primary',
        stop: function (event, ui)
        {
            users.updateYoutubeVideoOrder();
        }
    });
}


users.updateYoutubeVideoOrder = function ()
{
    var data = $('#ytVideoSortList').sortable('serialize');

    // alert(JSON.stringify(data));

    $.post("/user/saveyoutubeorder", data + "&" + $.param({ 'cgi_token': global.CSRF_hash }), function(data){
            if (data.status == 'SUCCESS')
            {
                users.renderYoutubeVideos();
            }
            if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');
}

users.deleteYTVideo = function (id)
{
    if (confirm("Are you sure you wish to delete this video?"))
    {
        $.post("/user/deleteytvideo", { id:id,cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                users.renderYoutubeVideos();
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
        }, 'json');
    }
}

users.loginAs = function ()
{
    if (confirm("Are you sure you wish login as this user?"))
    {
        $('#loginAsBtn').attr('disabled', 'disabled');

        redirect('/intranet/loginas/' + $('#id').val());
    }
}

users.getInvoice = function (transId, userid)
{
    global.ajaxLoader('#invoiceDisplay');

    $.get("/user/invoice/" + transId + '/' + userid, function(data){
        $('#invoiceDisplay').html(data);
    });
}

users.checkNewCardForm = function ()
{
    if ($('#ccName').val() == '')
    {
        global.renderAlert("Please enter the Name on Card.", undefined, 'newCardAlert');
        $('#ccName').effect(global.effect);
        $('#ccName').focus();
        return false;
    }

    if ($('#ccNumber').val() == '') 
    {
        global.renderAlert('Please enter a Card Number', undefined, 'newCardAlert');
        $('#ccNumber').effect(global.effect);
        $('#ccNumber').focus();
        return false;
    }

    if ($('#ccExpM :selected').val() == '')
    {
        global.renderAlert('Please select an Experation Month', undefined, 'newCardAlert');
        $('#ccExpM').effect(global.effect);
        $('#ccExpM').focus();
        return false;
    }
    
    if ($('#ccExpY :selected').val() == '')
    {
        global.renderAlert('Please select an Experation Year', undefined, 'newCardAlert');
        $('#ccExpY').effect(global.effect);
        $('#ccExpY').focus();
        return false;
    }

    if ($('#ccCvv').val() == '')
    {
       global.renderAlert('Please enter the card\'s CVV (usually found on back of card)', undefined, 'newCardAlert');
       $('#ccCvv').effect(global.effect);
       $('#ccCvv').focus();
        return false;
    }

    $('#NCsubmitBtn').attr('disabled', 'disabled');

    $.post("/user/saveNewCard", $('#newCardForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            users.refreshCardsOnFile($('#id').val());
            global.renderAlert(data.msg, 'alert-success');
            $('#newCardModal').modal('hide');

            $('#NCsubmitBtn').removeAttr('disabled');

        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'newCardAlert')
            $('#NCsubmitBtn').removeAttr('disabled');
            return false;
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger', 'newCardAlert');
            $('#NCsubmitBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert("No server response!", 'alert-danger', 'newCardAlert');
            $('#NCsubmitBtn').removeAttr('disabled');
            return false;
        }

    }, 'json');


}


users.refreshCardsOnFile = function (userid)
{
    global.ajaxLoader('#cardOnFileBody');

    $.get("/user/getcardsonfile/" + userid, function(data){
        $('#cardOnFileBody').html(data);

        if ($('#cardTbl').exists())
        {
            $('#cardTbl').dataTable({ "bStateSave": true });
        }
    });
}

/*
 * // old functions
 * 
users.addCard = function ()
{
    $('#NCsubmitBtn').attr('disabled', 'disabled');

    $.post("/user/saveNewCard", $('#newCardForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');
            $('#newCardModal').modal('hide');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');
        }

    }, 'json');

}
*/

users.resignDocument = function (user, doc, title)
{
    if (confirm("Are you sure you wish to clear all the signatures for " + title + " document?"))
    {
        $.post("/docs/setresign", { user: user, doc: doc, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                global.renderAlert(data.msg, 'alert-success');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-danger');
            }

        }, 'json');
    }
}

users.deleteCard = function (token, userid)
{
    if (confirm("Are you sure you want to delete this card?"))
    {
        redirect('/user/deleteCC/' + token + '/' + userid);
    }
}

users.cancelSubPrompt = function (b)
{
    if (confirm("Are you sure you wish to cancel your subscription?"))
    {
        $(b).attr('disabled');

        $.post("/user/cancelsub", { user: $('#id').val(), cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                // $('#createSubPanel').show(global.showEffect); // not quite working yet, user needs to refresh page to create new sub again
                $('#cancelSubDisplay').hide(global.hideEffect);

                global.renderAlert(data.msg, 'alert-success');
                $(b).removeAttr('disabled');
                return true;
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                $(b).removeAttr('disabled');
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                $(b).removeAttr('disabled');
                return false;
            }
        }, 'json');
    }
}

users.toggleExempt = function ()
{
    if ($('#exempt').val() == 1)
    {
        $('#exempt').val('0'); // sets back to non-exempt
        $('#exemptBtn').removeClass('btn-success');
        $('#exemptBtn').addClass('btn-danger');

        $('#exemptBtn').html("<i class='fa fa-ban'></i> Non-Exempt");
    }
    else
    {
        $('#exempt').val('1'); // sets to exempt

        $('#exemptBtn').addClass('btn-success');
        $('#exemptBtn').removeClass('btn-danger');

        $('#exemptBtn').html("<i class='fa fa-check'></i> Exempt");
    }
    
    $.post("/user/setbillingexempt", { exempt: $('#exempt').val(), user: $('#id').val(), cgi_token: global.CSRF_hash  }, function(data){
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');
            return true;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
            return false;
        }
    }, 'json');
}

/*
users.toggleCreateSubBtn = function ()
{
    var planChecked = false;

    if ($('#subscriptionTbl').exists())
    {
        $('#subscriptionTbl').find("input").each(function(index, item){
            if ($(item).prop('checked') == true)
            {
                planChecked = true;
                return true;
            }
        });
    }

    if (planChecked == true)
    {
        $('#createSubBtn').removeAttr('disabled');
    }
    else
    {
        $('#createSubBtn').attr('disabled', 'disabled');
    }

    users.toggleCardSelect();
}
*/
users.toggleCardSelect = function ()
{
    global.ajaxLoader('#cardDisplay');

    var subSelected = false;

    // finds which plan has been chosen
    if ($('#subscriptionTbl').exists())
    {
        $('#subscriptionTbl').find("input").each(function(index, item){
            if ($(item).prop('checked') == true)
            {
                $("#createPriceDisplay").html('$' + $(item).attr('planPrice'));

                subSelected = true;

                return true;
            }
        });
    }


    // finds which card has been chosen
    var cardSelected = false;


    if ($('#cardTbl').exists())
    {
        $('#cardTbl').find("input").each(function(index, item){
            if ($(this).prop('checked') == true)
            {
                $("#createHolderNameDisplay").html($(item).attr('ccName'));
                $("#createCardSelectDisplay").html($(item).attr('ccNumber'));
                $("#createCardExpDisplay").html($(item).attr('ccExp'));

                cardSelected = true;

                return true; // breaks loop
            }

        });
    }

    var html = $('#cardDataHtml').html();

    $('#cardDisplay').html(html);

    if (cardSelected == true && subSelected == true)
    {
        $('#createSubBtn').removeAttr('disabled');
    }
    else
    {
        $('#createSubBtn').attr('disabled', 'disabled');
    }
}

users.createSubscription = function (b)
{
    $(b).attr('disabled', 'disabled');

    var plan = '';
    var token = '';

    // finds which plan has been chosen
    if ($('#subscriptionTbl').exists())
    {
        $('#subscriptionTbl').find("input").each(function(index, item){
            if ($(item).prop('checked') == true)
            {
                $("#createPriceDisplay").html('$' + $(item).attr('planPrice'));
                plan = $(item).val();;
                return true;
            }
        });
    }

    // finds card token
    if ($('#cardTbl').exists())
    {
        $('#cardTbl').find("input").each(function(index, item){
            if ($(this).prop('checked') == true)
            {
                token = $(item).val();
                return true; // breaks loop
            }
        });
    }


    $.post("/user/createsubscription", { user: $('#id').val(), plan: plan, token: token, cgi_token: global.CSRF_hash  }, function(data){
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');
            $('#createSubPanel').hide(global.hideEffect);
            $('#cancelSubDisplay').show(global.showEffect);
            return true;
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $(b).removeAttr('disabled');
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');
            $(b).removeAttr('disabled');
        }
    }, 'json');
}


users.toggleCompleteBillingmodal = function ()
{
    // must first get content
    $.get("/user/completebilling", function(data){
        $('#completeBillingModal').html(data);

        $('#completeBillingModal').modal('show');
    });

}

users.completeFBReg = function (b)
{
    // first checks if a plan is selected

    var planChecked = false;

    $('#planTbl').find("input").each(function(index, item){

        if ($(item).prop('checked') == true)
        {
            planChecked = true;
            return true;
        }

    });

    if (planChecked == false)
    {
        global.renderAlert('Please select a plan!', undefined, 'billingAlert');
        return false;
    }

    if ($('#compBillingForm #ccName').val() == '')
    {
        $('#compBillingForm #ccName').focus();
        $('#compBillingForm #ccName').effect(global.effect);

        global.renderAlert('Please enter the name on the card!', undefined, 'billingAlert');
        return false;
    }


    if ($('#compBillingForm #ccNumber').val() == '')
    {
        $('#compBillingForm #ccNumber').focus();
        $('#compBillingForm #ccNumber').effect(global.effect);

        global.renderAlert('Please enter the number on the card!', undefined, 'billingAlert');
        return false;
    }

    if ($('#compBillingForm #ccExpM :selected').val() == '')
    {
        $('#compBillingForm #ccExpM').focus();
        $('#compBillingForm #ccExpM').effect(global.effect);

        global.renderAlert('Please select the month of when the card expires!', undefined, 'billingAlert');
        return false;
    }

    if ($('#compBillingForm #ccExpY :selected').val() == '')
    {
        $('#compBillingForm #ccExpY').focus();
        $('#compBillingForm #ccExpY').effect(global.effect);

        global.renderAlert('Please select the year of when the card expires!', undefined, 'billingAlert');
        return false;
    }

    if ($('#compBillingForm #ccCvv').val() == '')
    {
        $('#compBillingForm #ccCvv').focus();
        $('#compBillingForm #ccCvv').effect(global.effect);

        global.renderAlert('Please enter the cards CVV number!', undefined, 'billingAlert');
        return false;
    }


    if ($('#compBillingForm #agree').prop('checked') == false)
    {
        $('#compBillingForm #agree').focus();
        $('#compBillingForm #agree').effect(global.effect);

        global.renderAlert('You must agree to our sites terms of service, privacy policy, and Return &amp; Refunds Policy.', undefined, 'billingAlert');
        return false;
    }
    
    $(b).attr('disabled', 'disabled');
    $(b).find('i').removeClass('fa-arrow-right');
    $(b).find('i').addClass('fa-spinner');
    $(b).find('i').addClass('fa-spin');

    $.post("/user/savecompletedbilling", $('#compBillingForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            $('#cancelRegBtn').attr('disabled', 'disabled');
            
            if ($('#company').val() == 35)
            {
                redirect('http://productpricetracker.com/welcome/login?site-success=' + escape(data.msg));
            }
            else
            {
                redirect('/users/edit/' + $('#id').val() + '?site-success=' + escape(data.msg));
            }
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'billingAlert');

            $(b).removeAttr('disabled');
            $(b).find('i').addClass('fa-arrow-right');
            $(b).find('i').removeClass('fa-spinner');
            $(b).find('i').removeClass('fa-spin');
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'billingAlert');

            $(b).removeAttr('disabled');
            $(b).find('i').addClass('fa-arrow-right');
            $(b).find('i').removeClass('fa-spinner');
            $(b).find('i').removeClass('fa-spin');

        }
    }, 'json');
}


users.viewAlbum = function (album)
{
	$('body').css('overflow-x', 'hidden');

	$('#photos-display').show().css('opacity', 0);
	
	$('.album-container').velocity({ 'margin-left':'130%' }, {
		complete: function(){
			$('body').removeAttr('style');
				
			$(this).hide();
			
			$('#photos-display').velocity({ opacity:1 });
			
			global.ajaxLoader('#photos-display');
		
			$.get('/user/viewalbum/' + album + '/' + $('#id').val(), function(data){
				$('#photos-display').html(data);
			
				$('#album-photo-container').justifiedGallery({
					rowHeight: 250,
					margins:15
				});
			});
		}
	});
}

users.viewAlbums = function ()
{
	

	$('#photos-display').velocity({ opacity:0 }, {
		complete: function(){
			$(this).empty().hide();
			
			$('body').css('overflow-x', 'hidden');
			
			$('.album-container').show().velocity({ 'margin-left':'0' }, {
				complete: function(){
					$('body').removeAttr('style');	
				}
			});
		}
	});
	
}

users.renderStreamImgPreviews = function ()
{
	$('#masonry-container').find('.post-photo-container').each(function(index, item){
		$(item).justifiedGallery();
	});
}

users.email.renderTree = function ()
{
	users.email.tree = $('#email-tree').fancytree({
	    source: {
		    url: '/user/emailtree/' + $('#id').val(),
		    cache: false
	    },
        click: function(e, o)
        {
			users.email.loadSettinsgPage(o.node.data.page, o.node.key);
        }
    }).fancytree('getTree');	
    

	/*
	$('#email-tree').ajaxLoader();
	

	$.getJSON('/user/emailtree/' + $('#id').val(), function(d){
	
		
		

		$('#email-tree').ajaxLoader.clear();
	
		if (d.status == 'SUCCESS')
		{
			var tree = $.parseJSON(d.tree);
			
			if (tree.length <= 0)
			{
				$('#email-tree').Warning("No email accounts have been setup.", 'No External E-mail', { clearTimeoutSeconds:0, zIndex:1 });
				
				users.email.add();
				
				return false;
			}

			users.email.tree = $('#email-tree').fancytree({
			    source: tree,
		        click: function(e, o)
		        {
					users.email.loadSettinsgPage(o.node.data.page, o.node.key);
		        }
		    }).fancytree('getTree');	

			
			
			
		}
		else
		{
			Danger(d.msg);
		}

	});
	*/
	
	return true;
}



users.email.add = function (b)
{
	$('#saveBtn').unbind('click');

	$('#saveBtn').click(function(e){
		users.email.save_new();
	});
	
	users.email.setDelBtn(undefined);
	
	$('#testAcctBtn')
		.unbind('click')
		.click(function(){

			users.email.test_new();
		});
	
	
		var $div = $('#email-edit-display');
		
		$div.ajaxLoader();

		$.get('/user/email_new', function (html){
			
			$div.ajaxLoader.clear();
			$div.find('select').selectpicker();	
			$div.append(html);

			$div.find('select').selectpicker();
		});		

	
}

users.email.loadSettinsgPage = function (page, id)
{

	var $div = $('#email-edit-display');

	$('#saveBtn').unbind('click');

	$div.ajaxLoader();
	
	$.get('/user/email_settings/' + $('#id').val() + '/' + id + '/' + encodeURI(page), function(html){
		
		$div.ajaxLoader.clear(function(){
			$div.prepend(html);

		
			$div.find('select').selectpicker();
			
			if (page == 'email_outgoing_servers')
			{
				$('#saveBtn').click(function(e){
					Warning("Please select an SMTP Server");
				});
	
				$('#outgoingServer').change(function(e){
					users.email.showOutgoingDetails(this);
					
				});
			}
			else if (page == 'email_sig')
			{
				$('#saveBtn').click(function(e){
					users.email.updateSignature();
				});
				
				global.ckeditor($('#signature'));
			}
			else
			{
	
				$('#testAcctBtn')
					.unbind('click')
					.click(function(){
						users.email.test($('#username').val(), undefined, $('#mailbox').val(), $('#port').val(), $('#type').val(), $('#ssl').val(), $('#auth').val(), id);
					});
	
				users.email.setDelBtn(id);
				
				$('#email-edit-display #passwd').trackChange();
	
				$('#saveBtn').click(function(e){
					users.email.save_settings();
				});
			}
		
		});
	});

	
}

/**
* saves pages settings for an account
*/
users.email.save_settings = function ()
{
	if (!$('#email-edit-display').exists()) return false;
	
	var valid = $('#email-edit-display').validateForm();
	
	if (!valid) return false;
	
	
	$('#email-edit-display select').selectpicker('refresh');

	var data = $('<form>').append($('#email-edit-display').clone()).serialize();

	if ($('#type').exists()) data = $.addParam(data, 'type', $('#type').val());
	if ($('#outgoingServer').exists()) data = $.addParam(data, 'outgoingServer', $('#outgoingServer').val());
	if ($('#ssl').exists()) data = $.addParam(data, 'ssl', $('#ssl').val());
	if ($('#auth').exists()) data = $.addParam(data, 'auth', $('#auth').val());
	
	if ($('#email-edit-display #passwd').exists())
	{
		var changed = $('#email-edit-display #passwd').data('changed');
		
		if (changed == undefined) changed = false; 
		
		// removes passwd from data to be saved
		if (!changed) data = $.addParam(data, 'passwd', null);
	}
		
	data = $.addParam(data, 'cgi_token', global.CSRF_hash );
	
	//if ($('#auth').exists()) data += '&'
	
	clog(data);
	
	
	$('#saveBtn').disableSpin();
	
	$.post('/msg/update_account_settings', data, function(data){

		$('#saveBtn').enableSpin();
			
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
		}
		else
		{
			Danger(data.msg);
		}
	}, 'json');
}

users.email.showOutgoingDetails = function (sel)
{
	$('#testAcctBtn').unbind('click');
	
	if ($(sel).val() == '')
	{				
		$('#outgoing-settings-display').velocity('fadeOut');
	}
	else
	{

		$('#saveBtn').unbind('click').click(function(e){
			users.email.save_settings();
		});
		
	


		users.email.info($(sel).val(), function(d){
			//$('#outgoing-settings-display').ajaxLoader.clear();
		
			users.email.setDelBtn(d.info.id);
				
			
			$('#SMTPAccountID').val(d.info.id);
			
			$('#ssl').selectpicker('val', d.info.ssl);
			$('#auth').selectpicker('val', d.info.auth);
			
			$('#alias').val(d.info.alias);
			$('#mailbox').val(d.info.mailbox);
			$('#port').val(d.info.port);
			
			$('#username').val(d.info.username);

			if (d.info.passwd.length > 0)
			{
				$('#passwd').val(d.info.passwd);
				
				$('#remPasswd').prop('checked', true);
			}
			else
			{
				$('#passwd').val('');
				$('#remPasswd').prop('checked', false);
			}
				
			
			$('#email-edit-display #passwd').trackChange();
			$('#email-edit-display #mailbox').trackChange();
				
			$('#outgoing-settings-display').velocity('fadeIn', { duration: 200, complete:function(){
				bs.resize();
				//	global.backgroundStretch();
			} });
			
			// sets test btn
			$('#testAcctBtn').click(function(){
				users.email.test($('#username').val(), undefined, $('#mailbox').val(), $('#port').val(), 3, $('#ssl').val(), $('#auth').val());
			});
			

		});
	}
	
	return true;
}

users.email.info = function (id, sf)
{
	$.ajax({
		url: '/user/email_info/' + $('#id').val() + '/' + id,
		dataType: 'json',
		method: 'get',
		success: function (data)
		{
			if (data.status == 'SUCCESS') if (sf !== undefined && typeof sf == 'function') sf(data);
			else Danger(data.msg);
		},
		error: function (data)
		{
			Danger('There was an error retreiving the data');
		}
	});

	
	return true;
}

users.email.save_new = function ()
{
	var valid = $('#email-edit-display').validateForm();

	if (!valid) return false;

	$('#saveBtn').disableSpin();
	$('.subnav').disable();

	$.post('/msg/save_new_account', $('#editForm').serialize(), function(data){
		
		$('#saveBtn').enableSpin();
		$('.subnav').enable();
		
		if (data.status == 'SUCCESS')
		{
			$('#email-edit-display').ajaxLoader();
		
			if (users.email.tree !== undefined)
			{
				users.email.tree.reload().done(function(){
					$('#email-edit-display').html('');
				});				
			}
			else
			{
				// if tree has not been created, renders it
				users.email.renderTree();
			}
		}
	}, 'json');

	
	
	return true;	
}

users.email.updateSignature = function ()
{
	$('#saveBtn').disableSpin();
	$('.subnav').disable();
	
	$.ajax({
		url: '/user/update_email_signature',
		data: { userid: $('#id').val(), signature: $('#signature').val(), id:$('#signatureID').val(), cgi_token: global.CSRF_hash },
		method: 'post',
		dataType: 'json',
		success: function (data)
		{
			if (data.status == 'SUCCESS')
			{
				Success(data.msg);
			}
			else
			{
				Warning(data.msg);
			}
		},
		error: function (data)
		{
			Danger(data.msg);
		},
		complete: function ()
		{
			$('#saveBtn').enableSpin();
			$('.subnav').enable();
		}
	});
}

/**
* test a new account
*/
users.email.test_new = function ()
{
	users.email.testAjax = users.email.test($('#username').val(), $('#passwd').val(), $('#incomingHost').val(), $('#incomingPort').val(), $('#type').val(), $('#incomingSSL').val(), $('#incomingAuth').val(), 0, function(data){
		if (data.status == 'SUCCESS')
		{
			users.email.testAjax = users.email.test($('#username').val(), $('#passwd').val(), $('#outgoingHost').val(), $('#outgoingPort').val(), 3, $('#outgoingSSL').val(), $('#outgoingAuth').val(), 0, function(data){
				if (data.status == 'SUCCESS')
				{
					Confirm("Connection Test was successful, would you like to save this account?", function(){
						users.email.save_new();
					});

				}
			});
		}
	});
}


users.email.test = function (username, passwd, server, port, type, ssl, auth, account, sf)
{
	var valid = $('#email-edit-display').validateForm();
	
	if (!valid) return false;
	
	if (account == undefined) account = 0;
	
	$('#testAcctBtn').disableSpin({ spinClass:'fa fa-spin fa-notch' });
	
	var typeDisplay = (type == 3) ? 'Outgoing' : 'Incoming';
	
	var infoAlert = Info("Testing " + typeDisplay + " Settings", "E-mail Test", { clearTimeoutSeconds:0 });
	
	var data = {
		userid: $('#id').val(),
		username: username,
		server: server,
		port: port,
		type:type,
		ssl:ssl,
		auth:auth,
		account: account,
		cgi_token: global.CSRF_hash
		};
	
	// adds password
	if (passwd !== undefined && passwd.length > 0) data = $.addParam(data, 'passwd', passwd);


	$.post('/msg/test', data, function(data){
		
		clearAlert(infoAlert);
		
		$('#testAcctBtn').enableSpin();

		if (data.status == 'SUCCESS')
		{	
			Success(data.msg);
		}
		else
		{
			Danger(data.msg, "Test Failed", { clearTimeoutSeconds: 10 });
		}
		
		
		if (sf !== undefined && typeof sf == 'function') sf(data);
		
	}, 'json');
}

users.email.setDelBtn = function (id)
{
	$('#delAcctBtn').unbind('click');
	
	if (id == undefined)
	{
		$('#delAcctBtn').attr('disabled', 'disabled');
		return false;
	}
	
	// undisables button if it was
	if ($('#delAcctBtn').attr('disabled') == 'disabled') $('#delAcctBtn').removeAttr('disabled');
	
	// sets click
	$('#delAcctBtn').click(function(e){
		users.email.delete(id);
	});
	
	return true;
}

/**
* deleteing an email account
*/ 
users.email.delete = function (id)
{
	Confirm("Are you sure you wish to delete this account?", function(e){
	
		$('#delAcctBtn').disableSpin();
		
		$.post('/msg/delete_account', { userid: $('#id').val(), id: id, cgi_token: global.CSRF_hash }, function(data){
			$('#delAcctBtn').enableSpin();
			
			if (data.status == 'SUCCESS')
			{
				Success(data.msg);
				
				if (users.email.tree !== undefined)
				{
					users.email.tree.reload().done(function(){
						$('#email-edit-display').velocity('slideUp',{ 
							complete: function ()
							{
								$('#email-edit-display').empty().show();
							}
						});
					});					
				}

			}
			else
			{
				Danger(data.msg);
			}
			
		}, 'json');	

	});
}


// updates profile img when a new image is uploaded, goes by filename rather than userid
users.updateProfileImg = function (file, size)
{
	if (size == undefined) size = 400;
	
	var $curImg = $('#profilePicContainer img');
	
	$img = $("<img>", { class:'img-responsive img-thumbnail', src:'/user/profileimg/' + size + '/0/' + encodeURI(file), style:'opacity:0;display:none' });
	
	$('#profilePicContainer').append($img);
	
	$curImg.velocity('fadeOut', {
		complete:function ()
		{
			$(this).remove();
			
			$img.velocity('fadeIn');
		}
	});
	
	return true;
}

