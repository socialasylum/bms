var cal = {}

cal.indexInit = function ()
{
	$('#createEventBtn').removeAttr('disabled');

	$('#createEventBtn').click(function(e){
		
		redirect('/calendar/edit/' + $('#month').val() + '/' + $('#year').val());
		
		$('#createEventBtn').disableSpin();
	});


	$(window).resize(function(){
        cal.setEventWidth();
    });
    
	cal.setEventWidth();
}


cal.editInit = function ()
{
	$('select').selectpicker();
	
	CKEDITOR.replace('description', {
		toolbar: 'Basic'
	});

	$('#startDate, #endDate').datepicker({
		dateFormat: 'yy-mm-dd'
	});
	
	$('#startTime, #endTime').timeEntry();
	
	$('#eventLocation').change(function(){
		if ($(this).val() == 'OTHER')
		{
			$('#otherLocRow').show('highlight');
		}
		else
		{
			$('#otherLocRow').hide('highlight');
		}
	});

	$('#videoUploadBtn').click(function(e){
		cal.addVideoUpload();
	});
	
	$('#photoUploadBtn, #fileUploadBtn').click(function(e){
		cal.addFileUpload();
	});

	// ensures buttons are not disabled on page load
	$('#cancelBtn').removeAttr('disabled');
	$('#saveBtn').removeAttr('disabled');

	$('#saveBtn').click(function(e){
		cal.checkEventForm();
	});
	
	$('#cancelBtn').click(function(e){
		if (confirm("Are you sure you wish to cancel?"))
		{
			
			$('#saveBtn').attr('disabled', 'disabled');
			
			$(this).disableSpin();

			redirect('/calendar/index/' + $('#month').val() + '/' + $('#year').val());
		}
	});

	$('#repeatEvent').click(function(e){
		if ($(this).prop('checked') == true)
		{
			$('#repeatEventModal').modal('show');
		}
		else
		{
			
		}
		
		cal.setRepeatCheck();
	});
	
	//modal init stuff
	$('#startsOn').datepicker({
		dateFormat: 'yy-mm-dd'
	});

	$('#repeatCancelBtn').click(function(e){
		$('#repeatEventModal').modal('hide');
		$('#repeatEvent').removeAttr('checked');
		
		cal.setRepeatCheck();
	});
	
	$('#repeatDoneBtn').click(function(e){
		cal.checkRepeatModal();
	});
	
	$('#repeatType').change(function(e){
		cal.changeRepeatTypeTxt($('option:selected', this).val());
	});
	
	$('#occurrences').focus(function(){
		$('#ends_2').attr('checked', 'checked');
	});
	
	$('#endsOnDate').datepicker({
		dateFormat: 'yy-mm-dd'
	});
	
	$('#endsOnDate').focus(function(){
		$('#ends_3').attr('checked', 'checked');
	});
	
	if ($('#deleteBtn').exists())
	{
		$('#deleteBtn').click(function(e){
			cal.deleteEvent();
		});	
	}

	$('#allDay').click(function(e){
		cal.allDayEvent();
	});

    var preloadColor = $('#color').val();

    // console.log('preload color: ' + preloadColor);

    $('#color').ColorPicker({
        onSubmit: function(hsb, hex, rbg, el){
            $('#color').css('background-color', '#' + hex);
            $('#color').attr('hex', hex);
            $('#color').val(hex);
        },
        onChange: function(hsbc, hex, rgb){
            $('#color').css('background-color', '#' + hex);
            $('#color').attr('hex', hex);
            $('#color').val(hex);
        }
    });


    if (preloadColor !== '')
    {
        $('#color').ColorPickerSetColor(preloadColor);
    }
    
    cal.changeRepeatTypeTxt();
	cal.setRepeatCheck();
}

cal.changeRepeatTypeTxt = function (repeatType)
{
	if (repeatType == undefined) repeatType = $('#repeatType :selected').val();

	if (repeatType == 1)
	{
		$('#repeatTypeTxt').text('Days');
	}
	else if (repeatType == 2)
	{
		$('#repeatTypeTxt').text('Weeks');
	}
	else if (repeatType == 3)
	{
		$('#repeatTypeTxt').text('Months');
	}
	else
	{
		$('#repeatTypeTxt').text('Years');
	}
}

cal.addVideoUpload = function ()
{
	var html = $('#videoUploadHtml').html();

	$('#uploadContainer').append(html);
}

cal.removeVideoUpload = function (b)
{
	$(b).parent().parent().parent().hide('highlight', function(){
		$(this).html('');
	});
}

cal.addFileUpload = function ()
{
	var html = $('#fileUploadHtml').html();

	$('#uploadContainer').append(html);
}

cal.removeFileUpload = function (b)
{
	$(b).parent().parent().parent().hide('highlight', function(){
		$(this).html('');
	});
}


cal.removeFileUploaded = function (b, event, id)
{
	if (confirm("Are you sure wish to delete this file?"))
	{
		$(b).attr('disabled', 'disabled');
	
		$.post('/calendar/deletefile', { id: id, event:event, cgi_token: global.CSRF_hash }, function(data){
			if (data.status == 'SUCCESS')
			{
				cal.removeFileUpload(b);
			}
			else
			{
				global.renderAlert("There was an error trying to delete the file!<br><br>" + data.msg, 'alert-error');
				
				$(b).removeAttr('disabled');
				
				return false;
			}
		}, 'json');
	}
}

cal.removeVieoUploaded = function (b, id)
{
	if (confirm("Are you sure wish to delete this file?"))
	{
		$(b).attr('disabled', 'disabled');
	
		$.post('/calendar/deletevideo', { id:id, cgi_token: global.CSRF_hash }, function(data){
			if (data.status == 'SUCCESS')
			{
				cal.removeVideoUpload(b);
			}
			else
			{
				global.renderAlert("There was an error trying to delete the video!<br><br>" + data.msg, 'alert-error');
				
				$(b).removeAttr('disabled');
				
				return false;
			}
		}, 'json');
	}
}


cal.checkRepeatModal = function ()
{
	if ($('#startsOn').val() == '')
	{
		$('#startsOn').focus();
		$('#startsOn').effect('highlight');
		global.renderAlert("Please enter a day in which this event will start on!", undefined, 'repeatModalAlert');
		return false;
	}
	
	if ($('#ends_2').prop('checked') == true && $('#occurrences').val() == '')
	{
		$('#occurrences').focus();
		$('#occurrences').effect('highlight');
		global.renderAlert("Please enter how many occurrences this event has!", undefined, 'repeatModalAlert');
		return false;
	}

	if ($('#ends_3').prop('checked') == true && $('#endsOnDate').val() == '')
	{
		$('#endsOnDate').focus();
		$('#endsOnDate').effect('highlight');
		global.renderAlert("Please enter which day this event will end on!", undefined, 'repeatModalAlert');
		return false;
	}

	$('#repeatEventModal').modal('hide');
	$('#repeatEvent').attr('checked', 'checked');
	
	//events.setRepeatCheck();
}

cal.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert("Please enter a name for the event!");
        $('#name').effect(global.effect);
        $('#name').focus();
        return false;
    }


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);

    $('#saveBtn').attr('disabled', 'disabled');

    $.post("/calendar/saveevent", $('#editForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
        	redirect('/calendar/index/' + $('#month').val() + '/'  + $('#year').val() + '?site-success=' + escape(data.msg));
        }
        else if (data.status == 'ALERT')
        {
         	Warning(data.msg);
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
        else
        {
        	Danger(data.msg);

            $('#saveBtn').removeAttr('disabled');
            return false;
        }



    }, 'json');
}

cal.allDayEvent = function ()
{
	if ($('#allDay').prop('checked') == true)
	{
		$('#startTime').attr('disabled', 'disabled');
		$('#endTime').attr('disabled', 'disabled');
		
		// clears values
		$('#startTime').val('');
		$('#endTime').val('');
	}
	else
	{
		$('#startTime').removeAttr('disabled');
		$('#endTime').removeAttr('disabled');
	}
}

cal.showRepeatModal = function ()
{
	$('#repeatEventModal').modal('show');
	cal.changeRepeatTypeTxt();
}

cal.setRepeatCheck = function ()
{
	if ($('#repeatEvent').prop('checked') == true)
	{
		$('#repeatCheck').html("Repeat [<a href='javascript:cal.showRepeatModal();'>Edit</a>]");
	}
	else
	{
		$('#repeatCheck').html("Repeat...");
	}
}

cal.checkEventForm = function ()
{
	if ($('#title').val() == '')
	{
		$('#title').focus();
		$('#title').effect('highlight');
		global.renderAlert("Please enter an event title!");
		return false;
	}
	
	if ($('#startDate').val() == '')
	{
		$('#startDate').focus();
		$('#startDate').effect('highlight');
		global.renderAlert("Please enter a start date for this event!");
		return false;
	}
	
	if ($('#allDay').prop('checked') == true)
	{
		
	}
	else
	{
		if ($('#startTime').val() == '')
		{
			$('#startTime').focus();
			$('#startTime').effect('highlight');
			global.renderAlert("Please enter a start time for this event!");
			return false;
		}

		if ($('#endTime').val() == '')
		{
			$('#endTime').focus();
			$('#endTime').effect('highlight');
			global.renderAlert("Please enter a end time for this event!");
			return false;
		}
	}
	
	$('#saveBtn').attr('disabled', 'disabled');
	$('#saveBtn i').removeClass('fa-save');
	
	$('#saveBtn i').addClass('fa-spin');
	$('#saveBtn i').addClass('fa-spinner');
	
	$('#cancelBtn').attr('disabled', 'disabled');
	
	$('#repeatEventForm').submit();
}

cal.setTimeDisabled = function (disabled)
{
    if (disabled == undefined) disabled = true;

    if (disabled == true)
    {
        $('#fromHour').attr('disabled', 'disabled');
        $('#fromMin').attr('disabled', 'disabled');
        $('#fromAP').attr('disabled', 'disabled');

        $('#toHour').attr('disabled', 'disabled');
        $('#toMin').attr('disabled', 'disabled');
        $('#toAP').attr('disabled', 'disabled');
    }
    else
    {
        $('#fromHour').removeAttr('disabled');
        $('#fromMin').removeAttr('disabled');
        $('#fromAP').removeAttr('disabled');

        $('#toHour').removeAttr('disabled');
        $('#toMin').removeAttr('disabled');
        $('#toAP').removeAttr('disabled');
    }
}

cal.loadEventModal = function (id)
{
    $.get("/calendar/eventmodal/" + id + '/' + $('#month').val() + '/' + $('#year').val(), function(data){
        $('#eventModal').html(data);
        $('#eventModal').modal('show');

        var lat = $('#eventModal #lat').val();
        var lng = $('#eventModal #lng').val();

        gm.initialize('eventMap', lat, lng, 14, false);

        var location = new google.maps.LatLng(lat, lng);

        gm.addPoint(location, false);
    });
}


cal.deleteEvent = function ()
{
	if (!confirm("Are you sure you wish to delete this event?"))
	{
		return false;
	}
	
	$('#deleteBtn').attr('disabled', 'disabled');
	$('#saveBtn').attr('disabled', 'disabled');
	$('#cancelBtn').attr('disabled', 'disabled');
	
	$('#deleteBtn i').removeClass('fa-trash-o');
	$('#deleteBtn i').addClass('fa-spin');
	$('#deleteBtn i').addClass('fa-spinner');
	
	
	$.post('/calendar/deleteevent', { id: $('#id').val(), cgi_token:global.CSRF_hash  }, function(data){
			if (data.status == 'SUCCESS')
			{
				redirect('/events/index/' + $('#month').val() + '/' + $('#year').val());
			}
			else
			{
				global.renderAlert("There was an error trying to delete the event!<br><br>" + data.msg, 'alert-error');
				
				$('#deleteBtn').removeAttr('disabled');
				$('#saveBtn').removeAttr('disabled');
				$('#saveBtn').removeAttr('disabled');
				
				$('#deleteBtn i').addClass('fa-trash-o');
				$('#deleteBtn i').removeClass('fa-spin');
				$('#deleteBtn i').removeClass('fa-spinner');
				
				return false;
			}
	}, 'json');
}

cal.setEventWidth = function ()
{
    // gets width of dayEvent
    var w = $('.day-container').width();

    //console.log("W:" + w);

    // checks companies
    $('#calendar').find("label").each(function(index, item)
    {
        var days = parseInt($(item).attr('days'));

        //console.log("Days: " + days);
        if (days < 2)
        {
            $(item).width(w - 15);
        }
        else
        {
            //days = days + 1;

            var wdr = parseInt($(item).attr('weekDaysRemaining')) + 1;

            if (wdr < days)
            {
                $(item).width((w * wdr));
            }
            else
            {
                $(item).width((w * days));
            }
        }
    });

	cal.setStaggered();
}

cal.setStaggered = function ()
{
	var cnt = 1;
	var dow = 0;
	var bufferDays = 0; // holds how many top-margin is required


    // goes through each day and checks the events
    $('#calendar').find(".day-container").each(function(index, item)
    {
    	//console.log("Cnt: " + cnt + " | Dow: " + dow);
    
		var eventCnt = 0;
		
		// before labels checked
		// applys top margin
		if (bufferDays > 0)
		{
			//console.log('ADD BUFFER');
			var mtop = bufferDays * 25;
			$(item).find('.eventContainer').css('margin-top', mtop + 'px');
		}
		
		// will now go through each even int that day to determine buffering
		$(item).find('.label').each(function(subIndex, calEvent){
			eventCnt++;
			
			var days = parseInt($(calEvent).attr('days'));
			var wdr = parseInt($(calEvent).attr('weekdaysremaining'));
			
			if (days >= 2)
			{
				bufferDays += days;
				
			}
			
			//console.log("event!" + eventCnt + "| bufferDays: " + bufferDays);
		});


		if (bufferDays > 0)
		{
			bufferDays -= 1;
		}
			
    
    	cnt++;
    	dow++;
    	
    	
    	if (dow >= 7)
    	{
	    	dow = 0;
	    	bufferDays = 0;
    	}
    	
    });
}

