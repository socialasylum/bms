var reg = {}

reg.indexInit = function ()
{
    $('#submitBtn').click(function(e){
        reg.checkRegForm();
    });


    $('#resetFormBtn').click(function(e){
        if (confirm("Are you sure you wish to reset the registration form?"))
        {
            $('#regForm')[0].reset();
        }
    });

   $('#industry').change(function(){
        if ($('#industry :selected').val() == '0')
        {
            $('#otherIndustryFormGroup').show(global.showEffect);
        }
        else
        {
            $('#otherIndustry').val(''); // clears value
            $('#otherIndustryFormGroup').hide(global.hideEffect);
        }
    });
}

reg.confirmRegInit = function ()
{
    $('#approveBtn').click(function(e){
        reg.approveUser($(this).attr('value'));
    });

    $('#denyBtn').click(function(e){
        reg.denyUser($(this).attr('value'));
    });

}

reg.denyRegInit = function ()
{
    $('#submitBtn').click(function(e){
        reg.denyReg();
    });
}

reg.paymentInit = function ()
{
    $('#submitBtn').click(function(e){
        reg.checkBilling();
    });
}

reg.checkRegForm = function ()
{

    if ($('#companyName').val() == '')
    {
        global.renderAlert('Please enter the company name!');
        $('#companyName').focus();
        return false;
    }

    if ($('#firstName').val() == '')
    {
        global.renderAlert('Please enter your first name');
        $('#firstName').focus();
        return false;
    }

    if ($('#lastName').val() == '')
    {
        global.renderAlert('Please enter your last name!');
        $('#lastName').focus();
        return false;
    }

    if ($('#email').val() == '')
    {
        global.renderAlert('Please enter an email address!');
        $('#email').focus();
        return false;
    }

    if ($('#confirmPassword').val() == '')
    {
        global.renderAlert('Please confirm your password!');
        $('#confirmPassword').focus();
        return false;
    }

    if ($('#password').val() !== $('#confirmPassword').val())
    {
        global.renderAlert('Your passwords did not match');

        // clears password fields
        $('#password').val('');
        $('#confirmPassword').val('');

        $('#password').focus();
        return false;
    }

    $('#submitBtn').attr('disabled', 'disabled');

    $.post("/register/reguser", $('#regForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            if ($('#company').val() == '35')
            {
                window.location = 'http://productpricetracker.com/welcome/login?site-success=' + escape("Your account has been created. Please Sign in! =)");
            }
            else
            {
                window.location = '/intranet/login?site-success=' + escape("Your account has been created. Please login with the user you created during the signup process.");
            }
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#submitBtn').removeAttr('disabled');
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-danger');
            $('#submitBtn').removeAttr('disabled');
        }
    }, 'json');
}

reg.approveUser = function(id){
    $('#approveBtn').attr('disabled','disabled');
    $('#denyBtn').attr('disabled','disabled');

    window.location = '/register/approveReg/'+id+'/1';
}

reg.denyUser = function(id){
    $('#approveBtn').attr('disabled','disabled');
    $('#denyBtn').attr('disabled','disabled');

    window.location = '/register/denyReg/'+id+'/2';
}

// reg.denyReg = function(id){
    // $('#submitBtn').attr('disabled','disabled');

    // $.post("/register/update");
// }

reg.checkBilling = function(){
    /*
    if ($('#ccName').val() == '')
    {
        global.renderAlert('Please enter Billing Name!');
        $('#ccName').focus();
        return false;
    }

    if ($('#ccNumber').val() == '')
    {
        global.renderAlert('Please enter Credit Card Number!');
        $('#ccNumber').focus();
        return false;
    }

    if ($('#ccExpM').val() == '')
    {
        global.renderAlert('Please enter card Expiration Month!');
        $('#ccExpM').focus();
        return false;
    }

    if ($('#ccExpY').val() == '')
    {
        global.renderAlert('Please enter card Expiration Year!');
        $('#ccExpY').focus();
        return false;
    }

    if ($('#ccCvv').val() == '')
    {
        global.renderAlert('Please enter card CVV!');
        $('#ccCvv').focus();
        return false;
    }
    */

    if ($('#agree').val() == '0')
    {
        global.renderAlert('Please check that you agree to our Terms of Service!');
        $('#agree').focus();
        return false;
    }

    $('#submitBtn').attr('disabled', 'disabled');

    $.post("/register/saveBilling", $('#billingInfo').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/intranet/landing?site-success=' + escape("Your billing info has been saved!");
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-error');
        }
    }, 'json');

}
