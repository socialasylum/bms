    var kb = {}

kb.sectionCnt = 1;

kb.viewingSectionModal = 0;

kb.indexInit = function ()
{

    folders.folderInit('#kbArticle-container', 'kb');
    kb.setFileDraggable(false);

    $('#kbArticle-container').find("li").each(function(index, item)
    {
            // articles
            if ($(item).attr('itemType') == '2')
            {
                // if they double click on a article
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    window.location = "/kb/view/" + $(item).attr('value') + '/' + $('#folder').val() + '/' + $('#rootFolder').val();
                });
            }
    });

    $.rightClickMenu({
        selector: '.kbArticle',
        src: '/kb/rightclick/' + $('#folder').val(),
        selected: function (item, menu, action)
        {
            if (action == 'EDIT')
            {
                // window.location = '/kb/edit/' + $(item).attr('itemID') + '/' + $('#folder').val()
                window.location = '/kb/edit/' + $(item).attr('value');
            }

            if (action == 'DELETE')
            {
                kb.deleteKBArticle($(item).attr('value'));
                // console.log('delete:' + $(item).attr('value'));
            }
            // alert($(item).attr('itemID'));
            // intranet.loadWidget($(item).attr('url'), $(item).attr('id') ,$(port).val());
        }
    });

    $('#createFolderBtn').click(function(e){
        folders.createFolder();
    });
}

kb.editInit = function ()
{
    
    $('#cancelBtn').cancelButton('/kb');
    
    // $('#section').ckeditor();

    // all textares basically turn into Ckeditors
    $('#section-container').find("textarea").each(function(index, item)
    {
    	CKEDITOR.replace($(item).attr('id'));
        //$(item).ckeditor();
    });
    

    $('#addSectionBtn').click(function(e){
        kb.addSection();
    });

    $('#settingsBtn').click(function(e){
        $('#mainSettingsModal').modal('show');
    });

    $('#saveBtn').click(function(e){
        kb.checkEditForm();
    });


    $('#sectionPropModal').modal({
        show: false
    });


    $('#sectionPropModal').on('hide.bs.modal', function (){

        var heading = $('#sectionPropModal').find('#sectionPropHeading').val();

        kb.updateSectionSettings(kb.viewingSectionModal, heading);
        kb.viewingSectionModal = 0;
    });


    kb.setSectionsSortable();

}

kb.addSection = function ()
{

    // removes instance
    CKEDITOR.instances['body_0'].destroy();


    // 
    /*
    for (var i in CKEDITOR.instances)
    {
        str = '';

        str = CKEDITOR.instances[i].getData();
        $(CKEDITOR.instances[i].name).val(str);
    }
    */

    // $('#section-container').find('textarea').first().ckeditor().destroy();

    // var html = $('#master-section').html();
    var html = $('#section-wrapper-0').html();

	CKEDITOR.replace('body_0');
    //$('#body_0').ckeditor()

	

    $('#section-container').append("<li><div class='row' id='section-wrapper-" + kb.sectionCnt + "'>" + html + "</div></li>");

    $('#section-container').find('textarea').last().attr('id', 'body_' + kb.sectionCnt);
    
    $('#section-container').find('input#sectionCnt').last().val(kb.sectionCnt);
    
    // $('#section-container').find('input#sectionCnt').last().attr('id', 'sectionCnt_' + String(kb.sectionCnt));
    // $('#sectionCnt_' + kb.sectionCnt).val(kb.sectionCnt);

    // console.log($('#sectionCnt_' + kb.sectionCnt).val());

    // clears value
    $('#body_' + kb.sectionCnt).val('');
    $('#body_' + kb.sectionCnt).ckeditor();

    // clears heading
    $('#section-wrapper-' + kb.sectionCnt).find('.panel-title').text('Section Heading');
    $('#section-wrapper-' + kb.sectionCnt).find('textarea').attr('body_' + kb.sectionCnt);
    $('#section-wrapper-' + kb.sectionCnt).find('#sectionHeading').val('');
    $('#section-wrapper-' + kb.sectionCnt).find('#sectionId').val('0');
    
    kb.sectionCnt++;
}

kb.deleteSection = function (b)
{
    if (confirm("Are you sure you wish to delete this section?"))
    {
        var section = $(b).parent().parent().parent().parent().parent().parent().parent().parent().parent();

        // console.log('CLASS: ' + section.attr('class'));
        // console.log('ID: ' + section.attr('id'));

        section.hide(global.hideEffect, 500, function(){
            $(this).html('');

        });
    }
}


kb.getSectionHeadingProperties = function (b)
{
    var section = $(b).parent().parent().parent().parent().parent().parent().parent().children('div.panel-heading').children('#sectionCnt');

    kb.viewingSectionModal = section.attr('value');

    // console.log(section.attr('value'));

    /*
    $.get("/kb/sectionprop/" + section.attr('value'), function(data){
        $('#sectionPropModal').html(data);
        $('#sectionPropModal').modal('show');
    });
    */

    $.post("/kb/sectionprop/" + section.attr('value'), $('#editForm').serialize(), function(data){
        $('#sectionPropModal').html(data);
        $('#sectionPropModal').modal('show');
    });
}

kb.checkEditForm = function ()
{
    if ($('#title').val() == '')
    {
        global.renderAlert("Please enter a title for this article!");
        $('#title').effect(global.effect);
        $('#title').focus();
        return false;
    }


    var str = '';

    // updates HTML dom textarea with CKeditor contents for each ck isntace
    for (var i in CKEDITOR.instances)
    {
        str = '';

        str = CKEDITOR.instances[i].getData();
        $(CKEDITOR.instances[i].name).val(str);
    }


    $.post("/kb/save", $('#editForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/kb?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-danger');
            }
    }, 'json');
}

kb.updateSectionSettings = function (sectionCnt, heading)
{
    // console.log('update section: ' + sectionCnt );

    // console.log('HEADING: ' + heading);

    // first update heading

    $('#section-wrapper-' + sectionCnt).find('.panel-title').text(heading);
    $('#section-wrapper-' + sectionCnt).find('#sectionHeading').val(heading);
}

kb.setSectionsSortable = function ()
{
    $('#section-container').sortable({
        start: function (event, ui)
        {
            // destorys all instances while moving
            for (var i in CKEDITOR.instances)
            {
                CKEDITOR.instances[i].destroy();
            }
        },
        stop: function (event, ui)
        {

            // all textares basically turn into Ckeditors
            $('#section-container').find("textarea").each(function(index, item)
            {
                $(item).ckeditor();
            });
    
        }
    });

}


kb.setFileDraggable = function(clearPreviousSortable)
{
        if (clearPreviousSortable == undefined) clearPreviousSortable = false; // deafults to false

        if (clearPreviousSortable == true)
        {
            // removes previous functionality
            $('#fileList').sortable('destroy');
        }

        // goes through each li and for folders makes it so they are not disbled
        $('#fileList').find("li").each(function(index, item)
        {
            // folders
            if ($(item).attr('itemType') == '1')
            {
                $(item).removeClass('ui-state-disabled');
            }
        });

        $('#fileList').selectable({
            filter: "li",
            selected: function (event, ui)
            {
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }

                    $(ui.selected).draggable({
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#fileList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });

}

kb.deleteKBArticle = function (article)
{
    if (confirm("Are you sure you want to delete this knowledge base article?"))
    {
        $.post("/kb/deletekbarticle", { article:article, cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/kb?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-danger');
            }
        }, 'json');
    }
}
