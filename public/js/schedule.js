var schedule = {}

schedule.indexInit = function ()
{

    if ($('#location :selected').val() !== '')
    {
        schedule.loadStoreSchedule();
    }

    $('#location').change(function(){
        schedule.loadStoreSchedule();
    });
}

schedule.loadStoreSchedule = function (month, year)
{

    if (month == undefined) month = '0';
    if (year == undefined) year = '0';

    global.ajaxLoader('#schedule-display');

    $.get("/schedule/overview/" + $('#location :selected').val() + '/' + month + '/' + year, function(data){
        $('#schedule-display').html(data).effect('highlight');
    });
}

schedule.editInit = function ()
{
    $('#saveBtn').click(function(e){
        schedule.checkEditForm();
    });

    $('#shiftStart').timePicker({
        show24Hours: false
    });

    $('#shiftEnd').timePicker({
        show24Hours: false
    });



}

schedule.checkEditForm = function ()
{
    if ($('#user :selected').val() == '')
    {
        global.renderAlert("Please select a user!");
        $('#user').effect('highlight');
        return false;
    }

    if ($('#shiftStart').val() == '')
    {
        global.renderAlert("Please select a shift start time!");
        $('#shiftStart').effect('highlight');
        return false;
    }

    if ($('#shiftEnd').val() == '')
    {
        global.renderAlert("Please select a shift end time!");
        $('#shiftEnd').effect('highlight');
        return false;
    }


    $.post("/schedule/saveentry", $('#editForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            // global.renderAlert(data.msg, 'alert-success', 'day' + day + 'alerts');
            window.location = '/schedule/edit/' + $('#location').val() + '?date=' + $('#date').val() + '&site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');

}

schedule.deleteDaySchedule = function (b, id)
{
    if (confirm("Are you sure you wish to delete this schedule entry?"))
    {
        $.post("/schedule/deleteentry", { id: id, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                $(b).parent().parent().hide('highlight');

                // window.location = '/schedule/edit/' + $('#location').val() + '?date=' + $('#date').val() + '&site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
        }, 'json');
    }
}
