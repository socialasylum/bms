var compsettings = {};

compsettings.index = {};
compsettings.edit = {};


compsettings.index.init = function ()
{
	global.createDataTable('#compTbl');
	
	/*
	$('table tr').click(function(e){
		var id = $(this).data('id');
		
		if (id !== undefined && parseInt(id) > 0)
		{
			redirect('/companysettings/edit/' + id);
		}
	});
	*/
}

compsettings.index.edit = function (b, id)
{
	$(b).disableSpin();
	
	redirect('/companysettings/edit/' + id);

	return true;
}

compsettings.edit.init = function ()
{
	$('select:not(.cc)').selectpicker();
	$('select.cc').selectpicker({ width:100 });
	
	$("input[type='file']").uploader({
		uploadUrl: '/companysettings/logoupload',
		extraParams: { company: $('#id').val() },
		successFunction: function (d, fd)
		{
			compsettings.loadLogo(d.file);
			
			$('#logo').val(d.file);
			
			$('#delLogoBtn').removeAttr('disabled');
						
			// if same company as current, updates nav bar
			if (compsettings.isThisComp()) compsettings.updateNavbar(d.thumb);
		},
		CSRF:
		{
			enabled: true,
			key: 'cgi_token', 
			val: global.CSRF_hash
		}
	}).data('uploader');

    $('#saveBtn').click(function(e){
        compsettings.checkSettingsForm();
    });
    
    $('#cancelBtn').click(function(e){
	    e.preventDefault();
	    
	    if (confirm("Are you sure you wish to cancel?"))
	    {
	    	$(this).disableSpin();
			redirect("/companysettings");
	    }
    });

    if ($('#cancelSubBtn').exists())
    {
        $('#cancelSubBtn').click(function(e){
            compsettings.cancelSubscription();
        });
    }
    
    $('#delLogoBtn').click(function(){
	    compsettings.delLogo();
    });

    $('#slider').slider({
        range: 'min',
        min: 5,
        max: 2000,
        step: 5,
        value: $('#users').val(),
        slide: function (event, ui)
        {
            $('#numUsers').text(ui.value);
            
            $('#billingUsers').text(ui.value);

            // sets hidden input how many users were selected for use later
            $('#users').val(ui.value);
            $('input[name=users]').val(ui.value);

            compsettings.calculate();
        }
    });


    compsettings.calculate();


    if ($('#becomeMemberTxt').exists())
    {
    	global.ckeditor($('#becomeMemberTxt'));


    }

	$('#addDomainBtn').click(function(e){

		compsettings.addDomain();
	});
	
	$('#domain').focus(function(){
	
		$(this).unbind('keypress');
		
		$(this).bind('keypress', function(e){
	
			var code = e.keyCode || e.which;
			
			if (code == 13 && !e.shiftKey)
			{		
				e.preventDefault();
				compsettings.addDomain();
			}
		});
	});
	
	
	$('#hideFooter').click(function(){
		if (compsettings.isThisComp())
		{
			if ($(this).prop('checked')) compsettings.toggleFooter(false);
			else compsettings.toggleFooter(true);
		}
	});

    $('#logo').on('change keyup', function(e){
	    if ($(this).val().length > 0) $('#delLogoBtn').removeAttr('disabled');
	    else $('#delLogoBtn').attr('disabled', 'disabled');
    });
}

// checks if the user is editing the company they are currently logged in as
compsettings.isThisComp = function ()
{
	if ($('#id').val() == $('body').data('company')) return true;

	return false;
}

compsettings.checkSettingsForm = function ()
{

    if ($('#companyName').val() == '')
    {
        global.renderAlert("Please enter a company name!");
        $('#companyName').focus();
        return;
    }
    var exempt = false;

    if ($('#exempt').exists())
    {
        if ($('#exempt').prop('checked') == true)
        {
            if (!confirm("Are you sure you wish to continue? By making this company billing exempt it will cancel any current subscriptions the company may have."))
            {
                global.renderAlert('Company settings were not saved!');
                return false;
            }
            else
            {
                exempt = true;
            }
        }
    }


    if (exempt == false)
    {

        if ($('#billingFirstName').val() == '')
        {

            // changes tab to Billing tab`
            $('#compSettingsTabs li:eq(1) a').tab('show');
            
            global.renderAlert("Please enter a billing first name.");
            $('#billingFirstName').focus();
            $('#billingFirstName').effect(global.effect);
            return;
        }

        if ($('#billingLastName').val() == '')
        {
            // changes tab to Billing tab`
            $('#compSettingsTabs li:eq(1) a').tab('show');
            $('#billingLastName').effect(global.effect);
            global.renderAlert("Please enter a billing last name.");
            $('#billingLastName').focus();
            return;
        }


        // check if any onfile cards were selected
        var onfileCardSelected = false;

        if ($('#cardTbl').exists())
        {
            $('#cardTbl').find("input").each(function(index, item)
            {
                if ($(item).attr('type') == 'radio')
                {
                    if ($(item).prop('checked') == true)
                    {
                        onfileCardSelected = true;
                        return true;
                    }
                }
            });
        }


        var priceChange = (parseFloat($('#orgPrice').val()) == parseFloat($('#calcPrice').val())) ? false : true;

    
        if ($('#subStatus').val() !== 'ACTIVE' || priceChange == true)
        {

            if  (onfileCardSelected == false)
            {

                // user did not select a card on file to renew subscription with

                if ($('#ccName').val() == '')
                {
                    global.renderAlert('Please enter the card holders name.');
                    $('#ccName').focus();
                    return false;
                }

                if ($('#ccNumber').val() == '')
                {
                    global.renderAlert('Please enter the card number.');
                    $('#ccNumber').focus();
                    return false;
                }

                if ($('#ccExpM :selected').val() == '')
                {
                    global.renderAlert('Please select the card expiration month.');
                    $('#ccExpM').focus();
                    return false;
                }

                if ($('#ccExpY :selected').val() == '')
                {
                    global.renderAlert('Please select the card expiration yard.');
                    $('#ccExpY').focus();
                    return false;
                }

                if ($('#ccCvv').val() == '')
                {
                    global.renderAlert('Please enter the card CVV number (usually on the back; a-mex on the front).');
                    $('#ccCvv').focus();
                    return false;
                }
            }
        }
    }

    $('#saveBtn').disableSpin();
    
    // updates textarea with latest content from CKeditor before saving
    
    if ($('#becomeMemberTxt').exists())
    {
    	
		//var str = CKEDITOR.instances['becomeMemberTxt'].getData();
		//$('#becomeMemberTxt').val(str);
    }
    

    
	$.post('/companysettings/update', $('#companyForm').serialize(), function(data){
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
		}
		else
		{
			Danger(data.msg);
		}
		
		$('#saveBtn').enableSpin();
	}, 'json');
}

compsettings.selectModule = function (sel, module)
{
    if (sel.hasClass('package-selected'))
    {
        // un-set module
        sel.removeClass('package-selected');
    }
    else
    {
        // set module
        sel.addClass('package-selected');
    }

    compsettings.calculate();
}


compsettings.calculate = function ()
{

    $('#moduleHiddenContainer').html('');

    // first gets the number of users user has selected
    var users = parseInt($('#users').val());

    var total = 0;
    var moduleTotal = 0;

    $('#mod-container-list').find("div").each(function(index, item)
    {
        if ($(item).hasClass('package-selected'))
        {

            $('#moduleHiddenContainer').append("<input type='hidden' name=\"module[]\" value='" + $(this).attr('value') + "'>");
            
            moduleTotal = moduleTotal + parseFloat($(this).attr('ppu'));
        }
    });



    // total = total * parseInt(users) + 100;
    total = global.calculatePricing(users, moduleTotal);

    // total = global.addCommas(total);


	total = $.number(total, 2);

    // sets hidden input of the calculate price.
    $('#calcPrice').val(total);
    $('input[name=calcPrice]').val(total);

    $('#est-display').html(total);
    $('#billingTotal').html('$' + total);
}


compsettings.cancelSubscription = function ()
{
    $('#cancelSubBtn').attr('disabled', 'disabled');

    if (confirm("Are you sure you wish to cancel the subscription associated with this company?"))
    {
        $.getJSON("/companysettings/cancelsub", function(data){

            if (data.status == 'SUCCESS')
            {
                // global.renderAlert(data.msg, 'alert-success');
                window.location = '/companysettings?site-success=' + escape(data.msg);
                return true;
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                $('#cancelSubBtn').removeAttr('disabled');
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                $('#cancelSubBtn').removeAttr('disabled');
                return false;
            }

        });
    }
}


compsettings.addDomain = function ()
{
	$('#addDomainBtn').disableSpin();
	
	var $div = $("<div>", { class:'label label-info msg-to-lbl', style:"opacity:0;" });

	var $closeBtn = $("<button>", { class:'close', type:'button' });

	$closeBtn.html("&times;");

	$closeBtn.click(function(){
		compsettings.removeDomain(this);	
	});

	$div.html("<label>" + $('#domain').val() + "</label>");
	
	$div.append($closeBtn);
	
	$('#domains').prepend($div);
	
	$div.velocity({ opacity:1 });

	$.post('/companysettings/adddomain', { domain: $('#domain').val(), company: $('#id').val(), cgi_token:global.CSRF_hash }, function(data){
		if (data.status == 'SUCCESS')
		{
			Success(data.msg);
			$('#domain').val('').focus();
			
			$div.data('key', data.id);

			$('#addDomainBtn').enableSpin();
		}
	}, 'json');

	return true;
}

compsettings.removeDomain = function (closeBtn)
{

	$(closeBtn).disableSpin();
	
	var el = $(closeBtn).parent();

	var id = $(el).data('key');
	
	el.css('overflow', 'hidden');
	
	el.velocity({ scaleX:0, scaleY:0, opacity:0 });
	
	$.post('/companysettings/removedomain', { id:id, company: $('#id').val(), cgi_token:global.CSRF_hash }, function(data){
		if (data.status == 'SUCCESS')
		{
			el.remove();
			
			Success(data.msg);
		}
		else
		{
			// brings element back if error
			el.velocity('reverse');

			$(closeBtn).enableSpin();
			
			Danger(data.msg);
		}
	}, 'json');
}

compsettings.loadLogo = function (file)
{
	var src = "/public/uploads/logos/" + $('#id').val() + '/' + file;

	var $img = $('<img>', { class:'img-responsive', id:'logoPreview', src:src, style:"opacity:0" });
		
	
	if ($('#logo-display > img').exists())
	{
		$('#logo-display > img').velocity('fadeOut', { complete:function(){
			$(this).remove();	
			
			$('#logo-display').prepend($img);
		
			$img.velocity('fadeIn');
		}});
	}
	else
	{
	
		$('#logo-display').prepend($img);
		
		$img.velocity('fadeIn');
				
	}

	return $img;
}

compsettings.delLogo = function ()
{
	if (confirm("Are you sure you wish to delete logo?"))
	{
		$('#delLogoBtn').disableSpin();
		
		$.post('/companysettings/rmlogo', { company:$('#id').val(), file: $('#logo').val(),  cgi_token:global.CSRF_hash  }, function(data){
			
			$('#delLogoBtn').enableSpin();

			if (data.status == 'SUCCESS')
			{
				Success(data.msg);
				
				$('#logo').val('');
				
				$('#delLogoBtn').attr('disabled', 'disabled');
				
				$('#logoPreview').velocity('fadeOut', {
					complete:function()
					{
						$(this).remove();
					}
				});
			}
			else
			{
				Danger(data.msg);	
			}

		}, 'json');
	}
}

compsettings.updateNavbar = function (thumb)
{
	var src = "/public/uploads/logos/" + $('#id').val() + '/' + thumb;
	
	var $img = $('<img>', { src:src, id:'navbar-logo', style:"opacity:0" });
	
	if ($('#navbar-logo').exists())
	{
		$('#navbar-logo').velocity('fadeOut', {
			complete:function(){
				$(this).remove();
				
				$('a.navbar-brand').append($img);
				
				$img.velocity({ opacity:1 }, {
					complete:function(){
					    global.adjustNavbar();
					}
				});
			    global.adjustNavbar();
			}
		});
	}
	else
	{
		$('a.navbar-brand')
			.addClass('logoAdjust')
			.attr('id', 'navbar-logo')
			.html("<span class='helper'></span>")
			.append($img);
			
		
		$img.velocity({ opacity: 1 }, {
			complete:function(){
			    global.adjustNavbar();
			}
		});
		
		global.adjustNavbar();
    		
	}
}

compsettings.toggleFooter = function (display)
{
	if (display == undefined) return false;
	
	if (display) $('.footer').velocity('fadeIn');
	else $('.footer').velocity('fadeOut');
	
	return true;
}