/**
 * Author: William Gallios
 * 
 * Created: 2013-08-07
 *
 * Description: Checks all inputs from a selector and if they are changed 
 * will kick off 'change' callback function
 */
(function($){
    $.fn.inputedit = function (options)
    {
        $(this).find("input,select,textarea,radio,checkbox,button").each(function(index, item){


            $(item).change(function()
            {
                options.change(this);
            });

            if ($(item).prop('tagName') == 'BUTTON')
            {
                    $(item).click(function(e){
                        options.change(this);
                    });
            }
     
            /*
                // gets type attribute
                var type = $(item).attr('type');

                // if a button (or submit button) is clicked - ensures lower case
                if (type.toLowerCase() == 'button' || type.toLowerCase() == 'submit')
                {
                    $(item).click(function(e){
                        options.change(this);
                    });
                }
            */
        });

    }
})(jQuery);
