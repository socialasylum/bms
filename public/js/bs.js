
var bs = {}

bs.sel;

bs.init = function ()
{	
	//return false;
	var bg = $('body').data('bg');
	
	if (bg == undefined) return false;
	
	var hideFooter = $('body').attr('data-hideFooter');
	
	//bs.sel = $('#site-wrapper');
	//bs.sel = (hideFooter == undefined || hideFooter == '0') ? $('.wrapper') : $('#site-wrapper');

	$('#site-wrapper').anystretch(bg, { speed:150 });

	$('#site-wrapper').css('position', '');
	
	bs.listen();
	
	return true;
}

bs.listen = function ()
{
	
	$('#main-container').resize(function(e){
		
		$('#site-wrapper').resize();
		
	});
	
	// listens for tab changes
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

		$('#site-wrapper').resize();
	});
	
	return true;
}

bs.resize = function ()
{
	$('#site-wrapper').resize();
	
	return true;
}