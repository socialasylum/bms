var photos = {}


photos.photoModalMaxHeight = 0;
photos.photoModalHeight = 0;

/*

$(function(){
	
	photos.setPhotoModalMaxHeight();
	
	$(window).resize(function(){
	
		photos.setPhotoModalMaxHeight();
		photos.setPhotoBgHeight();
	});

	$('#photo-modal').on('show.bs.modal', function (e) {
		photos.setPhotoModalMaxHeight();
		photos.setPhotoBgHeight();
  	});
	
	$('#photo-modal').on('shown.bs.modal', function (e) {
		photos.setPhotoModalMaxHeight();
	
		photos.setPhotoBgHeight();
		photos.getInfo();
		
		photos.getExtraViewData();
  	});

	$('#photo-modal').on('hidden.bs.modal', function (e) {

		// clears min-height when modal is closed for different image dimensions
		photos.resetMaxHeights();
  	});
});
*/
photos.viewPhoto = function (photoID, userid, fileName, setDimensions)
{
	if (setDimensions == undefined) setDimensions = false;

	var src = "/public/uploads/userimgs/" + userid + "/" + fileName;

	// sets hidden input in modal for photoID
	$('#photo-modal #modalPhotoID').val(photoID);

	$('#photo-modal #photo-img').attr('src', src);
	
	// get img width and height
	var w = $('#img-preview-' + photoID).width();
	var h = $('#img-preview-' + photoID).height();

	// sets width and height of anchor link to keep dimensions
	if (setDimensions == true)
	{
		$('#img-preview-link-' + photoID).width(w);
		$('#img-preview-link-' + photoID).height(h);
		
		$('#img-preview-link-' + photoID).css('display', 'inline-block');
	}
	
	var bgsize = w + 'px ' + h + 'px';	

	$('#img-preview-link-' + photoID).css('background-image', 'url(' + src + ')');
	$('#img-preview-link-' + photoID).css('background-size', bgsize);
	
	var l = $('#img-preview-link-' + photoID).html();
	
	$('#img-preview-link-' + photoID).effect('highlight');
	//alert(l);
	
	// sets DL Link
	$('#photoDLLink').attr('href', '/public/uploads/userimgs/' + userid + '/' + fileName);

	global.ajaxLoader('#img-preview-link-' + photoID);

	$('#photo-modal #photo-img').load(function(e){
		//loader.setPercent(100).show();

		$('#img-preview-link-' + photoID).html(l);
		
		//$('#img-preview-' + photoID).css('opacity', '1');
		//$('#img-preview-' + photoID).css('fileter', 'alpha(opacity=100)');
	
		$('#photo-modal').modal('show');

		$('#photo-modal .img-bg, #photo-modal .img-bg *').hover(function(e){
			photos.showPhotoNav();
		});

		
		$('#photo-modal .info-col').hover(function(e){
			photos.hidePhotoNav();
		});


		//photos.setPhotoBgHeight();
	});	
}

photos.setPhotoModalMaxHeight = function ()
{
	var h = parseInt($(window).height());
	
	h -= 60; // removes 60 pixels to compensate for padding/margins
	
	photos.photoModalHeight = h;
	
	$('#photo-modal .modal-content').css('max-height', h + 'px');
	$('#photo-modal #photo-img').css('max-height', h + 'px');
	
	$('#photo-modal .info-col').css('max-height', (h + 30) + 'px');

	//$('#photo-modal .slimScrollDiv').html('');	
	//$('#photo-modal .slimScrollDiv').removeAttr('style');

}

photos.resetMaxHeights = function ()
{
	photos.photoModalMaxHeight = 0;
	photos.photoModalHeight = 0;

	$('#photo-modal #photo-img').attr('src', '');

	$('#photo-modal .modal-content').css('max-height', '');
	$('#photo-modal #photo-img').css('max-height', '');
	
	$('#photo-modal .img-bg').css('min-height', '');
	$('#photo-modal .info-col').css('max-height', '');
	
	$('#photo-modal #photo-info-display').html('');

	$('#photo-modal #photo-info-display').css('height', '');
}

photos.setPhotoBgHeight = function ()
{
	var h = $('#photo-modal').find('.modal-dialog').height();
	
	photos.photoModalHeight = h;
	//console.log("H: " + h);
	
	//$('#photo-modal .img-bg').height(h);
	$('#photo-modal .img-bg').css('min-height', h + 'px');
	$('#photo-modal #photo-info-display').css('height', h + 'px');
	
	
}

photos.photoCommentEnter = function ()
{
	$(window).unbind('keypress');

	$(window).bind('keypress', function(e){
		var code = e.keyCode || e.which;
		
		if (code == 13)
		{
			photos.savePhotoComment();
		}
	});
}

photos.getInfo = function (photoID)
{
	// if modal is being used
	if ($('#photo-modal #modalPhotoID').exists())
	{
		// if photoID is not passed, checks for one in hidden input in modal
		if (photoID == undefined) photoID = $('#photo-modal #modalPhotoID').val();
	}

	global.ajaxLoader('#photo-info-display');
	
	$.get('/photos/modalinfo/' + photoID, function (data){
		//setPercent(100).show();

		
		$('#photo-info-display').html(data);
		
		//photos.setPhotoBgHeight(); // readjusts bg
		
		// sets autogrow for textarea
		
		$('#photo-modal #caption').autosize({
			callback: function ()
			{
				photos.sizeCommentsScroll(true);
			}
		});
		
		$('#photo-modal #caption').css('height', '40px');
		
		$('#photo-modal #caption').focus(function(e){
			//photos.photoCommentEnter();
		});
		
		var finalh = photos.sizeCommentsScroll();
		
		// sets slimscroll on container element
		$('#photo-scroll').slimScroll({
			 height: finalh + 'px'
		});
	});
	
}

photos.getExtraViewData = function (photoID)
{
	// if modal is being used
	if ($('#photo-modal #modalPhotoID').exists())
	{
		// if photoID is not passed, checks for one in hidden input in modal
		if (photoID == undefined) photoID = $('#photo-modal #modalPhotoID').val();
	}

	$.getJSON('/photos/extradata/' + photoID, function(data){
		$('#photo-modal #albumBtn').html(data.albumName);
	});
}

photos.sizeCommentsScroll = function (updateScroll)
{
	if (updateScroll == undefined) updateScroll = false;
	
	var sth = $('#photo-modal .static-content').height();
	
	sth += 15; // adds 30 for margins
	
	//console.log("STH: " + sth);
	//console.log("Modal Height: " + user.photoModalHeight);
	
	var finalh = photos.photoModalHeight - sth;

	if (updateScroll == true)
	{
		//finalh += 15;
		$('.slimScrollDiv, #photo-scroll').css('height', finalh + 'px');
	}
		
	return finalh;	
}

photos.showPhotoNav = function ()
{
	$('#photo-modal .photo-actions').velocity({ opacity:1 });
	$('#photo-modal .actions-bg').velocity({ opacity:0.5 });
}


photos.hidePhotoNav = function ()
{
	$('#photo-modal .photo-actions').velocity({ opacity:0 });
	$('#photo-modal .actions-bg').velocity({ opacity:0 });
}