var locations = {}

locations.map;


locations.indexInit = function ()
{

	locations.map = gm.initialize('locGoogMap', undefined, undefined, undefined, false);

	gm.loadSavedMarkers(false, false, false);
	
	global.createDataTable('#locations');
	
	/*
	$(window).on('gmaps.api.loaded', function(e){
		gm.postInit(false);
		
		gm.loadSavedMarkers(false, false, false);
	});
	*/
}

locations.createInit = function ()
{
    $('#openDate').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('#createBtn').click(function(e){
        locations.checkCreateForm();
    });

    $('#cancelBtn').cancelButton('/location');
}

locations.editInit = function ()
{
	$('select').selectpicker();
	
    $('#openDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    if ($('#empsTbl').exists())
    {
        // $('#empsTbl').dataTable();
    }

    $('#saveBtn,#uploadPictureBtn').click(function(e){
        locations.checkEditForm();
    });


    $('#addYTBtn').click(function(e){
        locations.checkYouTubeUrl();
    });

    $('#cancelBtn').cancelButton('/location');

    CKEDITOR.replace('description');
    CKEDITOR.replace('addDescription');

    locations.renderPictures();
    locations.renderYoutubeVideos();

}

locations.checkCreateForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert("Please enter a location name!");
        $('#name').focus();
        return false;
    }

    $('#createBtn').attr('disabled', 'disabled');

    $.post("/location/createlocation", $('#createForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/location/edit/' + data.msg + '?site-success=' + escape("Location has been created!");
            $('#cancelBtn').attr('disabled', 'disabled');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            $('#createBtn').removeAttr('disabled');
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-error');
            $('#createBtn').removeAttr('disabled');
        } 
    }, 'json');
}

locations.checkEditForm = function ()
{
    if ($('#name').val() == '')
    {
        global.renderAlert('Please enter a location name!');
        $('#name').effect('highlight');
        return false;
    }


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['description'].getData();
    $('#description').val(str);


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['addDescription'].getData();
    $('#addDescription').val(str);



    $('#saveBtn').attr('disabled', 'disabled');
    $('#cancelBtn').attr('disabled', 'disabled');
    $('#editForm').submit();
}

locations.renderPictures = function ()
{
    global.ajaxLoader('#pictureDisplay');

    $.get("/location/pictures/" + $('#id').val(), function(data){
        $('#pictureDisplay').html(data);
        // users.ytVideoSort();
    });
}

locations.checkYouTubeUrl = function ()
{
    if ($('#youTubeUrl').val() == '')
    {
        global.renderAlert("Please enter a url.");
        $('#youTubeUrl').effect(global.effect);
        $('#youTubeUrl').focus();
    }

    $.post("/location/addyoutubeurl", { location: $('#id').val(), url:$('#youTubeUrl').val(), cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                global.renderAlert(data.msg);

                $('#youTubeUrl').val('');
                $('#youTubeUrl').effect(global.effect);

                locations.renderYoutubeVideos();

            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');
}


locations.renderYoutubeVideos = function ()
{
    global.ajaxLoader('#YTVideoDisplay');

    $.get("/location/youtubevideos/" + $('#id').val(), function(data){
        $('#YTVideoDisplay').html(data);
        locations.ytVideoSort();
    });
}


/**
 * makes youtube videos sortable
 */
locations.ytVideoSort = function ()
{
    $('#ytVideoSortList').sortable({
        placeholder: 'yt-primary',
        stop: function (event, ui)
        {
            locations.updateYoutubeVideoOrder();
        }
    });
}


locations.updateYoutubeVideoOrder = function ()
{
    var data = $('#ytVideoSortList').sortable('serialize');

    // alert(JSON.stringify(data));

    $.post("/location/saveyoutubeorder", data + "&" + $.param({ 'cgi_token': global.CSRF_hash }), function(data){
            if (data.status == 'SUCCESS')
            {
                locations.renderYoutubeVideos();
            }
            if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');
}

locations.deleteYTVideo = function (id)
{
    if (confirm("Are you sure you wish to delete this video?"))
    {
        $.post("/location/deleteytvideo", { id:id,cgi_token:global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                locations.renderYoutubeVideos();
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
        }, 'json');
    }
}
