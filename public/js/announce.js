var announce = {}

announce.indexInit = function ()
{
	global.createDataTable('#annTbl');

    $('#helpBtn').click(function(e){
        global.viewModKBArticle($('#module').val());
    });
}

announce.editInit = function ()
{

    $('#cancelBtn').cancelButton('/announce');

    CKEDITOR.replace('announcement');

    $('#settingsBtn').click(function(e){
        $('#settingsModal').modal('show');
    });

    $('#posAccessBtn').click(function(e){
        $('#posAccessModal').modal('show');
    });

    $('#locAccessBtn').click(function(e){
        $('#locAccessModal').modal('show');
    });

    $('#sendToType').change(function(e){
        announce.clearTerAndLocInputs();
        announce.renderSendToType();
    });

    $('#helpBtn').click(function(e){
        global.viewModKBArticle($('#module').val());
    });

    $('#startDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#endDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });


    $('#posAccessModal').on('hide.bs.modal', function (){
        announce.serializePositions();
    });


    $('#locAccessModal').on('hide.bs.modal', function (){

        // 2 = territories
        if ($('#sendToType :selected').val() == 2)
        {
            announce.serializeTerritories();
        }

        // 3 = locations
        if ($('#sendToType :selected').val() == 3)
        {
            announce.serializeLocations();
        }
    });


    $('#saveBtn').click(function(e){
        announce.checkEditForm();
    });

    // hidden id input exists - they are editing an announcement
    if ($('#id').exists())
    {
        announce.positionAccessInit();


        // 2 = territories
        if ($('#sendToType :selected').val() == 2)
        {
            announce.territoryAccessInit();
        }

        // 3 = locations
        if ($('#sendToType :selected').val() == 3)
        {
            announce.locationAccessInit();
        }

    }
}

announce.viewInit = function ()
{
    $('#returnBtn').click(function(e){
        window.location = '/announce';
        $(this).attr('disabled', 'disabled');
    });
}

announce.positionAccessInit = function ()
{

    $('#posAccessInputs').find("input").each(function(index, item)
    {
        var id = $(item).val();

        if ($('#posAccessBtn_' + id).exists())
        {
            $('#posAccessBtn_' + id).removeClass('btn-default');
            $('#posAccessBtn_' + id).addClass('btn-success');
        }
    });
}

announce.territoryAccessInit = function ()
{
    $('#terAccessInputs').find("input").each(function(index, item)
    {
        var id = $(item).val();

        if ($('#terAccessBtn_' + id).exists())
        {
            $('#terAccessBtn_' + id).removeClass('btn-default');
            $('#terAccessBtn_' + id).addClass('btn-success');
        }
    });
}

announce.locationAccessInit = function ()
{
    $('#locAccessInputs').find("input").each(function(index, item)
    {
        var id = $(item).val();

        if ($('#locAccessBtn_' + id).exists())
        {
            $('#locAccessBtn_' + id).removeClass('btn-default');
            $('#locAccessBtn_' + id).addClass('btn-success');
        }
    });
}

announce.clearTerAndLocInputs = function ()
{
    $('#terAccessInputs').html('');
    $('#locAccessInputs').html('');
}

announce.renderSendToType = function ()
{
    var sendToType = $('#sendToType :selected').val();

    if (sendToType == '')
    {
        global.renderAlert('Please select how you wish to distribute this announcement.', undefined, 'locAccessAlert');
        $('#sendToType').effect(global.effect);
        $('#sendToType').focus();

        $('#sendToTypeDisplay').html(''); // clears HTML
        return false;
    }

    if (sendToType == 1)
    {
        $('#sendToTypeDisplay').html('');
        return true;
    }

    global.ajaxLoader('#sendToTypeDisplay');

    $.get("/announce/sendtotype/" + String(sendToType), function(data){
        $('#sendToTypeDisplay').html(data);
    });
}

announce.serializePositions = function ()
{
    // clears hidden input div
    $('#posAccessInputs').html('');


    $('#posAccessBody').find("button").each(function(index, item)
    {
        if ($(item).hasClass('btn-success'))
        {
            var id = $(item).attr('value');

            $('#posAccessInputs').append("<input type='hidden' name='position[]' id='position_" + id + "' value='" + id + "'>");
        }
    });
}

announce.serializeLocations = function ()
{
    announce.clearTerAndLocInputs();

    $('#locAccessBody').find("button").each(function(index, item)
    {
        if ($(item).hasClass('btn-success'))
        {
            var id = $(item).attr('value');

            $('#locAccessInputs').append("<input type='hidden' name='location[]' id='location_" + id + "' value='" + id + "'>");
        }
    });
}

announce.serializeTerritories = function ()
{
    announce.clearTerAndLocInputs();

    $('#terAccessBody').find("button").each(function(index, item)
    {
        if ($(item).hasClass('btn-success'))
        {
            var id = $(item).attr('value');

            $('#terAccessInputs').append("<input type='hidden' name='territory[]' id='territory_" + id + "' value='" + id + "'>");
        }
    });
}

announce.checkEditForm = function ()
{
    if ($('#title').val() == '')
    {
        global.renderAlert("Please enter a title or subject for this announcement!");
        $('#title').effect(global.effect);
        $('#title').focus();
        return false;
    }

    // checks if a position is set to view the announcement
    var posChecked = false;

    $('#posAccessBody').find("button").each(function(index, item)
    {
        if ($(item).hasClass('btn-success'))
        {
            posChecked = true;
            return true;
        }
    });

    if (posChecked == false)
    {
        $('#posAccessModal').modal('show');
        global.renderAlert("Please select at least one position that has access to view this announcement!", undefined, 'posAccessAlert');
        return false;
    }

    if ($('#sendToType :selected').val() == '')
    {
        $('#locAccessModal').modal('show');
        global.renderAlert("Please select a type of coverage to send this announcement to!", undefined, 'locAccessAlert');
        return false;
    }

    // they have selected territory access
    if ($('#sendToType :selected').val() == '2')
    {
        var terChecked = false;

        $('#terAccessBody').find("button").each(function(index, item)
        {
            if ($(item).hasClass('btn-success'))
            {
                terChecked = true;
                return true;
            }
        });

        if (terChecked == false)
        {
            $('#locAccessModal').modal('show');
            global.renderAlert("Please select at least one territory that has access to view this announcement!", undefined, 'locAccessAlert');
            return false;
        }
    }


    // they have selected location access
    if ($('#sendToType :selected').val() == '3')
    {
        var locChecked = false;

        $('#locAccessBody').find("button").each(function(index, item)
        {
            if ($(item).hasClass('btn-success'))
            {
                locChecked = true;
                return true;
            }
        });

        if (locChecked == false)
        {
            $('#locAccessModal').modal('show');
            global.renderAlert("Please select at least one location that has access to view this announcement!", undefined, 'locAccessAlert');
            return false;
        }
    }


    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['announcement'].getData();
    $('#announcement').val(str);

    $.post("/announce/save", $('#editForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            window.location = '/announce?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
        }
        else if (data.status == 'ERROR')
        {
            global.renderAlert(data.msg, 'alert-error');
        }
    }, 'json');

}


announce.view = function (id)
{
    window.location = '/announce/view/' + id;
}
