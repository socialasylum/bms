var docs = {}

docs.indexInit = function()
{

    folders.folderInit('#doc-container', 'docs');
    docs.setFileDraggable(false);

    // goes through each span1 div in doc-container and makes draggable/droppable
    $('#doc-container').find("li").each(function(index, item)
    {
            // documents
            if ($(item).attr('itemType') == '2')
            {
                // if they double click on a document
                $(item).dblclick(function()
                {
                    $(item).attr('disabled', 'disabled');
                    // window.location = "/docs/view/" + $(item).attr('value') + "?folder=" + $('#folder').val() + "&menuId=" + $('#menuId').val();
                    window.location = "/docs/view/" + $(item).attr('value') + '/' + $('#folder').val() + '/' + $('#rootFolder').val();
                });

                var id = $(item).attr('value');

                // when the user right clicks on the screen
                $(item).mousedown(function(e){
                    switch (e.which)
                    {
                        case 3:
                        docs.indexRightClick(e, $(item), id);
                        break;
                    }
                });
            }

    });


    $('#createFolderBtn').click(function(e){
        // docs.checkNewFolderForm();
        folders.createFolder();
    });

    $('#createDocBtn').click(function(e){
        docs.checkNewDocForm();
    });
}

docs.editInit = function()
{

    global.containerResize($('#doc-container'), 300);

    $(window).resize(function(){
        global.containerResize($('#doc-container'), 300);
    });

    docs.body = CKEDITOR.replace('body');
    CKEDITOR.config.extraPlugins = 'maxheight';
    /*
    docs.body.getCommand('save').exec = function(e)
    {
        alert('save');
        return true;
    };
    */
    $('#saveBtn').click(function(e){
        docs.saveDocument();
    });

    $('#exitBtn').click(function(e){
        if (confirm("Are you sure you wish to exit?"))
        {
            var folder = ($('#folder').val() == '') ? '' : '/index/' + $('#folder').val();
            window.location = '/docs' + folder
        }
    });

    $('#startDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#endDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    // docs.editToopTips();

    $('#selectAll').click(function(e){
        docs.toggleSelectAllPositions();
    });

    if ($('#player').exists())
    {
        /*
        flowplayer("player", "/public/flowplayer/flowplayer-3.2.16.swf",{
            clip:
            {
                autoPlay:false,
                autoBuffering: true
            },

            content:
            {
                padding:15
            }
        });
        */
    }

    $('#sendToType').change(function(e){
        docs.getSendToDisplay($('option:selected', this).val());
    });

    if ($('#sendToType :selected').val() !== '')
    {
        docs.getSendToDisplay($('#sendToType :selected').val());
    }

}

docs.viewInit = function()
{
    $('#returnBtn').click(function(e){

        if ($('#folder').val() !== '') var folder = '/index/' + $('#folder').val() + '/' + $('#rootFolder').val();
        else var folder = '';

        window.location = "/docs" + folder;
        $(this).attr('disabled', 'disabled');
    });


    $('#printBtn').click(function(e){
        window.open("/docs/preview/" + $('#id').val() + "/" + $('#version').val(), '_blank');
    });

    $('#signDocBtn, #ackDocBtn').click(function(e){
        docs.saveDocAck();
    });


    if ($('#player').exists())
    {
        flowplayer("player", "/public/flowplayer/flowplayer-3.2.16.swf",{
            clip:
            {
                autoPlay:false,
                autoBuffering: true
            },

            content:
            {
                padding:15
            }
        });
    }
}


docs.checkNewFolderForm = function()
{
    if ($('#folderName').val() == '')
    {
        global.renderAlert("Please enter a folder name!", undefined, 'folderAlert');
        return false;
    }


    $.post("/docs/createfolder", $('#createFolderForm').serialize(), function(data){

        var folder = '';

        if (data.status == 'SUCCESS')
        {
            // clears alert
            global.renderAlert(undefined, undefined, 'folderAlert');

            $('#createFolderModal').modal('hide');
            $('#folderName').val('');

            if ($('#folder').val() != '')
            {
                folder = '/' + $('#folder').val();
            }


            window.location = "/docs/index" + folder;
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'folderAlert');
            $('#createFolderBtn').removeAttr('disabled');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'folderAlert');
            $('#createFolderBtn').removeAttr('disabled');
            return false;
        }

    }, 'json');
}

docs.checkNewDocForm = function ()
{
    if ($('#docName').val() == '')
    {
        global.renderAlert("Please enter a document name!", undefined, 'docAlert');
        return false;
    }

    $('#createDocBtn').attr('disabled', 'disabled');

    window.location = "/docs/edit?title=" + escape($('#docName').val()) + "&folder=" + $('#folder').val();
}

docs.saveDocument = function ()
{
    if ($('#title').val() == '')
    {
        // global.renderAlert("Please enter a title!");
        global.alertDialog("Please enter a title");
        return false;
    }

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['body'].getData();
    $('#body').val(str);

    $.post("/docs/save", $('#docForm').serialize(), function(data){
        if (data.status == 'SUCCESS')
        {
            // alert('Document has been saved!');
            global.renderAlert("Document has been saved!");
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }

    }, 'json');
}

docs.setFileSortable = function (clearPreviousSelectable)
{

        if (clearPreviousSelectable == undefined) clearPreviousSelectable = false;

        if (clearPreviousSelectable == true)
        {
            $('#fileList .ui-selected').removeClass('ui-selected')

            // removes previous functionality
            $('#fileList').selectable('destroy');
        }

        // goes through each li and for folders makes it so they are not sortable
        $('#fileList').find("li").each(function(index, item)
        {
            // folders
            if ($(item).attr('itemType') == '1')
            {
                $(item).addClass('ui-state-disabled');
            }
        });


        $('#fileList').sortable({
            placeholder: "ui-state-highlight",
            items: "li:not(.ui-state-disabled)"
        });

        $('#fileList').disableSelection();

}

docs.setFileDraggable = function(clearPreviousSortable)
{
        if (clearPreviousSortable == undefined) clearPreviousSortable = false; // deafults to false

        if (clearPreviousSortable == true)
        {
            // removes previous functionality
            $('#fileList').sortable('destroy');
        }

        // goes through each li and for folders makes it so they are not disbled
        $('#fileList').find("li").each(function(index, item)
        {
            // folders
            if ($(item).attr('itemType') == '1')
            {
                $(item).removeClass('ui-state-disabled');
            }
        });

        $('#fileList').selectable({
            filter: "li",
            selected: function (event, ui)
            {
                // clears previous right click if showing
                if ($('#right-click-menu').exists())
                {
                    $('#right-click-menu').remove();
                }

                    $(ui.selected).draggable({
                    start: function (e, u)
                    {
                        // $(this).addClass('moving');
                    },
                    stop: function (e, u)
                    {
                        // $(this).removeClass('moving');
                    },
                    helper: function(){

                        var selected = $('#fileList').find('.ui-selected');

                        if (selected.length === 0)
                        {
                            selected = $(this);
                        }
                        var container = $('<div/>').attr('id', 'draggingContainer');
                        var clone = selected.clone();
                        clone.addClass('moving');
                        container.append(clone);
                        return container;
                        }

                    });
            },
            unselected: function (event, ui)
            {
                $(ui.unselected).draggable('destroy');
            }
        });

}

docs.indexRightClick = function(e, item, id)
{
    docs.clearRightClick();

    var html = "<div class='dropdown'>" +
        "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.editDocument(" + id + ");\"><i class='fa fa-pencil'></i> Edit</a></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"window.open('/docs/preview/" + id + "', '_blank');\"><i class='fa fa-print'></i> Print</a></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.getProperties(" + id + ");\"><i class='fa fa-cog'></i> Properties</a></li>" +
        "<li class='divider'></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.deleteFile(" + id + ");\"><i class='fa fa-trash-o'></i> Delete</a></li>" +
        "</ul>" +
        "</div>";


        if (docs.admin == false)
        {

    var html = "<div class='dropdown'>" +
        "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"window.open('/docs/preview/" + id + "', '_blank');\"><i class='fa fa-print'></i> Print</a></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.getProperties(" + id + ");\"><i class='fa fa-cog'></i> Properties</a></li>" +
        "</ul>" +
        "</div>";
        }

    // right click menu for folders
    if (item.attr('itemType') == 1)
    {
    var html = "<div class='dropdown'>" +
        "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.deleteFolder(" + id + ");\"><i class='fa fa-trash-o'></i> Delete</a></li>" +
        "</ul>" +
        "</div>";

        if (docs.admin == false) html = '';

    }

    // right click menu for PDF's and Videos
    if ((item.attr('itemType') == 2 && item.attr('documentType') == 3) || (item.attr('itemType') == 2 && item.attr('documentType') == 11))
    {
    var html = "<div class='dropdown'>" +
        "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.editDocument(" + id + ");\"><i class='fa fa-pencil'></i> Edit</a></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.getProperties(" + id + ");\"><i class='fa fa-cog'></i> Properties</a></li>" +
        "<li class='divider'></li>" +
        "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.deleteFile(" + id + ");\"><i class='fa fa-trash-o'></i> Delete</a></li>" +
        "</ul>" +
        "</div>";

        if (docs.admin == false)
        {
            var html = "<div class='dropdown'>" +
                "<ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" +
                "<li><a tabindex='-1' href=\"javascript:void(0);\" onclick=\"docs.getProperties(" + id + ");\"><i class='fa fa-cog'></i> Properties</a></li>" +
                "</ul>" +
                "</div>";
        }
    }
    var parentOffset = item.parent().offset();
    //or $(this).offset(); if you really just want the current element's offset
    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;

    $('body').append("<div id='right-click-menu' style=\"top:" + String(e.pageY)  + "px;left:" + String(e.pageX) + "px;\">" + html + "</div>");
    // $('#rightClickDD').dropdown('toggle');
}

docs.clearRightClick = function()
{
    // clears previous right click if showing
    if ($('#right-click-menu').exists())
    {
        $('#right-click-menu').remove();
    }
}


docs.editDocument = function (id)
{
     window.location = "/docs/edit/" + id + "?folder=" + $('#folder').val();
}

docs.getProperties = function (id)
{
    $.get("/docs/properties/" + id, function(data){
        $('#propertiesModal').html(data);

        $('#propertiesModal').modal('show');

        // $('#docVersionTbl').dataTable();

        docs.clearRightClick();
    });
}

docs.deleteFile = function(id)
{
    if (confirm("Are you sure you wish to delete this file?"))
    {
        $.post("/docs/deleteFile", { id: id, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                $('#doc_' + id).hide();
                docs.clearRightClick();
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
        }, 'json');

    }
}

docs.getSendToDisplay = function (code)
{
    global.ajaxLoader('#sendTo-display');

    $.get("/docs/getSendToDisplay/" + code + '/' + $('#id').val(), function(data){
        $('#sendTo-display').html(data);
    });
}

docs.saveDocAck = function ()
{
    if ($('#ack').val() == 1) // requires signature
    {
        // checks if user entered a password
        if ($('#password').val() == '')
        {
            global.renderAlert("Please enter your password", undefined, 'ackAlert');
            $('#password').focus();
            $('#password').effect(global.effect);
            return false;
        }
    }

    $.post("/docs/saveack", $('#docViewForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/docs/view/' + $('#id').val() + '/' + $('#folder').val() + '/' + $('#rootFolder').val() + '?site-success=' + escape(data.msg);
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg, undefined, 'ackAlert');
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger', 'ackAlert');
                return false;
            }
    }, 'json');
}
