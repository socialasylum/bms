var pos = {}


pos.ccNumber;
pos.ccExp;
pos.cvv;

/**
 * 1 - POS just initalized - employee needs to enter password to continue
 */
pos.tranStatus = 1; // flag for the process of the transaction

pos.indexInit = function ()
{

    $(window).resize(function(){
        pos.containerResize();
    });

    // pos.loadNumbersPanel();


    pos.loadActions();

    pos.renderTrans();

    pos.containerResize();

    global.slimScroll('#pos-sales-content');

    // pos.inputPassword();

    // executes function on resizing of window
    $(window).resize(function(){
        pos.containerResize();
    });

    $(window).load(function(){
        pos.containerResize();
    });
}


pos.containerResize = function ()
{
    var h = $(window).height() - 100;

    $('#pos-content').css('min-height', h + 'px');

    // $('#pos-content').find('.panel').css('min-height', h + 'px');
    
    $('#pos-sales-content').css('min-height', (h - 140) + 'px');

    // var rowh = ($('#pos-content').height() / 4);
    var rowh = (h / 4);

    $('#pos-content').find('.row').css('min-height', rowh + 'px');

    $('.pos-numbers').find('.col-4').css('min-height', rowh + 'px');
    $('.pos-numbers').find('button').css('min-height', (rowh - 10) + 'px');

    $('.pos-numbers').find('button').css('min-width', '100%');

}

pos.setHeading = function (html)
{
    $('#pos-heading').html(html);
}


pos.loadNumbersPanel = function (enterFunction)
{
    // global.ajaxLoader('#rightCol');

    $.get("/pos/numbers", function(data){
        $('#rightCol').html(data);
   
        // unbinds enter btn
        $('#btnEnter').unbind();

        $('#btnEnter').click(function(e){
            if (typeof enterFunction === 'function')
            {
                enterFunction();
            }
        });
        pos.containerResize();
    });


    // loads keyboard listeners
    
    //first unbinds window
    $(window).unbind();

    $(window).keypress(function(e){
        console.log(e.which);

        var txt = $('#posInput').val();

        if (e.which == 13)
        {
            if (typeof enterFunction === 'function')
            {
                enterFunction();
            }
            // pos.checkPassword();
        }

        // 8 = backspace
        if (e.which == 8) pos.delPosChar();

        if (e.which == 48) pos.addPosInput('0');
        if (e.which == 49) pos.addPosInput('1');
        if (e.which == 50) pos.addPosInput('2');
        if (e.which == 51) pos.addPosInput('3');
        if (e.which == 52) pos.addPosInput('4');
        if (e.which == 53) pos.addPosInput('5');
        if (e.which == 54) pos.addPosInput('6');
        if (e.which == 55) pos.addPosInput('7');
        if (e.which == 56) pos.addPosInput('8');
        if (e.which == 57) pos.addPosInput('9');

    });
}

pos.loadActions = function ()
{
    // global.ajaxLoader('#rightCol');

    $.get("/pos/actions", function(data){
        $('#rightCol').html(data);
        pos.containerResize();
    });
}


pos.loadItems = function (folder)
{
    // global.ajaxLoader('#rightCol');


    if (folder == undefined) folder = 0;

    $.get("/pos/items/" + folder, function(data){
        $('#rightCol').html(data);
        pos.containerResize();
    });
}

/**
 * This is the panel in which user selects payment type
 * in order to complete the transaction
 */
pos.loadFinishTrans = function ()
{
    // global.ajaxLoader('#rightCol');

    $.get("/pos/finishtrans", function(data){
        $('#rightCol').html(data);
        pos.containerResize();
    });
}

pos.loadPostTrans = function ()
{
    $.get("/pos/posttrans", function(data){
        $('#rightCol').html(data);
        pos.containerResize();
    });
}

pos.addPosInput = function (c)
{
    console.log(c);
    var txt = $('#posInput').val();

    $('#posInput').val(txt + c);
}

pos.delPosChar = function ()
{

    var txt = $('#posInput').val();

    // console.log('new String: ' + txt.substring(-1));

    $('#posInput').val(txt.substring(0, (txt.length -1) ));

}

pos.inputPassword = function ()
{
    $('#posInput').attr('type', 'password');
}

pos.checkPassword = function ()
{
    $.post("/pos/pwcheck", { password: $('#posInput').val(), cgi_token: global.CSRF_hash  }, function(data){

        if (data.status == 'SUCCESS')
        {

        }
        else if (data.status == 'ALERT')
        {
            alert(data.msg);
            return false;
        }
        else
        {
            return false;
        }


    }, 'json');
}


pos.renderTrans = function ()
{
    // global.ajaxLoader('#pos-sales-content');

    $.get("/pos/trans", function(data){
        $('#pos-sales-content').html(data);
    });
}

pos.addItem = function (id)
{
    $.post("/pos/additem", { item: id, cgi_token: global.CSRF_hash }, function(data){
            if (data.status == 'SUCCESS')
            {
                pos.renderTrans();
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
    }, 'json');
}

pos.deleteItem = function (b, limit)
{
    if (confirm("Are you sure you wish to remove " + $(b).attr('itemName') + "?"))
    {
        if (limit == undefined) limit = 0;

        $.getJSON("/pos/removeitem/" + $(b).attr('itemID') + '/' + limit, function(data){
            if (data.status == 'SUCCESS')
            {
                pos.renderTrans();
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
                return false;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-error');
                return false;
            }
        });
    }
}

pos.payCC = function ()
{
    var enterFunction = function ()
    {
        pos.ccNumber = $('#posInput').val();

        // clears input value
        $('#posInput').val('');

        pos.getcardExpMonth();
    };

    // renders numbers panel
    pos.loadNumbersPanel(enterFunction);

    pos.setHeading("Please enter the card number");

}

/**
 * function actually used to get entire cc
 */
pos.getcardExpMonth = function ()
{
    var enterFunction = function ()
    {
        // check length of input to ensure its the right size (thats what she said)
        if ($('#posInput').length !== 6)
        {
            alert("Expiration Date is invalid!");
            return false;
        }

        pos.ccExp = $('#posInput').val();

        // clears input value
        $('#posInput').val('');

        pos.getCardCvv();

    };

    pos.loadNumbersPanel(enterFunction);

    pos.setHeading('Enter Card Expiration MMYYYY (Example: 022014)');
}

pos.getCardCvv = function ()
{
    var enterFunction = function ()
    {
        pos.cvv = $('#posInput').val();

        // clears input value
        $('#posInput').val('');

        pos.processOrder();
    };

    pos.loadNumbersPanel(enterFunction);

    pos.setHeading('Enter Card Expiration CVV (Example: 823)');

}

pos.processOrder = function ()
{
    // pos.setHeading('Transaction Complete');
    pos.loadPostTrans();

    $.post("/pos/proccessorder", { ccNumber:pos.ccNumber, ccExp:pos.ccExp, cvv:pos.cvv, cgi_token:global.CSRF_hash }, function(data){
        if (data.status == 'SUCCESS')
        {
            pos.setHeading('Transaction successlly complete!');

            $.get("/pos/receipt", function(data){
                $('#pos-sales-content').html(data);
            });
        }
        else
        {
            // send them back to payment panel
            pos.loadFinishTrans();
        }

            // clear out data
            pos.ccNumber = 0;
            pos.ccExp = 0;
            pos.cvv = 0;

    }, 'json');

    // global.ajaxLoader('#pos-sales-content');
}
