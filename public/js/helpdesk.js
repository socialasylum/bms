var helpdesk = {}

helpdesk.indexInit = function ()
{
    $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('#submitBtn').click(function(e){
        helpdesk.checkTicketForm();
    });

    $('#department').change(function(e){
    helpdesk.getDepartmentTechs('#departmentTechsDisplay', $('#department :selected').val(), '3');
    });
}

helpdesk.myTicketsInit = function()
{
    $('#myTicketsTable').dataTable();
}

helpdesk.depTicketsInit = function()
{
    $('#depTicketsTable').dataTable();
}

helpdesk.myTechTicketsInit = function()
{
    $('#myTechTicketsTable').dataTable();
}

helpdesk.editTicketInit = function()
{
    $('#updateBtn').click(function(e){
        helpdesk.updateTicket();
    });

    $('#department').change(function(e){
    helpdesk.getDepartmentTechs('#departmentTechsDisplay', $('#department :selected').val(), '2');
    });
}

helpdesk.getDepartmentTechs = function(divId, departmentId, colSize)
{
    $.get('/helpdesk/getDepartmentTechs/' + departmentId + '/' + colSize, function(data){
    $(divId).html(data);
    });
}

helpdesk.updateTicket = function()
{

    if ($('#note').val() == '')
    {
        global.renderAlert("Please Add A Note!!");
        $('#note').focus();
        return;
    }

    $('#updateBtn').attr('disabled', 'disabled');

    $.post("/helpdesk/insertNote", $('#editTicket').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                window.location = '/helpdesk/editTicket/'+$('#id').val()+'?site-success='+escape('Note has been added');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');

}



helpdesk.checkTicketForm = function()
{

    if ($('#department').val() == '')
    {
        global.renderAlert("Please select a Department!!");
        $('#department').focus();
        return;
    }

    if ($('#datepicker').val() == '')
    {
        global.renderAlert("Please select a date!!");
        $('#datepicker').focus();
        return;
    }

    if ($('#subject').val() == '')
    {
        global.renderAlert("Please type a subject!");
        $('#subject').focus();
        return;
    }

    if ($('#issue').val() == '')
    {
        global.renderAlert("Please explain your issue!");
        $('#issue').focus();
        return;
    }

    $('#submitBtn').attr('disabled', 'disabled');

    $.post("/helpdesk/submitTicket", $('#createForm').serialize(), function(data){
            if (data.status == 'SUCCESS')
            {
                // window.location = '/users';
                global.renderAlert(data.msg, 'alert-success');
            }
            else if (data.status == 'ALERT')
            {
                global.renderAlert(data.msg);
            }
            else if (data.status == 'ERROR')
            {
                global.renderAlert(data.msg, 'alert-error');
            }
    }, 'json');

}
