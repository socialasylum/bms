
var ms = {};

ms.socketID;
ms.connected = false;

ms.getUserID = function ()
{
	return $('body').data('userid');
}

ms.validateUser = function (sf)
{
	$.getJSON('/intranet/validate/' + ms.getUserID(), function(data){
		if (data.status = 'SUCCESS')
		{
			if (sf !== undefined && typeof sf == 'function') sf(data);
		}
	});
}

ms.emit = function (type, data)
{
	if (ms.connected)
	{
		ms.socket.emit(type, data);
	}
	else
	{
		
	}
}


ms.processNotification = function (d)
{
	var type = d.type;
	
	if (type == 'new-message')
	{
		//Info("New Message from: " + d.from);
	}
}

ms.load = function (io)
{

if (ms.getUserID() !== undefined)
{
	ms.validateUser(function(){
	
		//msg.socket = io.connect('http://107.170.235.121:3000');
		
		ms.socket = io('107.170.235.121:3000', {'connect timeout': 3000});
		
		ms.socket.on('connect', function(){

			ms.connected = true;
			
			ms.socket.on('socketID', function (id){
				ms.socketID = id;
			});
		
			ms.emit('adduser', ms.getUserID());
			
			//ms.socket.emit('adduser', ms.getUserID());
			
			ms.socket.on('event', function(data){
				
				console.log("Event: " + data);
				
			});
			
			ms.socket.on('update-users', function (data){
				console.log(data);
			});
			
			ms.socket.on('disconnect', function(){
				ms.connected = false;
						
				//console.log("Disconnected from server");	
			});
		
			ms.socket.on('data-received', function (d){
				$(window).trigger("socket.data-received", d);
			});
		
			ms.socket.on('notification', function (d){
				
				//ms.processNotification(d);
				$(window).trigger("socket.notification", d);
			});
		
		});
		
	});


}

}