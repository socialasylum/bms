var codes = {}

codes.indexInit = function ()
{
    $('#group').change(function(){
        codes.loadGroupDisplay();
    });


    $('#createGroupBtn').click(function(e){
        codes.createCodeGroup();
    });


    if ($('#group :selected').val() > 0)
    {
        codes.loadGroupDisplay();
    }
}

codes.cancelEditing = function ()
{
    if (confirm("Are you sure you wish to cancel? You will lose any changes you've made!"))
    {
        codes.loadGroupDisplay();
        global.renderAlert('Code group has been reloaded.', 'alert-info');
    }
}

codes.loadGroupDisplay = function ()
{
    global.ajaxLoader('#codes-display');

    $.get("/codes/codegroupdisplay/" + $('#group :selected').val(), function(data){
        $('#codes-display').effect('highlight');
        $('#codes-display').html(data);

        $('#saveBtn').click(function(e){
            codes.saveCodeGroup();
        });

        $('#addBtn').click(function(e){
            codes.addNewCode();
        });

        $('#cancelBtn').click(function(e){
            codes.cancelEditing();
        });
    });
}

codes.addNewCode = function ()
{
    var html = '';

    html = $('#master-code-panel').html();

    $('#add-code-display').html(html);

    $('#add-code-display').show('highlight');


    $('#addBtn').hide(global.hideEffect);

    $('#add-code-display .panel-footer').last().hide(global.hideEffect);

    $('#add-code-display input').last().val('');
    $('#add-code-display input').last().removeAttr('disabled');

    $('#add-code-display div#hiddenInputs').last().html('');


    // changes name of display input
    $('#add-code-display input#display').last().attr('name', 'newDisplay');

    // changes name of company access select
    $('#add-code-display select#company').last().attr('name', 'newCompany');


    // $('#add-code-display select').last().val('');
    $('#add-code-display select :nth-child(0)').last().prop('selected', true);
    $('#add-code-display select').last().removeAttr('disabled');

    $('#add-code-display #newCodeNumber').last().html("<span class='muted'>Undefined</span>");
}

codes.deactiveCode = function (item)
{
    $(item).hide('highlight');

    $(item).parent().find('#actBtn').show('highlight');

    // disables company access select box
    $(item).parent().parent().find('select').attr('disabled', 'disabled');
    $(item).parent().parent().find('input#display').attr('disabled', 'disabled');


    // sets hidden status input to deacivated
    $(item).parent().parent().find('input#active').val('0');

}

codes.activeCode = function (item)
{
    // hides highlight button
    $(item).hide('highlight');

    $(item).parent().find('#deactBtn').show('highlight');

    $(item).parent().parent().find('select').removeAttr('disabled');
    $(item).parent().parent().find('input#display').removeAttr('disabled');


    // sets hidden status input to deacivated
    $(item).parent().parent().find('input#active').val('1');
}

codes.saveCodeGroup = function ()
{
    if (!confirm("Are you sure you wish to save these changes to this code group?"))
    {
        return false;
    }

    $('#saveBtn').attr('disabled', 'disabled');
    $('#cancelBtn').hide('highlight');
    $('#addBtn').hide('highlight');

    $.post("/codes/save", $('#codeGroupForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');
            codes.loadGroupDisplay();
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error');
            return false;
        }
    }, 'json');
}

codes.createCodeGroup = function ()
{
    if ($('#groupName').val() == '')
    {
        global.renderAlert('Please enter a code group name!', undefined, 'codeGroupAlert');
        $('#groupName').effect('highlight');
        $('#groupName').focus();
        return false;
    }

    if (!confirm("Are you sure you wish to create this code group?"))
    {
        return false;
    }

    $.post("/codes/creategroup", $('#newGroupForm').serialize(), function(data){

        if (data.status == 'SUCCESS')
        {
            window.location = "/codes/index/" + data.id + '?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'codeGroupAlert');
            return false;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'codeGroupAlert');
            return false;
        }
    }, 'json');
}
