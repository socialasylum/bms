DROP TABLE IF EXISTS `formElements`;

CREATE TABLE `formElements` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(50) DEFAULT NULL,
    `placeholder` boolean DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO formElements (type, placeholder) VALUES
('text', 1),
('select', 1),
('textarea', 1),
('password', 1),
('radio', 1),
('checkbox', 1);
