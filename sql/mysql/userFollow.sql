DROP TABLE IF EXISTS `userFollow`;

CREATE TABLE `userFollow` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` DATETIME DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `followingUser` INT(10) UNSIGNED DEFAULT 0,
    `active` BINARY DEFAULT 0,
    `dateApproved` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX userFollowIndex ON userFollow (userid, followingUser);