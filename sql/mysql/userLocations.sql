DROP TABLE IF EXISTS `userLocations`;

CREATE TABLE `userLocations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location` int(10) unsigned DEFAULT 0,
  `userid` int(10) unsigned DEFAULT 0,
  `homeLocation` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX userLocationIndex ON userLocations (location, userid);
