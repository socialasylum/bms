DROP TABLE IF EXISTS `newhire`;

CREATE TABLE `newhire` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `document` INT(10) UNSIGNED DEFAULT 0,
  `day` SMALLINT(5) UNSIGNED DEFAULT 0,
  `order` SMALLINT(5) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX newhireIndex ON newhire (company, document, day);
