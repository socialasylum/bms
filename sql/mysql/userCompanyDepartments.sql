DROP TABLE IF EXISTS `userCompanyDepartments`;

CREATE TABLE `userCompanyDepartments` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `department` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX userCompanyDepartmentsIndex ON userCompanyDepartments (userid, company);
