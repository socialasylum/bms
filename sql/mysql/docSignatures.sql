DROP TABLE IF EXISTS `docSignatures`;

CREATE TABLE `docSignatures` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `docId` INT(10) UNSIGNED DEFAULT 0,
    `version` SMALLINT(5) UNSIGNED DEFAULT 0,
    `ackType` SMALLINT(5) UNSIGNED DEFAULT 0,
    `datestamp` DATETIME DEFAULT NULL,
    `resign` BINARY DEFAULT 0,
    `resignBy` INT(10) UNSIGNED DEFAULT 0,
    `resignDate` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
