DROP TABLE IF EXISTS `videoViews`;

CREATE TABLE `videoViews` (
  `videoId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned DEFAULT 0,
  `datestamp` DATETIME DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX videoViewIndex ON videoViews (`videoId`, `userId`, `datestamp`);
