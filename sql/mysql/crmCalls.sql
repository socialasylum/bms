DROP TABLE IF EXISTS `crmCalls`;

CREATE TABLE `crmCalls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contactId` INT(10) UNSIGNED NOT NULL,
  `datestamp` datetime DEFAULT NULL,
  `calldate` datetime DEFAULT NULL,
  `type` smallint(5) unsigned DEFAULT 0,
  `status` smallint(5) unsigned DEFAULT 0,
  `notes` TEXT DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
