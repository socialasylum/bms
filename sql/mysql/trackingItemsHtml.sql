DROP TABLE IF EXISTS `trackingItemsHtml`;

CREATE TABLE `trackingItemsHtml` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `trackingItemID` INT(10) UNSIGNED DEFAULT 0,
    `datestamp` datetime DEFAULT NULL,
    `fileName` varchar(250) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
