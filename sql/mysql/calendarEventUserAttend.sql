DROP TABLE IF EXISTS `calendarEventUserAttend`;

CREATE TABLE `calendarEventUserAttend` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`event` INT(10) UNSIGNED NOT NULL,
	`datestamp` datetime DEFAULT NULL,
	`userid` INT(10) UNSIGNED NOT NULL,
	`dateAttending` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
