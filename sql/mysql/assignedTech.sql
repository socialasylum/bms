DROP TABLE IF EXISTS `assignedTech`;

CREATE TABLE `assignedTech` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticketid` int(10) unsigned DEFAULT 0,
  `userid` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX assignedTechIndex ON assignedTech (ticketid, userid);
