DROP TABLE IF EXISTS `appEdu`;

CREATE TABLE `appEdu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `appId` INT(10) UNSIGNED NOT NULL,
  `type` smallint(5) unsigned DEFAULT NULL,
  `desc` TEXT DEFAULT NULL,
  `graduate` BINARY DEFAULT 0,
  `years` smallint(5) unsigned DEFAULT 0,
  `studied` VARCHAR(300) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


