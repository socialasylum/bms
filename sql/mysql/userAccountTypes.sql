drop table if exists `userAccountTypes`;

create table `userAccountTypes` (
    `id` int(10) unsigned not null auto_increment,
    `userid` int(10) unsigned default 0,
    `company` int(10) unsigned default 0,
    `accountType` SMALLINT(5) UNSIGNED DEFAULT 0,
  primary key (`id`)
) engine=innodb auto_increment=1 default charset=latin1;


CREATE UNIQUE INDEX userAccountTypesIndex ON userAccountTypes (userid, company);
