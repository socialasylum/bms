DROP TABLE IF EXISTS `datasetFolderAssign`;

CREATE TABLE `datasetFolderAssign` (
  `datasetId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`datasetId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
