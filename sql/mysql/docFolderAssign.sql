DROP TABLE IF EXISTS `docFolderAssign`;

CREATE TABLE `docFolderAssign` (
  `docId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
  `docOrder` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`docId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


