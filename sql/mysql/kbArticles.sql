DROP TABLE IF EXISTS `kbArticles`;

CREATE TABLE `kbArticles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `userId` INT(10) UNSIGNED DEFAULT 0,
  `title` varchar(250) DEFAULT NULL,
  `active` BOOLEAN DEFAULT 1,
  `deleted` BOOLEAN DEFAULT 0,
  `deletedBy` INT(10) UNSIGNED DEFAULT NULL,
  `deleteDate` DATETIME DEFAULT NULL,
  `module` INT(10) UNSIGNED DEFAULT 0,
  `globalArticle` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
