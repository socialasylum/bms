DROP TABLE IF EXISTS `formApprovals`;

CREATE TABLE `formApprovals` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `form` INT(10) UNSIGNED DEFAULT 0,
    `lvl` INT(10) UNSIGNED DEFAULT 0,
    `position` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX formApprovalsIndex ON formApprovals (form, lvl, position);
