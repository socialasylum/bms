DROP TABLE IF EXISTS `annPositionAccess`;

CREATE TABLE `annPositionAccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `announcement` INT(10) UNSIGNED DEFAULT 0,
  `position` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX annPositionAccessIndex ON annPositionAccess (announcement, position);
