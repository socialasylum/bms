DROP TABLE IF EXISTS `modulePermissions`;

CREATE TABLE `modulePermissions` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `bit` BIGINT UNSIGNED DEFAULT 0,
    `module` INT(10) UNSIGNED DEFAULT 0,
    `title` VARCHAR(150) DEFAULT NULL,
    `description` TEXT DEFAULT NULL,
    `admin` BINARY DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX modulePermIndex ON modulePermissions (bit, module);
