DROP TABLE IF EXISTS `companyModules`;

CREATE TABLE `companyModules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `module` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX companyModuleIndex ON companyModules (company, module);
