DROP TABLE IF EXISTS `reportFolderAssign`;

CREATE TABLE `reportFolderAssign` (
  `reportId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`reportId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
