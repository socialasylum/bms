DROP TABLE IF EXISTS `childPositions`;

CREATE TABLE `childPositions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` INT(10) UNSIGNED NOT NULL,
  `childPosition` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX positionIndex ON childPositions (position, childPosition);
