DROP TABLE IF EXISTS `userPhotoAlbums`;

CREATE TABLE `userPhotoAlbums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `name` varchar(250) DEFAULT NULL,
  `stream` BINARY DEFAULT 0,
  `mobile` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
