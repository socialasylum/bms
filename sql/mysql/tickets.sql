DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `issueDate` datetime DEFAULT NULL,
  `department` int DEFAULT NULL,
  `subject` varchar(250) DEFAULT NULL,
  `issue` text DEFAULT NULL,
  `status` int DEFAULT NULL,
  `priority` int DEFAULT NULL,
  `company` int DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

