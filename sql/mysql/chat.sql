DROP TABLE IF EXISTS `chat`;

CREATE TABLE `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` DATETIME DEFAULT NULL,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `from` INT(10) UNSIGNED DEFAULT 0,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `body` TEXT DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
