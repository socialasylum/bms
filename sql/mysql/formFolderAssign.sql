DROP TABLE IF EXISTS `formFolderAssign`;

CREATE TABLE `formFolderAssign` (
  `formId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
  `formOrder` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`formId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


