DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` DATETIME DEFAULT NULL,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `type` SMALLINT(5) DEFAULT 0,
  `extendedAmount` DOUBLE DEFAULT 0,
  `discountAmount` DOUBLE DEFAULT 0,
  `taxPercent` DOUBLE DEFAULT 0,
  `paymentType` SMALLINT(5) DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
