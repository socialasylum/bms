DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `name` VARCHAR(300) DEFAULT NULL,
    `description` TEXT DEFAULT NULL,
    `shortDesc` TEXT DEFAULT NULL,
    `url` VARCHAR(300) DEFAULT NULL,
    `namespace` VARCHAR(300) DEFAULT NULL,
    `price` DOUBLE DEFAULT 0,
    `active` BINARY DEFAULT 0,
    `icon` VARCHAR(50) DEFAULT NULL,
    `sellable` BINARY DEFAULT 1,
    `unvPackage` BINARY DEFAULT 0,
    `usesFolders` BINARY DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX namespaceIndex ON modules (namespace);
