DROP TABLE IF EXISTS `menuAccess`;

CREATE TABLE `menuAccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu` INT(10) UNSIGNED DEFAULT 0,
  `position` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX menuAccessIndex ON menuAccess (menu, position);
