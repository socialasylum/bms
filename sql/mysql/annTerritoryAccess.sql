DROP TABLE IF EXISTS `annTerritoryAccess`;

CREATE TABLE `annTerritoryAccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `announcement` INT(10) UNSIGNED DEFAULT 0,
  `territory` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX annTerritoryAccessIndex ON annTerritoryAccess (announcement, territory);
