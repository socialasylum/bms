DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticketID` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `note` text DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

