DROP TABLE IF EXISTS `openPositions`;

CREATE TABLE `openPositions` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `positionId` INT(10) UNSIGNED DEFAULT 0,
    `title` VARCHAR(300) DEFAULT NULL,
    `locationId` INT(10) UNSIGNED DEFAULT 0,
    `locationTxt` VARCHAR(300) DEFAULT NULL,
    `startDate` datetime DEFAULT NULL,
    `endDate` datetime DEFAULT NULL,
    `description` TEXT DEFAULT NULL,
    `deleted` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
