DROP TABLE IF EXISTS `locationReviews`;

CREATE TABLE `locationReviews` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `location` INT(10) UNSIGNED DEFAULT 0,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `datestamp` DATETIME DEFAULT NULL,
    `name` VARCHAR(300) DEFAULT NULL,
    `email` VARCHAR(300) DEFAULT NULL,
    `comment` TEXT DEFAULT NULL,
    `rating` SMALLINT(5) UNSIGNED DEFAULT 0,
    `autoImported` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

