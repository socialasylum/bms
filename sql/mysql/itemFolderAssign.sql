DROP TABLE IF EXISTS `itemFolderAssign`;

CREATE TABLE `itemFolderAssign` (
  `itemId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX itemFolderIndex ON itemFolderAssign (`itemId`, `folderId`);
