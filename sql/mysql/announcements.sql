DROP TABLE IF EXISTS `announcements`;

CREATE TABLE `announcements` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` DATETIME DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `title` VARCHAR(300) DEFAULT NULL,
    `body` TEXT DEFAULT NULL,
    `excerpt` TEXT DEFAULT NULL,
    `startDate` DATETIME DEFAULT NULL,
    `endDate` DATETIME DEFAULT NULL,
    `sendToType` SMALLINT(5) DEFAULT 0,
    `active` BINARY DEFAULT 1,
    `deleted` BINARY DEFAULT 0,
    `deletedBy` INT(10) UNSIGNED DEFAULT 0,
    `deletedDate` DATETIME DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
