DROP TABLE IF EXISTS `annLocationAccess`;

CREATE TABLE `annLocationAccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `announcement` INT(10) UNSIGNED DEFAULT 0,
  `location` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX annLocationAccessIndex ON annLocationAccess (announcement, location);
