DROP TABLE IF EXISTS `docCountry`;

CREATE TABLE `docCountry` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `docId` INT(10) UNSIGNED DEFAULT 0,
    `country` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX docCountryIndex ON docCountry (docId, country);
