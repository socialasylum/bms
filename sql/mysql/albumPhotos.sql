DROP TABLE IF EXISTS `albumPhotos`;

CREATE TABLE `albumPhotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `uploadedBy` INT(10) UNSIGNED DEFAULT 0,
  `album` INT(10) UNSIGNED DEFAULT 0,
  `fileName` varchar(300) DEFAULT NULL,
  `caption` TEXT DEFAULT NULL,
  `imgOrder` SMALLINT(5) UNSIGNED DEFAULT 0,
  `posted` BINARY DEFAULT 0,
  `postID` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
