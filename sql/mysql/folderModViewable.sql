DROP TABLE IF EXISTS `folderModViewable`;

CREATE TABLE `folderModViewable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `folder` int(10) unsigned DEFAULT 0,
  `module` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX folderModIndex ON folderModViewable (folder, module);
