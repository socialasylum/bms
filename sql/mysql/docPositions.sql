DROP TABLE IF EXISTS `docPositions`;

CREATE TABLE `docPositions` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `docId` INT(10) UNSIGNED DEFAULT 0,
    `position` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX docPositionIndex ON docPositions (docId, position);
