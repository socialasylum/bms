DROP TABLE IF EXISTS `territories`;

CREATE TABLE `territories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `createdBy` INT(10) UNSIGNED DEFAULT 0,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `name` varchar(250) DEFAULT NULL,
  `positionOwner` INT(10) UNSIGNED DEFAULT 0,
  `deleted` BINARY DEFAULT 0,
  `deletedBy` INT(10) UNSIGNED DEFAULT 0,
  `deleteDate` datetime DEFAULT NULL,
  `mapCenterLat` DOUBLE DEFAULT 0,
  `mapCenterLng` DOUBLE DEFAULT 0,
  `mapZoom` SMALLINT(5) DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
