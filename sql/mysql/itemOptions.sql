DROP TABLE IF EXISTS `itemOptions`;

CREATE TABLE `itemOptions` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `datestamp` datetime DEFAULT NULL,
    `itemID` INT(10) UNSIGNED DEFAULT 0,
    `name` varchar(250) DEFAULT NULL,
    `multiSelect` BOOLEAN DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
