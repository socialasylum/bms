DROP TABLE IF EXISTS `wallPosts`;

CREATE TABLE `wallPosts` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `postingUser` INT(10) UNSIGNED DEFAULT 0,
    `body` TEXT DEFAULT NULL,
    `parentPost` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;