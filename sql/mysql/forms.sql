DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` DATETIME DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `approvals` SMALLINT(5) UNSIGNED DEFAULT 0,
    `title` VARCHAR(300) DEFAULT NULL,
    `instructions` TEXT DEFAULT NULL,
    `briefIns` TEXT DEFAULT NULL,
    `formHtml` TEXT DEFAULT NULL,
    `active` BOOLEAN DEFAULT 1,
    `deleted` BOOLEAN DEFAULT 0,
    `deletingUser` INT(10) UNSIGNED DEFAULT 0,
    `deleteDate DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
