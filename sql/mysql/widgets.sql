DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `module` INT(10) UNSIGNED DEFAULT 0,
  `active` BINARY DEFAULT 1,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


INSERT INTO widgets (datestamp, name, url, module) VALUES
(NOW(), 'Messages', '/msg/widget', 6),
(NOW(), 'Help Desk', '/helpdesk/widget', 7),
(NOW(), 'Users', '/user/widget', 5);

