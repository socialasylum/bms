DROP TABLE IF EXISTS `itemOptionChoices`;

CREATE TABLE `itemOptionChoices` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `optionId` INT(10) UNSIGNED DEFAULT 0,
    `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
