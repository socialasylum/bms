DROP TABLE IF EXISTS `positionModuleAccess`;

CREATE TABLE `positionModuleAccess` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `position` INT(10) UNSIGNED DEFAULT 0,
    `module` INT(10) UNSIGNED DEFAULT 0,
    `permissions` BIGINT UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX posModIndex ON positionModuleAccess (position, module);
