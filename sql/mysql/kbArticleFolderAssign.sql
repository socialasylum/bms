DROP TABLE IF EXISTS `kbArticleFolderAssign`;

CREATE TABLE `kbArticleFolderAssign` (
  `articleId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`articleId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX kbArticleFolderIndex ON kbArticleFolderAssign (`articleId`, `folderId`);
