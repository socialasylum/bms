DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` DATETIME DEFAULT NULL,
  `from` INT(10) UNSIGNED DEFAULT 0,
  `subject` VARCHAR(300) DEFAULT NULL,
  `body` TEXT DEFAULT NULL,
  `deleted` BINARY DEFAULT 0,
  `emailDate` DATETIME DEFAULT NULL,
  `extMsgID` INT(10) UNSIGNED DEFAULT 0,
  `extFrom` VARCHAR(300) DEFAULT NULL,
  `parsed` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
