DROP TABLE IF EXISTS `companyAdmins`;

CREATE TABLE `companyAdmins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assignedBy` INT(10) UNSIGNED DEFAULT 0,
  `datestamp` datetime DEFAULT NULL,
  `userId` INT(10) UNSIGNED DEFAULT 0,
  `company` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX compAdminIndex ON companyAdmins (`userId`, `company`);
