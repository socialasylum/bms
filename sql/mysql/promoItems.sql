DROP TABLE IF EXISTS `promoItems`;

CREATE TABLE `promoItems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `promo` INT(10) UNSIGNED DEFAULT 0,
  `item` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX promoItemsIndex ON promoItems (promo, item);
