DROP TABLE IF EXISTS `crmPhone`;

CREATE TABLE `crmPhone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contactId` INT(10) UNSIGNED NOT NULL,
  `type` smallint(5) unsigned DEFAULT 0,
  `phoneNumber` VARCHAR(200) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
