DROP TABLE IF EXISTS `photoLikes`;

CREATE TABLE `photoLikes` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `photoID` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX photoLikesIndex ON photoLikes (userid, photoID);
