DROP TABLE IF EXISTS `userWidgets`;

CREATE TABLE `userWidgets` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `portlet` INT(10) UNSIGNED DEFAULT 0,
    `widget` INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- CREATE UNIQUE INDEX userWidgetIndex ON userWidgets (userid, company, widget);
-- CREATE UNIQUE INDEX userWidgetPortletIndex ON userWidgets (userid, company, portlet);
