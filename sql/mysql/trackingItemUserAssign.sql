DROP TABLE IF EXISTS `trackingItemUserAssign`;

CREATE TABLE `trackingItemUserAssign` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `trackingItemID` INT(10) UNSIGNED DEFAULT 0,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX trackItemUserAssignIndex ON trackingItemUserAssign (trackingItemID, userid);
