DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `title` varchar(250) DEFAULT NULL,
  `body` TEXT DEFAULT NULL,
  `type` smallint(5) unsigned DEFAULT 0,
  `ack` smallint(5) unsigned DEFAULT 0,
  `version` INT(10) UNSIGNED DEFAULT 0,
  `file` varchar(250) DEFAULT NULL,
  `startDate` DATETIME DEFAULT NULL,
  `endDate` DATETIME DEFAULT NULL,
  `trainingDocument` BINARY DEFAULT 0,
  `active` BOOLEAN DEFAULT 1,
  `deleted` BOOLEAN DEFAULT 0,
  `deletedBy` INT(10) UNSIGNED DEFAULT NULL,
  `deleteDate` DATETIME DEFAULT NULL,
  `accessType` smallint(5) unsigned DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
