DROP TABLE IF EXISTS `formPostData`;

CREATE TABLE `formPostData` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` DATETIME DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `form` INT(10) UNSIGNED DEFAULT 0,
    `formTitle` VARCHAR(300) DEFAULT NULL,
    `formHtml` TEXT DEFAULT NULL,
    `postdata` TEXT DEFAULT NULL,
    `processed` BINARY DEFAULT 0,
    `processedBy` INT(10) UNSIGNED DEFAULT 0,
    `processDate` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
