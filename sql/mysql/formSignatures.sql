DROP TABLE IF EXISTS `formSignatures`;

CREATE TABLE `formSignatures` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` DATETIME DEFAULT NULL,
    `form` INT(10) UNSIGNED DEFAULT 0,
    `lvl` INT(10) UNSIGNED DEFAULT 0,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `approved` BINARY DEFAULT 0,
    `final` BINARY DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX formSigIndex ON formSignatures (form, lvl);
