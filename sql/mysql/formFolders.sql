DROP TABLE IF EXISTS `formFolders`;

CREATE TABLE `formFolders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `name` varchar(250) DEFAULT NULL,
  `createdBy` int(10) unsigned DEFAULT 0,
  `parentFolder` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

