DROP TABLE IF EXISTS `trackingItemPrices`;

CREATE TABLE `trackingItemPrices` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `trackingItemID` INT(10) UNSIGNED DEFAULT 0,
    `datestamp` datetime DEFAULT NULL,
    `priceDay` DATE DEFAULT NULL,
    `price` DOUBLE DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
