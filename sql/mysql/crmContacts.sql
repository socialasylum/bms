DROP TABLE IF EXISTS `crmContacts`;

CREATE TABLE `crmContacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `updated` datetime DEFAULT NULL,
  `updatedBy` INT(10) UNSIGNED DEFAULT 0,
  `name` varchar(250) DEFAULT NULL,
  `companyName` VARCHAR(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `websiteUrl` VARCHAR(300) DEFAULT NULL,
  `type` smallint(5) unsigned DEFAULT 0,
  `status` smallint(5) unsigned DEFAULT 0,
  `deleted` BOOLEAN DEFAULT 0,
  `deletedBy` INT(10) UNSIGNED DEFAULT NULL,
  `deleteDate` DATETIME DEFAULT NULL,
  `industry` SMALLINT(5) UNSIGNED DEFAULT 0,
  `otherIndustry` VARCHAR(300) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
