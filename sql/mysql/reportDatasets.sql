DROP TABLE IF EXISTS `reportDatasets`;

CREATE TABLE `reportDatasets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `createdBy` INT(10) UNSIGNED DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `name` varchar(250) DEFAULT NULL,
  `query` TEXT DEFAULT NULL,
  `active` BOOLEAN DEFAULT 1,
  `deleted` BOOLEAN DEFAULT 0,
  `deletedBy` INT(10) UNSIGNED DEFAULT NULL,
  `deleteDate` DATETIME DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
