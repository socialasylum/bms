DROP TABLE IF EXISTS `locationYouTubeVideos`;

CREATE TABLE `locationYouTubeVideos` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `locationid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `datestamp` DATETIME DEFAULT NULL,
    `url` VARCHAR(300) DEFAULT NULL,
    `videoOrder` SMALLINT(5) UNSIGNED DEFAULT 0,
    `title` VARCHAR(300) DEFAULT NULL,
    `thumbnail` VARCHAR(300) DEFAULT NULL,
    `description` TEXT DEFAULT NULL,
    `videoID` VARCHAR(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

