DROP TABLE IF EXISTS `userSigs`;

CREATE TABLE `userSigs` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `company` INT(10) UNSIGNED DEFAULT 0,
    `signature` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX userSigsIndex ON userSigs (userid, company);
