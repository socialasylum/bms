DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `userid` INT(10) UNSIGNED DEFAULT 0,
  `title` varchar(250) DEFAULT NULL,
  `fileName` VARCHAR(300) DEFAULT NULL,
  `youtubeUrl` VARCHAR(300) DEFAULT NULL,
  `thumbnail` VARCHAR(400) DEFAULT NULL,
  `youtubeData` TEXT DEFAULT NULL,
  `active` BOOLEAN DEFAULT 1,
  `deleted` BOOLEAN DEFAULT 0,
  `deletedBy` INT(10) UNSIGNED DEFAULT NULL,
  `deleteDate` DATETIME DEFAULT NULL,
  `description` TEXT DEFAULT NULL,
  `tags` TEXT DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
