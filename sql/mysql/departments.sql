DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datestamp` datetime DEFAULT NULL,
  `createdBy` INT(10) UNSIGNED DEFAULT 0,
  `company` INT(10) UNSIGNED DEFAULT 0,
  `name` varchar(250) DEFAULT NULL,
  `active` BINARY DEFAULT 0,
  `default` BINARY DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
