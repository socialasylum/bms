DROP TABLE IF EXISTS `photoComments`;

CREATE TABLE `photoComments` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `photoID` INT(10) UNSIGNED DEFAULT 0,
    `body` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;