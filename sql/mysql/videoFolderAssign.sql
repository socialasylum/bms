DROP TABLE IF EXISTS `videoFolderAssign`;

CREATE TABLE `videoFolderAssign` (
  `videoId` int(10) unsigned NOT NULL,
  `folderId` int(10) unsigned DEFAULT 0,
    PRIMARY KEY (`videoId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE UNIQUE INDEX videoFolderIndex ON videoFolderAssign (`videoId`, `folderId`);
