<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pos_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Goes through transaction array to calculates 
     * the appropriate qty for each item
     *
     * @param array $transArray 
     *
     * @return array
     */
    public function compileTransQty ($transArray)
    {
        $return = array
            (
                'item' => array(),
                'qty' => array()
            );

        if (!empty($transArray))
        {
            foreach ($transArray as $k => $v)
            {
                if (in_array($v, $return['item']))
                {
                    // item already in array, increase qty
                    $return['qty'][$v]++;
                }
                else
                {
                    // item not in array, add it and set qty to 1

                    $return['item'][] = $v;

                    $return['qty'][$v] = 1;
                }
            }
        }

        return $return;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     * deprecated, use orders table
    public function insertTransaction ($type = 1)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'type' => 1,
                'extendedAmount' => 0,
                'discountAmount' => 0,
                'taxPercent' => 0,
                'paymentType' => 1
            );

        $this->db->insert('transactions', $data);

        return $this->db->insert_id();
    }
    */
    /**
     * TODO: short description.
     *
     * @param mixed $transaction      
     * @param mixed $transactionItems 
     *
     * @return TODO
     */
    /*
     // deprecated
    public function saveTransactionItems ($transaction, $transactionItems)
    {
        if (empty($transaction)) throw new Exception("Transaction ID is empty!");

        $items = $this->compileTransQty($transactionItems);

        if (!empty($items['item']))
        {
            foreach ($items['item'] as $itemID)
            {

            }
        }

    }
     */
}
