<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orders_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getOrders ($company = 0, $crmContact = 0)
    {
        // if (empty($crmContact)) throw new Exception("CRM Contact ID is empty!");

        if (empty($company)) $company = $this->session->userdata('company');

        if (empty($company)) throw new Exception("Company ID is empty");

        $mtag = "orders-{$company}-{$crmContact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('orders');
            $this->db->where('company', $company);

            if (!empty($crmContact)) $this->db->where('crmContact', $crmContact);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getOrderInfo ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("ID is empty!");

        $mtag = "orderInfo-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('orders');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $Order 
     *
     * @return TODO
     */
    public function getOrderItems ($order)
    {
        $order = intval($order);

        if (empty($order)) throw new Exception("Order ID is empty");

        $mtag = "orders-{$crmContact}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('orderItems');
            $this->db->where('orderID', $order);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $orderID 
     *
     * @return TODO
     */
    public function calculateOrderAmount ($orderID)
    {
        if (empty($orderID)) throw new Exception("Order ID is empty!");

        $total = 0;

        $items = $this->getOrderItems($orderID);

        if (!empty($items))
        {
            foreach ($items as $r)
            {
                $total += ($r->qty * $r->retailPrice);
            }
        }

        // FIXME incoroproate shipping costs into this functions

        return number_format($total, 2);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $order 
     *
     * @return TODO
     */
    public function getNotes ($order)
    {
        $order = intval($order);

        if (empty($order)) throw new Exception("Order ID is empty");

        $mtag = "orderNotes-{$order}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, datestamp, userid, note');
            $this->db->from('orderNotes');
            $this->db->where('orderID', $order);
            $this->db->where('deleted', 0);
            $this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;


    }

    /**
     * Inserts a new order note
     *
     * @param mixed $p 
     *
     * @return int - row ID
     */
    public function insertNote ($order, $note)
    {
        $order = intval($order);

        if (empty($order)) throw new Exception("Order ID is empty");
        if (empty($note)) throw new Exception("Note is empty! No point in wasting database space for an empty note.");

        $note = $this->functions->removeCode($note);

        $data = array
            (
                'orderID' => $order,
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'note' => $note
            );

        $this->db->insert('orderNotes', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateOrder ($p)
    {
        $p['id'] = intval($p['id']);

        if (empty($p['id'])) throw new Exception("Order ID is empty!");

        $data = array
            (
                'status' => $_POST['status']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('orders', $data);

        return true;
    }

    public function createOrder ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'crmContact' => $p['crmContactID'],
                'company' => $this->session->userdata('company'),
                'email' => $p['email'],
                'status' => $p['status'],
                'crmShippingAddress' => $p['crmShippingAddress'],
                'crmBillingAddress' => $p['crmBillingAddress']
            );

        if (!empty($p['location'])) $data['location'] = $p['location'];

        $this->db->insert('orders', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $orderID     
     * @param mixed $item        
     * @param mixed $qty         
     * @param mixed $cost        
     * @param mixed $retailPrice 
     *
     * @return TODO
     */
    public function insertOrderItem ($orderID, $item, $qty = 0, $cost = 0, $retailPrice = 0)
    {
        if (empty($orderID)) throw new Exception("Order ID is empty!");
        if (empty($item)) throw new Exception("Item ID is empty!");

        if (empty($qty)) return false; // do nothing if qty is empty as it should not be inserted (logic mistake?)

        $data = array
            (
                'orderID' => $orderID,
                'item' => $item,
                'qty' => $qty,
                'cost' => $cost,
                'retailPrice' => $retailPrice
            );

        $this->db->insert('orderItems', $data);

        return $this->db->insert_id();
    }

    /**
     * Used to update the status of an order
     *
     * @param int $orderID
     * @param int $status  
     *
     * @return boolean
     */
    public function updateOrderStatus ($orderID, $status)
    {
        $orderID = intval($orderID);
        $status = intval($status);

        if (empty($orderID)) throw new Exception("Order ID is empty!");
        if (empty($status)) throw new Exception("Status is empty!");

        $data = array('status' => $stats);

        $this->db->where('id', $orderID);
        $this->db->update('orders', $data);

        return true;
    }
}
