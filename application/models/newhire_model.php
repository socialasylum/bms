<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class newhire_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $days 
     *
     * @return TODO
     */
    public function updateTrainingDays ($days)
    {
        $days = intval($days);

        $data = array
            (
                'newhireDays' => $days
            );

        $this->db->where('id', $this->session->userdata('company'));
        $this->db->update('companies', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function docSearch ()
    {
        $mtag = 'docSearch-' . $this->session->userdata('company');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, title");
            $this->db->from('documents');
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->where('deleted', 0);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        // $return = array('optons');

        foreach ($data as $r)
        {
            $return[] = array
                (
                    'id' => $r->id,
                    'label' => $r->title,
                    'value' => $r->title
                    );
            // $return['id'][] = $r->id;
            // $return[] = $r->title;
        }

        return $return;

        // return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function addNewhireDoc ($p)
    {
        if (empty($p['id'])) throw new Exception("Document ID is empty");
        if (empty($p['day'])) throw new Exception("newhire day is empty");


        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $this->session->userdata('company'),
                'document' => $p['id'],
                'day' => $p['day'],
                'order' => 999
            );

        $this->db->insert('newhire', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getNewhireDocs ($day)
    {
        if (empty($day)) throw new Exception("Day is empty");

        $mtag = "newhireDocs-{$this->session->userdata('company')}-{$day}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id, document, order');
            $this->db->from('newhire');
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->where('day', $day);
            $this->db->order_by('order', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateDayOrder ($p)
    {
        if (!empty($p['doc']))
        {
            foreach ($p['doc'] as $k => $id)
            {
                $data = array
                    (
                        'order' => $k
                    );

                $this->db->where('id', $id);
                $this->db->where('day', $p['day']);
                $this->db->where('company', $this->session->userdata('company'));
                $this->db->update('newhire', $data);
            }
        }


        return true;
    }
}
