<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class user_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getUsers($id = 0)
    {

        $mtag = "userIDList-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('users');
            $this->db->where('deleted', 0);
            // $this->db->where('company', $this->session->userdata('company'));

            if (!empty($id)) $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            if (!empty($id)) $data = $results[0];
            else $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    /**
     * Main function used to create a user
     *
     * @param mixed $p - $_POSt array of data to create user
     *
     * @return INT - userid
     */
    public function createUser ($p, $admin = false)
    {

        $data = array(
            'datestamp' => DATESTAMP,
            'firstName' => $p['firstName'],
            'lastName' => $p['lastName'],
            'email' => $p['email'] . '@' . $p['emaildomain'],
            'timezone' => $p['timezone'],
            'passwd' => sha1($p['password']),
            'status' => 1,
            'permissions' => 0,
            'industry' => $p['industry'],
            'lat' => $p['lat'],
            'lng' => $p['lng']
        );

        if (!empty($p['address'])) $data['addressLine1'] = $p['address'];
        if (!empty($p['address2'])) $data['addressLine2'] = $p['address2'];
        if (!empty($p['address3'])) $data['addressLine3'] = $p['address3'];
        if (!empty($p['city'])) $data['city'] = $p['city'];
        if (!empty($p['state'])) $data['state'] = $p['state'];
        if (!empty($p['postalCode'])) $data['postalCode'] = $p['postalCode'];

        if (!empty($p['companyName'])) $data['companyName'] = $p['companyName'];

        if (!empty($p['facebookUrl'])) $data['facebookUrl'] = $p['facebookUrl'];
        if (!empty($p['twitterUrl'])) $data['twitterUrl'] = $p['twitterUrl'];
        if (!empty($p['linkedInUrl'])) $data['linkedInUrl'] = $p['linkedInUrl'];
        if (!empty($p['youtubeUrl'])) $data['youtubeUrl'] = $p['youtubeUrl'];
        if (!empty($p['googlePlusUrl'])) $data['googlePlusUrl'] = $p['googlePlusUrl'];
        if (!empty($p['companyWebsiteUrl'])) $data['companyWebsiteUrl'] = $p['companyWebsiteUrl'];
        if (!empty($p['phone'])) $data['phone'] = $p['phone'];
        if (!empty($p['mobile'])) $data['mobile'] = $p['mobile'];
        if (!empty($p['fax'])) $data['fax'] = $p['fax'];
        if (!empty($p['bio'])) $data['bio'] = $p['bio'];
        
        if (!empty($p['otherIndustry'])) $data['otherIndustry'] = $p['otherIndustry'];

        if (!empty($p['profileimg'])) $data['profileimg'] = $p['profileimg'];
        if (!empty($p['uploadedProfileImg'])) $data['profileimg'] = $p['uploadedProfileImg'];
        
        // if (!empty($p['editor'])) $data['editor'] = $p['editor'];

        // only updates these fields if user doing the update is a site admin
        if ($this->users->isAdmin() == true)
        {
            $data['BrainTreeCustomerID'] = $p['BrainTreeCustomerID'];
            $data['BrainTreeSubscriptionID'] = $p['BrainTreeSubscriptionID'];
        }

        $data['admin'] = ($admin) ? 1 : 0;
        
        if (!empty($p['facebookID'])) $data['facebookID'] = $p['facebookID'];
        if (!empty($p['accountType'])) $data['accountType'] = $p['accountType'];

        // if (!empty($p[''])) $data[''] = $p[''];

        $this->db->insert('users', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getPermissions ()
    {
        $this->db->from('permissions');
        $this->db->where('active', '1');
        $this->db->order_by('label', 'ASC');

        $query = $this->db->get();

        return $query->result();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateUser ($p)
    {
        if (empty($p['id'])) throw new Exception('user ID is empty!');

        $data = array
            (
                'firstName' => $p['firstName'],
                'lastName' => $p['lastName'],
                'status' => $p['status'],
                'email' => $p['email'] . '@' . $p['emaildomain'],
                'timezone' => $p['timezone'],
                'admin' => $p['admin'],
                'salType' => $p['salType'],
                'payRate' => $p['salAmount'],
                'salFreq' => $p['salFreq'],
                'commission' => $p['commission'],
                'comType' => $p['comType'],
                'industry' => $p['industry'],
                'otherIndustry' => $p['otherIndustry'],
                'lat' => $p['lat'],
                'lng' => $p['lng']
            );

        if (!empty($p['profileimg'])) $data['profileimg'] = $p['profileimg'];

        if (!empty($p['address'])) $data['addressLine1'] = $p['address'];
        if (!empty($p['address2'])) $data['addressLine2'] = $p['address2'];
        if (!empty($p['address3'])) $data['addressLine3'] = $p['address3'];
        if (!empty($p['city'])) $data['city'] = $p['city'];
        if (!empty($p['state'])) $data['state'] = $p['state'];
        if (!empty($p['postalCode'])) $data['postalCode'] = $p['postalCode'];

        if (!empty($p['companyName'])) $data['companyName'] = $p['companyName'];

        if (!empty($p['facebookUrl'])) $data['facebookUrl'] = $p['facebookUrl'];
        if (!empty($p['twitterUrl'])) $data['twitterUrl'] = $p['twitterUrl'];
        if (!empty($p['linkedInUrl'])) $data['linkedInUrl'] = $p['linkedInUrl'];
        if (!empty($p['youtubeUrl'])) $data['youtubeUrl'] = $p['youtubeUrl'];
        if (!empty($p['googlePlusUrl'])) $data['googlePlusUrl'] = $p['googlePlusUrl'];
        if (!empty($p['companyWebsiteUrl'])) $data['companyWebsiteUrl'] = $p['companyWebsiteUrl'];
        if (!empty($p['phone'])) $data['phone'] = $p['phone'];
        if (!empty($p['mobile'])) $data['mobile'] = $p['mobile'];
        if (!empty($p['fax'])) $data['fax'] = $p['fax'];
        if (!empty($p['bio'])) $data['bio'] = $p['bio'];

        // only updates these fields if user doing the update is a site admin
        if ($this->users->isAdmin() == true)
        {
            $data['BrainTreeCustomerID'] = $p['BrainTreeCustomerID'];
            $data['BrainTreeSubscriptionID'] = $p['BrainTreeSubscriptionID'];
        }

        $this->db->where('id', $p['id']);
        $this->db->update('users', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function setUserDeleted ($id)
    {
        if (empty($id)) throw new Exception("user ID is empty");


        $data = array
            (
                'deleted' => 1,
                'deletedBy' => $this->session->userdata('userid'),
                'deleteDate' => DATESTAMP
            );


        $this->db->where('id', $id);
        $this->db->update('users', $data);


    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function clearAssignedCompanies ($userid, $company = 0)
    {
        $userid = intval($userid);

        if (empty($userid)) throw new Exception("User ID is empty");

        $this->db->where('userid', $userid);

        if (!empty($company)) $this->db->where('company', $company);

        $this->db->delete('userCompanies');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid    
     * @param mixed $companies 
     *
     * @return TODO
     */
    public function saveAssignedCompanies ($userid, $companies)
    {
        $userid = intval($userid);

        if (empty($userid)) throw new Exception("User ID is empty");

        if (is_array($companies))
        {
            foreach ($companies as $company => $v)
            {

                if (empty($v))
                {
                    $this->clearAssignedCompanies($userid, $company);
                }
                else
                {
                    // first check if company is already assigned
                    $assigned =  $this->companies->userAssignedToCompany($userid, $company);

                    $h = ($home == $company) ? true : false;

                    if ($assigned === false)
                    {
                        // if not assigned, will now assign
                        $this->insertUserCompany($userid, $company, $h);
                    }
                    else
                    {
                        // updates record
                        $this->updateUserCompany($userid, $company, $h);
                    }
                }
            }
        }
        else if (!empty($companies))
        {
            $assigned =  $this->companies->userAssignedToCompany($userid, $companies);

            if ($assigned === false)
            {
                // if not assigned, will now assign
                $this->insertUserCompany($userid, $companies, true);
            }
            else
            {
                // updates record
                $this->updateUserCompany($userid, $companies, true);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid  
     * @param mixed $company 
     *
     * @return TODO
     */
    public function insertUserCompany($userid, $company, $home = false)
    {

        if (empty($userid)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'userid' => $userid,
                'company' => $company,
                'homeCompany' => (int) $home
            );

        $this->db->insert('userCompanies', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $userid  
     * @param mixed $company 
     * @param mixed $home    Optional, defaults to false. 
     *
     * @return TODO
     */
    public function updateUserCompany($userid, $company, $home = false)
    {

        if (empty($userid)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'homeCompany' => (int) $home
            );



        $this->db->where('userid', $userid);
        $this->db->where('company', $company);
        $this->db->update('userCompanies', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $email 
     *
     * @return TODO
     */
    public function emailSearch ($q)
    {

        $mtag = "emailSearch-{$q}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, firstName, lastName, email');
            $this->db->from('users');
            $this->db->like('email', $q);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        $return = array();

        foreach ($data as $r)
        {
            $name = "{$r->firstName} {$r->lastName} ({$r->email})";

            $return[] = array
                (
                    'id' => $r->id,
                    'label' => $name,
                    'value' => $name
                    );
        }

        return $return;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    public function insertCompanyPosition ($user, $company, $position)
    {

        $user = intval($user);
        $company = intval($company);
        $position = intval($position);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($position)) throw new Exception("Position ID is empty!");

        $data = array
            (
                'userid' => $user,
                'company' => $company,
                'position' => $position
            );

        $this->db->insert('userCompanyPositions', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user     
     * @param mixed $company  
     * @param mixed $position 
     *
     * @return TODO
     */
    public function updateCompanyPosition ($user, $company, $position)
    {
        $user = intval($user);
        $company = intval($company);
        $position = intval($position);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($position)) throw new Exception("Position ID is empty!");

        $data = array
            (
                'position' => $position
            );

        $this->db->where('company', $company);
        $this->db->where('userid', $user);
        $this->db->update('userCompanyPositions', $data);

        return $user;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    public function insertCompanyDepartment ($user, $company, $department)
    {

        $user = intval($user);
        $company = intval($company);
        $department = intval($department);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($department)) throw new Exception("Department ID is empty!");

        $data = array
            (
                'userid' => $user,
                'company' => $company,
                'department' => $department
            );

        $this->db->insert('userCompanyDepartments', $data);

    return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $user     
     * @param mixed $company  
     * @param mixed $department
     *
     * @return TODO
     */
    public function updateCompanyDepartment ($user, $company, $department)
    {
        $user = intval($user);
        $company = intval($company);
        $department = intval($department);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($department)) throw new Exception("Department ID is empty!");

        $data = array
            (
                'department' => $department
            );

        $this->db->where('company', $company);
        $this->db->where('userid', $user);
        $this->db->update('userCompanyDepartments', $data);

        return $user;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $url    
     * @param mixed $userid Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function addYouTubeUrl ($url, $order = 0, $userid = 0)
    {
        if (empty($url)) throw new Exception("URL is empty!");

        if (empty($userid)) $userid = $this->session->userdata('userid');

        if (empty($userid)) throw new Exception("Userid is empty!");

        $data = array
            (
                'userid' => $userid,
                'company' => $this->session->userdata('company'),
                'datestamp' => DATESTAMP,
                'url' => $url
            );

        if (empty($order)) $data['videoOrder'] = $order;

        $this->db->insert('userYouTubeVideos', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $videos 
     *
     * @return TODO
     */
    public function updateYoutubeVideoOrder ($videos)
    {
        if (!empty($videos))
        {
            foreach ($videos as $order => $video)
            {
                $this->updateYTOrder($video, $order);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $video 
     * @param mixed $order 
     *
     * @return TODO
     */
    public function updateYTOrder ($video, $order)
    {
        $video = intval($video);

        if (empty($video)) throw new Exception("Video ID is empty!");

        $data = array
            (
                'videoOrder' => $order
            );

        $this->db->where('id', $video);
        $this->db->update('userYouTubeVideos', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $video 
     *
     * @return TODO
     */
    public function deleteYTVideo ($video)
    {
        $video = intval($video);

        if (empty($video)) throw new Exception("Video ID is empty!");

        $this->db->where('id', $video);
        $this->db->delete('userYouTubeVideos');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param int $user         
     * @param int $user         
     * @param int $companyAdmin 
     *
     * @return true
     */
    public function saveCompanyAdmin ($user, $company, $companyAdmin)
    {
        $currentAdmin = $this->users->isCompanyAdmin($user, $company);

        if ($companyAdmin == 0)
        {
            if ($currentAdmin == true)
            {
                // remove admin status as it has been removed
                $this->_removeCompanyAdmin($user, $company);
            }
        }
        else
        {
            if ($currentAdmin == false)
            {
                // they are currently not an admin, and will now make them one
                $this->_assignCompanyAdmin($user, $company);
            }
        }
    }

    /**
     * removes all rows of for a user in companyAdmin table
     * essentially remove all their company admin permission
     *
     * @param mixed $user 
     *
     * @return true
     */
    private function _removeCompanyAdmin ($user, $company)
    {
        $user = intval($user);
        $company = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $this->db->where('userId', $user);
        $this->db->where('company', $company);
        $this->db->delete('companyAdmins');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    private function _assignCompanyAdmin ($user, $company)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'assignedBy' => $this->session->userdata('userid'),
                'datestamp' => DATESTAMP,
                'userid' => $user,
                'company' => $company
            );

        $this->db->insert('companyAdmins', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $user         
     * @param mixed $company      
     * @param mixed $companyEditor
     *
     * @return TODO
     */
    public function saveCompanyEditor ($user, $company, $companyEditor)
    {
        $currentEditor = $this->users->isCompanyEditor($user, $company);

        if ($companyEditor == 0)
        {
            if ($currentEditor == true)
            {
                // remove admin status as it has been removed
                $this->_removeCompanyEditor($user, $company);
            }
        }
        else
        {
            if ($currentEditor == false)
            {
                // they are currently not an admin, and will now make them one
                $this->_assignCompanyEditor($user, $company);
            }
        }

    }

    /**
     * removes all rows of for a user in companyEditors table
     *
     * @param mixed $user 
     *
     * @return true
     */
    private function _removeCompanyEditor ($user, $company)
    {
        $user = intval($user);
        $company = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $this->db->where('userId', $user);
        $this->db->where('company', $company);
        $this->db->delete('companyEditors');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    private function _assignCompanyEditor ($user, $company)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'assignedBy' => $this->session->userdata('userid'),
                'datestamp' => DATESTAMP,
                'userId' => $user,
                'company' => $company
            );

        $this->db->insert('companyEditors', $data);

        return $this->db->insert_id();
    }

    public function updateStatus($userid, $status)
    {
        if(empty($userid))  throw new Exception('User ID is empty!');
        if(empty($status))  throw new Exception('No new status detected!');

        $data = array
            (
                'status' => $status,
            );

        $this->db->where('id', $userid);
        $this->db->update('users', $data);

        return "SUCCESS";
    }


    public function getOnfileCards ($user)
    {
        if (empty($user)) throw new Exception("User ID is empty!");

        $mtag = "userCards-{$user}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, cardType, lastFour, token");
            $this->db->from('userTokens');
            $this->db->where('userid', $user);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $rate 
     * @param mixed $user Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function updateSyncRate ($rate, $user = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($user)) throw new Exception("User ID is empty!");

        $rate = $this->functions->onlyNumbers($rate);

        $data = array('syncRate' => $rate);

        $this->db->where('id', $user);
        $this->db->update('users', $data);

        return true;
    }
    
    /**
     * TODO: short description.
     *
     * @param mixed $user Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getCompleteForms ($user = 0, $company = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($user)) throw new Exception("User ID is empty!");

        if (empty($company)) $company = $this->session->userdata('company');
        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "userCompletedForms-{$user}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id, datestamp, formTitle, processed, processedBy, processDate');
            $this->db->from('formPostData');
            $this->db->where('userid', $user);
            $this->db->where('company', $company);
            $this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    public function getSignedDocuments ($user, $company)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($user)) throw new Exception("User ID is empty!");

        if (empty($company)) $company = $this->session->userdata('company');
        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "userSignedDocuments-{$user}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('docId, MAX(version) as version, MAX(datestamp) datestamp');
            $this->db->from('docSignatures');
            $this->db->where('userid', $user);
            $this->db->where('company', $company);
            $this->db->group_by('docId');
            // $this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user     
     * @param mixed $password 
     *
     * @return TODO
     */
    public function updateUserPassword ($user, $password)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($password)) throw new Exception("You must enter some type of password...something..anything");

        $data = array ('passwd' => sha1($password));

        $this->db->where('id', $user);
        $this->db->update('users', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    public function insertExempt($user, $company)
    {
        $user = intval($user);
        $companyr = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $user,
                'company' => $company
            );

        $this->db->insert('userBillingExempt', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function clearExempt ($user, $company)
    {
        $user = intval($user);
        $companyr = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $this->db->where('Company', $company);
        $this->db->where('userid', $user);
        $this->db->delete('userBillingExempt');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return TODO
     */
    public function isCompanyBillingExempt ($user, $company)
    {
        $user = intval($user);
        $companyr = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('userBillingExempt');
            $this->db->where('userid', $user);
            $this->db->where('company', $company);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ((int) $data > 0) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function checkCustomerID ($user)
    {
        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        // gets current customer ID
        $BrainTreeCustomerID = $this->users->getTableValue('BrainTreeCustomerID', $user);

        if (empty($BrainTreeCustomerID))
        {
            // will now create a subscription ID
            $info = $this->users->getRow($user);

            // $customerID = $user;

            if ($this->session->userdata('company') == 6) $customerID = $user; // promote leads customer ID are just userid
            else $customerID = "user-{$user}";

            $data = array
                (
                    'customerID' => $customerID,
                    'firstName' => $info->firstName,
                    'lastName' => $info->lastName,
                    'companyName' => $info->companyName,
                    'email' => $info->email,
                    'phone' => $info->phone,
                    'fax' => $info->fax,
                    'websiteUrl' => $info->companyWebsiteUrl
                );

            $customerID = $this->payment->createCustomer($data, false);

            // updates BrainTree customer ID is users table
            $this->users->updateBTCustomerID($customerID, $user);

            $BrainTreeCustomerID = $customerID;
        }

        return $BrainTreeCustomerID;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user       
     * @param mixed $facebookID 
     *
     * @return TODO
     */
    public function updateFacebookID ($user, $facebookID = 0)
    {
        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        $data = array('facebookID' => $facebookID);

        $this->db->where('id', $user);
        $this->db->update('users', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $facebookID 
     *
     * @return TODO
     */
    public function clearFacebookID ($facebookID)
    {
        if (empty($facebookID)) throw new Exception("Facebook ID is empty!");

        $data = array('facebookID' => 0);

        $this->db->where('facebookID', $facebookID);
        $this->db->update('users', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $url 
     *
     * @return TODO
     */
    public function downloadfbphoto ($user, $url)
    {
        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        if (empty($url)) throw new Exception("URL is empty, nothing to download!");

        $path = 'public' . DS . 'uploads' . DS . 'profileimgs' . DS;

        // ensures company uploads directory has been created
        $this->functions->createDir($path);


        $path = $_SERVER['DOCUMENT_ROOT'] . 'public' . DS . 'uploads' . DS . 'profileimgs' . DS;

        $ext = $this->functions->getFileExt($url);

        $filename = uniqid() . '_' . date("YmdGis") . '.' . $ext;

        $getContents = file_get_contents($url);

        if ($getContents === false) throw new Exception("Unable to get file content from: {$url}");

        $put = file_put_contents(($path.$filename), $getContents);

        if ($put === false) throw new Exception("Unable to save contents of ({$url}) to: {$path}{$filename}");

		$this->updateProfileImg($user, $filename);

		/*
        $data = array('profileimg' => $filename);

        $this->db->where('id', $user);
        $this->db->update('users', $data);
		*/

        return $filename;

    }
    
    public function updateProfileImg ($user, $filename)
    {
    	$user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");
        
        $data = array('profileimg' => $filename);

        $this->db->where('id', $user);
        $this->db->update('users', $data);

		return true;
    }
    
    public function getPhotoAlbums ($user = 0, $id = 0)
    {
    	$user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");
    	
    	$company = $this->session->userdata('company');

        if (empty($company)) throw new Exception("Company ID is empty!");
     	
        $mtag = "getPhotoAlbums-{$user}-{$company}-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id, datestamp, name, stream, mobile');
            $this->db->from('userPhotoAlbums');
            $this->db->where('userid', $user);
            $this->db->where('company', $company);
            
            if (!empty($id)) $this->db->where('id', $id);
            
            $this->db->order_by('name');

            $query = $this->db->get();

            $results = $query->result();

			$data = (empty($id)) ? $results : $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

		//echo $this->db->last_query();
        
        return $data;
    }
    
	
	public function getAlbumPhotos ($album, $limit = false)
	{
		//$userid = intval($userid);
		$album = intval($album);
				
		//if (empty($userid)) throw new Exception('User ID is empty!');
		if (empty($album)) throw new Exception('Album ID is empty!');
				
		$mtag = "getAlbumPhotos-{$album}-{$limit}";

        $data = $this->cache->memcached->get($mtag);
        
		if (!$data)
        {
			$this->db->select('id, datestamp, userid, fileName');
            $this->db->from('albumPhotos');
            $this->db->where('posted', 1);
			//$this->db->where('userid', $userid);
			$this->db->where('album', $album);
			
			if ($limit !== false) $this->db->limit($limit);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}
	
}
