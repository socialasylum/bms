<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class helpdesk_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    public function submitTicket($p)
    {
        $data = array
            (
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'datestamp' => DATESTAMP,
                'department' => $p['department'],
                'issueDate' => $p['datepicker'],
                'priority' => $p['priority'],
                'status' => $p['status'],
                'subject' => $p['subject'],
                'issue' => $p['issue'],
            );

        $this->db->insert('tickets', $data);

    return $this->db->insert_id();
    }

    /**
     * Pulls Tickets For myTickets view
     */

    public function viewMyTickets($id = null)
    {
        $mtag = "userTickets-{$this->session->userdata('userid')}-$id";
        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('tickets');
            $this->db->where('userid', $this->session->userdata('userid'));
            $this->db->where('company', $this->session->userdata('company'));
            if (!empty($id)) $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results;

            if (!empty($id)) $data = $results[0];
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

        }

        return $data;

    }

    public function getTechTicketInfo($id, $tech)
    {
        $mtag = "userTechTickets-{$this->session->userdata('userid')}-$id";
        $data = $this->cache->memcached->get($mtag);
        if (empty($data))
        {
            $this->db->from('tickets');
            $this->db->where_in('userid', $tech->userid);
            if (!empty($id)) $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results;

            if (!empty($id)) $data = $results[0];
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

        }

        return $data;
    }
    /**
     * Gets Notes/Comments On Ticket You Are Viewing
     */

    public function getNotes($id)
    {
        $mtag = "ticketNotes$id";
        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('notes');
            $this->db->where('ticketID', $id);
            $this->db->order_by('id', 'ASC');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }

    /**
     * Allows User To Add Note/Comment To Ticket
     */

    public function addNote($p)
    {
        $note = array(
            'ticketid' => $p['id'],
            'userid' => $this->session->userdata('userid'),
            'datestamp' => DATESTAMP,
            'note' => $p['note'],
        );

        $this->db->insert('notes', $note);
    }

    public function getMyTechTickets()
    {
        $mtag = "myTechTickets{$this->session->userdata('userid')}";
        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->select('ticketid');
            $this->db->from('assignedTech');
            $this->db->where('userid', $this->session->userdata('userid'));

            $query = $this->db->get();

            $data = $query->result();
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

        }
        return $data;
    }

    /**
     * Gets Assigned Tech For That Ticket
     */

    public function getTech($ticketid)
    {
        if (empty($ticketid)) throw new Exception('TicketID is empty');

        $mtag = "ticketTech($ticketid}";
        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('userid');
            $this->db->from('assignedTech');
            $this->db->where('ticketid', $ticketid);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }

    /**
     * Gets Info About Specific Ticket
     */

    public function getTicketInfo($ticketid)
    {
        if (empty($ticketid)) throw new Exception('TicketID is empty');

        $mtag = "ticketInfo{$ticketid}";
        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            // $this->db->select('userid, status, issueDate, datestamp, issue');
            $this->db->from('tickets');
            $this->db->where('id', $ticketid);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Pulls All Tickets For Department The User Is Assigned To
     */

    public function getDepTickets($id = null)
    {
        $mtag = "depTickets{$this->session->userdata('department')}-$id";
        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->select('id');
            $this->db->from('tickets');
            $this->db->where('department', $this->session->userdata('department'));
            if (!empty($id)) $this->db->where('id', $id);

            $query = $this->db->get();

            $data = $query->result();

            if (!empty($id)) $data = $results[0];
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

        }
        return $data;
    }

    /**
     * Get A Count For My Tickets In Nav Bar
     */

   public function getMyTicketCount()
    {
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->from('tickets');
        $results = $this->db->count_all_results();

        return $results;
    }

    /**
     * Gets A Count For Departmnet Tickets In Nav Bar
     */

    public function getDepTicketCount()
    {
        $this->db->where('department', $this->session->userdata('department'));
        $this->db->from('tickets');
        $results = $this->db->count_all_results();

        return $results;
    }

    /**
     * Get A Count Of Tickets Assigned To User As A Tech
     */

    public function getMyTechTicketCount()
    {
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->from('assignedTech');
        $results = $this->db->count_all_results();

        return $results;
    }

    /**
     * Gets A List Of Users Assigned As Techs For That Department
     */

    public function getDepartmentTechs($departmentId)
    {
        $this->db->select('users.id');
        $this->db->from('users');
        $this->db->join('userCompanyDepartments','users.id = userCompanyDepartments.userid','left');
        $this->db->where('department', $departmentId);
        $this->db->where('tech', '1');

        $query = $this->db->get();

        $data = $query->result();

        return $data;
    }

    public function getUpdateTime($id)
    {
        $this->db->select_max('datestamp');
        $this->db->where('ticketID', $id);
        $this->db->from('notes');

        $query = $this->db->get();

        $results = $query->result();
        if (empty($results[0]->datestamp))
        {
            $this->db->select('datestamp');
            $this->db->where('id', $id);
            $this->db->from('tickets');

            $query = $this->db->get();

            $results = $query->result();
        }

        $data = $results[0];

        return $data;
    }
}
