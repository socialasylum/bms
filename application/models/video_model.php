<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class video_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    public function getFolderContent ($folder = 0)
    {
        // $folder = (empty($folder)) ? ' IS NULL' : ' = ' . intval($folder);

        $folders = $this->modules->getViewableFolderIDs('video');

        $this->db->select("id, name, ('1') AS type, active, ('') AS youtubeUrl, ('') AS thumbnail");
        $this->db->from('documentFolders');
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('parentFolder', $folder);
        $this->db->where('deleted', 0);
        $this->db->where_in('id', $folders);

        $query = $this->db->get();

        $dirResults = $query->result();

        // now gets items


        $this->db->select("videos.id, videos.title AS name, ('2') as type, videos.active, videos.youtubeUrl, videos.thumbnail");
        $this->db->from('videoFolderAssign');
        $this->db->join('videos', 'videoFolderAssign.videoId = videos.id', 'left');
        $this->db->where('videoFolderAssign.folderId', $folder);
        $this->db->where('videos.deleted', '0');
        $this->db->where('videos.company', $this->session->userdata('company'));

        $query = $this->db->get();

        $videoResults = $query->result();

        $results = array_merge($dirResults, $videoResults);

        return $results;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $video 
     *
     * @return TODO
     */
    public function getInfo ($video)
    {
        if (empty($video)) throw new Exception("Video ID is empty");

        $mtag = "videoInfo-{$video}-" . $this->session->userdata('company');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('videos');
            $this->db->where('id', $video);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];
            
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertVideo ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $this->session->userdata('company'),
                'userid' => $this->session->userdata('userid'),
                'title' => $p['title'],
                'fileName' => $p['fileName'],
                'youtubeUrl' => $p['youtubeUrl'],
                'thumbnail' => $p['thumbnail'],
                'youtubeData' => $p['youtubeData']
            );

        if (!empty($p['description'])) $data['description'] = $p['description'];
        if (!empty($p['tags'])) $data['tags'] = $p['tags'];

        // if (!empty($p['youtubeData'])) $data['youtubeData'] = $p['youtubeData'];

        $this->db->insert('videos', $data);

        return $this->db->insert_id();
    }

    public function assignVideoToFolder ($video, $folder)
    {
        $this->clearVideoFolderAssign($video);

        $folder = (empty($folder)) ? '0' : $folder;

        if (empty($video)) throw new Exception("Video ID is empty!");

        $data = array
            (
                'videoId' => $video,
                'folderId' => $folder
            );

        $this->db->insert('videoFolderAssign', $data);

        return true;
    }

    /**
     * Clears previous item folder assign
     *
     * @param int $video - Video ID
     *
     * @return boolean
     */
    public function clearVideoFolderAssign ($video)
    {
        if (empty($video)) throw new Exception("Video ID is empty!");

        $this->db->where('videoId', $video);
        $this->db->delete('videoFolderAssign');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $video 
     *
     * @return TODO
     */
    public function insertVideoView ($video)
    {
        $video = intval($video);

        if (empty($video)) throw new Exception("Video ID is empty");

        $data = array
            (
                'videoId' => $video,
                'userid' => $this->session->userdata('userid'),
                'datestamp' => DATESTAMP
            );

        $this->db->insert('videoViews', $data);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $video 
     *
     * @return TODO
     */
    public function getCompany ($video)
    {
        if (empty($video)) throw new Exception("Video ID is empty");

        $mtag = "videoComp-{$video}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('company');
            $this->db->from('videos');
            $this->db->where('id', $video);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->company;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }
}
