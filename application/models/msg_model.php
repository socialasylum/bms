<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class msg_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function getMsgFolders($mbox, $mbox_url)
    {
        return $this->msgs->getMsgFolders($mbox, $mbox_url);
    }

    public function check_sync_lock($userid) {
        if (empty($userid))
            throw new Exception("User ID is empty");

        if (file_exists("/tmp/bms_sync_lock_{$userid}"))
            return true;

        return false;
    }

    /**
     * deprecated
     *
     * @param mixed $userid 
     * @param int $folder (optional) - defaults to 1 (inbox)
     *
     * @return TODO
     */
    public function getMessages($userid, $folder = 1, $lastID = 0, $returnArray = false, $limit = 0) {
        if (empty($userid))
            throw new Exception("userid is empty!");

        if (empty($limit))
            $limit = (int) $this->config->item('msgLimit'); // uses config limit

        $mtag = "messages-{$userid}-{$folder}-{$limit}-{$returnArray}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            /*
              $this->db->select("messages.id, messageTo.datestamp, messages.emailDate, messages.subject, messages.from, messageTo.read, messageTo.account, messages.extFrom");
              $this->db->from('messageTo');
              $this->db->join('messages', 'messages.id = messageTo.msgId', 'left');
              $this->db->where('messageTo.to', $userid);
              $this->db->where('messageTo.folder', $folder);
              $this->db->where('messageTo.deleted', '0');

              if (!empty($lastID)) $this->db->where('messages.id <', $lastID);

              $this->db->order_by('messages.emailDate', 'desc');

              $this->db->limit($limit);

              $query = $this->db->get();

              $data = ($returnArray) ? $query->result_array() : $query->result();

              $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

              //elog($this->db->last_query()); */
        }


        return $data;
    }

    // gets all folder messages for a user
    /*public function getFolderMsgs($userid, $folder, $lastID = 0, $limit = 0) {
        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($folder))
            throw new Exception("Folder ID is empty!");

        if (empty($limit))
            $limit = (int) $this->config->item('msgLimit'); // uses config limit

        $mtag = "getFolderMsgs-{$userid}-{$folder}-{$lastID}-{$limit}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->select('id, datestamp, msgId, emailAddress, read, account');
            $this->db->from("messageTo");
            $this->db->where('to', $userid);
            $this->db->where('folder', $folder);

            if (!empty($lastID))
                $this->db->where('msgId <', $lastID);

            $this->db->order_by('datestamp', 'desc');
            $this->db->limit($limit);

            $data = $this->db->get()->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }*/
    
    /*
     * Rewriting getFolderMsgs to use IMAP instead of DB
     */
    
    public function getFolderMsgs($mbox)
    {
        $MC = imap_check($mbox->mbox);
        $messages = imap_fetch_overview($mbox->mbox, "1:{$MC->Nmsgs}", 0);
        $sort_array = array();
        foreach ($messages as $msg)
        {
            $sort_array[$msg->udate] = $msg;
        }
        krsort($sort_array);
        return $sort_array;
    }

    // gets body of msg
    /*
    public function getBody($msg) {
        if (empty($msg))
            throw new Exception("Message ID is empty!");

        $mtag = "getBody-{$msg}";

        $data = $this->bms->checkCacheData($mtag);

        if ($data === false) {
            $this->db->select('body');
            $this->db->from('messages');
            $this->db->where('id', $msg);

            $data = $this->db->get()->result()[0]->body;

            $this->bms->saveCacheData($mtag, $data);
        }

        return $data;
    }*/
    
    /*
     * Rewriting getBody to use IMAP instead of DB
     */
    
    public function getBody($mbox, $id, $html = false)
    {
        $body = imap_fetchbody($mbox->mbox, $id, ($html) ? '2' : '1');
        $decoded = ($html) ? quoted_printable_decode($body) : $body;
        return $decoded;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $account 
     * @param mixed $folder  
     *
     * @return TODO
     */
    public function getAccountMessages($account, $folder, $lastInternalID = 0) {
        throw new Exception("Function Deprecated!");
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function getSentMessages($userid, $lastInternalID = 0) {

        if (empty($userid))
            throw new Exception("userid is empty!");

        $mtag = "sentMsg{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->db->select("id, datestamp, subject, from");
            $this->db->from('messages');
            $this->db->where('from', $userid);

            if (!empty($lastInternalID))
                $this->db->where('id <', $lastInternalID);

            $this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     * @param mixed $id     
     *
     * @return TODO
     */
    /*
    public function getMessageInfo($id) {
        if (empty($id))
            throw new Exception("id is empty!");

        $mtag = "getMessageInfo-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {
            $this->db->select('id, emailDate, from, subject, extMsgID, parsed');
            $this->db->from('messages');
            $this->db->where('id', $id);
            $this->db->where('deleted', '0');

            $data = $this->db->get()->result()[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }*/
    
    /*
     * Rewriting getMessageInfo to use IMAP instead of DB
     */
    
    public function getMessageInfo($mbox, $id)
    {
        $data = new stdClass();
        $MC = imap_check($mbox->mbox);
        $msg_info = imap_fetch_overview($mbox->mbox, "$id,$id", 0);
        $data->emailDate = $msg_info[0]->udate;
        $data->from = $msg_info[0]->from;
        $data->to = $msg_info[0]->to;
        $data->subject = imap_utf8($msg_info[0]->subject);
        $data->extMsgID = $id;
        $data->structure = imap_fetchstructure($mbox->mbox, $id);
        $data->parsed = $this->getBody($mbox, $id, ($data->structure->subtype == 'ALTERNATIVE') ? true : false);
        return $data;
    }

    /**
     * Checks if user was sent a message
     * this is used so the user cannot access messages not sent to them simply by
     * entering random ID'
     *
     * @param INT $userid - users ID
     * @param INT $msgId - message ID
     *
     * @return boolean - true if has access
     */
    public function checkUserAssignedToMessage($userid, $msgId) {
        return $this->msgs->checkUserAssignedToMessage($userid, $msgId);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $query 
     *
     * @return TODO
     */
    public function userSearch($q) {
        $mtag = 'userSearch-' . $q;

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->select("id, firstName, lastName, email");
            $this->db->from('users');
            $this->db->where("(`status` = '1' AND `deleted` = '0' AND `email` LIKE '%{$q}%')");
            $this->db->or_where("(`status` = '1' AND `deleted` = '0' AND `firstName` LIKE '%{$q}%')");
            $this->db->or_where("(`status` = '1' AND `deleted` = '0' AND `lastName` LIKE '%{$q}%')");


            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data))
            return false;

        // $return = array('optons');

        foreach ($data as $r) {
            $return[] = array
                (
                'id' => $r->id,
                'label' => "{$r->firstName} {$r->lastName} ({$r->email})",
                'value' => "{$r->firstName} {$r->lastName} ({$r->email})"
            );
        }

        return $return;
    }

    /**
     * TODO: short description.
     *
     * @param array $p - post array
     *
     * @return TODO
     */
    public function insertMessage($p) {

        $data = array
            (
            'datestamp' => DATESTAMP,
            'from' => $this->session->userdata('userid'),
            'subject' => $p['subject'],
            'body' => $p['body']
        );

        if (!empty($p['emailDate']))
            $data['emailDate'] = $p['emailDate'];
        else
            $data['emailDate'] = DATESTAMP;

        if (!empty($p['extMsgID']))
            $data['extMsgID'] = $p['extMsgID'];
        if (!empty($p['extFrom']))
            $data['extFrom'] = $p['extFrom'];

        $this->db->insert('messages', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $to     
     * @param mixed $type   
     * @param mixed $folder Optional, defaults to 1. 
     *
     * @return TODO
     */
    public function insertMessageTo($msgId, $to, $type, $folder = 1, $read = false, $emailAddress = null, $account = 0, $date = null) {
        if (empty($msgId))
            throw new Exception("message ID is empty!");

        $date = (empty($date)) ? DATESTAMP : $date;

        $data = array
            (
            'datestamp' => $date,
            'msgId' => $msgId,
            'to' => $to,
            'type' => $type,
            'folder' => $folder,
        );

        if (!empty($emailAddress))
            $data['emailAddress'] = $emailAddress;
        if (!empty($account))
            $data['account'] = $account;

        if ($read == true)
            $data['read'] = '1';

        $this->db->insert('messageTo', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $msgId
     *
     * @return TODO
     */
    public function getMessageTo($msgId, $type) {

        if (empty($msgId))
            throw new Exception("msgId is empty!");
        if (empty($type))
            throw new Exception("type is empty!");

        $mtag = "messageTo-{$msgId}-{$type}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->db->select('to, emailAddress');
            $this->db->from('messageTo');
            $this->db->where('msgId', $msgId);
            $this->db->where('type', $type);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        /*
          $return = array();

          if (!empty($data))
          {
          foreach ($data as $r)
          {
          $name = (empty($r->to)) ? $r->emailAddress : $this->users->getName($r->to);

          $return[] = $name;
          }
          }

          return $return;
         */

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getFolderTotal($mbox, $onlyUnread = true)
    {
        return imap_check($mbox->mbox)->Nmsgs;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function getSentTotal($userid) {
        if (empty($userid))
            throw new Exception("from (user id) is empty!");

        $mtag = "sentMsgTotal{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->from('messages');
            $this->db->where('from', $userid);
            $this->db->where('deleted', 0);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id     
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function setMessageread($id, $userid) {

        if (empty($id))
            throw new Exception("message id is empty!");
        if (empty($userid))
            throw new Exception("user id is empty!");

        $data = array
            (
            'read' => '1'
        );

        $this->db->where('msgId', $id);
        $this->db->where('to', $userid);
        $this->db->update('messageTo', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folderName 
     *
     * @return TODO
     */
    public function createFolder($folderName, $mbox) {
        if (empty($folderName))
            throw new Exception("Foldername is empty!");

        $folder = $this->__insertFolder($this->session->userdata('userid'), $folderName, 1, $mbox);

        return $folder;
    }

    /*protected function __insertFolder($userid, $folderName, $deletable, $inbox = 0, $outbox = 0, $sent = 0, $junk = 0, $trash = 0, $drafts = 0, $parentFolder = 0) {

        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($folderName))
            throw new Exception("Foldername is empty!");

        $data = array
            (
            'datestamp' => DATESTAMP,
            'userid' => $userid,
            'name' => $folderName,
            'deletable' => $deletable,
            'inbox' => $inbox,
            'outbox' => $outbox,
            'sent' => $sent,
            'junk' => $junk,
            'trash' => $trash,
            'drafts' => $drafts,
            'parentFolder' => $parentFolder,
            'deleted' => 0
        );

        $this->db->insert('msgFolders', $data);

        //elog($this->db->last_query());

        return $this->db->insert_id();
    }
    */
    protected function __insertFolder($userid, $folderName, $deletable, $mbox, $inbox = 0, $outbox = 0, $sent = 0, $junk = 0, $trash = 0, $drafts = 0, $parentFolder = 0) {

        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($folderName))
            throw new Exception("Foldername is empty!");

        imap_createmailbox($mbox->mbox, $mbox->url . $folderName);
        
        return 1;
    }
    

    /**
     * TODO: short description.
     *
     * @param mixed $msg    
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function moveMsg($mbox, $msg, $folder) {
        if (empty($msg))
            throw new Exception("message id is empty!");
        if (empty($folder))
            throw new Exception("folder is empty!");

        $moved = imap_mail_move($mbox->mbox, $msg, $folder);
        imap_expunge($mbox->mbox);
        return $moved;
    }

    /**
     * Sets all messages in Trash folder to deleted
     */
    public function emptyTrash($userid) {
        if (empty($userid))
            throw new Exception("User ID is empty!");

        $trashFolder = $this->getTypeFolderID($userid, 'trash');

        if (empty($trashFolder))
            throw new Exception("Unable to get Trash folder ID");

        $data = array
            (
            'deleted' => 1
        );

        $this->db->where('folder', $trashFolder);
        $this->db->where('to', $userid);
        $this->db->update('messageTo', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $signature 
     *
     * @return TODO
     */
    public function insertSignature($userid, $signature) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception("User ID is empty!");

        $data = array
            (
            'userid' => $this->session->userdata('userid'),
            'company' => $this->session->userdata('company'),
            'signature' => $signature
        );

        $this->db->insert('userSigs', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $signature 
     *
     * @return TODO
     */
    public function updateSignature($userid, $signature) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception("User ID is empty!");

        $data = array
            (
            'signature' => $signature
        );

        $this->db->where('userid', $userid);
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->update('userSigs', $data);

        return true;
    }

    // function moved to msgs lib
    public function getAccountInfo($id) {
        return $this->msgs->getAccountInfo($id);
    }

    // function moved to msgs lib
    public function getAdditionalAccounts($user = 0, $type = 0) {
        throw new Exception("Function has been deprecated");
        return false;
    }

    public function openAccount($url, $username, $passwd, $timeout = 5) {
        return $this->msgs->openAccount($url, $username, $passwd, $timeout);
    }

    public function buildImapUrl($server, $port, $type, $ssl, $auth) {
        return $this->msgs->buildImapUrl($server, $port, $type, $ssl, $auth);
    }

    public function insertEmailMsg($account, $folder, $d) {
        throw new Exception("This function has been deprecated");
    }

    // checks if an email message has already been inserted into DB
    public function checkEmailInserted($account, $folder, $msgID) {
        throw new Exception("This function has been deprecated");
    }

    public function getLatestEmailID($account, $folder) {
        throw new Exception("This function has been deprecated");
    }

    public function getEmailInfo($account, $id) {
        throw new Exception("Function Deprecated");
    }

    public function getAccountFolderCnt($account, $folder, $unreadOnly = false) {
        if (empty($account))
            throw new Exception("Account ID is empty!");
        if (empty($folder))
            throw new Exception("Folder is empty!");

        $mtag = "emailFolderCnt-{$account}-{$folder}-" . (int) $unreadOnly;

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->from('emailMsgs');
            $this->db->where('account', $account);
            $this->db->where('folder', $folder);
            $this->db->where('deleted', 0);

            if ($unreadOnly == true)
                $this->db->where('read', 0);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id      
     * @param mixed $account 
     *
     * @return TODO
     */
    public function setEmailMessageRead($id, $account) {
        throw new Exception("This function has been deprecated");
        /*
          if (empty($id)) throw new Exception("E-mail message ID is empty!");
          if (empty($account)) throw new Exception("Account ID is empty!");

          $data = array
          (
          'read' => '1'
          );

          $this->db->where('account', $account);
          $this->db->where('msgID', $id);
          $this->db->update('emailMsgs', $data);

          return true;
         */
    }

    public function checkDefaultFolders($userid) {
        if (empty($userid))
            throw new Exception('userid is empty!');

        $inbox = $outbox = $sent = $junk = $trash = $drafts = false;

        $folders = $this->getMsgFolders($userid);

        if (!empty($folders)) {
            foreach ($folders as $r) {
                if ($r->inbox > 0)
                    $inbox = true;
                if ($r->outbox > 0)
                    $outbox = true;
                if ($r->sent > 0)
                    $sent = true;
                if ($r->junk > 0)
                    $junk = true;
                if ($r->trash > 0)
                    $trash = true;
                if ($r->drafts > 0)
                    $drafts = true;
            }
        }

        if (!$inbox)
            $this->__insertFolder($userid, 'Inbox', 0, 1);
        if (!$outbox)
            $this->__insertFolder($userid, 'Outbox', 0, 0, 1);
        if (!$sent)
            $this->__insertFolder($userid, 'Sent', 0, 0, 0, 1);
        if (!$junk)
            $this->__insertFolder($userid, 'Junk', 0, 0, 0, 0, 1);
        if (!$trash)
            $this->__insertFolder($userid, 'Trash', 0, 0, 0, 0, 0, 1);
        if (!$drafts)
            $this->__insertFolder($userid, 'Drafts', 0, 0, 0, 0, 0, 0, 1);

        return true;
    }

    public function getTypeFolderID($userid, $type = 'inbox') {
        $type = strtolower($type);

        if (empty($type))
            throw new Exception("Type is empty!");

        $mtag = "getTypeFolderID-{$userid}-{$type}-";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->from('msgFolders');

            $this->db->where('userid', $userid);
            $this->db->where($type, 1);

            $data = $this->db->get()->result()[0]->id;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function deleteFolder($mbox, $folder) {
        if (empty($folder))
            throw new Exception('Folder ID is empty!');
        
        return imap_deletemailbox($mbox->mbox, $mbox->url . $folder);
    }

    /**
     * removes account
     */
    public function deleteMsgAccount($userid, $accountID) {
        $userid = intval($userid);
        $accountID = intval($accountID);

        if (empty($userid))
            throw new Exception('userid is empty!');
        if (empty($accountID))
            throw new Exception('Account ID is empty!');


        $this->_deleteMsgAccountFolders($accountID);
        $this->_deleteAccountEmails($accountID);

        $this->db->where('userid', $userid);
        $this->db->where('id', $accountID);
        $this->db->delete('msgAccounts');

        return true;
    }

    // deletes all messages for an account
    private function _deleteAccountEmails($account) {
        $msgs = $this->_getMessageToFromAccount($account);

        if (!empty($msgs)) {
            foreach ($msgs as $r) {
                $this->_deleteMsg($r->msgId); // deletes msg row
            }
        }


        $this->db->where('account', $account);
        $this->db->delete('messageTo');

        return true;
    }

    // deletes actual row from messages table
    private function _deleteMsg($id) {
        $this->db->where('id', $id);
        $this->db->delete('messages');

        return true;
    }

    // gets all the messageTo messages from account ID
    private function _getMessageToFromAccount($account) {
        if (empty($account))
            throw new Exception("Account ID is empty!");

        $this->db->from('messageTo');
        $this->db->where('account', $account);

        return $this->db->get()->result();
    }

    // removes account folder data
    private function _deleteMsgAccountFolders($account) {
        if (empty($account))
            throw new Exception("Account ID is empty!");

        // gets folders associated with the msg accounts
        $accountFolders = $this->_getMsgAccountFolders($account);

        if (!empty($accountFolders)) {
            foreach ($accountFolders as $r) {
                $this->_deleteMsgFolder($r->folder);
            }
        }

        $this->db->where('account', $account);
        $this->db->delete('msgAccountFolders');

        return true;
    }

    /**
     * used to test a connection
     */
    public function testAccount($server, $port, $type, $username, $passwd, $ssl, $auth) {
        $valid = true;

        if (empty($server))
            throw new Exception("No Server specified!");

        $url = $this->buildImapUrl($server, $port, $type, $ssl, $auth);

        $mbox = $this->msgs->openAccount($url, $username, $passwd, 5, true);

        if ($mbox === false)
            $valid = false;

        $check = imap_check($mbox);

        if ($check === false)
            $valid = false;

        imap_close($mbox);

        return $valid;
    }

    public function testSMTP($server, $port, $username, $passwd, $timeout = 30) {
        $conn = fsockopen($server, $port, $errno, $errstr, $timeout);

        if ($conn == false)
            throw new Exception("Unable to connect to SMTP Server {$server}<br><b>Error </b> ({$errorno}): {$errstr}");

        // SMTP server can take longer to respond, give longer timeout for first read
        // Windows does not have support for this timeout function
        if (substr(PHP_OS, 0, 3) != "WIN")
            socket_set_timeout($conn, $timeout, 0);

        return true;
    }

    public function getExternalFolders($userid, $downloadMsg = false, GearmanJob $job = null) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $accounts = $this->msgs->getAdditionalAccounts($userid, array(1, 2));

        if (empty($accounts))
            return true;

        foreach ($accounts as $r) {
            #error_log(print_r($r, true));
            // connects to account			
            $conn = $this->msgs->accountConnect($r->id);

            $boxes = imap_getmailboxes($conn->mbox, $conn->url, '*');

            $url = $conn->url;

            //shits empty, continue
            if (empty($boxes) || !is_array($boxes))
                continue;

            foreach ($boxes as $k => $v) {

                $box = str_replace($url, '', $v->name);

                $mc = imap_status($conn->mbox, $v->name, SA_UNSEEN + SA_MESSAGES);


                $folder = $this->checkFolderExists($box);

                if ($folder === false) {
                    $folder = $this->__insertFolder($userid, $box, 0, 0, 0, 0, 0, 0, 0, 0);
                } else {
                    if ($downloadMsg) {
                        $totalInDB = $this->getMsgAccountFolderTotal($r->id, $folder);

                        //elog("{$box} DB: {$totalInDB} Server: {$mc->messages}");

                        if ($totalInDB == (int) $mc->messages) {
                            // no new messages no need to download
                            if (!empty($job))
                                $job->sendData("No new messages in {$box}\n");
                            continue;
                        }
                    }
                }

                // folder has been created will now clear previous assigned
                $this->updateMsgAccountFolder($userid, $r->id, $folder, $mc->messages, $mc->unseen);

                if ($downloadMsg) {
                    if (!empty($job)) {
                        $msg = "Downloading {$box} Total: $mc->messages Unread: $mc->unseen";

                        $job->sendData("{$msg}\n");
                        elog($msg);
                    }

                    $this->downloadAccountFolderEmails($userid, $r->id, $folder, $box, 0, 0, $conn, false, $job);
                }
            }

            // closes imap connection
            imap_close($conn->mbox);
        }

        return true;
    }

    public function updateMsgAccountFolder($userid, $account, $folder, $total = 0, $totalUnread = 0) {
        $maf = $this->_msgAccountFolderExists($account, $folder);

        if ($maf === false) {
            $maf = $this->_insertMsgAccountFolder($userid, $account, $folder, $total, $totalUnread);
        } else
            $this->updateAccountFolder($maf, $total, $totalUnread);

        return true;
    }

    private function _removeMsgAccountFolder($account, $folder) {
        if (empty($account))
            throw new Exception("Account ID is empty!");
        if (empty($folder))
            throw new Exception("Folder ID is empty!");

        $this->db->where('account', $account);
        $this->db->where('folder', $folder);
        $this->db->delete('msgAccountFolders');

        return true;
    }

    private function _insertMsgAccountFolder($userid, $account, $folder, $total = 0, $totalUnread = 0, $parentFolder = 0) {
        $userid = intval($userid);
        $account = intval($account);
        $folder = intval($folder);

        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($account))
            throw new Exception("Account ID is empty!");
        if (empty($folder))
            throw new Exception("Folder ID is empty!");

        $data = array
            (
            'userid' => $userid,
            'account' => $account,
            'folder' => $folder,
            'total' => $total,
            'totalUnread' => $totalUnread,
            'parentFolder' => $parentFolder
        );

        $this->db->insert('msgAccountFolders', $data);

        return $this->db->insert_id();
    }

    // alias of _getMsgAccountFolder
    private function _getMsgAccountFolders($account) {
        return $this->_getMsgAccountFolder($account);
    }

    private function _getMsgAccountFolder($account, $folder = 0) {
        $mtag = "_getMsgAccountFolder-{$account}-{$folder}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->db->from('msgAccountFolders');
            $this->db->where('account', $account);

            if (!empty($folder))
                $this->db->where('folder', $folder);

            $data = $this->db->get()->result();

            if (!empty($folder))
                $data = $data[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    // checks if a folder is assigned to an account
    private function _msgAccountFolderExists($account, $folder) {
        $mtag = "_msgAccountFolderExists-{$account}-{$folder}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->db->select('id');
            $this->db->from('msgAccountFolders');
            $this->db->where('account', $account);
            $this->db->where('folder', $folder);

            $query = $this->db->get();

            $data = $query->result();

            $data = $data[0]->id;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data))
            return false;

        return $data;
    }

    public function updateAccountFolder($id, $total = 0, $totalUnread = 0) {
        if (empty($id))
            throw new Exception("Folder ID is empty");

        $data = array
            (
            'total' => $total,
            'totalUnread' => $totalUnread
        );

        $this->db->where('id', $id);
        $this->db->update('msgAccountFolders', $data);

        return true;
    }

    protected function __getBoxes($mbox, $pattern = '*') {
        throw new Exception("Function has been deprecated");
    }

    public function checkFolderExists($name) {
        $mtag = "checkFolderExists-{$name}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->db->select('id');
            $this->db->from('msgFolders');
            $this->db->where('name', $name);

            $query = $this->db->get();

            $data = $query->result();

            $data = $data[0]->id;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data))
            return false;

        return $data;
    }

    // checks if an external message has already been downloaded, if os, uses that version
    private function _checkEmailCached($id) {
        $mtag = "_checkEmailCached-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data)) {
            $this->db->from('messages');
            $this->db->where('extMsgID', $id);

            $query = $this->db->get();

            $data = $query->result();

            $data = $data[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data))
            return false;

        return $data;
    }

    public function getExternalEmail($userid, $account, $id) {

        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($account))
            throw new Exception("Account ID is empty");

        $data = $this->_checkEmailCached($id);

        $info = $this->getAccountInfo($account);

        $conn = $this->msgs->accountConnect($info);

        //$body = $this->msgs->getEmailBody($conn->mbox, $id);

        imap_close($conn->mbox);

        return $body;
    }

    // gets the emails inside an accounts folder limited to 20 by default
    public function downloadAccountFolderEmails($userid, $account, $folder, $folderName, $lastID = 0, $page = 1, $conn = null, $closeConnection = true, GearmanJob $job = null) {
        $data = array();

        $account = intval($account);

        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($account))
            throw new Exception("Account ID is empty");

        // connects to account
        if (empty($conn))
            $conn = $this->msgs->accountConnect($account);

        $re = $conn->url . $folderName;

        imap_reopen($conn->mbox, $re);

        $c = imap_check($conn->mbox);

        if (empty($c->Nmsgs))
            return false;

        if (empty($page)) {
            $to = 1;
            $from = $c->Nmsgs;
        } else {
            $l = (int) $this->config->item('msgLimit');

            $from = (empty($lastID)) ? $c->Nmsgs : $lastID;

            if (!empty($lastID))
                $from = ($from - 1) - $l;

            $to = $from - $l;
        }

        //elog("{$to}:{$from}");

        $raw = imap_fetch_overview($conn->mbox, "{$to}:{$from}");


        $downloaded = $cached = 0;

        foreach ($raw as $k => $e) {
            $read = (empty($e->seen)) ? false : true;


            $date = strtotime($e->date);

            $date = date('Y-m-d H:i:s', $date);

            // does not yet add body
            $msgData = array
                (
                'subject' => $e->subject,
                'emailDate' => $date,
                'extMsgID' => $e->msgno,
                'extFrom' => $e->from
            );

            $cached = $this->_checkEmailCached($e->msgno);

            if ($cached)
                $cached++;
            else {
                $msgID = $this->insertMessage($msgData);

                $this->insertMessageTo($msgID, $userid, 1, $folder, $read, $e->from, $account, $date);

                $downloaded++;
            }
        }

        if (!empty($job)) {
            $job->sendData("Downloaded: {$downloaded} ( {$cached} cached )\n");
            elog("Downloaded: {$downloaded} ({$cached} cached)");
        }

        if ($closeConnection) {
            // closes imap connection
            imap_close($conn->mbox);
            unset($conn);
        }

        if (count($data) == 1)
            $data = $data[0];

        return true;
    }

    public function getMsgAccountFolderTotal($account, $folder) {
        $mtag = "getMsgAccountFolderTotal-{$account}-{$folder}";

        $data = $this->bms->checkCacheData($mtag);

        if ($data === false) {
            $this->db->select('total');
            $this->db->from('msgAccountFolders');
            $this->db->where('account', $account);
            $this->db->where('folder', $folder);



            $data = (int) $this->db->get()->result()[0]->total;

            $this->bms->saveCacheData($mtag, $data);
        }

        return $data;
    }

    public function combinedFolderContent($userid, $folder, $folderName, $lastIDOverall, $accountIDs, $page = 1) {

        $this->getAccountFolderMsgs($userid, $folder, $folderName, $accountIDs);


        if ($info->sent > 0) {
            $data = $this->getSentMessages($userid);
        } else {
            $data = $this->getMessages($userid, $folder, $lastIDOverall, true);
        }

        #$data = PHPFunctions::sortByDate($data);

        return $data;
    }

    // gets the messages for each account within a particular folder
    /*public function getAccountFolderMsgs($userid, $folder, $folderName, $accountIDs, $page = 1) {

        $accountIDs = $this->msgs->parseLastID($accountIDs);

        $accounts = $this->msgs->getAdditionalAccounts($userid, array(1, 2));

        // downlaods the messages for each account for that folder
        if (!empty($accounts)) {
            foreach ($accounts as $r) {
                $exists = $this->_msgAccountFolderExists($r->id, $folder);

                if ($exists === false)
                    continue;

                $lastID = $this->msgs->findAccountLastID($accountIDs, $r->id);

                $this->downloadAccountFolderEmails($userid, $r->id, $folder, $folderName, $lastID, $page);
            }
        }

        return true;
    }*/
    
    /*
     * Rewriting getAccountFolderMsgs to use new stuff
     */
    public function getAccountFolderMsgs($userid, $folder, $folderName, $accountIDs, $page = 1)
    {
        
    }

    // actually deletes msg folder - will not allow deletion of primary default folders
    private function _deleteMsgFolder($id) {
        //if (empty($userid)) throw new Exception("User ID is empty!");
        if (empty($id))
            throw new Exception("Folder ID is empty!");

        $this->db->where('inbox', 0);
        $this->db->where('outbox', 0);
        $this->db->where('sent', 0);
        $this->db->where('junk', 0);
        $this->db->where('trash', 0);
        $this->db->where('drafts', 0);
        //$this->db->where('deletable', 1);
        $this->db->where('id', $id);

        //$this->db->where('userid', $userid);

        $this->db->delete('msgFolders');

        return true;
    }

    // saves response from account syncing
    public function saveSyncStatus($userid, $status, $response) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception("UserID is empty!");

        $data = array
            (
            'datestamp' => date('Y-m-d G:i:s'),
            'userid' => $userid,
            'status' => $status,
            'response' => json_encode($response)
        );

        $this->db->insert('msgAccountSync', $data);

        sleep(1); // sleeps for 1 secon to ensure following status have a later date

        return $this->db->insert_id();
    }

    public function execSync($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception("UserID is empty!");

        //if the log file is broken then this doesnt work
        //$cmd = "php {$_SERVER['DOCUMENT_ROOT']}index.php gearman sync {$userid} > {$_SERVER['DOCUMENT_ROOT']}../logs/gearman_sync.log &";
        $cmd = "php {$_SERVER['DOCUMENT_ROOT']}index.php gearman sync {$userid} &";

        exec($cmd);

        return true;
    }

    public function getLastSyncResponse($userid) {
        $mtag = "getLastSyncResponse-{$userid}";

        $data = $this->bms->checkCacheData($mtag);

        if ($data === false) {
            $this->db->select('status, response');
            $this->db->from('msgAccountSync');
            $this->db->where('userid', $userid);
            $this->db->order_by('id', 'desc');
            $this->db->limit(1);

            $data = $this->db->get()->result()[0];

            $this->bms->saveCacheData($mtag, $data);
        }

        return $data;
    }
    
    public function updatePassword($email, $pass)
    {
        $this->db->set('passwd', $this->encrypt->encode($pass));
        $this->db->where('emailAddress', $email);
        return $this->db->update('msgAccounts');
    }
}
