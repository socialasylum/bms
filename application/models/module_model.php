<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class module_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getModules ()
    {
        $mtag = "modules";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, name, url, namespace, sellable, unvPackage, price');
            $this->db->from('modules');
            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }


        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getModuleInfo ($module)
    {
        if (empty($module)) throw new Exception("module ID is empty!");

        $this->db->from('modules');
        $this->db->where('id', $module);

        $query = $this->db->get();

        $results = $query->result();

        return $results[0];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateModule ($p)
    {
        if (empty($p['id'])) throw new Exception("module ID is empty!");

        $data = array
            (
                'name' => $p['name'],
                'description' => $p['description'],
                'shortDesc' => $p['shortDesc'],
                'url' => $p['url'],
                'namespace' => $p['namespace'],
                'price' => $p['price'],
                'active' => $p['active'],
                'icon' => $p['icon'],
                'sellable' => $p['sellable'],
                'unvPackage' => $p['unvPackage'],
                'usesFolders'=> $p['usesFolders']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('modules', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertModule ($p)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'name' => $p['name'],
                'description' => $p['description'],
                'shortDesc' => $p['shortDesc'],
                'url' => $p['url'],
                'namespace' => $p['namespace'],
                'price' => $p['price'],
                'active' => $p['active'],
                'icon' => $p['icon'],
                'unvPackage' => $p['unvPackage'],
                'sellable' => $p['sellable'],
                'usesFolders'=> $p['usesFolders']
            );

        $this->db->insert('modules', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function getModuleCompanies ($module)
    {
        if (empty($module)) throw new Exception("module ID is empty");

        $this->db->select('company');
        $this->db->from('companyModules');
        $this->db->where('module', $module);

        $query = $this->db->get();

        return $query->result();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function clearCompanyModules ($module)
    {
        if (empty($module)) throw new Exception("module ID is empty");

        $this->db->where('module', $module);
        $this->db->delete('companyModules');

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $companies 
     *
     * @return TODO
     */
    public function insertCompanymodules ($module, $companies)
    {
        if (empty($module)) throw new Exception("module ID is empty!");

        if (empty($companies)) return true;

        foreach ($companies as $company)
        {
            if (empty($company)) continue;

            $data = array
                (
                    'company' => $company,
                    'module' => $module,
                );

            $this->db->insert('companyModules', $data);
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getModuleWidgets ($module, $id = 0)
    {
        if (empty($module)) throw new Exception("module ID is empty!");

        $this->db->from('widgets');
        $this->db->where('module', $module);
        if (!empty($id)) $this->db->where('id', $id);
        $this->db->order_by('name', 'asc');

        $query = $this->db->get();

        $results = $query->result();

        if (!empty($id)) return $results[0];

        return $results;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $_POST 
     *
     * @return TODO
     */
    public function insertWidget ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'name' => $p['name'],
                'url' => $p['url'],
                'module' => $p['module'],
                'active' => $p['active']
            );

        $this->db->insert('widgets', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateWidget ($p)
    {
        if (empty($p['id'])) throw new Exception("widget ID is empty!");

        $data = array
            (
                'name' => $p['name'],
                'url' => $p['url'],
                'active' => $p['active']
            );


        $this->db->where('id', $p['id']);
        $this->db->update('widgets', $data);

        return $p['id'];
    }
}
