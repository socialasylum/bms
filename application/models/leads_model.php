<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class leads_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getLeads ($assignedTo = 0, $assignedFrom = 0, $countOnly = false, $chapter = false)
    {

        $assignedTo = intval($assignedTo);
        $assignedFrom = intval($assignedFrom);

        if ($chapter == false)
        {
            if (empty($assignedTo) && empty($assignedFrom)) throw new Exception("Assigned To or From are not set!");
        }

        $mtag = "leads-{$assignedTo}-{$assignedFrom}-" . (int) $countOnly . '-' . (int) $chapter;

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id');
            $this->db->from('leads');
            $this->db->where('active', 1);
            $this->db->where('company', $this->session->userdata('company'));

            if (!empty($assignedTo)) $this->db->where('assignedTo', $assignedTo);
            if (!empty($assignedFrom)) $this->db->where('assignedFrom', $assignedFrom);

            if ($chapter == true) $this->db->where('assignedTo', 0);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertLead ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'contactName' => $p['contactName'],
                'companyName' => $p['companyName'],
                'leadDate' => $p['leadDate'],
                'assignedFrom' => $p['from'],
                'assignedTo' => $p['to'],
                'contactType' => $p['contactType'],
                'amount' => $p['amount'],
                'status' => 1,
                'details' => $p['details']
            );

        $this->db->insert('leads', $data);

        return $this->db->insert_id();
    }

    /**
     * updates lead
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateLead ($p)
    {
        if (empty($p['id'])) throw new Exception('Lead ID is empty');

        $data = array
            (
                'datestamp' => DATESTAMP,
                'contactName' => $p['contactName'],
                'companyName' => $p['companyName'],
                'leadDate' => $p['leadDate'],
                'assignedFrom' => $p['from'],
                'assignedTo' => $p['to'],
                'contactType' => $p['contactType'],
                'amount' => $p['amount'],
                'status' => 1,
                'details' => $p['details']
            );


        // to ensure only their company leads are updated
        $this->db->where('company', $this->session->userdata('company'));

        $this->db->where('id', $p['id']);
        $this->db->update('leads', $data);

        return $p['id'];
    }

    public function clearAddresses ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $this->db->where('leadId', $lead);
        $this->db->delete("leadAddress");

        return true;
    }

    /**
     * used to save array of email addresses from lead edit page
     *
     * @param mixed $emails 
     *
     * @return TODO
     */
    public function saveEmails ($lead, $emails)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        if (!empty($emails))
        {
            foreach ($emails as $email)
            {
                // skips email if empty
                if (empty($email)) continue;

                $this->insertLeadEmail($lead, $email);
            }
        }
    }

    /**
     * Inserts an email for a lead
     *
     * @param INT $lead - lead ID
     * @param String $email-
     *
     * @return INT
     */
    public function insertLeadEmail($lead, $email)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $data = array
            (
                'leadId' => $lead,
                'email' => $email
            );

        $this->db->insert('leadEmails', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $lead
     * @param mixed $phones  
     *
     * @return TODO
     */
    public function savePhones ($lead, $phones)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        if (!empty($phones))
        {
            foreach ($phones['number'] as $k => $number)
            {
                $this->insertLeadPhone($lead, $phones['type'][$k], $number);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $lead 
     * @param mixed $type    
     * @param mixed $number  
     *
     * @return TODO
     */
    public function insertLeadPhone ($lead, $type, $number)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $data = array
            (
                'leadId' => $lead,
                'type' => $type,
                'phoneNumber' => $number
            );

        $this->db->insert('leadPhone', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $addresses 
     *
     * @return TODO
     */
    public function saveAddress($lead, $addresses)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        if (!empty($addresses))
        {
            foreach ($addresses['description'] as $k => $void)
            {
                $this->insertLeadAddress($lead, $k, $addresses);
            }
        }

        return true;
    }


    /**
     * TODO: short description.
     *
     *
     * @return TODO
     */
    public function insertLeadAddress ($lead, $k, $p)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        // will attempt to geolocate the address
        $p['lat'] = $p['lng'] = 0;

        if (!empty($p['addressLine1'][$k]))
        {
            $address = $p['addressLine1'][$k];

            if (!empty($p['addressLine2'][$k])) $address .= ' ' . $p['addressLine2'][$k];
            if (!empty($p['addressLine3'][$k])) $address .= ' ' . $p['addressLine3'][$k];

            $address .= " {$p['city'][$k]}, {$p['state'][$k]} {$p['postalCode'][$k]}";

            // will geolcode address
            $ll = $this->functions->getAddressLatLng($address);

            $p['lat'] = $ll->lat;
            $p['lng'] = $ll->lng;

        }

        $data = array
            (
                'leadId' => $lead,
                'datestamp' => DATESTAMP,
                'description' => $p['description'][$k],
                'notes' => $p['notes'][$k],
                'address' => $p['addressLine1'][$k],
                'address2' => $p['addressLine2'][$k],
                'address3' => $p['addressLine3'][$k],
                'city' => $p['city'][$k],
                'state' => $p['state'][$k],
                'postalCode' => $p['postalCode'][$k],
                'lat' => $p['lat'],
                'lng' => $p['lng']
            );

        $this->db->insert('leadAddress', $data);

        return $this->db->insert_id();
    }

    /**
     * used to clear lead phones
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function clearPhones ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $this->db->where('leadId', $lead);
        $this->db->delete('leadPhone');
    }

    public function clearEmails ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $this->db->where('leadId', $lead);
        $this->db->delete('leadEmails');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getInfo ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "leadInfo{$lead}-" . $this->session->userdata('company');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("companyName, contactName, companyName, leadDate, amount, assignedFrom, assignedTo, contactType, status, active, bogus, details");
            $this->db->from('leads');
            $this->db->where('id', $lead);
            $this->db->where('company', $this->session->userdata('company'));

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getMainPhone ($lead, $type = 1)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "lead-Main-Phone-{$lead}-{$type}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('phoneNumber');
            $this->db->from('leadPhone');
            $this->db->where('leadId', $lead);
            $this->db->where('type', $type);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->phoneNumber;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }


        return $data;
    }


    /**
     * Gets the main email address for a lead
     *
     * @param mixed $lead
     *
     * @return String - Email
     */
    public function getMainEmail ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "lead-Main-Email-{$lead}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("email");
            $this->db->from('leadEmails');
            $this->db->where('leadId', $lead);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->email;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * gets main address for a lead
     *
     * @param int $lead
     *
     * @return object
     */
    public function getMainAddress ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "lead-Main-Address-{$lead}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("description, address, address2, address3, city, state, postalCode");
            $this->db->from('leadAddress');
            $this->db->where('leadId', $lead);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * gets all emails for a lead
     *
     * @param mixed $lead 
     *
     * @return array->object
     */
    public function getEmails ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "leadEmails{$lead}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('email');
            $this->db->from('leadEmails');
            $this->db->where('leadId', $lead);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * gets phone list for leads
     *
     * @param int $lead
     *
     * @return array->object
     */
    public function getPhones ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "leadPhones{$lead}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('type, phoneNumber');
            $this->db->from('leadPhone');
            $this->db->where('leadId', $lead);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;

    }

    /**
     * gets addresses for a lead
     *
     * @param int $lead
     *
     * @return array->object
     */
    public function getAddresses ($lead)
    {
        if (empty($lead)) throw new Exception("Lead ID is empty!");

        $mtag = "leadAddr-{$lead}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('description, notes, address, address2, address3, city, state, postalCode, lat, lng');
            $this->db->from('leadAddress');
            $this->db->where('leadId', $lead);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

}
