<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class register_model extends CI_Model
{


    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $user        
     * @param mixed $company     
     * @param mixed $accountType 
     *
     * @return TODO
     */
    public function insertAccountType ($user, $company, $accountType)
    {
        $user = intval($user);
        $company = intval($company);
        $accountType = intval($accountType);

        if (empty($user)) throw new Exception("User ID is empty");
        if (empty($company)) throw new Exception("Company ID is empty");
        if (empty($accountType)) throw new Exception("Account Type is empty!`");

        $data = array
            (
                'userid' => $user,
                'company' => $company,
                'accountType' => $accountType
            );

        $this->db->insert('userAccountTypes', $data);


        return $this->db->insert_id();
    }
}
