<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class docs_model extends CI_Model
{

    public $folders;
    public $folder;
    public $breadCrumbs;
    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        $this->breadCrumbs = array();

        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getFolderContent ($folder = 0)
    {
        $folders = $this->modules->getViewableFolderIDs('docs');

        $this->db->select("id, name, ('1') AS type, active");
        $this->db->from('documentFolders');
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('parentFolder', $folder);
        $this->db->where('deleted', 0);
        $this->db->where_in('id', $folders);

        $query = $this->db->get();

        $dirResults = $query->result();

        // now gets documents


        $this->db->select("documents.id, documents.title AS name, ('2') as type, documents.active");
        $this->db->from('docFolderAssign');
        $this->db->join('documents', 'docFolderAssign.docId = documents.id', 'left');
        $this->db->where('docFolderAssign.folderId', $folder);
        $this->db->where('documents.deleted', '0');
        $this->db->where('documents.company', $this->session->userdata('company'));

        $query = $this->db->get();

        $docResults = $query->result();

        $results = array_merge($dirResults, $docResults);

        /*
        $sql= "SELECT id, name, 1 [type], 0 [policyType], 1 active, null fileName, null video, 0 [order]
            FROM documentFolders
            WHERE parentFolder {$folder}
            UNION ALL
            SELECT p.id, p.title, 2, p.type, p.active, p.fileName, p.video, dfa.docOrder
            FROM docFolderAssign dfa
            LEFT JOIN policies p ON dfa.docId = p.id
            WHERE folderId {$folder}
            AND p.deleted = 0
            {$sqlAdd}
            ORDER BY [type], [order], name";

        $query = $this->db->query($sql);
        */


        return $results;
    }

    // function is deprecated - use folder model
    public function createFolder ($p, $userid, $company)
    {
        if (empty($p['folderName'])) throw new Exception("folder name is empty!");
        if (empty($userid)) throw new Exception("userid is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'name' => $p['folderName'],
                'company' => $company,
                'createdBy' => $userid
            );

        if (!empty($p['parentFolder'])) $data['parentFolder'] = $p['parentFolder'];

        $this->db->insert('documentFolders', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function saveDocument ($p)
    {
        if (empty($p['id']))
        {
            $p['id'] = $this->insertDocument($p);

            // assigns document to current folder
            $this->assignDocToFolder($p['id'], $p['folder']);

            $historyVersion = 1;

        }
        else
        {
            $this->updateDocument($p);

            // clears all old positions
            // $this->clearPolicyPositions($p['id']);

            // $this->clearPolicyCountries($p['id']);

            // $this->clearStoreTypes($p['id']);

            $historyVersion = (int) $p['version'] + 1;

        }

        // inserts history
        $this->insertDocument($p, $historyVersion, $p['id']);

        $this->clearPositions($p['id']);
        $this->clearLocations($p['id']);
        $this->clearCountry($p['id']);


        // inserts new positions to sign
        $this->insertPositions($p['id'], $p['position']);

        if ($p['sendToType'] == 1) $this->insertLocations($p['id'], $p['location']);
        if ($p['sendToType'] == 2) $this->insertCountry($p['id'], $p['location']);

    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertDocument ($p, $version = 1, $history = null)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $p['company'],
                'userid' => $p['userid'],
                'title' => $p['title'],
                'body' => $p['body'],
                'type' => $p['type'],
                'ack' => $p['ack'],
                'version' => $version,
                'file' => $p['file'],
                'active' => $p['active'],
                'trainingDocument' => $p['newhire'],
                'accessType' => $p['sendToType']
            );


        if (empty($p['startDate'])) $this->db->set('startDate', null);
        else $data['startDate'] = $p['startDate'];

        if (empty($p['endDate'])) $this->db->set('endDate', null);
        else $data['endDate'] = $p['endDate'];


        if (empty($history))
        {
            $this->db->insert('documents', $data);
        }
        else
        {
            $data['docId'] = $history;

            $this->db->insert('documentHistory', $data);
        }

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @return TOO
     */
    public function updateDocument ($p)
    {

        if (empty($p['id'])) throw new Exception('document ID is empty!');

        $version = $p['version'] + 1;

        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $p['company'],
                'userid' => $p['userid'],
                'title' => $p['title'],
                'body' => $p['body'],
                'type' => $p['type'],
                'ack' => $p['ack'],
                'version' => $version,
                'file' => $p['file'],
                'active' => $p['active'],
                'trainingDocument' => $p['newhire'],
                'accessType' => $p['sendToType']
            );


        if (empty($p['startDate'])) $this->db->set('startDate', null);
        else $data['startDate'] = $p['startDate'];

        if (empty($p['endDate'])) $this->db->set('endDate', null);
        else $data['endDate'] = $p['endDate'];

        $this->db->where('id', $p['id']);
        $this->db->update('documents', $data);

        return true;
    }

    /**
     * Clears previous documents folder assign
     *
     * @param int $id - document id
     *
     * @return boolean
     */
    public function clearDocFolderAssign ($id)
    {
        if (empty($id)) throw new Exception("id is empty!");

        $this->db->where('docId', $id);
        $this->db->delete('docFolderAssign');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param int $id - document id
     * @param int $folder - folder id
     *
     * @return boolean
     */
    public function assignDocToFolder ($id, $folder)
    {
        $this->clearDocFolderAssign($id);

        $folder = (empty($folder)) ? '0' : $folder;

        if (empty($id)) throw new Exception("document id is empty!");

        $data = array
            (
                'docId' => $id,
                'folderId' => $folder,
                'docOrder' => 0
            );

        $this->db->insert('docFolderAssign', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getDocInfo ($id, $version = 0)
    {
        if (empty($id)) throw new Exception('Document ID is empty!');

        $mtag = "document-{$id}-{$version}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $table = (empty($version)) ? 'documents' : 'documentHistory';

            $idCol = (empty($version)) ? 'id' : 'docId';

            $this->db->from($table);
            $this->db->where($idCol, $id);

            if (!empty($version)) $this->db->where('version', $version);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

    return $data;
    }

    /**
     * TODO: short description.
     *
     * @param int $docId
     *
     * @return TODO
     */
    public function getPreviousVersions ($docId)
    {
        if (empty($docId)) throw new Exception('Document ID is empty!');

        $mtag = "docPrevVer{$docId}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {

            $this->db->select("id, datestamp, userid, title, version");
            $this->db->from('documentHistory');
            $this->db->where('docId', $docId);
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->order_by('datestamp', 'desc');


            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

    return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function setDocumentDeleted ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Document ID is empty!");

        $data = array
            (
                'deleted' => 1,
                'deletedBy' => $this->session->userdata('userid'),
                'deleteDate' => DATESTAMP
            );

        $this->db->where('id', $id);
        $this->db->update('documents', $data);

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function clearPositions ($id)
    {
        if (empty($id)) throw new Exception("Doc id is empty!");

        $this->db->where('docId', $id);
        $this->db->delete('docPositions');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function clearLocations ($id)
    {
        if (empty($id)) throw new Exception("Doc id is empty!");

        $this->db->where('docId', $id);
        $this->db->delete('docLocations');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function clearCountry ($id)
    {
        if (empty($id)) throw new Exception("Doc id is empty!");

        $this->db->where('docId', $id);
        $this->db->delete('docCountry');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $positions 
     *
     * @return TODO
     */
    public function insertPositions ($id, $positions)
    {
        if (empty($id)) throw new Exception("Doc ID is empty!");

        if (!empty($positions))
        {
            foreach ($positions as $k => $positionId)
            {

                $data = array
                    (
                        'docId' => $id,
                        'position' => $positionId
                    );

                $this->db->insert('docPositions', $data);
            }
        }

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $locations
     *
     * @return TODO
     */
    public function insertLocations ($id, $locations)
    {
        if (empty($id)) throw new Exception("Doc ID is empty!");

        if (!empty($locations))
        {
            foreach ($locations as $k => $locationId)
            {

                $data = array
                    (
                        'docId' => $id,
                        'location' => $locationId
                    );

                $this->db->insert('docLocations', $data);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $countries
     *
     * @return TODO
     */
    public function insertCountry ($id, $countries)
    {
        if (empty($id)) throw new Exception("Doc ID is empty!");

        if (!empty($countries))
        {
            foreach ($countries as $k => $countryId)
            {

                $data = array
                    (
                        'docId' => $id,
                        'country' => $countryId
                    );

                $this->db->insert('docCountry', $data);
            }
        }

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id       
     * @param mixed $position 
     *
     * @return TODO
     */
    public function checkPositionAssigned ($id, $position)
    {
        if (empty($id)) throw new Exception('Document ID is empty!');
        if (empty($position)) throw new Exception("Position is empty!");

        $mtag = "docPos-{$id}-{$position}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->from('docPositions');
            $this->db->where('docId', $id);
            $this->db->where('position', $position);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

    return false;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id       
     * @param mixed $position 
     *
     * @return TODO
     */
    public function checkLocationAssigned ($id, $location)
    {
        if (empty($id)) throw new Exception('Document ID is empty!');
        if (empty($location)) throw new Exception("Location is empty!");


        $mtag = "docLoc-{$id}-{$location}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->from('docLocations');
            $this->db->where('docId', $id);
            $this->db->where('location', $location);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

    return false;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id       
     * @param mixed $country
     *
     * @return TODO
     */
    public function checkCountryAssigned ($id, $country)
    {
        if (empty($id)) throw new Exception('Document ID is empty!');
        if (empty($country)) throw new Exception("Country is empty!");


        $mtag = "docCntry-{$id}-{$location}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->from('docCountry');
            $this->db->where('docId', $id);
            $this->db->where('country', $location);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

    return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    /*
    public function getAssignedDocs ($user = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');

        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        // first gets a list of all document ID's

        $docIDs = $this->getDocumentIDList(true);

        // no active docments to sign
        if (empty($docIDs)) return false;

        $return = array();

        foreach ($docIDs as $doc)
        {


            // adds ID to return array as a required document
            $return[] = $doc;

        }

        return $return;
    }
    */

    /**
     * Gets the ID's of all active and undeleted documents for a paticular company
     *
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return array
     */
    public function getDocumentIDList ($requireAck = false, $company = 0)
    {
        if (empty($companyr)) $company = $this->session->userdata('company');

        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "docIDList-" . ((int) $requireAck) . "-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id');
            $this->db->from('documents');
            $this->db->where('active', 1);
            $this->db->where('deleted', 0);

            if ($requireAck == true)
            {
                // gets documents that requires view or signature
                $this->db->where_in('ack', array(1,2));
            }

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return int - row ID
     */
    public function saveDocAck ($p, $user = 0, $company = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($user)) throw new Exception("User ID is empty!");

        if (empty($company)) $company = $this->session->userdata('company');
        if (empty($company)) throw new Exception("Company ID is empty!");



        if (empty($p['id'])) throw new Exception("Document ID is empty!");

        $data = array
            (
                'userid' => $user,
                'company' => $company,
                'docId' => $p['id'],
                'version' => $p['version'],
                'ackType' => $p['ack'],
                'datestamp' => DATESTAMP
            );

        $this->db->insert('docSignatures', $data);

        return $this->db->insert_id();
    }

    /**
     * Gets the latest signature data of a particular document
     *
     * @return TODO
     */
    public function getDocAckInfo ($user, $doc, $version = 0, $includeResign = false)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($doc)) throw new Exception("Document ID is empty!");
        // if (empty($version)) throw new Exception("Version number is empty!");

        $mtag = "docAckInfo-{$user}-{$doc}-{$version}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, ackType, datestamp, version');
            $this->db->from('docSignatures');
            $this->db->where('userid', $user);
            $this->db->where('docId', $doc);

            if (!empty($version)) $this->db->where('version', $version);

            if ($includeResign == false) $this->db->where('resign', 0);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        // returns false if no signature data
        if (empty($data)) return false;

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $user    
     * @param mixed $doc     
     * @param mixed $version 
     *
     * @return TODO
     */
    public function insertDocViewHistory ($user, $doc, $version)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($doc)) throw new Exception("Document ID is empty!");
        if (empty($version)) throw new Exception("Version number is empty!");

        $data = array
            (
                'userid' => $user,
                'company' => $this->session->userdata('company'),
                'docId' => $doc,
                'version' => $version,
                'datestamp' => DATESTAMP
            );

        $this->db->insert('docViewHistory', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user
     * @param mixed $doc 
     *
     * @return TODO
     */
    public function clearUsersSignatures($user, $doc)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($doc)) throw new Exception("Document ID is empty!");

            $data = array
                (
                    'resign' => 1,
                    'resignBy' => $this->session->userdata('userid'),
                    'resignDate' => DATESTAMP
                );

        $this->db->where('resign', 0);
        $this->db->where('docId', $doc);
        $this->db->where('userid', $user);
        $this->db->update('docSignatures', $data);

        return true;
    }
}
