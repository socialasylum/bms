<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class formbuilder_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function saveForm ($p)
    {
        if (empty($p['id']))
        {
            $id = $this->insertForm($p);

            // assigns document to current folder
            $this->assignFormToFolder($id, $p['folder']);

        }
        else
        {
            $id = $this->updateForm($p);
        }

        // clears previous approval levels
        $this->clearPreviousApprovals($id);

        // will now assign new
        if (!empty($p['position']))
        {
            foreach ($p['position'] as $lvl => $v)
            {
                foreach ($v as $position)
                {
                    $this->insertApproval($id, $lvl, $position);
                }
            }
        }

    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    private function insertForm ($p)
    {
        // $title = $this->getFormTitle($p['render']);

        // cleans up form html;
        $html = $this->stripSortTags($p['render']);

        // $html = $this->stripAllLITags($html);

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'approvals' => $p['approvals'],
                'title' => $p['title'],
                'instructions' => $p['instructions'],
                'briefIns' => $p['briefIns'],
                'formHtml' => $html,
                'active' => $p['active']
            );

        $this->db->insert('forms', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    private function updateForm ($p)
    {
        if (empty($p['id'])) throw new Exception("Form ID is empty!");

        $html = $p['render'];

        // error_log('**** HTML BEFORE STRIP****');
        // error_log($html);

        // cleans up form html;
        $html = $this->stripSortTags($html);

        // $html = $this->stripAllLITags($html);

        // error_log('**** HTML AFTER STRIP****');
        // error_log($html);

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'approvals' => $p['approvals'],
                'title' => $p['title'],
                'instructions' => $p['instructions'],
                'briefIns' => $p['briefIns'],
                'formHtml' => $html,
                'active' => $p['active']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('forms', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $html 
     *
     * @return TODO
     */
    public function getFormTitle ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("form ID is empty!");

        $mtag = "formTitle-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('title');
            $this->db->from('forms');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->title;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function getFormCompany ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("form ID is empty!");

        $mtag = "formCompany-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('company');
            $this->db->from('forms');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->company;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $formId 
     *
     * @return TODO
     */
    private function clearPreviousApprovals ($formId)
    {
        if (empty($formId)) throw new Exception("form id is empty!");

        $this->db->where('form', $formId);
        $this->db->delete('formApprovals');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $form     
     * @param mixed $lvl      
     * @param mixed $position 
     *
     * @return TODO
     */
    private function insertApproval ($form, $lvl, $position)
    {
        if (empty($form)) throw new Exception("form id is empty!");
        if (empty($lvl)) throw new Exception("form approval lvl is empty!");
        if (empty($position)) throw new Exception("position id is empty!");

        $data = array
            (
                'form' => $form,
                'lvl' => $lvl,
                'position' => $position
            );

        $this->db->insert('formApprovals', $data);

        return $this->db->insert_id();
    }

    public function createFolder ($p, $userid, $company)
    {
        if (empty($p['folderName'])) throw new Exception("folder name is empty!");
        if (empty($userid)) throw new Exception("userid is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'name' => $p['folderName'],
                'company' => $company,
                'createdBy' => $userid
            );

        if (!empty($p['parentFolder'])) $data['parentFolder'] = $p['parentFolder'];

        $this->db->insert('documentFolders', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getFolderContent ($folder = 0)
    {
        $folders = $this->modules->getViewableFolderIDs('formbuilder');

        $this->db->select("id, name, ('1') AS type, active");
        $this->db->from('documentFolders');
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('parentFolder', $folder);
        $this->db->where('deleted', 0);
        $this->db->where_in('id', $folders);

        $query = $this->db->get();

        $dirResults = $query->result();

        // now gets forms


        $this->db->select("forms.id, forms.title AS name, ('2') as type, forms.active");
        $this->db->from('formFolderAssign');
        $this->db->join('forms', 'formFolderAssign.formId = forms.id', 'left');
        $this->db->where('formFolderAssign.folderId', $folder);
        $this->db->where('forms.deleted', '0');
        $this->db->where('forms.company', $this->session->userdata('company'));

        $query = $this->db->get();

        $docResults = $query->result();

        $results = array_merge($dirResults, $docResults);

        return $results;
    }


    /**
     * Clears previous documents folder assign
     *
     * @param int $id - document id
     *
     * @return boolean
     */
    public function clearFormFolderAssign ($id)
    {
        if (empty($id)) throw new Exception("id is empty!");

        $this->db->where('formId', $id);
        $this->db->delete('formFolderAssign');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param int $id - form id
     * @param int $folder - folder id
     *
     * @return boolean
     */
    public function assignFormToFolder ($id, $folder)
    {
        $this->clearFormFolderAssign($id);

        $folder = (empty($folder)) ? '0' : $folder;

        if (empty($id)) throw new Exception("form id is empty!");

        $data = array
            (
                'formId' => $id,
                'folderId' => $folder,
                'formOrder' => 0
            );

        $this->db->insert('formFolderAssign', $data);

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getFormInfo ($id)
    {

        if (empty($id)) throw new Exception("form ID is empty!");

        $mtag = "form-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('forms');
            $this->db->where('id', $id);
            $this->db->where('company', $this->session->userdata('company'));

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $html 
     *
     * @return TODO
     */
    public function buildForm ($html)
    {

        $html = str_replace("<form ", "<form name='customForm' id='customForm' ", $html);


    return $html;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p        
     * @param mixed $formHtml 
     *
     * @return TODO
     */
    public function savePostdata ($p, $title, $formHtml)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'form' => $p['id'],
                'formTitle' => $title,
                'formHtml' => $formHtml,
                'postdata' => json_encode($p)
            );

        $this->db->insert('formPostData', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getSubmittedForms ($company = 0)
    {
        if (empty($company)) $company = $this->session->userdata('company');

        $mtag = "submittedForms-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, datestamp, userid, form, formTitle');
            $this->db->from('formPostData');
            $this->db->where('company', $company);
            $this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * gets form post data full row
     *
     * @param mixed $id 
     *
     * @return array
     */
    public function getFormPostDataInfo ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("FormPostData ID is empty!");

        $mtag = "submittedFormData-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('formPostData');
            $this->db->where('company', $this->session->userdata('company')); // ensures only can pull their company forms
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * Strips the UL tags from the HTML form
     *
     * @param mixed $html 
     *
     * @return String
     */
    public function stripSortTags ($html)
    {

        $html = preg_replace('/^<ul(.*?)>/i', '', $html);
        // $html = preg_replace('/^<li(.*?)>/i', '', $html);
        
        $html = str_ireplace("<li>", '', $html);
        $html = str_ireplace("</li>", '', $html);
        $html = str_ireplace("</ul>", '', $html);

        return $html;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $html 
     *
     * @return TODO
     */
    public function stripAllLITags($html)
    {

        // $html = str_replace("<ul>", '', $html);
        // $html = str_replace("</ul>", '', $html);

        return $html;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $form 
     *
     * @return TODO
     */
    public function getApprovalLvls ($form)
    {
        $form = intval($form);
    
        if (empty($form)) throw new Exception("Form ID is empty!");

        $mtag = "formAppLvls-{$form}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("lvl, position");
            $this->db->from('formApprovals');
            $this->db->where('form', $form);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }


        return $data;
    }

    /**
     * Sets a form to deleted
     *
     * @param mixed $form
     *
     * @return TODO
     */
    public function setFormDeleted ($form)
    {
        $form = intval($form);

        if (empty($form)) throw new Exception("Form ID is empty!");

        $data = array
            (
                'deleted' => '1',
                'deletingUser' => $this->session->userdata('userid'),
                'deleteDate' => DATESTAMP
            );

        $this->db->where('id', $form);
        $this->db->update('forms', $data);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getDatasets ()
    {
        $company = $this->session->userdata('company');

        $mtag = "reportDatasets-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, name");
            $this->db->from('reportDatasets');
            $this->db->where('company', $company);
            $this->db->where('active', 1);
            $this->db->where('deleted', 0);
            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function active ($form)
    {
        $form = intval($form);

        if (empty($form)) throw new Exception("Form ID is empty!");

        $mtag = "formActive-{$form}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('active');
            $this->db->from('forms');
            $this->db->where('id', $form);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->active;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data == 1) return true;
 
        return false;
    }

    /**
     * gets the signature data for a paticular level
     *
     * @param mixed $id  
     * @param mixed $lvl 
     *
     * @return TODO
     */
    public function getSigLevelInfo ($form, $lvl)
    {
        $form = intval($form);
        $lvl = intval($lvl);

        if (empty($form)) throw new Exception("Form ID is empty!");
        if (empty($lvl)) throw new Exception("Signature Lvl is empty!");

        $mtag = "formSigLvl-{$form}-{$lvl}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('formSignatures');
            $this->db->where('form', $form);
            $this->db->where('lvl', $lvl);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id       
     * @param mixed $lvl      
     * @param mixed $approved Optional, defaults to false. 
     * @param mixed $final    Optional, defaults to false. 
     *
     * @return TODO
     */
    public function insertFormSignature ($id, $lvl, $approved = false, $final = false)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'form' => $id,
                'lvl' => $lvl,
                'userid' => $this->session->userdata('userid'),
                'approved' => (int) $approved,
                'final' => (int) $final
            );

        $this->db->insert('formSignatures', $data);

        return $this->db->insert_id();
    }

    /**
     * Checks if the user has the permission to sign the lvl
     *
     * @param mixed $id 
     * @param mixed $i  
     *
     * @return TODO
     */
    public function canSignlvl($id, $lvl, $user = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');

        if (empty($user)) throw new Exception("User ID is empty!");

        $mtag = "formCanSignLvl-{$id}-{$lvl}-{$user}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $position = $this->users->getPosition($user);

            if (empty($position)) throw new Exception("Position ID is empty for user {$user}!");

            $this->db->select('id');
            $this->db->from('formApprovals');
            $this->db->where('form', $id);
            $this->db->where('lvl', $lvl);
            $this->db->where('position', $position);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

        // if admin or company admin, will allow them to sign
        $companyAdmin = $this->users->isCompanyAdmin($this->session->userdata('userid'), $this->session->userdata('company'));
        if ($companyAdmin) return true;

        $admin = $this->users->isAdmin($this->session->userdata('userid'));
        if ($admin) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function hasFinalSignature ($id)
    {
        if (empty($id)) throw new Exception("ID is empty!");

        $mtag = "formFinalSig-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            // $this->db->select('lvl');
            $this->db->from('formSignatures');
            $this->db->where('form', $id);
            $this->db->where('approved', 1);
            $this->db->where('final', 1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (!empty($data)) return $data;

        return false;
    }
}
