<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class companysettings_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


	/**
	* gets list of companies
	*/
	public function getCompanies ()
	{
        $mtag = "getCompanies-";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
        	$this->db->select('id, companyName');
            $this->db->from('companies');
            $this->db->where('deleted', 0);
			$this->db->order_by('companyName');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;	
	}

    /**
     * Gets a companies row from DB
     */
    public function getInfo ($company)
    {
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "compGetInfo-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('companies');
            $this->db->where('id', $company);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateCompany ($p)
    {
        if (empty($p['id'])) throw new Exception("company ID is empty");


        $data = array
            (
                'companyName' => $p['companyName'],
                'addressLine1' => $p['addressLine1'],
                'addressLine2' => $p['addressLine2'],
                'addressLine3' => $p['addressLine3'],
                'city' => $p['city'],
                'state' => $p['state'],
                'postalCode' => $p['postalCode'],
                'phone' => $p['phone'],
                'email' => $p['email'],
                'allowReg' => $p['allowReg'],
                'becomeMemberTxt' => $p['becomeMemberTxt'],
                'allowFBreg' => $p['allowFBreg'],
                'landingUrl' => $p['landingUrl'],
                'indexUrl' => $p['indexUrl'],
                'fbSync' => $p['fbSync'],
                'ytUpload' => $p['ytUpload'],
                'hideFooter' => $p['hideFooter']
            );

        if (!empty($p['logo'])) $data['logo'] = $p['logo'];
        
        if (!empty($p['exempt'])) $data['billingExempt'] = $p['exempt'];
        if (!empty($p['users'])) $data['users'] = $p['users'];

        if (isset($p['billingFirstName'])) $data['billingFirstName'] = $p['billingFirstName'];
        if (isset($p['billingLastName'])) $data['billingLastName'] = $p['billingLastName'];


        $this->db->where('id', $p['id']);
        $this->db->update('companies', $data);


    return true;
    }
    
    /**
    * updates the company logo
    */
    public function updateLogo ($company, $logo)
    {
    	$company = intval($company);
    	
        if (empty($company)) throw new Exception("company ID is empty");
    
    	$data['logo'] = $logo;
    	
        $this->db->where('id', $company);
        $this->db->update('companies', $data);
        
        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function getOnfileCards ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "companyCards-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, cardType, lastFour, token");
            $this->db->from('companyBilling');
            $this->db->where('company', $company);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $file 
     *
     * @return TODO
     */
    public function resizeLogo($file, $company, $path = null)
    {
        if (empty($company)) throw new Exception("Company ID is empty");

        //$logo = $this->companies->getTableValue('logo', $company);

        $path = 'public' . DS . 'uploads' . DS . 'logos' . DS . $company . DS;

        $config['image_library'] = 'gd2';
        $config['source_image']= './' . $path . $file;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 100;
        $config['height']= 50;

        $this->load->library('image_lib', $config);

        $this->image_lib->resize();
    }


    /**
     * Clears all modules assigned to a company
     *
     * @param mixed $company 
     *
     * @return true
     */
    public function clearCompanyModules ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $this->db->where('company', $company);
        $this->db->delete('companyModules');


        return true;
    }

    /**
     * Updates the company subscirption price in companies table
     *
     * @param mixed $company 
     * @param mixed $price   
     *
     * @return true
     */
    public function setsubscriptionPrice ($company, $price)
    {
        if (empty($company)) throw new Exception("Company ID is empty");

        $data = array('subscriptionPrice' => $price);

        $this->db->where('id', $company);
        $this->db->update('companies', $data);

        return true;
    }
    


	public function addDomain ($userid, $company, $domain)
	{
        $company = intval($company);
        $userid = intval($userid);
        
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($userid)) throw new Exception("User ID is empty!");
        if (empty($domain)) throw new Exception("Domain is empty!");
        
        $data = array
        (
        	'company' => $company,
        	'userid' => $userid,
        	'datestamp' => DATESTAMP,
        	'domain' => $domain
        );
        
        $this->db->insert('companyDomains', $data);
        
        return $this->db->insert_id();
	}
	
	public function removeDomain ($id, $company)
	{
		$id = intval($id);
        $company = intval($company);

		if (empty($id)) throw new Exception("Company Domain ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        
        $this->db->where('company', $company);
        $this->db->where('id', $id);
        $this->db->delete('companyDomains');
        
        return true;
	}
}
