<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class layout_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

	/**
	* removes all layout values for a company
	*/
	public function clearCompValues ($company)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
		
		$this->db->where('company', $company);
		$this->db->delete('layout');
		
		return true;
	}
	
	/**
	* clears a single value
	*/
	public function clearValue ($company, $k)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
		if (empty($k)) throw new Exception("Key is empty!");
		
		$this->db->where('key', $k);		
		$this->db->where('company', $company);
		$this->db->delete('layout');
		
		return true;
	}
	
	public function insertValue ($company, $k, $v)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
		
		if (empty($k)) throw new Exception("Key is empty!");
		
		$data = array
		(
			'company' => $company,
			'datestamp' => DATESTAMP,
			'key' => $k,
			'val' => $v
		);
		
		$this->db->insert('layout', $data);
		
		return $this->db->insert_id();
	}
	
	
	public function saveValues ($company, $p)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
		
		if (!empty($p))
		{
			foreach ($p as $k => $v)
			{
				$this->clearValue($company, $k);
				
				$this->insertValue($company, $k, $v);
			}
		}
		
		return true;
	}
	
	public function deleteImg ($file)
	{
		$del = unlink($_SERVER['DOCUMENT_ROOT'] . $file);
		
		if ($del === false) throw new Exception("Unable to delete file ({$file})!");
		
		return true;
	}
	
	/**
	* builds layout css file
	*/
	public function compile ($company, $preview = false)
	{

		$file = ($preview) ? 'preview' : 'layout';
	
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
		
		$style['values'] = $this->style->getAllValues($company);
		$style['company'] = $company;
		
		$less = $this->load->view('layout/less', $style, true);
		
		$lessFile = $_SERVER['DOCUMENT_ROOT'] . $this->path . $file . '.less';
		@unlink($lessFile); // deletes less file
		
		// writes LESS content to files
		$writeLess = file_put_contents($lessFile, $less);
		
		if ($writeLess === false) throw new Exception("Unable to write LESS Styles to {$lessFile}");
		
		// paths for css Files
		$cssFile = $_SERVER['DOCUMENT_ROOT'] . $this->path . $file . '.css';
		@unlink($cssFile); // deletes css file
		
		//PHPFunctions::create
		
		// compiles content
		$this->less->checkedCompile($lessFile, $cssFile);
				
		return true;
	}
	
	/*
	public function preview ($company)
	{
		if (empty($company)) throw new Exception("Company ID is empty!");
		
		$style['values'] = $this->style->getAllValues($company);
		//$style['preview'] = true;
		
		$preview = $this->load->view('layout/less', $style, true);
		
		$previewFile = $_SERVER['DOCUMENT_ROOT'] . $this->path . 'preview.less';
		@unlink($previewFile);
		
		// writes LESS content to files
		$writePreview = file_put_contents($previewFile, $preview);
		
		$cssPreviewFile = $_SERVER['DOCUMENT_ROOT'] . $this->path . 'preview.css';
		@unlink($cssPreviewFile); // deletes preview css file
		
		// compiles content
		$this->less->checkedCompile($previewFile, $cssPreviewFile);
		
		return true;
	}
	*/
}