<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class department_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getDepartments ($id = 0, $countOnly = false)
    {

        $mtag = "departments-{$id}-" . $this->session->userdata('company') . '-' . (int) $countOnly;

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, name, active, default');
            $this->db->from('departments');
            $this->db->where('company', $this->session->userdata('company'));

            if (!empty($id)) $this->db->where('id', $id);


            if ($countOnly == true)
            {
                $data = $this->db->count_all_results();
            }
            else
            {
                $query = $this->db->get();

                $results = $query->result();

                if (!empty($id)) $data = $results[0];
                else $data = $results;

            }

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;

    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function createDepartment ($p, $userid = 0, $company = 0)
    {
        if (empty($userid)) $userid = $this->session->userdata('userid');
        if (empty($company)) $company = $this->session->userdata('company');


        if (empty($userid)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'createdBy' => $userid,
                'company' => $company,
                'name' => $p['name'],
                'active' => $p['active'],
                'default' => $p['default']
            );

        $this->db->insert('departments', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateDepartment ($p)
    {
        if (empty($p['id'])) throw new Exception("Department ID is empty!");

        $data = array
            (
                'name' => $p['name'],
                'active' => $p['active'],
                'default' => $p['default']
            );

        $this->db->where('company', $this->session->userdata('company')); /// ensures they can only update their company depatments
        $this->db->where('id', $p['id']);
        $this->db->update('departments', $data);
        
        return true;
    }

    public function clearDefaultDepartment ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array('default' => 0);

        $this->db->where('company', $company);
        $this->db->update('departments', $data);

        return true;
    }
}
