<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clockinout_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /**
     * Add a timepunch 
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return int
     */
    public function addTimePunch($user = 0, $company = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($company)) $company = $this->session->userdata('company');

        $user = intval($user);
        $company = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty");

        $data = array
            (
                'userid' => $user,
                'company' => $company,
                'timepunch' => DATESTAMP
            );

        $this->db->insert('userTimePunch', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    Optional, defaults to 0. 
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getPunches ($user = 0, $company = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($company)) $company = $this->session->userdata('company');

        $user = intval($user);
        $company = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty");

        $mtag = "punches-{$user}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('timepunch');
            $this->db->from('userTimePunch');
            $this->db->where('userid', $user);
            $this->db->where('company', $company);
            $this->db->order_by('timepunch', 'ASC');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
}
