<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class menu_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertMenuItem ($p)
    {

        if (empty($p['id'])) throw new Exception('company ID is empty!');

        $data = array
        (
            'datestamp' => DATESTAMP,
            'createdBy' => $this->session->userdata('userid'),
            'company' => $p['id'],
            'active' => '1'
        );


        if (!empty($p['module']))
        {
            $data['module'] = $p['module'];
            $data['header'] = 0;
        }
        else
        {
            $data['name'] = $p['name'];
            $data['header'] = $p['header'];

            if (!empty($p['url'])) $data['url'] = $p['url'];
            if (!empty($p['target'])) $data['target'] = $p['target'];
        }

        // if item being inserted is not a header, it requires a parentMenu - if not, menu item will be orphaned
        if ($p['header'] == 0 && empty($p['parentMenu'])) throw new Exception('parentMenu is empty!');

        if (!empty($p['parentMenu'])) $data['parentMenu'] = $p['parentMenu'];

        $this->db->insert('menu', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getMenu ($companyId, $header = false, $parentMenu = null)
    {

        if (empty($companyId)) throw new Exception('companyId is empty!');

        $this->db->from('menu');
        $this->db->where('company', $companyId);
        if ($header == true) $this->db->where('header', '1');
        else $this->db->where('header', '0');

        if (!empty($parentMenu)) $this->db->where('parentMenu', $parentMenu);

        if ($header) $this->db->order_by('menuOrder', 'asc');
		else $this->db->order_by('name', 'asc');
		
        $query = $this->db->get();

        $results = $query->result();

        return $results;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $companyId 
     *
     * @return TODO
     */
    public function getCompanyModules ($companyId)
    {
        $this->db->from('companyModules');
        $this->db->join('modules', 'companyModules.module = modules.id', 'left');
        $this->db->where('company', $companyId);

        $this->db->order_by('name', 'asc');

        $query = $this->db->get();

        $results = $query->result();

        return $results;
    }

    /**
     * Checks if a company has assigned a module to a menu header
     *
     * @param int $company
     * @param int $module 
     *
     * @return boolean - true if assigned
     */
    public function checkModuleAssigned ($company, $module)
    {
        $this->db->from('menu');
        $this->db->where('company', $company);
        $this->db->where('module', $module);

        if ($this->db->count_all_results() > 0) return true;

    return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateHeaderOrder ($p)
    {
        if (!empty($p['order']))
        {
            foreach ($p['order'] as $order => $id)
            {
                $data = array
                    (
                        'menuOrder' => $order
                    );

                $this->db->where('id', $id);
                $this->db->update('menu', $data);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id      
     * @param mixed $company 
     *
     * @return TODO
     */
    public function deleteMenuItem ($id, $company)
    {
        if (empty($id)) throw new Exception('ID is empty!');
        if (empty($company)) throw new Exception('company ID is empty!');

        $this->db->where('id', $id);
        $this->db->where('company', $company);
        $this->db->delete('menu');

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function clearAccess ($menuId)
    {
        if (empty($menuId)) throw new Exception('menuId is empty!');

        $this->db->where('menu', $menuId);
        $this->db->delete('menuAccess');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function saveMenuAccess ($p)
    {
        if (empty($p['id'])) throw new Exception("menu Id is empty!");

        if (!empty($p['position']))
        {
            foreach ($p['position'] as $position)
            {
                $data = array
                    (
                        'menu' => $p['id'],
                        'position' => $position
                    );

                if (empty($position)) throw new Exception("position id is empty!");

                $this->db->insert('menuAccess', $data);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function getModulePermissions ($module)
    {
        if (empty($module)) throw new Exception("module ID is empty!");

        $mtag = "modPerms-{$module}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('modulePermissions');
            $this->db->where('module', $module);
            $this->db->order_by('bit', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * Gets next highest bit permission available to be assigned for a module
     *
     * @param int $module 
     *
     * @return int
     */
    public function getNextModuleBit ($module)
    {
        if (empty($module)) throw new Exception("module ID is empty!");

        $this->db->select("bit");
        $this->db->from('modulePermissions', 1);
        $this->db->where('module', $module);
        $this->db->order_by('bit', 'desc');

        $query = $this->db->get();

        $results = $query->result();

        $bit = $results[0]->bit;

        if (empty($bit))
        {
            return 1;
        }

        return ($bit * 2);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertModulePermission ($p)
    {

        if (empty($p['bit'])) throw new Exception("bit is empty!");
        if (empty($p['module'])) throw new Exception("module ID is empty!");

        if ($this->session->userdata('admin') !== true) throw new Exception("must be a site admin to execute this function!");

        $data = array
        (
            'bit' => $p['bit'],
            'module' => $p['module'],
            'title' => $p['title'],
            'description' => $p['description'],
            'admin' => $p['admin']
        );


        $this->db->insert('modulePermissions', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateModulePermission ($p)
    {
        if (empty($p['bit'])) throw new Exception("bit is empty!");
        if (empty($p['id'])) throw new Exception("module permission ID is empty!");
        if (empty($p['module'])) throw new Exception("module ID is empty!");

        if ($this->session->userdata('admin') !== true) throw new Exception("must be a site admin to execute this function!");

        $data = array
        (
            'bit' => $p['bit'],
            'title' => $p['title'],
            'description' => $p['description'],
            'admin' => $p['admin']
        );

        $this->db->where('id', $p['id']);
        $this->db->where('module', $p['module']);
        $this->db->update('modulePermissions', $data);

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id     
     * @param mixed $module 
     *
     * @return TODO
     */
    public function getModPermInfo($id, $module)
    {
        $this->db->select('bit, title, description, admin');
        $this->db->from('modulePermissions');
        $this->db->where('id', $id);
        $this->db->where('module', $module);

        $query = $this->db->get();

        $results = $query->result();

        return $results[0];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function saveModulePositionAccess ($p)
    {
        $this->clearPositionModuleAccess($p['module']);

        if (!empty($p['position']))
        {
            foreach($p['position'] as $position => $b)
            {
                $total = 0;

                if (!empty($position))
                {
                    foreach ($p['position'][$position] as $bit => $v)
                    {
                        $total += $bit;
                    }
                }

                $this->updatePositionModuleAccess($position, $p['module'], $total);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    private function clearPositionModuleAccess ($module)
    {
        if (empty($module)) throw new Exception('module ID is empty!');

        $this->db->where('module', $module);
        $this->db->delete('positionModuleAccess');

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $position 
     * @param mixed $bits     
     *
     * @return TODO
     */
    private function updatePositionModuleAccess ($position, $module, $bits)
    {

        if (empty($position)) throw new Exception("position ID is empty!");
        if (empty($module)) throw new Exception("module ID is empty!");

        $data = array
        (
            'position' => $position,
            'module' => $module,
            'permissions' => $bits
        );


        $this->db->insert('positionModuleAccess', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function getModulePositionAccess ($module)
    {
        $this->db->select('position, permissions');
        $this->db->from('positionModuleAccess');
        $this->db->where('module', $module);

        $query = $this->db->get();

        return $query->result();
    }

    /**
     * Deletes menu items based by its parent ID
     * used for deleting a menu header
     *
     * @param mixed $menuParentID 
     *
     * @return TODO
     */
    public function deleteMenuItemsByParentID ($menuParentID)
    {
        $menuParentID = intval($menuParentID);

        if (empty($menuParentID)) throw new Exception("Menu Parent ID is empty!");

        $this->db->where('company', $this->session->userdata('company')); // ensures only this companies rows are removed
        $this->db->where('parentMenu', $menuParentID);
        $this->db->delete('menu');

        return true;
    }
}
