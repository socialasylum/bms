<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class announce_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getAnnouncements ($user = 0)
    {

        $user = intval($user);

        $mtag = "announcements-" . $this->session->userdata('company');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $companyAdmin = $this->users->isCompanyAdmin($this->session->userdata('userid'), $this->session->userdata('company'));

            if ($companyAdmin == true)
            {
                // needs to check position access
                $posAnn = $this->getAnnouncementsByPosition($this->session->userdata('position'));
            }


            $this->db->select('id, datestamp, title, userid, startDate, endDate');
            $this->db->from('announcements');
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->where('deleted', 0);


            if ($companyAdmin == true)
            {
                // they are a company admin; can view all announcements
            }
            else
            {
                $this->db->where_in('id', $posAnn);
            }

            $query = $this->db->get();

            // echo "QUERY: " . $this->db->last_query();

            $data = $query->result();
    
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $announcement 
     *
     * @return TODO
     */
    public function getAnnouncementInfo ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("ID is empty!");

        $mtag = "annInfo{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('announcements');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertAnnouncement ($p)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'title' => $p['title'],
                'body' => $p['announcement'],
                'excerpt' => $p['excerpt'],
                'startDate' => $p['startDate'],
                'endDate' => $p['endDate'],
                'sendToType' => $p['sendToType']
            );

        $this->db->insert('announcements', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateAnnouncement ($p)
    {
        if (empty($p['id'])) throw new Exception("Announcement ID is empty!");

        $data = array
            (
                'title' => $p['title'],
                'body' => $p['announcement'],
                'excerpt' => $p['excerpt'],
                'startDate' => $p['startDate'],
                'endDate' => $p['endDate'],
                'sendToType' => $p['sendToType']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('announcements', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $positions 
     *
     * @return TODO
     */
    public function savePositionAccess ($id, $positions)
    {
        if (empty($id)) throw new Exception("Announcement ID is empty!");

        if (!empty($positions))
        {
            foreach($positions as $k => $position)
            {
                $this->insertPosition($id, $position);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id       
     * @param mixed $position 
     *
     * @return TODO
     */
    public function insertPosition ($id, $position)
    {
        if (empty($id)) throw new Exception("Announcement ID is empty!");
        if (empty($position)) throw new Exception("Position ID is empty!");


        $data = array
            (
                'announcement' => $id,
                'position' => $position
            );

        $this->db->insert('annPositionAccess', $data);

        return $this->db->insert_id();

    }


    /**
     * TODO: short description.
     *
     * @param mixed $id          
     * @param mixed $territories 
     *
     * @return TODO
     */
    public function saveTerritoryAccess ($id, $territories)
    {
        if (empty($id)) throw new Exception("Announcement ID is empty!");

        if (!empty($territories))
        {
            foreach($territories as $k => $territoryID)
            {
                $this->insertTerritory($id, $territoryID);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $territory 
     *
     * @return TODO
     */
    public function insertTerritory ($id, $territory)
    {
        if (empty($id)) throw new Exception("Announcement ID is empty!");
        if (empty($territory)) throw new Exception("Territory ID is empty!");

        $data = array
            (
                'announcement' => $id,
                'territory' => $territory
            );

        $this->db->insert('annTerritoryAccess', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $locations 
     *
     * @return TODO
     */
    public function saveLocationAccess ($id, $locations)
    {
        if (empty($id)) throw new Exception("Announcement ID is empty!");

        if (!empty($locations))
        {
            foreach($locations as $k => $locationID)
            {
                $this->insertLocation($id, $locationID);
            }
        }

        return true;
    }

    public function insertLocation ($id, $location)
    {
        if (empty($id)) throw new Exception("Announcement ID is empty!");
        if (empty($location)) throw new Exception("Location ID is empty!");

        $data = array
            (
                'announcement' => $id,
                'location' => $location
            );

        $this->db->insert('annLocationAccess', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $announcement 
     *
     * @return TODO
     */
    public function clearPositionAccess ($announcement)
    {
        if (empty($announcement)) throw new Exception("Announcement ID is empty!");

        $this->db->where('announcement', $announcement);
        $this->db->delete('annPositionAccess');

        return true;
    }

    public function clearTerritoryAccess ($announcement)
    {
        if (empty($announcement)) throw new Exception("Announcement ID is empty!");

        $this->db->where('announcement', $announcement);
        $this->db->delete('annTerritoryAccess');

        return true;
    }

    public function clearLocationAccess ($announcement)
    {
        if (empty($announcement)) throw new Exception("Announcement ID is empty!");

        $this->db->where('announcement', $announcement);
        $this->db->delete('annLocationAccess');

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getPositionAccess ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Announcement ID is empty!");

        $mtag = "annPosAccess-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('position');
            $this->db->from('annPositionAccess');
            $this->db->where('announcement', $id);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getLocationAccess ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Announcement ID is empty!");

        $mtag = "annLocAccess-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('location');
            $this->db->from('annLocationAccess');
            $this->db->where('announcement', $id);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets territory ID's that are assigned to an announcement
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getTerritoryAccess ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Announcement ID is empty!");

        $mtag = "annTerAccess-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('territory');
            $this->db->from('annTerritoryAccess');
            $this->db->where('announcement', $id);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * returns array of announcement ID's that a position has access to
     *
     * @param mixed $position 
     *
     * @return array
     */
    public function getAnnouncementsByPosition ($position)
    {
        $position = intval($position);

        if (empty($position)) throw new Exception("Position ID is empty!");

        $mtag = "annIDsByPosition-{$position}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('announcement');
            $this->db->from('annPositionAccess');
            $this->db->where('position', $position);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        $returnArray = array();

        if (!empty($data))
        {
            foreach ($data as $r)
            {
                $returnArray[] = $r->announcement;
            }
        }

        return $returnArray;
    }
}
