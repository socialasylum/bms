<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class schedule_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertSchedule ($p)
    {

        $shiftStart = date("H:i:s", strtotime("{$p['date']} {$p['shiftStart']}"));
        $shiftEnd = date("H:i:s", strtotime("{$p['date']} {$p['shiftEnd']}"));

        $data = array
            (
                'datestamp' => DATESTAMP,
                'submittedBy' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'location' => $p['location'],
                'userid' => $p['user'],
                'workDay' => $p['date'],
                'shiftStart' => $shiftStart,
                'shiftEnd' => $shiftEnd
            );

        $this->db->insert('schedule', $data);

        // error_log($this->db->last_query());

        return $this->db->insert_id();

    }


    /**
     * TODO: short description.
     *
     * @param mixed $date 
     *
     * @return TODO
     */
    public function getDaySchedule ($location, $date)
    {
        if (empty($location)) throw new Exception("Location ID is empty!");
        if (empty($date)) throw new Exception("date is empty!");

        $mtag = "daySchedule-{$location}-{$date}-" . $this->session->userdata('company');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, datestamp, submittedBy, userid, shiftStart, shiftEnd");
            $this->db->from('schedule');
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->where('location', $location);
            $this->db->where('workDay', $date);
            $this->db->order_by('shiftStart', 'ASC');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;

    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function deleteScheduleEntry ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Schedule ID is empty!");

        $this->db->where('id', $id);
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->delete('schedule');

        return true;
    }
}
