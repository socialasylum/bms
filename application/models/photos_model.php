<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class photos_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

	public function getInfo ($photoID)
	{
		$photoID = intval($photoID);
		
    	if (empty($photoID)) throw new Exception("Photo ID is empty!");

    	$mtag = "photoInfo-{$photoID}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('albumPhotos');
            $this->db->where('id', $photoID);

            $query = $this->db->get();

            $results = $query->result();

			$data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}

	public function getAlbumInfo ($album)
	{
		$album = intval($album);
		
    	if (empty($album)) throw new Exception("Album ID is empty!");

    	$mtag = "albumInfo-{$album}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('userPhotoAlbums');
            $this->db->where('id', $album);

            $query = $this->db->get();

            $results = $query->result();

			$data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}
	
	public function getPhotoComments ($photoID)
	{
		$photoID = intval($photoID);
		
		if (empty($photoID)) throw new Exception('Photo ID is empty!');
		
		$mtag = "photoComments-{$photoID}";

        $data = $this->cache->memcached->get($mtag);
        
		if (!$data)
        {
            $this->db->from('photoComments');
			$this->db->where('photoID', $photoID);
			$this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}
}