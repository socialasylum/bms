<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crm_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getContacts ()
    {
        $mtag = "crmContacts-" . $this->session->userdata('company');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, datestamp, companyName, name, title, type, status");
            $this->db->from('crmContacts');
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->where('deleted', 0);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getContactInfo ($contact)
    {
        if (empty($contact)) throw new Exception("contact ID is empty");

        $mtag = "contactInfo{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, datestamp, name, companyName, title, websiteUrl, type, status, industry, otherIndustry");
            $this->db->from('crmContacts');
            $this->db->where('id', $contact);
            $this->db->where('company', $this->session->userdata('company'));
            $this->db->where('deleted', 0);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertContact ($p)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $this->session->userdata('company'),
                'userid' => $this->session->userdata('userid'),
                'name' => $p['name'],
                'companyName' => $p['companyName'],
                'title' => $p['title'],
                'websiteUrl' => $p['websiteUrl'],
                'type' => $p['type'],
                'status' => $p['status'],
                'industry' => $p['industry'],
                'otherIndustry' => $p['otherIndustry']
            );

        $this->db->insert('crmContacts', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateContact ($p)
    {

        if (empty($p['id'])) throw new Exception('CRM contact ID is empty');

        $data = array
            (
                'updated' => DATESTAMP,
                'updatedBy' => $p['user'],
                'name' => $p['name'],
                'companyName' => $p['companyName'],
                'title' => $p['title'],
                'websiteUrl' => $p['websiteUrl'],
                'type' => $p['type'],
                'status' => $p['status'],
                'industry' => $p['industry'],
                'otherIndustry' => $p['otherIndustry']
            );

        // to ensure only their company CRM contacts are updated
        $this->db->where('company', $this->session->userdata('company'));

        $this->db->where('id', $p['id']);
        $this->db->update('crmContacts', $data);

        return $p['id'];
    }


    /**
     * TODO: short description.
     *
     * @param mixed $emails 
     *
     * @return TODO
     */
    public function saveEmails ($contact, $emails)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        if (!empty($emails))
        {
            foreach ($emails as $email)
            {
                // skips email if empty
                if (empty($email)) continue;

                $this->insertContactEmail($contact, $email);
            }
        }
    }

    /**
     * Inserts an email for a CRm contact
     *
     * @param INT $contact - Contact ID
     * @param String $email-
     *
     * @return INT
     */
    public function insertContactEmail($contact, $email)
    {

        if (empty($contact)) throw new Exception("CRM contact ID is empty!");


        $data = array
            (
                'contactId' => $contact,
                'email' => $email
            );

        $this->db->insert('crmEmails', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function clearContactEmails ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $this->db->where('contactId', $contact);
        $this->db->delete('crmEmails');

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertContactAddress ($contact, $k, $p)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        // will attempt to geolocate the address
        $p['lat'] = $p['lng'] = 0;

        if (!empty($p['addressLine1'][$k]))
        {
            $address = $p['addressLine1'][$k];

            if (!empty($p['addressLine2'][$k])) $address .= ' ' . $p['addressLine2'][$k];
            if (!empty($p['addressLine3'][$k])) $address .= ' ' . $p['addressLine3'][$k];

            $address .= " {$p['city'][$k]}, {$p['state'][$k]} {$p['postalCode'][$k]}";

            // will geolcode address
            $ll = $this->functions->getAddressLatLng($address);

            $p['lat'] = $ll->lat;
            $p['lng'] = $ll->lng;

        }


        $data = array
            (
                'contactId' => $contact,
                'datestamp' => DATESTAMP,
                'description' => $p['description'][$k],
                'notes' => $p['notes'][$k],
                'address' => $p['addressLine1'][$k],
                'address2' => $p['addressLine2'][$k],
                'address3' => $p['addressLine3'][$k],
                'city' => $p['city'][$k],
                'state' => $p['state'][$k],
                'postalCode' => $p['postalCode'][$k],
                'lat' => $p['lat'],
                'lng' => $p['lng']
            );

        $this->db->insert('crmAddress', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     * @param mixed $phones  
     *
     * @return TODO
     */
    public function savePhones ($contact, $phones)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        if (!empty($phones))
        {
            foreach ($phones['number'] as $k => $number)
            {
                $this->insertContactPhone($contact, $phones['type'][$k], $number);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     * @param mixed $type    
     * @param mixed $number  
     *
     * @return TODO
     */
    public function insertContactPhone ($contact, $type, $number)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $data = array
            (
                'contactId' => $contact,
                'type' => $type,
                'phoneNumber' => $number
            );

        $this->db->insert('crmPhone', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function clearContactPhone ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $this->db->where('contactId', $contact);
        $this->db->delete('crmPhone');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id        
     * @param mixed $addresses 
     *
     * @return TODO
     */
    public function saveAddress($contact, $addresses)
    {

        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        if (!empty($addresses))
        {
            foreach ($addresses['description'] as $k => $void)
            {
                $this->insertContactAddress($contact, $k, $addresses);
            }
        }

        return true;
    }


    public function clearContactAddress ($contact)
    {

        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $this->db->where('contactId', $contact);
        $this->db->delete("crmAddress");

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactEmails ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "contactEmails{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('email');
            $this->db->from('crmEmails');
            $this->db->where('contactId', $contact);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets the main email address for a contact
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactEmail ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "CRM-Main-Email-{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("email");
            $this->db->from('crmEmails');
            $this->db->where('contactId', $contact);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->email;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactAddresses ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "contactAddr{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('description, notes, address, address2, address3, city, state, postalCode');
            $this->db->from('crmAddress');
            $this->db->where('contactId', $contact);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactPhones ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "contactPhones{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('type, phoneNumber');
            $this->db->from('crmPhone');
            $this->db->where('contactId', $contact);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;

    }

    /**
     * TODO: short description.
     *
     * @param mixed $note 
     *
     * @return TODO
     */
    public function insertCRMNote($contact, $note)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'contactId' => $contact,
                'userid' => $this->session->userdata('userid'),
                'note' => $note,
            );

        $this->db->insert('crmNotes', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactNotes ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "contactNotes{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('datestamp, userid, note');
            $this->db->from('crmNotes');
            $this->db->where('contactId', $contact);
            $this->db->order_by('datestamp', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $date   
     * @param mixed $type   
     * @param mixed $status 
     * @param mixed $notes  
     *
     * @return TODO
     */
    public function insertCall ($contact, $date, $type, $status, $notes)
    {
        if (empty($contact)) throw new Exception("Contact ID is empty!");


        $callDate = date("Y-m-d H:i:s", strtotime($date));

        $data = array
            (
                'contactId' => $contact,
                'datestamp' => DATESTAMP,
                'callDate' => $callDate,
                'type' => $type,
                'status' => $status,
                'notes' => $notes
            );

        $this->db->insert('crmCalls', $data);


        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactAddress ($contact)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "CRM-Main-Address-{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, description, address, address2, address3, city, state, postalCode");
            $this->db->from('crmAddress');
            $this->db->where('contactId', $contact);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets a CRM address by its ROW ID
     *
     * @param mixed $id 
     *
     * @return object
     */
    public function getAddressByID ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("CRM address row ID is empty!");

        $mtag = "CRMAddressByID-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, description, address, address2, address3, city, state, postalCode");
            $this->db->from('crmAddress');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;

    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactPhone ($contact, $type = 1)
    {
        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "CRM-Main-Phone-{$contact}-{$type}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('phoneNumber');
            $this->db->from('crmPhone');
            $this->db->where('contactId', $contact);
            $this->db->where('type', $type);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->phoneNumber;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }


        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function deleteCrmContact ($contact)
    {
        $contact = intval($contact);

        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $data = array
            (
                'deleted' => 1,
                'deletedBy' => $this->session->userdata('userid'),
                'deleteDate' => DATESTAMP
            );

        $this->db->where('id', $contact);
        $this->db->update('crmContacts', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $contact 
     *
     * @return TODO
     */
    public function getContactCompany ($contact)
    {
        $contact = intval($contact);

        if (empty($contact)) throw new Exception("CRM contact ID is empty!");

        $mtag = "crmContactsCompany-{$contact}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("company");
            $this->db->from('crmContacts');
            $this->db->where('id', $contact);
            $this->db->where('deleted', 0);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->company;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
}
