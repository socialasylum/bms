<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class setup_model extends CI_Model
{
	public $tablePath, $folders, $user, $group;
	
    function __construct()
    {
        parent::__construct();
        
        $this->tablePath = $_SERVER['DOCUMENT_ROOT'] . 'sql' . DS . 'mysql' . DS;
    
		// will check that these folders have been created and have write permissions
		$this->folders = array
		(
			$_SERVER['DOCUMENT_ROOT'] . 'public/uploads',
			$_SERVER['DOCUMENT_ROOT'] . 'public/less'
		);
    
		// gets name of apache user and group
		$this->user = exec('whoami');
		
		$this->group = exec("groups {$this->user}");
		
		$this->group = trim(substr($this->group, stripos($this->group, ':') + 1));
    }
    
    public function loadDBUtils ()
    {
	    $this->load->dbutil();
	    $this->load->dbforge();
	    
	    return true;
    }


	public function connect ($host, $username, $password, $database = null, $type = 'mysqli')
	{
        // working on testing on connecting w/o db name
        $config = array
            (
                'hostname' => $host,
                'username' => $username,
                'password' => $password,
                'database' => $database,
                'dbdriver' => 'mysqli',
                'dbprefix' => '',
                'pconnect' => false,
                'db_debug' => false
            );

		$dbConnection = $this->load->database($config, true);

        if ($dbConnection !== true)
        {
            throw new exception("Unable to connect to database during setup process!");
        }
        
        return true;
	}
	
	public function closeConnection ()
	{
		$this->db->close();
		
		return true;
	}
	
	// checks if database exists
	public function dbExists ($database)
	{
		if (empty($database)) throw new Exception("Database name is empty!");
		
		if (!$this->dbutils->database_exists($database)) return false;
		
		return true;
	}
	
	public function createDatabase ($database)
	{
		$create = $this->dbforge->create_database($database);
		
		if (!$create) throw new Exception("Unable to create database: {$database}");
		
		return true;	
	}

	public function checkTables ()
	{
		// first gets table list
		$tables = $this->_getTableList();
		
		if (empty($tables)) throw new Exception("Table list is empty... I'm pretty sure this thing uses a database");
		
		foreach ($tables as $tbl)
		{
			$exists = $this->db->table_exists($tbl);
			
			// if table exists, skips creating it
			if ($exists) continue;
			
			// table does not exist, will attempt to create it
			$this->_createTbl($tbl);
		}
		
		return true;
	}
	
	
	private function _getTableList ()
	{
		$list = array();
		
		$files = $this->functions->getDirContents($this->tablePath, false, true, false);
		
		
		if (empty($files)) throw new Exception("Files are empty. Check file path {$this->tablePath}");
		
		foreach ($files as $file)
		{
			$list[] = str_replace('.sql', '', $file);
		}
	
		return $list;
	}
	
	private function _createTbl ($tbl)
	{
		if (empty($tbl)) throw new Exception("Table name is empty!");
		
		$file = $this->tablePath . $tbl . '.sql';
		
		$sql = file_get_contents($file);
		
		$e = error_get_last();
		
		// was unable to get file content
		if ($sql === false) throw new Exception("Unable to get SQL for {$tbl}. {$e['message']}");
		
		// exec sql
		$db->query($sql);
		
		return true;
	}
	
	public function checkFolders ()
	{
		if (empty($this->folders)) return true;
		
		foreach ($this->folders as $folder)
		{
			if (!file_exists($folder)) PHPFunctions::createDir($folder, false, 0755);
			
			// checks if folders are writable
			if (!is_writable($folder)) throw new Exception("Unable to write to folder! <code>chown {$this->user}:{$this->group} {$folder}</code><code>chmod 0755 {$folder}</code>");
			
						
		}
		
		return true;
	}
}