<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class item_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function insertItem ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'itemID' => $p['itemID'],
                'name' => $p['name'],
                'description' => $p['description'],
                'shortDescription' => $p['shortDescription'],
                'cost' => $p['cost'],
                'retailPrice' => $p['retailPrice'],
                'brand' => $p['brand'],
                'model' => $p['model'],
                'sku' => $p['sku']
            );

        $this->db->insert('items', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateItem ($p)
    {
        if (empty($p['id'])) throw new Exception("Item ID is empty!");

        $data = array
            (
                'itemID' => $p['itemID'],
                'name' => $p['name'],
                'description' => $p['description'],
                'shortDescription' => $p['shortDescription'],
                'cost' => $p['cost'],
                'retailPrice' => $p['retailPrice'],
                'brand' => $p['brand'],
                'model' => $p['model'],
                'sku' => $p['sku']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('items', $data);

        return $p['id'];
    }

    public function getFolderContent ($folder = 0)
    {
        $folders = $this->modules->getViewableFolderIDs('item');

        $this->db->select("id, name, ('1') AS type, active");
        $this->db->from('documentFolders');
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('parentFolder', $folder);
        $this->db->where('deleted', 0);
        $this->db->where_in('id', $folders);

        $query = $this->db->get();

        $dirResults = $query->result();

        // now gets items


        $this->db->select("items.id, items.name, ('2') as type, items.active");
        $this->db->from('itemFolderAssign');
        $this->db->join('items', 'itemFolderAssign.itemId = items.id', 'left');
        $this->db->where('itemFolderAssign.folderId', $folder);
        $this->db->where('items.deleted', '0');
        $this->db->where('items.company', $this->session->userdata('company'));

        $query = $this->db->get();

        $itemResults = $query->result();

        $results = array_merge($dirResults, $itemResults);

        return $results;
    }

    public function assignItemToFolder ($id, $folder)
    {
        $this->clearItemFolderAssign($id);

        $folder = (empty($folder)) ? '0' : $folder;

        if (empty($id)) throw new Exception("item id is empty!");

        $data = array
            (
                'itemId' => $id,
                'folderId' => $folder
            );

        $this->db->insert('itemFolderAssign', $data);

        return true;
    }



    /**
     * Clears previous item folder assign
     *
     * @param int $id - item id
     *
     * @return boolean
     */
    public function clearItemFolderAssign ($id)
    {
        if (empty($id)) throw new Exception("id is empty!");

        $this->db->where('itemId', $id);
        $this->db->delete('itemFolderAssign');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item 
     *
     * @return object
     */
    public function getInfo ($item)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty!");

        $mtag = "itemInfo-{$item}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('items');
            $this->db->where('id', $item);
            $this->db->where('company', $this->session->userdata('company')); // ensures they can only pull their company items

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));

        }

        return $results[0];
    }

    public function getItemMainImage ($item)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty!");

        $mtag = "itemMainImg-{$item}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('imageName');
            $this->db->from('itemImages');
            $this->db->where('itemID', $item);
            $this->db->order_by('imgOrder', 'asc');
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->imageName;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     * @param mixed $p  
     *
     * @return TODO
     */
    public function saveOptions ($id, $p)
    {
        if (empty($id)) throw new Exception("Item ID is empty!");

        if (!empty($p['option']))
        {
            foreach ($p['option'] as $void => $k)
            {
                $optionID = $this->insertItemOption($id, $p['optionName'][$k], $p['type'][$k]);

                if (!empty($p['optionChoice'][$k]))
                {
                    foreach ($p['optionChoice'][$k] as $choice)
                    {
                        if (!empty($choice)) $this->insertOptionChoice($optionID, $choice);
                    }
                }
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id   
     * @param mixed $name 
     * @param mixed $type 
     *
     * @return TODO
     */
    public function insertItemOption ($id, $name, $multiSelect)
    {

        if (empty($id)) throw new Exception("Item ID is empty!");

        $data = array
            (
                'userid' => $this->session->userdata('userid'),
                'datestamp' => DATESTAMP,
                'itemID' => $id,
                'name' => $name,
                'multiSelect' => $multiSelect
            );

        $this->db->insert('itemOptions', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $optionId 
     * @param mixed $name     
     *
     * @return TODO
     */
    public function insertOptionChoice ($optionId, $name)
    {
        if (empty($optionId)) throw new Exception("Option ID is empty!");

        $data = array
            (
                'optionId' => $optionId,
                'name' => $name
            );

        $this->db->insert('itemOptionChoices', $data);


        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item 
     *
     * @return TODO
     */
    public function getItemOptions ($item)
    {

        if (empty($item)) throw new Exception("Item ID is empty!");

        $mtag = "itemOptions-{$item}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, name, multiSelect');
            $this->db->from('itemOptions');
            $this->db->where('itemId', $item);
            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $optionId 
     *
     * @return TODO
     */
    public function getItemOptionChoices ($optionId)
    {
        if (empty($optionId)) throw new Exception("option ID is empty!");

        $mtag = "itemOptionChoices-{$optionId}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, name');
            $this->db->from('itemOptionChoices');
            $this->db->where('optionId', $optionId);
            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $img 
     *
     * @return TODO
     */
    public function saveUploadedImages ($item, $img)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty!");

        if (!empty($img))
        {
            foreach ($img as $k => $file)
            {
                $uploaded = false;
                
                try
                {
                    $uploaded = $this->imageUploaded($item, $file);

                    if ($uploaded == true)
                    {
                        $this->_updateImg($item, $file, $k);
                    }
                    else
                    {
                        $this->_insertImg($item, $file, $k);
                    }
                }
                catch(Exception $e)
                {
                    // if an error occurs, skips the row
                    $this->functions->sendStackTrace($e);
                    continue;
                }

            }
        }
    }

    /**
     * Checks if an item image has been uploaded
     *
     * @param mixed $item 
     * @param mixed $file 
     *
     * @return TODO
     */
    public function imageUploaded ($item, $file)
    {
        if (empty($item)) throw new Exception("Item ID is empty!");
        if (empty($file)) throw new Exception("File name is empty!");

        $mtag = "itemImageUploaded-{$item}-{$file}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('itemImages');
            $this->db->where('itemID', $item);
            $this->db->where('imageName', $file);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }


        if ($data > 0) return true;


        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item 
     *
     * @return TODO
     */
    public function getItemImages ($item, $limit = 0)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty");

        $mtag = "itemImages-{$item}-{$limit}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('imageName');
            $this->db->from('itemImages');
            $this->db->where('itemID', $item);
            $this->db->order_by('imgOrder', 'asc');

            if (!empty($limit)) $this->db->limit($limit);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Deletes an image from the server (does not affect db)
     *
     * @param mixed $fileName 
     *
     * @return true 
     */
    public function deleteImgFromServer ($fileName)
    {
        // checks if file exists
        $path = $_SERVER['DOCUMENT_ROOT'] . 'public' . DS . 'uploads' . DS . 'uploader' . DS . $this->session->userdata('company') . DS;

        $exists = file_exists($path . $fileName);

        if ($exists == false) throw new Exception("Image ({$path}{$fileName}) does not exist!");

        $delete = unlink($path . $fileName);

        if ($delete == false) throw new Exception("Unable to delete file ({$path}{$fileName})!");

        return true;
    }


    /**
     * Deletes img row in db for item
     *
     * @param mixed $itemID    
     * @param mixed $imageName 
     *
     * @return true
     */
    public function deleteItemImg ($item, $imageName)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty");
        if (empty($imageName)) throw new Exception("Image file name is empty!");

        $this->db->where('imageName', $imageName);
        $this->db->where('itemID', $item);
        $this->db->delete('itemImages');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $file 
     *
     * @return TODO
     */
    private function _insertImg ($item, $file, $order = 999)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty!");

        $order = intval($order);

        $data = array
            (
                'userid' => $this->session->userdata('userid'),
                'datestamp' => DATESTAMP,
                'itemID' => $item,
                'imageName' => $file,
                'imgOrder' => $order
            );

        $this->db->insert('itemImages', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item  
     * @param mixed $file  
     * @param mixed $order 
     *
     * @return TODO
     */
    private function _updateImg ($item, $file, $order = 999)
    {

        $data = array
            (
                'imgOrder' => $order
            );

        $this->db->where('imageName', $file);
        $this->db->where('itemID', $item);
        $this->db->update('itemImages', $data);

        return true;
    }

    /**
     * Gets a list of all items assigned to a company
     *
     * @return TODO
     */
    public function getItems ($company = 0)
    {
        if (empty($company)) $company = $this->session->userdata('company');

        if (empty($company)) throw new Exception("Company ID is empty!");


        $mtag = "allItems-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, name, itemID, shortDescription, cost, retailPrice, active');
            $this->db->from('items');
            $this->db->where('company', $company);
            $this->db->where('deleted', 0);
            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item 
     * @param mixed $p    
     *
     * @return TODO
     */
    public function saveAssignedPromos ($item, $p)
    {
        $this->_clearItemPromos($item);

        if (!empty($p['promo']))
        {
            foreach ($p['promo'] as $k => $promo)
            {
                $this->promo->insertPromoItem($promo, $item);
            }
        }

        return true;
    }

    private function _clearItemPromos ($item)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty!");

        $this->db->where('item', $item);
        $this->db->delete('promoItems');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item 
     *
     * @return TODO
     */
    public function setDeleted ($item)
    {
        $item = intval($item);

        if (empty($item)) throw new Exception("Item ID is empty!");

        $data = array('deleted' => 1);

        $this->db->where('id', $item);
        $this->db->update('items', $data);

        return true;
    }
}
