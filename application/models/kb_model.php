<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kb_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getFolderContent ($folder = 0)
    {
        $folders = $this->modules->getViewableFolderIDs('kb');

        $this->db->select("id, name, ('1') AS type, active");
        $this->db->from('documentFolders');
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('parentFolder', $folder);
        $this->db->where_in('id', $folders);

        $query = $this->db->get();

        $dirResults = $query->result();

        // now gets kb Articles

        $this->db->select("kbArticles.id, kbArticles.title AS name, ('2') as type, kbArticles.active");
        $this->db->from('kbArticleFolderAssign');
        $this->db->join('kbArticles', 'kbArticleFolderAssign.articleId = kbArticles.id', 'left');
        $this->db->where('kbArticleFolderAssign.folderId', $folder);
        $this->db->where('kbArticles.deleted', '0');
        // $this->db->where('kbArticles.company', $this->session->userdata('company'));

        $this->db->where_in('kbArticles.company', array('0', $this->session->userdata('company')));

        $query = $this->db->get();

        $kbResults = $query->result();

        $results = array_merge($dirResults, $kbResults);

        return $results;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertKbArticle ($p)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $p['company'],
                'userId' => $this->session->userdata('userid'),
                'title' => $p['title'],
                'module' => $p['module']
            );

        if (isset($p['globalArticle'])) $data['globalArticle'] = $p['globalArticle'];

        $this->db->insert('kbArticles', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateKbArticle ($p)
    {
        if (empty($p['id'])) throw new Exception("Article ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $p['company'],
                'userId' => $this->session->userdata('userid'),
                'title' => $p['title'],
                'module' => $p['module']
            );
        
        if (isset($p['globalArticle'])) $data['globalArticle'] = $p['globalArticle'];

        $this->db->where('id', $p['id']);
        $this->db->update('kbArticles', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article  
     * @param mixed $sections 
     *
     * @return TODO
     */
    public function saveSections ($p)
    {
        if (empty($p['id'])) throw new Exception("Article ID is empty!");

        if (!empty($p['sectionCnt']))
        {
            foreach ($p['sectionCnt'] as $k => $v)
            {
                if (empty($p['sectionId'][$k]))
                {
                    // new section - must be inserted
                    $this->insertSectionHeading($_POST['id'], $p['sectionHeading'][$k], $p['body'][$k], $k);
                }
                else
                {
                    // updating an existing section
                    $this->updateSectionHeading($p['sectionId'][$k], $p['sectionHeading'][$k], $p['body'][$k], $k);
                }
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article        
     * @param mixed $sectionHeading 
     * @param mixed $body           
     * @param mixed $order          Optional, defaults to 999. 
     *
     * @return TODO
     */
    public function insertSectionHeading ($article, $sectionHeading, $body, $order = 999)
    {
        if (empty($article)) throw new Exception("Article ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'kbArticle' => $article,
                'sectionHeading' => $sectionHeading,
                'body' => $body,
                'sectionOrder' => $order
            );

        $this->db->insert('kbArticleSections', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $sectionID      
     * @param mixed $article        
     * @param mixed $sectionHeading 
     * @param mixed $body           
     * @param mixed $order          Optional, defaults to 999. 
     *
     * @return TODO
     */
    public function updateSectionHeading ($sectionID, $sectionHeading, $body, $order = 999)
    {
        if (empty($sectionID)) throw new Exception("Section ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'sectionHeading' => $sectionHeading,
                'body' => $body,
                'sectionOrder' => $order
            );

        $this->db->where('id', $sectionID);
        $this->db->update('kbArticleSections', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param int $id - document id
     * @param int $folder - folder id
     *
     * @return boolean
     */
    public function assignArticleToFolder ($article, $folder = 0)
    {
        if (empty($article)) throw new Exception("Article ID is empty!");

        $this->clearArticleFolderAssign($article);

        $data = array
            (
                'articleId' => $article,
                'folderId' => $folder
            );

        $this->db->insert('kbArticleFolderAssign', $data);

        return true;
    }


    /**
     * Clears previous previous folders this article was assigned to
     *
     * @param int $article - article ID
     *
     * @return boolean
     */
    public function clearArticleFolderAssign ($article)
    {
        if (empty($article)) throw new Exception("Article ID is empty!");

        $this->db->where('articleId', $article);
        $this->db->delete('kbArticleFolderAssign');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article 
     *
     * @return TODO
     */
    public function getArticleData ($article)
    {
        $article = intval($article);

        if (empty($article)) throw new Exception("Article ID is empty!");

        $mtag = "kbArticleData-{$article}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('kbArticles');
            $this->db->where_in('company', array('0', $this->session->userdata('userid')));
            $this->db->where('id', $article);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article 
     *
     * @return TODO
     */
    public function getSectionData ($article)
    {
        $article = intval($article);

        if (empty($article)) throw new Exception("Article ID is empty!");

        $mtag = "kbArticleSectionData-{$article}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, sectionHeading, body");
            $this->db->from('kbArticleSections');
            $this->db->where('kbArticle', $article);
            $this->db->order_by('sectionOrder', 'asc');


            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Used to check if a module has already been assigned
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function checkModuleAssigned ($module)
    {
        $module = intval($module);

        if (empty($module)) throw new Exception("Module ID is empty!");

        $mtag = "kbModuleAssigned-{$module}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('kbArticles');
            $this->db->where('module', $module);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

        return false;
    }

    /**
     * gets the KB article ID assigned to a particular module
     *
     * @param mixed $module 
     *
     * @return int - KB article ID; (boolean) false if empty
     */
    public function getArticleIDFromModuleID ($module)
    {
        $module = intval($module);

        if (empty($module)) throw new Exception("Module ID is empty!");

        $mtag = "kbIDFromModID-{$module}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('kbArticles');
            $this->db->where('active', 1);
            $this->db->where('deleted', 0);
            $this->db->where('module', $module);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article
     *
     * @return TODO
     */
    public function deleteArticle ($article)
    {

        if (empty($article)) throw new Exception("Article ID is empty!");

        $data = array
            (
                'active' => 0,
                'deleted' => 1,
                'deleteDate' => DATESTAMP,
                'deletedBy' => $this->session->userdata('userid')
            );

        $this->db->where('company', $this->session->userdata('company')); // safeguard ensures they can only delete their company kb
        $this->db->where('id', $article);
        $this->db->update('kbArticles', $data);

        return true;
    }
}
