<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class blog_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function createNewPost ($p)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'publishDate' => $p['publishDate'],
                'userid' => $p['user'],
                'company' => $this->session->userdata('company'),
                'title' => $p['title'],
                'body' => $p['body'],
                'excerpt' => $p['excerpt'],
                'category' => $p['category'],
                'allowComments' => $p['allowComments'],
                'active' => $p['active']
            );

        $this->db->insert('blogs', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updatePost ($p)
    {

        if (empty($p['id'])) throw new Exception("Blog ID is empty!");

        $data = array
            (
                'publishDate' => $p['publishDate'],
                'userid' => $p['user'],
                'company' => $this->session->userdata('company'),
                'title' => $p['title'],
                'body' => $p['body'],
                'excerpt' => $p['excerpt'],
                'category' => $p['category'],
                'allowComments' => $p['allowComments'],
                'active' => $p['active']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('blogs', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getBlogs($page = 1, $category = 0, $company = 0)
    {
        if (empty($company) && $this->session->userdata('logged_in') == true)
        {
            $company = $this->session->userdata('company');
        }

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "blogs-{$page}-{$category}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $perPage = $this->config->item('blogsPerPage');

            $offset = ($page * $perPage) - $perPage;

            $this->db->select('id, publishDate, title, userid, active');
            $this->db->from('blogs');
            $this->db->where('company', $company);
            $this->db->where('deleted', 0);
            // $this->db->where('active' , 1);

            if (!empty($category)) $this->db->where('category', $category);

            $this->db->order_by('publishDate', 'DESC');
            $this->db->limit($perPage, $offset);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * Gets the total count of blogs
     *
     * @return int
     */
    public function getTotalBlogCnt ($company = 0, $category = 0)
    {
        if (empty($company) && $this->session->userdata('logged_in') == true)
        {
            $company = $this->session->userdata('company');
        }

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "totalBlogCnt-{$company}-{$category}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('blogs');
            $this->db->where('company', $company);
            $this->db->where('deleted' , 0);
            // $this->db->where('active' , 1);

            if (!empty($category)) $this->db->where('category', $category);

            $this->db->order_by('publishDate', 'DESC');

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getBlogInfo($id, $company = 0)
    {
        if (empty($id)) throw new Exception('Blog ID is empty!');

        if (empty($company) && $this->session->userdata('logged_in') == true)
        {
            $company = $this->session->userdata('company');
        }

        if (empty($company)) throw new Exception("Company ID is empty!");


        $mtag = "blogInfo-{$id}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->from('blogs');
            $this->db->where('id', $id);
            $this->db->where('company', $company);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $_POST 
     *
     * @return TODO
     */
    public function insertComment ($p)
    {
        // if (empty($p['userid'])) throw new Exception("User ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'blog' => $p['id'],
                'userid' => $p['userid'],
                'comment' => $p['comment'],
                'approved' => 1,
                'IP' => $_SERVER['REMOTE_ADDR']
            );

        if (!empty($p['name'])) $data['name'] = $p['name'];
        if (!empty($p['email'])) $data['email'] = $p['email'];

        $this->db->insert('blogComments', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $blog 
     *
     * @return TODO
     */
    public function getBlogComments ($blog)
    {
        if (empty($blog)) throw new Exception("blog ID is empty!");

        $mtag = "blogsComments-" . $blog;

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, datestamp, userid, name');
            $this->db->from('blogComments');
            $this->db->where('blog', $blog);
            $this->db->where('approved' , 1);
            $this->db->where('deleted', 0);
            $this->db->where('spam', 0);
            $this->db->order_by('datestamp', 'DESC');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param int $commentId
     *
     * @return TODO
     */
    public function getCommentBody ($commentId)
    {
        if (empty($commentId)) throw new Exception("comment ID is empty!");

        $mtag = "blogCmtBody{$commentId}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('comment');
            $this->db->from('blogComments');
            $this->db->where('id' , $commentId);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->comment;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Clears all tags for a blog
     *
     * @param mixed $blog 
     *
     * @return true
     */
    public function clearTags ($blog)
    {
        $blog = intval($blog);

        if (empty($blog)) throw new Exception("Blog ID is empty!");

        $this->db->where('blog', $blog);
        $this->db->delete('blogTags');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $blog - Blog id
     * @param mixed $tags 
     *
     * @return TODO
     */
    public function saveTags ($blog, $tags)
    {
        $blog = intval($blog);

        if (empty($blog)) throw new Exception("Blog ID is empty!");

        if (!empty($tags))
        {
            foreach ($tags as $k => $tag)
            {
                $this->insertTag($blog, $tag);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $blog 
     * @param mixed $tag  
     *
     * @return TODO
     */
    public function insertTag ($blog, $tag)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'blog' => $blog,
                'userid' => $this->session->userdata('userid'),
                'tag' => $tag
            );

        $this->db->insert('blogTags', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $blog 
     *
     * @return TODO
     */
    public function getTags ($blog)
    {
        $blog = intval($blog);

        if (empty($blog)) throw new Exception("Blog ID is empty!");

        $mtag = "blogTags-{$blog}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, tag');
            $this->db->from('blogTags');
            $this->db->where('blog', $blog);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Updates a comment and sets it to deleted
     *
     * @param mixed $comment 
     *
     * @return true
     */
    public function deleteComment ($comment)
    {
        $comment = intval($comment);

        if (empty($comment)) throw new Exception("Comment ID is empty!");

        $data = array
            (
                'deleted' => 1,
                'deletedBy' => $this->session->userdata('userid'),
                'deleteDate' => DATESTAMP
        );

        $this->db->where('id', $comment);
        $this->db->update('blogComments', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $comment 
     *
     * @return TODO
     */
    public function setSpamComment ($comment)
    {
        $comment = intval($comment);

        if (empty($comment)) throw new Exception("Comment ID is empty!");

        $data = array
            (
                'spam' => 1,
                'spamSetBy' => $this->session->userdata('userid'),
                'spamSetDate' => DATESTAMP
        );

        $this->db->where('id', $comment);
        $this->db->update('blogComments', $data);

        return true;
    }
    
    /**
    * updates a blogs featured image
    */ 
    public function updateFeaturedImg ($blog, $img)
    {
    	$blog = intval($blog);

        if (empty($blog)) throw new Exception("Blog ID is empty!");
    
	    $data = array
	    (
	    	'featuredImg' => $img
	    );
	    
	    $this->db->where('id', $blog);
	    $this->db->update('blogs', $data);
	    
	    return true;
    }
}
