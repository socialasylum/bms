<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class position_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    public function getPositions ($id = 0, $countOnly = false)
    {
        $mtag = "positions-{$id}-" . $this->session->userdata('company') . '-' . (int) $countOnly;

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('positions');
            $this->db->where('deleted', 0);
            $this->db->where('company', $this->session->userdata('company'));

            if (!empty($id)) $this->db->where('id', $id);

            $this->db->order_by('name', 'asc');

            if ($countOnly == true)
            {
                $data = $this->db->count_all_results();
            }
            else
            {
                $query = $this->db->get();

                $results = $query->result();

                if (!empty($id)) $data = $results[0];
                else $data = $results;
            }

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function createPosition ($p, $company = 0)
    {
        if (empty($company)) $company = $this->session->userdata('company');
        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $company,
                'name' => $p['name'],
                'shortName' => $p['shortName'],
                'default' => $p['default'],
                'active' => 1,
                'deleted' => 0,
                'description' => $p['description']
            );

        $this->db->insert('positions', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateInfo ($p)
    {
        if (empty($p['id'])) throw new Exception("Position ID is empty!");

        $data = array
            (
                'name' => $p['name'],
                'shortName' => $p['shortName'],
                'default' => $p['default'],
                'description' => $p['description']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('positions', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param int $id - Posiiton ID
     *
     * @return TODO
     */
    public function clearChildPositions ($id)
    {
        if (empty($id)) throw new Exception("Position ID is empty!");

        $this->db->where('position', $id);
        $this->db->delete('childPositions');

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param array $p - post array
     *
     * @return TODO
     */
    public function saveChildPositions ($p)
    {

        if (empty($p['id'])) throw new Exception("Position ID is empty!");

        if (!empty($p['childPosition']))
        {
            foreach ($p['childPosition'] as $childPosition)
            {
                if (empty($childPosition)) throw new Exception('childPosition ID is empty!');

                $data = array
                    (
                        'position' => $p['id'],
                        'childPosition' => $childPosition,
                    );

                $this->db->insert('childPositions', $data);
            }
        }

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $position      
     * @param mixed $childPosition 
     *
     * @return boolean - true if position is a child of the childPosition (if that makes sense?)
     */
    public function checkIsChildPosition ($position, $childPosition)
    {

        $this->db->where('position', $position);
        $this->db->where('childPosition', $childPosition);
        $this->db->from('childPositions');


        if ($this->db->count_all_results() > 0) return true;

    return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function deletePosition ($p)
    {
        if (empty($p['id'])) throw new Exception("Position ID is empty!");
        if (empty($p['company'])) throw new Exception("company ID is empty!");

        $data = array
            (
                'deleted' => 1
            );

        $this->db->where('id', $p['id']);
        $this->db->where('company', $p['company']);
        $this->db->update('positions', $data);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function clearDefaultPosition ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $data = array('default' => 0);

        $this->db->where('company', $company);
        $this->db->update('positions', $data);

        return true;
    }
}
