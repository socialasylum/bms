<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class postfix_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Function to add a new domain to postfix. Uses postfixadmin's CLI shell script.
     * 
     * @param $domain string, domain name to add
     * @param $quota int, max size of mailboxes for domain, default to 0 (unlimited)
     * @param $mailboxes int, max number of mailboxes allowed for domain, default to 0 (unlimited)
     * @return array representing return value from exec()
     */
    
    public function createDomain($domain, $quota = 0, $mailboxes = 0)
    {
        $results = array();
        exec("sh /var/www/html/postfixadmin/scripts/postfixadmin-cli domain add $domain --quota $quota --mailboxes $mailboxes --active --default-aliases", $results);
        return $results;
    }
    
    /*
     * Function to add a new mailbox to postfix. Uses postfixadmin's CLI shell script.
     * 
     * @param $email string, email address to add
     * @param $password string, password for mailbox
     * @param $name string, full name for mailbox, default to ''
     * @param $quota int, max size of mailbox, default to 0 (unlimited)
     * @return array representing return value from exec()
     */
    
    public function createMailbox($email, $password, $name = '', $quota = 0)
    {
        $results = array();
        exec("sh /var/www/html/postfixadmin/scripts/postfixadmin-cli mailbox add $email --password $password --password2 $password --quota $quota --active", $results);
        return $results;
    }
    
    /*
     * Function to change email password
     * 
     * @param $email string, email to change
     * @param $pass string, password to change to
     * @return array representing return value from exec()
     */
    
    public function updatePassword($email, $pass)
    {
        $result = array();
        exec("sh /var/www/html/postfixadmin/scripts/postfixadmin-cli mailbox update $email --password $password --password2 $password", $results);
        return $results;
    }
}

