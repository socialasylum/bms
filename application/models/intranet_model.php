<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class intranet_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $email    
     * @param mixed $passwd
     *
     * @return TODO
     */
    public function checkLogin ($email, $passwd, $passwordHashed = false, $facebookID = 0)
    {
        if ($passwordHashed == false) $passwd = sha1($passwd);

        $this->db->select('id, status, admin');
        $this->db->from('users');
        $this->db->where_in('status', array(1,3));

		$this->db->where('deleted', 0);

        $this->db->where('email', $email);
        $this->db->where('passwd', $passwd);
        /*
        if (empty($facebookID))
        {
            $this->db->where('email', $email);
            $this->db->where('passwd', $passwd);
        }
        else
        {
            $this->db->where('(email', $email);
            $this->db->or_where('facebookID', "{$facebookID})");
        }
        */
        $query = $this->db->get();

        $results = $query->result();

        // error_log($this->db->last_query());

        if (empty($results)) return false;

        return $results[0];
    }


    /**
     * TODO: short description.
     *
     * @param mixed $widget 
     *
     * @return TODO
     */
    public function checkWidgetAssigned ($widget)
    {
        if (empty($widget)) throw new Exception("Widget ID is empty");

        $mtag = "widgetAssigned-{$widget}-" . $this->session->userdata('userid');

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('userWidgets');
            $this->db->where('userid', $this->session->userdata('userid'));
            $this->db->where('widget', $widget);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $portlet 
     * @param mixed $widget  
     *
     * @return TODO
     */
    public function saveWidgetPortlet ($portlet, $widget)
    {

        if (empty($portlet)) throw new Exception("Portlet ID is empty!");
        if (empty($widget)) throw new Exception("Widget ID is empty");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'portlet' => $portlet,
                'widget' => $widget
            );

        $this->db->insert('userWidgets', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function checkUserAssignedToCompany ($company)
    {
        return $this->companies->userAssignedToCompany($this->session->userdata('userid'), $company);
    
            // deprecated
        /*

        if (empty($company)) throw new Exception("Company ID is empty!");

        if ((int) $company == (int) $this->session->userdata('company')) return true;

        $this->db->from('userCompanies');
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->where('company', $company);

        if ($this->db->count_all_results() > 0) return true;


        return false;
         */
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updatePortletOrder ($p)
    {
        if (!empty($p['widget']))
        {
            foreach ($p['widget'] as $k => $port)
            {

                // echo "Port: {$port} - K: {$k}\n";

                $this->updatePortlet($port, ((int) $k + 1));
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function updatePortlet ($port, $widget)
    {
        if (empty($port)) throw new Exception("Port ID is empty!");
        if (empty($widget)) throw new Exception("Widget ID is empty!");

        $userid = $this->session->userdata('userid');

        if (empty($userid)) throw new Exception("userid is empty!");

        $data = array
        (
            'portlet' => $port
        );

        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('userid', $userid);
        $this->db->where('widget', $widget);
        $this->db->update('userWidgets', $data);

        // echo "PORT: {$port} - NEWPORT: {$newPort}" . PHP_EOL;
        // echo $this->db->last_query() . PHP_EOL;

    return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $porlet 
     *
     * @return boolean
     */
    public function clearPortlet ($porlet)
    {
        $porlet = intval($porlet);

        if (empty($porlet)) throw new Exception("Portlet ID is empty!");


        $this->db->where('portlet', $porlet);
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->delete('userWidgets');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $modules 
     *
     * @return TODO
     */
    public function checkUnvModuleAssign ($modules)
    {
        if (empty($modules)) throw new Exception("Module list is empty!");

        $assignedModules = $this->modules->getAssignedModules();

        foreach ($modules as $r)
        {

            $moduleToAssign = $r->id;
            $assigned = false;

            // module is a universal package
            if ((bool) $r->unvPackage == false) continue; // skips module if not universal

            // error_log("MODULE ID : {$r->id} - UNV:  {$r->unvPackage}");

            // checks if module is assigned
            if (!empty($assignedModules))
            {
                // goes through each module to check if assigned
                foreach ($assignedModules as $am)
                {
                    // error_log("ASSIGNED MODULE ID : {$am->module}" . PHP_EOL);

                    if ($r->id == $am->module)
                    {
                        // error_log("MODULE IS ASSIGNED: {$am->module}" . PHP_EOL);
                        $assigned = true;
                        break;
                    }
                }
            }

            // module is not assigned during check - assign it
            if ($assigned == false)
            {
                $this->modules->insertCompanyModule($this->session->userdata('company'), $moduleToAssign);
            }

        }

    }

    /**
     * TODO: short description.
     *
     * @param mixed $user    Optional, defaults to 0. 
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getNotifications ($countOnly = false, $user = 0, $company = 0, $activeOnly = true)
    {
        if (empty($user)) $user = $this->session->userdata('userid');
        if (empty($company)) $company = $this->session->userdata('company');

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "notifications-" . (int) $countOnly . "-{$user}-{$company}-" . (int)$activeOnly;

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, datestamp, module, icon, msg, url");
            $this->db->from('notifications');
            $this->db->where('userid', $user);
            $this->db->where('company', $company);

            if ($activeOnly == true) $this->db->where('active', 1);

            if ($countOnly == true)
            {
                $data = $this->db->count_all_results();
            }
            else
            {
                $query = $this->db->get();

                $data = $query->result();
            }
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }


        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getNotificationUrl ($id)
    {
        $mtag = "notificationUrl-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("url");
            $this->db->from('notifications');
            $this->db->where('id', $id);
            $this->db->where('userid', $this->session->userdata('userid'));
            $this->db->where('company', $this->session->userdata('company'));

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->url;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function clearNotification ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Notification ID is empty!");

        $data = array
            (
                'active' => 0
            );

        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('userid', $this->session->userdata('userid'));
        $this->db->where('id', $id);
        $this->db->update('notifications', $data);


        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function insertPasswordResetRequest ($user, $company)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $requestID = uniqid(null, true);

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $user,
                'company' => $company,
                'requestID' => $requestID,
            );

        $this->db->insert('passwordResets', $data);

        return $requestID;
    }

    /**
     * Gets the userid for a password reset based upon the password requestID
     *
     * @param mixed $requestID
     *
     * @return int - User ID
     */
    public function getPasswordResetUser ($requestID)
    {
        if (empty($requestID)) throw new Exception("Request ID is empty!");

        $mtag = "passwdResetUser-{$requestID}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('userid');
            $this->db->from('passwordResets');
            $this->db->where('requestID', $requestID);
            $this->db->where('active', 1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->userid;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * used to update password when user is reseting password
     *
     * @param mixed $user     
     * @param mixed $password 
     *
     * @return TODO
     */
    public function updateUserPassword ($user, $password)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($password)) throw new Exception("Password is empty!");

        $data = array
            (
                'passwd' => sha1($password)
            );

        $this->db->where('id', $user);
        $this->db->update('users', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function deactivatePasswordRequests ($user)
    {
        if (empty($user)) throw new Exception("User ID is empty!");

        $data = array
            (
                'active' => 0,
                'processed' => DATESTAMP
            );


        $this->db->where('userid', $user);
        $this->db->update('passwordResets', $data);

        return true;
    }
    
    
    
    /// saves the order of the ports
    public function saveDashboard ($ports)
    {
	    if (empty($ports)) throw new Exception("Ports are empty!");
	    
	    $userid = $this->session->userdata('userid');
	    $company = $this->session->userdata('company');
	    
	    $this->_clearDashboard($userid, $company);
	    
	    $id = array();
	    
	    foreach ($ports as $p)
	    {
	    	$data = array
	    	(
	    		'userid' => $userid,
	    		'company' => $company,
	    		'row' => $p['row'],
	    		'col' => $p['col'],
	    		'x' => $p['x'],
	    		'y' => $p['y'],
	    		'url' => $p['url']
	    	);
	    	
	    	$this->db->insert('dashboardPorts', $data);
	    	
	    	$id[] = $this->db->insert_id();
	    }
	    
	    return $id;
    }
    
    // clears all dashboard ports for a user
    private function _clearDashboard ($userid, $company)
    {
	    $userid = intval($userid);
	    $company = intval($company);
	    
	    if (empty($userid)) throw new Exception("User ID is empty!");
	    if (empty($company)) throw new Exception("Company ID is empty!");
	    
	    $this->db->where('company', $company);
	    $this->db->where('userid', $userid);
	    $this->db->delete('dashboardPorts');
	    
	    return true;
    }
    
    public function getDashboardPorts ($userid, $company)
    {
	    $userid = intval($userid);
	    $company = intval($company);

	    if (empty($userid)) throw new Exception("User ID is empty!");
	    if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "dashboardPorts-" . $userid . "-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('dashboardPorts');
            $this->db->where('userid', $userid);
            $this->db->where('company', $company);
            
			$query = $this->db->get();
			
			$data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

		return $data;
    }
}
