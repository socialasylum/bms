<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stream_model extends CI_Model
{
    /**
     * TODO: short description.
     *
     */
    function __construct ()
    {
        parent::__construct();
    }

	public function getPosts ($userid, $parentPost = 0)
	{
		$userid = intval($userid);
		$parentPost = intval($parentPost);
    	
    	if (empty($userid)) throw new Exception("UserID is empty!");
    	
    	$company = $this->session->userdata('company');
    	
    	$mtag = "posts-{$userid}-{$parentPost}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id, datestamp, postingUser');
            $this->db->from('wallPosts');
            $this->db->where('userid', $userid);
            $this->db->where('parentPost', $parentPost);
            $this->db->where('company', $company);
            $this->db->order_by('datestamp', 'desc');
            //$this->db->limit(10);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}
	
	public function getPostBody ($id)
	{
		$id = intval($id);
    	
    	if (empty($id)) throw new Exception("Post ID is empty!");
    	
    	$mtag = "postBody-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('body');
            $this->db->from('wallPosts');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

			$data = $results[0]->body;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}
	
	
	public function getLikeCnt ($postID)
	{
		$postID = intval($postID);
    	
    	if (empty($postID)) throw new Exception("Post ID is empty!");
    	
    	$mtag = "likeCnt-{$postID}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->from('wallPostLikes');
            $this->db->where('post', $postID);

			$data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}

	public function getPostPhotos ($postID)
	{
		$postID = intval($postID);
		
    	if (empty($postID)) throw new Exception("Post ID is empty!");

    	$mtag = "postPhotos-{$postID}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('id, fileName');
            $this->db->from('albumPhotos');
            $this->db->where('postID', $postID);
            
            $this->db->order_by('imgOrder');
            $this->db->order_by('datestamp');
            $this->db->order_by('fileName');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        
        return $data;
	}
	
}

