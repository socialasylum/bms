<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class territories_model extends CI_Model
{
    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    /*
     // deprecated - used function in Territory library
    public function getTerritories ($company, $id = null)
    {
        if (empty($company)) throw new Exception('company ID is empty!');

        $mtag = "territories{$company}{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('territories');
            $this->db->where('company', $company);

            if (!empty($id)) $this->db->where('id', $id);

            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            if (empty($id)) $data = $results;
            else $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    */

    /**
     * TODO: short description.
     *
     * @param mixed $name    
     * @param mixed $company 
     *
     * @return TODO
     */
    public function checkTerritoryNameInUse ($name, $company)
    {
        if (empty($name)) throw new Exception('territory name is empty!');
        if (empty($company)) throw new Exception('company ID is empty!');

        $this->db->from('territories');
        $this->db->where('name', $name);
        $this->db->where('company', $company);

        if ($this->db->count_all_results() > 0) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function createTerritory ($p)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'createdBy' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'name' => $p['name'],
                'positionOwner' => $p['positionOwner'],
                'mapCenterLat' => $p['mapCenterLat'],
                'mapCenterLng' => $p['mapCenterLng'],
                'mapZoom' => $p['mapZoom']
            );

        $this->db->insert('territories', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateTerritory ($p)
    {

        if (empty($p['id'])) throw new Exception("Territory ID is empty!");

        $data = array
            (
                'name' => $p['name'],
                'positionOwner' => $p['positionOwner'],
                'mapCenterLat' => $p['mapCenterLat'],
                'mapCenterLng' => $p['mapCenterLng'],
                'mapZoom' => $p['mapZoom']
            );

        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('id', $p['id']);
        $this->db->update('territories', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $lat    
     * @param mixed $lng    
     * @param mixed $radius 
     *
     * @return TODO
     */
    public function saveMarker ($territory, $lat, $lng, $radius, $color, $opacity)
    {

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'territoryId' => $territory,
                'lat' => $lat,
                'lng' => $lng,
                'radius' => $radius,
                'color' => $color,
                'opacity' => $opacity
            );

        $this->db->insert('territoryMarkers', $data);

        return $this->db->insert_id();

    }

    /**
     * TODO: short description.
     *
     * @param mixed $territory 
     *
     * @return TODO
     */
    public function clearMarkers ($territory)
    {
        $territory = intval($territory);

        if (empty($territory)) throw new Exception("Territory ID is empty!");

        $this->db->where('territoryId', $territory);
        $this->db->delete('territoryMarkers');

        return true;
    }

    public function getTerritoryLocationCount($id)
    {
        $this->db->select('id');
        $this->db->from('locations');
        $this->db->where('territory', $id);

        $cnt = $this->db->count_all_results();

        return $cnt;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $territory 
     *
     * @return TODO
     */
    public function getMarkersColor ($territory)
    {
        $territory = intval($territory);

        if (empty($territory)) throw new Exception("Territory ID is empty!");

        $mtag = "terMarkersColor-{$territory}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('color');
            $this->db->from('territoryMarkers');
            $this->db->where('territoryId', $territory);
            $this->db->limit(1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->color;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
}
