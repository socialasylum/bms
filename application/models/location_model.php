<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class location_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param String $name - location name
     * @param INT @company - companyID
     *
     * @return boolean - True if name is in use
     */
    public function checkStoreNameInUse ($name, $company)
    {
        if (empty($name)) throw new Exception('territory name is empty!');
        if (empty($company)) throw new Exception('company ID is empty!');

        $this->db->from('locations');
        $this->db->where('name', $name);
        $this->db->where('company', $company);

        if ($this->db->count_all_results() > 0) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function createLocation ($p)
    {

        $p['salesTax'] = doubleval($p['salesTax']);

        $data = array
            (
                'datestamp' => DATESTAMP,
                'createdBy' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'name' => $p['name'],
                'address' => $p['address'],
                'address2' => $p['address2'],
                'address3' => $p['address3'],
                'city' => $p['city'],
                'state' => $p['state'],
                'postalCode' => $p['postalCode'],
                'territory' => $p['territory'],
                'openDate' => $p['openDate'],
                'lat' => $p['lat'],
                'lng' => $p['lng'],
                'salesTax' => $p['salesTax'],
                'active' => $p['active'],
                'allowUpdate' => $p['allowUpdate'],
                'lastUpdated' => DATESTAMP
            );

        if (!empty($p['StoreID'])) $data['StoreID'] = $p['StoreID'];
        if (!empty($p['description'])) $data['description'] = $p['description'];
        if (!empty($p['addDescription'])) $data['addDescription'] = $p['addDescription'];


        if (!empty($p['phone'])) $data['phone'] = $p['phone'];
        if (!empty($p['fax'])) $data['fax'] = $p['fax'];
        if (!empty($p['websiteUrl'])) $data['websiteUrl'] = $p['websiteUrl'];


        $this->db->insert('locations', $data);

        $id = $this->db->insert_id();

        if (empty($p['StoreID'])) $this->assignStoreIDFromDBID($id);

        return $id;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateLocation ($p)
    {
        if (empty($p['id'])) throw new Exception("Location Primary Key ID is empty!");

        $p['salesTax'] = doubleval($p['salesTax']);

        $data = array
            (
                'StoreID' => $p['StoreID'],
                'name' => $p['name'],
                'address' => $p['address'],
                'address2' => $p['address2'],
                'address3' => $p['address3'],
                'city' => $p['city'],
                'state' => $p['state'],
                'postalCode' => $p['postalCode'],
                'territory' => $p['territory'],
                'openDate' => $p['openDate'],
                'rent' => $p['rent'],
                'utilities' => $p['utilities'],
                'lat' => $p['lat'],
                'lng' => $p['lng'],
                'salesTax' => $p['salesTax'],
                'active' => $p['active'],
                'allowUpdate' => $p['allowUpdate'],
                'lastUpdated' => DATESTAMP
            );

        if (!empty($p['description'])) $data['description'] = $p['description'];
        if (!empty($p['addDescription'])) $data['addDescription'] = $p['addDescription'];

        if (!empty($p['phone'])) $data['phone'] = $p['phone'];
        if (!empty($p['fax'])) $data['fax'] = $p['fax'];
        if (!empty($p['websiteUrl'])) $data['websiteUrl'] = $p['websiteUrl'];

        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('id', $p['id']);
        $this->db->update('locations', $data);

        return $p['id'];
    }


    /**
     * If user does not specify a store ID, it will automatically assign the StoreID to the auto increment ID to give it some value
     *
     * @param int $id - Locations DB ID
     *
     * @return boolean
     */
    public function assignStoreIDFromDBID ($id)
    {
        if (empty($id)) throw new Exception("Location ID is empty!");

        $data = array
            (
                'StoreID' => $id
            );

        $this->db->where('id', $id);
        $this->db->update('locations', $data);

        return true;
    }

    /**
     * Clears all  users assigned to a patciular location
     *
     * @param int $location 
     *
     * @return boolean
     */
    public function clearAssignedUsers ($location)
    {
        $location = intval($location);

        if (empty($location)) throw new Exception("Location ID is empty!");

        $this->db->where('location', $location);
        $this->db->delete('userLocations');

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $loctaion 
     * @param mixed $p        
     *
     * @return TODO
     */
    public function saveAssignedUsers ($location, $p)
    {
        if (!empty($p['assigned']))
        {
            foreach ($p['assigned'] as $userid)
            {
                $homeLocation = (empty($p['home'][$userid])) ? false : true;

                if ($homeLocation == true) $this->clearPreviousHomeLocations($userid);

                $this->assignUserToLocation($userid, $location, $homeLocation);
            }
        }
    }


    /**
     * Assigns user to a location
     *
     * @param mixed $userid   
     * @param mixed $loctaion 
     *
     * @return int
     */
    public function assignUserToLocation ($userid, $location, $homeLocation = false)
    {
        $userid = intval($userid);
        $location = intval($location);

        if (empty($userid)) throw new Exception("User ID is empty");
        if (empty($location)) throw new Exception("location ID is empty");


        $data = array
            (
                'location' => $location,
                'userid' => $userid
            );


        if ($homeLocation == true) $data['homeLocation'] = 1;

        $this->db->insert('userLocations', $data);


        return $this->db->insert_id();

    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function getUserAssigned ($userid, $location)
    {
        $userid = intval($userid);
        $location = intval($location);

        if (empty($userid)) throw new Exception("User ID is empty!");
        if (empty($location)) throw new Exception("location ID is empty!");

        $mtag = "userLocAssigned-{$userid}-{$location}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select("location, homeLocation");
            $this->db->from('userLocations');
            $this->db->where('userid', $userid);
            $this->db->where('location', $location);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * Sets all locations to not home locations for a user
     *
     * @param mixed $userid 
     *
     * @return boolean
     */
    public function clearPreviousHomeLocations ($userid)
    {
        $userid = intval($userid);

        if (empty($userid)) throw new Exception("User ID is empty!");
        

        // gets list of all location ID's in that company


        $data['homeLocation'] = 0;

        $this->db->where('userid', $userid);
        $this->db->update('userLocations', $data);

        return true;
    }

    public function addYouTubeUrl ($location, $url, $order = 0)
    {
        if (empty($location)) throw new Exception("Location ID is empty!");
        if (empty($url)) throw new Exception("URL is empty!");


        $data = array
            (
                'userid' => $this->session->userdata('userid'),
                'locationid' => $location,
                'company' => $this->session->userdata('company'),
                'datestamp' => DATESTAMP,
                'url' => $url
            );

        if (empty($order)) $data['videoOrder'] = $order;

        $this->db->insert('locationYouTubeVideos', $data);

        return $this->db->insert_id();
    }

    public function updateYoutubeVideoOrder ($videos)
    {
        if (!empty($videos))
        {
            foreach ($videos as $order => $video)
            {
                $this->updateYTOrder($video, $order);
            }
        }
    }

    public function updateYTOrder ($video, $order)
    {
        $video = intval($video);

        if (empty($video)) throw new Exception("Video ID is empty!");

        $data = array
            (
                'videoOrder' => $order
            );

        $this->db->where('id', $video);
        $this->db->update('locationYouTubeVideos', $data);

        return true;
    }

    public function deleteYTVideo ($video)
    {
        $video = intval($video);

        if (empty($video)) throw new Exception("Video ID is empty!");

        $this->db->where('id', $video);
        $this->db->delete('locationYouTubeVideos');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $fileName 
     *
     * @return TODO
     */
    public function addImg ($location, $fileName)
    {
        if (empty($location)) throw new Exception("Location ID is empty");
        if (empty($fileName)) throw new Exception("File Name is empty!");

        $data = array
            (
                'locationid' => $location,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'datestamp' => DATESTAMP,
                'fileName' => $fileName,
                'imgOrder' => 9999
            );


        $this->db->insert('locationImages', $data);

        return $this->db->insert_id();
    }
}
