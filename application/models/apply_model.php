<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class apply_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getOpenPositions ($company, $id = 0)
    {

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "openPositions-{$company}-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('openPositions');
            $this->db->where('company', $company);

            if (!empty($id)) $this->db->where('id', $id);

            $this->db->order_by('datestamp', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            if (!empty($id)) $data = $results[0];
            else $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getAppCompany ($id)
    {
        if (empty($id)) throw new Exception("application ID is empty");

        $mtag = "appCompany-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('company');
            $this->db->from('openPositions');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->company;
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertMainApplication ($id)
    {

        $ssn = $this->session->userdata('ssn');

        $ssnComp = preg_replace("/[^0-9]/","", $ssn[0] . $ssn[1] . $ssn[2]);

        $data = array
            (
                'datestamp' => DATESTAMP,
                'jobId' => $id,
                'company' => $this->session->userdata('company'),
                'firstName' => $this->session->userdata('firstName'),
                'lastName' => $this->session->userdata('lastName'),
                'MI' => $this->session->userdata('MI'),
                'SSN' => $ssnComp,
                'address' => $this->session->userdata('address'),
                'city' => $this->session->userdata('city'),
                'state' => $this->session->userdata('state'),
                'postalCode' => $this->session->userdata('postalCode'),
                'phone' => $this->session->userdata('phone'),
                'mobile' => $this->session->userdata('mobile'),
                'email' => $this->session->userdata('email'),
                'is18' => $this->session->userdata('is18'),
                'workInUSA' => $this->session->userdata('workInUSA'),
                'referredBy' => $this->session->userdata('referredBy'),
                'related' => $this->session->userdata('related'),
                'relatedWho' => $this->session->userdata('relatedWho'),
                'relatedWhere' => $this->session->userdata('relatedWhere'),
                'previous' => $this->session->userdata('previous'),
                'previousFrom' => $this->session->userdata('previousFrom'),
                'previousTo' => $this->session->userdata('previousTo'),
                'previousReason' => $this->session->userdata('previousReason'),
                'desiredState' => $this->session->userdata('desiredState'),
                'location' => $this->session->userdata('location'),
                'employed' => $this->session->userdata('employed'),
                'contact' => $this->session->userdata('contact'),
                'desiredPosition' => $this->session->userdata('desiredPosition'),
                'startDate' => $this->session->userdata('startDate'),
                'salary' => $this->session->userdata('salary'),
                'monAM' => $this->session->userdata('monAM'),
                'tueAM' => $this->session->userdata('tueAM'),
                'wedAM' => $this->session->userdata('wedAM'),
                'thuAM' => $this->session->userdata('thuAM'),
                'friAM' => $this->session->userdata('friAM'),
                'satAM' => $this->session->userdata('satAM'),
                'sunAM' => $this->session->userdata('sunAM'),
                'monPM' => $this->session->userdata('monPM'),
                'tuePM' => $this->session->userdata('tuePM'),
                'wedPM' => $this->session->userdata('wedPM'),
                'thuPM' => $this->session->userdata('thuPM'),
                'friPM' => $this->session->userdata('friPM'),
                'satPM' => $this->session->userdata('satPM'),
                'sunPM' => $this->session->userdata('sunPM'),
                'refName1' => $this->session->userdata('refName1'),
                'refPhone1' => $this->session->userdata('refPhone1'),
                'refOcc1' => $this->session->userdata('refOcc1'),
                'refYear1' => $this->session->userdata('refYear1'),
                'refName2' => $this->session->userdata('refName2'),
                'refPhone2' => $this->session->userdata('refPhone2'),
                'refOcc2' => $this->session->userdata('refOcc2'),
                'refYear2' => $this->session->userdata('refYear2'),
                'refName3' => $this->session->userdata('refName3'),
                'refPhone3' => $this->session->userdata('refPhone3'),
                'refOcc3' => $this->session->userdata('refOcc3'),
                'refYear3' => $this->session->userdata('refYear3'),
                'skills' => $this->session->userdata('skills'),
                'milService' => $this->session->userdata('milService'),
                'rank' => $this->session->userdata('rank'),
                'guard' => $this->session->userdata('guard'),
                'convict' => $this->session->userdata('convict'),
                'offense' => $this->session->userdata('offense')
            );

        $this->db->insert('applications', $data);

    return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $jobId 
     * @param mixed $p     
     *
     * @return TODO
     */
    public function insertSchools ($appId)
    {
        $type = $this->session->userdata('type');
        $desc = $this->session->userdata('desc');
        $years = $this->session->userdata('years');
        $grad = $this->session->userdata('grad');
        $studied = $this->session->userdata('studied');

        if(!empty($desc))
        {
            foreach($desc as $k => $v)
            {
                $data = array
                    (
                        'appId' => $appId,
                        'type' => $type[$k],
                        'desc' => $desc[$k],
                        'graduate' => $grad[$k],
                        'years' => $years[$k],
                        'studied' => $studied[$k]
                    );

                $this->db->insert('appEdu', $data);
            }
        }

    }

    public function insertJobs ($appId)
    {
        $companyName = $this->session->userdata('companyName');
        $companyAddress = $this->session->userdata('companyAddress');
        $companyPhone = $this->session->userdata('companyPhone');
        $from = $this->session->userdata('from');
        $to = $this->session->userdata('to');
        $salary = $this->session->userdata('salary');
        $position = $this->session->userdata('position');
        $reason = $this->session->userdata('reason');

        if(!empty($companyName))
        {
            foreach($companyName as $k => $v)
            {
                $data = array
                    (
                        'appdId' => $appId,
                        'from' => $from[$k],
                        'to' => $to[$k],
                        'companyName' => $companyName[$k],
                        'address' => $companyAddress[$k],
                        'phone' => $companyPhone[$k],
                        'salary' => $salary[$k],
                        'position' => $position[$k],
                        'reason' => $reason[$k]
                    );

                $this->db->insert('appJobs', $data);
            }
        }
    }


}
