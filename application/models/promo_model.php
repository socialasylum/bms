<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class promo_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getPromos ()
    {
        $mtag = "promos-{$this->session->userdata('company')}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, datestamp, name, startDate, endDate, active');
            $this->db->from('promos');
            $this->db->where('deleted', 0);
            $this->db->where('company', $this->session->userdata('company'));

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function promoInfo ($id)
    {
        $id = intval($id);

        if (empty($id)) throw new Exception("Promo ID is empty");

        $mtag = "promoInfo-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('promos');
            $this->db->where('id', $id);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $promo 
     *
     * @return TODO
     */
    public function getPromoDesc ($promo)
    {
        $promo = intval($promo);

        if (empty($promo)) throw new Exception("Promo ID is empty");

        $mtag = "promoDesc-{$promo}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('description');
            $this->db->from('promos');
            $this->db->where('id', $promo);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->description;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;

    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertPromo ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'name' => $p['name'],
                'description' => $p['description'],
                'startDate' => $p['startDate'],
                'endDate' => $p['endDate'],
                'activeatedBy'=> $p['activeatedBy'],
                'active' => $p['active']
            );

        if (!empty($p['bannerImg'])) $data['bannerImg'] = $p['bannerImg'];

        $this->db->insert('promos', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updatePromo ($p)
    {
        $p['id'] = intval($p['id']);

        if (empty($p['id'])) throw new Exception("Promo ID is empty!");

        $data = array
            (
                'name' => $p['name'],
                'bannerImg' => $p['bannerImg'],
                'description' => $p['description'],
                'startDate' => $p['startDate'],
                'endDate' => $p['endDate'],
                'activeatedBy'=> $p['activeatedBy'],
                'active' => $p['active']
            );


        $this->db->where('id', $p['id']);
        $this->db->update('promos', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function savePromoItems ($p)
    {
        $this->_clearPromoItems($p['id']);

        if (!empty($p['item']))
        {
            foreach ($p['item'] as $item)
            {
                $this->insertPromoItem($p['id'], $item);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $promo 
     * @param mixed $item  
     *
     * @return TODO
     */
    public function insertPromoItem ($promo, $item)
    {
        $promo = intval($promo);
        $item = intval($item);

        if (empty($promo)) throw new Exception("Promo ID is empty!");
        if (empty($item)) throw new Exception("Item ID is empty!");

        $data = array
            (
                'promo' => $promo,
                'item' => $item
            );

        $this->db->insert('promoItems', $data);

        return $this->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $promo 
     *
     * @return TODO
     */
    private function _clearPromoItems ($promo)
    {
        $promo = intval($promo);

        if (empty($promo)) throw new Exception("Promo ID is empty!");

        $this->db->where('promo', $promo);
        $this->db->delete('promoItems');

        return true;
    }

    /**
     * gets all items assigned to a promo
     *
     * @param mixed $promo 
     *
     * @return array
     */
    public function getPromoItems ($promo)
    {
        $promo = intval($promo);

        if (empty($promo)) throw new Exception("Promo ID is empty!");

        $mtag = "promoItems-{$promo}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('item');
            $this->db->from('promoItems');
            $this->db->where('promo', $promo);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        $items = array();

        foreach ($data as $r)
        {
            $items[] = $r->item;
        }

        return $items;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $item  
     * @param mixed $promo 
     *
     * @return TODO
     */
    public function checkItemPromoAssigned ($item, $promo)
    {
        $promo = intval($promo);
        $item = intval($item);

        if (empty($promo)) throw new Exception("Promo ID is empty!");
        if (empty($item)) throw new Exception("Item ID is empty!");

        $mtag = "itemPromoAssigned-{$item}-{$promo}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('promoItems');
            $this->db->where('item', $item);
            $this->db->where('promo', $promo);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ((int) $data > 0) return true;

        return $false;
    }
}
