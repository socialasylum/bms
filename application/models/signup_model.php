<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class signup_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createCompany ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'companyName' => $p['companyName'],
                'addressLine1' => $p['address'],
                'addressLine2' => $p['address2'],
                'addressLine3' => $p['address3'],
                'city' => $p['city'],
                'state' => $p['state'],
                'postalCode' => $p['postalCode'],
                'active' => 1,
                'phone' => $p['phone'],
                'users' => $p['users'],
                'industry' => $p['industry']
            );

        if (!empty($p['email'])) $data['email'] = $p['email'] . '@' . $p['emaildomain'];
        
        if (!empty($p['otherIndustry'])) $data['otherIndustry'] = $p['otherIndustry'];

        $this->db->insert('companies', $data);

        return $this->db->insert_id();
    }

    /**
     * inserts the modules the company has purchased
     *
     * @param int $companyId 
     * @param array $modules
     *
     * @return boolean
     */
    public function assignPurchasedModules ($companyId, $modules)
    {
        if (empty($companyId)) throw new Exception("company ID is empty!");

        if (!empty($modules))
        {
            foreach ($modules as $k => $module)
            {
                $data = array
                    (
                        'company' => $companyId,
                        'module' => $module
                    );

                $this->db->insert('companyModules', $data);
            }
        }

        return true;
    }

    /**
     * assigns universal modules to company, modules must be passwd
     *
     * @param mixed $companyId  
     * @param mixed $unvModules 
     *
     * @return boolean
     */
    public function assignUnvModules($companyId, $unvModules)
    {
        if (empty($companyId)) throw new Exception("company ID is empty!");

        if (!empty($unvModules))
        {
            foreach ($unvModules as $r)
            {
                $data = array
                    (
                        'company' => $companyId,
                        'module' => $r->id
                    );

                $this->db->insert('companyModules', $data);
            }
        }

        return true;

    }

}
