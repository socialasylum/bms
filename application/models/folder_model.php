<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class folder_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * TODO: short description.
     *
     * @param mixed $folder  Optional, defaults to 0. 
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getFolders ($folder = 0, $company = 0, $namespace = null)
    {
        $folder = intval($folder);
        $company = intval($company);

        if (empty($company)) $company = $this->session->userdata('company');

        if (empty($company)) throw new Exception("Company ID is empty!");

        if (!empty($namespace)) $folders = $this->modules->getViewableFolderIDs($namespace);

        $mtag = "folderList-{$folder}-{$company}-{$namespace}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, name, active');
            $this->db->from('documentFolders');
            $this->db->where('company', $company);
            $this->db->where('parentFolder', $folder);
            $this->db->where('deleted', 0);

            if (!empty($namespace)) $this->db->where_in('id', $folders);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getFolderData ($folder, $company = 0)
    {
        $folder = intval($folder);
        $company = intval($company);

        if (empty($company)) $company = $this->session->userdata('company');

        if (empty($folder)) throw new Exception("Folder ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "folderData-{$folder}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('documentFolders');
            $this->db->where('id', $folder);
            $this->db->where('company', $company);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function buildTree ($folder = 0, $selected = 0, $company = 0, $namespace = null, $includeFiles = false)
    {
        $html = "";

        // error_log("COMPANY: {$company}");
        // error_log("namespace: {$namespace}");
        // error_log("Include Files: {$includeFiles}");

        if ($includeFiles == true && (empty($namespace) OR empty($company))) throw new Exception("Namespace & Company ID must be specified in order to include files");

        $folders = $this->getFolders($folder, $company, $namespace);

        if ($includeFiles == true && $namespace == 'kb') $files = $this->knowledgebase->getTreeFiles($folder);


        if (empty($folders) && empty($files))
        {

        }
        else
        {

            $html .= EOL . "<ul>" . EOL;

            foreach ($folders as $r)
            {
                // if user is not logged in, and folder is not active, hides it
                if (empty($r->active))
                {
                    if ($this->session->userdata('logged_in') == true)
                    {
                        // do nothing
                    }
                    else
                    {
                        continue;
                    }
                }

                $selectedClass = ($r->id == $selected) ? ' selected' : null;

                $html .= TAB . "<li id='{$r->id}' class='folder expanded{$selectedClass}'>{$r->name}";

                $html .= $this->buildTree($r->id, $selected, $company, $namespace, $includeFiles);

                $html .= "</li>" . EOL;

            }
            
                if ($includeFiles == true AND $namespace == 'kb' AND !empty($files))
                {
                       
                        foreach ($files as $k => $id)
                        {
                            $info = null;

                            // first much check file company to ensure things
                            $global = $this->knowledgebase->isGlobalArticle($id);
                            error_log("Global {$id}: {$global}");
                            // skips article if it is not set to global Article
                            if (empty($global)) continue;

                            $info = $this->knowledgebase->getRow($id);


                            $html .= "<li id='{$id}' class='file{$selectedClass}'>{$info->title}</li>" . EOL;
                        }
                }

            $html .= "</ul>" . PHP_EOL;

        }


        return $html;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $name         
     * @param mixed $parentFolder 
     *
     * @return TODO
     */
    public function insertFolder($name, $parentFolder = 0)
    {
        if (empty($name)) throw new Exception("Folder name is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'name' => $name,
                'company' => $this->session->userdata('company'),
                'createdBy' => $this->session->userdata('userid'),
                'parentFolder' => $parentFolder
            );

        $this->db->insert('documentFolders', $data);

        return $this->db->insert_id();
    }


    /**
     * Moves a folder into another folder
     *
     * @param INT $id
     * @param INT $parentFolder
     *
     * @return boolean
     */
    public function moveFolder ($id, $parentFolder = 0)
    {
        $id = intval($id);
        $parentFolder = intval($parentFolder);

        if (empty($id)) throw new Exception("id is empty!");

        $data = array('parentFolder' => $parentFolder);

        $this->db->where('id', $id);
        $this->db->update('documentFolders', $data);

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param int $id - document id
     * @param int $folder - folder id
     *
     * @return boolean
     */
    public function assignItemToFolder ($id, $folder, $tbl, $col)
    {
        $this->clearItemFolderAssign($id, $tbl, $col);

        $id = intval($id);
        $folder = intval($folder);

        if (empty($id)) throw new Exception("id is empty!");
        // if (empty($folder)) throw new Exception("Folder ID is empty!");
        if (empty($tbl)) throw new Exception("Table Prefix is empty!");


        $data = array
            (
                'folderId' => $folder
            );

        if (empty($col)) $data[$tbl . 'Id'] = $id;
        else $data[$col] = $id;

        $this->db->insert($tbl . 'FolderAssign', $data);

        return $this->db->insert_id();;
    }

    /**
     * Clears previous itemss folder assign
     *
     * @param int $id - item id
     * @param string $tbl - Table prefix:  item, doc etc.
     *
     * @return boolean
     */
    public function clearItemFolderAssign ($id, $tbl, $col)
    {
        if (empty($id)) throw new Exception("id is empty!");
        if (empty($tbl)) throw new Exception("Table Prefix is empty!");


        if (empty($col)) $this->db->where($tbl . 'Id', $id);
        else $this->db->where($col, $id);

        $this->db->delete($tbl . 'FolderAssign');

        // error_log($this->db->last_query());

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateFolder ($p)
    {
        $p['folder'] = intval($p['folder']);

        if (empty($p['folder'])) throw new Exception("Folder ID is  empty!");

        $data = array
            (
                'name' => $p['name'],
                'active' => $p['active']
            );

        $this->db->where('id', $p['folder']);
        $this->db->update('documentFolders', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function saveModsViewable ($p)
    {
        $p['folder'] = intval($p['folder']);

        if (empty($p['folder'])) throw new Exception("Folder ID is  empty!");

        $this->clearFolderViewableMods($p['folder']);

        if (!empty($p['module']))
        {
            foreach ($p['module'] as $module)
            {
                $this->saveFolderMoldViewable($p['folder'], $module);
            }
        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     * @param mixed $module 
     *
     * @return TODO
     */
    private function saveFolderMoldViewable ($folder, $module)
    {
        $folder = intval($folder);
        $module = intval($module);

        if (empty($folder)) throw new Exception("Folder ID is  empty!");
        if (empty($module)) throw new Exception("Module ID is  empty!");

        $data = array
            (
                'folder' => $folder,
                'module' => $module
            );

        $this->db->insert('folderModViewable', $data);

        return $this->db->insert_id();
    }

    /**
     * clears all modules assigned to a folder
     *
     * @param mixed $folder 
     *
     * @return boolean
     */
    private function clearFolderViewableMods ($folder)
    {
        $folder = intval($folder);

        if (empty($folder)) throw new Exception("Folder ID is  empty!");

        $this->db->where('folder', $folder);
        $this->db->delete('folderModViewable');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getViewableMods ($folder)
    {
        $folder = intval($folder);

        if (empty($folder)) throw new Exception("Folder ID is empty!");

        $mtag = "viewableMods-{$folder}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, module');
            $this->db->from('folderModViewable');
            $this->db->where('folder', $folder);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function folderModViewInit ($folder)
    {
        $folder = intval($folder);

        if (empty($folder)) throw new Exception("Folder ID is empty!");

        // gets all modules that utilize folders
        $mods = $this->modules->getModulesUsingFolders();

        // throws exception if no modules - folder would be orphaned
        if (empty($mods)) throw new Exception("There are no modules that this folder can be viewable in. It will be lost in space =/");

        foreach ($mods as $module)
        {
            // should it fail while inserting, will skip module and keep going
            try
            {
                // sets each module viewable for the module
                $this->saveFolderMoldViewable($folder, $module);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                continue;
            }

        }

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function deleteFolder ($folder)
    {
        $folder = intval($folder);

        if (empty($folder)) throw new Exception("Folder ID is empty!");

        $data = array
            (
                'deleted' => 1,
                'deletedBy' => $this->session->userdata('userid'),
                'deletedDate' => DATESTAMP
            );


        // ensures they only set their company folders to deleted
        $this->db->where('company', $this->session->userdata('company'));

        $this->db->where('id', $folder);
        $this->db->update('documentFolders', $data);

        return true;
    }
}
