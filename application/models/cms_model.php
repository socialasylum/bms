<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cms_model extends CI_Model
{
	public $cfg;
	
    function __construct()
    {
        parent::__construct();
		
		$this->cfg = $this->config->item('cms');
    }
    
    public function checkFolders ($path)
    {
    	// checks main web folder
	    PHPFunctions::createDir($path, false);
	    
	    $folders = $this->cfg['folders'];
	    
	    if (empty($folders)) return true;
	    
		foreach ($folders as $k => $folder)
		{
	        PHPFunctions::createDir($path . $folder . DS, false);
		}
	    
	    return true;
	    
    }
    
    /**
    * parses HTML toget its DOM elements
    */
    public function parseHtml ($file)
    {
	    $dom = new DOMDocument();
	    
	    $dom->loadHTMLFile($file);
	    
	    return $dom;
    }

	public function sitemapUL ($path)
	{
		$files = $this->functions->getDirContents($this->path, true, true, true, false, $this->cfg['extensions']);

		$params = array
		(
			'ul' => array('id' => 'file-tree', 'class' => 'file-tree')
		);
		
		return $this->functions->buildDirectoryUL($files, $this->path, $params, true);
	}
}