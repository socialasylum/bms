<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class app_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getOpenPositions ($id = 0)
    {
        $mtag = "openPositions-" . $this->session->userdata('company') . "-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('openPositions');
            $this->db->where('company', $this->session->userdata('company'));

            if (!empty($id)) $this->db->where('id', $id);

            $this->db->order_by('datestamp', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            if (!empty($id)) $data = $results[0];
            else $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertCompanyPosition ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company')
            );

        if (!empty($p['position'])) $data['positionId'] = $p['position'];
        if (!empty($p['title'])) $data['title'] = $p['title'];

        if (!empty($p['locationId'])) $data['locationId'] = $p['locationId'];
        if (!empty($p['locationTxt'])) $data['locationTxt'] = $p['locationTxt'];

        if (!empty($p['startDate'])) $data['startDate'] = $p['startDate'];
        if (!empty($p['endDate'])) $data['endDate'] = $p['endDate'];

        if (!empty($p['description'])) $data['description'] = $p['description'];

        $this->db->insert('openPositions', $data);

        return $this->db->insert_id();
    }

    
    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    public function getApplications ($id = null)
    {
        $mtag = "apps-" . $this->session->userdata('company') . "-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('applications');
            $this->db->where('company', $this->session->userdata('company'));

            if (!empty($id)) $this->db->where('id', $id);

            $this->db->order_by('datestamp', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            if (!empty($id)) $data = $results[0];
            else $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $appId 
     *
     * @return TODO
     */
    public function getJobs ($appId)
    {
        $mtag = "appJobs-{$appId}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('appJobs');
            $this->db->where('appId', $appId);
            $this->db->order_by('from', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $appId 
     *
     * @return TODO
     */
    public function getEdu ($appId)
    {
        $mtag = "appEdu-{$appId}";

        $data = $this->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->db->from('appEdu');
            $this->db->where('appId', $appId);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
}
