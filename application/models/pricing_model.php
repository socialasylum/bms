<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class pricing_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getModules ($universal = false)
    {

        // $mtag = "modules";

        $mtag = ($universal == true) ? 'modUnv' : 'modSellabe';

        $data = $this->cache->memcached->get('modules');

        if (empty($data))
        {

            $this->db->from('modules');
            $this->db->where('active', 1);

            if ($universal == false) $this->db->where('sellable', 1);
            else $this->db->where('unvPackage', 1);

            $this->db->order_by('name', 'asc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
}
