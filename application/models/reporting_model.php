<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class reporting_model extends CI_Model
{

    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getTables ()
    {

        $mtag = "dbTables";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $sql = "SHOW TABLES";

            $query = $this->db->query($sql);

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $tbl 
     *
     * @return TODO
     */
    public function explainTbl ($tbl)
    {
        if (empty($tbl)) throw new Exception("Table name is empty");

        $mtag = "tblExplain-{$tbl}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $sql = "EXPLAIN " . $this->db->escape_str($tbl);

            $query = $this->db->query($sql);

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }



    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertReport ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $this->session->userdata('company'),
                'name' => $p['name']
            );

        $this->db->insert('reports', $data);

        return $this->db->insert_id();
    }

    public function updateReport ($p)
    {
        if (empty($p['id'])) throw new Exception("Report ID is empty");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'company' => $this->session->userdata('company'),
                'name' => $p['name']
            );

        $this->db->where('id', $p['id']);
        $this->db->update('reports', $data);

        return $p['id'];
    }


    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getFolderContent ($folder = 0)
    {
        $folders = $this->modules->getViewableFolderIDs('reporting');

        $this->db->select("id, name, ('1') AS type, active");
        $this->db->from('documentFolders');
        $this->db->where('company', $this->session->userdata('company'));
        $this->db->where('parentFolder', $folder);
        $this->db->where('deleted', 0);
        $this->db->where_in('id', $folders);

        $query = $this->db->get();

        $dirResults = $query->result();

        // now gets forms


        $this->db->select("reports.id, reports.name, ('2') as type, reports.active");
        $this->db->from('reportFolderAssign');
        $this->db->join('reports', 'reportFolderAssign.reportId = reports.id', 'left');
        $this->db->where('reportFolderAssign.folderId', $folder);
        $this->db->where('reports.deleted', '0');
        $this->db->where('reports.company', $this->session->userdata('company'));

        $query = $this->db->get();

        $docResults = $query->result();

        // gets datasets

        $this->db->select("reportDatasets.id, reportDatasets.name, ('3') as type, reportDatasets.active");
        $this->db->from('datasetFolderAssign');
        $this->db->join('reportDatasets', 'datasetFolderAssign.datasetId = reportDatasets.id', 'left');
        $this->db->where('datasetFolderAssign.folderId', $folder);
        $this->db->where('reportDatasets.deleted', '0');
        $this->db->where('reportDatasets.company', $this->session->userdata('company'));

        $query = $this->db->get();

        $dsResults = $query->result();

        // $results = array_merge($docResults, $dsResults);

        // usort($results->name);

        $results = array_merge($dirResults, $docResults, $dsResults);

        // array_multisort($results);
        return $results;
    }



    /**
     * TODO: short description.
     *
     * @param int $report - report id
     * @param int $folder - folder id
     *
     * @return boolean
     */
    public function assignReportToFolder ($report, $folder)
    {
        $this->clearReportFolderAssign($report);

        $folder = (empty($folder)) ? '0' : $folder;

        if (empty($report)) throw new Exception("report id is empty!");

        $data = array
            (
                'reportId' => $report,
                'folderId' => $folder
            );

        $this->db->insert('reportFolderAssign', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $dataset 
     * @param mixed $folder  
     *
     * @return TODO
     */
    public function assignDatasetToFolder ($dataset, $folder)
    {
        // $this->clearReportFolderAssign($report);

        $folder = (empty($folder)) ? '0' : $folder;

        if (empty($dataset)) throw new Exception("Dataset ID is empty!");

        $data = array
            (
                'datasetId' => $dataset,
                'folderId' => $folder
            );

        $this->db->insert('datasetFolderAssign', $data);

        return true;
    }

    /**
     * Clears previous report folder assign
     *
     * @param int $report - report id
     *
     * @return boolean
     */
    public function clearReportFolderAssign ($report)
    {
        if (empty($report)) throw new Exception("report ID is empty!");

        $this->db->where('reportId', $id);
        $this->db->delete('reportFolderAssign');

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $sql 
     *
     * @return TODO
     */
    public function checkSqlHarmful ($sql)
    {

        $uSql = strtoupper($sql);

        $keywords = array('DROP', 'TRUNCATE', 'INSERT', 'UPDATE', 'ALTER', 'DELETE');

        if (preg_match("[" . implode($keywords, '|') . "]", $uSql))
        {
            return true;
        }


        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $sql 
     *
     * @return TODO
     */
    public function execQuery ($sql)
    {
        $harmful = $this->checkSqlHarmful($sql);

        if ($checkHarmful == true) throw new Exception("SQL Statement can only execute SELECT statements!");

        // $sql = $this->db->escape_str($sql);

        $query = $this->db->query($sql);

        $data = $query->result_array();

        return $data;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertDataset ($p)
    {
        $data = array
            (
                'datestamp' => DATESTAMP,
                'createdBy' => $this->session->userdata('userid'),
                'company' => $this->session->userdata('company'),
                'name' => $p['name'],
                'query' => $p['query']
            );

        $this->db->insert('reportDatasets', $data);

        return $this->db->insert_id();
    }


    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateDataset ($p)
    {

        $p['id'] = intval($p['id']);

        if (empty($p['id'])) throw new Exception("Dataset ID is empty!");

        $data = array
            (
                'datestamp' => DATESTAMP,
                'createdBy' => $this->session->userdata('userid'),
                'name' => $p['name'],
                'query' => $p['query']
            );

        $this->db->where('id', $p['id']);
        $this->db->insert('reportDatasets', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $dataset 
     *
     * @return TODO
     */
    public function getDatasetInfo ($dataset)
    {
        $dataset = intval($dataset);

        if (empty($dataset)) throw new Exception("Dataset ID is empty!");

        $mtag = "dsInfo-{$dataset}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select("id, datestamp, createdBy, name, query");
            $this->db->from("reportDatasets");
            $this->db->where('id', $dataset);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $query 
     *
     * @return TODO
     */
    public function getSqlVars($query)
    {

        $query = str_replace(',', ' ', $query);
        $query = str_replace('%', ' ', $query);

        $vars = array();

        $vcnt = substr_count($query, '@');

        $start = 0;

        // goes through and finds all the variables in the query
        for ($i = 1; $i <= $vcnt; $i++)
        {

            $vs = strpos($query, '@', $start);

            // echo "VARSTART: {$vs}<BR>";

            $endPos = $this->findSqlVarEndPosition($query, $vs);

            // echo "VAREND: {$endPos}<BR>";

            $diff = $endPos - $vs;

            // echo "DIFF: {$diff}<BR>";

            $var = substr($query, $vs, $diff);


            $vars[] = $var;

            $start = $endPos;

//             echo ("VAR: {$var}<BR>");
            // break;
        }

        if (empty($vars)) return false;


        // sets session with cookie data
        $this->session->set_userdata('dsVars', $vars);

        return $vars;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $query 
     * @param mixed $start Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function findSqlVarEndPosition ($query, $start = 0)
    {
        $endChars = array(' ', ',', '%', "\n");

        $endPosition = 0;

        // echo "START:{$start}<BR>";

        foreach ($endChars as $ec)
        {
            $pos = strpos($query, $ec, $start);

            if ($pos === false)
            {
                // not found - continue on
                continue;
            }
            else
            {
                $endPosition = $pos;
                break;
            }
        }

        return $endPosition;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function parseQuery ($p)
    {

        $vars = $this->session->userdata('dsVars');

        $query = $p['query'];

        if (!empty($vars))
        {
            foreach ($vars as $k => $v)
            {
                $key = (int) $k;

                $replace = $p['vars'][$key];

                $query = str_replace($v, $replace, $query);
            }
        }

        return $query;
    }
}
