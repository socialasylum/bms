<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class codes_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Gets all code groups
     *
     * @return array->object
     */
    public function getCodeGroups ($group = 0)
    {
        $mtag = "codeGroups-{$group}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->db->select('group, display');
            $this->db->from('codes');
            // $this->db->where('active', 1);
            $this->db->where('code', 0);

            if (!empty($group)) $this->db->where('group', $group);

            $this->db->order_by('group', 'asc');

            $query = $this->db->get();

            $results = $query->result();

            if (!empty($group)) $data = $results[0];
            else $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets all codes in a paticular group, also that part of their company, or 0 (global)
     *
     * @param int $group 
     *
     * @return array->object
     */
    public function getGroupCodes ($group)
    {
        if (empty($group)) throw new Exception("code Group is empty!");

        // defines companies array, starts with 0 as first position for all "global" codes
        $companies = array('0');

        // gets all companies user is assigned to
        $userCompanies = $this->companies->getCompanies();

        if (!empty($userCompanies))
        {
            foreach ($userCompanies as $r)
            {
                $companies[] = $r->id;
            }
        }


        $this->db->select('code, display, active, company');
        $this->db->from('codes');
        $this->db->where('group', $group);
        $this->db->where('code <>', 0);
        // $this->db->where('active', 1);
        $this->db->where_in('company', $companies);
        $this->db->order_by('code', 'asc');

        $query = $this->db->get();

        $data = $query->result();

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $group    
     * @param mixed $editable Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function insertGroup ($name, $editable = 0)
    {
        if (empty($name)) throw new Exception("Group name is empty!");

        $group = $this->getNextGroup();


        $data = array
            (
                'group' => $group,
                'code' => 0,
                'display' => $name,
                'active' => 1,
                'editable' => $editable
            );

        $this->db->insert('codes', $data);

        return $group;
    }

    /**
     * Gets the next available group number
     *
     * @return int
     */
    public function getNextGroup ()
    {
        $this->db->select_max('group', 'groupMax');
        $query = $this->db->get('codes');

        $results = $query->result();

        $group = (int) $results[0]->groupMax + 1;

        return $group;
    }

    /**
     * Gets next code to be inserted for a paticular group
     *
     * @return int
     */
    public function getNextGroupCode ($group)
    {
        if (empty($group)) throw new Exception("Group ID is empty!");

        $this->db->select_max('code');
        $this->db->from('codes');
        $this->db->where('group', $group);

        $query = $this->db->get();

        $results = $query->result();

        return ((int) $results[0]->code + 1);
    }


    /**
     * TODO: short description.
     *
     * @return INT - inserts code
     */
    public function insertGroupCode ($group, $display, $active, $company, $editable = 0)
    {
        if (empty($group)) throw new Exception("Group ID is empty!");

        $code = $this->getNextGroupCode($group);

        if (empty($code)) throw new Exception("code ID is empty");


        $data = array
            (
                'group' => $group,
                'code' => $code,
                'display' => $display,
                'active' => $active,
                'company' => $company,
                'editable' => $editable
            );

        $this->db->insert('codes', $data);

        return $code;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $group    
     * @param mixed $code     
     * @param mixed $display  
     * @param mixed $active   
     * @param mixed $company  
     * @param mixed $editable Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function updateCode($group, $code, $display, $active, $company, $editable = 0)
    {

        if (empty($group)) throw new Exception("Group is Empty!");
        if (empty($code)) throw new Exception("code is Empty!");


        $data = array
            (
                'active' => $active
            );

        if ($active == 1)
        {
            $data = array
                (
                    'display' => $display,
                    'active' => 1,
                    'company' => $company,
                    'editable' => $editable
                );
        }


        $this->db->where('group', $group);
        $this->db->where('code', $code);
        $this->db->update('codes', $data);

        return true;
    }

}
