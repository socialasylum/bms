<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Positions extends Library
{

    public function __construct()
    {
        $this->tableName = 'positions';

        parent::__construct();
        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getName ($id, $shortName = false)
    {
        if (empty($id)) throw new Exception('position id is empty!');

        $ci =& get_instance();

        $ci->load->driver('cache');

        $mtag = "positionName{$id}-{$shortName}";

        $data = $ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            if ($shortName == true) $ci->db->select('shortName');
            else $ci->db->select('name');

            $ci->db->from('positions');
            $ci->db->where('id', $id);

            $query = $ci->db->get();

            $results = $query->result();

            if ($shortName == true) $data = $results[0]->shortName;
            else $data = $results[0]->name;

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getPositions ($active = null, $company = 0)
    {
        $ci =& get_instance();

        if (empty($company)) $company = $ci->session->userdata('company');

        if (empty($company)) throw new Exception("company is empty!");

        $mtag = "positions{$active}-{$company}";

        $data = $ci->cache->memcached->get($mtag);

        if (empty($data))
        {

            $ci->db->select('id, name, shortName');
            $ci->db->from('positions');
            $ci->db->where('company', $company);
            $ci->db->order_by('name', 'asc');

            if ($active == true) $ci->db->where('active', '1');

            $ci->db->where('deleted', 0);

            $query = $ci->db->get();

            $data = $query->result();

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $position 
     *
     * @return TODO
     */
    public function getChildPositions ($position)
    {
        $position = intval($position);

        if (empty($position)) throw new Exception("Position ID is empty!");

        $mtag = "childPositions-{$position}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data))
        {

            $this->ci->db->from('childPositions');
            $this->ci->db->where('position', $position);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }
        
        return $data;

    }

    /**
     * TODO: short description.
     *
     * @param mixed $position         
     * @param mixed $checkingPosition 
     *
     * @return TODO
     */
    public function isChildPosition ($position, $checkingPosition)
    {
        $position = intval($position);
        $checkingPosition = intval($checkingPosition);

        if (empty($position)) throw new Exception("Position ID is empty!");
        if (empty($checkingPosition)) throw new Exception("Checking Position ID is empty!");

        $isChild = false;

        $childPositions = $this->getChildPositions($position);

        if (!empty($childPositions))
        {
            foreach ($childPositions as $r)
            {
                if ((int) $r->childPosition == (int) $checkingPosition)
                {
                    $isChild = true;
                    break;
                }
            }
        }

        // error_log('CHECKING: ' . $position . ':' . $checkingPosition . ' = ' . (int) $isChild);

        if ($isChild == true) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function getDefaultPosition ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("company is empty!");

        $mtag = "defaultPosition-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->ci->db->select('id');
            $this->ci->db->from('positions');
            $this->ci->db->where('company', $company);
            $this->ci->db->where('default', 1);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }
}
