<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Announcements extends Library
{
    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'announcements';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }
}
