<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Locations extends Library
{
    public function __construct()
    {
        parent::__construct();
        $this->tableName = 'locations';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    public function getLocations ($company = 0, $id = null)
    {
        if (empty($company)) throw new Exception('company ID is empty!');

        $ci =& get_instance();

        $mtag = "locations-{$company}-{$id}";

        $data = $ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $ci->db->from('locations');
            $ci->db->where('company', $company);

            if (!empty($id)) $ci->db->where('id', $id);

            $ci->db->order_by('name', 'asc');

            $query = $ci->db->get();

            $results = $query->result();

            if (empty($id)) $data = $results;
            else $data = $results[0];

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets all locations a user has access to
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function getAssignedLocations ($userid)
    {
        $userid = intval($userid);

        if (empty($userid)) throw new Exception("User ID is empty!");

        $mtag = "assignedLocs-{$userid}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select("location, homeLocation");
            $this->ci->db->from('userLocations');
            $this->ci->db->where('userid', $userid);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets a list of all user ID's who are assigned to this location
     *
     * @param mixed $location 
     *
     * @return array->object
     */
    public function getLocationUsers ($location)
    {
        $location = intval($location);

        if (empty($location)) throw new Exception("Location ID is empty!");

        $mtag = "locUsers-{$location}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select("userid, homeLocation");
            $this->ci->db->from('userLocations');
            $this->ci->db->where('location', $location);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets the home location of a user
     *
     * @param mixed $user Optional, defaults to 0. 
     *
     * @return INT - location ID
     */
    public function getHome ($user = 0)
    {
        if (empty($user)) $user = $this->ci->session->userdata('userid');

        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        $mtag = "homeLoc-{$user}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {

            $this->ci->db->select('location');
            $this->ci->db->from('userLocations');
            $this->ci->db->where('userid', $user);
            $this->ci->db->where('homeLocation', 1);
            $this->ci->db->limit(1);

            $query = $this->ci->db->get();

            $results = $query->result();

            // user has no home location
            if (empty($results[0]->location)) return false;

            $data = $results[0]->location;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }
        return $data;
    }

    public function getYouTubeVideos ($location)
    {
        if (empty($location)) throw new Exception("Location ID is empty");

        $mtag = "locationYouTubeVideos-{$location}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select("id, url");
            $this->ci->db->from('locationYouTubeVideos');
            $this->ci->db->where('locationid', $location);
            $this->ci->db->order_by('videoOrder', 'ASC');

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $location 
     *
     * @return TODO
     */
    public function getImgs ($location)
    {
        $location = intval($location);

        if (empty($location)) throw new Exception("Location ID is empty");

        $mtag = "locationImages-{$location}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select("id, fileName");
            $this->ci->db->from('locationImages');
            $this->ci->db->where('locationid', $location);
            $this->ci->db->order_by('imgOrder', 'ASC');

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets the userid's of all users that are assigned to the users location (if that makes sense)
     *
     * @param mixed $user 
     *
     * @return array
     */
    public function getAllAssignedLocationusers ($user = 0)
    {
        if (empty($user)) $user = $this->session->userdata('userid');

        if (empty($user)) throw new Exception("User ID is empty!");

        $userArray = array();

        // first get all assigned locations
        $locations = $this->getAssignedLocations($user);

        if (!empty($locations))
        {
            foreach ($locations as $r)
            {
                // check if location is a current company location
                $company = $this->getTableValue('company', $r->location);

                // error_log("COMPANY: {$company} : {$this->ci->session->userdata('company')} ");

                // skips location if not part of this company session
                if ((int) $company !== (int) $this->ci->session->userdata('company')) continue;

                $users = null;

                // gets all users assigned to that location
                $users = $this->getLocationUsers($r->location);

                if (!empty($users))
                {
                    // goes through each usesr
                    foreach ($users as $ur)
                    {
                        // checks if they are already in user array
                        if (!in_array($ur->userid, $userArray))
                        {
                            // User ID is not in the array, will now add it
                            $userArray[] = $ur->userid;
                        }
                    }
                }
            }
        }

        return $userArray;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function getCompanyLocationID ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty");
    }
}
