<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Documents extends Library
{

    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'documents';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getRequiredDocs ($userid = 0, $countOnly = false, $all = false)
    {
        if (empty($userid)) $userid = $this->ci->session->userdata('userid');

        if (empty($userid)) throw new Exception("User ID is empty");

        $mtag = "requireDocs-{$userid}-" . (int) $countOnly . '-' . (int) $all;

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {

            // gets users position
            // $position = $this->ci->users->getTableValue('position', $userid);
            $position = $this->ci->users->getPosition();


            // gets docID's assigned to this users position
            $positionDocs = $this->getPositionDocs($position, true);

            $this->ci->db->select('id');
            $this->ci->db->from('documents');
            $this->ci->db->where('active', 1);
            $this->ci->db->where('deleted', 0);
            
            if ($all == false) $this->ci->db->where_in('ack', array(1,2)); // requires view or sig

            $this->ci->db->where_in('id', $positionDocs);

            if ($countOnly == true)
            {
                $data = $this->ci->db->count_all_results();
            }
            else
            {
                $query = $this->ci->db->get();

                $data = $query->result();
            }

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }


        return $data;
    }


    /**
     * Gets document ID's that are assigned to a paticular position
     *
     * @return TODO
     */
    public function getPositionDocs ($position, $returnArray = false)
    {

        $position = intval($position);

        if (empty($position)) throw new Exception("Position ID is empty!");

        $mtag = "positionDocs-{$position}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select("docId");
            $this->ci->db->from('docPositions');
            $this->ci->db->where('position', $position);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if ($returnArray == false) return $data;


        $return = array();

        if (!empty($data))
        {
            foreach ($data as $r)
            {
                $return[] = $r->docId;
            }
        }

        return $return;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $doc 
     *
     * @return TODO
     */
    public function getTotalViewCnt ($user, $doc)
    {
        $user = intval($user);
        $doc = intval($doc);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($doc)) throw new Exception("Document ID is empty!");

        $mtag = "docTotalViewCnt-{$user}-{$doc}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->from('docViewHistory');
            $this->ci->db->where('userid', $user);
            $this->ci->db->where('company', $this->ci->session->userdata('company'));
            $this->ci->db->where('docId', $doc);

            $data = $this->ci->db->count_all_results();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }
}
