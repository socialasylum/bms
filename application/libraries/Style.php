<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Style extends Library
{
	public $bgFils, $dbConnected;

    public function __construct()
    {
        parent::__construct();
        
        $this->dbConnected = true;

        $this->tableName = 'layout';

        // if connected to DB
        if (!class_exists('CI_DB'))
        {
            $this->dbConnected = false;
        }

		$this->bgFills = array
		(
			1 => 'Tile',
			//'Center', 
			//3 => 'Fill Screen', 
			4 => 'Fit to Screen', 
			5 => 'Stretch to Fill Screen'
		);
    }
    
    public function getAllValues ($company)
    {
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty");
		
		$mtag = "allLayoutValues-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
        	$this->ci->db->select('id, key, val');
        	$this->ci->db->from('layout');
        	$this->ci->db->where('company', $company);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }
        
        
        $return = array();
        
        if (!empty($data))
        {
	        foreach ($data as $r)
	        {
		        $return[$r->key] = $r->val;
	        }
        }
 
		return $return;
    }

	/**
	* gets layout val
	*/
	public function getVal ($company, $k)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty");
		
		if (empty($k)) throw new Exception("Key is empty");
		
        $mtag = "layoutVal-{$company}-{$k}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
        	
        	$this->ci->db->select('val');
        	$this->ci->db->from('layout');
        	$this->ci->db->where('company', $company);
        	$this->ci->db->where('key', $k);

            $query = $this->ci->db->get();
			
			$results = $query->result();

            $data = $results[0]->val;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));  
        }
        
        return $data;
	}
	
	// for getting the value or default if not defined
	public function getFinalVal ($company, $k)
	{
		$company = intval($company);
		
		$allowEmpty = array('contentBorderRadius');
		
		if (empty($company)) throw new Exception("Company ID is empty");
		
		if (empty($k)) throw new Exception("Key is empty");

		// gets value
		$val = $this->getVal($company, $k);

		// if allows empty values, will return the val from DB
		if (in_array($k, $allowEmpty)) return $val;

		if (empty($val)) $val = $this->getDefaultStyle($k);

		return $val;
	}
	
	/**
	* used to generate CSS tags
	*/
	public function css ($company, $tag, $attr, $k, $wildCard = '*')
	{
		$company = intval($company);
	
		// checks if tag has url( for background image
		$bgImg = (stripos($attr, 'url(') === false) ? false : true;

		if (empty($company)) throw new Exception("Company ID is empty");
		
		if (empty($attr)) $attr = '*';

		$css = null;

		if (!empty($tag)) $css .= $tag . ':';
		
		// gets value
		$val = $this->getFinalVal($company, $k);
		
		if (!empty($val) && PHPFunctions::isHexColor($val)) $val = '#' . $val;
		

		if ($bgImg && $val == 'none') $css .= $val;
		else $css .= str_replace($wildCard, $val, $attr);
		
		if (!empty($tag)) $css .= ';' . PHP_EOL;
		
		return $css;
	}
	
	// goes through default style vals in config and gets default
	public function getDefaultStyle ($k)
	{
		$sections = $this->ci->config->item('defaultStyles');

		$v = null;
		
		if (!empty($sections))
		{
			foreach ($sections as $type => $array)
			{
				if (!empty($array) && is_array($array))
				{
					foreach ($array as $key => $val)
					{
						if ($key == $k)
						{
							$v = $val;
							break;
						}
					}
				}
			}
		}
		
		return $v;
	}
	
	/**
	* checks if the company has defined styles
	*/
	public function hasStyles ($company, $returnCnt = false)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty");
		
		$mtag = "hasStyles-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
        	$this->ci->db->from('layout');
        	$this->ci->db->where('company', $company);

            $query = $this->ci->db->get();
            
            $data = $this->ci->db->count_all_results();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }
		
		if ($returnCnt) return (int) $data;
		
		if ($data > 0) return true;
		
		return false;
	}
	
	/**
	* checks if a .css layout file has been compiled
	*/
	public function styleFileExists ($company, $file = 'layout.css')
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty");
		
		$file = "{$_SERVER['DOCUMENT_ROOT']}public/uploads/layout/{$company}/{$file}";
		
		if (file_exists($file)) return true;
		
		return false;
	}

}