<?php

class MY_Email extends CI_Email
{
    function __construct($config = array())
    {
        parent::__construct($config);
    }
    
    public function setFinalMsg()
    {
        parent::_build_message();
    }
    
    public function getFinalFullMsg()
    {
        $this->setFinalMsg();
        return preg_replace('/(\n){3}/', "\n\n", $this->_header_str . $this->_finalbody);
    }
    
    public function saveToFolder($mbox, $folder = 'Sent', $draftuid = 0)
    {
        if ($folder == 'Drafts' && $draftuid > 0)
        {
            imap_delete($mbox->mbox, $draftuid, FT_UID);
            imap_expunge($mbox->mbox);
        }
        if (imap_append($mbox->mbox, $mbox->baseUrl . $folder, $this->getFinalFullMsg()))
        {
            $MC = imap_check($mbox->mbox);
            $msgInfo = imap_fetch_overview($mbox->mbox, "{$MC->Nmsgs}:{$MC->Nmsgs}", 0);
            return $msgInfo[0]->uid;
        }
        else
        {
            return 0;
        }
    }
}