<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Folders extends Library
{
    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'documentFolders';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return TODO
     */
    public function getBreadCrumbs ($folder)
    {
        $folder = intval($folder);

        // $this->folder = $folder;

        $mtag = "docFolders-" . $this->ci->session->userdata('company');

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('id, name, parentFolder');
            $this->ci->db->from('documentFolders');
            $this->ci->db->where('company', $this->ci->session->userdata('company'));

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;
        // print_r($data);

        $return = $this->buildFolderCrumbList($folder, $data);

        return array_reverse($return);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $folder 
     *
     * @return array
     */
    private function buildFolderCrumbList ($folder, $folders)
    {
        $breadCrumbs = array();

        $parentFolder = false;

        if (!empty($folders))
        {

            foreach ($folders as $k => $r)
            {
                if ($r->id == $folder)
                {
                    $breadCrumbs[] = $r->id;

                    // $this->folder = $r->id;

                    if (!empty($r->parentFolder))
                    {
                        $return = $this->buildFolderCrumbList($r->parentFolder, $folders);

                        $breadCrumbs = array_merge($breadCrumbs, $return);
                    }

                    break;
                }
            }
        }

        return $breadCrumbs;
    }
}
