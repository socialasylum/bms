<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once 'application/third_party/phpfunctions/libraries/PHPFunctions.php';

class Functions extends PHPFunctions {

    public $ci, $email;

    //public $minscripts;

    public function __construct() {
        $this->ci = & get_instance();

        $args = array
            (
            'min_version' => $this->ci->config->item('min_version'),
            'min_debug' => $this->ci->config->item('min_debug')
        );

        parent::__construct($args);

        $this->ci->load->library('session');

        $this->email = $this->ci->session->userdata('email');

        // if connected to DB
        if (class_exists('CI_DB')) {
            
        }
    }

    public function getInstance() {
        return $this->ci;
    }

    /**
     * Checks if user is logged into backend
     *
     * @return boolean TRUE if logged in
     */
    public function checkLoggedIn($redirect = true) {
        // starts session if not already started

        //$ci = & get_instance();

        $this->ci->load->helper('url');


        $pattern = '/^intranet\/login/';
        $login = preg_match($pattern, uri_string());


        $allowedUrlPatterns = array
            (
            '/^user\/profileimg/',
            '/^user\/iframeupload/',
            '/^intranet\/forgotpassword/',
            '/^intranet\/resetpassword/',
            '/^intranet\/destroy/',
            '/^intranet\/logout/',
            '/^intranet\/validate/',
            '/^intranet\/processpasswordrequest/',
            '/^blog\/savecomment/',
            '/^user\/checksubscription/',
            '/^intranet\/iframelogin/',
            '/^intranet\/fblogin/',
            '/^user\/fbcallback/',
            '/^folder\/tree/',
            '/^user\/externalimgupload/',
            '/^calendar\/externalfileupload/',
            '/^user\/albumphotoupload/',
            '/^user\/albumphoto/'
        );


        $allowed = false;
        foreach ($allowedUrlPatterns as $k => $pat) {
            $m = preg_match($pat, uri_string());
            if ($m > 0) {
                $allowed = true;
                break;
            }
        }

        if ($allowed)
            return false;

        if ($login == 0) {
            //if(!isset($_COOKIE['userid']))
            if ($this->ci->session->userdata('logged_in') === true)
                return true;
            else {
                if ($redirect)
                    PHPFunctions::redirect("/intranet/login?site-error=" . urlencode("You are not logged in") . "&ref=" . uri_string() . '&email=' . urlencode($this->email));

                return false;
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function setLoginSession($user, $email, $company, $position, $department, $rememberMe = false, $logged_in = true, $admin = false) {

        if (empty($user))
            throw new Exception("User ID is empty!");
        if (empty($email))
            throw new Exception("Email is empty!");
        if (empty($company))
            throw new Exception("Company ID is empty!");
        if (empty($position))
            throw new Exception("Position ID is empty!");

        $array = array
            (
            'userid' => (int) $user,
            'email' => $email,
            'company' => (int) $company,
            'position' => (int) $position,
            'rememberMe' => (bool) $rememberMe,
            'admin' => (bool) $admin,
            'logged_in' => $logged_in
        );

        if (!empty($department))
            $array['department'] = (int) $department;

        try {
            $companyAdmin = $this->ci->users->isCompanyAdmin($user, $company);

            $array['companyAdmin'] = (bool) $companyAdmin;

            $companyEditor = $this->ci->users->isCompanyEditor($user, $company);

            $array['companyEditor'] = (bool) $companyEditor;
        } catch (Exception $e) {
            $this->sendStackTrace($e);
        }

        $this->ci->session->set_userdata($array);

        //$this->checkCKFinderPath($company);

        return true;
    }

    /*
     * cleans an entire array
     */

    public function recursiveClean($array) {
        $ci = & get_instance();

        if (!empty($array)) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $array[$k] = $ci->functions->recursiveClean($v);
                } else {
                    $array[$k] = $ci->db->escape_str($v);
                }
            }
        }

        return $array;
    }

    /**
     * Removes certain programming tags like PHP, JS, and certain HTML
     *
     * @param String $s 
     *
     * @return String
     */
    public function removeCode($s) {
        $s = str_ireplace("<?php", '', $s);
        $s = str_ireplace("<?", '', $s);
        $s = str_ireplace("?>", '', $s);
        $s = str_ireplace("<script", '', $s);
        $s = str_ireplace("</script>", '', $s);
        $s = str_ireplace("type='application/javascript'", '', $s);
        $s = str_ireplace("type=\"application/javascript\"", '', $s);
        $s = str_ireplace("type='text/javascript'", '', $s);
        $s = str_ireplace("type=\"text/javascript\"", '', $s);


        return $s;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $headers Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getMenu($header = false, $parentMenu = null) {

        $ci = & get_instance();

        // $ci->load->library('session');
        $ci->load->driver('cache');

        $userid = $ci->session->userdata('userid');

        $data = $ci->cache->memcached->get("menu-{$userid}-{$header}-{$parentMenu}");

        $data = null;

        if (empty($data)) {
            $ci->db->from('menu');
            $ci->db->where('company', $ci->session->userdata('company'));

            if ($header == true)
                $ci->db->where('header', '1');
            else
                $ci->db->where('header', '0');

            if (!empty($parentMenu))
                $ci->db->where('parentMenu', $parentMenu);

            if ($header == true)
            {
                $ci->db->where('name', 'Admin');
                $ci->db->order_by('menuOrder', 'asc');
            }
            else
            {
                $ci->db->where('module', 5);
                $ci->db->order_by('name', 'asc');
            }
            
            $query = $ci->db->get();

            $data = $query->result();

            $ci->cache->memcached->save("menu-{$userid}-{$header}-{$parentMenu}", $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Checks if a position has access to a menu item
     *
     * @return boolean - True if position has access
     */
    public function checkMenuAccess($menu, $position) {
        if (empty($menu))
            throw new Exception("menu ID is empty!");
        if (empty($position))
            throw new Exception("position ID is empty!");

        $ci = & get_instance();

        $mtag = "menuAccess-{$menu}-{$position}";

        $data = $ci->cache->memcached->get($mtag);

        if (!$data) {
            $ci->db->where('menu', $menu);
            $ci->db->where('position', $position);
            $ci->db->from('menuAccess');

            $data = $ci->db->count_all_results();

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        if ($data > 0)
            return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $group     
     * @param mixed $company   Optional, defaults to 0. 
     * @param mixed $orderCol  Optional, defaults to null. 
     * @param mixed $orderType Optional, defaults to null. 
     *
     * @return TODO
     */
    public function getCodes($group, $company = 0, $orderCol = 'display', $orderType = 'asc') {
        $this->ci->load->driver('cache');

        $mtag = "codes-{$group}-{$company}-{$orderCol}-{$orderType}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data)) {
            $this->ci->db->from('codes');
            $this->ci->db->where('group', $group);
            $this->ci->db->where('code <>', 0);
            $this->ci->db->where('active', 1);
            $companyArray = array('0');

            if (!empty($company))
                $companyArray[] = $company;

            $this->ci->db->where_in('company', $companyArray);

            if (empty($orderCol))
                $this->ci->db->order_by('display', 'asc');
            else
                $this->ci->db->order_by($orderCol, $orderType);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * gets all groups of codes
     *
     * @return TODO
     */
    public function getCodeGroups($company = 0) {
        if (empty($company))
            $this->ci->session->userdata('company');

        $tag = "codeGroups-{$company}";

        $data = $this->ci->cache->memcached->get($tag);

        if (empty($data)) {
            $this->ci->db->from('codes');
            $this->ci->db->where('code', 0);

            $companyArray = array('0');

            if (!empty($company))
                $companyArray[] = $company;

            $this->ci->db->where_in('company', $companyArray);

            if (empty($orderCol))
                $this->ci->db->order_by('display', 'asc');
            else
                $this->ci->db->order_by($orderCol, $orderType);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($tag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getTimezones() {
        static $regions = array
            (
            'Africa' => DateTimeZone::AFRICA,
            'America' => DateTimeZone::AMERICA,
            'Antarctica' => DateTimeZone::ANTARCTICA,
            'Asia' => DateTimeZone::ASIA,
            'Atlantic' => DateTimeZone::ATLANTIC,
            'Australia' => DateTimeZone::AUSTRALIA,
            'Europe' => DateTimeZone::EUROPE,
            'Indian' => DateTimeZone::INDIAN,
            'Pacific' => DateTimeZone::PACIFIC
        );

        foreach ($regions as $name => $mask) {
            $tzlist[] = DateTimeZone::listIdentifiers($mask);
        }

        return $tzlist;
    }

    public function codeDisplay($group, $code) {
        if (empty($group))
            throw new Exception("Group is empty!");
        if (empty($code))
            throw new Exception("code is empty!");


        $ci = & get_instance();

        $mtag = "code-$group-$code";

        $data = $ci->cache->memcached->get($mtag);

        if (empty($data)) {
            $ci->db->select('display');
            $ci->db->from('codes');
            $ci->db->where('group', $group);
            $ci->db->where('code', $code);

            $query = $ci->db->get();

            $results = $query->result();

            $data = $results[0]->display;

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function headerLogo() {
        $company = $this->ci->session->userdata('company');

        $logo_url = $this->ci->config->item('logo_url');

        $logo = $this->ci->companies->getTableValue('logo', $company);

        $thumb = str_replace('.', '_thumb.', $logo);

        $logoFile = $_SERVER['DOCUMENT_ROOT'] . "{$logo_url}{$company}/{$thumb}";

        $landingUrl = $this->ci->companies->landingUrl($company);

        // if no logo img to display, uses company text
        if (empty($logo) OR ! file_exists($logoFile)) {
            $companyName = $this->ci->companies->getCompanyName($company);

            $html = "<a class='navbar-brand' href='{$landingUrl}'>{$companyName}</a>";
        } else {
            $img = "<img id='navbar-logo' src='/{$logo_url}{$company}/{$thumb}'>";

            $html = "<a class='navbar-brand logoAdjust' id='headerLogo' href='{$landingUrl}'><span class='helper'></span>{$img}</a>";
        }

        return $html;
    }

    /**
     * Gets the last time punch for a user for a paticular company
     *
     * @param mixed $user    Optional, defaults to 0. 
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return datetime
     */
    public function getLastTimePunch($user = 0, $company = 0) {
        if (empty($user))
            $user = $this->ci->session->userdata('userid');
        if (empty($company))
            $company = $this->ci->session->userdata('company');

        $user = intval($user);
        $company = intval($company);

        if (empty($user))
            throw new Exception("User ID is empty!");
        if (empty($company))
            throw new Exception("Company ID is empty");

        $mtag = "latestTimePunch-{$user}-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data) {
            $this->ci->db->select('timepunch');
            $this->ci->db->from('userTimePunch');
            $this->ci->db->where('userid', $user);
            $this->ci->db->where('company', $company);
            $this->ci->db->order_by('timepunch', 'desc');
            $this->ci->db->limit(1);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->timepunch;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets ID's and name for all active reports for a company.
     *
     * @return array->object
     */
    public function getReportList($company = 0) {
        if (empty($company))
            $company = $this->ci->session->userdata('company');

        $company = intval($company);

        if (empty($company))
            throw new Exception("Company ID is empty");

        $mtag = "reportList-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data) {
            $this->ci->db->select('id, name');
            $this->ci->db->from('reports');
            $this->ci->db->where('company', $company);
            $this->ci->db->where('active', 1);
            $this->ci->db->where('deleted', 0);
            $this->ci->db->order_by('name', 'asc');

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $string 
     *
     * @return 
     */
    public function onlyNumbers($string) {
        $string = ereg_replace("[^0-9]", "", $string);

        return $string;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function insertNotification($user, $company, $msg, $icon = 'fa fa-exclamation', $module = 0, $url = null) {

        if (empty($user))
            throw new Exception("User ID is empty!");
        if (empty($company))
            throw new Exception("Company ID is empty!");

        $data = array
            (
            'datestamp' => DATESTAMP,
            'userid' => $user,
            'company' => $company,
            'icon' => $icon,
            'msg' => $msg
        );

        if (!empty($module))
            $data['module'] = $module;
        if (!empty($url))
            $data['url'] = $url;

        $this->ci->db->insert('notifications', $data);

        return $this->ci->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $from    
     * @param mixed $subject 
     * @param mixed $message
     * @param mixed $to      
     * @param mixed $cc      
     * @param mixed $bcc     
     * @param array $config - array with alternate config settings
     *
     * @return TODO
     */
    public function sendEmail($subject, $message, $to, $from = 'noreply@cgisolution.com', $fromName = 'CGI Solution', $cc = null, $bcc = null, $config = null, $mbox = null, $draft = false, $draftuid = 0) {
        // if no config params were defined for sending the message
        // will use localhost relay
        if (empty($config)) {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            // $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = false;
            $config['mailtype'] = 'html';
        }

        //elog($config);
        $this->ci->load->library('email', $config);
        
        $this->ci->email->from($from, $fromName);

        // Adds To

        $this->ci->email->to($to);

        // Adds CC
        $this->ci->email->cc($cc);

        // adds BCC
        $this->ci->email->bcc($bcc);
        
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
        if (!$draft)
        {
            $send = $this->ci->email->send();
            if (!$send) {
                throw new Exception($this->ci->email->print_debugger());
            }
        }
        echo $this->ci->email->print_debugger();
        return $this->ci->email->saveToFolder($mbox, (($draft) ? 'Drafts' : 'Sent'), $draftuid);
    }

    /**
     * Main function to check subscription statuses
     *
     * @return TODO
     */
    public function checkSubscriptionStatus($company) {
        if (empty($company))
            throw new Exception("Company ID is empty!");

        $this->ci->load->library('payment');

        // first checks if they are exempt
        $exempt = $this->ci->companies->getTableValue('billingExempt', $company);

        if ((bool) $exempt == false) {
            // checks subscription status
            $subscriptionID = $this->ci->companies->getTableValue('BrainTreeSubscriptionID', $company);

            if (empty($subscriptionID))
                return 'NO_SUBSCRIPTION_ID';

            // gets subscription info
            $subscription = $this->ci->payment->getSubscriptionByID($subscriptionID);

            $status = strtoupper($subscription->status);

            return $status;
        }
        else {
            return 'EXEMPT';
        }

        return 'NO_SUBSCRIPTION_FOUND';
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    private function clearCompanySubcriptionID($company) {
        if (empty($company))
            throw new Exception("Company ID is empty");

        $data = array('BrainTreeSubscriptionID' => null);

        $this->ci->db->where('id', $company);
        $this->ci->db->update('companies', $data);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function promoteLeadVersion() {
        if (in_array(strtolower($_SERVER['HTTP_HOST']), $this->ci->config->item('promoteleadURLs'))) {
            return true;
        }

        return false;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function checkCKFinderPath($company = 0) {
        if (empty($company))
            $company = $this->session->userdata('company');

        if (empty($company))
            throw new Exception("Company ID is empty!");

        $path = "public/uploads/ckfinder/{$company}/";

        // error_log($path);

        $this->createDir($path);

        return true;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $address 
     *
     * @return TODO
     */
    public function geoCodeAddress($address) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&sensor=true";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);

        $results = curl_exec($ch);

        if ($results === false)
            throw new Exception("Unable to Curl Address ({$url})! " . curl_error($ch));

        curl_close($ch);

        return $results;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $address 
     *
     * @return object
     */
    public function getAddressLatLng($address) {
        $results = $this->geoCodeAddress($address);

        $results = json_decode($results);

        return $results->results[0]->geometry->location;
    }

    /**
     * used for facebook callback url when user deauthorizes our app
     *
     * @param mixed $input 
     *
     * @return String
     */
    public function base64_url_decode($input) {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    public function dateDiff($start, $end, $type = 1) {
        $dStart = new DateTime();
        $dStart->setTimestamp($start);

        $dEnd = new DateTime();
        $dEnd->setTimestamp($end);

        $diff = date_diff($dStart, $dEnd);

        //$dDiff = $dStart->diff($dEnd);

        if ($type == 3)
            return $diff->m; // use for point out relation: smaller/greater
        if ($type == 3)
            return $diff->m; // use for point out relation: smaller/greater

        return $dDiff->days;
    }

}
