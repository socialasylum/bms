<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Territory extends Library
{
    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'territories';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    public function getTerritories ($company, $id = null)
    {
        if (empty($company)) throw new Exception('company ID is empty!');

        $ci =& get_instance();

        $mtag = "territories-{$company}-{$id}";

        $data = $ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $ci->db->from('territories');
            $ci->db->where('company', $company);

            if (!empty($id)) $ci->db->where('id', $id);

            $ci->db->order_by('name', 'asc');

            $query = $ci->db->get();

            $results = $query->result();

            if (empty($id)) $data = $results;
            else $data = $results[0];

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $territory 
     *
     * @return TODO
     */
    public function getMarkers ($territory)
    {
        if (empty($territory)) throw new Exception('Territory ID is empty!');

        $mtag = "terMarkers-{$territory}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('lat, lng, radius, color, opacity');
            $this->ci->db->from('territoryMarkers');
            $this->ci->db->where('territoryId', $territory);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }
}
