<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Companies extends Library
{

    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'companies';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getCompanies ($userid = 0)
    {
        $ci =& get_instance();
        $ci->load->driver('cache');

        // if (empty($userid)) $userid = $ci->session->userdata('userid');

        // if (empty($userid)) throw new Exception("userid is empty!");

        // $admin = $this->ci->users->isAdmin($userid);

        $data = $ci->cache->memcached->get('companies-' . $userid . '-' . $this->ci->session->userdata('userid'));

        if (empty($data))
        {
            $ci->db->select('companies.*, userCompanies.homeCompany');
            $ci->db->from('userCompanies');
            $ci->db->join('companies', 'userCompanies.company = companies.id', 'left');

            if (empty($userid)) $ci->db->where('userid', $this->ci->session->userdata('userid'));
            else $ci->db->where_in('userid', array($userid, $this->ci->session->userdata('userid')));

            $ci->db->where('companies.active', 1);
            $ci->db->where('companies.deleted', 0);
            $ci->db->order_by('companyName', 'asc');

            $query = $ci->db->get();

            $data = $query->result();

            $ci->cache->memcached->save('companies', $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets a list of all companies. For admin use only
     *
     * @return array->object
     */
    public function getAllCompanies ()
    {
        if ($this->ci->session->userdata('admin') !== true) throw new Exception("Must be an admin to execute this function");

        $mtag = "allComps";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->ci->db->from('companies');
            $this->ci->db->order_by('companyName', 'asc');
            $this->ci->db->where('deleted', 0);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getCompanyName ($id)
    {
        $ci =& get_instance();

        $ci->load->driver('cache');

        if (empty($id)) throw new Exception('ID is empty!');

        $data = $ci->cache->memcached->get('companyName' . $id);

        if (empty($data))
        {
            $ci->db->select('companyName');
            $ci->db->from('companies');
            $ci->db->where('id', $id);

            $query = $ci->db->get();

            $results = $query->result();

            $data = $results[0]->companyName;

            $ci->cache->memcached->save('companyName' . $id, $data, 3600);
        }

        return $data;
    }

    /**
     * Checks if user is assigned to a company
     *
     * @param mixed $userid 
     *
     * @return boolean - true if they are assigned
     */
    public function checkUserAssigned ($userid, $company)
    {

        $mtag = "userCompAssigned-{$userid}-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->ci->db->from('userCompanies');
            $this->ci->db->where('userid', $userid);
            $this->ci->db->where('company', $company);

            $data = $this->ci->db->count_all_results();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function getHomeLocation ($user, $homeOnly = false)
    {
        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        $mtag = "userHomeLoc-{$user}-" . (int) $homeOnly;

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('company');
            $this->ci->db->from('userCompanies');
            $this->ci->db->where('userid', $user);

            if ($homeOnly == true) $this->ci->db->where('homeCompany', 1);

            $this->ci->db->order_by('homeCompany', 'desc');
            $this->ci->db->limit(1);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->company;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * checks if a user is assigned to a company
     *
     * @param mixed $userid  
     * @param mixed $company 
     *
     * @return boolean - True if assigned to that company
     */
    public function userAssignedToCompany ($user, $company)
    {
        $user = intval($user);
        $company = intval($company);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");

        $assignedUsers = $this->assignedUsers($company);

        if (!empty($assignedUsers))
        {
            foreach ($assignedUsers as $r)
            {
                if ((int) $r->userid == (int) $user)
                {
                    return true;
                }
            }
        }


        return false;
    }

    /**
     * Gets all userID's assigned to a company
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function assignedUsers ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "compAssignedUsers-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('userid, homeCompany');
            $this->ci->db->from('userCompanies');
            $this->ci->db->where('company', $company);
            $this->ci->db->order_by('userid');

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Checks if a compnay is set as a users home company
     *
     * @param mixed $userid   
     * @param mixed $location 
     *
     * @return boolean - True if location is set to home company
     */
    public function checkHomeCompany ($userid, $company)
    {
        if (empty($userid)) throw new Exception("Userid is empty!");
        if (empty($company)) throw new Exception("company ID is empty!");

        $mtag = "userHomeComp-{$userid}-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->from('userCompanies');
            $this->ci->db->where('userid', $userid);
            $this->ci->db->where('company', $company);
            $this->ci->db->where('homeCompany', 1);

            $data = $this->ci->db->count_all_results();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if ($data > 0) return true;

        return false;
    }

    /**
     * Gets all the all companies user is assigned to
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function getUserAssignedCompanies ($user)
    {
        if (empty($user)) $user = $this->session->userdata('userid');

        $user = intval($user);

        if (empty($user)) throw new Exception("User ID is empty!");

        $mtag = "userAssignComp-{$user}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('company');
            $this->ci->db->from("userCompanies");
            $this->ci->db->where('userid', $user);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    public function getCompanyLogo ($id)
    {
        $ci =& get_instance();

        $ci->load->driver('cache');

        if (empty($id)) throw new Exception('ID is empty!');

        $data = $ci->cache->memcached->get('companyName' . $id);

        if (empty($data))
        {
            $ci->db->select('logo');
            $ci->db->from('companies');
            $ci->db->where('id', $id);

            $query = $ci->db->get();

            $results = $query->result();

            $data = $results[0]->logo;

            $ci->cache->memcached->save('companyLogo' . $id, $data, 3600);
        }

        return $data;
    }


    public function getDomains ($company = 0)
    {
        //$company = intval($company);

        //if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "getDomains-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
        	$this->ci->db->select('id, company, domain');
            $this->ci->db->from('companyDomains');
            if (!empty($company)) $this->ci->db->where('company', $company);
			$this->ci->db->order_by('domain');

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;	    
    }
    
    /**
    * finds a company based upon domain
    */
    public function getCompanyByHost ()
    {
		$company = 0;
		
		$domain = strtoupper($_SERVER['HTTP_HOST']);
		
		$domains = $this->getDomains();
		
		if (!empty($domains))
		{
			foreach ($domains as $r)
			{
				if (strpos($domain, strtoupper($r->domain)) !== false)
				{
					$company = $r->company;
					break;	
				}
			}
		}
		
		if (empty($company)) return false;
		
		return $company;
    }
    
    
    /**
    * Will determine which company based on the domain the user is on
    */
    public function getHostIndex ()
    {
    	$company = $this->getCompanyByHost();
		
		if ($company === false) return $this->ci->config->item('defaultIndexUrl');
		
		$indexUrl = $this->getTableValue('indexUrl', $company);
		
		if (empty($indexUrl)) return $this->ci->config->item('defaultIndexUrl');
		
		return $indexUrl;
    }
	
	/**
	* will find the companies default landing URL should it be changed
	*/
	public function landingUrl ($company)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
		
		$landingUrl = $this->getTableValue('landingUrl', $company);
		
		if (empty($landingUrl)) return $this->ci->config->item('defaultLandingUrl');

		return $landingUrl;		
	}

	/**
	* pulls just settings from DB about the company
	*/
	public function getSettings ($company)
	{
		$company = intval($company);
		
		if (empty($company)) throw new Exception("Company ID is empty!");
        $mtag = "getDomains-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
        	$this->ci->db->select('active, newhireDays, deleted, users, billingExempt, allowReg, allowFBreg, landingUrl, indexUrl, fbSync, ytUpload');
            $this->ci->db->from('companies');
            $this->ci->db->where('id', $company);
			
            $query = $this->ci->db->get();

            $results = $query->result();
			
			$data = $results[0];

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;	  
	}
}
