<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Downline
{
    public function __construct()
    {
        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    public function userDownline ($userid)
    {
        if (empty($userid)) throw new Exception('userid is empty!');

        $ci =& get_instance();

        $userCompany = $this->getUserCompany($userid);

        $downline = array();

        $cp = $this->getChildPositions($userCompany);

        if (!empty($cp))
        {
            foreach ($cp as $r)
            {
                $downline[] = (int) $r->childPosition;
            }
        }

        $ci->db->select('id');
        $ci->db->select('firstName');
        $ci->db->select('lastName');
        $ci->db->from('users');
        $ci->db->where_in('position', $downline);

        $query = $ci->db->get();

        $results = $query->result();

        return $results;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $position 
     *
     * @return TODO
     */
    private function getChildPositions($position)
    {

        if (empty($position)) throw new Exception('position id is empty!');

        $ci =& get_instance();

        $ci->db->select('childPosition');
        $ci->db->from('childPositions');
        $ci->db->where('position', $userid);

        $query = $ci->db->get();

        $results = $query->result();

        return $results;
    }

    /**
     * Gets the ID of the company the user is assigned to
     *
     * @param mixed $userid 
     *
     * @return TODO
     */
    private function getUserCompany ($userid)
    {
        if (empty($userid)) throw new Exception('userid is empty!');

        $ci =& get_instance();

        $ci->load->library('session');

        if ($ci->session->userdata('logged_in') == true AND $ci->session->userdata('userid') == $userid)
        {
            return (int) $ci->session->userdata('company');
        }

        $ci->db->select('company');
        $ci->db->from('users');
        $ci->db->where('id', $userid);

        $query = $ci->db->get();

        $results = $query->result();

        if (empty($results)) return false;

    return (int) $results[0]->company;
    }
}
