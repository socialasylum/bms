<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once $_SERVER['DOCUMENT_ROOT'] . 'braintree_php-2.23.1' . DS . 'lib' . DS . 'Braintree.php';
/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class Payment
{

    public $errorReason;

    private $ci, $email;

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->library('session');

        $this->email = $this->ci->session->userdata('email');

        // $this->ci->load->helper('url');

        if ($this->ci->config->item('live') == true)
        {
            Braintree_Configuration::environment('production');
            Braintree_Configuration::merchantId($this->ci->config->item('merchantId'));
            Braintree_Configuration::publicKey($this->ci->config->item('publicKey'));
            Braintree_Configuration::privateKey($this->ci->config->item('privateKey'));
        }
        else
        {
            // sent environment
            Braintree_Configuration::environment('sandbox');
            Braintree_Configuration::merchantId($this->ci->config->item('merchantId'));
            Braintree_Configuration::publicKey($this->ci->config->item('publicKey'));
            Braintree_Configuration::privateKey($this->ci->config->item('privateKey'));
        }

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * Process a transaction
     *
     * @return TODO
     */
    public function processTransaction ($p)
    {
        if (empty($p['amount'])) throw new Exception("Amount for transaction is empty!");

        $saleArray = array(
                'amount' => $p['amount'],
                'creditCard' => array
                    (
                        'number' => $p['number'],
                        'cvv' => $p['cvv'],
                        'expirationMonth' => $p['expMonth'],
                        'expirationYear' => $p['expYear']
                    ),
                'options' => array
                    (
                        'submitForSettlement' => true
                    )
            );


        $result = Braintree_Transaction::sale($saleArray);

        if ($result->success)
        {
            error_log("Success Transaction ID:" . $result->transaction->id);
            return $result->transaction->id;
        }
        else if ($result->transaction)
        {
            error_log("ERROR:" . $result->message);
            error_log("code: " . $result->transaction->processorResponseCode);
        }
        else
        {
            error_log("Validation Errors");

            foreach ($results->errors->deepAll() as $error)
            {
                error_log($error->message);
            }
        }

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $d 
     *
     * @return TODO
     */
    public function createCustomer($d, $saveCompanyData = true)
    {
        if (empty($d['customerID'])) throw new Exception("Customer ID is empty!");
        if (empty($d['firstName'])) throw new Exception("Billing First Name is empty!");
        if (empty($d['lastName'])) throw new Exception("Billing Last Name is empty!");

        $customerArray = array(
            'id' => $d['customerID'],
            'firstName' => $d['firstName'],
            'lastName' => $d['lastName']
        );

        if (!empty($d['companyName'])) $customerArray['company'] = $d['companyName'];
        if (!empty($d['email'])) $customerArray['email'] = $d['email'];
        if (!empty($d['phone'])) $customerArray['phone'] = $d['phone'];
        if (!empty($d['fax'])) $customerArray['fax'] = $d['fax'];
        if (!empty($d['websiteUrl'])) $customerArray['website'] = $d['websiteUrl'];

        $result = Braintree_Customer::create($customerArray);

        if ($result->success)
        {
            // will now update companies table with its BrainTreeCustomerID
            // Copmany ID and BrainTreeCustomerID should usually match
            if($saveCompanyData == true) $this->updateCompanyCustomerID ($d['customerID'], $result->customer->id);

            // error_log('BTcustomer:' . $result->customer->id);
            return $result->customer->id;
        }
        else
        {
            // error_log('FAILED');
            // foreach($result->errors->deepAll() AS $error) {
                // error_log($error->code . ": " . $error->message . "\n");
                    // }
        }

        return false;
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createSubscription ($company, $token, $planId, $price = 0, $saveCompanyData = true, $returnID = false)
    {
        if (empty($token)) throw new Exception("Payment token is empty!");
        if (empty($planId)) throw new Exception("Subscription Plan ID is empty!<br><br><strong>IT has beeen notified!</strong>");
        // if (empty($price)) throw new Exception("Unable to process a subscription of $0.00!");

        error_log("*****PlanID: {$planId}");

        $subscriptionArray = array
            (
                'paymentMethodToken' => $token,
                'planId' => "{$planId}"
            );

        if (!empty($price)) $subscriptionArray['price'] = $price;

        $result = Braintree_Subscription::create($subscriptionArray);

        if ($result->success)
        {

            $subscription = $result->subscription;
            
            // $subID = $subscription->id;
            // $subID = $result->subscription->id;

            error_log("*****SUBID: {$result->subscription->id}");

            $status = strtoupper($subscription->status);

            // error_log($subID);
            // saves subscription
            if($saveCompanyData == true) $this->updateCompanySubscriptionID($company, $result->subscription->id, $price);

            // BrainTree_Transaction::PROCESSOR_DECLINE
            // BrainTree_Transaction::ACTIVE
            // BrainTree_Transaction::PENDING

            if ($returnID == true) return $result->subscription->id;
            else return $status;
        }
        else
        {
            error_log("Subscription Creation Errors");

            foreach ($result->errors->deepAll() as $error)
            {
                error_log($error->message);
            }

            return false;
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     * @param mixed $token  - Credit Card Token
     * @param mixed $planId  
     * @param mixed $price   
     *
     * @return TODO
     */
    public function updateSubscription ($subID, $token, $planId, $price)
    {
        if (empty($subID)) throw new Exception("Subscription is empty!");
        if (empty($token)) throw new Exception("Payment token is empty!");
        if (empty($planId)) throw new Exception("Subscription Plan ID is empty!<br><br><strong>IT has beeen notified!</strong>");
        if (empty($price)) throw new Exception("Unable to have a subscription of $0.00!");

        $updateArray = array
            (
                'paymentMethodToken' => $token,
                'price' => $price,
                'planId' => $planId
            );

        $result = Braintree_Subscription::update($subID, $updateArray);


        if ($result->success)
        {
            return true;
        }

        return false;
    }


    /**
     * Main function used to cancel a subscription
     *
     * @param mixed $subscriptionID 
     *
     * @return object
     */
    public function cancelSubscription ($subscriptionID)
    {
        if (empty($subscriptionID)) throw new Exception("Subscription ID is empty!");

        $result = Braintree_Subscription::cancel($subscriptionID);

        return $result;
    }


    /**
     * TODO: short description.
     *
     * @param array $d
     *
     * @return TODO
     */
    public function createCreditCard ($d, $saveCompanyData = true, $default = false)
    {
        if ($saveCompanyData == true && (empty($d['company']))) throw new Exception("Company ID is empty!");
        if (empty($d['BTcustomerID'])) throw new Exception("BT Customer ID is empty!");

        $d['ccNumber'] = $this->ci->functions->onlyNumbers($d['ccNumber']);

        if(!empty($default)) $default = true;

        $array = array
            (
                'customerId' => $d['BTcustomerID'],
                'cardholderName' => $d['ccName'],
                'cvv' => $d['ccCvv'],
                'number' => $d['ccNumber'],
                'expirationDate' => $d['ccExpM'] . '/' . $d['ccExpY'], // 05/12
                'options' => array
                (
                    'verifyCard' => true
                )
            );

        if ($default == true)
        {
            $array['optons']['makeDefault'] = true;
        }


        // error_log("Create CC Array: " . print_r($array, true));

        $result = Braintree_CreditCard::create($array);

        // error_log('Results: ' . print_r($result, true));

        if ($result->success)
        {
            // will save CC token
            if($saveCompanyData == true) $this->saveCreditCardToken($d['company'], $d['ccNumber'], $result->creditCard->token);

            // error_log('token:' . $result->creditCard->token);

            return $result->creditCard->token;
        }
        else
        {
            // error_log('FAILED');
            $verification = $result->creditCardVerification;

            error_log('STATUS: ' . print_r($verification->status, true));
            // "gateway_rejected`"
            error_log('Gateway Rejection: ' .$verification->gatewayRejectionReason);
            return false;
        }

        return false;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $subscriptionID 
     *
     * @return TODO
     */
    public function getSubscriptionByID ($subscriptionID)
    {
        if (empty($subscriptionID)) throw new Exception("Braintree Subscription ID is empty");

        $subscription = Braintree_Subscription::find($subscriptionID);

        return $subscription;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company             
     * @param mixed $BrainTreeCustomerID 
     *
     * @return TODO
     */
    private function updateCompanyCustomerID ($company, $BrainTreeCustomerID)
    {
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($BrainTreeCustomerID)) throw new Exception("Brain Tree customer ID is empty!");

        $data = array('BrainTreeCustomerID' => $BrainTreeCustomerID);

        $this->ci->db->where('id', $company);
        $this->ci->db->update('companies', $data);

        return true;
    }

    private function updateCompanySubscriptionID ($company, $subscriptionID, $price = 0)
    {
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($subscriptionID)) throw new Exception("Brain Tree subscription ID is empty!");

        $data = array
            (
                'BrainTreeSubscriptionID' => $subscriptionID,
                'subscriptionPrice' => $price
            );

        $this->ci->db->where('id', $company);
        $this->ci->db->update('companies', $data);

        return true;
    }


    /**
     * TODO: short description.
     *
     * @param mixed $company 
     * @param mixed $number  
     * @param mixed $token   
     *
     * @return TODO
     */
    private function saveCreditCardToken ($customerID, $number, $token)
    {
        if (empty($customerID)) throw new Exception("Customer ID is empty!");
        if (empty($number)) throw new Exception("Card number is empty!");
        if (empty($token)) throw new Exception("BrainTree Credit Card Token is empty!");


        $cardType = (int) substr($number, 0, 1);
        $lastFour = (int) substr($number, -4);

        if (empty($cardType)) throw new Exception("Card Type is empty!");
        if (empty($lastFour)) throw new Exception("last four digits of card is empty!");

        $data = array
            (
                'company' => $customerID,
                'cardType' => $cardType,
                'lastFour' => $lastFour,
                'token' => $token,
            );

        $this->ci->db->insert('companyBilling', $data);

        return $this->ci->db->insert_id();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getCardTokenByID ($id)
    {
        if (empty($id)) throw new Exception("Card ID for token is empty!");

        $mtag = "cardTokenByID-{$id}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('token');
            $this->ci->db->from('companyBilling');
            $this->ci->db->where('id', $id);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->token;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));

        }

        return $data;
    }

    public function getPlans()
    {
        $plans = Braintree_Plan::all();

        return $plans;
    }


    public function getAddOns()
    {
        $addOns = Braintree_AddOn::all();

        return $addOns;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $customerID 
     *
     * @return TODO
     */
    public function getTransactions ($customerID)
    {
        $searchArray = array
            (
                Braintree_TransactionSearch::customerId()->is($customerID)
            );

        $collection = Braintree_Transaction::search($searchArray);

        return $collection;
    }

    public function getCreditCards($token)
    {
        $creditCard = Braintree_CreditCard::find($token);

        return $creditCard;
    }

    public function getTransactionInfo($tranid)
    {
        $traninfo = Braintree_Transaction::find($tranid);

        return $traninfo;
    }

    public function getCustomerInfo($customerid)
    {
        $customer = Braintree_Customer::find($customerid);

        return $customer;
    }

    public function deleteCCfromBT($token)
    {
        if (empty($token)) throw new Exception("Card ID is missing!");
        
        Braintree_CreditCard::delete($token);

        return 'SUCCESS';
    }

    public function checkBTforCC($token)
    {
        if (empty($token)) throw new Exception("Card ID is missing!");

        $creditCard = Braintree_CreditCard::find($token);

        return $creditCard;
    }

    public function updateDefaultCC($token)
    {
        $updateResult = Braintree_CreditCard::update($token,
            array(
                'options' => array(
                    'makeDefault' => true
                )
            )
        );
    }
}
