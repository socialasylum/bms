<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Knowledgebase extends Library
{
    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'kbArticles';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * Gets the Article ID's assigned to a folder
     * from the kbArticleFolderAssign tbl
     *
     * @param mixed $folder Optional, defaults to 0. 
     *
     * @return array
     */
    public function getTreeFiles ($folder = 0)
    {
        $mtag = "kbTreeFiles-{$folder}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('articleId');
            $this->ci->db->from('kbArticleFolderAssign');
            $this->ci->db->where('folderId', $folder);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        $return = array();

        foreach ($data as $r)
        {
            $return[] = $r->articleId;
        }

        return $return;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article 
     *
     * @return int - company ID (possibly 0)
     */
    public function getArticleCompany ($article)
    {
        if (empty($article)) throw new Exception("Article ID is empty!");

        $mtag = "kbArticleCompany-{$article}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('company');
            $this->ci->db->from('kbArticles');
            $this->ci->db->where('id', $article);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->company;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $article 
     *
     * @return TODO
     */
    public function isGlobalArticle ($article)
    {
        if (empty($article)) throw new Exception("Article ID is empty!");

        $mtag = "kbArticleGlobalArticle-{$article}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('globalArticle');
            $this->ci->db->from('kbArticles');
            $this->ci->db->where('id', $article);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->globalArticle;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (!empty($data)) return true;

        return false;
    }
}
