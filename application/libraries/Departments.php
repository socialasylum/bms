<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Departments extends Library
{
    public function __construct()
    {
        parent::__construct();
        $this->tableName = 'departments';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getDepartments ($company = 0)
    {
        $ci =& get_instance();

        if (empty($company)) $company = $ci->session->userdata('company');

        $ci->load->driver('cache');

        $mtag = "departments{$company}";
        $data = $ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $ci->db->from('departments');
            $ci->db->where('company', $company);

            $query = $ci->db->get();

            $data = $query->result();

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }




    public function getName ($id)
    {
        $ci =& get_instance();

        $ci->load->driver('cache');

        $mtag = "depName{$id}";

        $data = $ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $ci->db->select('name');
            $ci->db->from('departments');
            $ci->db->where('id', $id);

            $query = $ci->db->get();

            $results = $query->result();

            $data =  $results[0]->name;

            $ci->cache->memcached->save($mtag, $data, $ci->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * checks if a user is assigned to a department
     *
     * @param mixed $userid  
     * @param mixed $company 
     *
     * @return boolean - True if assigned to that department for that company
     */
    public function userAssignedToDepartment ($user, $company, $department)
    {
        $user = intval($user);
        $company = intval($company);
        $department = intval($department);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($department)) throw new Exception("Department ID is empty!");

        $assignedUsers = $this->assignedUsers($company);

        if (!empty($assignedUsers))
        {
            foreach ($assignedUsers as $r)
            {
                if ((int) $r->userid == (int) $user)
                {
                    return true;
                }
            }
        }


        return false;
    }


    /**
     * checks if a user is assigned to a company
     *
     * @param mixed $userid  
     * @param mixed $company 
     *
     * @return boolean - True if assigned to that department for that company
     */
    public function userAssignedToCompany ($user, $company, $department)
    {
        $user = intval($user);
        $company = intval($company);
        $department = intval($department);

        if (empty($user)) throw new Exception("User ID is empty!");
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($department)) throw new Exception("Department ID is empty!");

        $assignedUsers = $this->assignedUsers($company, $department);

        if (!empty($assignedUsers))
        {
            foreach ($assignedUsers as $r)
            {
                if ((int) $r->userid == (int) $user)
                {
                    return true;
                }
            }
        }


        return false;
    }

    /**
     * Gets all userID's assigned to a company department
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function assignedUsers ($company, $department)
    {
        $company = intval($company);
        $department = intval($department);

        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($department)) throw new Exception("department ID is empty!");

        $mtag = "compAssignedUserDepartments-{$company}-{$department}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('userid');
            $this->ci->db->from('userCompanyDepartments');
            $this->ci->db->where('company', $company);
            $this->db->where('department', $department);
            $this->ci->db->order_by('userid');

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }


    public function getDefaultDepartment ($company)
    {
        $company = intval($company);

        if (empty($company)) throw new Exception("company is empty!");

        $mtag = "defaultDepartment-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->ci->db->select('id');
            $this->ci->db->from('departments');
            $this->ci->db->where('company', $company);
            $this->ci->db->where('default', 1);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }



}
