<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'Library.php';

class Msgs extends Library {

    //public $htmlmsg, $plainmsg, $charset, $attachments;

    public function __construct() {
        parent::__construct();

        $this->tableName = 'messages';

        // if connected to DB
        if (class_exists('CI_DB')) {
            
        }
    }

    /**
     * Checks if user was sent a message
     * this is used so the user cannot access messages not sent to them simply by
     * entering random ID'
     *
     * @param INT $userid - users ID
     * @param INT $msgId - message ID
     *
     * @return boolean - true if has access
     */
    public function checkUserAssignedToMessage($userid, $msgId) {
        if (empty($userid))
            throw new Exception("userid is empty!");
        if (empty($msgId))
            throw new Exception("msg id is empty!");

        $mtag = "userMessageCheck{$userid}{$msgId}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->ci->db->from('messageTo');
            $this->ci->db->where('msgId', $msgId);
            $this->ci->db->where('to', $userid);

            $data = $this->ci->db->count_all_results();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if ($data > 0)
            return true;


        // checks if the message was sent from the user
        // if so will return true

        $fromUserId = $this->getTableValue('from', $msgId);

        if ((int) $fromUserId == (int) $userid)
            return true;

        return false;
    }

    /**
     * Gets a users message signature for a company
     *
     * @param mixed $user    
     * @param mixed $company 
     *
     * @return object
     */
    public function getSignature($user = 0, $company = 0) {
        if (empty($user))
            $user = $this->ci->session->userdata('userid');
        if (empty($company))
            $company = $this->ci->session->userdata('company');

        if (empty($user))
            throw new Exception("User ID is empty!");
        if (empty($company))
            throw new Exception("Company ID is empty!");

        $mtag = "userSig-{$user}-{$company}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data)) {

            $this->ci->db->select('id, signature');
            $this->ci->db->from('userSigs');
            $this->ci->db->where('userid', $user);
            $this->ci->db->where('company', $company);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    public function openAccount($url, $username, $passwd, $timeout = 5, $test = false) {
        // sets open timeout
        imap_timeout(IMAP_OPENTIMEOUT, $timeout);

        $flags = 0;

        if ($test)
            $flags = OP_READONLY + OP_DEBUG;

        $mbox = imap_open($url, $username, $passwd, $flags);

        if ($mbox === false)
            throw new Exception("Unable to open account! <p><b>URL:</b> $url</p><p><b>Username:</b> {$username}<p>" . imap_last_error());

        return $mbox;
    }

    public function buildImapUrl($server, $port, $type, $ssl, $auth, $folder = null) {

        $url = "{{$server}:{$port}";

        if ($ssl == 2)
            $url .= '/ssl';

        if ($auth == 2)
            $url .= '/secure/validate-cert';
        if ($auth == 1)
            $url .= '/novalidate-cert';

        if ($type == 1)
            $url .= '/pop3';
        elseif ($type == 2)
            $url .= '/imap';

        $url .= '}' . $folder;

        return $url;
    }
    
    public function getBaseIMAPUrl($server, $folder = null)
    {
        return "{{$server}}";
    }

    /**
     * opens and connects to an Imap account
     */
    public function accountConnect($account, $folder = null) {
        if (empty($account))
        {
            throw new Exception("Account ID is empty!");
        }
        $obj = new StdClass();
        $info = $this->getAccountInfo($account);
        $url = $this->buildImapUrl($info->mailbox, $info->port, $info->type, $info->ssl, $info->auth, $folder);
        $baseUrl = $this->getBaseIMAPUrl($info->mailbox);
        $mbox = $this->openAccount($url, $info->emailAddress, $this->ci->encrypt->decode($info->passwd));
        $obj->url = $url;
        $obj->baseUrl = $baseUrl;
        $obj->mbox = $mbox;
        return $obj;
    }

    // returns an accounts decrypted password
    public function getAccountPassword($userid, $account) {
        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($account))
            throw new Exception('Account ID is empty!');

        $mtag = "getAccountPassword-{$userid}-{$account}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data)) {
            $this->ci->db->select('passwd');
            $this->ci->db->from('msgAccounts');
            $this->ci->db->where('userid', $userid);
            $this->ci->db->where('id', $account);

            $data = $this->ci->db->get()->result()[0]->passwd;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        $decryptPassword = $this->ci->encrypt->decode($data);

        return $decryptPassword;
    }
    
    /*
     * Replacing getMsgFolders functions with one using IMAP instead of DB
     */
    
    public function getMsgFolders($mbox, $mbox_url)
    {
        $folders = array();
        foreach (imap_list($mbox, $mbox_url, '*') as $folder)
        {
            $folder_arr = explode('}', $folder);
            $folders[] = $folder_arr[1];
        }
        if (!in_array('Trash', $folders))
        {
            imap_createmailbox($mbox, $mbox_url . 'Trash');
        }
        if (!in_array('Junk', $folders))
        {
            imap_createmailbox($mbox, $mbox_url . 'Junk');
        }
        $mainFolders = array(
            'Drafts',
            'INBOX',
            'Junk',
            'Sent',
            'Trash'
        );
        $folders = array();
        foreach (imap_list($mbox, $mbox_url, '*') as $folder)
        {
            $folder_arr = explode('}', $folder);
            if (!in_array($folder_arr[1], $mainFolders))
            {
                $folders[] = $folder_arr[1];
            }
        }
        sort($folders, SORT_NATURAL | SORT_FLAG_CASE);
        return array_merge($mainFolders, $folders);
    }
    
    /*
     * Rewriting getEmail to use IMAP instead of DB
     */
    
    public function getEmail($mbox, $id)
    {
        $obj = new StdClass();
        $body = null;
        $s = imap_fetchstructure($mbox, $id);
        $obj = new StdClass();
        if (empty($s->parts))
        {
            $partObj = $this->getEmailBody($mbox, $id, $s);
            $obj->plain .= nl2br($partObj->plain);
            $obj->html .= $partObj->html;
        }
        else
        {
            foreach ($s->parts as $key => $part)
            {
                $n = $key + 1;
                $partObj = $this->getEmailBody($mbox, $id, $part, $n);
                $obj->plain .= $partObj->plain;
                $obj->html .= $partObj->html;
            }
            $obj->plain = nl2br($obj->plain);
        }
        $obj->header = imap_headerinfo($conn->mbox, $id);
        return $obj;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getEmailBody($mbox, $id, $part, $n = 0) {
        $obj = new StdClass();

        $obj->plain = null;
        $obj->html = null;

        $msg = null;
        #error_log(print_r($part, true));
        if (empty($mbox))
            throw new Exception("Mailbox was not specified!");
        if (empty($id))
            throw new Exception("Msg No. was not specified");

        #error_log("Part: {$n} | Subtype: {$part->subtype}");
        #$s = imap_fetchstructure($mbox, $part);

        $data = (empty($n)) ? imap_body($mbox, $id) : imap_fetchbody($mbox, $id, $n);

        $logMsg = "Part: {$n} | Encoding: {$part->encoding} | Subtype: {$part->subtype}";



        //if ($part->encoding == 4) $data = quoted_printable_decode($data);
        //elseif ($part->encoding == 3) $data = base64_decode($data);

        if ($part->encoding == 4)
            $data = quoted_printable_decode($data);
        elseif ($part->encoding == 3)
            $data = base64_decode($data);
        elseif ($part->encoding == 1)
            $data = quoted_printable_decode($data);
        else
            $data = imap_qprint($data);

        if (!empty($data))
            $logMsg .= " Data: " . print_r($data, true);


        if (empty($part->parts) && empty($part->type)) {
            #error_log("CONTENT. " . __line__);
            if (strtoupper($part->subtype) == 'PLAIN' && !empty($data))
                $obj->plain .= $data;

            if (strtoupper($part->subtype) == 'HTML' && !empty($data))
                $obj->html .= $data;

            #print_r($obj->html);
        }
        else {
            foreach ($part->parts as $subKey => $subPart) {
                $subN = "{$n}." . ($subKey + 1);
                #error_log("subN: {$subN}");
                $sub = $this->getEmailBody($mbox, $id, $subPart, $subN);

                $obj->plain .= $sub->plain;
                $obj->html .= $sub->html;
            }
        }

        //error_log($obj->html);

        return $obj;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getAdditionalAccounts($user = 0, $type = 0, $cnt = false) {
        if (empty($user))
            throw new Exception("User ID is empty!");

        $mtag = "msgAccountInfo-{$user}-{$type}-{$cnt}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data) {
            $this->ci->db->from('msgAccounts');
            $this->ci->db->where('userid', $user);


            if (!empty($type)) {
                if (is_array($type))
                    $this->ci->db->where_in('type', $type);
                else
                    $this->ci->db->where('type', $type);
            }

            if ($cnt)
                $data = $this->ci->db->count_all_results();
            else {
                $query = $this->ci->db->get();

                $data = $query->result();
            }

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    // returns accounts that do not have passwords saved
    public function getAccountsWithNoPasswd($userid) {
        $mtag = "getAccountsWithNoPasswd-{$userid}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data) {

            $data = array();

            $this->ci->db->select('id');
            $this->ci->db->from('msgAccounts');
            $this->ci->db->where('userid', $userid);
            $this->ci->db->where('passwd', '');

            $results = $this->ci->db->get()->result();

            if (!empty($results)) {
                foreach ($results as $r) {
                    $data[] = $r->id;
                }
            }

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }


        return $data;
    }

    /**
     * Gets info about additional external account
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getAccountInfo($id) {

        if (empty($id))
            throw new Exception("Account ID is empty!");

        $mtag = "msgAccountInfo-{$id}";

        $data = $this->ci->cache->memcached->get($mtag);
        $data = false;
        if (!$data) {
            $this->ci->db->from('msgAccounts');
            $this->ci->db->where('userid', $id);
            $query = $this->ci->db->get();
            $results = $query->result();
            $data = $results[0];
            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }
    
    /*
     * Grab outgoing account info
     */
    
    public function getOutgoingAccountInfo($id) {

        if (empty($id))
            throw new Exception("Account ID is empty!");

        $mtag = "msgOutgoingAccountInfo-{$id}";

        $data = $this->ci->cache->memcached->get($mtag);
        $data = false;
        if (!$data) {
            $this->ci->db->from('msgAccounts');
            $this->ci->db->where('id', $id);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function updateAdditionalAccount($p) {
        if (empty($p['id']))
            throw new Exception("Account ID is empty!");

        $data = array();

        if (isset($p['type']))
            $data['type'] = $p['type'];
        if (isset($p['mailbox']))
            $data['mailbox'] = $p['mailbox'];
        if (isset($p['port']))
            $data['port'] = $p['port'];
        if (isset($p['username']))
            $data['username'] = $p['username'];
        if (isset($p['alias']))
            $data['alias'] = $p['alias'];
        if (isset($p['outgoingServer']))
            $data['outgoingServer'] = $p['outgoingServer'];
        if (isset($p['yourname']))
            $data['yourname'] = $p['yourname'];
        if (isset($p['emailAddress']))
            $data['emailAddress'] = $p['emailAddress'];
        if (isset($p['ssl']))
            $data['ssl'] = $p['ssl'];
        if (isset($p['auth']))
            $data['auth'] = $p['auth'];

        if ((bool) $p['remPasswd'] == true && !empty($p['passwd']))
            $data['passwd'] = $this->ci->encrypt->encode($p['passwd']);

        if (empty($data))
            return false;

        $this->ci->db->where('id', $p['id']);
        $this->ci->db->update('msgAccounts', $data);

        return $p['id'];
    }

    /**
     * TODO: short description.
     *
     * @param mixed $p 
     *
     * @return TODO
     */
    public function insertAdditionalAccount($p) {
        if (empty($p['userid']))
            throw new Exception("User ID is empty!");

        $data = array
            (
            'datestamp' => DATESTAMP,
            'userid' => $p['userid'],
            'type' => $p['type'],
            'mailbox' => $p['mailbox'],
            'port' => $p['port'],
            'username' => $p['username'],
            'alias' => $p['alias'],
            'outgoingServer' => $p['smtpServer'],
            'yourname' => $p['yourname'],
            'emailAddress' => $p['emailAddress'],
            'ssl' => $p['ssl'],
            'auth' => $p['auth'],
            'passwd' => $p['passwd']
        );

        if (!empty($p['outgoingServer']))
            $data['outgoingServer'] = $p['outgoingServer'];

        if ((bool) $p['remPasswd'] == true)
            $data['passwd'] = $this->ci->encrypt->encode($p['passwd']);

        $this->ci->db->insert('msgAccounts', $data);

        return $this->ci->db->insert_id();
    }

    /**
     * gets message to data
     */
    public function msgTo($userid, $msg) {
        if (empty($userid))
            throw new Exception("User ID is empty!");
        if (empty($msg))
            throw new Exception("Message ID is empty!");

        $mtag = "msgTo-{$userid}-{$msg}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data) {
            $this->ci->db->from('messageTo');
            $this->ci->db->where('to', $userid);
            $this->ci->db->where('msgId', $msg);

            $query = $this->ci->db->get();

            $data = $query->result();

            $data = $data[0];

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    public function getpart($mbox, $id, $part, $key, $html = '', $plain = '', $attachments = array()) {
        $obj = new StdClass();

        $key = (string) $key;

        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
        // DECODE DATA ? multipart : simple
        $data = (!empty($key)) ? imap_fetchbody($mbox, $id, $key) : imap_body($mbox, $id);

        //if ((double) $key >= 1) if (!empty($data)) error_log(print_r($data, true));
        // Any part may be encoded, even plain text messages, so check everything.
        if ($part->encoding == 4)
            $data = quoted_printable_decode($data);
        elseif ($part->encoding == 3)
            $data = base64_decode($data);

        // PARAMETERS
        // get all parameters, like charset, filenames of attachments, etc.
        $params = array();

        if ($part->parameters) {
            foreach ($part->parameters as $x) {
                $params[strtolower($x->attribute)] = $x->value;
            }
        }

        if ($part->dparameters) {
            foreach ($part->dparameters as $x) {
                $params[strtolower($x->attribute)] = $x->value;
            }
        }



        // ATTACHMENT
        // Any part with a filename is an attachment,
        // so an attached text file (type 0) is not mistaken as the message.
        if ($params['filename'] || $params['name']) {
            // filename may be given as 'Filename' or 'Name' or both
            $filename = ($params['filename']) ? $params['filename'] : $params['name'];

            // filename may be encoded, so see imap_mime_header_decode()
            $attachments[$filename] = $data;  // this is a problem if two files have same name
        }

        // TEXT
        if (empty($part->type) && $data) {
            // Messages may be split in different parts because of inline attachments,
            // so append parts together with blank row.
            if (strtoupper($part->subtype) == 'PLAIN')
                $plain .= trim($data) . "\n\n";
            else {

                $html .= $data;

                $obj->charset = $params['charset'];  // assume all parts are same charset
            }
        }

        // EMBEDDED MESSAGE
        // Many bounce notifications embed the original message as type 2,
        // but AOL uses type 1 (multipart), which is not handled here.
        // There are no PHP functions to parse embedded messages,
        // so this just appends the raw source to the main message.
        elseif ($part->type == 2 && $data) {
            $html .= $data . "\n\n";
        }
        //elseif ($part->type == 1 && $data) $html .= $data;
        // SUBPART RECURSION

        if ($part->parts) {


            foreach ($part->parts as $k => $nextPart) {
                $subObj = $this->getpart($mbox, $id, $nextPart, $key . '.' . ($k + 1), $html, $plain, $attachments);  // 1.2, 1.2.1, etc.
                //error_log('subobjectXX');
                //error_log($subObj);
                $html .= $subObj->html;

                $plain .= $subObj->plain;

                $attachments = array_merge($subObj->attachments, $attachments);
            }
        }


        $obj->html = $html;
        $obj->attachments = $attachments;
        $obj->plain = $plain;
        // error_log(print_r($obj, true));
        return $obj;
    }

    /**
     * parses last ID from javascript to turn it back into a regular array/hastable $array[acount] = [msgID]
     */
    public function parseLastID($array) {
        $return = array();

        foreach ($array as $data) {
            $return[$data['account']] = $data['id'];
        }

        return $return;
    }

    public function findAccountLastID($accountIDs, $account) {
        $return = 0;

        foreach ($accountIDs as $acnt => $id) {
            if ((int) $account == (int) $acnt) {
                $return = $id;

                break;
            }
        }

        return $return;
    }

}
