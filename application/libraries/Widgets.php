<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Widgets extends Library
{
    public function __construct()
    {
        parent::__construct();

        $this->tableName = 'widgets';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    public function getWidgets ($id = null)
    {
        $mtag =  "widgets-" . $this->ci->session->userdata('company') . "-{$id}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (empty($data))
        {
            $this->ci->db->select("widgets.id, widgets.name, widgets.url, widgets.module");
            $this->ci->db->from('widgets');
            $this->ci->db->join('companyModules', 'companyModules.module = widgets.module', 'left');
            $this->ci->db->where('widgets.active', 1);
            $this->ci->db->where('companyModules.company', $this->ci->session->userdata('company'));

            if (!empty($id)) $this->ci->db->where('widgets.id', $id);

            $this->ci->db->order_by('widgets.name', 'asc');

            $query = $this->ci->db->get();

            $results = $query->result();

            if (empty($id)) $data = $results;
            else $data = $results[0];

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $portlet 
     *
     * @return TODO
     */
    public function getPortletWidget ($portlet)
    {
        $mtag =  "porletWidget-{$portlet}-" . $this->ci->session->userdata('userid') . '-' . $this->ci->session->userdata('company');

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('id, widget');
            $this->ci->db->from('userWidgets');
            $this->ci->db->where('portlet', $portlet);
            $this->ci->db->where('userid', $this->ci->session->userdata('userid'));
            $this->ci->db->where('company', $this->ci->session->userdata('company'));

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0];

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }
    
    
    public function addHistory ($widget, $url)
    {
    	$file = '/tmp/widget_' . $widget;
    	
    	$h = fopen($file, 'a+', false);
    	
    	$e = error_get_last();
    	
    	if ($h === false) throw new Exception("Unable to open file {$file}! " . $e['message']);
    	
    	$write = fwrite($h, $url . PHP_EOL);
    	
    	$e = error_get_last();
    	
    	if ($write === false) throw new Exception("Cannot write url to file! {$file}!" . $e['message']);
    	
    	@fclose($h);
    	
    	return true;
    }
}
