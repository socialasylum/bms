<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'Library.php';

class Modules extends Library
{
    public function __construct()
    {

        parent::__construct();

        $this->tableName = 'modules';

        // if connected to DB
        if (class_exists('CI_DB'))
        {

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function moduleInfo ($module)
    {
        $ci =& get_instance();

        if (empty($module)) throw new Exception('module ID is empty!');

        $ci->db->from('modules');
        $ci->db->where('id', $module);

        $query = $ci->db->get();

        $results = $query->result();

        return $results[0];
    }

	/**
	* determines module ID based upon the address
	*/
	public function getModIDFromUrl ()
	{
		$url = $_SERVER['PHP_SELF'];
		
		$url = str_replace('/index.php/', '', $url);

		$slashPos = strpos($url, '/');
		$hashPos = strpos($url, '#');
		$qPos = strpos($url, '?');
		
		if ($slashPos === false)
		{
			
			if ($hashPos !== false) $url = substr($url, 0, $hashPos);
			
			if ($qPos !== false) $url = substr($url, 0, $qPos);

			$namespace = $url;
		}
		else
		{
			$namespace = substr($url, 0, $slashPos);	
		}
		
		$module = $this->getModIDFromController($namespace);

		return $module;
	}

    /**
     * Gets the Module ID from a controller name
     *
     * @param mixed $controller 
     *
     * @return TODO
     */
    public function getModIDFromController ($controller)
    {
        if (empty($controller)) throw new Exception("Controller bane is empty");

        $mtag = "modIdFromCon-" . $controller;

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('id');
            $this->ci->db->from('modules');
            $this->ci->db->where('namespace', strtolower($controller));

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $controller 
     *
     * @return TODO
     */
    public function getModulePermissions ($controller)
    {
        if (empty($controller)) throw new Exception("Controller name is empty");

        $module = $this->getModIDFromController($controller);

        if ($module === false) throw new Exception("Unable to find module ID for controller: " . $controller);

        $mtag = "modPerms-" . $this->ci->session->userdata('position') . '-' . $module;

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('permissions');
            $this->ci->db->from('positionModuleAccess');
            $this->ci->db->where('position', $this->ci->session->userdata('position'));
            $this->ci->db->where('module', $module);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->permissions;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $controller 
     * @param mixed $bit        
     *
     * @return TODO
     */
    public function checkPermission($controller, $bit, $redirect = false)
    {
        if (empty($controller)) throw new Exception("Controller namespace is empty!");
        if (empty($bit)) throw new Exception("Permission bit is empty!");

        $permissions = $this->getModulePermissions($controller);

        // permission is set
        if ($permissions & $bit) return true;


        // check if they are a company admin
        $companyAdmin = $this->ci->users->isCompanyAdmin($this->ci->session->userdata('userid'), $this->ci->session->userdata('company'));
        if ($companyAdmin == true) return true;

        // checks if they are an overall site admin
        $siteAdmin = $this->ci->users->isAdmin($this->ci->session->userdata('userid'));
        if ($siteAdmin == true) return true;

        if ($redirect == true)
        {
            header("Location: /error/access");
            exit;
        }

        return false;
    }

    /**
     * Gets list of all module ID's assigned to a company
     *
     * @param mixed $company Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function getAssignedModules ($company = 0)
    {
        if (empty($company)) $company = $this->ci->session->userdata('company');

        $company = intval($company);

        if (empty($company)) throw new Exception("Company ID is empty!");

        $mtag = "assignedCompanyModules-" . $company;

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('module');
            $this->ci->db->from("companyModules");
            $this->ci->db->where('company', $company);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

   /**
     * Inserts a module for a company
     *
     * @param int $company
     * @param int $module
     *
     * @return int
     */
    public function insertCompanyModule ($company, $module)
    {
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($module)) throw new Exception("module ID is empty!");


        $data = array
            (
                'company' => $company,
                'module' => $module,
            );

        $this->ci->db->insert('companyModules', $data);

        return $this->ci->db->insert_id();
    }

    /**
     * Checks if a module is assigned to a comapny or not
     *
     * @param int $company 
     * @param int @moduoel
     *
     * @return boolean - true if assigned, false if not
     */
    public function checkModuleAssigned ($company, $module)
    {
        if (empty($company)) throw new Exception("Company ID is empty!");
        if (empty($module)) throw new Exception("module ID is empty!");

        // gets assigned modules
        $assignedModules = $this->getAssignedModules($company);

        $assigned = false;

        if (!empty($assignedModules))
        {
            foreach ($assignedModules as $r)
            {
                if ($r->module == $module)
                {
                    $assigned = true;
                }
            }
        }

        if ($assigned == true) return true;


        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function getPrice ($module)
    {
        $module = intval($module);

        if (empty($module)) throw new Exception("Module ID is empty!");

        $mtag = "modPrice-{$module}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('price');
            $this->ci->db->from("modules");
            $this->ci->db->where('id', $module);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->price;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }


    /**
     * Checks if the user has access to the module they are accessing
     *
     * @return TODO
     */
    public function checkAccess ($namespace, $redirect = false)
    {
        // first checks if user is a company admin
        // check if they are a company admin
        $companyAdmin = $this->ci->users->isCompanyAdmin($this->ci->session->userdata('userid'), $this->ci->session->userdata('company'));
        if ($companyAdmin == true) return true;

        // checks if they are an overall site admin
        $siteAdmin = $this->ci->users->isAdmin($this->ci->session->userdata('userid'));
        if ($siteAdmin == true) return true;


        if (empty($namespace)) throw new Exception("Module namespace is empty!");

        $module = $this->getModIDFromController($namespace);

        if ($module === false) throw new Exception("Unable to find Module ID based upon the namespace ($namespace)");

        $menuID = $this->getMenuIDFromModID($this->ci->session->userdata('company'), $module);

        // echo "MENU ID: {$menuID}";

        // gets users Position ID
        $position = $this->ci->users->getPosition();

        if (empty($position))
        {
            error_log("Position ID is not set for user " . $this->ci->session->userdata('userid'));
            return false;
        }

        $mtag = "modAccess-{$menuID}-{$position}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('id');
            $this->ci->db->from('menuAccess');
            $this->ci->db->where('menu', $menuID);
            $this->ci->db->where('position', $position);

            $data = $this->ci->db->count_all_results();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        // user does have access the page
        if ($data > 0)
        {
            return true;
        }

        if ($redirect == true)
        {
            header("Location: /errors/access?module={$module}");
            exit;
        }

        return false;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $company 
     * @param mixed $module  
     *
     * @return TODO
     */
    private function getMenuIDFromModID ($company, $module)
    {

        if (empty($company)) throw new Exception("Company ID is empty");
        if (empty($module)) throw new Exception("Module ID is empty");

        $mtag = "menuIDFromModID-{$company}-{$module}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('id');
            $this->ci->db->from('menu');
            $this->ci->db->where('module', $module);
            $this->ci->db->where('company', $company);

            $query = $this->ci->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        return $data;
    }

    /**
     * Gets ID's of modules that utilize folders
     *
     * @return array, false if no modules
     */
    public function getModulesUsingFolders ()
    {
        $mtag = "modsUsingFolders";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('id');
            $this->ci->db->from('modules');
            $this->ci->db->where('usesFolders', 1);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        $mods = array();

        foreach ($data as $r)
        {
            $mods[] = $r->id;
        }

        return $mods;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $namespace 
     *
     * @return TODO
     */
    public function getViewableFolderIDs ($namespace)
    {
        $module = $this->getModIDFromController($namespace);

        if (empty($module)) throw new Exception("Unable to get Module ID from namespace: {$namespace}");

        $mtag = "viewableFolderIDs-{$module}";

        $data = $this->ci->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->ci->db->select('folder');
            $this->ci->db->from('folderModViewable');
            $this->ci->db->where('module', $module);

            $query = $this->ci->db->get();

            $data = $query->result();

            $this->ci->cache->memcached->save($mtag, $data, $this->ci->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        $folders = array();

        foreach ($data as $r)
        {
            $folders[] = $r->folder;
        }

        return $folders;
    }


	public function getIcon ($render = false)
	{
		$mod = $this->getModIDFromUrl();
		
		$icon = $this->getTableValue('icon', $mod);
		
		if ($render && !empty($icon)) return "<i class='{$icon}'></i> ";
		
		return $icon;
	}
}
