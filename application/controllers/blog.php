<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller
{
    public $bcControllerUrl = '/blog';
    public $bcControllerText = "<i class='fa fa-quote-left'></i> Blog";

    public $bcViewText;
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Blog()
    {
        parent::__construct();

        $this->load->model('blog_model', 'blog', true);
        $this->load->driver('cache');

        $this->load->library('blogs');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($page = 1)
    {
        $header['headscript'] = $this->functions->jsScript('blog.js');

        $header['onload'] = "blog.indexInit();";
		
		
        $body['page'] = $page;

        try
        {
            $body['blogs'] = $this->blog->getBlogs($page);

            $body['editBlog'] = $this->modules->checkPermission($this->router->fetch_class(), 1); // checks if user has ability to edit blogs

            $body['totalBlogs'] = $totalBlogs = $this->blog->getTotalBlogCnt();

            $body['totalPages'] = $totalPages = ceil((int) $totalBlogs / (int) $this->config->item('blogsPerPage'));
            
            $body['namespace'] = $this->router->fetch_class();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }



        $this->load->view('template/header_intranet', $header);
        $this->load->view('blog/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = 0, $folder = 0, $page = 1)
    {

        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'New Blog' : 'Edit Blog');

        $header['headscript'] = $this->functions->jsScript('blog.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['onload'] = "blog.editInit();";

        $header['ckeditor'] = true;
        $header['slimscroll'] = true;
		$header['fancytree'] = true;

        $body['id'] = $id;
        $body['folder'] = $folder;
        $body['page'] = $page;

        try
        {
            $body['status'] = array('Deactived', 'Active');
            $body['allowComments'] = array('Do not allow comments', 'Allow Comments');

            $this->modules->checkPermission($this->router->fetch_class(), 1, true); // checks if user has ability to edit blogs

            if (!empty($id))
            {
                $body['info'] = $info = $this->blog->getBlogInfo($id);

                // $body['publishDate'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->publishDate, "Y-m-d");
                $body['publishDate'] = date("Y-m-d", strtotime($info->publishDate));

                $body['tags'] = $this->blog->getTags($id);
            }

            $body['users'] = $this->users->getUsers();

            $body['namespace'] = $this->router->fetch_class();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }



        $this->load->view('template/header_intranet', $header);
        $this->load->view('blog/edit', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['id']))
                {
                    $_POST['id'] = $this->blog->createNewPost($_POST);

                    $msg = "Blog post has been created!";
                   // $this->functions->jsonReturn('SUCCESS', 'Blog post has been created!');
                }
                else
                {
                    $this->blog->updatePost($_POST);

                    // clear tags
                    $this->blog->clearTags($_POST['id']);

                    $msg = "Blog post has been updated!";
                }

                $this->blog->saveTags($_POST['id'], $_POST['tag']);
 
				if (!empty($_FILES['featuredImg']['name']))
				{
					// creates diretory
					$path = "public/uploads/blog/";
					$this->functions->createDir($path);
					
				
					$config['upload_path'] = './' . $path;
					$config['allowed_types'] = 'gif|jpg|png';
					$config['encrypt_name'] = true;
					
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('featuredImg'))
					{
						throw new Exception("Unable to upload blog image");
					}
					
					$uploadData = $this->upload->data();

                    // upload blog featured img
                    $this->blog->updateFeaturedImg($_POST['id'], $uploadData['file_name']);
				}
				
				header("Location: /blog/index/{$_POST['page']}?site-success=" . urlencode($msg));
				exit;
            
                //$this->functions->jsonReturn('SUCCESS', $msg);
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
				header("Location: /blog?site-error=" . urlencode($e->getMessage()));
				exit;
            }

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function view ($id, $page = 1)
    {

        $header['headscript'] = $this->functions->jsScript('blog.js');

        $header['ckeditor'] = true;
        $header['onload'] = "blog.viewInit();";

        $body['id'] = $id;
        $body['page'] = $page;

        try
        {
            $body['info'] = $this->blog->getBlogInfo($id);
            $body['comments'] = $this->blog->getBlogComments($id);

            $body['editBlog'] = $this->modules->checkPermission($this->router->fetch_class(), 1); // checks if user has ability to edit blogs
            $body['editComments'] = $this->modules->checkPermission($this->router->fetch_class(), 2); // checks if user has ability to edit blog comments

            $body['namespace'] = $this->router->fetch_class();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('blog/view', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savecomment ()
    {
        if ($_POST)
        {
            try
            {
                $this->blog->insertComment($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Comment has been saved!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deletecomment ()
    {
        if ($_POST)
        {
            try
            {
                $this->blog->deleteComment($_POST['comment']);
                $this->functions->jsonReturn('SUCCESS', "<i class='fa fa-trash-o'></i> Comment has been deleted!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function setspamcomment ()
    {
        if ($_POST)
        {
            try
            {
                $this->blog->setSpamComment($_POST['comment']);
                $this->functions->jsonReturn('SUCCESS', "<i class='fa fa-ban'></i> Comment has been set to spam!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
