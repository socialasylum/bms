<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller
{

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Menu()
    {
        parent::__construct();

        $this->load->model('menu_model', 'menu', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::redirect("/intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
        }

    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
    	$header['headscript'] = $this->functions->jsScript('menu.js');
		$header['jqueryui'] = true;

        $body['id'] = $id = $this->session->userdata('company');

        $header['onload'] = "menu.indexInit();";

        // $header['bootstrapmodal'] = true;

        $body['linkTargets'] = array
            (
                'Normal' => '',
                'New Window' => '_blank'
            );

        try
        {
            // $body['companies'] = $this->companies->getCompanies();

            $body['companyName'] = $this->companies->getCompanyName($id);

            $body['headers'] = $this->menu->getMenu($id, true);

            $body['modules'] = $this->menu->getCompanyModules($id);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        // $this->load->view('menu/index', $body);
        $this->load->view('menu/menucontent', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function menuContent ($id)
    {
        $body['id'] = $id;

        try
        {
            $body['companyName'] = $this->companies->getCompanyName($id);

            $body['headers'] = $this->menu->getMenu($id, true);

            $body['modules'] = $this->menu->getCompanyModules($id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('menu/menucontent', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createMenuItem ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->menu->insertMenuItem($_POST);

                $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveHeaderOrder ()
    {
        if ($_POST)
        {
            try
            {
                $this->menu->updateHeaderOrder($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Menu header order has been updated!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deletemenuitem ()
    {
        if ($_POST)
        {
            try
            {
                $this->menu->deleteMenuItem($_POST['id'], $_POST['company']);
                $this->functions->jsonReturn('SUCCESS', 'Menu item has been deleted!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function access ($id)
    {
        $body['id'] = $id;

        try
        {
            $body['positions'] = $this->positions->getPositions(true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('menu/access', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveaccess ()
    {
        if ($_POST)
        {
            try
            {
                $this->menu->clearAccess($_POST['id']);

                $this->menu->saveMenuAccess($_POST);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function modulepermissions ($module)
    {

        $body['module'] = $module;

        try
        {
            $body['permissions'] = $this->menu->getModulePermissions($module);
            $body['positions'] = $this->positions->getPositions(true);
            $body['access'] = $this->menu->getModulePositionAccess($module);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('menu/modulepermissions', $body);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $module 
     *
     * @return TODO
     */
    public function editpermission ($module, $id = null)
    {

        try
        {
            $body['module'] = $module;

            if (empty($id))
            {
                $body['bit'] = $this->menu->getNextModuleBit($module);
            }
            else
            {
                $body['info'] = $this->menu->getModPermInfo($id, $module);

                $body['bit'] = $body['info']->bit;
            }

            $body['id'] = $id;

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('menu/editpermission', $body);

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savemodpermission ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['id']))
                {
                    $this->menu->insertModulePermission($_POST);
                    $this->functions->jsonReturn('SUCCESS', 'Module Permission has been created!');
                }
                else
                {
                    $this->menu->updateModulePermission($_POST);
                    $this->functions->jsonReturn('SUCCESS', 'Module Permission has been updated!');
                }
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savemodpermaccess ()
    {
        if ($_POST)
        {
            try
            {
                $this->menu->saveModulePositionAccess($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Module Position Permissions access has been updated!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function search ()
    {
        try
        {
            $query = urldecode($_GET['query']);

            // $menu = $this->menu->search($query);

            echo json_encode($menu);

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteheader ()
    {
        if ($_POST)
        {
            try
            {
                $this->menu->deleteMenuItemsByParentID($_POST['id']);
                $this->menu->deleteMenuItem($_POST['id'], $this->session->userdata('company'));
                
                $this->functions->jsonReturn('SUCCESS', "Menu header has been deleted!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
