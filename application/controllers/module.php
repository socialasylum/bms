<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module extends CI_Controller
{
    public $bcControllerUrl = '/module';
    public $bcControllerText = 'Modules';

    public $bcViewText;

    function Module()
    {
        parent::__construct();

        $this->load->model('module_model', 'module', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

        $header['headscript'] = $this->functions->jsScript('module.js');
        $header['onload'] = "module.indexInit();";

        $header['datatables'] = true;

        try
        {
            $body['modules'] = $this->module->getModules();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('module/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to null. 
     *
     * @return TODO
     */
    public function edit ($id = null)
    {
        $header['headscript'] = $this->functions->jsScript('module.js');
        $header['onload'] = "module.editInit();";
        $header['ckeditor'] = true;
        $header['datatables'] = true;
        //$header['bootstrapmodal'] = true;
		$header['fancytree'] = true;
		$header['perfectscroll'] = true;
		$header['prettycheck'] = true;
		
        $body['id'] = $id;

        if (empty($id)) $this->bcViewText = "Create Module";
        else $this->bcViewText = "Edit Module";

        try
        {
            $body['status'] = array('Deactived', 'Active');

            if (!empty($id))
            {
                $body['info'] = $this->module->getModuleInfo($id);
                $body['modComps'] = $this->module->getModuleCompanies($id);
                $body['widgets'] = $this->module->getModuleWidgets($id);
            }

            $body['companies'] = $this->companies->getAllCompanies();

			$files = $this->functions->getDirContents($_SERVER['DOCUMENT_ROOT'] . 'public/js/', true, true, true, false, array('js', 'css'), false, true);

			$params = array
			(
				'ul' => array('id' => 'file-tree', 'class' => 'file-tree')
			);
			
			$body['ul'] = $this->functions->buildDirectoryUL($files, '/public/js/', $params);
//$body['libs'] = $this->functions->getDirContents($_SERVER['DOCUMENT_ROOT'] . 'public/js/', true, true, true, false, array('js', 'css'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('module/edit', $body);
        $this->load->view('template/footer_intranet');

    }

	public function jsfunctions ()
	{
		try
		{
			$body['file'] = $file = urldecode($_GET['file']);
			
			$body['jsContent'] = $jsContent = PHPFunctions::getFileContent($file);
			
			$body['functions'] = PHPFunctions::getJSFunctions($jsContent);
			
			print_r($body['functions']);
			
		}
		catch (Exception $e)
		{
			$this->functions->sendStackTrace($e);
		}
		
		
	}

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['id']))
                {
                    // inserts new module
                    $module = $this->module->insertModule($_POST);

                    $msg = $_POST['name'] . " module has been created!";
                }
                else
                {
                    // module needs to be updated
                    $module = $this->module->updateModule($_POST);

                    $msg = $_POST['name'] . " module has been updated!";
                }

                $this->module->clearCompanyModules($module);

                $this->module->insertCompanymodules($module, $_POST['company']);

                $this->functions->jsonReturn('SUCCESS', $msg);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function widgetmodal ($module, $id = null)
    {

        $body['module'] = $module;

        try
        {
            $body['status'] = array('Deactived', 'Active');
            $body['id'] = $id;

            if (!empty($id)) $body['info'] = $this->module->getModuleWidgets($module, $id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('module/widgetmodal', $body);

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savewidget ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['id']))
                {
                    // create new widget
                    $id = $this->module->insertWidget($_POST);

                    $this->functions->jsonReturn('SUCCESS', "Widget has been created!");
                }
                else
                {
                    // update widget
                    $id = $this->module->updateWidget($_POST);

                    $this->functions->jsonReturn('SUCCESS', "Widget has been updated!");
                }

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
