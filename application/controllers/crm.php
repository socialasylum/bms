<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crm extends CI_Controller
{

    public $bcControllerUrl = '/crm';
    public $bcControllerText = "<i class='fa fa-rocket'></i> CRM";

    public $bcViewText;


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Crm()
    {
        parent::__construct();

        $this->load->model('crm_model', 'crm', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::redirect("/intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

		$header['headscript'] = $this->functions->jsScript('crm.js');
		$header['datatables'] = true;

        $header['onload'] = "crm.indexInit();";

        $header['datatables'] = true;
        try
        {
            $body['contacts'] = $this->crm->getContacts();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('crm/index', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = null)
    {

        $this->load->model('orders_model', 'orders', true);
        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Contact";

		$header['headscript'] = $this->functions->jsScript('crm.js');
		$header['jqueryui'] = true;

        $header['onload'] = "crm.editInit();";

        $header['ckeditor'] = true;
        $header['inputedit'] = true;

        $body['ampm'] = array('AM', 'PM');
        $body['id'] = $id;

        try
        {
            if (!empty($id))
            {
                $body['info'] = $this->crm->getContactInfo($id);
                $body['emails'] = $this->crm->getContactEmails($id);
                $body['phones'] = $this->crm->getContactPhones($id);
                $body['addresses'] = $this->crm->getContactAddresses($id);
                $body['notes'] = $this->crm->getContactNotes($id);


                $body['orders'] = $this->orders->getOrders($this->session->userdata('company'), $id);
            }

            $body['industry'] = $this->functions->getCodes(17);
            $body['types'] = $this->functions->getCodes(8, 'display');
            $body['phoneTypes'] = $this->functions->getCodes(9, 'display');
            $body['states'] = $this->functions->getStates();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('crm/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {

            // print_r($_POST);die;

            try
            {
                // $edited = (bool) $_POST['edited'];

                if (empty($_POST['id']))
                {
                    $id = $this->crm->insertContact($_POST);

                    $msg = "CRM contact has been created!";

                    // $edited = true;
                    $_POST['edited'] = 1;
                }
                else
                {
                    $id = $this->crm->updateContact($_POST);

                    $this->crm->clearContactEmails($id);
                    $this->crm->clearContactPhone($id);
                    $this->crm->clearContactAddress($id);

                    $msg = "CRM contact has been updated!";
                }

                $this->crm->saveEmails($id, $_POST['email']);
                $this->crm->savePhones($id, $_POST['phone']);
                $this->crm->saveAddress($id, $_POST['address']);

                if (!empty($_POST['contactNote']))
                {
                    // insertes note if note is not empty
                    $noteID = $this->crm->insertCRMNote($id, $_POST['contactNote']);
                }

                if (!empty($_POST['callDate']))
                {
                    // if they have selected a date for a call, will save the call

                    $date = "{$_POST['date']} {$_POST['callHour']}:{$_POST['callMin']} {$_POST['callAP']}";

                    $callID = $this->crm->insertCall($id, $date, $_POST['callType'], $_POST['callStatus'], $_POST['callNote']);
                }

                $this->functions->jsonReturn('SUCCESS', $msg, $id);


            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deletecontact ()
    {
        if ($_POST)
        {
            try
            {
                $this->crm->deleteCrmContact($_POST['contact']);
                $this->functions->jsonReturn('SUCCESS', 'Contact has been deleted!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
