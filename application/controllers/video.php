<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video extends CI_Controller
{
    public $bcControllerUrl = '/video';
    public $bcControllerText = "<i class='fa fa-film'></i> Videos";

    public $bcViewText;
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Video()
    {
        parent::__construct();

        $this->load->model('video_model', 'video', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($folder = 0, $rootFolder = 0)
    {

        $header['headscript'] = $this->functions->jsScript('video.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['onload'] = "video.indexInit();";
        $header['rightlick'] = true;

        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;

        try
        {
            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
            $body['content'] = $this->video->getFolderContent($folder);
            $body['parentFolder'] = $this->folders->getTableValue('parentFolder', $folder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('video/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveyoutubeurl ()
    {
        if ($_POST)
        {
            try
            {
                // gets YouTube Video ID from URL
                $id = $this->functions->getYoutubeVideoID($_POST['youtubeUrl']);

                // gets youtube data about video
                $_POST['youtubeData'] = $youtubeData = $this->functions->getYoutudeVideoData($id, false);

                $data = json_decode($youtubeData);

                $_POST['title'] = $data->entry->title->{'$t'};
                $_POST['thumbnail'] = $data->entry->{'media$group'}->{'media$thumbnail'}[0]->url;

                $video = $this->video->insertVideo($_POST);
                $this->video->assignVideoToFolder($video, $_POST['folder']);

                $this->functions->jsonReturn('SUCCESS', "YouTube video has been saved!", $video);
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function view ($id)
    {
        try
        {
            $body['info'] = $info = $this->video->getInfo($id);

            $this->bcViewText = $info->title;

            if (!empty($info->youtubeData))
            {
                $body['youtubeData'] = $youtubeData = json_decode($info->youtubeData);

                $body['src'] = $youtubeData->entry->content->src;
                $body['description'] = $youtubeData->entry->{'media$group'}->{'media$description'}->{'$t'};
            }

            $this->video->insertVideoView($id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('video/view', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = 0, $folder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('video.js');

        $header['onload'] = "video.editInit();";
        $header['ckeditor'] = true;

        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Video";

        $body['id'] = $id;
        $body['folder'] = $folder;

        $body['status'] = array('Deactived', 'Active');

        try
        {
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('video/edit', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {

                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "swf|flv|mp4|mov|mpg|mpeg|avi";
                $config['max_size'] = "512000";
                $config['encrypt_name'] = true;

                // if profile image hass been uploaded
                if (!empty($_FILES['src']['name']))
                {
                    $path = 'public' . DS . 'uploads' . DS . 'videos' . DS . $this->session->userdata('company') . DS;

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);
                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image
                    if (!$this->upload->do_upload('src'))
                    {
                        throw new Exception("Unable to upload video!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();

                    $_POST['fileName'] = $uploadData['file_name'];
                }

                // will now upload thumbnail
                if (!empty($_FILES['thumbnail']['name']))
                {
                    $path = 'public' . DS . 'uploads' . DS . 'videothumbnails' . DS . $this->session->userdata('company') . DS;

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);
                    
                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "png|gif|jpg|bmp";
                    
                    // reloads upload library with new config
                    $this->load->library('upload', $config);

                    // will now upload actual image
                    if (!$this->upload->do_upload('thumbnail'))
                    {
                        throw new Exception("Unable to upload thumbnail!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();

                    $_POST['thumbnail'] = $uploadData['file_name'];
                }

                // will now save video
                $video = $this->video->insertVideo($_POST);

                $this->video->assignVideoToFolder($video, $_POST['folder']);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function ytdata ()
    {
        try
        {
            $url = "http://www.youtube.com/watch?v=Hhzvn_FLs-Q";

            $id = $this->functions->getYoutubeVideoID($url);

            $data = $this->functions->getYoutudeVideoData($id, false);

            print_r($data);
            echo '<hr>' . PHP_EOL;

            $title = $data->entry->title->{'$t'};
        
            $thumbnail = $data->entry->{'media$group'}->{'media$thumbnail'}[0]->url;

            // print_r($title);

            echo "Title: {$title}" . PHP_EOL;
            echo "Thumbnail: {$thumbnail}" . PHP_EOL;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
    }
}
