<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller
{
	public $cfg;	
	public $bcControllerUrl = '/cms';
	public $bcControllerText = "<i class='fa fa-wrench'></i> CMS";
	
	function Cms ()
	{
		parent::__construct();
		$this->load->config('cms');
				
        $this->load->model('cms_model', 'cms', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();
        
        $this->path = $_SERVER['DOCUMENT_ROOT'] . 'public' . DS . 'uploads' . DS . 'web' . DS . $this->session->userdata('company') . DS;
		
		$this->cfg = $this->config->item('cms');
		
        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);

            // checks folder structure
            $this->cms->checkFolders($this->path);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }
        
	}
	
	public function index ()
	{
		$header['headscript'] = $this->functions->jsScript('cms.js');
		$header['onload'] = "cms.index.init();";
		$header['container'] = 'container-fluid';
		$header['perfectscroll'] = true;
		$header['fancytree'] = true;
		$header['uploader'] = true;
		
		try
		{
			$body['sitemapUL'] = $this->cms->sitemapUL($this->path);
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
        $this->load->view('template/header_intranet', $header);
        $this->load->view('cms/index', $body);
        $this->load->view('template/footer_intranet');	
	}
	
	public function upload ()
	{
		try
		{
			if ($_POST && !empty($_FILES['file']['name']))
			{
                $path = $this->path . $_POST['path']; 
               
               // error_log($path);
                // ensures company uploads directory has been created
                $this->functions->createDir($path, false);

                $config['upload_path'] = $path;
                //$config['allowed_types'] = "html|html|js|css|less|txt";
                $config['allowed_types'] = implode('|', $this->cfg['extensions']);
                $config['max_size'] = "5120";
                $config['encrypt_name'] = false;

                // loads upload library
                $this->load->library('upload', $config);

                // will now upload actual image

                if (!$this->upload->do_upload('file'))
                {
                    throw new Exception("Unable to upload file!" . $this->upload->display_errors());
                }

                $uploadData = $this->upload->data();
                
                $this->functions->jsonReturn('SUCCESS', "File has been uploaded!", true, 0, array('file' => $uploadData['file_name']));

			}
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
	
	public function sitemap ()
	{
		try
		{
			echo $this->cms->sitemapUL($this->path);
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
       // $this->load->view('cms/sitemap', $body);	
	}
	
	public function open ()
	{
		try
		{
			//$html = PHPFunctions::getFileContent($this->path . $_POST['file'], null);
			$html = $this->cms->parseHtml($this->path . $_POST['file']);

			print_r($html->getElementsByTagName('body'));

		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
	
}