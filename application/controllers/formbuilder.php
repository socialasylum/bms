<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formbuilder extends CI_Controller
{
    public $bcControllerUrl = '/formbuilder';
    public $bcControllerText = "<i class='fa fa-code'></i> Form Builder";

    public $bcViewText;

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Formbuilder()
    {
        parent::__construct();

        $this->load->model('formbuilder_model', 'formbuilder', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($folder = 0, $rootFolder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('formbuilder.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['onload'] = "formbuilder.indexInit();";
        // $body['admin'] = true;
        $header['datatables'] = true;
        $header['rightlick'] = true;

        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;

        try
        {
            $body['content'] = $this->formbuilder->getFolderContent($folder);

            $body['submittedForms'] = $this->formbuilder->getSubmittedForms();
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('formbuilder/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {
        $this->bcViewText = "<i class='fa fa-edit'></i> Edit form";

        $header['ckeditor'] = true;

        $body['status'] = array('Deactived', 'Active');

        $header['headscript'] = $this->functions->jsScript('formbuilder.js');

        $header['onload'] = "formbuilder.editInit();";

        if (isset($_GET['title'])) $body['title'] = urldecode($_GET['title']);

        try
        {
            $body['id'] = $id;

            if (!empty($id))
            {
                $body['info'] = $info = $this->formbuilder->getFormInfo($id);

                // sets html session
                $this->session->set_userdata('formHtml', $info->formHtml);
            }
            else
            {
                // clears HTML session
                $this->session->unset_userdata('formHtml');
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('formbuilder/edit', $body);
        $this->load->view('template/footer_intranet', $footer);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addapplvl ($cnt, $id = 0)
    {
        $body['cnt'] = (int) $cnt;

        try
        {
            $body['positions'] = $this->positions->getPositions(true);

            if (!empty($id))
            {
                $body['approvals'] = $this->formbuilder->getApprovalLvls($id);
            }

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('formbuilder/addapplvl', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {

                // print_r($_POST);die;

                $id = $this->formbuilder->saveForm($_POST);

                $this->functions->jsonReturn('SUCCESS', "Form has been saved!", $id);
            }
            catch(Exception $e)
            {
                $this->function->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createfolder ()
    {
        if ($_POST)
        {
            try
            {
                $folderId = $this->formbuilder->createFolder($_POST, $this->session->userdata('userid'), $this->session->userdata('company'));

                $this->functions->jsonReturn('SUCCESS', $folderId);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id         
     * @param mixed $folder     Optional, defaults to 0. 
     * @param mixed $rootFolder Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function view ($id, $folder = 0, $rootFolder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('formbuilder.js');

        $header['onload'] = "formbuilder.viewInit();";

        $body['id'] = $id;

        try
        {
            $body['info'] = $this->formbuilder->getFormInfo($id);
        }
        catch(Exception $e)
        {
             $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('formbuilder/view', $body);
        $this->load->view('template/footer_intranet', $footer);
    }


    /**
     * View submitted forms
     *
     * @param mixed $id 
     *
     */
    public function viewsubmitted ($id, $folder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('formbuilder.js');

        $header['onload'] = "formbuilder.viewsubmittedInit();";

        $body['id'] = $id;
        $body['folder'] = $folder;

        try
        {
            $body['info'] = $info = $this->formbuilder->getFormPostDataInfo($id);

            $body['formInfo'] = $this->formbuilder->getFormInfo($info->form);

            $body['date'] = $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->datestamp, "m/d/Y g:i A");

            $body['submitName'] = $this->users->getName($info->userid);

            $body['final'] = $this->formbuilder->hasFinalSignature($id);
        }
        catch(Exception $e)
        {
             $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('formbuilder/viewsubmitted', $body);
        $this->load->view('template/footer_intranet', $footer);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveformpost ()
    {
        if ($_POST)
        {
            try
            {
                $info = $this->formbuilder->getFormInfo($_POST['id']);

                $this->formbuilder->savePostdata($_POST, $info->title, $info->formHtml);

                $this->functions->jsonReturn('SUCCESS', "Form has been saved!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);

                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function complete ($id = 0)
    {
        $header['headscript'] = $this->functions->jsScript('formbuilder.js');
        $header['onload'] = "formbuilder.completeInit();";

        try
        {
            $body['title'] = $this->formbuilder->getFormTitle($id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('formbuilder/complete', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addelement ()
    {
        if ($_POST)
        {
            if (empty($_POST['html'])) $this->functions->jsonReturn('ALERT', "No HTML element to add!");

            $html = $this->session->userdata('formHtml');

            $html .= $_POST['html'];

            $this->session->set_userdata('formHtml', $html);

            $this->functions->jsonReturn('SUCCESS', "HTML has been added!");
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function renderdynamic ()
    {
        try
        {
            $body['formHtml'] = $this->session->userdata('formHtml');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('formbuilder/renderdynamic', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function clearformhtml ()
    {
        $this->session->unset_userdata('formHtml');
        $this->functions->jsonReturn('SUCCESS', "Form HTML has been cleared");
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getelproperties ($options = 0)
    {

        // uses codes modal
        // $this->load->model('codes_model', 'codes', true);

        $body['options'] = $options = (bool) $options;

        try
        {
            if ($options == true)
            {

                $body['datasets'] = $this->formbuilder->getDatasets();

                $body['groups'] = $this->functions->getCodeGroups();
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('formbuilder/getelproperties', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function updatedynform ()
    {
        try
        {
            $html = $this->formbuilder->stripSortTags($_POST['html']);

            $this->session->set_userdata('formHtml', $html);

            $this->functions->jsonReturn('SUCCESS', "HTML has been added!");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function delete ()
    {
        if ($_POST)
        {
            try
            {
                $this->formbuilder->setFormDeleted($_POST['id']);
                $this->functions->jsonReturn('SUCCESS', "Form has been deleted!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function rightclick ($folder = 0)
    {
        $body['folder'] = $folder;

        $this->load->view('item/rightclick', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function setsignlvl ()
    {
        if  ($_POST)
        {
            try
            {
                $id = $this->formbuilder->insertFormSignature($_POST['id'], $_POST['lvl'], $_POST['decision'], $_POST['final']);
                $this->functions->jsonReturn('SUCCESS', "Decision has been saved!", $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function tagtest ()
    {
        $html = "<ul class=\"ui-sortable\" id=\"formSort\"><li><input type='text' name='name' id='name' value=\"\" placeholder='Jane Doe'></li>
            <li><a href='#'></a></li>
        </ul>";

        echo "<xmp>{$html}</xmp>";
        echo "<hr>";

        $html = $this->formbuilder->stripSortTags($html);

        echo "<xmp>{$html}</xmp>";
    }

    /**
     * TODO: short description.
     *
     * @param mixed $group 
     *
     * @return TODO
     */
    public function codegrouppreview ($group)
    {
        try
        {
            $body['codes'] = $this->functions->getCodes($group);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('formbuilder/codegrouppreview', $body);
    }
}
