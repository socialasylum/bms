<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporting extends CI_Controller
{
    public $bcControllerUrl = '/reporting';
    public $bcControllerText = "<i class='fa fa-bar-chart-o'></i> Reporting Services";

    public $bcViewText;
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Reporting()
    {
        parent::__construct();

        $this->load->model('reporting_model', 'reporting', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($folder = 0, $rootFolder = 0)
    {

        $header['headscript'] = $this->functions->jsScript('reporting.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['onload'] = "reporting.indexInit();";
        $header['rightlick'] = true;

        // $body['admin'] = true;

        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;

        try
        {
            $body['content'] = $this->reporting->getFolderContent($folder);
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('reporting/index', $body);
        $this->load->view('template/footer_intranet');

    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {
        $header['headscript'] = $this->functions->jsScript('reporting.js');

        $header['onload'] = "reporting.editInit();";


        $body['id'] = $id;

        // if (empty($id)) $body['title

        try
        {
        
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('reporting/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($p['id']))
                {
                    $id = $this->reporting->insertReport($_POST);
                }
                else
                {
                    $id = $this->reporting->updateReport($_POST);
                }

                $this->reporting->assignReportToFolder($id, $_POST['folder']);

                $this->functions->jsonReturn('SUCCESS', "Report has been saved!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createds ($id = 0, $folder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('reporting.js');

        $header['ckeditor'] = true;
        $header['dynatree'] = true;

        $header['onload'] = "reporting.createdsInit();";
        
        $this->bcViewText = "<i class='fa fa-edit'></i> " . (empty($id) ? 'Create'  : 'Edit') . " Dataset";

        $body['id'] = $id;
        $body['folder'] = $folder;

        try
        {

            if (!empty($id))
            {
                $body['info'] = $this->reporting->getDatasetInfo($id);
            }
            else
            {
                $body['info']->query = "SELECT @1, @2 FROM users"; // for testing variables
            }

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('reporting/createds', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function foldertree ()
    {

        try
        {
            $body['tables'] = $this->reporting->getTables();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('reporting/foldertree', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function execsql ()
    {
        if ($_POST)
        {
            try
            {

                $vars = $this->session->userdata('dsVars');

                if (empty($vars)) $query = $_POST['query'];
                else $query = $this->reporting->parseQuery($_POST);

                $checkHarmful = $this->reporting->checkSqlHarmful($query);

                if ($checkHarmful == true) throw new Exception("SQL Statement can only execute SELECT statements!");

                $body['data'] = $this->reporting->execQuery($query);
                
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);

                echo $this->alerts->error($e->getMessage());
                exit;
            }


            $this->load->view('reporting/execsql', $body);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveds ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['id']))
                {
                    // inserts new dataset
                    $id = $this->reporting->insertDataset($_POST);

                    // assigns dataset to folder
                    $this->reporting->assignDatasetToFolder($id, $_POST['folder']);

                    $msg = "Dataset has been created!";
                }
                else
                {
                    // updates dataset
                    $id = $this->reporting->updateDataset($_POST);

                    $msg = "Dataset has been updated!";
                }

                $this->functions->jsonReturn('SUCCESS', $msg, $id);

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function rightclick ($folder = 0)
    {
        $body['folder'] = $folder;

        $this->load->view('reporting/rightclick', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function variables ()
    {

        try
        {
            $this->session->unset_userdata('dsVars');

            $body['query'] = $_POST['query'];

            $body['cnt'] = substr_count($_POST['query'], '@');

            $body['vars'] = $this->reporting->getSqlVars($_POST['query']);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('reporting/variables', $body);
    }

}
