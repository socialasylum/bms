<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compatibility extends CI_Controller
{
    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Compatibility()
    {
        parent::__construct();

        // $this->load->model('compatibility_model', 'compatibility', true);
        $this->load->driver('cache');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['nav'] = 'compat';

        $this->load->view('template/header', $header);
        $this->load->view('compatibility/index', $body);
        $this->load->view('template/footer');
    }
}
