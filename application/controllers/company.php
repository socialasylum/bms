<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller
{
    public $bcControllerUrl = '/company';
    public $bcControllerText = 'Company Management';

    public $bcViewText;
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Company()
    {
        parent::__construct();

        $this->load->driver('cache');

        $this->load->model('company_model', 'company', true);
        $this->load->model('position_model', 'position', true);
		$this->load->model('department_model', 'department', true);
		$this->load->model('user_model', 'user', true);

        $this->functions->checkLoggedIn();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function logo ($company = 0)
    {

        if (empty($company)) $company = $this->session->userdata('company');

        try
        {
            $logo = $this->companies->getTableValue('logo', $company);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $path = 'public' . DS . 'uploads' . DS . 'logos' . DS . $this->session->userdata('company') . DS;

        $config['image_library'] = 'gd2';
        $config['source_image']= './' . $path . $logo;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 100;
        $config['height']= 50;

        $this->load->library('image_lib', $config);

        $this->image_lib->resize();
    }

	public function create ()
	{
		if ($_POST)
		{
			try
			{
				$isAdmin = $this->users->isAdmin($this->session->userdata('userid'));
				
				if ($isAdmin !== true)
				{
					throw new Exception("Someone attempted to create a company who is not an admin");
					$this->functions->jsonReturn('ERROR', "You must be an administrator to create new companies");
				}
				
				// insert new company
				$company = $this->company->createCompany($_POST);
				
				// will now insert default position
				$posData = array
				(
					'name' => $_POST['posName'],
					'shortName' => $_POST['posName'],
					'default' => 1,
					'description' => null
				);
				
				$this->position->createPosition($posData, $company);
				
				// will now create the default department
				$depData = array
				(
					'name' => $_POST['depName'],
					'active' => 1,
					'default' => 1
				);
				
				$this->department->createDepartment($depData, $this->session->userdata('userid'), $company);
				
				// if user wants to be added to company during creation
				if ($_POST['addUser'] == 1)
				{
					$this->user->insertUserCompany($this->session->userdata('userid'), $company, false);
				}
				
				
				$this->functions->jsonReturn('SUCCESS', "Company has been created!");
			}
			catch (Exception $e)
			{
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
			}
		}
	}
}
