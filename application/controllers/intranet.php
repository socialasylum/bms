<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH . 'third_party' . DS . 'bms' . DS .'index.php';

class Intranet extends CI_Controller
{

    function Intranet()
    {
        parent::__construct();

		//$this->load->database();

        $this->load->model('intranet_model', 'intranet', true);

        $this->load->library('widgets');

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


		$folders = array
		(
			'public/uploads',
			'public/css',
			'public/less'
		);
		
		try
		{
			//$this->functions->checkFolders($folders);
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			echo $this->alerts->error($e->getMessage());
		}
		
    }
    
    function __destruct ()
    {
	    $this->db->close();
    }
    
    public function index()
    {
        $header['headscript'] = $this->functions->jsScript('intranet.js');
        $header['container_style'] = "margin-top:0;background:none;";
        
        $header['container'] = 'container-fluid';
        
        $header['gridster'] = true;
        $header['opacity'] = 0;
        $header['perfectscroll'] = true;
        $header['href'] = "javascript:intranet.openPage('URL', undefined, 'widget');";
        //$header['wrapperbg'] = 'none';
		$header['dashboard'] = true;
		
        
       // $header['css'] = "body{ background: url('/public/images/pattern.png') repeat; }";
        //$header['jqueryui'] = false;
       
        
        //$header['wrapper_style'] = "overflow:hidden;";
        
  //      $header['onload'] = "intranet.indexInit();";

		$footer['hide'] = true;
		
		try
		{
			if ($this->style->getVal($this->session->userdata('company'), 'bgRepeat') == '4') $header['css'] = "body{ background:none; }";
		
			//$body['ports'] = $this->intranet->getDashboardPorts($this->session->userdata('userid'), $this->session->userdata('company'));
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
        $this->load->view('template/header_intranet', $header);
        $this->load->view('intranet/index', $body);
        $this->load->view('template/footer_intranet', $footer);
    }

	public function savedashboard ()
	{
		try
		{
			if ($_POST)
			{
				$ids = $this->intranet->saveDashboard($_POST['ports']);
				
				PHPFunctions::jsonReturn('SUCCESS', $ids);
			}
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			PHPFunctions::jsonReturn('SUCCESS', $e->getMessage());
		}
		
		PHPFunctions::jsonReturn('WARNING', 'No port data');
	}

	public function dashboard ()
	{
		try
		{
			$body['ports'] = $this->intranet->getDashboardPorts($this->session->userdata('userid'), $this->session->userdata('company'));
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
        $this->load->view('intranet/dashboard', $body);
	}

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function login ($company = 0)
    {
        $header['headscript'] = $this->functions->jsScript('intranet.js');

        $header['onload'] = "intranet.loginInit();";

		$header['container'] = 'container';
		
		$header['css'] = " #site-wrapper{ height:100%; }";
        $header['login'] = true;
        $header['hide'] = true;
        $footer['hide'] = false;
        
            try
            {
	            if ($_POST)
				{
	                $check = $this->intranet->checkLogin($_POST['email'], $_POST['password']);

	                if ($check === false)
	                {
	                    // invalid login
	                    PHPFunctions::redirect("/intranet/login?email=" . urlencode($_POST['email']) . "&site-error=" . urlencode("Invalid username or password"));
	                }
	                else
	                {
	                	$company = $_POST['company'];
	                
	                	if (empty($company))
	                	{
	    	                // gets thier default inital company
		                    $company = $this->companies->getHomeLocation($check->id);            	
	                	}
	
	                    // gets the employees position for that company
	                    $position = $this->users->getPosition($check->id, $company);
	
	
	                    // gets the users department for that company
	                    $department = $this->users->getDepartment($check->id, $company);
                        
	                    $this->functions->setLoginSession($check->id, $_POST['email'], $company, $position, $department, (bool) $_POST['rememberMe'], true, $check->admin);

                        // user tried accessing a page while not logged in - takes them back to that page instead of landing
	                    if (!empty($_POST['ref'])) PHPFunctions::redirect('/' . $_POST['ref']);
                        
	                    PHPFunctions::redirect('/msg');	
	
	                }
	                
                } // end of IF post

				// if user is logged in already, takes them back to their landing page
				if ($this->session->userdata('logged_in'))
				{
					PHPFunctions::redirect('/msg');	
				}


		        if (!empty($this->session->userdata('email'))) $body['email'] = $this->session->userdata('email');
        
		        if (!empty($_GET['email'])) $body['email'] = urldecode($_GET['email']);
		        
		        
		        
		        $body['company'] = $this->companies->getCompanyByHost();
		        
		        if (!empty($this->session->userdata('company'))) $body['company'] = $this->session->userdata('company');
		        
		        if (!empty($_GET['company'])) $body['company'] = urldecode($_GET['company']);

				$header['company'] = $body['company'];

		        // a company is set, gets its info
		        if (!empty($body['company']))
		        {
			        $body['compName'] = $this->companies->getTableValue('companyName', $body['company']);
			        $body['compLogo'] = $this->companies->getTableValue('logo', $body['company']);
                }
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
            }


        $this->load->view('template/header', $header);
        $this->load->view('intranet/login', $body);
        $this->load->view('template/footer', $footer);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function loginas ($user)
    {
        // page will not cache
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        // checks if they are an admin first
        if ($this->session->userdata('admin') !== true)
        {
            header("Location: /intranet/landing?site-error=" . urlencode("You do not have permissions to access that functionality"));
            exit;
        }

        try
        {
            if (empty($user)) throw new Exception("User ID is empty!");

            // destorys entire session
            // $this->session->sess_destroy();

            $info = $this->users->getRow($user);

            // gets thier default inital company
            $company = $this->companies->getHomeLocation($user);

            // gets the employees position for that company
            $position = $this->users->getPosition($user, $company);


            // gets the users department for that company
            $department = $this->users->getDepartment($user, $company);

            $this->functions->setLoginSession($user, $info->email, $company, $position, $department, true, $info->admin);

            header("Location: /intranet/landing?site-success=" . urlencode("You are now logged in as {$info->firstName} {$info->lastName}."));
            exit;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            show_error("There was an error loading the page!<br><br>{$e->getMessage()}");
        }

    }

    public function logout($ajax = 0, $forceLogout = false)
    {
        // page will not cache
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        
        /*
    	$loggedIn = $this->functions->checkLoggedIn(false);
    	
    	// someone tried to access logout who is not logged in
    	// takes to destroy session page to remove all session data just incase
    	if ($loggedIn == false)
		{
			header("Location: /intranet/destroy");
			exit;		
		}
    */
    
        // $rememberMe = (empty($_COOKIE['rememberEmail'])) ? false : true;

        $email = $this->session->userdata('email');
        $company = $this->session->userdata('company');
        $rememberMe = $this->session->userdata('rememberMe');
        
        if ((bool) $forceLogout == false)
        {

            // checks clockin's
            try
            {
            	/*
                $clockedIn = $this->users->checkAllClockedIn($this->session->userdata('userid'));

                if ($clockedIn !== false)
                {
                    $companyName = $this->companies->getCompanyName($clockedIn);

                    $lastTimepunch = $this->functions->getLastTimePunch();

                    $body['lastpunch'] = $this->users->convertTimezone($this->session->userdata('userid'), $lastTimepunch, "m/d/Y g:i A");

                    $html = $this->load->view('intranet/logout', $body, true);

                    $this->functions->jsonReturn('CLOCKIN', "You are currently clocked in for <strong>{$companyName}</strong>! Last Time Punch: {$body['lastpunch']}", null, $html);
                    
                }
				*/
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }

        }


		$this->session->unset_userdata($this->config->item('logout_unset'));


        // destorys entire session
        //$this->session->sess_destroy();

        /*
        // sets cookie to remember email address
        if ($rememberMe == true)
        {
            setcookie("rememberEmail", $email, time() + 604800);
        }
         */

		
		if ($rememberMe)
		{
			$data = array
			(
				'company' => $company,
				'email' => $email
			);
			
			$this->session->set_userdata($data);
		}       
		        

        if ((bool) $ajax == true)
        {
            PHPFunctions::jsonReturn('SUCCESS', "You have logged out!", true, 0, array('email' => $email, 'company' => $company));
        }
        else
        {
            PHPFunctions::redirect("/intranet/login?site-alert=" . urlencode("You have logged out!") . '&email=' . urlencode($email) . '&company=' . $company);
            //PHPFunctions::redirect("/intranet/login?site-alert=" . urlencode("You have logged out!"));

        }
    }
    
    public function destroy ()
    {
        // page will not cache
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        
	    $this->session->sess_destroy();
	    header("Location: /intranet/login");
	    exit;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function landing()
    {

        $header['headscript'] = $this->functions->jsScript('intranet.js');

        $header['perfectscroll'] = true;
        $header['datatables'] = true;
        $header['googleMaps'] = true;
        $header['contextmenu'] = true;


        $header['onload'] = "intranet.landingInit();";


        try
        {
            $body['widgets'] = $this->widgets->getWidgets();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('intranet/landing', $body);
        $this->load->view('template/footer_intranet');

    }

    /**
     * Deprecated
     */
    /*
    public function widgetrightclick ($id)
    {

        $body['id'] = $id;

        try
        {
            $body['widgets'] = $this->widgets->getWidgets();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('intranet/widgetrightclick', $body);
    }
    */

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savewidgetportlet ()
    {
        if ($_POST)
        {
            try
            {
                // check if widget is already assigned
                // $check = $this->intranet->checkWidgetAssigned($_POST['widget']);

                    $id = $this->intranet->saveWidgetPortlet($_POST['portlet'], $_POST['widget']);
                    $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function clearportlet ()
    {
        if ($_POST)
        {
            try
            {
                $this->intranet->clearPortlet($_POST['port']);

                $this->functions->jsonReturn('SUCCESS', "Portlet has been cleared");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function switchcompany ($company)
    {
        try
        {
            // $check = $this->intranet->checkUserAssignedToCompany($company);
            $check = $this->companies->userAssignedToCompany($this->session->userdata('userid'), $company);

            if ($check === false) throw new Exception("You do not have access to this company");

            // gets position
            $position = $this->users->getPosition($this->session->userdata('userid'), $company);

            // posiition is not set, this is required
            if (empty($position))
            {
                // header("Location: /intranet/landing?site-error=" . urlencode("You are not assigned a position in this company!"));
                // exit;
            }


            // gets the users department for that company
            $department = $this->users->getDepartment($check->id, $company);

            // department is nto set, this is required
            if (empty($department))
            {
                // header("Location: /intranet/landing?site-error=" . urlencode("You are not assigned to a department in this company!"));
                // exit;
            }

            // clears out sales terminal cart
            $this->session->unset_userdata('transaction');

            // switches company cookie
            $this->session->set_userdata('company', (int) $company);

            // sets position for this company
            $this->session->set_userdata('position', (int) $position);

            // sets department for thsi company
            $this->session->set_userdata('department', (int) $department);


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was a problem switching companies<br>" . $e->getMessage()));
            exit;
        }


	    // checks if the company has a specifind landing URL other than default
	    $landingUrl = $this->companies->getTableValue('landingUrl', $company);
	    
	    if (!empty($landingUrl))
	    {
	        header("Location: " . $landingUrl);
	        exit;
	    }

        header("Location: " . $this->config->item('defaultLandingUrl'));
        exit;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveportorder ()
    {
        if ($_POST)
        {
            // print_r($_POST);

            try
            {
                // $this->intranet->updatePortletOrder($_POST);
                
                $this->intranet->updatePortlet($_POST['port'], $_POST['widget']);
                $this->functions->jsonReturn('SUCCESS', "order has been updated");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function landingrightclick ()
    {
        try
        {
            $body['widgets'] = $this->widgets->getWidgets();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('intranet/landingrightclick', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function reqcheck ()
    {
        // loads required models
        $this->load->model('department_model', 'department', true);
        $this->load->model('position_model', 'position', true);
        $this->load->model('user_model', 'user', true);
        $this->load->model('module_model', 'module', true);

        // reload flag, should something be adjust during the check
        // JS will require the user to reload the page
        $reload = false;

        try
        {
            // checks number of departments for the company
            $depCnt = $this->departments->getDepartments(0, true);

            if (empty($depCnt))
            {
                $data = array
                    (
                        'name' => 'Corporate',
                        'active' => 1
                    );

                // corproate department has been created
                $corpDepID = $this->department->createDepartment($data);

                $reload = true;
            }

            // gets current department user is assigned to
            $currentDepartment = $this->users->getDepartment();

            if ($currentDepartment === false)
            {

                if (!isset($corpDepID))
                {
                    $body['departments'] = $this->departments->getDepartments();

                    $view = $this->load->view('intranet/selectdepartment', $body, true);

                    $this->functions->jsonReturn('SELECT_DEPARTMENT', null, null, $view);
                }
                else
                {
                    // user is not assigned a department, 
                    $this->user->insertCompanyDepartment($this->session->userdata('userid'), $this->session->userdata('company'), $corpDepID);

                    $reload = true;
                }
            }


            // check show many positions have been created
            $posCnt = $this->position->getPositions(0, true);

            if (empty($posCnt))
            {
                $data = array
                    (
                        'name' => 'Administrator',
                        'shortName' => 'Admin',
                        'description' => 'Site Default Administrator'
                    );

                $adminPosId = $this->position->createPosition($data);
            }


            // gets the current position user is assigned to
            $currentPosition = $this->users->getPosition();

            if ($currentPosition === false)
            {
                // positions have been created for the company
                // user needs to select which one they want to be apart of
                if (empty($adminPosId))
                {

                    $body['positions'] = $this->positions->getPositions();

                    $view = $this->load->view('intranet/selectposition', $body, true);

                    $this->functions->jsonReturn('SELECT_POSITION', null, null, $view);
                }
                else
                {

                    // user is not assigned to any position, and there no positions to assign them to
                    // will assign them as "Admin" Position
                    $this->user->insertCompanyPosition($this->session->userdata('userid'), $this->session->userdata('company'), $adminPosId);
                    $reload = true;
                }
            }

            $modules = $this->module->getModules();

            // goes through and checks and assigns all universal modules
            $this->intranet->checkUnvModuleAssign($modules);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }

        if ($reload == true) $this->functions->jsonReturn('RELOAD', "Company settings updated");

        $this->functions->jsonReturn('SUCCESS', 'No company settings were changed');

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function notifications ()
    {

        try
        {
            // gets the count of new notifications
            $body['cnt'] = $cnt = $this->intranet->getNotifications(true);

            if (empty($cnt)) $this->functions->jsonReturn('NONE', 'no notifications');

            $body['notifications'] = $this->intranet->getNotifications();

            $view = $this->load->view('intranet/notifications', $body, true);

            $this->functions->jsonReturn('SUCCESS', $cnt, 0, $view);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function forgotpassword ()
    {
        if ($_POST)
        {
            try
            {
                // get user ID from email address
                $user = $this->users->getIDFromEmail($_POST['fpEmail']);

                if ($user !== false)
                {
                    // gets home company
                    $company = $this->companies->getHomeLocation($user, 1);

                    $requestID = $this->intranet->insertPasswordResetRequest($user, $company);

                    // get user email Address
                    $emailTo = $this->users->getTableValue('email', $user);

                    $companyName = $this->companies->getCompanyName($company);

                    $subject = "Password Reset";

                    $msg = "<h1>Password Reset</h1><p><a href='http://" . $_SERVER['HTTP_HOST'] . "/intranet/resetpassword/?requestID=" . urlencode($requestID)  . "' target='_blank'>Click here to reset your password</a></p>";
                    
                    $this->functions->sendEmail($subject, $msg, $emailTo, 'noreply@cgisolution.com', $companyName);
                }

                $this->functions->jsonReturn('SUCCESS', "An e-mail will be sent to <strong>{$_POST['fpEmail']}</strong> with instructions on how to reset the password if there is an account associated with that e-mail address.", $requestID);


            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function gotonot ($id)
    {
        try
        {
            if (empty($id)) throw new Exception("ID is empty!");


            $url = $this->intranet->getNotificationUrl($id);

            $this->intranet->clearNotification($id);

            header("Location: {$url}");
            exit;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error" . urlencode("There was an error clearing the notification"));
            exit;
        }
    }

    /**
     * TODO: short description.
     */
    public function resetpassword ()
    {
        $header['headscript'] = $this->functions->jsScript('intranet.js');
        $header['onload'] = "intranet.resetpasswordInit();";

        $body['requestID'] = $requestID = urldecode($_GET['requestID']);

        // no requestId was passed
        if (empty($requestID))
        {
            header("Location: /intranet/login");
            exit;
        }

        try
        {
            // first verify requestID is valid
            $user = $this->intranet->getPasswordResetUser($requestID);

            if (empty($user))
            {
                header("Location: /intranet/login?site-alert=" . urlencode("Unable to find a valid password reset request based upon that password request ID"));
                exit;
            }

            // $body['user'] = $user;
            $body['email'] = $this->users->getTableValue('email', $user);

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('intranet/resetpassword', $body);
        $this->load->view('template/footer');
    }

    /**
     * used to process a password request
     */
    public function processpasswordrequest ()
    {
        if ($_POST)
        {
            try
            {
                // first verify requestID is valid
                $user = $this->intranet->getPasswordResetUser($_POST['requestID']);

                if (empty($user)) $this->functions->jsonReturn('ALERT', "Unable to find a valid password reset request based upon that password request ID");

                // first reset password
                $this->intranet->updateUserPassword($user, $_POST['password']);

                // deactivates all password reset requests for that user - they need to fill out the form again to reset their password again
                $this->intranet->deactivatePasswordRequests($user);
                
                $this->functions->jsonReturn('SUCCESS', "Password has been reset");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
    
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function checksubscription ()
    {
        try
        {
            $subStatus = $this->functions->checkSubscriptionStatus($this->session->userdata('company'));

            // print_r($subscription);
 
 
            if ($subStatus == 'EXEMPT')
            {
                $this->functions->jsonReturn('EXEMPT', 'Company is billing exempt');
            }
            else if ($subStatus == 'NO_SUBSCRIPTION_ID')
            {
                $this->functions->jsonReturn('ALERT', 'This company does not have an active subscription.');
            }
            else
            {
                if ($subStatus == 'CANCELED')
                {
                    $this->functions->jsonReturn('ALERT', 'The subscription the company had was canceled. If you have any questions please call us directly at (888) 444-9530.');
    
                    // clear subscriptoin ID from company
                }

            }


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function iframelogin ()
    {
        try
        {
            if ($this->session->userdata('logged_in') == true)
            {
                $this->functions->jsonReturn('LOGGED_IN', "You are currently logged in");
            }
            else
            {
                $email = urldecode($_GET['email']);
                $password = $this->encrypt->decode(urldecode($_GET['password']));
                $company = urldecode($_GET['company']);

                // attempt to login
                $check = $this->intranet->checkLogin($email, $password, true);

                if ($check === false) $this->functions->jsonReturn('ERROR', "Invalid username or password");
                else
                {
                    // gets thier default inital company
                    // $company = $this->companies->getHomeLocation($check->id);

                    // gets the employees position for that company
                    $position = $this->users->getPosition($check->id, $company);


                    // gets the users department for that company
                    $department = $this->users->getDepartment($check->id, $company);

                    $this->functions->setLoginSession($check->id, $email, $company, $position, $department, true, $check->admin);

                    $this->functions->jsonReturn('LOGGED_IN', "You are now logged in");
                }
            }
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function fblogin ()
    {
        if ($_POST)
        {
            try
            {
                // check if there is an account that has that facebookID
                $user = $this->users->getUserIDFromFacebookID($_POST['facebookID']);

                if ($user === false) $this->functions->jsonReturn('ALERT', 'Unable to find an account linked to that account.');
// echo "ID: {$user->id}\n";
                // get users info
                $info = $this->users->getRow($user->id);
// print_r($info);
                // gets thier default inital company
                $company = $this->companies->getHomeLocation($info->id);

                // gets the employees position for that company
                $position = $this->users->getPosition($info->id, $company);

                // gets the users department for that company
                $department = $this->users->getDepartment($info->id, $company);

                $this->functions->setLoginSession($user->id, $info->email, $company, $position, $department, true, $user->admin);

                // user tried accessing a page while not logged in - takes them back to that page instead of landing
                /*
                if (!empty($_POST['ref']))
                {
                    header("Location: /" . $_POST['ref']);
                    exit;
                }
                */

                $this->functions->jsonReturn('SUCCESS', 'You are now logged in!');

                // header("Location: /intranet/landing");
                // exit;
            
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
    
    public function validate ($userid)
    {
	    try
	    {
	    	if ($this->session->userdata('logged_in') !== true) PHPFunctions::jsonReturn('FAIL', 'You must be logged in to validate.');
	    
			
			if ((int) $userid == (int) $this->session->userdata('userid'))
			{
				// get user email
				$email = $this->users->getTableValue('email', $userid);
			
				PHPFunctions::jsonReturn('SUCCESS', 'User ID is valid', true, 0, array('email' => $email));
			}
			else PHPFunctions::jsonReturn('FAIL', 'User ID is NOT valid');
	    
	    }
	    catch (Exception $e)
	    {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
	    }
    }
    
    public function addWidgetHistory ()
    {
	    try
	    {
	    	if ($_POST) $this->widgets->addHistory($_POST['widget'], $_POST['url']);
	  
			PHPFunctions::jsonReturn('SUCCESS', 'History added');
	    }
	    catch (Exception $e)
	    {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
	    }
    }
    
    public function addWidget ()
    {
	    try
	    {
	    	if ($_POST)
	    	{
	    		//$widget = 
		    	PHPFunctions::jsonReturn('SUCCESS', 'Widget Added', true, $widget);
	    	}
	    }
	    catch (Exception $e)
	    {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
	    }
    }
    
    public function welcome ()
    {
    	try
    	{
    		$body['access'] = $access = $this->modules->checkAccess('menu', false);
    		
    		if ($access) $body['menu'] = $this->modules->moduleInfo($this->config->item('menu'));
    	}
    	catch (Exception $e)
    	{
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
    	}

	    $this->load->view('template/header_intranet', $header);
		$this->load->view('intranet/welcome', $body);
        $this->load->view('template/footer_intranet');
    }
    
}
