<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller
{
    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Search ()
    {
        parent::__construct();

        $this->load->model('search_model', 'search', true);

        $this->load->library('msgs');
        $this->load->library('documents');
        $this->load->library('knowledgebase');

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


        $config = array
            (
                'server' => $this->config->item('server'),
                'connect_timeout' => $this->config->item('connect_timeout'),
                'array_result' => $this->config->item('array_result')
            );

        $this->load->library('sphinxsearch', $config);


        // $this->load->spark('sphinx/0.0.1');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

        $this->load->model('crm_model', 'crm', true);
        $this->load->model('formbuilder_model', 'formbuilder', true);
        $this->load->model('video_model', 'video', true);

        $header['headscript'] = $this->functions->jsScript('search.js');
        $header['headscript'] .= $this->functions->jsScript('docs.js');
        $header['headscript'] .= $this->functions->jsScript('kb.js');

        $header['datatables'] = true;
        $header['onload'] = "search.indexInit();docs.indexInit();";

        $body['admin'] = true;

        $body['namespace'] = $namespace = urldecode($_GET['namespace']);


        try
        {
            $body['userResults'] = $this->sphinxsearch->Query($_GET['q'], 'users');
            $body['docResults'] = $this->sphinxsearch->Query($_GET['q'], 'docs');
            $body['msgResults'] = $this->sphinxsearch->Query($_GET['q'], 'test1');
            $body['kbResults'] = $this->sphinxsearch->Query($_GET['q'], 'kb');
            $body['crmResults'] = $this->sphinxsearch->Query($_GET['q'], 'crm');
            $body['formResults'] = $this->sphinxsearch->Query($_GET['q'], 'forms');
            $body['videoResults'] = $this->sphinxsearch->Query($_GET['q'], 'videos');

            // echo "WARNING: " . $this->sphinxsearch->GetLastWarning();
            // echo "<hr>";
            // echo "ERROR: " . $this->sphinxsearch->GetLastError();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('search/index', $body);
        $this->load->view('template/footer_intranet');


    }

}
