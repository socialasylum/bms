<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uploader extends CI_Controller
{
    // public $bcControllerUrl = '/item';
    // public $bcControllerText = 'Item Management';

    // public $bcViewText;


    function Uploader()
    {
        parent::__construct();

        // $this->load->model('uploader_model', 'upload', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $this->load->view('uploader/index', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function upload ()
    {
        try
        {
            // if profile image hass been uploaded
            if (!empty($_FILES['fileUploader']['name']))
            {

                $path = 'public' . DS . 'uploads' . DS . 'uploader' . DS . $this->session->userdata('company') . DS;

                // ensures profileimgs folder is create
                $this->functions->createDir('public' . DS . 'uploads' . DS . 'uploader' . DS);

                // ensures company uploads directory has been created
                $this->functions->createDir($path);

                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "gif|jpg|png";
                $config['max_size'] = "5120";
                $config['encrypt_name'] = true;

                // loads upload library
                $this->load->library('upload', $config);

                // will now upload actual image

                if (!$this->upload->do_upload('fileUploader'))
                {
                    throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                }

                $uploadData = $this->upload->data();

                $fileName = $uploadData['file_name'];
                $this->functions->jsonReturn('SUCCESS', $fileName);
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
        
    }
}
