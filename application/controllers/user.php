<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
    public $bcControllerUrl = '/user';
    public $bcControllerText = "<i class='fa fa-user'></i> Users";

    public $bcViewText;
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function User ()
    {
        parent::__construct();

        $this->load->model('user_model', 'user', true);       
        $this->load->model('stream_model', 'stream', true);
                            
        $this->load->driver('cache');
        $this->load->library('Payment');

        $this->load->library('msgs');

        $this->functions->checkLoggedIn();
		 
        try
        {
            // $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = $this->functions->jsScript('photos.js users.js');
        
        $header['onload'] = "users.indexInit();";

        $header['datatables'] = true;
        $header['typeahead'] = true;
        $header['title'] = 'Users';

        try
        {
            $body['users'] = $this->user->getUsers();
			$body['admin'] = $this->users->isCompanyAdmin($this->session->userdata('userid'), $this->session->userdata('company'));
			
            $body['positions'] = $this->positions->getPositions();
            $body['departments'] = $this->departments->getDepartments();

            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());

        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('users/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createNewUser ()
    {
        if ($_POST)
        {
            try
            {
                $check = $this->users->checkEmailAvailable($_POST['email']);

                if ($check == false) $this->functions->jsonReturn('ALERT', 'E-mail address is already in use!');

                $id = $this->user->createUser($_POST);

                // inserts home company
                $this->user->insertUserCompany($id, $this->session->userdata('company'), true);

                // inserts position
                $this->user->insertCompanyPosition($id, $this->session->userdata('company'), $_POST['position']);

                /*
                 * Make Email Box
                 */
                
                $this->load->model('postfix_model', 'postfix', true);
                $this->postfix->createMailbox($_POST['email'], $_POST['password'], $_POST['firstName'] . ' ' . $_POST['lastName']);
                
                $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id, $tab = 0)
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        $this->load->model('docs_model', 'docs', true);

        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " User";
        
        $header['headscript'] = $this->functions->jsScript('users.js photos.js');

        $header['datatables'] = true;
        $header['onload'] = "users.editInit();";
        $header['ckeditor'] = true;
        $header['nailthumb'] = true;
		$header['justgal'] = true;
		$header['masonry'] = true;
		$header['fancytree'] = true;
		$header['autosize'] = true;
		$header['uploader'] = true;
		$header['bootbox'] = true;
		$header['strength'] = true;

        $body['tab'] = $tab;
        $body['userid'] = $body['id'] = $id;

        $body['completeBilling'] = $_GET['completeBilling'];

        try
        {

            // $this->user->checkCustomerID($id);

            $company = $this->session->userdata('company');

            if  ($company == 35) // product price tracker
            {
                $body['subs'] = array('priceTrackerMonthly');
            }
            else if ($company == 6) // Promote Leads
            {
                $body['subs'] = array('001', '002', '003');
            }

            $body['sig'] = $this->msgs->getSignature($id, $this->session->userdata('company'));
            
            // $body['info'] = $this->user->getUsers($id);
            if (!empty($id)) $body['info'] = $this->users->getRow($id);

            $body['positions'] = $this->positions->getPositions();
            $body['departments'] = $this->departments->getDepartments();
            $body['timezones'] = $this->functions->getTimezonesFull();
            $body['permissions'] = $this->user->getPermissions();
            $body['states'] = $this->functions->getStates();
            $body['liveUrl'] = $this->config->item('liveUrl');
            
            if ($this->users->isAdmin() == true)
            {
                $body['companies'] = $this->companies->getAllCompanies();
            }
            else
            {
                $body['companies'] = $this->companies->getCompanies($this->session->userdata('userid'));
            }

            $body['statuses'] = $this->functions->getCodes(7);
            $body['salTypes'] = $this->functions->getCodes(12);
            $body['salFreq'] = $this->functions->getCodes(13);
            $body['comTypes'] = $this->functions->getCodes(14);
            $body['docPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 2); // checks if user has ability to view documents
            $body['formPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 4); // checks if user has ability to view forms
            $body['emailPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 8); // checks if user has ability to edit email accounts
            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());
            $body['compSettings'] = $this->companies->getSettings($this->session->userdata('company'));
            
            if (!empty($id))
            {
              	$body['albums'] = $this->user->getPhotoAlbums($id);
			  	$body['streamPosts'] = $this->stream->getPosts($id);
                $body['exempt'] = $this->user->isCompanyBillingExempt($id, $this->session->userdata('company'));
				$body['position'] = $this->users->getPosition($id);

	            // gets users current department
	            $body['department'] = $this->users->getDepartment($id, $this->session->userdata('company'));
	            $body['youtubeVideos'] = $this->users->getYouTubeVideos($id);
	            $body['companyAdmin'] = $this->users->isCompanyAdmin($id, $this->session->userdata('company'));
	            $body['companyEditor'] = $this->users->isCompanyEditor($id, $this->session->userdata('company'));
	            $body['token'] = $token = $this->users->getCardTokenByUserID($id);
	            if ($this->session->userdata('company') == 35) $customerID = "user-{$id}";
	            else $customerID = $id;
	            $body['transData'] = $this->payment->getTransactions($customerID);
                
            }
            // error_log("TransData:" . $transData);
    

        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }
        $emailParts = explode('@', $this->session->userdata('email'));
        $body['domain'] = $emailParts[1];
        $this->load->view('template/header_intranet', $header);
        $this->load->view('users/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function update ()
    {
        if ($_POST)
        {
            try
            {
                $_POST['lat'] = $_POST['lng'] = 0;

                // if an address is entered, will attempt to geocode it
                if (!empty($_POST['address']))
                {
                    $address = $_POST['address'];

                    if (!empty($_POST['address2'])) $address .= ' ' . $_POST['address2'];
                    if (!empty($_POST['address3'])) $address .= ' ' . $_POST['address3'];

                    $address .= " {$_POST['city']}, {$_POST['state']} {$_POST['postalCode']}";

                    // will geolcode address
                    $ll = $this->functions->getAddressLatLng($address);

                    $_POST['lat'] = $ll->lat;
                    $_POST['lng'] = $ll->lng;

                }

                if (empty($_POST['id']))
                {
                    // inserts a new user
                    $this->load->model('postfix_model', 'postfix', true);
                    $_POST['id'] = $this->user->createUser($_POST);
                    $this->postfix->createMailbox($_POST['email'] . '@' . $_POST['emaildomain'], $_POST['password']);
                }
                else
                {
                    $this->user->updateUser($_POST);

                    if (!empty($_POST['password']) && ($_POST['password'] === $_POST['confirmPassword']))
                    {
                        // update password
                        $this->user->updateUserPassword($_POST['id'], $_POST['password']);
                        $this->load->model('msg_model', 'msg', true);
                        $this->load->model('postfix_model', 'postfix', true);
                        $this->msg->updatePassword($_POST['email'], $_POST['password']);
                        $this->postfix->updatePassword($_POST['email'], $_POST['password']);
                    }

                }

                $id = $_POST['id'];

                // clears assigned companies
                // $this->user->clearAssignedCompanies($_POST['id']);

                // saves assigned companies
                $this->user->saveAssignedCompanies($_POST['id'], $_POST['company'], true);

                $currentPosition = $this->users->getPosition($_POST['id'], $this->session->userdata('company'));


                if (empty($currentPosition))
                {
                    // insert company position
                    $this->user->insertCompanyPosition($_POST['id'], $this->session->userdata('company'), $_POST['position']);
                }
                elseif ($currentPosition !== $_POST['position'])
                {
                    // update company position
                    $this->user->updateCompanyPosition($_POST['id'], $this->session->userdata('company'), $_POST['position']);
                }

				// if a department was selected
				if (!empty($_POST['department']))
				{				
	                // checks current department
	                $currentDepartment = $this->users->getDepartment($_POST['id'], $this->session->userdata('company'));
	
	                if (empty($currentDepartment))
	                {
	                    // inserts the users department for the company
	                    $this->user->insertCompanyDepartment($_POST['id'], $this->session->userdata('company'), $_POST['department']);
	                }
	                else
	                {
	                    // updates users department
	                    $this->user->updateCompanyDepartment($_POST['id'], $this->session->userdata('company'), $_POST['department']);
	                }
                }

                // will now save if company admin
                $this->user->saveCompanyAdmin($_POST['id'], $this->session->userdata('company'), $_POST['companyAdmin']);

                // saves if they are a company editor
                $this->user->saveCompanyEditor($_POST['id'], $this->session->userdata('company'), $_POST['companyEditor']);

                $this->load->model('msg_model', 'msg', true);
                $this->msg->updateSignature($_POST['id'], $_POST['signature']);
                
				PHPFunctions::jsonReturn('SUCCESS', 'User data has been saved!', true, $_POST['id']);
            }
            catch (Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
    			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }

        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteuser ()
    {
        if ($_POST)
        {
            try
            {
                $name =  $this->users->getName($_POST['id']);

                $this->user->setUserDeleted($_POST['id']);

                $this->functions->jsonReturn('SUCCESS', "User {$name} ({$_POST['id']}) has been deleted!");
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function invite ()
    {
        if ($_POST)
        {
            try
            {
                // first check if e-mail exists
                // $emailCheck = $this->users->checkEmailAvailable($_POST['email']);

                // if ($emailCheck === true) throw new Exception("No user exists by that e-mail address");

                // gets userid from e-mail address
                // $id = $this->users->getIDFromEmail($_POST['email']);

                // if (empty($id)) throw new Exception("Unable to find userid for e-mail {$_POST['email']}");

                $id = $_POST['addedUser'];

                // now check if the user is already assigned to this company
                $assigned = $this->companies->userAssignedToCompany($id, $this->session->userdata('company'));

                // if already assigned
                if ($assigned == true) $this->functions->jsonReturn('ALERT', 'User is already assigned to this company');

                // will now assign user to this company
                $this->user->insertUserCompany($id, $this->session->userdata('company'));

                // save company position for user
                $this->user->insertCompanyPosition($this->session->userdata('user'), $this->session->userdata('company'), $_POST['position']);

                // save company departments for user
                $this->user->insertCompanyDepartment($this->session->userdata('user'), $this->session->userdata('company'), $_POST['department']);

                $this->functions->jsonReturn('SUCCESS', 'User has been invited to this company!');

            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function autocomplete ()
    {
        try
        {
            $users = $this->user->emailSearch($_GET['term']);

            echo json_encode($users);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function widget ()
    {
        try
        {
            $body['users'] = $this->user->getUsers();
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }


        $this->load->view('users/widget', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function profileimg ($size = 50, $userid = 0, $file = null)
    {
        if (empty($userid)) $userid = $this->session->userdata('userid');

        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'profileimgs' . DS;

		if (!empty($file)) $file = urlencode($file);

        try
        {

			if (!empty($userid)) $img = $this->users->getTableValue('profileimg', $userid);

			if (!empty($file)) $img = $file;

			// checks if file exists
			if (!file_exists($path . $img)) $img = null;

            // no profile image is set - loads no profile img
            if (empty($img))
            {
                $path = $_SERVER["DOCUMENT_ROOT"] . DS . 'public' . DS . 'images' . DS;
                $img = 'dude.gif';
            }

            $is = getimagesize($path . $img);

            if ($is === false) throw new exception("Unable to get image size for ({$path}{$img})!");

            $ext = PHPFunctions::getFileExt($img);

            list ($width, $height, $type, $attr) = $is;

                if ($width == $height)
                {
                    $nw = $nh = $size;
                }
                elseif ($width > $height)
                {
                    $scale = $size / $height;
                    $nw = $width * $scale;
                    $nh = $size;
                    $leftBuffer = (($nw - $size) / 2);
                }
                else
                {
                    $nw = $size;
                    $scale = $size / $width;
                    $nh = $height * $scale;
                    $topBuffer = (($nh - $size) / 2);
                }

            $leftBuffer = $leftBuffer * -1;
            $topBuffer = $topBuffer * -1;

            if ($ext == "JPG") $srcImg = imagecreatefromjpeg($path . $img);
            if ($ext == "GIF") $srcImg = imagecreatefromgif($path  . $img);
            if ($ext == "PNG") $srcImg = imagecreatefrompng($path  . $img);

            $destImg = imagecreatetruecolor($size, $size); // new image

            #imagecopyresized($destImg, $srcImg, 0, 0, $leftBuffer, $topBuffer, $nw, $nh, $width, $height);
            imagecopyresized($destImg, $srcImg, $leftBuffer, $topBuffer, 0, 0, $nw, $nh, $width, $height);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }


        #echo "NW: {$nw} | NH: {$nh} | Scale: {$scale} | ";
        #echo "topBuffer: {$topBuffer}";
        #echo "leftBuffer: {$leftBuffer}";
        header('Content-Type: image/jpg');
        imagejpeg($destImg);

        imagedestroy($destImg);
        imagedestroy($srcImg);

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addyoutubeurl ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->user->addYouTubeUrl($_POST['url'], 0, $_POST['user']);
                $this->functions->jsonReturn('SUCCESS', "Youtube video was added!", $id);
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function youtubevideos ($id)
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        try
        {
            $body['youtubeVideos'] = $this->users->getYouTubeVideos($id);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('users/youtubevideos', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveyoutubeorder ()
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        try
        {
            $this->user->updateYoutubeVideoOrder($_POST['videos']);
            $this->functions->jsonReturn('SUCCESS', "Youtube video order was updated");
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteytvideo ()
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        if ($_POST)
        {
            try
            {
                $this->user->deleteYTVideo($_POST['id']);
                $this->functions->jsonReturn('SUCCESS', "Youtube video was deleted!");
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function invoice($tranid, $userid)
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        try
        {
            // gets transaction data
            $body['tranInfo'] = $tranInfo = $this->payment->getTransactionInfo($tranid);

            // gets company for user
            $userCompany = $this->session->userdata('company');

            // gets company data needed for invoice
            $company['name'] = $this->companies->getCompanyName($userCompany);
            $company['address'] = $this->companies->getTableValue('addressLine1', $userCompany);
            $company['address2'] = $this->companies->getTableValue('addressLine2', $userCompany);
            $company['city'] = $this->companies->getTableValue('city', $userCompany);
            $company['state'] = $this->companies->getTableValue('state', $userCompany);
            $company['postal'] = $this->companies->getTableValue('postalCode', $userCompany);
            $company['phone'] = $this->companies->getTableValue('phone', $userCompany);

            // gets customer data needed for invoice
            $customer['name'] = $this->users->getName($userid);
            $customer['address'] = $this->users->getTableValue('addressLine1', $userid);
            $customer['address2'] = $this->users->getTableValue('addressLine2', $userid);
            $customer['city'] = $this->users->getTableValue('city', $userid);
            $customer['state'] = $this->users->getTableValue('state', $userid);
            $customer['postal'] = $this->users->getTableValue('postalCode', $userid);

            // gets data needed for invoice
            $body['company'] = $company;
            $body['customer'] = $customer;

        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('welcome/invoice', $body);
    }

    public function newCard($userid)
    {
        try
        {
            $body['userid'] = $userid;
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('user/newCard', $body);
    }

    public function saveNewCard()
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past


        if ($_POST)
        {
            try
            {
                $_POST['BTcustomerID'] = $this->user->checkCustomerID($_POST['userid']);

                $token = $this->payment->createCreditCard($_POST, false, $_POST['makeDefault']);

                if (!empty($token))
                {
                    $savedToken = $this->users->saveCCToken($_POST['userid'], $_POST['ccNumber'], $token);

                    if (!empty($savedToken))
                    {
                        try
                        {
                            $userName = $this->users->getName($_POST['userid']);
                            $this->functions->jsonReturn('SUCCESS', "Congrats {$userName}, your card has been saved!");
                        }
                        catch(Exception $e)
                        {
                            PHPFunctions::sendStackTrace($e);
                            $this->functions->jsonReturn('ERROR', $e->getMessage());
                        }
                    }
                }
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function deleteCC($token, $userid)
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        $deletedDB = null;
        $deletedBT = null;

        try
        {
            $deletedDB = $this->users->deleteCCfromDB($token);

            $CCexists = $this->payment->checkBTforCC($token);

            if (!empty($CCexists))
            {
                $deletedBT = $this->payment->deleteCCfromBT($token);
            }
            else
            {
                $deletedBT = 'SUCCESS';
            }
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        if ($deletedDB == 'SUCCESS' && $deletedBT == 'SUCCESS')
        {
            header("Location: /user/edit/{$userid}");
            exit;
        }
        else
        {
            header('Location: /intranet/landing');
            exit;
        }
    }

    public function cancelsub ()
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        try
        {
            if (empty($_POST['user'])) $this->functions->jsonReturn('ALERT', "User ID was not passed");

            $info = $this->users->getRow($_POST['user']);

            if (empty($info->BrainTreeSubscriptionID)) $this->functions->jsonReturn('ALERT', "{$info->firstName} {$info->lastName} has no active Subscriptions");

            $this->payment->cancelSubscription($info->BrainTreeSubscriptionID);

            // clear users subscription information
            $this->users->updateSubscriptionInfo ($_POST['user'], null, 0);

            $this->functions->jsonReturn('SUCCESS', 'Subscription has been cancelled!');
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * Primary Ajax function used for checking subscriptions
     *
     * @return TODO
     */
    public function checksubscription  ($user, $company)
    {
        header('Access-Control-Allow-Origin:*');

        try
        {
            $user = intval($user);

            if (empty($user)) $this->functions->jsonReturn('ERROR', 'User ID is empty!');


            // checks if they are exempt
            $exempt = $this->user->isCompanyBillingExempt($user, $company);

            if ($exempt == true) $this->functions->jsonReturn('EXEMPT', "This user is exempt from billing");

            $subID = $this->users->getTableValue('BrainTreeSubscriptionID', $user);

            $subscription = $this->payment->getSubscriptionByID($subID);

            $status = strtoupper($subscription->status);

            $this->functions->jsonReturn($status);
        }
        catch (Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function setbillingexempt ()
    {
        try
        {
            if ($_POST['exempt'] == 1) // set to exempt
            {
                $this->user->insertExempt($_POST['user'], $this->session->userdata('company'));
                
                $msg = "Billing exempt has been set";
            }
            else // non-exempt
            {
                // delets exempt row
                $this->user->clearExempt($_POST['user'], $this->session->userdata('company'));

                $msg = "Billing exempt has been cleared.";
            }

            $this->funcitons->jsonReturn('SUCCESS', $msg);
        }
        catch (Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function getcardsonfile ($id)
    {
        try
        {
            $body['token'] = $token = $this->users->getCardTokenByUserID($id);
        }
        catch (Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('users/cardsOnFile', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createsubscription ()
    {
        if ($_POST)
        {
            try
            {
                $subscriptionID = $this->payment->createSubscription($this->session->userdata('company'), $_POST['token'], $_POST['plan'], 0, false, true);

                $this->users->updateSubscriptionInfo($_POST['user'], $subscriptionID, 9.99);

                $this->functions->jsonReturn('SUCCESS', 'Subscription has been created!');
            }
            catch (Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function connectfb ()
    {
        if ($_POST)
        {
            try
            {
                $this->user->updateFacebookID($this->session->userdata('userid'), $_POST['facebookID']);
                $this->functions->jsonReturn('SUCCESS', 'Account is now linked with Facebook!');
            }
            catch (Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function fbunlink ()
    {
        if ($_POST)
        {
            try
            {
                $this->user->updateFacebookID($_POST['user'], 0);
                $this->functions->jsonReturn('SUCCESS', 'Account is now unlinked with Facebook!');
            }
            catch (Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function fbcallback ()
    {
        try
        {
            $signed_request = $_REQUEST['signed_request'];

            list($encoded_sig, $payload) = explode('.', $signed_request, 2);

            // decode the data
            $sig = $this->functions->base64_url_decode($encoded_sig); // Use this to make sure the signature is correct

            $data = json_decode($this->functions->base64_url_decode($payload), true);

            $facebookID = $data['user_id'];

            //error_log("Deuahtorizing Facebook ID: {$facebookID}");

            if (!empty($facebookID)) $this->user->clearFacebookID($facebookID);
        }
        catch (Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savefbphoto ()
    {
        if ($_POST)
        {
            try
            {
                // will first downlaod picture
                $this->user->downloadfbphoto($this->session->userdata('userid'), $_POST['url']);

                $this->functions->jsonReturn('SUCCESS', 'Profile image has been saved!');
            }
            catch (Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function completebilling ()
    {
        try
        {
            // gets plans from braintree 
            // promote leads
            if ((int) $this->session->userdata('company') == 6)
            {
                // $body['allowedPlans'] = array('priceTrackerMonthly');
            }

            // product price trackier
            if ((int) $this->session->userdata('company') == 35)
            {
                $body['allowedPlans'] = array('priceTrackerMonthly');
            }

            $body['plans'] = $this->payment->getPlans();

            $body['name'] = $this->users->getName($this->session->userdata('userid'));
            $body['companyName'] = $this->companies->getCompanyName($this->session->userdata('company'));
        }
        catch (Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('users/completeBilling', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savecompletedbilling ()
    {
        if ($_POST)
        {
            try
            {
                $_POST['BTcustomerID'] = $_POST['userid'] = $this->session->userdata('userid');


                if ($this->session->userdata('company') == 35) // Price Tracker
                {
                    $_POST['BTcustomerID'] = "user-{$_POST['userid']}";
                }

                // creates CC info in BT and returns token
                $token = $this->payment->createCreditCard($_POST, false);

                if (empty($token)) $this->functions->jsonReturn('ALERT', 'Unable to save credit card');

                // will now save token
                $this->users->saveCCToken($_POST['userid'], $_POST['ccNumber'], $token);

                // creates subscription
                $subscription = $this->payment->createSubscription($_POST['userid'], $token, $_POST['planID'], 0, false, true);

                // something went wrong saving subscription to BrainTree
                if (empty($subscription)) $this->functions->jsonReturn('ALERT', 'Unable to create subscription! Card was not charged!');

                // saves subscription ID
                $this->users->updateSubscriptionInfo($_POST['userid'], $subscription);

                // updates user to Active Status
                $this->user->updateStatus($_POST['userid'], 1);
                
                $this->functions->jsonReturn('SUCCESS', "Billing information saved, your account is now active!");
            }
            catch (Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
   
    /*
     * used to update
     */
    public function externalimgupload ($userid)
    {
    
    	try
    	{
    		if ($_FILES)
            {
                // upload profile img

                // if profile image hass been uploaded
                if (!empty($_FILES['file']['name']))
                {
                    $this->load->model('companysettings_model', 'companysettings', true);

                    $path = 'public' . DS . 'uploads' . DS . 'profileimgs' . DS;

                    // ensures profileimgs folder is create
                    // $this->functions->createDir('public' . DS . 'uploads' . DS . 'profileimgs' . DS);

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);

                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "gif|jpg|png";
                    $config['max_size'] = "5120";
                    $config['encrypt_name'] = true;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image

                    if (!$this->upload->do_upload('file'))
                    {
                        throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();
                    
                    if (!empty($_POST['userid'])) $this->user->updateProfileImg($_POST['userid'], $uploadData['file_name']);
                    
                    $this->functions->jsonReturn('SUCCESS', "Image has been uploaded!", true, 0, array('file' => $uploadData['file_name']));

                }
            }
    	}
    	catch (Exception $e)
    	{
    		PHPFunctions::sendStackTrace($e);
    		$this->functions->jsonReturn('ERROR', $e->getMessage());
    	}

    }
    
    public function albumphotoupload ()
    {
	    if ($_FILES)
	    {
		    try
		    {
		        if (!empty($_FILES['file']['name']))
                {
                    $path = 'public' . DS . 'uploads' . DS . 'userimgs' . DS . $_POST['userid'] . DS;

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);

                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "gif|jpg|png";
                    $config['max_size'] = "5120";
                    $config['encrypt_name'] = false;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image

                    if (!$this->upload->do_upload('file'))
                    {
                        throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();

                    $this->functions->jsonReturn('SUCCESS', "Image has been uploaded!");

                }
		    }
		    catch (Exception $e)
		    {
		    	PHPFunctions::sendStackTrace($e);
				$this->functions->jsonReturn('ERROR', $e->getMessage());
		    }
	    }
    }
    
    public function albumphoto ($user, $file, $size = 80)
    {

    	$file = str_replace('_t.', '.', $file);
    	$file = str_replace('_m.', '.', $file);    
    	$file = str_replace('_n.', '.', $file);
    	$file = str_replace('_z.', '.', $file);
    	$file = str_replace('_c.', '.', $file);
    	$file = str_replace('_b.', '.', $file);

        $path = $_SERVER['DOCUMENT_ROOT'] . 'public' . DS . 'uploads' . DS . 'userimgs' . DS . $user . DS;

        $config['image_library'] = 'gd2';
        $config['source_image']= $path . $file;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['dynamic_output'] = true;
        $config['width'] = $size;
        $config['height'] = $size;

        $this->load->library('image_lib', $config);

        $this->image_lib->resize();
    }
    
    public function viewalbum ($album, $userid)
    {
    	$body['userid'] = $userid;
    	
	    try
	    {
			$body['photos'] = $this->user->getAlbumPhotos($album);
			$body['info'] = $this->user->getPhotoAlbums($userid, $album);
	    }
	    catch (Exception $e)
	    {
	    	PHPFunctions::sendStackTrace($e);	    
	    }
	    
	    $this->load->view('users/viewalbum', $body);
    }

	public function emailtree ($userid)
	{
		try
		{
			$body['emailAccounts'] = $this->msgs->getAdditionalAccounts($userid, array(1,2));

			// gets number of outgoing servers
			$body['outCnt'] = $this->msgs->getAdditionalAccounts($userid, 3, true);
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStacktrace($e);
		}
		
		$tree = $this->load->view('users/edit/email_tree', $body);
		
		
		//PHPFunctions::jsonReturn('SUCCESS', null, true, 0, array('tree' => $tree));
		
	}
	
	public function email_new ()
	{
		try
		{
			$body['ssl'] = $this->functions->getCodes(35);
			$body['auth'] = $this->functions->getCodes(36);
            
            $types = $this->functions->getCodes(20);
            
            unset($types[2]); //unsets SMTP
            
            $body['types'] = $types;
            
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStacktrace($e);
		}

		$this->load->view('users/edit/email_new', $body);
	}
	
	public function email_settings ($userid, $id, $page)
	{
		$page = urldecode($page);
		
		try
		{
			$body['ssl'] = $this->functions->getCodes(35);
			$body['auth'] = $this->functions->getCodes(36);

            $types = $this->functions->getCodes(20);
            
            unset($types[2]); //unsets SMTP
            
            $body['types'] = $types;
            
            if (!empty($id))
            {
	            $info = $this->msgs->getAccountInfo($id);
	            
	            $info->passwd = $this->encrypt->decode($info->passwd);
	            
	            $info->passwd = str_repeat('*', strlen($info->passwd)); // replaces passwd with asterisks
	            
	            $body['info'] = $info;
            }
            
            $body['outgoingServers'] = $this->msgs->getAdditionalAccounts($userid, 3);
            
			$body['sig'] = $this->msgs->getSignature($userid, $this->session->userdata('company'));
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStacktrace($e);
		}
		
		$this->load->view('users/edit/' . $page, $body);
	}
	
	public function email_info ($userid, $id)
	{
	
		header('Content-Type: application/json');
		
		try
		{
			$info = $this->msgs->getAccountInfo($id);
			
			if ($userid !== $info->userid) throw new Exception("User ID did not match!");
		
			$passwd = $this->encrypt->decode($info->passwd);
		
			$info->passwd = str_repeat('*', strlen($passwd));
			#$info->passwd = $passwd;
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStacktrace($e);
			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
			
		}
		
		PHPFunctions::jsonReturn('SUCCESS', null, true, 0, array('info' => $info));
	}
	
	public function update_email_signature ()
	{
		try
		{
			if ($_POST)
			{
				$this->load->model('msg_model', 'msg', true);
				
				if (empty($_POST['id'])) $this->msg->insertSignature($_POST['userid'], $_POST['signature']);
				else $this->msg->updateSignature($_POST['userid'], $_POST['signature']);
				
				PHPFunctions::jsonReturn('SUCCESS', "Signature has been saved");
			}
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStacktrace($e);
			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
}
