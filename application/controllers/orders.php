<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller
{

    public $bcControllerUrl = '/orders';
    public $bcControllerText = "<i class='fa fa-clipboard'></i> Orders";

    public $bcViewText;


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Orders()
    {
        parent::__construct();

        $this->load->model('orders_model', 'orders', true);
        $this->load->model('crm_model', 'crm', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = $this->functions->jsScript('orders.js');

        $header['onload'] = "orders.indexInit();";
        $header['datatables'] = true;

        try
        {
            $body['orders'] = $this->orders->getOrders();
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('orders/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function details ($id)
    {

        if (empty($id))
        {
            header("Location: /orders?site-error=" . urlencode("Order ID is empty!"));
            exit;
        }

        $header['headscript'] = $this->functions->jsScript('orders.js');

        $header['onload'] = "orders.detailsInit();";
        $header['ckeditor'] = true;


        $body['id'] = $id;

        $this->load->model('item_model', 'item', true);

        $this->bcViewText = "<i class='fa fa-list-alt'></i> Order Details";

        try
        {
            $body['info'] = $info = $this->orders->getOrderInfo($id);
            $body['items'] = $this->orders->getOrderItems($id);

            $body['crmContact'] = $this->crm->getContactInfo($info->crmContact);

            $body['shippingAddress'] = $this->crm->getAddressByID($info->crmShippingAddress);
            $body['billingAddress'] = $this->crm->getAddressByID($info->crmBillingAddress);

            $body['notes'] = $this->orders->getNotes($id);

            $body['status'] = $this->functions->getCodes(22);
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('orders/details', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function update ()
    {
        if ($_POST)
        {
            try
            {
                // saves note if there is one
                if (!empty($_POST['note']))
                {
                    $this->orders->insertNote($_POST['id'], $_POST['note']);
                }

                // updates order such as status
                $this->orders->updateOrder($_POST);

                $this->functions->jsonReturn('SUCCESS', 'Order has been updated!');
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
