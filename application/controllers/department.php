<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends CI_Controller
{

    public $bcControllerUrl = '/department';
    public $bcControllerText = "<i class='fa fa-sitemap'></i> Departments";

    public $bcViewText;


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Department()
    {
        parent::__construct();

        $this->load->model('department_model', 'department', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/departments.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "departments.indexInit();";
        $header['datatables'] = true;

        $body['nav'] = 'index';

        try
        {
            $body['departments'] = $this->department->getDepartments();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('departments/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id)
    {

        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/departments.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";


        $header['onload'] = "departments.editInit();";

        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Department";

        $body['status'] = array('Deactived', 'Active');

        $body['id'] = $id;

        try
        {
            if (!empty($id)) $body['info'] = $this->department->getDepartments($id);
            print_r($info);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('departments/edit', $body);
        $this->load->view('template/footer_intranet');

    }
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                // clear all previous default departments if default has been checked
                if (!empty($_POST['default']))
                {
                    $this->department->clearDefaultDepartment($this->session->userdata('company'));
                }

                if (empty($_POST['id']))
                {
                    $_POST['id'] = $this->department->createDepartment($_POST);
                    $msg = "Department has been created!";
                }
                else
                {
                    $this->department->updateDepartment($_POST);
                    $msg = "Department has been updated!";
                }
                $this->functions->jsonReturn('SUCCESS', $msg, $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * Ajax functiont to assign a department for a user
     *
     * @return JSON response
     */
    public function assign ()
    {
        if ($_POST)
        {
            $this->load->model('user_model', 'user', true);

            try
            {
                $this->user->insertCompanyDepartment($this->session->userdata('userid'), $this->session->userdata('company'), $_POST['department']);

                $this->functions->jsonReturn('SUCCESS', 'Department has been assigned');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
