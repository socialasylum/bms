<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller
{

    public $bcControllerUrl = '/calendar';
    public $bcControllerText = "<i class='fa fa-calendar'></i> Calendar";

    public $bcViewText;


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Calendar()
    {
        parent::__construct();

        $this->load->model('calendar_model', 'calendar', true);

        $this->load->driver('cache');

        $loginCheck = $this->functions->checkLoggedIn();


		if ($loginCheck !== false)
		{
	        try
	        {
	            $this->modules->checkAccess($this->router->fetch_class(), true);
	        }
	        catch(Exception $e)
	        {
	            $this->functions->sendStackTrace($e);
	            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
	            exit;
	        }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($month = null, $year = null)
    {
        $header['headscript'] = $this->functions->jsScript('calendar.js');
        $header['googleMaps'] = true;
        $header['onload'] = "cal.indexInit();";
        $header['timeentry'] = true;

        try
        {
            $body['month'] = $month = (empty($month)) ? date("n") : $month;
            $body['year'] = $year = (empty($year)) ? date("Y") : $year;

           // $body['navbarBrand'] = "<a href='#' class='navbar-brand'>" . date("F Y", mktime(0, 0, 0, $month, 1, $year)) . "</a>";


            $body['editPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 1); // permission to edit events
            
            $body['events'] = $this->calendar->getEvents();
            
            $this->bcViewText = date("F Y", mktime(0, 0, 0, $month, 1, $year));
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('calendar/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($month, $year, $id = 0)
    {
        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Event";

        $header['headscript'] = $this->functions->jsScript('calendar.js');
        $header['onload'] = "cal.editInit();";
        $header['ckeditor'] = true;
        $header['timeentry'] = true;
        $header['colorpicker'] = true;

        $body['month'] = $month;
        $body['year'] = $year;

        $body['id'] = $id;

        try
        {
            $body['ampm'] = array('AM', 'PM');

            $body['locations'] = $this->locations->getLocations($this->session->userdata('company'));


        	$body['repeatTypes'] = $this->functions->getCodes(30, 0, 'code');
        	$body['expLvls'] = $this->functions->getCodes(31, 0, 'code');
        
        	if (!empty($id))
        	{
	        	$body['info'] = $info = $this->calendar->getEventInfo($id);
	        	
	        	$body['videos'] = $this->calendar->getEventVideos($id);
	        	$body['files'] = $this->calendar->getEventFiles($id);
	        	
	        	$body['fromDate'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->fromDate, "Y-m-d");
	        	$body['fromTime'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->fromDate, "h:i:A");
				
	        	$body['toDate'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->toDate, "Y-m-d");
	        	$body['toTime'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->toDate, "h:i:A");
	        	
        	}
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('calendar/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
		if ($_POST)
		{
			try
			{
				if (empty($_POST['id']))
				{
					$event = $this->calendar->insertEvent($_POST);
				}
				else
				{
					$event = $_POST['id'];
					
					$this->calendar->updateEvent($_POST);
					
					// clear out previous saved videos
					$this->calendar->clearVideos($event);
				}			
			}
			catch (Exception $e)
			{
				$this->functions->sendStackTrace($e);
				header("Location: /calendar/index/{$_POST['month']}/{$_POST['year']}?site-error=" . urlencode('Database error saving event!'));
				exit;
			}
			
			// saves uploaded files
			if ($_FILES)
			{
				try
				{
					$path = 'public' . DS . 'uploads' . DS . 'events' . DS . $event . DS; 
	
	                // ensures company uploads directory has been created
	                $this->functions->createDir($path);
	
	                $config['upload_path'] = './' . $path;
	                $config['allowed_types'] = "gif|jpg|png|xls|xlsx|doc|docx|pdf|csv|txt";
	                $config['max_size'] = "5120";
	                $config['encrypt_name'] = true;
	
	                // loads upload library
	                $this->load->library('upload', $config);
	                
                    foreach ($_FILES['file']['name'] as $k => $file)
                    {
                        //$imgPostID = 0;
						//error_log("Image Name: {$k}:{$img}");

                        if (!empty($file))
                        {
                        	$_FILES['userfile']['name'] = $_FILES['file']['name'][$k];
					        $_FILES['userfile']['type'] = $_FILES['file']['type'][$k];
					        $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$k];
					        $_FILES['userfile']['error'] = $_FILES['file']['error'][$k];
					        $_FILES['userfile']['size'] = $_FILES['file']['size'][$k];
                        
                            if (!$this->upload->do_upload('userfile'))
                            {
                                throw new Exception("Unable to upload file!" . $this->upload->display_errors());
                            }

                            $uploadData = $this->upload->data();
 
							$fileID = $this->calendar->insertFile($event, $uploadData['file_name'], $_FILES['file']['name'][$k]);
                        }
                    }
	                
				}
				catch (Exception $e)
				{
					$this->functions->sendStackTrace($e);
				}
			}
			
			// will now upload videos
			if (!empty($_POST['videoUrl']))
			{
				foreach ($_POST['videoUrl'] as $k => $v)
				{
					if (!empty($v))
					{
						try
						{
							$this->calendar->insertVideo($event, $v);
						}
						catch (Exception $e)
						{
							$this->functions->sendStackTrace($e);
						}	
					}
				}
			}
			

			header("Location: /calendar/index/{$_POST['month']}/{$_POST['year']}?site-success=" . urlencode('Event has been saved!'));
			exit;
		}
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function eventmodal ($id, $month, $year)
    {
        $body['id'] = $id;
		$body['month'] = $month;
		$body['year'] = $year;

        try
        {
            $body['info'] = $info = $this->calendar->getEventInfo($id);
            $body['editPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 1); // permission to edit events

            if (empty($info->location))
            {
                $body['location'] = $info->otherLocation;

                $body['lat'] = $info->lat;
                $body['lng'] = $info->lng;
            }
            else
            {
                $body['location'] = $this->locations->getTableValue('name', $info->location);

                // gets lat/lng from location
                $body['lat'] = $this->locations->getTableValue('lat', $info->location);
                $body['lng'] = $this->locations->getTableValue('lng', $info->location);
            }

            if ($info->allDay == 1)
            {
                $body['from'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->fromDate, "m/d/Y") . ' All day';
                $body['to'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->toDate, "m/d/Y") . ' All day';
            }
            else
            {
                $body['from'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->fromDate, "m/d/Y g:i A");
                $body['to'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->toDate, "m/d/Y g:i A");
            }

            $body['createdBy'] = $this->users->getName($info->userid);
            
            if ($info->repeat == 1)
            {
	            $body['repeatSummary'] = $this->calendar->repeatSummary($id);
            }
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('calendar/eventmodal', $body);
    }

	public function externalfileupload ()
	{
		try
		{
			if ($_FILES)
			{
				if (!empty($_FILES['file']['name']))
                {
                    $path = 'public' . DS . 'uploads' . DS . 'events' . DS . $_POST['event'] . DS;

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);

                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "gif|jpg|png|xls|xlsx|doc|docx|pdf|csv|txt";
                    $config['max_size'] = "5120";
                    $config['encrypt_name'] = false;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image

                    if (!$this->upload->do_upload('file'))
                    {
                        throw new Exception("Unable to upload external file for event!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();
                    
                    $this->functions->jsonReturn('SUCCESS', "Image has been uploaded!");

                }
			}
		}
		catch (Exception $e)
		{
			$this->functions->sendStacktrace($e);
    		$this->functions->jsonReturn('ERROR', $e->getMessage());
		}
	}

    public function deletefile ()
    {
	    if ($_POST)
	    {
		    try
		    {
		    	$this->calendar->deleteFile($_POST['id'], $_POST['event']);
		    	
		    	$this->functions->jsonReturn('SUCCESS', 'File has been deleted!');
		    }
		    catch (Exception $e)
		    {
		    	$this->functions->sendStackTrace($e);
		    	$this->functions->jsonReturn('ERROR', $e->getMessage());
		    }
	    }
    }
    
    public function deletevideo ()
    {
	    if ($_POST)
	    {
		    try
		    {
		    	$this->calendar->clearVideoByID($_POST['id']);
		    
		    	$this->functions->jsonReturn('SUCCESS', 'Video has been deleted!');
		    }
		    catch (Exception $e)
		    {
		    	$this->functions->sendStackTrace($e);
		    	$this->functions->jsonReturn('ERROR', $e->getMessage());
		    }
	    }
    }
    
    public function deleteevent ()
    {
	    if ($_POST)
	    {
		    try
		    {
		    	$this->calendar->deleteEvent($_POST['id']);
		    	
		    	$this->functions->jsonReturn('SUCCESS', 'Event has been deleted!');
		    }
		    catch (Exception $e)
		    {
		  	 	$this->functions->sendStackTrace($e);
		    	$this->functions->jsonReturn('ERROR', $e->getMessage());
		    }
	    }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function dt ()
    {
        echo date_default_timezone_get();
    }
}
