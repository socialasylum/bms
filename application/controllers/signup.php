<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller
{

    function signup()
    {
        parent::__construct();
        $this->load->model('signup_model', 'signup', true);
        $this->load->driver('cache');
    }

    public function index()
    {
        $header['signup'] = true;
        $this->load->view('template/header', $header);
        $this->load->view('signup/index');
        $this->load->view('template/footer');
    }



    public function moduledivs($type)
    {
        $body['type'] = $type;
        $this->load->view('signup/moduledivs', $body);
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createaccount ()
    {
        if ($_POST)
        {
            $this->load->model('user_model', 'user', true);
            //$this->load->model('pricing_model', 'pricing', true);
            $this->load->model('position_model', 'position', true);
            $this->load->model('department_model', 'department', true);
            $this->load->model('postfix_model', 'postfix', true);
            $this->load->library('msgs');
            try
            {
                // ensures t hey agree with well tos etc.
                //if (empty($_POST['agree'])) throw new Exception("User did agree to terms of service, privacy, or return policy!");

                // removes any type of formating to the CC number to ensure its only a number
                $_POST['cardnum'] = $this->functions->onlyNumbers($_POST['cardnum']);

                // loads BrainTree payment library
                $this->load->library('payment');

                $check = $this->users->checkEmailAvailable($_POST['email'] . '@' . $_POST['emaildomain']);

                if ($check == false) $this->functions->jsonReturn('ALERT', 'E-mail address is already in use!');

                // will now create company
                $_POST['company'] = $companyId = $this->signup->createCompany($_POST);


                // assign purchased modules to company
                //$this->signup->assignPurchasedModules($companyId, $_POST['module']);

                // gets list of all current universal modules
                //$unvModules = $this->pricing->getModules(true);

                // assign universal modules
                //$this->signup->assignUnvModules($companyId, $unvModules);

                // inserts the intial user for the company
                $userid = $this->user->createUser($_POST, true);

                // assign user to company
                $this->user->insertUserCompany($userid, $companyId, true);


                // sets the user a company admin and editor
                $this->user->saveCompanyAdmin($userid, $companyId, 1);
                $this->user->saveCompanyEditor($userid, $companyId, 1);


                // create and assign a position
                $data = array(
                    'name' => 'Administrator',
                    'shortName' => 'Admin',
                    'description' => 'Site Default Administrator',
                    'default' => 1
                );

                $adminPosId = $this->position->createPosition($data, $companyId);
                $this->user->insertCompanyPosition ($userid, $companyId, $adminPosId);

                $data = array(
                    'name' => 'User',
                    'shortName' => 'User',
                    'description' => 'End User',
                    'default' => 1
                );
                
                $this->position->createPosition($data, $companyId);
                
                // create and assign department
                $data = array
                    (
                        'name' => 'Corporate',
                        'active' => 1,
                        'default' => 1
                    );

                // corproate department has been created
                $corpDepID = $this->department->createDepartment($data, $userid, $companyId);

                $this->user->insertCompanyDepartment($userid, $companyId, $corpDepID);

                $total = 0;

                // will now create payment
                // calculates price
                /*if (!empty($_POST['module']))
                {
                    foreach ($_POST['module'] as $m)
                    {
                        $info = $this->modules->moduleInfo($m);

                        $total += $info->price;
                    }
                }*/

                $users = intval($_POST['users']);

                // this is the final amount to be charged monthly
                //$price = $users * $total + 100;

                $_POST['customerID'] = $_POST['company'] = $companyId;

                $data = array(
                    'datestamp'         => DATESTAMP,
                    'userid'            => $userid,
                    'type'              => 2,
                    'mailbox'           => 'imap.greennvy.com',
                    'port'              => 143,
                    'username'          => $_POST['email'] . '@' . $_POST['emaildomain'],
                    'passwd'            => $this->encrypt->decode($_POST['password']),
                    'alias'             => 'BMS Email',
                    'outgoingServer'    => 'smtp.greennvy.com',
                    'yourname'          => $_POST['firstName'] . ' ' . $_POST['lastName'],
                    'emailAddress'      => $_POST['email'] . '@' . $_POST['emaildomain'],
                    'ssl'               => 0,
                    'auth'              => 1
                );

                $this->msgs->insertAdditionalAccount($data);
                
                // Step 1 Create Customer on BrainTree
                $data = array(
                    'customerID'    => $_POST['customerID'],
                    'firstName'     => $_POST['firstName'],
                    'lastName'      => $_POST['lastName'],
                    'companyName'   => $_POST['companyName'],
                    'email'         => $_POST['email'] . '@' . $_POST['emaildomain'],
                    'phone'         => $_POST['phone']
                );

                //$_POST['BTcustomerID'] = $BTcustomerID = $this->payment->createCustomer($data);
                
                // something went wrong creating the customer
                //if ($BTcustomerID == false) $this->functions->jsonReturn('ALERT', 'Unable to create customer!');

                /*$data = array(
                    'company'       => $companyId,
                    'BTcustomerID'  => $BTcustomerID,
                    'ccName'        => $_POST['cardname'],
                    'ccCvv'         => $_POST['cvv'],
                    'ccNumber'      => $_POST['cardnum'],
                    'ccExpM'        => $_POST['ccexpm'],
                    'ccExpY'        => $_POST['ccexpy']
                );*/
                
                // adds a credit card to bill and returns its token
                //$BTcreditCardToken = $this->payment->createCreditCard($data);
                
                // somethign went wrong saving the card to Braintree
                //if ($BTcreditCardToken == false) $this->functions->jsonReturn('ALERT', 'Unable to validation card information!!');

                //$sub = $this->payment->createSubscription($_POST['company'], $BTcreditCardToken, 'company_plan_monthly', $_POST['price'], true, false);

                /*if ($sub == 'ACTIVE') $this->functions->jsonReturn('SUCCESS', "Company and Subscription succesfully created! Please login with the e-mail and password you entered during the setup process.", $companyId);
                if ($sub == 'PROCESSOR_DECLINE') $this->functions->jsonReturn('ALERT', "Company was successfully create! However the credit card was declined by the processor.", $companyId);
                if ($sub == 'PENDING') $this->functions->jsonReturn('ALERT', "Company was successfully created and subscription is currently pending. You will be contacted within 1 business day.", $companyId);
                */
                $this->postfix->createDomain($_POST['emaildomain']);
                $this->postfix->createMailbox($_POST['email'] . '@' . $_POST['emaildomain'], $_POST['password']);
                
                $this->functions->setLoginSession($userid, $_POST['email'] . '@' . $_POST['emaildomain'], $companyId, $adminPosId, $corpDepID, false, true, true);
                $this->functions->jsonReturn('ALERT', "Company was successfully created. There might have been an error processing the payment however.", $companyId);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}

