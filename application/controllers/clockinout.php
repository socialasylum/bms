<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clockinout extends CI_Controller
{

    public $bcControllerUrl = '/clockinout';
    public $bcControllerText = "<i class='icon-time'></i> Clock in &amp; Out";

    public $bcViewText;

    function Clockinout()
    {
        parent::__construct();

        $this->load->model('clockinout_model', 'clockinout', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

        $header['headscript'] = $this->functions->jsScript('clockinout.js');

        $header['onload'] = "clockinout.indexInit();";
        $header['datatables'] = true;

        try
        {
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('clockinout/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addpunch ()
    {
        if ($_POST)
        {
            try
            {

                if ((bool) $_POST['addPunch'] == true)
                {
                    $id = $this->clockinout->addTimePunch();

                    $date = $this->users->convertTimezone($this->session->userdata('userid'), DATESTAMP, "m/d/Y g:i A");

                    $this->functions->jsonReturn('SUCCESS', $date, $id);
                }
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function history ()
    {
        try
        {
            $body['history'] = $this->clockinout->getPunches();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('clockinout/history', $body);
    }
}
