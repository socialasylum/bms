<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photos extends CI_Controller
{
    function Photos ()
    {
        parent::__construct();
        $this->load->model('photos_model', 'photos', true);
        
        $this->load->driver('cache');
    }
    
    /*
    public function modal ()
    {
        $this->load->view('users/index', $body);
    }
    */
    
    public function modalinfo ($photoID)
    {
		$body['photoID'] = $photoID;
		
		try
		{
			$body['info'] = $info = $this->photos->getInfo($photoID);
			
			// gets name of user who uploaded the photo
			$body['usersname'] = $this->users->getName($info->uploadedBy);

			$tzUserid = ($this->session->userdata('logged_in') == true) ? $this->session->userdata('userid') : $info->uploadedBy;
			
			$body['date'] = $this->users->convertTimezone($tzUserid, $info->datestamp, "F j Y g:iA");
			
			$body['comments'] = $this->photos->getPhotoComments($photoID);
		}
		catch (Exception $e)
		{
			$this->functions->sendStackTrace($e);
		}
		
		$this->load->view('photos/modalinfo', $body);
    }

	/**
	* provides additional info for viewing photos such as album name
	*/
	public function extradata ($photoID)
	{
	   try
	   {	
		  
			// get album naem
			$info = $this->photos->getInfo($photoID);
			$albumInfo = $this->photos->getAlbumInfo($info->album);
				   
	   		$data = array
	   		(
	   			'albumName' => $albumInfo->name
	   		);
	   
		   $this->functions->jsonReturn('SUCCESS', null, 0, null, $data);
	   }
	   catch (Exception $e)
	   {
	        $this->functions->sendStackTrace($e);
	        $this->functions->jsonReturn('ERROR', $e->getMessage());
	   }
	}

}