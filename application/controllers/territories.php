<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Territories extends CI_Controller
{

    public $bcControllerUrl = '/territories';
    public $bcControllerText = "<i class='fa fa-globe'></i> Territories";

    public $bcViewText;

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Territories()
    {
        parent::__construct();

        $this->load->model('territories_model', 'territories', true);
        $this->load->driver('cache');
        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

        $header['headscript'] = $this->functions->jsScript('territories.js');

        $header['onload'] = "territories.indexInit();";

        $header['datatables'] = true;
        $header['googleMaps'] = true;

        try
        {
            $body['territories'] = $this->territory->getTerritories($this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('territories/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function create ()
    {
        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/territories.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "territories.createInit();";

        $this->bcViewText = "Create Territory";

        try
        {
            $body['positions'] = $this->positions->getPositions(true, $this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('territories/create', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {
        $this->bcViewText = "<i class='fa fa-edit'></i> Edit Terriotry";
        $body['id'] = $id;

        $header['headscript'] = $this->functions->jsScript('territories.js');

        $header['googleMaps'] = true;
        $header['colorpicker'] = true;

        $header['onload'] = "territories.editInit();";

        try
        {
            $body['info'] = $this->territory->getTerritories($this->session->userdata('company'), $id);
            $body['positions'] = $this->positions->getPositions(true, $this->session->userdata('company'));


            if (!empty($id))
            {
                $body['markers'] = $this->territory->getMarkers($id);
                $body['color'] = $this->territories->getMarkersColor($id);
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('territories/edit', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                // if they are creating a new territory, or changing the name of an existing territory
                // will check the name
                if (empty($_POST['id']) OR ($_POST['name'] !== $_POST['orgName']))
                {
                    $check = $this->territories->checkTerritoryNameInUse($_POST['name'], $this->session->userdata('company'));

                    if ($check === true) $this->functions->jsonReturn('ALERT', "{$_POST['name']} is already a territory!");
                }


                if (empty($_POST['id']))
                {
                    $id = $this->territories->createTerritory($_POST);

                    $msg = "Territory has been created!";
                }
                else
                {
                    $id = $this->territories->updateTerritory($_POST);

                    // clear previous saved markers
                    $this->territories->clearMarkers($_POST['id']);

                    $msg = "Territory has been updated!";
                }
                
    
                $this->functions->jsonReturn('SUCCESS', $msg, $id);

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savemarker ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->territories->saveMarker($_POST['territory'], $_POST['lat'], $_POST['lng'], $_POST['radius'], $_POST['color'], $_POST['opacity']);
                
                $this->functions->jsonReturn('SUCCESS', 'marker saved', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
        
        $this->functions->jsonReturn('ERROR', 'GET is not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createterritory ()
    {
        if ($_POST)
        {
            try
            {
                $check = $this->territories->checkTerritoryNameInUse($_POST['name'], $this->session->userdata('company'));

                if ($check === true) $this->functions->jsonReturn('ALERT', "{$_POST['name']} is already a territory!");

                $id = $this->territories->createTerritory($_POST);

                $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET is not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function mapwidget ()
    {
        try
        {
            $body['territories'] = $this->territory->getTerritories($this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('territories/mapwidget', $body);
    }

}
