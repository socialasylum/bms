<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule extends CI_Controller
{
    public $bcControllerUrl = '/schedule';
    public $bcControllerText = "<i class='icon-calendar'></i> Schedule";

    public $bcViewText;



    function Schedule()
    {
        parent::__construct();

        $this->load->model('schedule_model', 'schedule', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    public function index()
    {

        $header['headscript'] = $this->functions->jsScript('schedule.js');
        $header['onload'] = "schedule.indexInit();";

        try
        {
            // $body['locations'] = $this->locations->getLocations($this->session->userdata('company'));
            $body['locations'] = $this->locations->getAssignedLocations($this->session->userdata('userid'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('schedule/index', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * TODO: short description.
     *
     * @param mixed $location 
     *
     * @return TODO
     */
    public function overview ($location, $month = null, $year = null)
    {

        $body['location'] = $location;

        $body['month'] = (empty($month)) ? date("n") : $month;
        $body['year'] = (empty($year)) ? date("Y") : $year;

        $this->load->view('schedule/overview', $body);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $location 
     *
     * @return TODO
     */
    public function edit ($location)
    {

        $header['headscript'] = $this->functions->jsScript('schedule.js');
        $header['onload'] = "schedule.editInit();";

        $header['timepicker'] = true;

        $this->bcViewText = "<i class='icon-edit'></i> Edit Schedule";

        $body['date'] = $date = urldecode($_GET['date']);

        $body['location'] = $location;

        try
        {
            $body['users'] = $this->locations->getLocationUsers($location);

            $body['schedule'] = $this->schedule->getDaySchedule ($location, $date);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('schedule/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveentry ()
    {
        try
        {
            $id = $this->schedule->insertSchedule($_POST);

            $this->functions->jsonReturn('SUCCESS', 'Schedule has been saved!', $id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteentry ()
    {
        if ($_POST)
        {
            try
            {
                $this->schedule->deleteScheduleEntry($_POST['id']);
                $this->functions->jsonReturn('SUCCESS', 'Schedule entry has been deleted!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
