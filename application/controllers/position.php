<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position extends CI_Controller
{
    public $bcControllerUrl = '/position';
    public $bcControllerText = "<i class='fa fa-group'></i> Positions";

    public $bcViewText;

    function Position()
    {
        parent::__construct();

        $this->load->model('position_model', 'position', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
    	$header['headscript'] = $this->functions->jsScript('positions.js');
        $header['datatables'] = true;
        $header['onload'] = "positions.indexInit();";

        $body['nav'] = 'index';

        try
        {
            $body['positions'] = $this->position->getPositions();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('positions/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * Deprecated, used edit function
     */
    /*
    public function create ()
    {
        $this->bcViewText = "Create Position";

        $body['nav'] = 'create';

        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/positions.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['ckeditor'] = true;

        $header['onload'] = "positions.createInit();";

        try
        {
            $body['companies'] = $this->companies->getCompanies();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('positions/create', $body);
        $this->load->view('template/footer_intranet');
    }
    */

    /**
     * Deprecated
     */
    /*
    public function createNewPosition ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->position->createPosition($_POST);

                $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }
    */
    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {

        $this->bcViewText = (empty($id)) ? "<i class='fa fa-edit'></i> Create Position" : "<i class='fa fa-edit'></i> Edit Position";

        // if (empty($id)) header("Location: /positions");

        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/positions.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
        $header['onload'] = "positions.editInit();";

        $header['ckeditor'] = true;

        $body['id'] = $id;

        try
        {
            $body['companies'] = $this->companies->getCompanies();

            $body['departments'] = $this->departments->getDepartments();

            $body['info'] = $this->position->getPositions($id);
            $body['positions'] = $this->position->getPositions();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('positions/edit', $body);
        $this->load->view('template/footer_intranet');

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function updateposition ()
    {
        if ($_POST)
        {
            try
            {

                // clear all previous default positions if default has been checked
                if (!empty($_POST['default']))
                {
                    $this->position->clearDefaultPosition($this->session->userdata('company'));
                }

                if (empty($_POST['id']))
                {
                    $_POST['id'] = $this->position->createPosition($_POST);

                    $msg = "Position has been created!";
                }
                else
                {
                    $this->position->updateInfo($_POST);

                    $msg = "Position has been updated!";
                }


                $this->position->clearChildPositions($_POST['id']);

                $this->position->saveChildPositions($_POST);

                $this->functions->jsonReturn('SUCCESS', $msg);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function structure ()
    {

        $body['nav'] = 'structure';

        try
        {
        
        }
        catch(Exception $e)
        {
        
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('positions/structure', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteposition ()
    {
        if ($_POST)
        {
            try
            {
                $this->position->deletePosition($_POST);
                $this->functions->jsonReturn('SUCCESS', "Position has been deleted!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    /**
     * Ajax functiont to assign a position for a user
     *
     * @return JSON response
     */
    public function assign ()
    {
        if ($_POST)
        {
            $this->load->model('user_model', 'user', true);

            try
            {
                $this->user->insertCompanyPosition($this->session->userdata('userid'), $this->session->userdata('company'), $_POST['position']);

                $this->functions->jsonReturn('SUCCESS', 'Position has been assigned');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }


}
