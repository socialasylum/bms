<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*	// module manages modify database
*/
class Dbase extends CI_Controller
{
    public $bcControllerUrl = '/dbase';
    public $bcControllerText = "<i class='fa fa-database'></i> Database";


	function Dbase ()
	{
        parent::__construct();
		
		$this->load->database();

        $this->load->model('dbase_model', 'dbase');

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();
	}
	
	
	public function index ()
	{
		$header['container'] = 'container-fluid';
		
		try
		{
			
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
        $this->load->view('template/header_intranet', $header);
        $this->load->view('dbase/index', $body);
        $this->load->view('template/footer_intranet', $footer);
	}
}