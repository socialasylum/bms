<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos extends CI_Controller
{
    public $bcControllerUrl = '/pos';
    public $bcControllerText = 'Sales Terminal';

    public $bcViewText;

    function Pos()
    {
        parent::__construct();

        $this->load->model('pos_model', 'pos', true);
        $this->load->model('orders_model', 'orders', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = $this->functions->jsScript('pos.js');

        $header['onload'] = "pos.indexInit();";
        $header['slimscroll'] = true;

        try
        {
            $homeLocation = $this->locations->getHome($this->session->userdata('userid'));
            
            $body['salesTax'] = $this->locations->getTableValue('salesTax', $homeLocation);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('pos/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function numbers ()
    {
        $this->load->view('pos/numbers', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function actions ()
    {
        $this->load->view('pos/actions', $body);
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function pwcheck ()
    {

        $this->load->model('intranet_model', 'intranet', true);

        try
        {
            $check = $this->intranet->checkLogin($_POST['email'], $_POST['password']);

            if ($check === false)
            {
                $this->functions->jsonReturn('ALERT', "Invalid Password");
            }
            else
            {
                $this->functions->jsonReturn('SUCCESS', "Valid Password");
            }

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function items ($folder = 0)
    {
        $this->load->model('item_model', 'item', true);

        try
        {
            // $body['items'] = $this->pos->getSalesItems();
            $body['items'] = $this->item->getFolderContent($folder);
            $body['parentFolder'] = $this->folders->getTableValue('parentFolder', $folder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('pos/items', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function additem ()
    {
        $transaction = $this->session->userdata('transaction');

        if (!$transaction) $transaction = array();

        if ($_POST)
        {
            try
            {
                $transaction[] = $_POST['item'];
                $this->session->set_userdata('transaction', $transaction);
                $this->functions->jsonReturn('SUCCESS', "Item has been added");
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }

        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $itemID 
     *
     * @return TODO
     */
    public function removeitem ($itemID, $limit = 0)
    {
        try
        {
            $transaction = $this->session->userdata('transaction');

            if (!empty($transaction))
            {
                $cnt = 0; // keeps count of how many were removed

                foreach ($transaction as $k => $v)
                {
                    if ($v == $itemID)
                    {
                        // clears item from array
                        unset($transaction[$k]);
                        $cnt++;

                        if (!empty($limit))
                        {
                            if ($cnt >= $limit) break;
                        }
                    }
                }

                $this->session->set_userdata('transaction', $transaction);
            }

            $this->functions->jsonReturn('SUCCESS', "Item has been removed");

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function trans ()
    {
        $body['transaction'] = $this->session->userdata('transaction');

        $this->load->view('pos/trans', $body);

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function finishtrans ()
    {

        $this->load->view('pos/finishtrans', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function posttrans ()
    {
        $this->load->view('pos/posttrans', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function complete ()
    {
        try
        {
            $id = $this->pos->insertTransaction(1);

            $transactionItems = $this->session->userdata('transaction');

            $this->saveTransactionItems($id, $transactionItems);

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function processorder ()
    {
        if ($_POST)
        {
            try
            {
                // gets current logged in employees home location
                $location = $this->locations->getHome($this->session->userdata('userid'));

                $salesTax = $this->locations->getTableValue('salesTax', $homeLocation);

                $data = array
                    (
                        'userid' => $this->session->userdata('userid'),
                        'crmContactID' => 0,
                        'status' => 1,
                        'location' => $location
                    );

                // will now save order
                $orderID = $this->orders->createOrder($data);

                // will now save order items
                $transaction = $this->session->userdata('transaction');

                $items = $this->pos->compileTransQty($transaction);


                $subtotal = 0;

                if (!empty($items))
                {
                    foreach ($transaction['item'] as $itemID)
                    {
                        $itemSubtotal = $retailPrice = $qty = 0;

                        try
                        {
                            $cost = $this->items->getTableValue('cost', $itemID);
                            $retailPrice = $this->items->getTableValue('retailPrice', $itemID);
                            $qty = $transaction['qty'][$itemID];
                        }
                        catch (Exception $e)
                        {
                            $this->functions->sendStackTrace($e);
                        }

                        $this->orders->insertOrderItem($orderID, $itemID, $qty, $cost, $retailPrice);

                        $subtotal += $itemSubtotal;
                    }
                }

                // final dollar amount to be charged
                $amount = ($subtotal * $salesTax) + $salesTax;

                // Gets expiration month and year - sent as example (MMYYYY): 072014
                $expM = substr($_POST['ccExp'], 0, 2);
                $expY = substr($_POST['ccExp'], 2);

                // will not process charge through braintree
                $data = array
                    (
                        'amount' => $amount,
                        'number' => $p['ccNumber'],
                        'cvv' => $p['cvv'],
                        'expMonth' => $expM,
                        'expYear' => $expY
                    );

                $response = $this->payment->processTransaction($data);

                // update order with CC response

                // transaction was successful
                if (!empty($response))
                {
                    // sets order to 'completed' status
                    $this->orders->updateOrderStatus($orderID, 5);
                }

                // clear cart
                $this->session->unset_userdata('transaction');


                $this->functions->jsonReturn('SUCCESS', "Transaction Authorization Approved");
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function receipt ()
    {
        $this->load->view('pos/receipt', $body);
    }
}
