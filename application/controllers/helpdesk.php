<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helpdesk extends CI_Controller
{
    public $bcControllerUrl = '/helpdesk';
    public $bcControllerText = 'Help Desk';

    public $bcViewText;
 
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Helpdesk()
    {
        parent::__construct();

        $this->load->model('helpdesk_model', 'help', true);
        $this->load->driver('cache');
        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    public function index()
    {
        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/helpdesk.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
        $header['onload'] = "helpdesk.indexInit();";
        $colSize == 3;
        $body['nav'] = 'index';

        try
        {
            $body['departments'] = $this->departments->getDepartments();
            $body['myTicketCount'] = $this->help->getMyTicketCount();
            $body['depTicketCount'] = $this->help->getDepTicketCount();
            $body['myTechTicketCount'] = $this->help->getMyTechTicketCount();
            $body['priority'] = $this->functions->getCodes('10');
            $body['status'] = $this->functions->getCodes('11');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('helpdesk/home', $body);
        $this->load->view('template/footer_intranet');
    }

    public function submitTicket()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->help->submitTicket($_POST);

                $this->functions->jsonReturn('SUCCESS',"Your ticket has been submitted, your ticket number is <strong>$id</strong>");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }

    public function myTickets()
    {
        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/helpdesk.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
        $header['onload'] = "helpdesk.myTicketsInit();";
        $header['datatables'] = true;

        $this->bcViewText = "My Tickets";

        $body['nav'] = 'myTickets';

         try
         {
            $body['departments'] = $this->departments->getDepartments();
            $body['myTicketCount'] = $this->help->getMyTicketCount();
            $body['depTicketCount'] = $this->help->getDepTicketCount();
            $body['myTechTicketCount'] = $this->help->getMyTechTicketCount();
            $body['tickets'] = $this->help->viewMyTickets();
         }
         catch(Exception $e)
         {
            $this->functions->sendStackTrace($e);
         }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('helpdesk/myTickets', $body);
        $this->load->view('template/footer_intranet');
    }

    public function depTickets()
    {
        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/helpdesk.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
        $header['onload'] = "helpdesk.depTicketsInit();";
        $header['datatables'] = true;

        $this->bcViewText = "Department Tickets";

        $body['nav'] = 'depTickets';

        try
        {
            $body['departments'] = $this->departments->getDepartments();
            $body['tickets'] = $this->help->getDepTickets();
            $body['myTicketCount'] = $this->help->getMyTicketCount();
            $body['depTicketCount'] = $this->help->getDepTicketCount();
            $body['myTechTicketCount'] = $this->help->getMyTechTicketCount();
            $body['tech'] = $this->help->getTech($id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('helpdesk/depTickets', $body);
        $this->load->view('template/footer_intranet');
    }

    public function myTechTickets()
    {
        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/helpdesk.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
        $header['onload'] = "helpdesk.myTechTicketsInit();";
        $header['datatables'] = true;

        $this->bcViewText = "My Tech Tickets";

        try
        {
            $body['departments'] = $this->departments->getDepartments();
            $body['myTicketCount'] = $this->help->getMyTicketCount();
            $body['depTicketCount'] = $this->help->getDepTicketCount();
            $body['myTechTicketCount'] = $this->help->getMyTechTicketCount();

            $body['tickets'] = $this->help->getMyTechTickets();
            $body['ticketInfo'] = $this->help->getTicketInfo();
            $body['tech'] = $tech = $this->help->getTech();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('helpdesk/myTechTickets', $body);
        $this->load->view('template/footer_intranet');

    }

    public function editTicket($id)
    {
        $header['onload'] = "helpdesk.editTicketInit();";
        $header['headscript'] .= "<script type='text/javascript' src='/min/?f=public/js/helpdesk.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $this->bcViewText = "Edit Ticket";

        $colSize == 2;
        $body['id'] = $id;

        try
        {
            $body['tech'] = $tech = $this->help->getTech($id);
            $body['tickets'] = $tickets = $this->help->getTechTicketInfo($id, $tech);
            $body['departments'] = $this->departments->getDepartments();
            $body['myTicketCount'] = $this->help->getMyTicketCount();
            $body['depTicketCount'] = $this->help->getDepTicketCount();
            $body['myTechTicketCount'] = $this->help->getMyTechTicketCount();
            $body['departments'] = $this->departments->getDepartments();
            $body['depTechs'] = $depTechs = $this->help->getDepartmentTechs($tickets->department);
            $body['notes'] = $this->help->getNotes($id);
            foreach ($tech as $r)
                {
                    $techTicket = ($r->userid == (int) $this->session->userdata('userid')) ? True : False;
                    if($techTicket == True)
                    {
                        $body['techTicket'] = True;
                    }
                }
            $body['priorities'] = $this->functions->getCodes('10');
            $body['status'] = $this->functions->getCodes('11');

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('helpdesk/editTicket', $body);
        $this->load->view('template/footer_intranet');
    }

    public function insertNote()
    {
        if ($_POST)
        {
            try
            {
                $this->help->addNote($_POST);
                $this->functions->jsonReturn('SUCCESS', null);

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function getDepartmentTechs($departmentId, $colSize)
    {
        try
        {
            $body['departmentTechs'] = $this->help->getDepartmentTechs($departmentId);
            $body['colSize'] = $colSize;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('/helpdesk/departmentTechs', $body);
    }
}
