<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msg extends CI_Controller {

    public $bcControllerUrl = '/msg';
    public $bcControllerText = "<i class='fa fa-envelope'></i> Messaging";
    public $pageTitle = "Messaging";
    public $bcViewText;

    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Msg() {
        parent::__construct();
        $this->load->model('msg_model', 'msg');
        $this->load->library('msgs');
        $this->load->driver('cache');
        $this->functions->checkLoggedIn();
        $this->load->database();
        try {
            $this->modules->checkAccess($this->router->fetch_class(), true);
            $this->msg->checkDefaultFolders($this->session->userdata('userid'));
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    function __destruct() {
        //$this->db->close();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index() {
        $header['headscript'] = $this->functions->jsScript('msg.js folders.js');
        $header['onload'] = "msg.indexInit();";
        $header['title'] = $this->pageTitle;
        $header['css'] = ".navbar.subnav{ margin-bottom:5px; };";
        $header['container_style'] = "margin-top:0;padding:5px;background:none;";
        $header['container'] = 'container-fluid';
        $footer['hide'] = true;
        $header['perfectscroll'] = true;
        $header['fancytree'] = true;
        $header['contextmenu'] = true;

        try {
            $body['folders'] = $this->msg->getMsgFolders($this->session->userdata('userid'));
            $body['inbox'] = $this->msg->getTypeFolderID($this->session->userdata('userid'), 'inbox');
            $body['trash'] = $this->msg->getTypeFolderID($this->session->userdata('userid'), 'trash');
            $body['syncRate'] = 600000;
            $body['noPasswdAccounts'] = $this->msgs->getAccountsWithNoPasswd($this->session->userdata('userid'));
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('msg/index', $body);
        $this->load->view('template/footer_intranet', $footer);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function foldertree() {
        try {
            $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'));
            //var_dump($mail_box);
            $body['folders'] = $this->msg->getMsgFolders($mail_box->mbox, $mail_box->url);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
        $this->load->view('msg/foldertree', $body);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function foldercontent($page = 1) {
        $body['id'] = $_POST['id'];
        try {
            // downloads the latest messages
            $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'), $_POST['name']);
            $body['messages'] = $this->msg->getFolderMsgs($mail_box);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
        $this->load->view('msg/foldercontent', $body);
    }

    public function compose()
    {
        $header['headscript'] = $this->functions->jsScript('msg.js');
        $header['title'] = "Compose Message";
        $header['perfectscroll'] = true;
        $header['ckeditor'] = true;
        $header['onload'] = "msg.composeInit();";
        if (isset($_GET['window'])) {
            $header['css'] = "#site-wrapper{  padding-top:35px; }";
            $footer['hide'] = true;
            $header['container'] = 'container-fluid';
            $header['container_style'] = "margin-top:0;height:100%;";
        }

        //$body['widget'] = (bool) $widget;

        try {
            $body['sendTypes'] = $this->functions->getCodes(3, 0, 'code');

            $body['sig'] = $sig = $this->msgs->getSignature();
            // clears previous sendTypes
            if (!empty($body['sendTypes'])) {
                foreach ($body['sendTypes'] as $r) {
                    $this->session->unset_userdata("msg{$r->code}");
                }
            }

            if (!empty($_GET['to']))
                $this->session->set_userdata('msg1', array($_GET['to']));

            // gets any additional accounts to allow them to select which account
            // any external recpients will recieve the email from
            $body['accounts'] = $this->msgs->getAdditionalAccounts($this->session->userdata('userid'), array(1, 2));

            if (isset($_GET['reply']) OR isset($_GET['forward']) OR isset($_GET['replyall'])) {
                if (!isset($_GET['account']) OR empty($_GET['account'])) {
                    $body['sendTypes'] = $this->functions->getCodes(3, 0, 'code');

                    if (!empty($_GET['reply']))
                        $id = $_GET['reply'];
                    if (!empty($_GET['replyall']))
                        $id = $_GET['replyall'];
                    if (!empty($_GET['forward']))
                        $id = $_GET['forward'];

                    $body['id'] = $id;

                    // gets message information
                    $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'), $_GET['folder']);
                    $body['info'] = $info = $this->msg->getMessageInfo($mail_box, $id);

                    if (empty($info->extMsgID)) {
                        $body['from'] = $this->users->getName($info->from);
                        $body['body'] = $this->msg->getBody($id);
                    } else {
                        if ($info->parsed == 1)
                            $body['body'] = $this->msg->getBody($id);
                    }


                    if (!empty($_GET['reply'])) {
                        $body['reply'] = $_GET['reply'];
                        $body['subject'] = "Re: {$body['info']->subject}";

                        // assigns to session to "from" sender
                        // $toArray = $this->session->userdata("msg1");

                        $toArray[] = $info->from;

                        $this->session->set_userdata('msg1', $toArray);
                    }

                    if (!empty($_GET['forward'])) {
                        $body['forward'] = $_GET['forward'];
                        $body['subject'] = "Fwd: {$body['info']->subject}";
                    }

                    $body['from'] = $info->from;
                    
                    $body['orgmsg'] = $this->load->view('msg/orgmsg', $body, true);
                }
            } else {
                $body['orgmsg'] = "<p></p>{$sig->signature}";
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('msg/compose', $body);
        $this->load->view('template/footer_intranet', $footer);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function usersearch() {
        try {
            $query = urldecode($_GET['term']);

            $users = $this->msg->userSearch($query);

            echo json_encode($users);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function msgcomposeto() {
        try {

            // gets sendTypes
            $body['sendTypes'] = $this->functions->getCodes(3, 0, 'code');

            $sessionKey = "msg{$_GET['sendType']}";

            // first gets messages "to" array
            $toArray = $this->session->userdata($sessionKey);

            // if a delete to array key was specified
            if (isset($_GET['deleteKey'])) {
                foreach ($toArray as $k => $v) {
                    if ((int) $k === (int) $_GET['deleteKey']) {
                        // removes element from array
                        unset($toArray[$k]);

                        // re-sets the to array
                        $this->session->set_userdata($sessionKey, $toArray);

                        // breaks foreach loop
                        break;
                    }
                }
            }


            // if adding a recipient by their UserID
            if (!empty($_GET['to'])) {
                $toArray[] = $_GET['to'];
                $this->session->set_userdata($sessionKey, $toArray);
            }

            // goes through each codes and gets arrays for each type of sendType
            if (!empty($body['sendTypes'])) {
                foreach ($body['sendTypes'] as $r) {
                    $body["toArray"][$r->code] = $this->session->userdata("msg{$r->code}");
                }
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('msg/msgcomposeto', $body);
    }

    /**
     * Main ajax function for when a message is sent from /msg/compose
     * set $draft to true to save as a draft, false to send the message
     * @return TODO
     */
    public function send($draft = false, $draftuid = 0) {
        if ($_POST) {
            try {
                $msguid = 0;
                $usersSentTo = array();
                $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'), (($draft) ? 'Drafts' : 'Sent'));
                $sendTypes = $this->functions->getCodes(3, 0, 'code');
                $config = array();
                if (!empty($sendTypes)) {
                    foreach ($sendTypes as $r) {
                        $toArray[$r->code] = $this->session->userdata("msg{$r->code}");
                        $extArray[$r->code] = array();

                        // there are recpients for this message type
                        if (!empty($toArray[$r->code])) {
                            foreach ($toArray[$r->code] as $key => $to) {
                                $box = 0;

                                if (is_numeric($to)) {
                                    $usersSentTo[] = $to; // adds user to array of user accounts who receieved the messsage

                                    $toID = $to;
                                    $emailAddress = null;

                                    $box = $this->msg->getTypeFolderID($to, 'inbox');

                                    //elog("{$to} Inbox: {$box}");

                                    if (empty($inbox)) {
                                        $this->msg->checkDefaultFolders($to);
                                        $box = $this->msg->getTypeFolderID($to, 'inbox');
                                    }
                                } else {
                                    $toID = 0;
                                    $emailAddress = $to;
                                    $extArray[$r->code][] = $to;
                                    $box = 0;
                                }
                            }
                        }

                        // will now clear session array data
                        if (!$draft)
                        {
                            $this->session->unset_userdata("msg{$r->code}");
                        }
                        if (!empty($_POST['add']))
                        {
                            $extArray[1][] = $_POST['add'];
                        }
                    }

                    // there are external email addresses to send this message to
                    if (!empty($extArray) && !empty($_POST['account']))
                    {
                        $accountInfo = $this->msgs->getAccountInfo($_POST['account']);
                        if (!empty($accountInfo->outgoingServer)) {
                            $smtpInfo = $this->msgs->getOutgoingAccountInfo($accountInfo->outgoingServer);
                            if (empty($smtpInfo))
                                throw new Exception("No SMTP infomation");

                            $host = "localhost";

                            $config = array(
                                'protocol' => 'smtp',
                                'mailpath' => '/usr/sbin/mail',
                                'mailtype' => 'html',
                                'smtp_host' => $host,
                                'smtp_port' => 25,
                                'smtp_user' => $smtpInfo->emailAddress,
                                'smtp_pass' => $this->encrypt->decode($smtpInfo->passwd)
                            );
                            
                            $msguid = $this->functions->sendEmail($_POST['subject'], $_POST['body'], $extArray[1], $accountInfo->emailAddress, $accountInfo->yourname, $extArray[2], $extArray[3], $config, $mail_box, $draft, $draftuid);
                        }
                    }
                    else if (!empty($extArray)) {
                        //probably temporary hack
                        //if compose specifies no account
                        //we use the users email and name
                        //for sending to external emails
                        $userData = $this->users->getRow($this->session->userdata('userid'));
                        $msguid = $this->functions->sendEmail($_POST['subject'], $_POST['body'], $extArray[1], $userData->email, $userData->firstName . ' ' . $userData->lastName, $extArray[2], $extArray[3], $config, $mail_box, $draft, $draftuid);
                    }
                }
                PHPFunctions::jsonReturn('SUCCESS', $id, true, 0, array('users' => $usersSentTo, 'msguid' => $msguid));
            } catch (Exception $e) {
                PHPFunctions::sendStackTrace($e);
                PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }
        }

        PHPFunctions::jsonReturn('ERROR', 'GET is not supported!');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function preview($folder, $id) {
        $body['id'] = $id;
        $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'), $folder);
        if (isset($_GET['window'])) {
            $header['headscript'] = $this->functions->jsScript('msg.js');
            $header['onload'] = "msg.preview();";
            $header['perfectscroll'] = true;
            $header['container'] = 'container-fluid';
            $header['container_style'] = "margin-top:0;height:100%;";
            $footer['hide'] = true;
            $header['css'] = "#site-wrapper{  padding-top:35px; }";
        }
        try {
            $info = $this->msg->getMessageInfo($mail_box, $id);
            $body['to'] = $to = $info->to;
            if (empty($info->extMsgID))
            {
                $body['from'] = $info->from;
                $body['body'] = $this->msg->getBody($mail_box, $id);
            }
            else
            {
                if (!isset($_GET['window']))
                {
                    if (empty($info->parsed))
                    {
                        $e = $this->msgs->getEmail($mail_box, $id);
                        $html = (empty($e->html)) ? $e->plain : $e->html;
                        $body['body'] = $html;
                    }
                    else
                    {
                        //$body['body'] = $this->msg->getBody($mail_box, $id);
                        $body['body'] = $info->parsed;
                    }
                }
                $body['from'] = $info->from;
                $body['from'] = str_replace('<', '&lt;', $body['from']);
                $body['from'] = str_replace('>', '&gt;', $body['from']);
            }
            $body['time'] = $this->users->convertTimezone($this->session->userdata('userid'), $info->emailDate, "F j, Y g:i A");
            $body['sendTypes'] = $this->functions->getCodes(3, 0, 'code');
            $body['info'] = $info;
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
        if (isset($_GET['window']))
        {
            $this->load->view('template/header_intranet', $header);
        }
        $this->load->view('msg/preview', $body);
        if (isset($_GET['window']))
        {
            $this->load->view('template/footer_intranet', $footer);
        }
    }

    /**
     * gets the body of a message
     */
    public function body($id) {
        try {
            $this->msg->setMessageread($id, $this->session->userdata('userid'));

            $info = $this->msg->getMessageInfo($id);

            $to = $this->msgs->msgTo($this->session->userdata('userid'), $id);


            if (empty($info->parsed)) {
                $e = $this->msgs->getEmail($this->session->userdata('userid'), $to->account, $to->folder, $info->extMsgID);

                $html = (empty($e->html)) ? $e->plain : $e->html;
            } else {
                $html = $this->msg->getBody($id);
            }
            // updates messages table with downloaded Content
            //$this->msg->updateMsgBody($id, $html);

            echo $html;
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
    }

    // saves a body after it has been parsed with javascript to remove body tags etc
    public function save_parsed_body() {
        try {
            if ($_POST) {
                $this->msg->updateMsgBody($_POST['id'], $_POST['body']);
                PHPFunctions::jsonReturn('SUCCESS', 'Msg body has been saved');
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage(), $id);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createfolder() {
        try {
            if ($_POST) {
                $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'));
                $id = $this->msg->createFolder($_POST['folderName'], $mail_box);
                PHPFunctions::jsonReturn('SUCCESS', $id);
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage(), $id);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function widget() {
        $body['widget'] = true;

        try {
            $fcBody['messages'] = $this->msg->getMessages($this->session->userdata('userid'), 1);

            $body['msgDisplay'] = $this->load->view('msg/foldercontent', $fcBody, true);

            $body['folders'] = $this->msg->getMsgFolders($this->session->userdata('userid'));
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('msg/widget', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function movemsg() {
        if ($_POST) {
            try {
                $mbox = $this->msgs->accountConnect($this->session->userdata('userid'));
                $response = $this->msg->moveMsg($mbox, $_POST['msg'], $_POST['folder']);
                PHPFunctions::jsonReturn('SUCCESS', $response);
            } catch (Exception $e) {
                PHPFunctions::sendStackTrace($e);
                PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function emptytrash() {
        try {
            $trashFolder = $this->msg->getTypeFolderID($this->session->userdata('userid'), 'trash');

            $cnt = $this->msg->getFolderTotal($trashFolder, $this->session->userdata('userid'));

            if (empty($cnt))
                PHPFunctions::jsonReturn('WARNING', "No messages to delete, trash is already empty.");

            $this->msg->emptyTrash($this->session->userdata('userid'));
            PHPFunctions::jsonReturn('SUCCESS', "Trash folder has been emptied");
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function notifications()
    {
        try {
            $inbox = $this->msgs->accountConnect($this->session->userdata('userid'));
            $sent = $this->msgs->accountConnect($this->session->userdata('userid'), 'Sent');
            // checks for new messages in users inbox
            $body['cnt'] = $cnt = $this->msg->getFolderTotal($inbox, true);
            $body['outboxCnt'] = $outboxCnt = $this->msg->getFolderTotal($sent, true);
            $body['messages'] = $this->msg->getFolderMsgs($this->session->userdata('userid'), $inbox, 0, 5);
            $view = $this->load->view('msg/notifications', $body, true);
            PHPFunctions::jsonReturn('SUCCESS', $cnt, true, 0, array('html' => $view));
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }
    
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function settings() {
        $header['headscript'] = $this->functions->jsScript('msg.js');
        $header['onload'] = "msg.settingsInit()";

        $header['ckeditor'] = true;
        $header['datatables'] = true;

        $this->bcViewText = "<i class='fa fa-cog'></i> Message Settings";

        try {
            $body['sig'] = $this->msgs->getSignature();

            $body['syncRate'] = $this->users->getTableValue('syncRate', $this->session->userdata('userid'));

            // gets POP3 accounts
            $body['accounts'] = $this->msgs->getAdditionalAccounts($this->session->userdata('userid'), 1);

            // gets SMTP accounts
            $body['smtpAccounts'] = $this->msgs->getAdditionalAccounts($this->session->userdata('userid'), 3);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('msg/settings', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savesettings() {
        if ($_POST) {
            try {
                // loads user model
                $this->load->model('user_model', 'user', true);

                // update syncreate
                $syncUpdate = $this->user->updateSyncRate($_POST['syncRate'], $this->session->userdata('userid'));

                if (empty($_POST['sigId'])) {
                    $this->msg->insertSignature($_POST['sig']);
                } else {
                    $this->msg->updateSignature($_POST['sigId'], $_POST['sig']);
                }

                PHPFunctions::jsonReturn('SUCCESS', 'Settings have been updated!');
            } catch (Exception $e) {
                PHPFunctions::sendStackTrace($e);
                PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addaccount($id = 0) {
        $header['headscript'] = $this->functions->jsScript('msg.js');
        $header['onload'] = "msg.addaccountInit();";

        $header['title'] = "Add Account";

        $this->bcViewText = "<i class='fa fa-plus'></i> Add Account";


        $body['id'] = $id;

        try {
            $body['types'] = $this->functions->getCodes(20);

            if (!empty($id)) {
                $body['info'] = $this->msg->getAccountInfo($id);
            }

            // gets SMTP accounts
            $body['smtpAccounts'] = $this->msgs->getAdditionalAccounts($this->session->userdata('userid'), 3);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('msg/addaccount', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveaddaccount() {
        if ($_POST) {
            try {
                if (empty($_POST['id'])) {
                    $_POST['id'] = $this->msgs->insertAdditionalAccount($_POST);
                } else {
                    $this->msgs->updateAdditionalAccount($_POST);
                }

                PHPFunctions::jsonReturn('SUCCESS', "Account has been saved!", $_POST['id']);
            } catch (Exception $e) {
                PHPFunctions::sendStackTrace($e);
                PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function imaptest($id = 0) {
        echo "<pre>";

        try {
            $this->msg->getExternalFolders($this->session->userdata('userid'), true);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
    }

    public function smtptest($account) {
        try {
            //echo "<pre>";

            $subject = "SMTP test";
            $body = "This is an SMTP test!";

            $smtpInfo = $this->msg->getAccountInfo($account);

            if (empty($smtpInfo))
                throw new Exception("No SMTP infomation");

            $config = array
                (
                'protocol' => 'smtp',
                //'mailtype' => 'html',
                'smtp_host' => $smtpInfo->mailbox,
                //            'smtp_host' => 'smtp.googlemail.com',
                'smtp_user' => $smtpInfo->username,
                'smtp_pass' => $this->encrypt->decode($smtpInfo->passwd),
                'smtp_port' => 80,
                'smtp_timeout' => 10,
                    //'smtp_auth' => true
                    //'charset' => 'utf-8',
                    //'mailtype'  => 'text',
                    //'wordwrap' => false,
                    //'validation' => true,
                    //'newline' => '\r\n',
                    //'crlf' => '\r\n'
            );

            // adds port if specified
            // if (!empty($smtpInfo->port)) $config['smtp_port'] = 465;
            elog($config);
            echo nl2br(print_r($config, true));

            // will now send to any external addreses
            $this->functions->sendEmail($subject, $body, "Williamgallios@gmail.com", 'noreply@cgisolution.com', 'CGI');

            echo "Sent";
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            print_r($e);
        }
    }

    /**
     * Goes through and sends/receives the latest messages for a users email accounts
     *
     * @return TODO
     */
    public function sync($userid = 0) {
        set_time_limit(0);

        try {
            if ($this->msg->check_sync_lock($this->session->userdata("userid"))) {
                PHPFunctions::jsonReturn('SYNCHRONIZING', "Synchronizing in progress");
            } else {
                // start job
                $this->msg->execSync($this->session->userdata("userid"));

                PHPFunctions::jsonReturn('STARTED', "Synchronizing in progress");
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function check_sync() {
        try {
            $response = $this->msg->getLastSyncResponse($this->session->userdata('userid'));

            $a = array('gearmanStatus' => $response->status, 'gearmanResponse' => json_decode($response->response));

            PHPFunctions::jsonReturn('SUCCESS', '', true, 0, $a);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function rclick() {
        try {
            $body['folders'] = $this->msg->getMsgFolders($this->session->userdata('userid'));
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('msg/rclick', $body);
    }

    public function deletefolder() {
        try {
            if ($_POST) {
                $mail_box = $this->msgs->accountConnect($this->session->userdata('userid'));
                $folderResponse = $this->msg->deleteFolder($mail_box, $_POST['folder']);
                PHPFunctions::jsonReturn('SUCCESS', $folderResponse);
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function movemsgs() {
        try {
            if ($_POST) {
                if (!empty($_POST['msgs'])) {
                    foreach ($_POST['msgs'] as $k => $msg) {
                        $this->msg->moveMsg($msg, $_POST['folder']);
                    }
                }

                PHPFunctions::jsonReturn('SUCCESS', 'Messages have been moved into new folder');
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function removeaccount() {
        try {
            if ($_POST) {
                $this->msg->deleteMsgAccount($this->session->userdata('userid'), $_POST['id']);
                PHPFunctions::jsonReturn('SUCCESS', 'Account has been removed!');
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function test() {
        set_time_limit(120); // 2 min

        try {
            if ($_POST) {
                if (!isset($_POST['passwd']) && !empty($_POST['account']))
                    $_POST['passwd'] = $this->msgs->getAccountPassword($_POST['userid'], $_POST['account']);

                if ($_POST['type'] == 3)
                    $test = $this->msg->testSMTP($_POST['server'], $_POST['port'], $_POST['username'], $_POST['passwd']);
                else
                    $test = $this->msg->testAccount($_POST['server'], $_POST['port'], $_POST['type'], $_POST['username'], $_POST['passwd'], $_POST['ssl'], $_POST['auth']);


                if ($test !== false)
                    PHPFunctions::jsonReturn('SUCCESS', 'Test was successful!');
            }
        } catch (Exception $e) {
            //PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }


        PHPFunctions::jsonReturn('ERROR', 'GET is not supported');
    }

    public function save_new_account() {
        try {
            $_POST['userid'] = $_POST['id'];

            // first save Incoming account
            $data = $_POST;


            unset($data['yourname']);

            $data['alias'] = $_POST['emailAddress'] . ' SMTP';
            $data['mailbox'] = $_POST['outgoingHost'];
            $data['type'] = 3;
            $data['port'] = $_POST['outgoingPort'];
            $data['ssl'] = $_POST['outgoingSSL'];
            $data['auth'] = $_POST['outgoingAuth'];
            $data['outgoingServer'] = 0;

            $outgoingServer = $this->msgs->insertAdditionalAccount($data);

            $data = $_POST;

            $data['mailbox'] = $_POST['incomingHost'];
            $data['type'] = $_POST['type'];
            $data['port'] = $_POST['incomingPort'];
            $data['ssl'] = $_POST['incomingSSL'];
            $data['auth'] = $_POST['incomingAuth'];
            $data['alias'] = $_POST['emailAddress'];
            $data['outgoingServer'] = $outgoingServer;

            $incomingServer = $this->msgs->insertAdditionalAccount($data);

            // ensures user has folders setup
            $this->msg->checkDefaultFolders($_POST['userid']);

            $this->msg->execSync($_POST['userid']);

            PHPFunctions::jsonReturn('SUCCESS', 'New e-mail account was saved!', true, 0, array('incomingID' => $incomingServer, 'outgoingID' => $outgoingServer));
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function update_account_settings() {
        try {
            if ($_POST) {
                $this->msgs->updateAdditionalAccount($_POST);

                PHPFunctions::jsonReturn('SUCCESS', 'E-mail account has been updated!');
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function delete_account() {
        try {
            if ($_POST) {
                $this->msg->deleteMsgAccount($_POST['userid'], $_POST['id']);
                PHPFunctions::jsonReturn('SUCCESS', 'E-mail account has been deleted!');
            }
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
        }
    }
}
