<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leads extends CI_Controller
{
    public $bcControllerUrl = '/leads';
    public $bcControllerText = "<i class='fa fa-money'></i> Leads";

    public $bcViewText;
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Leads()
    {
        parent::__construct();

        $this->load->model('leads_model', 'leads', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    public function index ()
    {
        $header['datatables'] = true;
        $header['headscript'] = $this->functions->jsScript('leads.js');

        $header['onload'] = "leads.indexInit();";

        $header['googleMaps'] = true;

        try
        {
            // gets leads sent from this user
            $body['sent'] = $this->leads->getLeads(0, $this->session->userdata('userid'));

            // gets leads sent to this user
            $body['received'] = $this->leads->getLeads($this->session->userdata('userid'));

            // gets overall chapter leads
            $body['chapter'] = $this->leads->getLeads(0,0, false, true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('leads/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {
        $header['ckeditor'] = true;
        $header['headscript'] = $this->functions->jsScript('leads.js');

        $header['onload'] = "leads.editInit();";

        $body['id'] = $id;

        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Lead";

        $body['ampm'] = array('AM', 'PM');

        try
        {
            // current users home location
            $homeLocation = $this->locations->getHome();

            $body['contactTypes'] = $this->functions->getCodes(16);
            // $body['users'] = $this->locations->getLocationUsers($homeLocation);

            $body['users'] = $this->locations->getAllAssignedLocationusers($this->session->userdata('userid'));

            if (!empty($id))
            {
                $body['info'] = $this->leads->getInfo($id);
                $body['emails'] = $this->leads->getEmails($id);
                $body['phones'] = $this->leads->getPhones($id);
                $body['addresses'] = $this->leads->getAddresses($id);
            }


            $body['types'] = $this->functions->getCodes(8, 'display');
            $body['phoneTypes'] = $this->functions->getCodes(9, 'display');
            $body['states'] = $this->functions->getStates();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('leads/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                // inserts or updates inital lead data
                if (empty($_POST['id']))
                {
                    $_POST['id'] = $this->leads->insertLead($_POST);

                    // if lead was assigned to someone
                    if (!empty($_POST['to']))
                    {
                        // send notifciation
                        $msg = "You have received a new lead!";
                        $icon = 'fa fa-money';
                        $module = $this->modules->getModIDFromController('leads');
                        $url = '/leads/edit/' . $_POST['id'];

                        $this->functions->insertNotification($_POST['to'], $this->session->userdata('company'), $msg, $icon, $module, $url);

                        // needs to get senders information
                        $userName = $this->users->getname($this->session->userdata('userid'));

                        $userCompanyName = $this->users->getTableValue('companyName', $this->session->userdata('userid'));

                        $companyName = $this->companies->getCompanyName($this->session->userdata('company'));

                        // who to send email alert to
                        $emailTo = $this->users->getTableValue('email', $_POST['to']);

                        // will also send an email

                        $subject = "You have a new lead!";
                    
                        $message = "<p>{$userName} " . (empty($userCompanyName) ? null : "of {$userCompanyName} ") . "has sent you a hot lead! To find out the details, please <a href='http://" . $_SERVER['HTTP_HOST'] . "/leads/edit/{$_POST['id']}'>Click Here...</a></p>
                            <p>&nbsp;</p>
                            <p>Thank You,<br>Leads@PromoteLeads.com</p>";
                    
                        $this->functions->sendEmail($subject, $message, $emailTo, 'noreply@cgisolution.com', $companyName);
                    }

                    $msg = "Lead has been sent!";
                }
                else
                {
                    $this->leads->updateLead($_POST);

                    $this->leads->clearAddresses($_POST['id']);
                    $this->leads->clearPhones($_POST['id']);
                    $this->leads->clearEmails($_POST['id']);

                    $msg = "Lead has been updated!";
                }

                $this->leads->saveEmails($_POST['id'], $_POST['email']);
                $this->leads->savePhones($_POST['id'], $_POST['phone']);
                $this->leads->saveAddress($_POST['id'], $_POST['address']);


                $this->functions->jsonReturn('SUCCESS', $msg, $_POST['id']);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
