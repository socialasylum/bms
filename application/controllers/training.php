<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training extends CI_Controller
{
    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Training()
    {
        parent::__construct();

        $this->load->model('training_model', 'training', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

    }
}
