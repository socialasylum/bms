<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Folder extends CI_Controller
{
    // public $bcControllerUrl = '/item';
    // public $bcControllerText = "<i class='fa fa-shopping-cart'></i> Item Management";

    // public $bcViewText;


    function Folder ()
    {
        parent::__construct();

        $this->load->model('folder_model', 'folder', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function rightclick ()
    {
        $this->load->view('folder/rightclick', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function properties ($folder)
    {
        $body['folder'] = $folder;

        try
        {
            $body['info'] = $this->folder->getFolderData($folder);

            $body['folderMods'] = $this->modules->getModulesUsingFolders();

            $body['viewableMods'] = $this->folder->getViewableMods($folder);

            $body['status'] = array('Deactived', 'Active');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('folder/properties', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function create ()
    {
        $this->load->view('folder/create', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createfolder ()
    {
        if ($_POST)
        {
            try
            {
                $folder = $this->folder->insertFolder($_POST['folderName'], $_POST['folder']);

                // makes folder viewable in all modules - user can edit from there
                $this->folder->folderModViewInit($folder);

                PHPFunctions::jsonReturn('SUCCESS', 'Folder has been created!', true, $folder, array('name' => $_POST['folderName']));
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
				PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * Used for dynatree
     */
    public function tree ($selected = 0, $company = 0, $includeFiles = 0)
    {
        // $body['selected'] = $selected;
        // $body['company'] = $company;
        // $body['includeFiles'] = $includeFiles;

        if (isset($_GET['namespace']))
        {
            // $body['namespace'] = urldecode($_GET['namespace']);
            $namespace = urldecode($_GET['namespace']);
        }

        try
        {
            echo $this->folder->buildTree(0, $selected, $company, $namespace, (bool) $includeFiles);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
        
        // $this->load->view('folder/tree', $body);

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function movefolder ()
    {
        if ($_POST)
        {
            try
            {
                // if they have permissions, will now move folder
                $this->folder->moveFolder($_POST['id'], $_POST['folder']);

                $this->functions->jsonReturn('SUCCESS', 'folder has been moved!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addItemToFolder ()
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        if ($_POST)
        {
            try
            {
                // if they have permissions, will now move folder
                $this->folder->assignItemToFolder($_POST['id'], $_POST['folder'], $_POST['tbl'], $_POST['col']);

                $this->functions->jsonReturn('SUCCESS', 'Item has been moved into folder!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveproperties ()
    {
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        if ($_POST)
        {
            try
            {

                // updates folder itself
                $this->folder->updateFolder($_POST);

                if (empty($_POST['module'])) $this->functions->jsonReturn('ALERT', 'This folder is not viewable in any modules!');

                // will now save which modules this folder will be viewiable in
                $this->folder->saveModsViewable($_POST);

                $this->functions->jsonReturn('SUCCESS', 'Folder properties have been saved!');
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deletefolder ()
    {
        if ($_POST)
        {
            try
            {
                $this->folder->deleteFolder($_POST['folder']);
                $this->functions->jsonReturn('SUCCESS', 'Folder has been deleted!');
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
