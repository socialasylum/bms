<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Codes extends CI_Controller
{

    public $bcControllerUrl = '/codes';
    public $bcControllerText = '<i class=\'icon-qrcode\'></i> Codes Editor';

    public $bcViewText;


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Codes()
    {
        parent::__construct();

        $this->load->model('codes_model', 'codes', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($group = 0)
    {

        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/codes.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "codes.indexInit();";

        if (!empty($group)) $body['group'] = $group;

        try
        {
            $body['groups'] = $this->codes->getCodeGroups();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('codes/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $group 
     *
     * @return TODO
     */
    public function codegroupdisplay ($group = 0)
    {

        $body['group'] = $group;

        try
        {
            if (!empty($group))
            {
                $body['info'] = $this->codes->getCodeGroups($group);
                $body['codes'] = $this->codes->getGroupCodes($group);
                $body['companies'] = $this->companies->getCompanies();
            }

            $body['status'] = array('Deactived', 'Active');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('codes/codegroupdisplay', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function creategroup ()
    {
        if ($_POST)
        {
            try
            {
                $group = $this->codes->insertGroup($_POST['groupName'], $_POST['editable']);
                $this->functions->jsonReturn('SUCCESS', "Group has been created!", $group);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function save ()
    {
        if ($_POST)
        {
            try
            {
                // print_r($_POST);die;


                if (!empty($_POST['codes']))
                {
                    foreach ($_POST['codes'] as $code => $void)
                    {
                        $this->codes->updateCode($_POST['group'], $code, $_POST['display'][$code], $_POST['active'][$code], $_POST['company'][$code], 1);
                    }
                }

                // insert a new code if entered
                if (!empty($_POST['newDisplay']))
                {
                    $this->codes->insertGroupCode ($_POST['group'], $_POST['newDisplay'], 1, $_POST['newCompany'], 1);
                }


                $this->functions->jsonReturn('SUCCESS', "Code Group has been updated!");

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
