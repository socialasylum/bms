<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documentation extends CI_Controller
{
    function Documentation ()
    {
        parent::__construct();

        $this->load->model('documentation_model', 'documentation', true);

        $this->load->driver('cache');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['nav'] = 'documentation';

        $header['headscript'] = $this->functions->jsScript('documentation.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['onload'] = "documentation.indexInit();";

        $header['ckeditor'] = true;
        $header['slimscroll'] = true;
        $header['dynatree'] = true;


        try
        {
        
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('documentation/index', $body);
        $this->load->view('template/footer');
    }
}
