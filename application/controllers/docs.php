<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docs extends CI_Controller
{
    public $bcControllerUrl = '/docs';
    public $bcControllerText = "<i class='fa fa-file-text'></i> Documents";


    function Docs()
    {
        parent::__construct();

        $this->load->model('docs_model', 'docs', true);

        $this->load->driver('cache');

        $this->load->library('documents');

        $this->functions->checkLoggedIn();


        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    public function index ($folder = 0, $rootFolder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('docs.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['datatables'] = true;
        $header['onload'] = "docs.indexInit();";
        $header['rightlick'] = true;

        $body['admin'] = true;

        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;

        try
        {
            $body['content'] = $this->docs->getFolderContent($folder);
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
            $body['parentFolder'] = $this->folders->getTableValue('parentFolder', $folder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('docs/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id)
    {
        $header['headscript'] = $this->functions->jsScript('docs.js');
        $header['ckeditor'] = true;
        $header['onload'] = "docs.editInit();";

        $body['id'] = $id;

        if (isset($_GET['title'])) $body['title'] = urldecode($_GET['title']);

        $body['folder'] = $_GET['folder'];

        $body['status'] = array('Deactived', 'Active');

        try
        {
            if (!empty($id))
            {
                $body['info'] = $this->docs->getDocInfo($id);
            }
            else
            {
                $body['info'] = (object) array
                    (
                        'FstartDate' => date("m/d/Y"),
                        'FendDate' => date("m/d/Y", mktime(0, 0, 0, date("m") + 1, date("d"), date("y")))
                    );
            }

            $body['positions'] = $this->positions->getPositions(true);
            // $body['locations'] = $this->locations->getLocations($this->session->userdata('userid'));

            $body['docack'] = $this->functions->getCodes(1, $this->session->userdata('company'));

            // $body['countries'] = $this->functions->getCodes(5, $this->session->userdata('company'));
            $body['sendTypes'] = $this->functions->getCodes(6, $this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('docs/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function createfolder()
    {
        $return = array();

        if ($_POST)
        {
            try
            {
                $folderId = $this->docs->createFolder($_POST, $this->session->userdata('userid'), $this->session->userdata('company'));

                $this->functions->jsonReturn('SUCCESS', $folderId);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET is not supported!');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                $_POST['company'] = $this->session->userdata('company');
                $_POST['userid'] = $this->session->userdata('userid');

                $save = $this->docs->saveDocument($_POST);

                $this->functions->jsonReturn('SUCCESS', null);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET is not supported!');
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function view ($id, $folder = 0, $rootFolder = 0, $version = 0, $user = 0)
    {
        if (empty($id))
        {
            header("Location: /docs?site-error=" . urlencode("Document ID is empty!"));
            exit;
        }

        if (empty($user)) $user = $this->session->userdata('userid');

        $header['headscript'] = $this->functions->jsScript('docs.js');
        $header['onload'] = "docs.viewInit();";

        $body['id'] = $id;
        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;
        $body['user'] = $user;

        try
        {
            $body['info'] = $info =  $this->docs->getDocInfo($id, $version);
            $body['date'] = $this->users->convertTimezone($user, DATESTAMP, "m/d/Y g:i A");


            // if version was not specifed uses the latest version
            $body['version'] = $version = (empty($version)) ? $info->version : $version;
            
            $body['signatureData'] = $this->docs->getDocAckInfo($user, $id, $version);

            $body['userName'] = $this->users->getName($user);

            $this->docs->insertDocViewHistory($this->session->userdata('userid'), $id, $version);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('docs/view', $body);
        $this->load->view('template/footer_intranet');
    }


    /**
     * Gets html for Properties modal in /docs
     *
     * @param int $id - Document ID
     */
    public function properties ($id)
    {
        $body['id'] = $id;

        try
        {
            $body['info'] = $this->docs->getDocInfo($id);

            $body['previousVersions'] = $this->docs->getPreviousVersions($id);

            $body['ackDisplay'] = $this->functions->codeDisplay(1, $body['info']->ack);

            $body['status'] = array('Deactived', 'Active');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('docs/properties', $body);
    }


    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function preview ($id, $version = 0)
    {
        if (empty($id))
        {
            header("Location: /docs?site-error=" . urlencode("Document ID is empty!"));
            exit;
        }

        // $body['version'] = $version;

        try
        {
            $body['info'] = $this->docs->getDocInfo($id, $version);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('docs/preview', $body);
    }

    /**
     * AJAX page used to set document to deleted
     *
     * @return Json Array
     */
    public function deleteFile ()
    {
        if ($_POST)
        {
            try
            {
                $this->docs->setDocumentDeleted($_POST['id'], $this->session->userdata('EMPID'));

                $this->functions->jsonReturn('SUCCESS', null);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $code 
     *
     * @return TODO
     */
    public function getSendToDisplay ($code, $id)
    {
        try
        {
            $body['code'] = $code;
            $body['id'] = $id;

            if ($code == 1) $body['locations'] = $this->locations->getLocations($this->session->userdata('userid'));
            elseif ($code == 2) $body['locations'] = $this->functions->getCodes(5, $this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('docs/getSendToDisplay', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function checkrequire ()
    {
        try
        {
            $docs = $this->documents->getRequiredDocs($this->session->userdata('userid'), true);

            if ($docs > 0) $msg = 'TRUE';
            else $msg = 'FALSE';

            $this->functions->jsonReturn('SUCCESS', $msg);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveack ()
    {
        if ($_POST)
        {
            try
            {
                $msg = 'Nothing Happened =/';

                if ($_POST['ack'] == 1) // requires signature
                {
                    // check if users signature is valid
                    $checkPassword = $this->users->checkPassword($this->session->userdata('userid'), $_POST['password']);

                    if ($checkPassword === false) $this->functions->jsonReturn('ALERT', 'Invalid password!');

                    $this->docs->saveDocAck($_POST);

                    $msg = "Document has been signed";
                }
                elseif ($_POST['ack'] == 2) // only reqiures acknowledgment
                {
                    $this->docs->saveDocAck($_POST);

                    $msg = "Document has been signed";
                }


                $this->functions->jsonReturn('SUCCESS', $msg);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function setresign ()
    {
        try
        {
            $this->docs->clearUsersSignatures($_POST['user'], $_POST['doc']);

            $title = $this->documents->getTableValue('title', $_POST['doc']);

            $name = $this->users->getName($_POST['user']);

            $this->functions->jsonReturn('SUCCESS', "Signates for {$title} been cleared for {$name}!");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }
}
