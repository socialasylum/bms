<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends CI_Controller
{

    public $bcControllerUrl = '/location';
    public $bcControllerText = "<i class='fa fa-building'></i> Locations";

    public $bcViewText;



    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Location()
    {
        parent::__construct();

        $this->load->model('location_model', 'location', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = $this->functions->jsScript('locations.js');

        $header['onload'] = "locations.indexInit();";
    
        $header['googleMaps'] = true;
        $header['datatables'] = true;

        try
        {
            $body['locations'] = $this->locations->getLocations($this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('location/index', $body);
        $this->load->view('template/footer_intranet');

    }

    /**
     * deprecated
     */
    /*
    public function create ()
    {
        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/locations.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "locations.createInit();";
        try
        {
            $body['territories'] = $this->territory->getTerritories($this->session->userdata('company'));
        
            $body['states'] = $this->functions->getStates();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
        $this->load->view('template/header_intranet', $header);
        $this->load->view('location/create', $body);
        $this->load->view('template/footer_intranet');
    }
     */
    /**
     * deprecated
     */
    /*
    public function createlocation ()
    {
        if ($_POST)
        {
            try
            {
                $check = $this->location->checkStoreNameInUse($_POST['name'], $this->session->userdata('company'));

                if ($check === true) $this->functions->jsonReturn('ALERT', "{$_POST['name']} is already a location!");

                $id = $this->location->createLocation($_POST);

                $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET is not supported!');
    }
     */
    /**
     * page used for creating and editing a location
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function edit ($id)
    {
        $this->bcViewText = (empty($id)) ? "Create Location" : "<i class='icon-edit'></i> Edit Location";

        $header['headscript'] = $this->functions->jsScript('locations.js');
        $header['onload'] = "locations.editInit();";
        $header['datatables'] = true;
        $header['ckeditor'] = true;

        $body['id'] = $id;

        try
        {
            if (!empty($id)) $body['info'] = $this->locations->getLocations($this->session->userdata('company'), $id);
            $body['states'] = $this->functions->getStates();

            $body['emps'] = $this->users->getUsers();
            $body['territories'] = $this->territory->getTerritories($this->session->userdata('company'));
            
			$body['yesno'] = array('No', 'Yes');

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('location/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                $_POST['lat'] = $_POST['lng'] = 0;

                if (!empty($_POST['address']))
                {
                    $address = $_POST['address'];

                    if (!empty($_POST['address2'])) $address .= ' ' . $_POST['address2'];
                    if (!empty($_POST['address3'])) $address .= ' ' . $_POST['address3'];

                    $address .= " {$_POST['city']}, {$_POST['state']} {$_POST['postalCode']}";

                    // will geolcode address
                    $ll = $this->functions->getAddressLatLng($address);

                    $_POST['lat'] = $ll->lat;
                    $_POST['lng'] = $ll->lng;

                }

                if (empty($_POST['id']))
                {
                    // insert new location
                    $id = $this->location->createLocation($_POST);
                }
                else
                {
                    $id = $this->location->updateLocation($_POST);
                }

                // if location image hass been uploaded
                if (!empty($_FILES['img']['name']))
                {
                    $path = 'public' . DS . 'uploads' . DS . 'locationimgs';

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);

                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "gif|jpg|png";
                    $config['max_size'] = "5120";
                    $config['encrypt_name'] = true;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image

                    if (!$this->upload->do_upload('img'))
                    {
                        throw new Exception("Unable to upload location image!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();

                    // $_POST['locationimg'] = $uploadData['file_name'];

                    // adds location iamge
                    $this->location->addImg($id, $uploadData['file_name']);


                } // end of if statement for file upload

                $this->location->clearAssignedUsers($id);

                $this->location->saveAssignedUsers($id, $_POST);

                header("Location: /location/edit/{$id}?site-success=" . urlencode("Location has been saved!"));
                exit;
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                header("Location: /location/edit/{$_POST['id']}?site-error=" . urlencode("There was an error while updating this location."));
                exit;
            }
        }

        header("Location: /location");
        exit;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function pictures ($location)
    {
    
        try
        {
            $body['images'] = $this->locations->getImgs($location);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('location/pictures', $body);
    }

    public function youtubevideos ($location)
    {
        try
        {
            $body['youtubeVideos'] = $this->locations->getYouTubeVideos($location);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('location/youtubevideos', $body);
    }

    public function addyoutubeurl ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->location->addYouTubeUrl($_POST['location'], $_POST['url']);
                $this->functions->jsonReturn('SUCCESS', "Youtube video was added!", $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * ajax page for location edit to save youtube video roder
     */
    public function saveyoutubeorder ()
    {
        try
        {
            $this->location->updateYoutubeVideoOrder($_POST['videos']);
            $this->functions->jsonReturn('SUCCESS', "Youtube video order was updated");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteytvideo ()
    {
        if ($_POST)
        {
            try
            {
                $this->location->deleteYTVideo($_POST['id']);
                $this->functions->jsonReturn('SUCCESS', "Youtube video was deleted!");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

}
