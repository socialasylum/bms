<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newhire extends CI_Controller
{
    public $bcControllerUrl = '/newhire';
    public $bcControllerText = 'Newhire';

    public $bcViewText;

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Newhire()
    {
        parent::__construct();

        $this->load->model('newhire_model', 'newhire', true);

        $this->load->driver('cache');

        $this->load->library('documents');

        $this->functions->checkLoggedIn();

    }

    public function index ()
    {

        $header['headscript'] = $this->functions->jsScript('newhire.js');

        $header['onload'] = "newhire.indexInit();";

        $header['dynatree'] = true;

        try
        {
            $body['docs'] = $docs = $this->documents->getRequiredDocs();

            $body['title'] = $this->documents->getTableValue('title', $docs[0]->id);
            $body['body'] = $this->documents->getTableValue('body', $docs[0]->id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('newhire/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function doctree ()
    {
        try
        {
            $body['docs'] = $this->documents->getRequiredDocs();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('newhire/doctree', $body);
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function settings ()
    {
        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/newhire.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "newhire.settingsInit();";

        $header['typeahead'] = true;

        try
        {
            $body['days'] = $this->companies->getTableValue('newhireDays', $this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('newhire/settings', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function tdays ($days)
    {
        $body['days'] = (int) $days;

        try
        {
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('newhire/tdays', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function updatedays ()
    {
        if ($_POST)
        {
            try
            {
                $this->newhire->updateTrainingDays($_POST['days']);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function docsearch ()
    {
        try
        {
            $docs = $this->newhire->docSearch();

            echo json_encode($docs);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function add ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->newhire->addNewhireDoc($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Document Added', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function updateorder ()
    {
        if ($_POST)
        {
            try
            {
                $this->newhire->updateDayOrder($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Order has been updated!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
