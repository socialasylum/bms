<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller
{

    /**
     * TODO: description.
     *
     * @var mixed
     */
    public $bcControllerUrl = '/app';
    public $bcControllerText = 'Applications';

    public $bcViewText;
    

    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function App()
    {
        parent::__construct();

        $this->load->driver('cache');
        $this->load->model('app_model', 'app', true);

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/app.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "app.indexInit();";

        $header['datatables'] = true;

        try
        {
            $body['openPositions'] = $this->app->getOpenPositions();

            $body['applications'] = $this->app->getApplications();

            $body['numOP'] = count($body['openPositions']);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('app/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function postposition ()
    {
        $this->bcViewText = "Post Open Position";

        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/app.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $header['onload'] = "app.postpositionInit();";

        $header['ckeditor'] = true;

        try
        {
            $body['positions'] = $this->positions->getPositions(true, $this->session->userdata('company'));

            $body['locations'] = $this->locations->getLocations($this->session->userdata('company'));


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('app/postposition', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function getpositiondesc ($id)
    {
        try
        {
            echo $this->positions->getTableValue('description', $id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function postopenposition ()
    {
        if ($_POST)
        {
            try
            {
                $id = $this->app->insertCompanyPosition($_POST);
                $this->functions->jsonReturn('SUCCESS', $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function view ($id)
    {
        if (empty($id))
        {
            header("Location: /app?site-error=" . urlencode("Applicaiton ID not specified!"));
            exit;
        }


        try
        {
            $body['info'] = $this->app->getApplications($id);

            $body['edu'] = $this->app->getEdu($id);
            $body['jobs'] = $this->app->getJobs($id);

            $body['companyName'] = $this->companies->getCompanyName($body['info']->company);

            $body['processApplications'] = $this->modules->checkPermission($this->router->fetch_class(), 1);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('app/view', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function widget ()
    {
        try
        {
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('app/widget', $body);
    }
}
