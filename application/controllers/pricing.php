<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pricing extends CI_Controller
{
    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Pricing()
    {
        parent::__construct();

        $this->load->model('pricing_model', 'pricing', true);
        $this->load->driver('cache');
    }

    public function index()
    {

        $header['headscript'] = $this->functions->jsScript('pricing.js');

        $header['onload'] = "pricing.indexInit();";

        $header['nav'] = 'pricing';

        try
        {
            $body['modules'] = $this->pricing->getModules();
            $body['unvModules'] = $this->pricing->getModules(true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('pricing/home', $body);
        $this->load->view('template/footer');
    }
}

/* End of file pricing.php */
/* Location: ./application/controllers/pricing.php */
