<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Errors extends CI_Controller
{

    public $bcControllerUrl = '/errors';
    public $bcControllerText = "<i class='fa fa-ban'></i> Errors";

    public $bcViewText;


    function Errors()
    {
        parent::__construct();

        $this->load->model('errors_model', 'errors', true);
        $this->load->driver('cache');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        header("Location: /intranet/landing");
        exit;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function html404 ()
    {
        $this->output->set_status_header('404');

        $body['content'] = 'error_404';


        $this->load->view('template/header_apply', $header);
        $this->load->view('errors/html404', $body);
        $this->load->view('template/footer_apply', $footer);
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function access ()
    {
        $this->bcViewText = "Access";

        try
        {
            
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('errors/access', $body);
        $this->load->view('template/footer_intranet', $footer);
    }
}
