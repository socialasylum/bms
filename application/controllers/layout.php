<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'application/third_party/less/lessc.inc.php';
	
class Layout extends CI_Controller
{
	public $path;
	public $less;
	
	public $bcControllerUrl = '/layout';
	public $bcControllerText = "<i class='fa fa-question-circle'></i> Layout";
	
	public $bcViewText;
	
	function Layout()
	{
	    parent::__construct();
	
		//$this->output->enable_profiler(true);

	    $this->load->model('layout_model', 'layout', true);
	
	    $this->load->driver('cache');
	
	    $this->functions->checkLoggedIn();
	    
	    $this->less = new lessc();
	
		$this->path = 'public' . DS . 'uploads' . DS . 'layout' . DS . $this->session->userdata('company') . DS;
	}
	
	
	public function index ()
	{
		$header['headscript'] = $this->functions->jsScript('layout.js');
        $header['onload'] = "layout.index.init();";
        $header['uploader'] = true;
        $header['jqueryui'] = true;
        $header['inputSpinner'] = true;
        $header['colorpicker'] = true;
        
		try
		{

			$body['bgFills'] = $this->style->bgFills;
			$body['values'] = $this->style->getAllValues($this->session->userdata('company'));
			
			$body['icon'] = $this->modules->getIcon();
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}

        $this->load->view('template/header_intranet', $header);
        $this->load->view('layout/index', $body);
        $this->load->view('template/footer_intranet', $footer);
	}
	
	public function save_values ()
	{
		try
		{
			if ($_POST)
			{

				// ensures company uploads directory has been created
				PHPFunctions::createDir($this->path);
                    
				// saves value
				$this->layout->saveValues($this->session->userdata('company'), $_POST);
				
				// checks if there are DB values
				$hasStyles = $this->style->hasStyles($this->session->userdata('company'));
			
				if (!$hasStyles) PHPFunctions::jsonReturn('WARNING', "No style attributes to preview");
				
				// builds preview
				$build = $this->layout->compile($this->session->userdata('company'), true);
				
                PHPFunctions::jsonReturn('SUCCESS', "Layout changes have been saved!");
			}
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}

	public function upload ()
	{
		try
		{
			if ($_FILES)
			{
				// if profile image hass been uploaded
                if (!empty($_FILES['file']['name']))
                {
                    // ensures company uploads directory has been created
                    PHPFunctions::createDir($this->path);

					$config['upload_path'] = './' . $this->path;
					$config['allowed_types'] = "gif|jpg|png";
					$config['max_size'] = "5120";
					$config['encrypt_name'] = true;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image
                    if (!$this->upload->do_upload('file'))
                    {
                        throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();
                    
                    $this->layout->clearValue($this->session->userdata('company'), $_POST['type']);
                    
                    $this->layout->insertValue($this->session->userdata('company'), $_POST['type'], $uploadData['file_name']);
   
                    PHPFunctions::jsonReturn('SUCCESS', "Image has been uploaded!", true, 0, array('file' => $uploadData['file_name']));

                }
			}
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
		}
	}
	
	
	public function preview ()
	{
		try
		{

		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
		}	

        $this->load->view('template/header_intranet', $header);
        $this->load->view('layout/preview', $body);
        $this->load->view('template/footer_intranet', $footer);
	}
	
	
	// dynamicalliy generates less stylesheet for company layout
	public function compile ()
	{
		try
		{
			$hasStyles = $this->style->hasStyles($this->session->userdata('company'));
			
			if (!$hasStyles) PHPFunctions::jsonReturn('WARNING', "No style attributes to compile");
				
			$build = $this->layout->compile($this->session->userdata('company'));
		
            PHPFunctions::jsonReturn('SUCCESS', "Styles have been built!");
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
		

	}
	
	public function less ($preview = false)
	{
		$body['preview'] = (bool) $preview;
			
		header("Content-type: text/css");
		
		try
		{
			$body['company'] = $this->session->userdata('company');
			$body['values'] = $this->style->getAllValues($this->session->userdata('company'));
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
		}
		
		$this->load->view('layout/less', $body);	
	}
	
	public function deleteImg ()
	{
		try
		{
			if ($_POST)
			{
				// removes image from server
				$this->layout->deleteImg($this->path . $_POST['file']);

				// clears layout value if passed
				if (!empty($_POST['type'])) $this->layout->clearValue($this->session->userdata('company'), $_POST['type']);
			
                PHPFunctions::jsonReturn('SUCCESS', "Image has been deleted!");
			}
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
	
	public function reset ()
	{
		try
		{
			// deletes layout files
			PHPFunctions::delDirContent($_SERVER['DOCUMENT_ROOT'] . $this->path);
			
			$this->layout->clearCompValues($this->session->userdata('company'));
			
            PHPFunctions::jsonReturn('SUCCESS', "Layout has been reset!");
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
	
	// admin function, rebuilds Site CSS
	public function rebuildCSS ()
	{
		try
		{
			if (!$this->users->isAdmin($this->session->userdata('userid'))) throw new Exception("You are not an admin!");
			// deletes site files
			@unlink($_SERVER['DOCUMENT_ROOT'] . 'public/less/main.css');
			@unlink($_SERVER['DOCUMENT_ROOT'] . 'public/less/.min_version');
			
			$params = array('public/less/', array('main.less' => 'main.css'), ENVIRONMENT, $this->config->item('min_version'));

			$compiler = new CI_less_pre_sys();
			
			$compiler->initialize($params);
			
			PHPFunctions::jsonReturn('SUCCESS', "CSS has been rebuilt. Reloading CSS...");
		}
		catch (Exception $e)
		{
            PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
}