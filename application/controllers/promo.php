<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends CI_Controller
{
    public $bcControllerUrl = '/promo';
    public $bcControllerText = "<i class='fa fa-star-o'></i> Promotions";

    public $bcViewText;


    function Promo ()
    {
        parent::__construct();

        $this->load->model('promo_model', 'promo', true);
        $this->load->model('item_model', 'item', true);
        $this->load->driver('cache');

        $this->functions->checkLoggedIn();


        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = $this->functions->jsScript('promo.js');
        $header['datatables'] = true;
    
        $header['onload'] = "promo.indexInit();";
            
        try
        {
            $body['promos'] = $this->promo->getPromos();
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('promo/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {
        $header['headscript'] = $this->functions->jsScript('promo.js');

        $header['onload'] = "promo.editInit();";
        $header['ckeditor'] = true;

        $this->bcViewText = (empty($id)) ? "<i class='fa fa-edit'></i> Create Promotion" : "<i class='fa fa-edit'></i> Edit Promotion";

        $body['status'] = array('Deactived', 'Active');


        $body['id'] = $id;

        try
        {
            $body['items'] = $this->item->getItems();
            $body['actTypes'] = $this->functions->getCodes(21);

            if (!empty($id))
            {
                $body['info'] = $this->promo->promoInfo($id);
                $body['promoItems'] = $this->promo->getPromoItems($id);
            }
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('promo/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {

                if (!empty($_FILES['bannerImg']['name']))
                {
                    $path = 'public' . DS . 'uploads' . DS . 'promoimgs' . DS . $this->session->userdata('company');

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);

                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "gif|jpg|png";
                    $config['max_size'] = "5120";
                    $config['encrypt_name'] = true;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image

                    if (!$this->upload->do_upload('bannerImg'))
                    {
                        throw new Exception("Unable to upload banner image!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();

                    $_POST['bannerImg'] = $uploadData['file_name'];

                    // error_log("Banner Image was uploaded: {$uploadData['file_name']}");
                }

                if (empty($_POST['id']))
                {
                    $_POST['id'] = $this->promo->insertPromo($_POST);
                }
                else
                {
                    if (empty($_POST['bannerImg'])) $_POST['bannerImg'] = $_POST['currentBannerImg'];

                    $this->promo->updatePromo($_POST);
                }

                $this->promo->savePromoItems($_POST);

                header("Location: /promo/edit/{$_POST['id']}?site-success=" . urlencode("Promotion has been saved!"));
                exit;
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                header("Location: /promo/edit/{$_POST['id']}?site-error=" . urlencode($e->getMessage()));
                exit;
            }
        }
    }
}
