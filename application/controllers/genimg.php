<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Genimg extends CI_Controller
{
    function Genimg()
    {
        parent::__construct();

        // $this->load->model('location_model', 'location', true);
        $this->load->driver('cache');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $fileName 
     * @param mixed $path     Optional, defaults to '/'. 
     * @param mixed $size     Optional, defaults to 50. 
     *
     * @return TODO
     */
    public function render ($size = 50)
    {
        // decodes img name and path
        $img = urldecode($_GET['img']);
        $path = urldecode($_GET['path']);

        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . $path . DS;

        try
        {
            // no profile image is set - loads no profile img
            if (empty($img))
            {
                $path = $_SERVER["DOCUMENT_ROOT"] . DS . 'public' . DS . 'images' . DS;
                $img = 'dude.gif';
            }

            $is = getimagesize($path . $img);

            if ($is === false) throw new exception("Unable to get image size for ({$path}{$img})!");

            $ext = $this->functions->getFileExt($img);

            list ($width, $height, $type, $attr) = $is;

                if ($width == $height)
                {
                    $nw = $nh = $size;
                }
                elseif ($width > $height)
                {
                    $scale = $size / $height;
                    $nw = $width * $scale;
                    $nh = $size;
                    $leftBuffer = (($nw - $size) / 2);
                }
                else
                {
                    $nw = $size;
                    $scale = $size / $width;
                    $nh = $height * $scale;
                    $topBuffer = (($nh - $size) / 2);
                }

            $leftBuffer = $leftBuffer * -1;
            $topBuffer = $topBuffer * -1;

            if ($ext == "jpg") $srcImg = imagecreatefromjpeg($path . $img);
            if ($ext == "gif") $srcImg = imagecreatefromgif($path  . $img);
            if ($ext == "png") $srcImg = imagecreatefrompng($path  . $img);

            $destImg = imagecreatetruecolor($size, $size); // new image

            #imagecopyresized($destImg, $srcImg, 0, 0, $leftBuffer, $topBuffer, $nw, $nh, $width, $height);
            imagecopyresized($destImg, $srcImg, $leftBuffer, $topBuffer, 0, 0, $nw, $nh, $width, $height);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        #echo "NW: {$nw} | NH: {$nh} | Scale: {$scale} | ";
        #echo "topBuffer: {$topBuffer}";
        #echo "leftBuffer: {$leftBuffer}";
        header('Content-Type: image/jpg');
        imagejpeg($destImg);

        imagedestroy($destImg);
        imagedestroy($srcImg);


    }
}
