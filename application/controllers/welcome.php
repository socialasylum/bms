<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Welcome ()
    {
        parent::__construct();
        
        // checks database is setup
		//if (!file_exists('application/config/database.production.php')) PHPFunctions::redirect('/setup');
        
        $this->load->driver('cache');
        $this->load->model('blog_model', 'blog', true);
    }

    public function index()
    {
        $header['headscript'] = $this->functions->jsScript('users.js');

        $header['nav'] = 'home';
        $header['datatables'] = true;
        $header['typeahead'] = true;

		try
		{
			$indexUrl = $this->companies->getHostIndex();
			
			if (!empty($indexUrl) && $indexUrl !== '/')
			{
				header("Location: " . $indexUrl);
				exit;
			}
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}

        $header['onload'] = "users.indexInit();";

        $this->load->view('template/header', $header);
        $this->load->view('welcome/home');
        $this->load->view('template/footer');
    }

    public function phpinfo()
    {
        $this->functions->checkLoggedIn();

        phpinfo();
    }

    public function termsofuse ()
    {
        $this->load->view('template/header');
        $this->load->view('welcome/termsofuse');
        $this->load->view('template/footer');
    }

    public function privacypolicy ()
    {
        $this->load->view('template/header');
        $this->load->view('welcome/privacypolicy');
        $this->load->view('template/footer');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function contactus ()
    {
        $header['headscript'] = $this->functions->jsScript('contactus.js');
        $header['onload'] = "contactus.init();";

        $header['nav'] = 'contactus';

        $this->load->view('template/header', $header);
        $this->load->view('welcome/contactus', $body);
        $this->load->view('template/footer');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function sendcontactus ()
    {
        $this->load->model('welcome_model', 'welcome', true);

        if ($_POST)
        {
            try
            {
                // saves contat form in db
                $this->welcome->insertContactus($_POST);


                $subject = "New Contact Us Form! {$_POST['name']}";


                $message = "
                <h1>New Contact Us Form</h1>

                <p><strong>Name:</strong> {$_POST['name']}</p>
                <p><strong>E-mail:</strong> {$_POST['email']}</p>
                <p><strong>Phone:</strong> {$_POST['phone']}</p>
                <p><strong>Message:</strong></p>
                " .
                nl2br($_POST['message']);

                $emailTo = array ('bvinall@cgisolution.com', 'williamgallios@gmail.com');

                // will now send out email
                $this->functions->sendEmail($subject, $message, $emailTo);

                $this->functions->jsonReturn('SUCCESS', "Contact us form has been received!");

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }

        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
