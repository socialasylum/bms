<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller
{
    public $bcControllerUrl = '/item';
    public $bcControllerText = "<i class='fa fa-shopping-cart'></i> Item Management";

    public $bcViewText;

    function Item ()
    {
        parent::__construct();

        $this->load->model('item_model', 'item', true);
        $this->load->model('promo_model', 'promo', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($folder = 0, $rootFolder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('item.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');

        $header['onload'] = "item.indexInit();";
        $header['rightlick'] = true;

        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;

        try
        {
            $body['content'] = $this->item->getFolderContent($folder);
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
            $body['parentFolder'] = $this->folders->getTableValue('parentFolder', $folder);
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('item/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function edit ($id = 0, $folder = 0)
    {
        $this->bcViewText = (empty($id)) ? "<i class='fa fa-edit'></i> Create Item" : "<i class='fa fa-edit'></i> Edit Item";

        $header['headscript'] = $this->functions->jsScript('item.js');

        $header['onload'] = "item.editInit();";
        $header['ckeditor'] = true;
        $header['uploader'] = true;

        $body['id'] = $id;
        $body['folder'] = $folder;

        try
        {
            if (!empty($id))
            {
                $body['info'] = $this->item->getInfo($id);
                $body['options'] = $this->item->getItemOptions($id);

                $body['images'] = $this->item->getItemImages($id);
            }
            
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
            $body['promos'] = $this->promo->getPromos();
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('item/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * Saves an item
     */
    public function save ()
    {
        if ($_POST)
        {
            // print_r($_POST);die;

            try
            {
                if (empty($_POST['id']))
                {
                    $id = $this->item->insertItem($_POST);

                    // assigns item to current folder
                    $this->item->assignItemToFolder($id, $_POST['folder']);

                    $msg = "Item has been created!";

                    $this->item->saveOptions($id, $_POST);
                }
                else
                {
                    $id = $this->item->updateItem($_POST);

                    $msg = "Item has been updated!";
                }

                $this->item->saveAssignedPromos($id, $_POST);

                $this->item->saveUploadedImages($id, $_POST['img']);

                // if item image hass been uploaded
                /*
                if (!empty($_FILES['image']['name']))
                {

                    $path = 'public' . DS . 'uploads' . DS . 'itemimgs' . DS . $this->session->userdata('company') . DS;

                    // ensures profileimgs folder is create
                    $this->functions->createDir('public' . DS . 'uploads' . DS . 'itemimgs' . DS);

                    // ensures company uploads directory has been created
                    $this->functions->createDir($path);

                    $config['upload_path'] = './' . $path;
                    $config['allowed_types'] = "gif|jpg|png";
                    $config['max_size'] = "5120";
                    $config['encrypt_name'] = true;

                    // loads upload library
                    $this->load->library('upload', $config);

                    // will now upload actual image

                    if (!$this->upload->do_upload('image'))
                    {
                        throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                    }

                    $uploadData = $this->upload->data();

                    $_POST['itemimg'] = $uploadData['file_name'];

                }
                */

                $this->functions->jsonReturn('SUCCESS', $msg, $id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);

                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function rightclick ($folder = 0)
    {
        $body['folder'] = $folder;

        $this->load->view('item/rightclick', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function renderimgs ()
    {
        $body['pictures'] = array();

        try
        {
            if ($_POST)
            {
                if (!empty($_POST['img']))
                {
                    foreach ($_POST['img'] as $k => $file)
                    {
                        $body['pictures'][] = $file;
                    }
                }
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('item/renderimgs', $body);
    }

    /**
     * ajax page used when deleting images in item/edit
     *
     * @return JSON response
     */
    public function deleteitemimg ()
    {
        if ($_POST)
        {
            try
            {
                // first deletes actual file from server
                $this->item->deleteImgFromServer($_POST['fileName']);

                // updates items picture table
                $this->item->deleteItemImg($_POST['item'], $_POST['fileName']);

                $this->functions->jsonReturn('SUCCESS', 'Item Image has been deleted!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deleteitem ()
    {
        if ($_POST)
        {
            try
            {
                $this->item->setDeleted($_POST['id']);
                $this->functions->jsonReturn('SUCCESS', "Item ({$_POST['id']}) has been deleted!");
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
