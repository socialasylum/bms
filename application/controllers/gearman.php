<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gearman extends CI_Controller
{

	protected $worker, $serverLockFile;
	
	function Gearman ()
    {
    	set_time_limit(0);
    	
        parent::__construct();
     	
		$this->load->driver('cache');
	}

	protected function __initServer ()
	{

		$this->load->model('msg_model', 'msg', true);
		$this->load->library('msgs');
	        
		elog("Initializing Gearman Server");
		
		$this->worker = new GearmanWorker();
		
		$this->worker->addServer('127.0.0.1', 4730);
		
		$this->worker->addFunction('serverSync', function(GearmanJob $job) {

			$this->download_mailbox($job);
			//$this->sync_tasks($job);
				
		});
		//$this->worker->addFunction('download_mailbox', array($this, 'server_sync'));

		elog("Waiting for jobs");
		//println("Waiting for jobs");
		
		while ($this->worker->work())
		{
			//println("Code: " . $this->worker->returnCode());
			
			if ($this->worker->returnCode() != GEARMAN_SUCCESS)
			{
				println("Worker Failed!" . $this->worker->error());
				break;
			}
		}

		//while ($this->worker->work());
	}
	
	public function server ()
	{
		try
		{	
			$this->__initServer();
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
		return true;
	}
	
	// downloads all email messages for a user
	public function sync ($userid)
	{
		try
		{
			if (empty($userid)) throw new Exception("User ID is empty!");
		
			$this->load->model('msg_model', 'msg', true);
	        $this->load->library('msgs');
	        
			if ($this->msg->check_sync_lock($userid)) PHPFunctions::jsonReturn('WARNING', "Process is already running", true);
			
			$client = new GearmanClient();
			
			$server = $client->addServer('127.0.0.1', 4730);
	
			if ($server === false) throw new Exception("Unable to connect to gearman server");
	
			$client->setDataCallback(array($this, 'sync_data'));
			$client->setFailCallback(array($this, 'sync_fail'));
			$client->setStatusCallback(array($this, 'sync_status'));
			$client->setCompleteCallback(array($this, 'sync_complete'));
			
			$done = false;
	
			// create lock
			$this->_sync_lock($userid);
			
			$this->msg->saveSyncStatus($userid, 'STARTED');
	/*
	do
	{
	
		sleep(1);
		
		$stat = $client->jobStatus($sync);
	   
		if (!$stat[0]) $done = true;
	   
		$running = ($stat[1]) ? true : false;
		
		$per = (empty($stat[3])) ? 0 : ($stat[2]/$stat[3]);		
	  
		println($per . '%');
	//   echo "Running: " . ($stat[1] ? "true" : "false") . ", numerator: " . $stat[2] . ", denomintor: " . $stat[3] . "\n";
	
	}
	while(!$done);
	*/
			do{
	
	
				$sync = $client->doLow('serverSync', $userid);
	
				//$client->addTaskStatus($sync);
				//$client->runTasks();
				
				switch ($client->returnCode())
				{
					case GEARMAN_WORK_DATA:
						println($sync);
					break;
					case GEARMAN_WORK_STATUS:
						list($num, $den) = $client->doStatus();
						
						$per = (empty($den)) ? 0 : number_format($num / $den, 1);
						
						println("{$per}% complete");
					break;
					case GEARMAN_WORK_FAIL:
						
						$this->msg->saveSyncStatus($userid, 'ERROR', array('error' => $sync));
						
						$this->_clear_sync_lock($userid);
						
						PHPFunctions::jsonReturn('ERROR', "Download Failed", false, $client->returnCode(), array('error' => $sync));
						exit;
					break;
					case GEARMAN_SUCCESS:
						
						$this->msg->saveSyncStatus($userid, 'SUCCESS', array('data' => $sync));
						
						$this->_clear_sync_lock($userid);
						
						PHPFunctions::jsonReturn('SUCCESS', "Download Successful", false, 0, array('data' => $sync));
	
					break;
					default:
						$this->msg->saveSyncStatus($userid, 'ERROR', array('error' => $client->error(), 'errorNo' => $client->getErrno()));
						
						$this->_clear_sync_lock($userid);
						
						PHPFunctions::jsonReturn('ERROR', "Download Failed", false, $client->returnCode(), array('error' => $client->error(), 'errorNo' => $client->getErrno()));
					exit;
				}
				
			}
			while($client->returnCode() != GEARMAN_SUCCESS);

			$this->_clear_sync_lock($userid);

			//println("Sync Done: {$sync}");
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			
			if (!empty($userid))
			{
				$this->msg->saveSyncStatus($userid, 'ERROR', $e);	
				$this->_clear_sync_lock($userid);
			}
		}

		return true;
	}
	/*
	public function sync_tasks ($job)
	{
		$task = $client->addTaskLow('download_mailbox', $userid);
		$client->runTasks();
		
		return true;
	}
	*/

	
	private function _sync_lock ($userid)
	{
		if (empty($userid)) throw new Exception("User ID is empty!");
		
		//if ($this->_check_sync_lock($userid)) return false;
	
		$file = @touch("/tmp/bms_sync_lock_{$userid}");
		
		if ($file === false) throw new Exception("Unable to set sync lock");
		
		return $file;
	}
	
	
	
	
	// clears lock
	private function _clear_sync_lock ($userid)
	{
		if (empty($userid)) throw new Exception("User ID is empty!");
		
		@unlink("/tmp/bms_sync_lock_{$userid}");
		
		return true;
	}
	
	public function sync_status ($task)
	{
		println("status function " . $task->data());
	}

	public function sync_data ($task)
	{
		println("data callback: " . $task->data());
		return true;
	}
	
	public function sync_complete ($task)
	{
		println("Sync has been completed");
		
		println($task->data());
	}
	
	public function sync_fail ($task)
	{
		println("Sync has failed!");
		
		println($task->data());
	}

	public function download_mailbox ($job)
	{
		$userid	= $job->workload();
		
		if (empty($userid)) throw new Exception("User ID is empty!");

		elog("Downloading mailbox for {$userid}");

		try
		{
			$job->sendData("Checking default folders...");
			$this->msg->checkDefaultFolders($userid);
			$job->sendData("Done!\n");
			
			$job->sendData("Downloading mailbox for {$userid}\n");
			
			$this->msg->getExternalFolders($userid, true, $job);
			
			elog("Download Complete");
			
			$job->sendComplete();
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			$job->sendException($e->getMessage());
			$job->sendFail();
		}
		
		//return true;
	}
}