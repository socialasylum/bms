<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companysettings extends CI_Controller
{

    public $bcControllerUrl = '/companysettings';
    public $bcControllerText = "<i class='fa fa-gears'></i> Company Settings";

    public $bcViewText;

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Companysettings()
    {
        parent::__construct();

        $this->load->model('companysettings_model', 'companysettings', true);
        $this->load->model('pricing_model', 'pricing', true);

        $this->load->library('payment');

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

        try
        {
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {
        $header['headscript'] = $this->functions->jsScript('companysettings.js');
		$header['datatables'] = true;

        $header['onload'] = "compsettings.index.init();";

        try
        {
			$body['companies'] = $this->companysettings->getCompanies();
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('companysettings/index', $body);
        $this->load->view('template/footer_intranet');
    }
    
    public function edit ($company)
    {
        $header['headscript'] = $this->functions->jsScript('companysettings.js');

        $header['onload'] = "compsettings.edit.init();";
        
        $header['jqueryui'] = true;
        $header['uploader'] = true;
        $header['ckeditor'] = true;
		
        try
        {
            $body['info'] = $info = $this->companysettings->getInfo($company);
            $body['states'] = $this->functions->getStates();

            $body['modules'] = $this->pricing->getModules();

            $body['assignedModules'] = $this->modules->getAssignedModules($company);
            $body['cards'] = $this->companysettings->getOnfileCards($company);

            $body['subStatus'] = $this->functions->checkSubscriptionStatus($company);

			$body['domains'] = $this->companies->getDomains($company);
			
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }
		
		$this->bcViewText = "<i class='fa fa-edit'></i> {$info->companyName}";
		
        $this->load->view('template/header_intranet', $header);
        $this->load->view('companysettings/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function update ()
    {
        if ($_POST)
        {

            $this->load->model('signup_model', 'signup', true);

            try
            {

                // removes any type of formating to the CC number to ensure its only a number
                $_POST['ccNumber'] = $this->functions->onlyNumbers($_POST['ccNumber']);


                $this->companysettings->updateCompany($_POST);

                // first clears assigned modules
                $this->companysettings->clearCompanyModules($_POST['id']);

                // gets list of all current universal modules
                $unvModules = $this->pricing->getModules(true);

                // assign universal modules
                $this->signup->assignUnvModules($_POST['id'], $unvModules);


                $moduleTotal = 0.0;

                // save assigned modules
                if (!empty($_POST['module']))
                {
                    foreach ($_POST['module'] as $module)
                    {
                        $module = intval($module);

                        // skips module if ID is empty
                        if (empty($module)) continue;

                        // saves module to company
                        $this->modules->insertCompanyModule($_POST['id'], $module);

                        // gets price of module
                        $modprice = $this->modules->getTableValue('price', $module);

                        // error_log($modprice);

                        $moduleTotal += $modprice;
                    }
                }

                // this is the final amount to be charged monthly
                $price = ($_POST['users'] * $moduleTotal) + 100;

                $this->companysettings->setsubscriptionPrice($_POST['id'], $price);

                // boolean if there was a price change
                $priceChange = ((double) $_POST['orgPrice'] == (double) $price) ? false : true;

                // they are not exempt and must go through the payment process
                if ((bool) $_POST['exempt'] == false)
                {

                    // will now try payment after all information has been saved
                    $BrainTreeSubscriptionID = $this->companies->getTableValue('BrainTreeSubscriptionID', $_POST['id']);

                    $BrainTreeCustomerID = $this->companies->getTableValue('BrainTreeCustomerID', $_POST['id']);
                    
                    // gets current status of subscription
                    $subStatus = $this->functions->checkSubscriptionStatus($_POST['id']);

                    if (empty($BrainTreeSubscriptionID) || $subStatus !=='ACTIVE' || $priceChange == true)
                    {
                        if (empty($BrainTreeCustomerID))
                        {
                            // will create a customer

                            $data = array
                                (
                                    'company' => $_POST['id'],
                                    'firstName' => $_POST['billingFirstName'],
                                    'lastName' => $_POST['billingLastName'],
                                    'companyName' => $_POST['companyName'],
                                    'email' => $_POST['email'],
                                    'phone' => $_POST['phone']
                                );

                            $BrainTreeCustomerID = $this->payment->createCustomer($data);
                        }

                        if (!empty($_POST['ccNumber']))
                        {
                            $data = array
                                (
                                    'BTcustomerID' => $BrainTreeCustomerID,
                                    'company' => $_POST['id'],
                                    'ccName' => $_POST['ccName'],
                                    'ccCvv' => $_POST['ccCvv'],
                                    'ccNumber' => $_POST['ccNumber'],
                                    'ccExpM' => $_POST['ccExpM'],
                                    'ccExpY' => $_POST['ccExpY']
                                );

                            $creditCardToken = $this->payment->createCreditCard($data);
                        }
                        else
                        {
                            // get token of card on file selected
                            $creditCardToken = $this->payment->getCardTokenByID($_POST['previousCard']);
                        }

                        if ($subStatus == 'ACTIVE')
                        {
                            // current subscription is active, simply an update needs to be made due to price change
                            $this->payment->updateSubscription($BrainTreeSubscriptionID, $creditCardToken, 'company_plan_monthly', $price);
                        }
                        else
                        {
                            // subscription is not active, will attempt create a new one
                            $BrainTreeSubscriptionID = $this->payment->createSubscription($_POST['id'], $creditCardToken, 'company_plan_monthly', $price);
                        }

                    }
                }
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
    			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
            }

        }

    	PHPFunctions::jsonReturn('SUCCESS', "Settings &amp; Subscription have been updated!");
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function cancelsub ()
    {
        try
        {
            $subscriptionID = $this->companies->getTableValue('BrainTreeSubscriptionID', $_POST['company']);

            $this->payment->cancelSubscription($subscriptionID);

            $this->functions->jsonReturn('SUCCESS', 'Subscription has been cancelled!');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }
    
    public function logoupload ()
    {
	    try
	    {
            if (!empty($_FILES['file']['name']))
            {
                // upload a company logo

                $path = 'public' . DS . 'uploads' . DS . 'logos' . DS . $_POST['company'] . DS;

                // ensures logos folder is create
                $this->functions->createDir('public' . DS . 'uploads' . DS . 'logos' . DS);

                // ensures company uploads directory has been created for uploads
                $this->functions->createDir($path);

                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "gif|jpg|png";
                $config['max_size'] = "5120";
                $config['encrypt_name'] = true;

                // loads upload library
                $this->load->library('upload', $config);

                // will now upload actual image

                if (!$this->upload->do_upload('file'))
                {
                    throw new Exception("Unable to upload company logo!" . $this->upload->display_errors());
                }

                $uploadData = $this->upload->data();

                $logo = $uploadData['file_name'];

                $this->companysettings->resizeLogo($logo, $_POST['company']);
                
                $this->companysettings->updateLogo($_POST['company'], $logo);
				
				$thumb = str_replace('.', '_thumb.', $logo);
				
				$this->functions->jsonReturn('SUCCESS', 'Logo has been uploaded!', true, 0, array('file' => $logo, 'thumb' => $thumb));
   
            }
	    }
	    catch (Exception $e)
	    {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
	    }
    }
    
    public function rmlogo ()
    {
	    try
	    {
	    	if ($_POST)
	    	{
		    	// deletes file
		    	@unlink("{$_SERVER['DOCUMENT_ROOT']}public/uploads/logos/{$_POST['company']}/{$_POST['file']}");
		    	
		    	// clears DB value
				$this->companysettings->updateLogo($_POST['company'], null);
				
				$this->functions->jsonReturn('SUCCESS', 'Logo has been removed!');
	    	}
	    }
	    catch (Exception $e)
	    {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());	    
	    }
    }
    
    public function adddomain ()
    {
	    try
	    {
	    	if ($_POST)
	    	{
		    	$id = $this->companysettings->addDomain($this->session->userdata('userid'), $_POST['company'], $_POST['domain']);
		    	
				PHPFunctions::jsonReturn('SUCCESS', "<b>{$_POST['domain']}</b> has been added", true, $id);   
	    	}
	    }
	    catch (Exception $e)
	    {
            PHPFunctions::sendStackTrace($e);
			PHPFunctions::jsonReturn('ERROR', $e->getMessage());   
	    }
    }
    
    public function removedomain ()
    {
	    try
	    {
	    	if ($_POST)
	    	{
		    	$this->companysettings->removeDomain($_POST['id'], $_POST['company']);
		    	
				PHPFunctions::jsonReturn('SUCCESS', 'Domain has been removed!');   
	    	}
	    }
	    catch (Exception $e)
	    {
            PHPFunctions::sendStackTrace($e);
			PHPFunctions::jsonReturn('ERROR', $e->getMessage());
	    }
    }
}
