<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller
{
    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Articles()
    {
        parent::__construct();

        $this->load->model('blog_model', 'blog', true);
        $this->load->driver('cache');
        $this->load->library('blogs');
    }

    public function index($page = 1)
    {

        $header['headscript'] = $this->functions->jsScript('articles.js');
        $header['nav'] = 'blog';

        $body['namespace'] = $this->router->fetch_class();

        try
        {
            // pulls only homepage blogs
            $body['blogs'] = $this->blog->getBlogs($page, 5, 1);

            $body['editBlog'] = false;

            $body['totalBlogs'] = $totalBlogs = $this->blog->getTotalBlogCnt(1, 5);

            $body['totalPages'] = $totalPages = ceil((int) $totalBlogs / (int) $this->config->item('blogsPerPage'));

            // $header['name'] = $this->users->getName($this->session->userdata('userid'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('blog/index', $body);
        $this->load->view('template/footer');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function view ($id)
    {

        $header['headscript'] = $this->functions->jsScript('articles.js');
        $header['headscript'] .= $this->functions->jsScript('blog.js');

        $header['onload'] = "articles.viewInit();";
        $header['nav'] = 'blog';

        $header['ckeditor'] = true;

        $body['id'] = $id;

        try
        {
            $body['namespace'] = $this->router->fetch_class();
            $body['info'] = $this->blog->getBlogInfo($id, 1);
            $body['comments'] = $this->blog->getBlogComments($id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('blog/view', $body);
        $this->load->view('template/footer');

    }

    public function savecomment ()
    {
        if ($_POST)
        {
            try
            {
                $this->blog->insertComment($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Comment has been saved!');
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }

        $this->functions->jsonReturn('ERROR', 'GET not supported!');

    }
}
