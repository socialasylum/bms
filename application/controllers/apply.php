<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Apply extends CI_Controller
{

    /**
     * TODO: short description.
     * 
     * @return TODO
     */
    function Apply()
    {

        parent::__construct();

        $this->load->driver('cache');
        $this->load->model('apply_model', 'apply', true);
    }


    /**
     * TODO: short description.
     *
     * @param mixed $company 
     *
     * @return TODO
     */
    public function index ($company)
    {

        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/apply.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        try
        {
            $header['companyName'] = $this->companies->getCompanyName($company);

            $body['openPositions'] = $this->apply->getOpenPositions($company);

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header', $header);
        $this->load->view('apply/index', $body);
        $this->load->view('template/footer');
    }


    public function process ($id = null, $page = 1, $edit = false)
    {

        // session_start();

        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/apply.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";

        $body['id'] = $id;
        $body['page'] = $page;
        $body['edit'] = $edit;

        // where user addes education
        if ($page == 13) 
        {
            $header['onload'] = "apply.step13Init();";
        }


        // where user adds job history
        if ($page == 14)
        {
            $header['onload'] = "apply.step14Init();";
        }

        try
        {
            $company = $this->apply->getAppCompany($id);

            $body['companyName'] = $header['companyName'] = $this->companies->getCompanyName($company);

            if ($page == 3)
            {
                $body['states'] = $this->functions->getStates();
            }

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('apply/step' . $page, $body);
        $this->load->view('template/footer');
    }


    public function save()
    {

        // session_start();

        if ($_POST)
        {

            // $this->sessions->user
            // $_SESSION = array_merge($_SESSION, $_POST);


            $this->session->set_userdata($_POST);

            if ($_POST['page'] >= 16 || $_POST['edit'] == true)
            {
                //finished with application
                header("Location: /apply/review/{$_POST['id']}");
                exit;
            }
            else
            {
                // goes to next step
                header("Location: /apply/process/{$_POST['id']}/" . ($_POST['page'] + 1));
                exit;
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function addEdu ($cnt)
    {
        $body['cnt'] = $cnt;

        try
        {
            $body['types'] = $this->functions->getCodes(4, 'code');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('apply/addEdu', $body);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $cnt 
     *
     * @return TODO
     */
    public function addJob ($cnt)
    {
        $body['cnt'] = $cnt;

        try
        {
            $body['types'] = $this->functions->getCodes(4, 'code');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('apply/addJob', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function review ($id)
    {
        $body['id'] = $id;
        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/apply.js".$this->config->item('min_debug')."&amp;".$this->config->item('min_version')."'></script>";

        try
        {
            // if(!empty($_SESSION['referred'])) $body['reasonDisplay'] = $this->Application_model->codeDisplay(14, $_SESSION['referred']);
            // if(!empty($_SESSION['position'])) $body['positionDisplay'] = $this->Application_model->codeDisplay(37, $_SESSION['position']);
            // if(!empty($_SESSION['kiosk'])) $body['locationInfo'] = $this->Application_model->getStoreInfo($_SESSION['kiosk']);

            $company = $this->apply->getAppCompany($id);

            $body['companyName'] = $header['companyName'] = $this->companies->getCompanyName($company);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_apply', $header);
        $this->load->view('apply/review', $body);
        $this->load->view('template/footer_apply');
    }

    public function finish ($id)
    {
        $body['id'] = $id;

        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/apply.js".$this->config->item('min_debug')."&amp;".$this->config->item('min_version')."'></script>";

        $header['onload'] = "apply.finishInit();";

        try
        {
            $company = $this->apply->getAppCompany($id);

            $body['companyName'] = $header['companyName'] = $this->companies->getCompanyName($company);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_apply', $header);
        $this->load->view('apply/finish', $body);
        $this->load->view('template/footer_apply');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function submit ($id = null)
    {

        // inserts main application
        try
        {

            $id = $this->apply->insertMainApplication($id);

            $this->apply->insertSchools($id); // edu

            $this->apply->insertJobs($id); // schools

            /*
            if (!empty($_SESSION['email']))
            {
                $subject = "Application Received";

                $msg = "<p>Thank you for submitting your application and expressing interest with [Company Name].  We will contact you if your qualifications meet our needs.</p>";

                $this->sendEmail($_SESSION['email'], null, null, $subject, $msg);
            }
             */

        }
        catch(Exception $e)
        {
            // send stack trace
            $this->functions->sendStackTrace($e);
        }

        header("Location: /apply/confirmation");
        exit;
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function confirmation ($id = null)
    {
        // session_destroy();
        $this->session->sess_destroy();
        
        $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/apply.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
    
        $header['onload'] = "apply.confirmationInit();";

        $body['id'] = $id;

        try
        {
            $company = $this->apply->getAppCompany($id);

            $body['companyName'] = $header['companyName'] = $this->companies->getCompanyName($company);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_apply', $header);
        $this->load->view('apply/confirmation', $body);
        $this->load->view('template/footer_apply');
    }
}
