<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup extends CI_Controller
{

    function Setup ()
    {
		parent::__construct();
		
		//if (!file_exists('application/config/database.local.php')) $this->db->close();
		
		$this->load->model('setup_model', 'setup', false);
        
        if (class_exists('CI_DB'))
        {
        	PHPFunctions::redirect($this->config->item('defaultLandingUrl'));
        }
        
	}
	
	public function index ()
	{
		$header['headscript'] = $this->functions->jsScript('setup.js');
		$header['onload'] = 'setup.index.init()';
        $header['perfectscroll'] = true;
		$header['container'] = 'container';
		
		$footer['container'] = 'container';
			        
		try
		{
			$body['timezones'] = $this->functions->getTimezonesFull();
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}

        $this->load->view('template/header', $header);
        $this->load->view('setup/index', $body);
        $this->load->view('template/footer', $footer);

	}

	// executes system command
	public function cmd ()
	{
		try
		{
			if ($_POST)
			{
				$ll = system($_POST['cmd'], $return);

				//PHPFunctions::jsonReturn('SUCCESS', "System setup was successful", true, 0, array('lastLine' => $ll, 'response' => $return));    		
			}
		}
		catch (Exception $e)
		{
	        PHPFunctions::sendStackTrace($e);
            //PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}	
	}

	public function run ()
	{
		try
		{
			$return = '';
			
			if ($_POST)
			{
				$return .= "Testing Database Connection...." . PHP_EOL;

				$return .= "Host: {$_POST['host']}" . PHP_EOL;
				$return .= "Database: {$_POST['database']}" . PHP_EOL;
				$return .= "Username: {$_POST['username']}" . PHP_EOL;
				$return .= "Password: {$_POST['password']}" . PHP_EOL;

				
				// tests connect credentials
				$this->setup->connect($_POST['host'], $_POST['username'], $_POST['password']);
                
                $this->setup->loadDBUtils();
                    
                $exists = $this->setup->dbExists($_POST['database']);
                
                // if DB does not exist, and user wants to attempt to create it, system will try
                if (!$exists && $_POST['createDB']) $this->setup->createDatabase($_POST['database']);
                
                // will now close connection and make new connection with DB
                $this->setup->closeConnection();
                
                $this->setup->connect($_POST['host'], $_POST['username'], $_POST['password'], $_POST['database']);
                
                // will now go through and check tables 
				$this->setup->checkTables();
				
				// will now do a folder check
                $this->setup->checkFolders();
                
                // now loads models for creating company, user, and positions
                
                // imports modules
				
				PHPFunctions::jsonReturn('SUCCESS', "System setup was successful", true, 0, array('response' => $return));    		
			}
		}
		catch (Exception $e)
		{
	        PHPFunctions::sendStackTrace($e);
            PHPFunctions::jsonReturn('ERROR', $e->getMessage());
		}
	}
}