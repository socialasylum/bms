<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announce extends CI_Controller
{
    public $bcControllerUrl = '/announce';
    public $bcControllerText = "<i class='fa fa-bullhorn'></i> Announcements";

    public $bcViewText;

    function Announce()
    {
        parent::__construct();

        $this->load->model('announce_model', 'announce', true);
        $this->load->driver('cache');

        $this->load->library('announcements');


        $this->functions->checkLoggedIn();

        try
        {
            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());
            $this->modules->checkAccess($this->router->fetch_class(), true);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            header("Location: /intranet/landing?site-error=" . urlencode("There was an error checking if you have access to that module"));
            exit;
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ()
    {

        $header['headscript'] = $this->functions->jsScript('announce.js');
        $header['onload'] = "announce.indexInit();";
        $header['datatables'] = true;


        try
        {
            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());
            $body['announcements'] = $this->announce->getAnnouncements();

            $body['editPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 1); // checks if user has ability to edit and create announcements
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('announce/index', $body);
        $this->load->view('template/footer_intranet');

    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {

        $header['headscript'] = $this->functions->jsScript('announce.js');
        $header['onload'] = "announce.editInit();";
        $header['ckeditor'] = true;

        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Announcement";

        $body['id'] = $id;

        try
        {
            $body['sendToTypes'] = $this->functions->getCodes(19);

            $body['status'] = array('Deactived', 'Active');

            $body['positions'] = $this->positions->getPositions();

            if (!empty($id))
            {
                $body['info'] = $info = $this->announce->getAnnouncementInfo($id);

                $body['posAccess'] = $this->announce->getPositionAccess($id);
                $body['locAccess'] = $this->announce->getLocationAccess($id);
                $body['terAccess'] = $this->announce->getTerritoryAccess($id);

                if ($info->sendToType == 2) $stbody['territories'] = $this->territory->getTerritories($this->session->userdata('company'));
                if ($info->sendToType == 3) $stbody['locations'] = $this->locations->getLocations($this->session->userdata('company'));

                $body['sendToHTML'] = $this->load->view('announce/sendtotype', $stbody, true);
            }

            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('announce/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $type 
     *
     * @return TODO
     */
    public function sendtotype ($type)
    {
        try
        {
            if ($type == 2) $body['territories'] = $this->territory->getTerritories($this->session->userdata('company'));
            if ($type == 3) $body['locations'] = $this->locations->getLocations($this->session->userdata('company'));
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('announce/sendtotype', $body);

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['id']))
                {
                    $_POST['id'] = $this->announce->insertAnnouncement($_POST);

                    $status = "Announcement has been sent!";
                }
                else
                {
                    $this->announce->updateAnnouncement($_POST);

                    // clear position, territory, and location access
                    $this->announce->clearPositionAccess($_POST['id']);
                    $this->announce->clearTerritoryAccess($_POST['id']);
                    $this->announce->clearLocationAccess($_POST['id']);

                    $status = "Announcement has been updated!";
                }

                // save position access
                $this->announce->savePositionAccess($_POST['id'], $_POST['position']);

                // send to set to territories
                if ($_POST['sendToType'] == 2)
                {
                    $this->announce->saveTerritoryAccess($_POST['id'], $_POST['territory']);
                }

                // send to set to locations
                if ($_POST['sendToType'] == 3)
                {
                    $this->announce->saveLocationAccess($_POST['id'], $_POST['location']);
                }

                $this->functions->jsonReturn('SUCCESS', $status, $_POST['id']);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function view ($id)
    {
        $header['headscript'] = $this->functions->jsScript('announce.js');
        $header['onload'] = "announce.viewInit();";

        try
        {
            $body['info'] = $info = $this->announce->getAnnouncementInfo($id);

            // if start date is specifed, uses that, if not goes by datestamp
            $annDate = (empty($info->startDate)) ? $info->datestamp : $info->startDate;

            $body['date'] = $this->users->convertTimezone($this->session->userdata('userid'), $annDate, "m/d/Y");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('announce/view', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function widget ()
    {

        $body['widget'] = true;

        try
        {
            $body['modId'] = $this->modules->getModIDFromController($this->router->fetch_class());
            $body['announcements'] = $this->announce->getAnnouncements();

            $body['editPerm'] = $this->modules->checkPermission($this->router->fetch_class(), 1); // checks if user has ability to edit and create announcements
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('announce/index', $body);
    }
}
