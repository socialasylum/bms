<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kb extends CI_Controller
{
    public $bcControllerUrl = '/kb';
    public $bcControllerText = "<i class='fa fa-question-circle'></i> Knowledge Base";

    public $bcViewText;

    function Kb()
    {
        parent::__construct();

        $this->load->model('kb_model', 'kb', true);

        $this->load->driver('cache');

        $this->functions->checkLoggedIn();

    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index ($folder = 0, $rootFolder = 0)
    {
        $header['headscript'] = $this->functions->jsScript('kb.js');
        $header['headscript'] .= $this->functions->jsScript('folders.js');
        
        $header['ckeditor'] = true;

        // $header['datatables'] = true;
        $header['onload'] = "kb.indexInit();";

        $body['admin'] = true;
        $header['rightlick'] = true;

        $body['folder'] = $folder;
        $body['rootFolder'] = $rootFolder;

        try
        {
            $body['content'] = $this->kb->getFolderContent($folder);
            $body['breadCrumbs'] = $this->folders->getBreadCrumbs($folder);
            $body['parentFolder'] = $this->folders->getTableValue('parentFolder', $folder);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('kb/index', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id Optional, defaults to 0. 
     *
     * @return TODO
     */
    public function edit ($id = 0)
    {
        $this->bcViewText = "<i class='fa fa-edit'></i> " . ((empty($id)) ? 'Create' : 'Edit') . " Article";

        $this->load->model('module_model', 'module', true);

        $header['headscript'] = $this->functions->jsScript('kb.js');
        $header['onload'] = "kb.editInit();";
        $header['ckeditor'] = true;


        $body['id'] = $id;

        $body['folder'] = (empty($_GET['folder'])) ? 0 : $_GET['folder'];


        try
        {
            $body['status'] = array('Deactived', 'Active');
            $body['modules'] = $this->module->getModules();

            if (!empty($id))
            {
                $body['info'] = $this->kb->getArticleData($id);
                $body['sections'] = $this->kb->getSectionData($id);
            
                $header['onload'] .= "kb.sectionCnt=" . (count($body['sections'])) . ';';
            }

        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }


        $this->load->view('template/header_intranet', $header);
        $this->load->view('kb/edit', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function save ()
    {
        if ($_POST)
        {
            try
            {
                // if company is assigned a module, sets company to 0 so all companies have access to it
                if (empty($_POST['module'])) $_POST['company'] = $this->session->userdata('company');
                else $_POST['company'] = 0;

                if (empty($_POST['id']))
                {
                    // insert article
                    $_POST['id'] = $this->kb->insertKbArticle($_POST);

                    $this->kb->assignArticleToFolder($_POST['id'], $_POST['folder']);


                    $msg = "Knowledge Base Article has been created!";
                }
                else
                {
                    // update article
                    $this->kb->updateKbArticle($_POST);

                    $msg = "Knowledge Base Article has been updated!";
                }


                // saves each section
                $this->kb->saveSections($_POST);
                
                $this->functions->jsonReturn('SUCCESS', $msg, $_POST['id']);
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function sectionprop ($sectionCnt)
    {
        try
        {
            $body['sectionCnt'] = $sectionCnt;

            $body['heading'] = $_POST['sectionHeading'][$sectionCnt];

        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }



        $this->load->view('kb/sectionprop', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function rightclick ($folder = 0)
    {
        $body['folder'] = $folder;

        $this->load->view('kb/rightclick', $body);
    }

    /**
     * TODO: short description.
     *
     * @param mixed $id 
     *
     * @return TODO
     */
    public function view ($id)
    {
        // module ID was passed
        if (isset($_GET['module']))
        {
            $module = urldecode($_GET['module']);

            // get article ID based upon module ID passed
            $id = $this->kb->getArticleIDFromModuleID($module);

            if (empty($id))
            {

                try
                {
                    $moduleName = $this->modules->getTableValue('name', $module);
                }
                catch(Exception $e)
                {
                    PHPFunctions::sendStackTrace($e);
                }

                header("Location: /kb?site-alert=" . urlencode("Unable to find knowledge base article based for " . ((empty($moduleName)) ? 'that' : "the {$moduleName}") . " module."));
                exit;
            }
        }

        if (empty($id))
        {
            header("Location: /kb?site-error=" . urlencode("Article ID is empty!"));
            exit;
        }

        try
        {
            $body['info'] = $info = $this->kb->getArticleData($id);
            $body['sections'] = $this->kb->getSectionData($id);

            // gets icon for module
            if (!empty($info->module))
            {
                $icon = $this->modules->getTableValue('icon', $info->module);

                if (!empty($icon))
                {
                    $body['headerIcon'] = "<i class='{$icon}'></i> ";
                }
            }

        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('kb/view', $body);
        $this->load->view('template/footer_intranet');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function deletekbarticle ()
    {
        if ($_POST)
        {
            try
            {
                $this->kb->deleteArticle($_POST['article']);
                $this->functions->jsonReturn('SUCCESS', "Knowledge base article has been deleted!");
            }
            catch(Exception $e)
            {
                PHPFunctions::sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
