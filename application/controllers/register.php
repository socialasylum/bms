<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller
{

    function Register()
    {

        parent::__construct();

        $this->load->driver('cache');
        $this->load->model('register_model', 'register', true);

        // loads user model
        $this->load->model('user_model', 'user', true);
        
        $this->load->library('Payment');
    }

    public function index ($company)
    {
        $header['headscript'] = $this->functions->jsScript('register.js');
        $header['onload'] = 'reg.indexInit();';


        if (isset($_GET['type'])) $body['type'] = intval($_GET['type']);

        if($this->config->item('live') == true)
        {
            if($_SERVER['HTTP_HOST'] == 'promoteleads.cgisolution.com') $company = 6;
        }

        try
        {
            $body['company'] = $company;
            $body['logo'] = $this->companies->getCompanyLogo($company);
            $body['companyName'] = $this->companies->getCompanyName($company);
            $body['states'] = $this->functions->getStates();
            $body['industry'] = $this->functions->getCodes(17);
            // $body['openPositions'] = $this->apply->getOpenPositions($company);

            $body['locations'] = $this->locations->getLocations($company);

            $body['becomeMemberTxt'] = $this->companies->getTableValue('becomeMemberTxt', $company);

            $body['allowFBreg'] = $this->companies->getTableValue('allowFBreg', $company);

            $body['utm_campaign'] = urldecode($_GET['utm_campaign']);

            $body['defaultPosition'] = $this->positions->getDefaultPosition($company);
            $body['defaultDepartment'] = $this->departments->getDefaultDepartment($company);

            $body['showBilling'] == true;
            if ($company == 35 && (int) $_GET['type'] == 1) $body['showBilling'] = false;


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        $this->load->view('template/header', $header);
        $this->load->view('register/index', $body);
        $this->load->view('template/footer');
    }

    /**
     * ajax page to register user
     *
     */
    public function reguser ()
    {

        try
        {
            if ($_POST)
            {

                if (empty($_POST['company'])) throw new Exception("Company was not specified");

                // check if email is available
                $emailAvailable = $this->users->checkEmailAvailable($_POST['email']);

                if ($emailAvailable == false) $this->functions->jsonReturn('ALERT', 'That e-mail address is already in use!');

                // will now create user
                $userid = $this->user->createUser($_POST);

                $_POST['customerID'] = $userid;

                if ($_POST['company'] == 35) // Price Tracker
                {
                    $_POST['customerID'] = "user-{$userid}";
                }

                // creates BT customer ID
                $BTcustomerID = $_POST['BTcustomerID'] = $this->payment->createCustomer($_POST, false);

                // something went wrong creating customer in BT
                // if (empty($BTcustomerID)) $this->functions->jsonReturn('Alert', 'Billing Customer not created!');

                // saves users braintree customerID in users table
                $this->users->updateBTCustomerID ($BTcustomerID, $userid);

                // sets home company
                $uc = $this->user->insertUserCompany($userid, $_POST['company'], true);

                // sets company position
                $cp = $this->user->insertCompanyPosition($userid, $_POST['company'], $_POST['position']);

                // sets company department
                $ud = $this->user->insertCompanyDepartment($userid, $_POST['company'], $_POST['department']);

                // sets user to a Training status until approved or training is completed
                
                if ($_POST['company'] == 35) $status = 1; // Price tracker users are auto active
                else $status = 3;

                $this->user->updateStatus($userid, $status);

                // sets email message for Promote Leads
                if($_POST['company'] == 6)
                {

                    // gets user name
                    $userName = $this->users->getName($userid);

                    // gets requesting location name
                    $location = $this->locations->getTableValue('name', $_POST['location']);

                    // gets industry name
                    if (!empty($_POST['industry'])) $industry = $this->functions->codeDisplay(17, $_POST['industry']);
                    else $industry = $_POST['industryOther'];

                    // sets email subject for new registration
                    $subject = "New Registration Request!";


                    $message = "{$userName} has requested to join <strong>{$location}</strong> under the category of <strong>{$industry}</strong>. Please <a href='https://" . $_SERVER['HTTP_HOST'] . "/register/confirmReg/{$userid}/{$_POST['location']}/{$_POST['industry']}'>Click Here</a> to either approve or deny this request. If approved, they will recieve an email taking them to where they can enter billing information. If denied, you will be asked to provide a reason that will be forwarded to the registrant.";

                    // sets who email is sent to
                    $to = "george@promoteleads.com";

                    // sets who email will be sent from
                    // $from = "noreply@cgisolution.com";

                    // sends email about new registrant
                    $this->functions->sendEmail($subject, $message, $to);
                }

                // Price Tracker needs to handle creating a subscription for the user
                if ($_POST['company'] == 35 && (int) $_POST['type'] == 2)
                {
                    // will create credit card info with briantree
                    $token = $this->payment->createCreditCard($_POST, false, true);

                    // save token
                    $this->users->saveCCToken($userid, $_POST['ccNumber'], $token);

                
                    $planID = 'priceTrackerMonthly';

                    if ($_POST['utm_campaign'] == 'facebookft') $planID = 'PTmonthlyFT';

                    // will now attempt to create subscription
                    $subID = $this->payment->createSubscription($_POST['company'], $token, $planID, 0, false, true);

                    $this->users->updateSubscriptionInfo($userid, $subID, 9.99);

                    // gets user name
                    $userName = $this->users->getName($userid);

                    $subject = "New User: {$userName}";

                    $msg = "<h1>New User</h1><p>A new user has signed up to ProductPriceTracker.com: {$userName}.</p>";

                    $emailTo = array('williamgallios@gmail.com', 'brandonvinall@gmail.com');

                    $this->functions->sendEmail($subject, $msg, $emailTo, 'noreply@productpricetracker.com', "ProductPriceTracker.com");
                }

                if ($_POST['company'] == 35)
                {
                    // insert accountType

                    $this->register->insertAccountType ($userid, $_POST['company'], $_POST['accountType']);

                    //send welcome e-mail
                }

                // has die command in it...
                $this->functions->jsonReturn('SUCCESS', 'User has been created!', $userid);
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function fbreg ()
    {
        if ($_POST)
        {
            try
            {
                if (empty($_POST['company'])) throw new Exception("Company was not specified");

                // check if email is available
                $emailAvailable = $this->users->checkEmailAvailable($_POST['data']['email']);

                if ($emailAvailable == false) $this->functions->jsonReturn('ALERT', 'That e-mail address is already registered to another account!');

                // checks if the facebook account is already linked 
                $fbAlreadyLinked = $this->users->checkFacebookIDLinked($_POST['data']['id']);

                if ($fbAlreadyLinked == true) $this->functions->jsonReturn('ALERT', "This facebook account is already linked to a user account!");

                $data = array
                    (
                        'firstName' => $_POST['data']['first_name'],
                        'lastName' => $_POST['data']['last_name'],
                        'email' => $_POST['data']['email'],
                        'password' => uniqid(),
                        'facebookID' => $_POST['data']['id']
                    );

                // will now create user
                $userid = $this->user->createUser($data);

                $_POST['customerID'] = $userid;

                if ($_POST['company'] == 35) // Price Tracker
                {
                    $_POST['customerID'] = "user-{$userid}";
                }


                // array for creating customer
                $data = array
                    (
                        'customerID' => $_POST['customerID'],
                        'firstName' => $_POST['data']['first_name'],
                        'lastName' => $_POST['data']['last_name'],
                        'email' => $_POST['data']['email']
                    );

                // creates BT customer ID
                $BTcustomerID = $_POST['BTcustomerID'] = $this->payment->createCustomer($data, false);

                // saves users braintree customerID in users table
                $this->users->updateBTCustomerID ($BTcustomerID, $userid);

                // sets home company
                $uc = $this->user->insertUserCompany($userid, $_POST['company'], true);

                // sets company position
                $cp = $this->user->insertCompanyPosition($userid, $_POST['company'], $_POST['position']);

                // sets company department
                $ud = $this->user->insertCompanyDepartment($userid, $_POST['company'], $_POST['department']);

                // sets user to a Training status until approved or training is completed
                
                if ($_POST['company'] == 35) $status = 1; // Price tracker users are auto active
                else $status = 3;

                $this->user->updateStatus($userid, $status);

                $this->functions->jsonReturn('SUCCESS', 'User has been created! Next please confirm your billing information.', $userid);

            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function finish ($company)
    {
        $header['headscript'] = $this->functions->jsScript('register.js');
        // $header['onload'] = 'reg.indexInit();';

        $body['company'] = $company;

        try
        {
            $body['companyName'] = $this->companies->getCompanyName($company);

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('register/finish', $body);
        $this->load->view('template/footer');
    }


    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function completereg ()
    {
        try
        {
            if ($_POST)
            {
                if ($_FILES)
                {

                }
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
    }

    public function confirmReg($userid, $location, $industry)
    {
        $this->functions->checkLoggedIn();

        try
        {
            $header['headscript'] = "<script type='text/javascript' src='/min/?f=public/js/register.js{$this->config->item('min_debug')}&amp;{$this->config->item('min_version')}'></script>\n";
            $header['onload'] = "reg.confirmRegInit();";

            $body['userid'] = $userid;
            $body['userName'] = $this->users->getName($userid);
            $body['location'] = $this->locations->getTableValue('name', $location);
            $body['industry'] = $this->functions->codeDisplay(17, $industry);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('register/confirmReg', $body);
        $this->load->view('template/footer_intranet');
    }

    public function approveReg($userid, $status)
    {
        try
        {
            // gets user name
            $userName = $this->users->getName($userid);

            // sets subject of email
            $subject = "You have been approved!";

            // set message of email
            $message = "<p>Hello {$userName}, we have reviewed your application, and we are pleased to inform you that an account has been approved with your requested details!</p>
                        <p>If you have any questions about your account, or if we can be of further service to you, please call us at our customer service number, 888-444-9350, and someone will be more than happy to assist you. When you call, please have either your UserID or email addres you used to register. (This email address)</p>
                        <p>Please <a href='https://" . $_SERVER['HTTP_HOST'] . "/register/payment/{$userid}/{$status}'>Click Here</a> to go to your payment page. Once you have paid, your account will be live!</p>
                        <br>
                        <p>Sincerely,</p>
                        <br><br><br>
                        <p><strong>CGI Solution LLC</strong></p>
                        <p>(888) 444-9350</p>";

            // set who email goes to
            $to = $this->users->getTableValue('email', $userid);

            // sends email notifying them they were approved
            $this->functions->sendEmail($subject, $message, $to);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        header('Location: /intranet/landing');
    }

    public function denyReg($userid, $status)
    {
        try
        {
            $header['headscript'] = $this->functions->jsScript('register.js');
            $header['onload'] = "reg.denyRegInit();";

            $body['userName'] = $this->users->getName($userid);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet');
        $this->load->view('register/denyReg', $body);
        $this->load->view('template/footer_intranet');

    }

    public function payment ($userid)
    {
        $this->functions->checkLoggedIn();

        $this->load->model('user_model', 'user', true);
        try
        {
            // loads javascript
            $header['headscript'] = $this->functions->jsScript('register.js');
            $header['onload'] = "reg.paymentInit();";

            $body['userName'] = $this->users->getName($userid);
            $body['userid'] = $userid;
            $body['plans'] = $this->payment->getPlans();
            
            $body['cards'] = $this->user->getOnfileCards($userid);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header_intranet', $header);
        $this->load->view('register/payment', $body);
        $this->load->view('template/footer_intranet');
    }

    public function saveBilling()
    {
         if ($_POST)
        {
            try
            {
                // loads user model
                $this->load->model('user_model', 'user', true);

                $_POST['BTcustomerID'] = $_POST['userid'];

                if (!empty($_POST['ccNumber']))
                {
                    // creates CC info in BT and returns token
                    $token = $this->payment->createCreditCard($_POST, false);

                    if (empty($token)) $this->functions->jsonReturn('ALERT', 'Unable to save credit card with Braintree');

                    // will now save token
                    $this->users->saveCCToken($_POST['userid'], $_POST['ccNumber'], $token);

                    // $price = 0;
                }
                else
                {
                    $token = $this->users->getCardTokenByID($_POST['previousCard']);

                    // if ($_POST['packages'] == '001') $price = 37;
                    // else $price = 0;

                    error_log('****PRICE: ' . $price);
                }


                error_log('****token:' . $token);

                // something went wrong saving CC 
                if (empty($token)) $this->functions->jsonReturn('Alert', 'Oh nooooooo! Something went wrong saving your billing information!!');

                // creates subscription
                $subscription = $this->payment->createSubscription($_POST['userid'], $token, $_POST['packages'], 0, false, true);

                // something went wrong saving subscription to BrainTree
                if (empty($subscription)) $this->functions->jsonReturn('Alert', 'Unable to create subscription! Card was not charged!');

                // saves subscription ID
                $this->users->updateSubscriptionInfo($_POST['userid'], $subscription);

                // updates user to Active Status
                $this->user->updateStatus($_POST['userid'], 1);

                $subject = "Account now active!!";

                // $message = "Account is now active. Please log in at <a href='http://" . $_SERVER['HTTP_HOST'] . "/intranet/login'>CGI Solution</a>";

                $message = $this->load->view('welcome/invoice', $body, true);

                $to = $this->users->getTableValue('email', $_POST['userid']);

                // sends email to Company
                $this->functions->sendEmail($subject, $message, $to);

                $this->functions->jsonReturn('SUCCESS', 'Your account is now Active!');

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
}
