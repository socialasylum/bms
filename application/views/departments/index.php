<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-sitemap'></i> Departments</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='/department/edit'><i class='fa fa-plus'></i> New Department</a></li>
        </ul>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>



<?php
if (empty($departments)) :
    echo $this->alerts->info('No departments have been created!');
else :
?>
    <table class='table table-striped table-condensed' id='departments'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Default Department</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach ($departments as $r)
    {

        $status = ($r->active == 1) ? 'Active' : 'Deactived';
        $default = ($r->default == 1) ? "<i class='fa fa-check'></i>" : null;

        echo "<tr>\n";


        echo "\t<td>{$r->name}</td>\n";
        echo "\t<td>{$status}</td>\n";
        echo "\t<td>{$default}</td>\n";

        echo "<td><a href='/department/edit/{$r->id}' class='btn btn-info btn-xs pull-right'><i class='fa fa-pencil'></i></a></td>";

        echo "</tr>\n";
    }
?>
        </tbody>
    </table>


<?php endif; ?>

