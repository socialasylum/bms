<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Department</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>
<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);

if (!empty($id))
{
    echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
}
?>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='name'>Name</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control'  name='name' id='name' value="<?=$info->name?>" placeholder='Human Resources'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='active'>Status</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
<select name="active" id="active" class='form-control'>
    <option value=""></option>
<?php
foreach ($status as $k => $v)
{
    if (!empty($id)) $sel = ($info->active == $k) ? "selected='selected'" : null;
    else $sel = ($k == 1) ? "selected='selected'" : null;

    echo "<option value='{$k}' {$sel}>{$v}</option>" . PHP_EOL;
}
?>
</select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='default'>Default?</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">

          <input type='hidden' name='default' value='0'>
	                
			<div class='checkbox'>
	            <label>
	                <?php if ($info->default == 1) $defaultChecked = "checked='checked'"; ?>
	                <input type='checkbox' name='default' id='default' value='1' <?=$defaultChecked?>> Default Department
	            </label>
				</div>

                <span class='help-block'>This department will be assigned when a user registers.</span>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->



</form>
