<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<div class='module'>
    <div class='module-head'><h3>Create Department</h3></div>
    <div class='module-body'>

        <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


        <p class='lead'>Here you can create new departments for any companies you are assigned to.</p>

        <form name='departmentForm' id='departmentForm' class='form-horizontal'>


            <div class="control-group">
            <label class="control-label" for='company'>Company</label>
                <div class="controls">
                    <select name='company' id='company'>
                        <option value=''></option>
                    <?php
                    if (!empty($companies))
                    {
                        foreach ($companies as $r)
                        {
                            echo "<option value='{$r->id}'>{$r->companyName}</option>\n";
                        }
                    }
                    ?>
                    </select>
                </div>
            </div>


            <div class="control-group">
            <label class="control-label" for='name'>Name</label>
                <div class="controls">
                    <input type='text' name='name' id='name' placeholder='Logistics'>
                </div>
            </div>


        </form>

        <div class='form-actions'>
            <button type='button' class='btn btn-primary' id='createBtn'>Create</button>
            <button type='button' class='btn' id='cancelBtn'>Cancel</button>
        </div>

    </div> <!-- .module-body -->

</div> <!-- .module -->
