<?php if(!defined('BASEPATH')) die('Direct access not allowed');

$a[$nav] = 'active';

?>
<div class='sidebar'>
    <ul class='widget widget-menu unstyled'>
        <li class='<?=$a['index']?>'><a href='/department'><i class='menu-icon icon-dashboard'></i>All Departments</a></li>
        <li class='<?=$a['create']?>'><a href='/department/create'><i class='menu-icon icon-bar-chart'></i>Create Department</a></li>

    </ul>
</div>


