<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Contact</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">

            <?php if (!empty($id)) : ?>
            <li><a href='javascript:void(0);' id='deleteBtn'><i class='fa fa-trash-o danger'></i> Delete</a></li>
            <?php endif; ?>

            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>



<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm'
        );

echo form_open('#', $attr);

if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>";
?>

<input type='hidden' name='edited' id='edited' value='0'>


<div class='form-horizontal'>

<div class='row'>
    <div class='col-lg-6'>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyName'>Company Name</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='companyName' id='companyName' value="<?=$info->companyName?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 control-label' for='name'>Name</label>
            <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>" placeholder=''>
            </div>
        </div>

        <div class="form-group">
            <label class='col-lg-3 control-label' for='type'>Type</label>
            <div class="col-lg-9 controls">
                <select class='form-control' name="type" id="type">
                    <option value=""></option>
<?php
    if (!empty($types))
    {
        foreach ($types as $r)
        {
            $sel = ($r->code == $info->type) ? 'selected' : null;

            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . EL;
        }
    }
?>
                </select>
            </div>
        </div>



    </div> <!-- .col-lg-6 -->

    <div class='col-lg-6'>
        <div class="form-group">
            <label class='col-lg-3 control-label' for='title'>Title</label>
            <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='title' id='title' value="<?=$info->title?>" placeholder=''>
            </div>
        </div>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='websiteUrl'>Website</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' maxlenght='300' name='websiteUrl' id='websiteUrl' value="<?=$info->websiteUrl?>" placeholder="http://bms.cgisolution.com">
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 control-label' for='status'>Status</label>
            <div class="col-lg-9 controls">
                <select class='form-control' name="status" id="status">
                    <option value=""></option>
                    <!-- options -->
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='industry'>Industry</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="industry" id="industry" class='form-control'>
                    <option value=""></option>
            <?php
            if (!empty($industry))
            {
                foreach ($industry as $r)
                {
                    $sel = ($r->code == $info->industry) ? "selected='selected'" : null;

                    echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
                }

                if (!empty($info->otherIndustry)) $otherSel = "selected='selected'";

            }
            ?>
                <option <?=$otherSel?> value='0'>Other</option>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group" <?php if (empty($info->otherIndustry)) echo "style='display:none;'"; ?> id='otherIndustryFormGroup' >
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='otherIn'>Other Industry</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='otherIndustry' id='otherIndustry' value="<?=$info->otherIndustry?>">
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

    </div> <!-- .col-lg-lg-6 -->
</div> <!-- .row -->

<hr>
<h2><i class='fa fa-building'></i> Addresses</h2>



<ul id='addSortList' class='formSort'>

<?php

    $cnt = count($addresses);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++)
    {

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        if ($i == 0) echo "<div id='master-address'>";

        echo <<< EOS
<li>
<div class='panel panel-default'>
    <div class='panel-heading'>Address <button type='button' onclick="crm.deleteAddress(this);" class=' pull-right deleteLink' {$disabled} ><i class='fa fa-trash-o'></i></button></div>

    <div class='panel-body'>

       <div class='row'>
        <div class='col-lg-12'>
         <div class="form-group">
             <label class='col-lg-2 control-label' for='description'>Description</label>
             <div class="col-lg-10 controls">
                 <input type='text' class='form-control' name='address[description][]' id='address_description' value="{$addresses[$i]->description}">
             </div>
         </div>

            <div class="form-group">
                <label class='col-lg-2 control-label' for='notes'>Notes</label>
                <div class="col-lg-10 controls">
                    <textarea name='address[notes][]' id='address_notes' class='form-control' rows='4'>{$addresses[$i]->notes}</textarea>
                </div>
            </div>

        </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->


        <div class='row'>
            <div class='col-lg-6'>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='address'>Address</label>
        <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='address[addressLine1][]' id='address_line_1' value="{$addresses[$i]->address}" placeholder=''>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='address2'>Address 2</label>
        <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='address[addressLine2][]' id='address_line_2' value="{$addresses[$i]->address2}" placeholder=''>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='address3'>Address 3</label>
        <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='address[addressLine3][]' id='address_line_3' value="{$addresses[$i]->address3}" placeholder=''>
        </div>
    </div>


            </div>
            <div class='col-lg-6'>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='city'>City</label>
        <div class="col-lg-9 controls">
            <input type='text' class='form-control' name='address[city][]' id='city' value="{$addresses[$i]->city}" placeholder='Las Vegas'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='state'>State</label>
        <div class="col-lg-9 controls">
        <select class='form-control' name="address[state][]" id="state_1">
            <option value=""></option>
EOS;

    if (!empty($states))
    {
        foreach ($states as $abb => $name)
        {
            $sel = ($abb == $addresses[$i]->state) ? 'selected' : null;
            echo "<option {$sel} value='{$abb}'>{$name}</option>" . PHP_EOL;
        }
    }

echo <<< EOS
        </select>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='postalCode'>Postal Code</label>
        <div class="col-lg-9 controls">
            <input type='text' class='form-control' name='address[postalCode][]' id='postalCode' value="{$addresses[$i]->postalCode}" placeholder='89101'>
        </div>
    </div>


            </div>


        </div> <!-- .row -->

        </div> <!-- .panel-body -->
    </div> <!-- .panel -->
    </li>
EOS;


        if ($i == 0) echo "</div>";
    }
?>

    <!-- div that holds dynamically added addresses -->
    <!-- <div id='address-container'></div> -->
    </ul>

    <div class='row'>
        <div class='col-lg-12'>
            <button type='button' class='btn btn-info' name='addAddressBtn' id='addAddressBtn'><i class='fa fa-plus'></i> Add Address</button>
        </div>
    </div>

<hr>

<h2><i class='fa fa-phone'></i> Phone &amp; Fax</h2>


    <p class='lead'>Here you can add various phone numbers with this contact.</p>


<ul id='phoneSortList' class='formSort'>
<?php

    $cnt = count($phones);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++) 
    {

        $phoneNumber = $phones[$i]->phoneNumber;
        $type = $phones[$i]->type;

        // if ($i == 0) echo "<div id='master-phone'>";

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        echo <<< EOS
        <li>
        <div class='row' style="margin-bottom:15px;">
            <div class='col-lg-3'>
                <select class='form-control' name="phone[type][]" id="phone_type_1">
                    <option value=""></option>
EOS;

    if (!empty($phoneTypes))
    {
        foreach ($phoneTypes as $r)
        {
            $sel = ($r->code == $type) ? 'selected' : null;

            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . EL;
        }
    }

echo <<< EOS
                </select>
            </div> <!-- .col-lg-3 -->

            <div class='col-lg-9'>

                <div class='input-group'>
					<span class="input-group-addon"><i class='fa fa-arrows-v'></i></span>
                <input type='text' class='form-control' name='phone[number][]' id='phone_number_1' value="{$phoneNumber}" placeholder='888-444-9350'>
                    <span class='input-group-btn'>
                        <button type='button' class='btn btn-danger' id='delBtn' {$disabled} onclick="crm.deletePhone(this);"><i class='fa fa-trash-o'></i></button>
                    </span>
                </div> <!-- .input-group -->
            </div> <!-- col-lg-9 -->

        </div> <!-- .row -->
        </li>
EOS;

        // if ($i == 0) echo "</div>";

    }

?>
    </ul>

    <!-- div that holds dynamically added phone -->
    <!-- <div id='phone-container'></div> -->


    <div class='row'>
        <div class='col-lg-12'>
            <button type='button' class='btn btn-info' name='addPhoneBtn' id='addPhoneBtn'><i class='fa fa-plus'></i> Add Number</button>
        </div>
    </div>



<hr>

<h2><i class='fa fa-envelope'></i> E-mail Addresses</h2>

<p class='lead'>Below is a list of all e-mail addresses associated with this contact</p>

<div class='row form-horizontal'>
    <div class='col-lg-12'>

<ul id='emailSortList' class='formSort'>

<?php

    // $cnt = (empty($id)) ? 1 : count($emails);

    $cnt = count($emails);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++) 
    {

        $email = $emails[$i]->email;

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        echo <<< EOS

        <li>
        <div class="form-group">
            <label class='col-lg-3 control-label' for='email'>E-mail</label>
            <div class="col-lg-9 controls">
                <div class='input-group'>
                <input type='text' class='form-control' name='email[]' id='email' value="{$email}" placeholder=''>
                    <span class='input-group-btn'>
                        <button type='button' class='btn btn-danger' id='delBtn' onclick="crm.deleteEmail(this);" {$disabled} ><i class='fa fa-trash-o'></i></button>
                    </span>
                </div> <!-- .input-group -->
            </div> <!-- col-lg-9 -->
        </div> <!-- .form-group -->
        </li>
EOS;

    }

?>

</ul>

        </div> <!-- .col-lg-9 -->


    <div class='col-lg-12'>
        <button type='button' class='btn btn-info' id='addEmailBtn'><i class='fa fa-plus'></i> Add E-mail</button>
    </div> <!-- .col-lg-9 .col-lg-offset-2 -->


    </div> <!-- .row -->
</div>




<hr>
<h2><i class='fa fa-comments'></i> Correspondence</h2>

    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabNotes' data-toggle="tab"><i class='fa fa-file'></i> Notes</a></li>
        <li><a href='#tabCalls' data-toggle="tab"><i class='fa fa-phone'></i> Phone Calls</a></li>
        <li><a href='#tabTasks' data-toggle="tab"><i class='fa fa-tasks'></i> Tasks</a></li>
        <li><a href='#tabAttach' data-toggle="tab"><i class='fa fa-paperclip'></i> Attachments</a></li>
        <li><a href='#tabOrders' data-toggle="tab"><i class='fa fa-clipboard'></i> Orders</a></li>
    </ul>

<div class='tab-content crm-notes'>
    <div class='tab-pane form-horizontal active' id='tabNotes'>
<?php
if (empty($notes))
{
	echo "<alert data-type='info' data-msg='This contact has no notes.' data-options='{\"clearTimeoutSeconds\":\"0\"}'></alert>" . PHP_EOL;
}
else
{
    foreach ($notes as $r)
    {
        try
        {
            $name = $this->users->getName($r->userid);

            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "Y-m-d g:i A");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips note if error pulling data
        }

        echo "<div class='panel panel-default'>";
        echo "<div class='panel-heading inline'><strong>{$name}</strong> <span class='pull-right'>{$date}</span></div>";

        echo "<div class='panel-body'>" . PHP_EOL;

        echo "<dl class='dl-horizontal'>";

        echo "<dt><img src='/user/profileimg/100/{$r->userid}' class='blog-preview-img img-polaroid'></dt>";

        echo "<dd>";
        echo "<blockquote>{$r->note}</blockquote>";
        echo "</dd>";

        echo "</dl>";

        echo "<div class='clearfix'></div>";

        echo "</div>"; // .panel-body
        echo "</div>"; // . panel

    }


}
?>

    <hr>

    <h3><i class='fa fa-pencil'></i> Leave a note</h3>
        <textarea name='contactNote' id='contactNote' class='form-control' rows='5'></textarea>
    </div> <!-- #tabNotes -->
    <div class='tab-pane form-horizontal' id='tabCalls'>
<?php
if (empty($calls))
{
	echo "<alert data-type='info' data-msg='This contact has no calls.' data-options='{\"clearTimeoutSeconds\":\"0\"}'></alert>" . PHP_EOL;
}
else
{

}
?>

<hr>
    <h3><i class='fa fa-phone'></i> Schedule a phone call</h3>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='callDate'>Date</label>
        <div class="col-lg-10 controls">
            <input type='text' name='callDate' id='callDate' class='form-control'>
    </div>
    </div>


    <div class="form-group">
        <label class='col-lg-2 control-label' for='callTime'>Time</label>
        <div class="col-lg-10 controls">
            <select name="callHour" id="callHour" class='form-control time-select'>
                <!-- <option value=""></option> -->
<?php
$cnt = 1;
while($cnt <= 12)
{
    #$sel = ($startHour == $cnt) ? 'selected' : null;
    if(!empty($date))
    {
        $sel = ($st[0] == $cnt) ? 'selected' : null;
    }
    echo "<option {$sel} value='{$cnt}'>{$cnt}</option>\n";
$cnt++;
}
?>
          </select>

            <select name="callMin" id="callMin" class='form-control time-select'>
                <!-- <option value=""></option> -->
<?php
$cnt = 0;
while($cnt <= 59)
{
    $display = ($cnt < 10) ? "0{$cnt}" : $cnt;
    
    #if(!empty($date)) $sel = ($st[1] == $cnt) ? 'selected' : null;
    
    echo "<option {$sel} value='{$display}'>{$display}</option>\n";
    $cnt++;
}
?>
            </select>


            <select name="callAP" id="callAP" class='form-control time-select'>
<?php
foreach($ampm as $ap)
{
    #if(!empty($date)) $sel = ($st[2] == $ap) ? 'selected' : null;
    echo "<option {$sel} value='{$ap}'>{$ap}</option>\n";
}
?>
            </select>

            <!--
            <select name="callAP" id="callAP" class='form-control time-select'>
            </select>
            //-->
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-2 control-label' for='callType'>Type</label>
        <div class="col-lg-10 controls">
            <select name='callType' id='callType' class='form-control'>
                <option value=""></option>
                <!-- options -->
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='callStatus'>Status</label>
        <div class="col-lg-10 controls">
            <select name='callStatus' id='callStatus' class='form-control'>
                <option value=""></option>
                <!-- options -->
            </select>
        </div>
    </div>



    <div class="form-group">
        <label class='col-lg-2 control-label' for='callNote'>Notes</label>
        <div class="col-lg-10 controls">
            <textarea name='callNote' id='callNote' class='form-control' rows='5'></textarea>
        </div>
    </div>



    </div> <!-- #tabCalls -->

    <div class='tab-pane' id='tabTasks'>
<?php
if (empty($tasks))
{
	echo "<alert data-type='info' data-msg='This contact has no tasks.' data-options='{\"clearTimeoutSeconds\":\"0\"}'></alert>" . PHP_EOL;
}
else
{


}
?>
    </div> <!-- #tabTasks -->


    <div class='tab-pane' id='tabAttach'>
<?php
if (empty($attachments))
{
	echo "<alert data-type='warning' data-msg='This contact has no attachments.' data-options='{\"clearTimeoutSeconds\":\"0\"}'></alert>" . PHP_EOL;
    //echo $this->alerts->info("This contact has no attachments.");
}
else
{


}
?>
    </div> <!-- #tabAttach -->

    <div class='tab-pane' id='tabOrders'>
        <h4><i class='fa fa-clipboard'></i> Orders</h4>
<?php
if (empty($orders))
{
	echo "<alert data-type='info' data-msg='This contact has not placed any orders!' data-options='{\"clearTimeoutSeconds\":\"0\"}'></alert>" . PHP_EOL;
}
else
{
echo <<< EOS
    <table class='table table-hover table-bordered'>
        <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Items</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($orders as $r)
    {
        $date = $items = null;

        $total = 0;

        try
        {
            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "m/d/Y g:i A");

            $items = $this->orders->getOrderItems($r->id);

            if (!empty($items))
            {
                $itemDisplay = null;

                foreach ($items as $ir)
                {
                    $itemName = $this->items->getTableValue('name', $ir->item);

                    $itemDisplay = $itemName . ' ' . PHP_EOL;
                
                    // calculates total
                    $total += ($ir->retailPrice * $ir->qty);
                }
            }
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "\t<td>{$date}</td>" . PHP_EOL;
        echo "\t<td>\${$total}</td>" . PHP_EOL;
        echo "\t<td>{$itemDisplay}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }


echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}

?>
    </div> <!-- #tabOrders -->
	
	<div class='clearfix'></div>
</div> <!-- .tab-content -->


<?php
/*
<div class='row'>

    <div class='col-lg-12'>
        <button type='button' class='btn btn-primary' id='saveBtn'><i class='fa fa-cloud-upload'></i> Save</button>
        <button type='button' class='btn btn-default' id='cancelBtn'><i class='fa fa-ban-circle'></i> Cancel</button>
    </div> <!-- .col-lg-9 .col-lg-offset-2 -->

</div> <!-- .row -->
 */
?>

</form>
