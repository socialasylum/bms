<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-rocket'></i> CRM</h1>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

    <ul class="nav navbar-nav">
        <li><a href='/crm/edit'><i class='fa fa-plus'></i> New Contact</a></li>
    </ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php
if (empty($contacts))
{
    echo $this->alerts->info('No CRM contacts have been created.');
}
else
{

echo <<< EOS
    <table id='crmTbl'  class='table table-hover table-condensed'>
        <thead>
            <tr>
                <th>Company Name</th>
                <th>Name</th>
                <th>Title</th>
                <th>Email</th>
                <th>Type</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
                <th>Phone</th>
            </tr>
        </thead>
        <tbody>
EOS;

        foreach ($contacts as $r)
        {

            $type = $status = $address = $phone = $email = null;

            try
            {
                if (!empty($r->type)) $type = $this->functions->codeDisplay(8, $r->type);

                $email = $this->crm->getContactEmail($r->id);

                $address = $this->crm->getContactAddress($r->id);
                $phone = $this->crm->getContactPhone($r->id, 1);
                $fax = $this->crm->getContactPhone($r->id, 3);

            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

            echo "<tr onclick=\"crm.editContact(this, {$r->id})\">";

            echo "<td>{$r->companyName}</td>";
            echo "<td>{$r->name}</td>";
            echo "<td>{$r->title}</td>";
            echo "<td>{$email}</td>";
            echo "<td>{$type}</td>";
            echo "<td>{$address->address}</td>";
            echo "<td>{$address->city}</td>";
            echo "<td>{$address->state}</td>";
            echo "<td>{$address->postalCode}</td>";
            echo "<td>{$phone}</td>";
            // echo "<td>{$fax}</td>";
            // echo "<td></td>";


            echo "</tr>";
        }

        echo "</tbody>";
        echo "</table>";


}
?>
