<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit' ?> Video</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm'
        );

echo form_open_multipart('/video/save', $attr);

if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
?>

    <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>

<div class='row'>
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6 form-horizontal'>
        <div class="form-group announcement-subject">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='title'>Title</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' name='title' id='title' class='form-control' value="" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='description'>Description</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <textarea class='form-control' id='description' name='description'></textarea>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='tags'>Tags</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <textarea class='form-control' id='tags' name='tags'></textarea>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='status'>Status</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <select name="status" id="status" class='form-control'>
                    <?php
                    if (!empty($status))
                    {
                        foreach ($status as $k => $v)
                        {

                            // $sel = ($r->code == $info->status) ? "selected='selected'" : null;

                            if (empty($id) && $k == 1) $sel = "selected='selected'";

                            echo "<option {$sel} value='{$k}'>{$v}</option>" . PHP_EOL;
                        }
                    }
                    ?>
                    </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='thumbnail'>Thumbnail</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='file' name='thumbnail' id='thumbnail'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

    </div> <!-- col-6 -->

    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6 form-horizontal'>
        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='src'>Source</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='file' name='src' id='src' class=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->
    </div>
</div> <!-- .row -->

</form>
