<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <h1><i class='fa fa-film'></i> Videos</h1>

    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='rootFolder' value='<?=$rootFolder?>'>
    <input type='hidden' id='admin' value='<?=(int) $admin?>'>
    <input type='hidden' id='folder' value='<?=$folder?>'>

    <input type='hidden' name='module' id='module' value='<?=$modId?>'>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class='fa fa-upload'></i> New Video <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a tabindex="-1" role='button' href="#youTubeModal" data-toggle='modal'><i class='fa fa-youtube'></i> YouTube</a></li>
                    <li><a href="/video/edit/0/<?=$folder?>" ><i class='fa fa-upload'></i> Upload Video</a></li>
                </ul>
          </li>

            <li><a href='javascript:void(0);' id="createFolderBtn"><i class='fa fa-folder-open-o'></i> New Folder</a></li>
        </ul>


    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<div id='video-container'>
<?php
    if (empty($content) && empty($folder))
    {
        echo $this->alerts->info('No videos have been uploaded!');
    }
    else
    {

    echo "<ol id='fileList'>\n";

    if (!empty($folder))
    {
        try
        {
            if (!empty($parentFolder)) $parentFolderName = $this->folders->getTableValue('name', $parentFolder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "\t<li class='ui-state-default folder' value='{$parentFolder}' itemType='1' id='folder_{$parentFolder}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>../{$parentFolderName}</div></li>\n";
    }

    // $rcnt = 1;
    foreach ($content as $r)
    {
        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        if ($r->type == 1)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} folder' value='{$r->id}' itemType='1' id='folder_{$r->id}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>{$name}</div></li>\n";
        }
        elseif ($r->type == 2)
        {


            $class = ($r->active == 1) ? null : ' hidden-doc';
            
            $icon = "<i class='fa fa-film drag-icon'></i>";


            if (!empty($r->youtubeUrl) && !empty($r->thumbnail)) $icon = "<img src='{$r->thumbnail}' height='50px'>";

            echo "\t<li class='ui-state-default{$class} item' value='{$r->id}' itemType='2' id='item_{$r->id}' itemID='{$r->id}' tbl='video'>{$icon}<div class='docName'>{$name}</div></li>\n";
        }

    }

    echo "</ol>\n";


    }
?>
</div>


<!-- YouTube modal //-->
<div id='youTubeModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'><i class='fa fa-youtube'></i> YouTube</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='youtubeAlert'></div>

        <p class='lead'><i class='fa fa-youtube'></i> Enter the URL of the YouTube Video</p>
<?php
    $attr = array
        (
            'name' => 'youtubeForm',
            'id' => 'youtubeForm'
        );

    echo form_open('#', $attr);
?>
        <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>
        <p><input type='text' class='form-control' name='youtubeUrl' id='youtubeUrl'  placeholder='http://youtu.be/...'></p>
        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true' id='closeYoutubeModalBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='youtubeBtn'>Save</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



