<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3><i class='fa fa-folder-open'></i> <?=$info->name?> Folder Properties</h3>
    </div> <!-- .modal-header -->

    <div class='modal-body form-horizontal'>
    
    <div id='folderPropAlert'></div>

<?php

    $attr = array
        (
            'name' => 'folderPropForm',
            'id' => 'folderPropForm'
        );

    echo form_open('#', $attr);
?>

    <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>


    <input type='hidden' name='currentName' id='currentName' value="<?=$info->name?>">

<div class='tabbable'>
    <ul class='nav nav-tabs' id='userTabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab"><i class='fa fa-cog'></i> Settings</a></li>
        <li><a href='#tabViewable' data-toggle="tab"><i class='fa fa-eye'></i> Viewable</a></li>
    </ul>



<div class="tab-content">

    <div id="tabSettings" class='tab-pane active'>
        <h3><i class='fa fa-cog'></i> General Folder Settings</h3>


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='name'>Name</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>">
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='active'>Active</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="active" id="active" class='form-control'>
            <?php
                foreach($status as $code => $display)
                {
                    // if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                    $sel = ($info->active == $code) ? "selected='selected'" : null;

                    echo "<option {$sel} value='{$code}'>{$display}</option>\n";
                }
            ?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->
    
        </div> <!-- #tabSettings -->

    <div id="tabViewable" class='tab-pane'>
        <h3><i class='fa fa-eye'></i> Folder Viewable</h3>

        <p class='lead'>Select which modules this folder is viewable in.</p>

<div id='modViewInputs'>
<?php
if (!empty($folder) && !empty($viewableMods))
{
    foreach ($viewableMods as $r)
    {
        echo "<input type='hidden' name='module[]' id='module_{$r->module}' value='{$r->module}'>" . PHP_EOL;
    }
}
?>
</div> <!-- #modviewInputs -->
<?php
if (empty($folderMods))
{
    echo $this->alerts->info("There are no modules that utilize folders!");
}
else
{
    // print_r($folderMods);
    foreach ($folderMods as $mod)
    {
        $icon = null;

        try
        {
            $name = $this->modules->getTableValue('name', $mod);
            $icon = $this->modules->getTableValue('icon', $mod);

            if (!empty($icon)) $icon = "<i class='{$icon}'></i> ";
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $name = "[Database Error]";
        }

        echo "<button type='button' class='btn btn-default btn-checkbox' onclick=\"global.toggleCheckboxBtn(this);\" id='moduleBtn_{$mod}' value='{$mod}'>{$icon}{$name}</button>" . PHP_EOL;
    }
}
?>


    </div> <!-- #tabViewable -->

</div> <!-- .tab-content -->

</div> <!-- .tabbable -->

        </form>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-info' id='savePropBtn' onclick="folders.saveProperties(this);">Save &amp; Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->


