<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='modal-dialog'>
    <div class='modal-content'>


<div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
    <h4 class='modal-title'>Create a New Folder</h4>
</div> <!-- .modal-header //-->

<div class='modal-body'>
    <div id='folderAlert'></div>

    <p class='lead'><img src='/public/images/windows_folder_icon.png'> Enter the name of your new folder</p>

    <?php
    //<form name='createFolderForm' id='createFolderForm'>

    $attr = array
    (
        'name' => 'createFolderForm',
        'id' => 'createFolderForm',
    );

    echo form_open('#', $attr);
    ?>
    <input type='hidden' name='folder' id='folder'  value='0'>

    <input type='text' class='form-control' name='folderName' id='folderName' placeholder='Folder Name'>
    </form>
</div> <!-- .modal-body //-->

<div class='modal-footer'>
    <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
    <button type='button' class='btn btn-primary' aria-hidden='true' id='createFolderBtn' onclick="folders.checkCreateForm(this);">Create Folder</button>
</div> <!-- .modal-footer //-->

    </div> <!-- .modal-content -->
</div> <!-- .modal-dialog -->
