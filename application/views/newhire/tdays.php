<?php if(!defined('BASEPATH')) die('Direct access not allowed');

if (empty($days))
{
    echo $this->alerts->info("No training days have been selected!");
}
else
{
    echo "<hr><p class='lead'><strong>Step 2</strong> search and assign documents, and forms to each day of training.</p>";


    for ($i = 1; $i <= $days; $i++)
    {

        try
        {
            $docs = $this->newhire->getNewhireDocs($i);

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<div class='panel panel-default'>";

        echo "<div class='panel-heading'>Day {$i}</div>";

        echo "<div class='panel-body form-horizontal'>" . PHP_EOL;


        echo "<div id='day{$i}alerts'></div>" . PHP_EOL;

        echo <<< EOS
            <div class="form-group">
                <label class='col-xs-3 col-sm-3 col-md-3 col-lg-2 control-label' for='doc_{$i}'>Search</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-10 controls">
                    <input type='text' class='typeahead form-control' name='doc[{$i}]' id='doc_{$i}' day='{$i}' autocomplete='off' >
                </div>
            </div>

EOS;

            echo "<hr>" . PHP_EOL;


        if (empty($docs))
        {
            echo $this->alerts->info("This day has no documents assigned to it!");
        }
        else
        {
            echo "<ul id='fileList' day='{$i}'>" . PHP_EOL;

            foreach ($docs as $r)
            {
                try
                {
                    $docName = $this->documents->getTableValue('title', $r->document);
                }
                catch(Exception $e)
                {
                    $this->functions->sendStackTrace($e);
                }

                $name = (strlen($docName) > 25) ? substr($docName, 0, 22) . '...' : $docName;

                if ($r->policyType == 3) $img = 'movie-icon.png'; // video
                elseif ($r->policyType == 11) $img = 'pdf-icon.png'; // pdf
                else $img = 'doc_blue_trans.png';


                echo "\t<li class='ui-state-default{$class}' value='{$r->id}' itemType='2' documentType='{$docType}' id='doc_{$r->id}'><img src='/public/images/{$img}'><div class='docName'>{$name}</div></li>\n";
            }


            echo "</ul>" . PHP_EOL;
        }

        echo "</div> <!-- .panel-body -->";
        echo "</div> <!-- .panel -->";
    }
}
?>
