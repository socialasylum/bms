<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>



<div class='module'>
    <div class='module-head'><h3>Newhire Settings</h3></div>
    <div class='module-body'>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


    <p class='lead'><strong>Step 1</strong> select how many days of training you would like new employees to go through.</p>

<div class='row'>
<div class='col-lg-12 form-horizontal'>
<div class="form-group">
    <label class='col-lg-3 control-label' for='days'>Training Days</label>
    <div class="col-lg-9 controls">
        <select name="days" id="days" class='form-control'>
            <option value=""></option>
<?php
    for ($i = 1; $i <= 10; $i++)
    {

        $sel = ($i == $days) ? 'selected' : null;

         echo "<option {$sel} value='{$i}'>{$i}</option>" . PHP_EOL;
    }
?>

        </select>
    </div>
</div>

</div> <!-- .col-lg-12 -->
</div> <!-- .row -->

<div id='dayDisplay'></div>

        </div> <!-- .module-body -->
</div> <!-- .module -->

