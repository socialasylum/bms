<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='icon-time'></i> Clock In &amp; Out</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

	<button type='button' id='addPunchBtn' class='btn btn-link navbar-btn'><i class='fa fa-plus info'></i> Add Time Punch</button>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<h3>Previous Time Punches</h3>

<div id='historyDisplay'></div>

<!-- punch modal //-->
<div id='punchModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-body'>
        <div id='punchAlert'></div>

        <p class='lead'>Adding time punch to server.</p>

        <div id='punchMsg' align='center'>
            <img src='/public/images/loader.gif'>
        </div>


<?php
$attr = array
    (
        'name' => 'punchForm',
        'id' => 'punchForm'
    );

echo form_open('#', $attr);
?>
        <input type='hidden' name='addPunch' value='1'>

        </form>
    </div> <!-- .modal-body //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



