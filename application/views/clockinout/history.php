<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
if (empty($history))
{
    echo $this->alerts->info("No time punches available");
}
else
{

    echo <<< EOS
    <table class='table table-striped table-condensed' id='punchTbl'>
        <thead>
            <tr>
                <th>Type</th>
                <th>Time Punch</th>
            </tr>
        </thead>
        <tbody>
EOS;


    foreach ($history as $k => $r)
    {
        $type = ($k % 2) ? 'Clock Out' : 'Clock In';

        $time = $this->users->convertTimezone($this->session->userdata('userid'), $r->timepunch, "m/d/Y g:i A");

        echo "<tr>" . PHP_EOL;

        echo "<td>{$type}</td>";
        echo "<td>{$time}</td>";

        echo "</tr>" . PHP_EOL;
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;

}
?>
