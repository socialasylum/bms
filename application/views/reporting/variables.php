<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='modal-dialog'>
    <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h2 class='modal-title'><i class='fa fa-asterisk'></i> Variables</h2>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='varAlert'></div>

        <p class='lead'><strong><?=$cnt?></strong> <?=($cnt > 1) ? 'variables have' : 'variable has'?> been detected in this query. Please enter a placeholder value below.</p>

        <code>
        <?=nl2br($query)?>
        </code>

<hr>

<?php
if (!empty($vars))
{
    foreach ($vars as $k => $v)
    {
echo <<< EOS
    <div class="form-group">
        <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='vars_{$k}'>$v</label>
        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='vars[{$k}]' id='vars_{$k}' value=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->
EOS;

    }
}
?>



    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Cancel</button>
        <button type='button' class='btn btn-primary' id='varExecBtn'>Execute</button>
    </div> <!-- .modal-footer //-->
    </div> <!-- .modal-content -->
</div> <!-- .modal-dialog -->
