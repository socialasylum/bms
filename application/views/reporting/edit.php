<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-bar-chart-o'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Report</h1>
<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'class' => 'form-horizontal'
        );

    echo form_open_multipart('/user/update', $attr);

    if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

    <ul class="nav navbar-nav">
        <li><a href='javascript:void(0);' id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        <li><a href='#'><i class='fa fa-cog'></i> Settings</a></li>
    </ul>

    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

    <div class='doc-title-row'>

        <input type='text' name='name' class='form-control' id='name' value="" placeholder='Report Name'>
    </div>

<?php
/*
<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='name'>Name</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' name='name' class='form-control' id='name' value="" placeholder='Report Name'>
    </div> <!-- .controls -->
</div> <!-- .form-group -->



        <div class='row'>
            <div class='col-12'>
            <button type='button' class='btn btn-primary' name='saveBtn' id='saveBtn'>Save</button>
            <button type='button' class='btn' name='cancelBtn' id='cancelBtn'>Cancel</button>
            </div>  <!-- .col-12 -->
        </div> <!-- .row -->
*/
?>
</form>
