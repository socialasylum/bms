<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-clipboard'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Dataset</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

    <ul class="nav navbar-nav">
        <li><a href='javascript:void(0);' id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        <li><a href='javascript:void(0);' id='execBtn'><i class='fa fa-exclamation danger'></i> Execute</a></li>
    </ul>

    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<?php
$attr = array
    (
        'id' => 'dsForm',
        'name' => 'dsForm'
    );

echo form_open('#', $attr);
?>


<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='folder' id='folder' value='<?=$folder?>'>

<div class='row report-container'>
    <div class='col-lg-3' id='folder-tree-display'>
        <h3><i class='fa fa-table'></i> Tables</h3>

            <div id='folder-tree'></div>

    </div> <!-- .col-lg-3 -->

    <div class='col-lg-9'>

        <div class='doc-title-row'>
            <input type='text' class='doc-title form-control' name='name' id='name' placeholder='Dataset Name' value="<?=$info->name?>">
        </div>

            <textarea rows='10' class='form-control' name='query' id='query'><?=$info->query?></textarea>

        <hr>
        <div id='resultsDisplay'>

            <?=$this->alerts->info('No results to display')?>

        </div> <!-- .row -->

    </div> <!-- .col-lg-9 -->

</div> <!-- .row .report-container -->


<div id='varModal' class='modal fade' data-backdrop=''></div>

</form>
