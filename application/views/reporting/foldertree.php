<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<ul>
    <li class='folder expanded' data="icon: '/public/images/icons/database.png'">Database
        <ul>
<?php
    if (empty($tables))
    {
        echo "<li>None</li>" . PHP_EOL;
    }
    else
    {
        foreach ($tables as $r)
        {
            $db = $this->db->database;

            $col = "Tables_in_{$db}";

            $table = $r->$col;

            try
            {
                // gets the explain for each table
                $explain = $this->reporting->explainTbl($table);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }


            echo "<li class='table' data=\"icon: '/public/images/icons/database_table.png'\">" . $table;  
    
            if (!empty($explain))
            {
                echo "<ul>";

                foreach($explain as $er)
                {

                    $icon = null;

                    if (!empty($er->Key))
                    {
                        if ($er->Key == 'PRI') $icon = "icon: '/public/images/icons/database_key.png'";
                        else $icon = "icon: '/public/images/icons/key.png'";
                    }
                    else $icon = "icon: '/public/images/icons/text_columns.png'";

                    echo "<li data=\"{$icon}\">{$er->Field} ({$er->Type})</li>" . PHP_EOL;
                }

                echo "</ul>";
            }

            echo "</li>" . PHP_EOL;
        }
    }
?>
        </ul>
    </li>
</ul>


<?php

/*
<ul>
    <li id="inbox" class="folder">Inbox
    <li id="outbox" class="folder">Outbox
    <li id="key1" title="Look, a tool tip!">item1 with key and tooltip
    <li id="key2" class="selected">item2: selected on init
    <li id="key3" class="folder">Folder with some children
        <ul>
            <li id="key3.1">Sub-item 3.1
            <li id="key3.2">Sub-item 3.2
        </ul>

    <li id="key4" class="expanded">Doc w/ children (expanded on init)
        <ul>
            <li id="key4.1">Sub-item 4.1
            <li id="key4.2">Sub-item 4.2
        </ul>

    <li id="key5" class="lazy folder">Lazy folder
    </ul>
*/
?>
