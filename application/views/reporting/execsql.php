<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (empty($data))
{
    echo $this->alerts->info('No results to display');
}
else
{

    $headers = array_keys($data[0]);

    echo "<table class='table table-bordered'>" . PHP_EOL;

    if (!empty($headers))
    {


        echo "<thead>" . PHP_EOL;

        echo "<tr>" . PHP_EOL;

        foreach ($headers as $col)
        {
            echo "\t<th>{$col}</th>" . PHP_EOL;
        }

        echo "</tr>";
        echo "</thead>" . PHP_EOL;
    }


    echo "<tbody>" . PHP_EOL;

    foreach ($data as $r)
    {
        echo "<tr>" . PHP_EOL;

        foreach ($headers as $k => $col)
        {

            echo "\t<td>{$r[$col]}</td>" . PHP_EOL;
        }


        echo "</tr>" . PHP_EOL;
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;

    // print_r($headers);

    // print_r($data);
}
?>
