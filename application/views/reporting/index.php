<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<h1><i class='fa fa-bar-chart-o'></i> Reporting Services</h1>

<input type='hidden' id='rootFolder' value='<?=$rootFolder?>'>
<input type='hidden' id='folder' value='<?=$folder?>'>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    
    <ul class="nav navbar-nav">
        <li><a href='/reporting/edit'><i class='fa fa-plus'></i> New Report</a></li>
        <li><a href='/reporting/createds'><i class='fa fa-clipboard'></i> New Dataset</a></li>
    </ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<div id='report-container'>
<?php
    if (empty($content))
    {
        echo $this->alerts->info('No reports have been created!');
    }
    else
    {

    echo "<div class='row-fluid'>\n";


    echo "<ol id='fileList'>\n";

    $rcnt = 1;
    foreach ($content as $r)
    {
        #if ($rcnt == 1) echo "<div class='row-fluid'>\n";

        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        if ($r->type == 1)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} folder' value='{$r->id}' itemType='1' id='folder_{$r->id}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>{$name}</div></li>\n";
        }

        elseif ($r->type == 2) // report
        {

            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} item' value='{$r->id}' itemType='2' documentType='{$docType}' id='doc_{$r->id}'><i class='fa fa-bar-chart-o drag-icon'></i><div class='docName'>{$name}</div></li>\n";
        }

        elseif ($r->type == 3) // datatset
        {

            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} item' value='{$r->id}' itemType='3' id='ds_{$r->id}'><i class='fa fa-clipboard drag-icon'></i><div class='docName'>{$name}</div></li>\n";
        }
    }

    echo "</ol>\n";

    if ($rcnt <= 12) echo "</div>\n <!-- .row-fluid (outside) //-->"; // closes row if not 12 docs
    }
?>
</div> <!-- #report-container -->


