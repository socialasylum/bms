<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row'>
    <div class='col-lg-12 form-horizontal'>
        <h1><i class='icon-qrcode'></i> Codes Editor</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

    <ul class="nav navbar-nav">
        <li><a href='#newCodeGroupModal' data-toggle='modal'><i class='icon-plus'></i> New Code Group</a></li>
        <!-- <li><a href='#'><i class='icon-cog'></i> Settings</a></li> -->
    </ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>




        <p class='lead'>Here you can edit the various codes on your intranet.</p>
        
        <p><strong>Step 1</strong> Select a code group</p>.
    
        <div class="form-group">
            <label class='col-lg-3 control-label' for='group'>Code Group</label>
            <div class="col-lg-9 controls">
                <select name="group" id="group" class='form-control'>
                    <option value="0"></option>
<?php
if (!empty($groups))
{
    foreach ($groups as $r)
    {
        $sel = ($r->group == $group) ? "selected='selected'" : null;

        echo "<option {$sel} value='{$r->group}'>{$r->display} : {$r->group}</option>" . PHP_EOL;
    }
}
?>
                </select>
            </div>
        </div>


    <div class='' id='codes-display'>
        <hr>
        <?=$this->alerts->info("Please select a code group")?>
    </div>


    </div> <!-- .col-lg-12 -->
</div> <!-- .row -->


<div class="modal fade" id='newCodeGroupModal'>
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">New Code Group</h3>
          </div> <!-- .modal-header -->

            <div class="modal-body form-horizontal">

            <div id='codeGroupAlert'></div>

            <p class='lead'>Enter the name of the new code group you want to create.</p>

<?php
        $attr = array
            (
                'name' => 'newGroupForm',
                'id' => 'newGroupForm'
            );

        echo form_open('#', $attr);
?>



            <div class="form-group">
                <label class='col-lg-4 control-label' for='groupName'>Code Group Name</label>
                <div class="col-lg-8 controls">
                    <input type='text' name='groupName' id='groupName' class='form-control' value="" placeholder=''>
                </div>
            </div>


            <div class="form-group">
                <label class='col-lg-4 control-label' for='editable'>Editable</label>
                <div class="col-lg-8 controls">
                    <label class='checkbox'>
                        <input type='hidden' name='editable' value='0'>
                        <input type='checkbox' name='editable' id='editable' value='1'> is this group editable?
                    </label>
                </div>
            </div>

        </form>

            </div> <!-- .modal-body -->

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id='createGroupBtn'>Create Group</button>
          </div> <!-- .modal-footer -->

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
