<?php if(!defined('BASEPATH')) die('Direct access not allowed'); 

echo "<hr>" . PHP_EOL;

if (empty($group))
{
    echo $this->alerts->info("Please select a code group");
}
else
{

       echo "<h3>{$info->display} : {$info->group}</h3>";


        if (empty($codes)) echo $this->alerts->alert("This group has no codes. You can create one below.");


        // sets flag if codes are avaiable for particular group
        $hasCodes = (empty($codes)) ? false : true;

        $attr = array
            (
                'name' => 'codeGroupForm',
                'id' => 'codeGroupForm'
            );

        echo form_open('#', $attr);

        echo "<input type='hidden' name='group' id='group' value='{$group}'>" . PHP_EOL;

        $rcnt = 1;
        $cnt = count($codes);

        if (empty($codes)) $cnt = 1;

        for ($i = 0; $i < $cnt; $i++)
        // foreach ($codes as $k => $r)
        {

            $displayInputName = ($hasCodes) ? "display[{$codes[$i]->code}]" : 'newDisplay';
            $companyInputName = ($hasCodes) ? "company[{$codes[$i]->code}]" : 'newCompany';

            $disabled = ($codes[$i]->active == 1) ? null : "disabled='disabled'";

            $codeDisplay = ($hasCodes) ? $codes[$i]->code : 'Undefined';


            if ($rcnt == 1) echo "<div class='row'>" . PHP_EOL;


            if ($k == 0)
            {
                echo "<div id='master-code-panel'>" . PHP_EOL;
            
                $closeMasterPanel = true;
            }
            echo <<< EOS

        <div class='col-lg-6'>

        <div class='panel panel-default form-horizontal'>
        <div class='panel-heading'>Code</div>

        <div class='panel-body'>

EOS;

        if ($hasCodes)
        {

echo <<< EOS
            <div id='hiddenInputs' class='hide' style='display:none;'>
                <input type='hidden' name='codes[{$codes[$i]->code}]' id='code' value='{$codes[$i]->code}'>
                <input type='hidden' name='active[{$codes[$i]->code}]' id='active' value='{$codes[$i]->active}'>
            </div> <!-- #hiddenInputs -->
EOS;
        } // $hasCodes

echo <<< EOS
            <div class='row code-display'>
                <div class='col-lg-3'>
                    <strong class='pull-right'>Code</strong>
                </div>

                <div class='col-lg-9' id='newCodeNumber'>
                    {$codeDisplay}
                </div>
            </div>

            <div class="form-group">
                <label class='col-lg-3 control-label' for='display'>Display</label>
                <div class="col-lg-9 controls">
                    <input type='text' name='{$displayInputName}' id='display' class='form-control' {$disabled} value="{$codes[$i]->display}" placeholder=''>
                </div>
            </div>

            <div class="form-group">
                <label class='col-lg-3 control-label' for='company'>Company Access</label>
                <div class="col-lg-9 controls">
                    <select name='{$companyInputName}' id="company" class='form-control' {$disabled} >
                        <option value="0">All</option>
EOS;

    if (!empty($companies))
    {
        foreach ($companies as $cr)
        {
            $sel = ($cr->id == $codes[$i]->company) ? 'selected' : null;
            echo "<option {$sel} value='{$cr->id}'>{$cr->companyName}</option>" . PHP_EOL;
        }
    }

echo <<< EOS
                    </select>
                </div>
            </div>

EOS;

    if ($hasCodes)
    {
        $actBtn = $deactBtn = null;

        if ($codes[$i]->active == 1) $actBtn = "display:none;";
        else $deactBtn = "display:none;";

        echo <<< EOS
            </div> <!-- .panel-body -->

            <div class='panel-footer'>
                    <button type='button' class='btn btn-danger' name='deactBtn' id='deactBtn' onclick="codes.deactiveCode(this);" style="{$deactBtn}" ><i class='icon-ban-circle'></i> Deactivate</button>

                    <button type='button' class='btn btn-success' name='actBtn' id='actBtn' onclick="codes.activeCode(this);" style="{$actBtn}"><i class='icon-ok'></i> Activate</button>
            </div> <!-- .panel-footer -->
EOS;
    }
echo <<< EOS
        </div> <!-- .panel -->
        </div> <!-- .col-lg-6 -->
EOS;


            if ($closeMasterPanel == true)
            {
                echo "<div class='clearfix'></div></div> <!-- .master-code-panel -->" . PHP_EOL; // .master-code-panel
                $closeMasterPanel = false;
            }

            if ($rcnt >= 2)
            {
                echo "<div class='clearfix'></div></div> <!-- .row (inside loop) -->";

               $rcnt = 1; 
            }
            else
            {
                $rcnt++;
            }

        } // end of foreach loop

        if ($rcnt >= 2)
        {

            echo "<div id='add-code-display' style=\"display:none;\"></div>";

            echo "</div> <!-- .row (outside loop) -->";
        }
        else
        {

            echo "<div class='row' id='add-code-display' style=\"display:none;\"></div>";
        }

?>

</form>

<div class='row'>
<hr>
        <button type='button' class='btn btn-primary' name='saveBtn' id='saveBtn'><i class='icon-cloud-upload'></i> Save</button>
        <button type='button' class='btn btn-default' name='cancelBtn' id='cancelBtn'><i class='icon-ban-circle'></i> Cancel</button>

        <?php if ($hasCodes) : ?>
        <button type='button' class='btn btn-info pull-right' name='addBtn' id='addBtn'><i class='icon-plus'></i> Add Code</button>
        <?php endif; ?>
</div>

<?php
/*
<div class='row' id='save-form-actions'>
    <div class='col-lg-12 form-actions'>
        <button type='button' class='btn btn-primary' name='saveBtn' id='saveBtn'><i class='icon-cloud-upload'></i> Save</button>
        <button type='button' class='btn btn-default' name='cancelBtn' id='cancelBtn'><i class='icon-ban-circle'></i> Cancel</button>

        <?php if ($hasCodes) : ?>
        <button type='button' class='btn btn-info pull-right' name='addBtn' id='addBtn'><i class='icon-plus'></i> Add Code</button>
        <?php endif; ?>

    </div>
</div>

 */
?>

<?php

    }

?>

