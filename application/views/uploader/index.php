<?php if(!defined('BASEPATH')) die('Direct access not allowed');

    $attr = array
        (
            'name' => 'uploaderForm',
            'id' => 'uploaderForm',
            'method' => 'post'
        );

echo form_open_multipart('/uploader/upload', $attr);
?>

    <input type='file' id='fileUploader' name='fileUploader'>

</form>


