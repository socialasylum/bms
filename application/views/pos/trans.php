<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<?php
if (empty($transaction))
{

}
else
{

    // print_r($transaction); // echo for debugging

    try
    {
        $items = $this->pos->compileTransQty($transaction);
        $tax = 0.085;
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }


    // print_r($items); // echo for debugging
    
    $subtotal = 0;

    echo <<< EOS
        <table class='table table-bordered trans-table'>
            <thead>
                <tr>
                    <th>Item</th>
                    <!-- <th>ID</th> //-->
                    <th>Qty</th>
                    <!-- <th>Unit Price</th> //-->
                    <th>Price</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
EOS;

    foreach ($items['item'] as $itemID)
    {

        $itemName = $price = $totalPrice = null;


        try
        {
            $itemName = $this->items->getTableValue('name', $itemID);
            $price = $this->items->getTableValue('retailPrice', $itemID);

            $totalPrice = $price * $items['qty'][$itemID];

            $subtotal += $totalPrice;

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        echo "<tr>" . PHP_EOL;

        echo "\t<td width='25%'>{$itemName}</td>" . PHP_EOL;
        // echo "\t<td>{$itemID}</td>" . PHP_EOL;
        echo "\t<td>";
        echo "
            <div class='input-group'>
                <span class='input-group-btn'>
                    <button class='btn btn-info' itemID='{$itemID}' itemName=\"{$itemName}\" onclick=\"pos.deleteItem(this, 1);\"><i class='fa fa-minus'></i></button>
                </span>
                <input type='text' class='form-control pos-qty' style='' value='{$items['qty'][$itemID]}' readonly>
                <span class='input-group-btn'>
                    <button class='btn btn-info' onclick=\"pos.addItem({$itemID});\"><i class='fa fa-plus'></i></button>
                </span>
            </div> <!-- .input-group -->
            ";
        echo "</td>" . PHP_EOL;

        // echo "\t<td>\$" . number_format($price, 2) . "</td>" . PHP_EOL;
        echo "\t<td>\$" . number_format($totalPrice, 2) . "</td>" . PHP_EOL;
        echo "\t<td><button class='btn btn-danger' itemID='{$itemID}' itemName=\"{$itemName}\" onclick=\"pos.deleteItem(this);\"><i class='fa fa-trash-o'></i></button></td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;


    }

    echo "<tr><td colspan='2' class='trans-total'>Subtotal</td><td colspan='2'>\$" . number_format($subtotal, 2) . "</td></tr>";
    echo "<tr><td colspan='2' class='trans-total'>Tax</td><td colspan='2'>\$" . number_format(($subtotal * $tax), 2) . " (" . number_format(($tax * 100), 2) . "%)</td></tr>";
    echo "<tr><td colspan='2' class='trans-total'>Total</td><td colspan='2'>\$" . number_format(($subtotal  + ($subtotal * $tax)), 2) . "</td></tr>";

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;

}

?>
