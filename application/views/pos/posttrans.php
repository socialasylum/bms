<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<div class='row pos-numbers'>
    <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default btn-item' name='printBtn' id='printBtn' onclick=""><i class='fa fa-print pos-folder'></i><br>Print Receipt</button>
    </div>

    <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-denger btn-item' name='postVoidBtn' id='postVoidBtn' onclick=""><i class='fa fa-ban pos-folder'></i><br>Post Transaction Void</button>
    </div>

    <div class='col-lg-4 col-md-4 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-denger btn-item' name='newTransBtnBtn' id='newTransBtn' onclick="pos.loadActions();"><i class='fa fa-arrow-right pos-folder'></i><br>New Transaction</button>
    </div>

</div>
