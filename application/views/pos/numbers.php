<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row pos-numbers'>
    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn7' id='btn7' onclick="pos.addPosInput('7');">7</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn8' id='btn8' onclick="pos.addPosInput('8');">8</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn9' id='btn9' onclick="pos.addPosInput('9');">9</button>
    </div>

</div>


<div class='row pos-numbers'>
    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn4' id='btn4' onclick="pos.addPosInput('4');">4</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn5' id='btn5' onclick="pos.addPosInput('5');">5</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn6' id='btn6' onclick="pos.addPosInput('6');">6</button>
    </div>

</div>

<div class='row pos-numbers'>
    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn1' id='btn1' onclick="pos.addPosInput('1');">1</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn2' id='btn2' onclick="pos.addPosInput('2');">2</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn3' id='btn3' onclick="pos.addPosInput('3');">3</button>
    </div>

</div>


<div class='row pos-numbers'>
    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btnBack' id='btnBack' onclick="pos.delPosChar();">&laquo;</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btn0' id='btn0'>0</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default' name='btnEnter' id='btnEnter'>Enter</button>
    </div>

</div>
