<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
if (empty($items))
{
    echo $this->alerts->info("No items available!");
}
else
{

    // only display 9 items max

    $totalItemCount = count($items);

    $rcnt = 1;
    $cnt = 1;
    foreach ($items as $r)
    {
        $priceDisplay = $folder = $onclick = null;

        // only displays active items
        if ($r->active == 1)
        {

            if ($rcnt == 1) echo PHP_EOL . "<div class='row pos-numbers'>" . PHP_EOL;

            // $folder = ($r->type == 1) ? "<i class='icon-folder-open pos-folder'></i><br>" : null;

            if ($r->type == 1)
            {
                $folder = "<i class='fa fa-folder-open pos-folder'></i><br>";

                $onclick = "pos.loadItems({$r->id});";
            }
            else
            {

                try
                {
                    $img = $this->item->getItemImages($r->id, 1);
                }
                catch (Exception $e)
                {
                    $this->functions->sendStackTrace($e);
                    $img = null;
                }

                $class = ($r->active == 1) ? null : ' hidden-doc';


                if (empty($img[0]->imageName)) $icon = "<i class='fa fa-tag pos-folder'></i>";
                else
                {
                    $icon = "<img src='/genimg/render/100?img=" . urlencode($img[0]->imageName) . "&path=" . urlencode("uploader/{$this->session->userdata('company')}") . "' class=''>";

                    $r->name = "<span class='imgItemName'>{$r->name}</span>" . PHP_EOL;
                }
                $folder = "{$icon}<br>";
                // $folder = "<i class='fa fa-tag pos-folder'></i><br>";

                $onclick = "pos.addItem({$r->id});";

            }


            try
            {
                if ($r->type == 2) $priceDisplay = "<br>\$" . $this->items->getTableValue('retailPrice', $r->id);
            }
            catch (Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

            echo "<div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>" . PHP_EOL;
            echo "\t<button type='button' class='btn btn-default btn-item' name='' id='' onclick=\"{$onclick}\">{$folder}{$r->name}{$priceDisplay}</button>" . PHP_EOL;
            echo "</div> <!-- .col-lg-4 col-md-5 col-sm-4 col-xs-4 -->" . PHP_EOL;

            $cnt++;

            if ($rcnt >= 3)
            {
                echo "</div> <!-- .row .pos-numbers -->" . PHP_EOL;
                $rcnt = 1;
            }
            else
            {
                $rcnt++;
            }

            if ($cnt >= 9) break;
        }

    }

    if ($rcnt > 1 && $rcnt <= 3)
    {
        echo "</div> <!-- .row .pos-numbers -->" . PHP_EOL;
    }
}


?>



<div class='row pos-numbers'>
    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
<?php
$prevOnclick = (empty($folder)) ? "pos.loadActions();" : "pos.loadItems({$parentFolder})";

?>
        <button type='button' class='btn btn-default btn-item' name='prevBtn' id='prevBtn' onclick="<?=$prevOnclick?>">&laquo; Previous</button>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
<?php if ($cnt >= 9) : ?>
        <button type='button' class='btn btn-default btn-item' name='nextBtn' id='nextBtn' onclick="">Next &raquo;</button>
<?php endif; ?>
    </div>

    <div class='col-lg-4 col-md-5 col-sm-4 col-xs-4'>
        <button type='button' class='btn btn-default btn-item' name='btn8' id='btn8' onclick="pos.loadFinishTrans();">Complete<br>Transaction &raquo;</button>
    </div>

</div>
