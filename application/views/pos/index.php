<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>
    <input type='hidden' id='salesTax' value='<?=$salesTax?>'>

<?php
/*
<ul class="breadcrumb" id='folderCrumbs'>
    <li><a href='/welcome/landing'><i class='icon-home'></i></a></li>
    <li><a href='/pos'><i class='icon-dollar'></i> Point of Sale</a></li>
</ul>
 */
?>
<div id='pos-content'>

    <div class='row'>

        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
            <div class='panel panel-default'>
                    <div class='panel-heading pos-heading' id='pos-heading'><i class='icon-warning-sign'></i> Click to begin your transaction</div>

                <div class='panel-body'>
                    <div id='pos-sales-content'></div>

                </div> <!-- .panel-body -->


                    <div class='panel-footer'>
                        <input type='text' class='form-control' id='posInput' name='posInput'>
                    </div> <!-- .panel-footer -->
            </div> <!-- .panel -->

        </div>

        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12' id='rightCol'>

        </div>


    </div> <!-- .row -->

</div> <!-- #pos-content -->
