<?php if(!defined('BASEPATH')) die('Direct access not allowed');

    if($edit == true)
    {
        $checkConvict[$this->session->userdata('convict')] = 'checked';
    }
?>

<h1>Employment Application</h1>

<h4>Step 16 of 16</h4>

<p class='lead'>Please complete each miscellaneous info field.</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>


<div class="control-group">
    <label class='control-label' for='skills'>Special Skills</label>
    <div class="controls">
        <textarea name='skills' id='skills' class='input-block-level'><?=$this->session->userdata('skills')?></textarea>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for='milService'>Military or Naval Service</label>
    <div class="controls">
        <input type='text' name='milService' id='milService' value='<?=$this->session->userdata('milService')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='rank'>Rank</label>
    <div class="controls">
        <input type='text' name='rank' id='rank' value='<?=$this->session->userdata('rank')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for=''>Present Membership in Nation Guard or Reserves</label>
    <div class="controls">
        <input type='text' class='textinput' name='guard' id='guard' value='<?=$this->session->userdata('guard')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for=''>Have you been convicted of a crime other than a traffic offense?</label>
    <div class="controls">
        <label class="radio inline">
            <input name="convict" type="radio" id="convict_1" value="1" <?=$checkConvict[1]?>> Yes
        </label>
        <label class="radio inline">
            <input name="convict" type="radio" value="0" id="convict_0" <?=$checkConvict[0]?>> No
        </label>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for=''>When, where and nature of offense?</label>
    <div class="controls">
        <textarea name='offense' id='offense' class='input-block-level'><?=$this->session->userdata('offense')?></textarea>
    </div>
</div>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>

</form>
