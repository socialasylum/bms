<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Employment Application</h1>

<h4>Step 10 of 16</h4>

<p class='lead'>Please complete this section with your desired employment.</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<div class="control-group">
    <label class='control-label' for='position'>Desired Position</label>
    <div class="controls">
        <select class='textinput' name='position' id='position'>
        <?php
        if(!empty($positions))
        {
            foreach($positions as $r)
            {
                $sel = ($r->code == $this->session->userdata('position')) ? 'selected' : null;

                echo "<option {$sel} value='{$r->code}'>{$r->display}</option>\n";
            }
        }
        ?>
        </select>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for='startDate'>Date you can start</label>
    <div class="controls">
        <input type='text' name='startDate' id='startDate' readonly value='<?=$this->session->userdata('startDate')?>'>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for='salary'>Salary Desired</label>
    <div class="controls">
        <input type='text' class='textinput' name='desiredSalary' id='desiredSalary' value='<?=$this->session->userdata('desiredSalary')?>'>
    </div>
</div>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>

</form>
