<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Employment Application Review</h1>


<?php print_r($_SESSION); ?>

<p class='lead'>Please verify all of your information.</p>

<div class='app-review'>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Name <a href='/apply/process/<?=$id?>/1/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Are you 18 or older? <a href='/apply/process/<?=$id?>/5/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Are you eligible to work in US? <a href='/apply/process/<?=$id?>/8/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$this->session->userdata('firstName')?> <?=$this->session->userdata('MI')?> <?=$this->session->userdata('lastName')?></td>
            <td><?php echo ($this->session->userdata('is18') == 1) ? "Yes" : "No"; ?></td>
            <td><?php echo ($this->session->userdata('workUsa') == 1) ? "Yes" : "No"; ?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Address <a href='/apply/process/<?=$id?>/3/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$this->session->userdata('address')?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>City</th>
            <th>State</th>
            <th>Postal Code</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$this->session->userdata('city')?></td>
            <td><?=$this->session->userdata('state')?></td>
            <td><?=$this->session->userdata('postalCode')?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Home Phone <a href='/apply/process/<?=$id?>/4/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Mobile Number <a href='/apply/process/<?=$id?>/4/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>E-mail Address <a href='/apply/process/<?=$id?>/6/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$this->session->userdata('phone')?></td>
            <td><?=$this->session->userdata('mobile')?></td>
            <td><?=$this->session->userdata('email')?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Referred By <a href='/apply/process/<?=$id?>/7/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$reasonDisplay?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Related to/knows someone who works for <?=$companyName?>? <a href='/apply/process/<?=$id?>/9/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Who</th>
            <th>Where</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo ($this->session->userdata('related') == 1) ? "Yes" : "No"; ?></td>
            <td><?=$this->session->userdata('relatedWho')?></td>
            <td><?=$this->session->userdata('relatedWhere')?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Have you previously worked for <?=$companyName?>? <a href='/apply/process/<?=$id?>/9/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>From</th>
            <th>To</th>
            <th>Reason For Leaving?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo ($this->session->userdata('previous') == 1) ? "Yes" : "No"; ?></td>
            <td><?=$this->session->userdata('previousFrom')?></td>
            <td><?=$this->session->userdata('previousTo')?></td>
            <td><?=$this->session->userdata('previousReason')?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Desired State <a href='/apply/process/<?=$id?>/12/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Desired Location <a href='/apply/process/<?=$id?>/12/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Currently Employed <a href='/apply/process/<?=$id?>/12/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Desired Position <a href='/apply/process/<?=$id?>/10/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Date you can start <a href='/apply/process/<?=$id?>/10/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
            <th>Salary Desired <a href='/apply/process/<?=$id?>/10/true' class='btn btn-mini pull-right'><i class='icon-pencil'></i></a></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$this->session->userdata('desiredState')?></td>
            <td><?=$locationInfo->Description?></td>
            <td><?php echo ($this->session->userdata('employed') == 1) ? "Yes" : "No"; ?></td>
            <td><?=$positionDisplay?></td>
            <td><?=$this->session->userdata('startDate')?></td>
            <td><?=$this->session->userdata('desiredSalary')?></td>
        </tr>
    </tbody>
</table>

<hr>
<h3>Availability</h3>

<p><a href='/apply/process/<?=$id?>/11/true' class='btn btn-mini'><i class='icon-pencil'></i></a></p>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
            <th>Saturday</th>
            <th>Sunday</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><strong>AM</strong></td>
            <td><?php if($this->session->userdata('monAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('tueAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('wedAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('thuAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('friAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('satAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('sunAM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
        </tr>
        <tr>
            <td><strong>PM</strong></td>
            <td><?php if($this->session->userdata('monPM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('tuePM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('wedPM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('thuPM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('friPM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('satPM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($this->session->userdata('sunPM') == 1) echo "<i class='icon-ok'></i>"; ?></td>
        </tr>
    </tbody>
</table>

<hr>

<h3>Education</h3>
<p><a href='/apply/process/<?=$id?>/13/true' class='btn btn-mini'><i class='icon-pencil'></i></a></p>
<?php
        $desc = $this->session->userdata('desc');
        $type = $this->session->userdata('type');
        $years = $this->session->userdata('years');
        $grad = $this->session->userdata('grad');
        $studied = $this->session->userdata('studied');

        if(!empty($desc))
        {
         echo <<< EOS
<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Type</th>
            <th>Name &amp; Location</th>
            <th># of years</th>
            <th>Graduate?</th>
            <th>Subjects Studied</th>
        </tr>
    </thead>
    <tbody>
EOS;
            foreach($desc as $k => $v)
            {
                $gradDisplay = ($grad[$k] == 1)? "Yes" : "No";

                $typeDisplay = $this->functions->codeDisplay(4, $type[$k]);

                echo "\n<tr>\n";

                echo "\t<td valign='top'>{$typeDisplay}</td>\n";
                echo "\t<td valign='top'>".nl2br($desc[$k])."</td>\n";
                echo "\t<td valign='top'>{$years[$k]}</td>\n";
                echo "\t<td valign='top'>{$gradDisplay}</td>\n";
                echo "\t<td valign='top'>{$studied[$k]}</td>\n";

                echo "\n</tr>\n";
            }


        echo "</tbody>\n";
        echo "</table>\n";
        }
        else
        {
            echo $this->alerts->info("No education has been entered.");
        }
?>

<hr>


<h3>Professional Experience</h3>


<p><a href='/apply/process/<?=$id?>/14/true' class='btn btn-mini'><i class='icon-pencil'></i></a></p>

<?php

    $companyName = $this->session->userdata('companyName');
    $companyAddress = $this->session->userdata('companyAddress');
    $companyPhone = $this->session->userdata('companyPhone');
    $from = $this->session->userdata('from');
    $to = $this->session->userdata('to');
    $salary = $this->session->userdata('salary');
    $position = $this->session->userdata('position');
    $reason = $this->session->userdata('reason');
        
        
if(!empty($companyName))
{
    echo <<< EOS
    <table class='table table-bordered table-striped'>
        <thead>
            <tr>
                <th>From</th>
                <th>To</th>
                <th>Name, Address &amp; Phone Number</th>
                <th>Salary</th>
                <th>Position</th>
                <th>Reason for leaving</th>
            </tr>
        </thead>
        <tbody>
EOS;
    foreach($companyName as $k => $v)
    {
        echo "\n<tr>\n";

        echo "\t<td valign='top'>{$from[$k]}</td>\n";
        echo "\t<td valign='top'>{$to[$k]}</td>\n";
        echo "\t<td valign='top'>{$companyName[$k]}<br>".nl2br($companyAddress[$k])."<br>{$companyPhone[$k]}</td>\n";
        echo "\t<td valign='top'>{$salary[$k]}</td>\n";
        echo "\t<td valign='top'>{$position[$k]}</td>\n";
        echo "\t<td valign='top'>{$reason[$k]}</td>\n";

        echo "\n</tr>\n";
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>\n";
}
else
{
    echo $this->alerts->info("No professional experience has been entered.");
}

?>

<hr>

<!-- <h3>References <a href='/apply/process/<?=$id?>/15/true'><img src='/public/images/icons/page_white_edit.png'></a></h3> -->

<h3>References</h3>

<p><a href='/apply/process/<?=$id?>/15/true' class='btn btn-mini'><i class='icon-pencil'></i></a></p>

<?php
    $refName = $this->session->userdata('refName');
    $refPhone = $this->session->userdata('refPhone');
    $refOcc = $this->session->userdata('refOcc');
    $refYear = $this->session->userdata('refYear');
?>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Business/Occupation</th>
            <th>Years Acquainted</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$refName[0]?></td>
            <td><?=$refPhone[0]?></td>
            <td><?=$refOcc[0]?></td>
            <td><?=$refYear[0]?></td>
        </tr>
        <tr>
            <td><?=$refName[1]?></td>
            <td><?=$refPhone[1]?></td>
            <td><?=$refOcc[1]?></td>
            <td><?=$refYear[1]?></td>
        </tr>
        <tr>
            <td><?=$refName[2]?></td>
            <td><?=$refPhone[2]?></td>
            <td><?=$refOcc[2]?></td>
            <td><?=$refYear[2]?></td>
        </tr>

        </tbody>
</table>

<hr>

<h3>Miscellaneous</h3>

<p><a href='/apply/process/<?=$id?>/16/true' class='btn btn-mini'><i class='icon-pencil'></i></a></p>
<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Special Skills</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=nl2br($this->session->userdata('skills'))?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>US Military or Naval Service</th>
            <th>Rank</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
            <?php
                $milService = $this->session->userdata('milService');

                echo (empty($milService)) ? '&nbsp;' : $milService;
            ?>
            </td>
            <td><?=$this->session->userdata('rank')?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Present Membership in National Guard or Reserves</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php
                $guard = $this->session->userdata('guard');

                echo (empty($guard)) ? '&nbsp;' : $guard;
                ?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Have you been convicted of a crime other than a traffic offense?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo ($this->session->userdata('convict') == 1) ? "Yes" : "No"; ?></td>
        </tr>
    </tbody>
</table>

<?php if($this->session->userdata('convict') == 1) : ?>
<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>When, where and nature of offense?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=nl2br($this->session->userdata('offense'))?></td>
        </tr>
    </tbody>
</table>
<?php endif; ?>


<div class='form-actions'>
    <button type='button' class='btn btn-primary' onclick="window.location='/apply/finish/<?=$id?>';$(this).attr('disabled', 'disabled');">Finish &amp; Submit</button>
</div>

</div> <!-- .app-review -->
