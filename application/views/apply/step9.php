<?php if(!defined('BASEPATH')) die('Direct access not allowed');

    if($edit == true)
    {
        $checkRelated[$this->session->userdata('related')] = 'checked';
        $checkPrevious[$this->session->userdata('previous')] = 'checked';
    }
?>

<h1>Employment Application</h1>

<h4>Step 9 of 16</h4>

<p class='lead'>Please list any relatives working for <?=$companyName?>. (If applicable)</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>
    <input type='hidden' name='page' id='page' value='<?=$page?>'>
    <input type='hidden' name='edit' id='edit' value='<?=$edit?>'>


<div class="control-group">
    <label class='control-label' for=''>Are you related to or do you know anyone who works for <?=$companyName?>?</label>
    <div class="controls">

    <label class="radio inline">
        <input name="related" type="radio" id="related" value="1" <?=$checkRelated[1]?>/> Yes
    </label>
    <label class="radio inline">
        <input name="related" type="radio" value="0" id="related" <?=$checkRelated[0]?>/> No
    </label>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for='relatedWho'>If yes, who?</label>
    <div class="controls">
        <input type='text' name='relatedWho' id='relatedWho' value='<?=$this->session->userdata('relatedWho')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='relatedWhere'>Where?</label>
    <div class="controls">
        <input type='text' name='relatedWhere' id='relatedWhere' value='<?=$this->session->userdata('relatedWhere')?>'>
    </div>
</div>

<hr>

<div class="control-group">
    <label class='control-label' for=''>Have you ever previously worked for <?=$companyName?>?</label>
    <div class="controls">
        <label class="radio inline">
            <input name="previous" type="radio" id="previous" value="1" <?=$checkPrevious[1]?>/> Yes
        </label>
        <label class="radio inline">
            <input name="previous" type="radio" value="0" id="previous" <?=$checkPrevious[0]?>/> No
        </label>
    </div>
</div>



<div class="control-group">
    <label class='control-label' for='previousFrom'>From</label>
    <div class="controls">
        <input type='text' name='previousFrom' id='previousFrom' value='<?=$this->session->userdata('previousFrom')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='previousTo'>To</label>
    <div class="controls">
        <input type='text' name='previousTo' id='previousTo' value='<?=$this->session->userdata('previousTo')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='previousReason'>Reason for leaving</label>
    <div class="controls">
        <input type='text' name='previousReason' id='previousReason' value='<?=$this->session->userdata('previousReason')?>'>
    </div>
</div>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>

</form>
