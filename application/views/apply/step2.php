<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row'>
    <div class='col-12'>


<h1>Employment Application</h1>

<h4>Step 2 of 16</h4>

<?php
if($_SESSION['country'] == 2) echo "<p class='lead'>Please enter your complete Social Insurance Number.</p>";
else echo "<p class='lead'>Please enter your complete Social Security Number.</p>";
?>


<p>&nbsp;</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<?php
if ($_SESSION['country'] == 2)
{
    $lbl = 'Social Insurance Number';

    $lengths = array (3,3,3);
}
else
{
    $lbl = 'Social Security Number';
    $lengths = array (3,2,4);
}
?>

<div class="form-group">
    <label class='col-4 control-label' for='ssn'><?=$lbl?></label>
    <div class="col-8 controls">
        <input type='text' class='form-control' name='ss[0]' id='ss[0]' maxlength='<?=$lengths[0]?>' style="float:left;width:75px">
        <input type='text' class='form-control' name='ss[1]' id='ss[1]' maxlength='<?=$lengths[1]?>' style="float:left;width:50px">
        <input type='text' class='form-control' name='ss[2]' id='ss[2]' maxlength='<?=$lengths[2]?>' style="float:left;width:80px">
    </div>
</div>

    </div> <!-- .col-12 -->
</div> <!-- .row -->


<div class='row'>
<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>
</div> <!-- .row -->
