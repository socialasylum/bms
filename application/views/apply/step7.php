<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Employment Application</h1>

<h4>Step 7 of 16</h4>

<p class='lead'>Were you referred to <?=$companyName?>?</p>


<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<div class="control-group">
    <label class='control-label' for='referred'>Referred By</label>
    <div class="controls">
        <select name="referred" id="referred">
        <?php
        if(!empty($refer))
        {
            foreach($refer as $r)
            {
                $sel = ($r->code == $this->session->userdata('referred')) ? 'selected' : null;

                echo "<option {$sel} value='{$r->code}'>{$r->display}</option>\n";
            }
        }
        ?>

        </select>
    </div>
</div>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>


</form>
