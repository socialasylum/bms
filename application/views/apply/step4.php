<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Employment Application</h1>

<h4>Step 4 of 16</h4>

<p class='lead'>Please enter all applicable telephone numbers. </p>
<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<div class="control-group">
    <label class='control-label' for='phone'>Telephone Number</label>
    <div class="controls">
        <input type='text' name='phone' id='phone' value='<?=$this->session->userdata('phone')?>'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='mobile'>Mobile Number</label>
    <div class="controls">
        <input type='text' name='mobile' id='mobile' value='<?=$this->session->userdata('mobile')?>'>
    </div>
</div>




<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>



