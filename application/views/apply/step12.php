<?php if(!defined('BASEPATH')) die('Direct access not allowed');

if ($edit == true)
{
    $checkEmployed[$this->session->userdata('employed')] = 'checked';
    $checkContact[$this->session->userdata('contact')] = 'checked';
}
?>

<h1>Employment Application</h1>

<h4>Step 12 of 16</h4>

<p class='lead'>Please choose a desired employment location and tell us about your current employment status. </p>

<p>Please choose only your preferred location. You do not need to apply for each location desired.</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<div class="control-group">
    <label class='control-label' for='desiredState'>Choose State</label>
    <div class="controls">
        <select name='desiredState' id='desiredState' onchange="getStores(null);">
            <option value=''></option>
            <?php
                if(!empty($states))
                {
                    foreach($states as $s)
                    {
                        $sel = ($s->abb == $this->session->userdata('desiredState')) ? "selected" : null;

                        echo "<option {$sel} value='{$s->abb}'>{$s->stateName}</option>\n";
                    }
                }
            ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for=''>Location</label>
    <div id='kioskDisplay' class="controls"><select></select></div>
</div>


<div class="control-group">
    <label class='control-label' for=''>Are you employed now?</label>
    <div class="controls">
        <label class="radio inline">
            <input name="employed" type="radio" id="employed_1" value="1" <?=$checkEmployed[1]?> /> Yes
        </label>
        <label class="radio inline">
            <input name="employed" type="radio" value="0" id="employed_0" <?=$checkEmployed[0]?>/> No
        </label>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for=''>May we contact your present employer?</label>
    <div class="controls">
        <label class="radio inline">
            <input name="contact" type="radio" id="contact_1" value="1" <?=$checkContact[1]?>/> Yes
        </label>
        <label class="radio inline">
            <input name="contact" type="radio" value="0" id="contact_0" <?=$checkContact[0]?>/> No
        </label>
    </div>
</div>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>
</form>
