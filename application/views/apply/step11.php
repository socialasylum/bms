<?php if(!defined('BASEPATH')) die('Direct access not allowed');

    if($edit == true)
    {
        $checkMonAM[$this->session->userdata('monAM')] = 'checked';
        $checkTueAM[$this->session->userdata('tueAM')] = 'checked';
        $checkWedAM[$this->session->userdata('wedAM')] = 'checked';
        $checkThuAM[$this->session->userdata('thuAM')] = 'checked';
        $checkFriAM[$this->session->userdata('friAM')] = 'checked';
        $checkSatAM[$this->session->userdata('satAM')] = 'checked';
        $checkSunAM[$this->session->userdata('sunAM')] = 'checked';

        $checkMonPM[$this->session->userdata('monPM')] = 'checked';
        $checkTuePM[$this->session->userdata('tuePM')] = 'checked';
        $checkWedPM[$this->session->userdata('wedPM')] = 'checked';
        $checkThuPM[$this->session->userdata('thuPM')] = 'checked';
        $checkFriPM[$this->session->userdata('friPM')] = 'checked';
        $checkSatPM[$this->session->userdata('satPM')] = 'checked';
        $checkSunPM[$this->session->userdata('sunPM')] = 'checked';
    }
?>

<h1>Employment Application</h1>

<h4>Step 11 of 16</h4>

<p class='lead'>Please select the times of each weekday that you're available to work.</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>


<table class='table table-bordered'>
    <thead>
        <tr>
            <th align='right'>&nbsp;</th>
            <th align='center'>Monday</th>
            <th align='center'>Tuesday</th>
            <th align='center'>Wednesday</th>
            <th align='center'>Thursday</th>
            <th align='center'>Friday</th>
            <th align='center'>Saturday</th>
            <th align='center'>Sunday</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align='right'><b>AM</b></td>
            <td align='center'><input type='checkbox' name='monAM' id='monAM' value='1' <?=$checkMonAM[1]?>></td>
            <td align='center'><input type='checkbox' name='tueAM' id='tueAM' value='1' <?=$checkTueAM[1]?>></td>
            <td align='center'><input type='checkbox' name='wedAM' id='wedAM' value='1' <?=$checkWedAM[1]?>></td>
            <td align='center'><input type='checkbox' name='thuAM' id='thuAM' value='1' <?=$checkThuAM[1]?>></td>
            <td align='center'><input type='checkbox' name='friAM' id='friAM' value='1' <?=$checkFriAM[1]?>></td>
            <td align='center'><input type='checkbox' name='satAM' id='satAM' value='1' <?=$checkSatAM[1]?>></td>
            <td align='center'><input type='checkbox' name='sunAM' id='sunAM' value='1' <?=$checkSunAM[1]?>></td>
        </tr>
        <tr>
            <td align='right'><b>PM</b></td>
            <td align='center'><input type='checkbox' name='monPM' id='monPM' value='1' <?=$checkMonPM[1]?>></td>
            <td align='center'><input type='checkbox' name='tuePM' id='tuePM' value='1' <?=$checkTuePM[1]?>></td>
            <td align='center'><input type='checkbox' name='wedPM' id='wedPM' value='1' <?=$checkWedPM[1]?>></td>
            <td align='center'><input type='checkbox' name='thuPM' id='thuPM' value='1' <?=$checkThuPM[1]?>></td>
            <td align='center'><input type='checkbox' name='friPM' id='friPM' value='1' <?=$checkFriPM[1]?>></td>
            <td align='center'><input type='checkbox' name='satPM' id='satPM' value='1' <?=$checkSatPM[1]?>></td>
            <td align='center'><input type='checkbox' name='sunPM' id='sunPM' value='1' <?=$checkSunPM[1]?>></td>
        </tr>
    </tbody>
</table>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>



</form>
