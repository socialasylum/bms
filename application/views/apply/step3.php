<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Employment Application</h1>

<h4>Step 3 of 16</h4>

<p>Please enter your complete mailing address. </p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<div class="control-group">
    <label class="control-label" for="address">Address</label>
    <div class="controls">
        <input type='text' class='input-xxlarge' name='address' id='address'  value='<?=$this->session->userdata('address')?>'>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="city">City</label>
    <div class="controls">
        <input type='text' name='city' id='city' value='<?=$this->session->userdata('city')?>'>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="state">State</label>
    <div class="controls">
        <select class='textinput' name='state' id='state' >
        <?php
            if(!empty($states))
            {
                foreach($states as $abb => $name)
                {
                    $sel = ($abb == $this->session->userdata('state')) ? "selected" : null;

                    echo "<option {$sel} value='{$abb}'>{$name}</option>\n";
                }
            }
        ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='postalCode'>Postal Code</label>
    <div class="controls">
<input type='text' class='textinput' name='postalCode' id='postalCode' value='<?=$this->session->userdata('postalCode')?>'>
    </div>
</div>


<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>


