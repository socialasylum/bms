<?php if(!defined('BASEPATH')) die('Direct access not allowed');

    if($edit == true)
    {
        $checkUSA[$this->session->userdata('workUsa')] = 'checked';
    }
?>

<h1>Employment Application</h1>

<h4>Step 8 of 16</h4>

<p class='lead'>Please select yes or no. </p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<?php
    if($this->session->userdata('country') == 2) $text = "Are you eligile to work in the the Canada?";
    else $text = "Are you eligile to work in the the United States?";
?>

    <p><?=$text?></p>
    <p><small>(If offered employment, you will be required to provide documentation to verify eligibility.)</small></p>

    <label class="radio inline">
        <input name="workUsa" type="radio" id="radiobutton" value="1" <?=$checkUSA[1]?> /> Yes
    </label>
    <label class="radio inline">
        <input name="workUsa" type="radio" value="0" id="radio" <?=$checkUSA[0]?>/> No
    </label>



<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>

</form>
