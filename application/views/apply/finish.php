<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Finish &amp; Submit</h1>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>

<p class='lead'>Please read the following disclaimer and check the bottom box to agree and submit your application.</p>

<h4>DECLARATION OF ACCURATE INFORMATION</h4>

<p>
I agree to comply with all the rules of <?=$companyName?> I hereby affirm and declare that all the foregoing statements are true and correct, 
and that I have not knowingly withheld any fact that would, if disclosed, affect my application unfavorably.  
I understand that the falsification of any information provided to <?=$companyName?>, 
including that contained in my employment application, or the failure to accurately disclose 
information requested will result in a decision not to hire/or termination of employment.<br />
</p>

<p>&nbsp;</p>

<h4>APPLICANT AUTHORIZATION OF BACKGROUND INVESTIGATION</h4>

<p>I hereby authorize <?=$companyName?>, by my signature below provide irrevocable consent for an 
investigation to be conducted and investigative report(s) and/or consumer report(s) to be procured as allowed by law.
The investigation shall include but not be limited to credit reports, educational history, employment history, public records 	
including criminal convictions, interviews and other sources of information as allowable by law.  If an adverse action 
will affect me from the results of the consumer or investigative report, I will first be provided with a copy of the report  
and a summary of my rights under the fair credit report act. This authorization expires 90 days after my departure from 
employment with <?=$companyName?> I hereby unconditionally release <?=$companyName?>
and any named or unnamed informants from any and all liability resulting from the furnishing of this information.  
I have read and understand the above statements.</p>

<p>&nbsp;</p>

<h4>APPLICANT AUTHORIZATION OF PRE-EMPLOYMENT SCREENING(S)</h4>

<p>
All applicants are required to complete the IntegriView Applicant Review, as allowable by law.  All managerial of	
corporate applicants are required the Predictive Index survey, as allowable by law.
</p>

<p>&nbsp;</p>

<h4>APPLICANT ACKNOWLEDGEMENT</h4>
<p>This employment application does not create an employment contract.  If applicable in your state, employment-at-will
statements prevent applicants from concluding the application or any statements made during the hiring process from
an employment contract.
</p>

<hr>

<label class='checkbox inline'>
<input type='checkbox' name='agree' id='agree' value='1' onclick="checkAgree();">
I Agree that all this information is true, and none is falsified.
</label>


<div class='form-actions'>

    <button type='button' class='btn btn-primary' id='submitBtn' disabled='disabled'>Submit Application</button>

    <div class='pull-right'>
        <dl class='dl-horizontal'>
            <dt>Date</dt>
            <dd><?=date("Y-m-d")?></dd>
        </dl>
    </div>
</div>
