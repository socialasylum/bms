<?php if(!defined('BASEPATH')) die('Direct access not allowed');

    if($edit == true)
    {
        $check18[$this->session->userdata('is18')] = 'checked';
    }
?>

<h1>Employment Application</h2>

<h4>Step 5 of 16</h4>

<p class='lead'>Please confirm that you are of age. </p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<p>Are you 18 years of age or older?</p>

<label class="radio inline">
    <input name="is18" type="radio" id="radiobutton" value="1" <?=$check18[1]?>/> Yes
</label>
<label class="radio inline">
    <input name="is18" type="radio" value="0" id="radio" <?=$check18[0]?>/> No
</label>

<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>
