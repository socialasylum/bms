<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
    $schoolCnt = $this->session->userdata('schoolCnt');

    $schoolCnt = (empty($schoolCnt)) ? '0' : $schoolCnt;
?>
<h1>Employment Application</h1>

<h4>Step 13 of 16</h4>

<p class='lead'>Please tell us about your history of education.</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post'
        );

echo form_open('/apply/save', $attr);
?>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>
    <input type='hidden' name='page' id='page' value='<?=$page?>'>
    <input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

    <input type='hidden' name='schoolCnt' id='schoolCnt' value='<?=$schoolCnt?>'>


<?php
if ($schoolCnt > 0)
{
    for ($i = 0; $i <= ($schoolCnt - 1); $i++)
    {


        try
        {
            $data['cnt'] = $i;
            $data['types'] = $this->functions->getCodes(4, 'code');
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo $this->load->view('apply/addEdu', $data, true);
    }
}
?>

<div id='edu-display-1'></div>

<div class='form-actions'>
    <button type='button' class='btn' onclick="apply.addEdu();"><i class='icon-plus'></i> Add More Education</button>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>


</form>
