<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Confirmation</h1>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>

<p class='lead'>Thank you, your application has been received.</p>

<p>Click the button below to return to the job listings page.</p>

<div class='form-actions'>
    <button type='button' class='btn btn-primary' name='returnBtn' id='returnBtn'>&laquo; Return to Job Listings</button>
</div>
