<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row'>
    <div class='col-lg-6'>
<h3>Search</h3>
<p>Enter your location to search for open positions near you!</p>
    </div>

    <div class='col-lg-6'>
        <div class='input-group pull-right'>

            <input type='text' class='form-control'  name='location' id='location' value="" placeholder='Las Vegas, NV'>
            <span class='input-group-btn'>
                <button type='button' class='btn btn-info' name='searchBtn' id='searchBtn'><i class='icon-search'></i></button>
            </span>
        </div>
    </div>

</div>

<hr>

<?php
if (empty($openPositions)) : echo $this->alerts->info("There are no open positions available.");
else:
?>

<table class='table table-stripped table-bordered'>
    <thead>
        <tr>
            <th>Position</th>
            <th>Location</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php
    foreach ($openPositions as $k => $r)
    {

        $positionDisplay = $locationDisplay = null;

        try
        {
            if (empty($r->positionId)) $positionDisplay = $r->title;
            else $positionDisplay = $this->positions->getName($r->positionId);

            if (empty($r->locationId)) $locationDisplay = $r->locationTxt;
                else $locationDisplay = $this->locations->getTableValue('name', $r->locationId);


            $startDate = $this->users->convertTimezone($r->userid, $r->startDate, "m/d/Y");
            $endDate = $this->users->convertTimezone($r->userid, $r->endDate, "m/d/Y");


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "<td>{$positionDisplay}</td>" . PHP_EOL;
        echo "<td>{$locationDisplay}</td>" . PHP_EOL;
        echo "<td>{$startDate}</td>" . PHP_EOL;
        echo "<td>{$endDate}</td>" . PHP_EOL;
        echo "\t<td><a href='/apply/process/{$r->id}/1' class='btn btn-link pull-right'>Apply</a></td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }
?>
    </tbody>
</table>



<?php endif ?>
