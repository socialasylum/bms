<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
    $from = $this->session->userdata('from');
    $to = $this->session->userdata('to');
    $companyName = $this->session->userdata('companyName');
    $companyAddress = $this->session->userdata('companyAddress');
    $companyPhone = $this->session->userdata('companyPhone');
    $salary = $this->session->userdata('salary');
    $position = $this->session->userdata('position');
    $reason = $this->session->userdata('reason');

?>

<div class='well form-horizontal' id='job-wrapper-<?=$cnt?>'>

    <button type='button' class='close' onclick="apply.removeJob(<?=$cnt?>);">&times;</button>
<h4>Employment Information</h4>

<p>Please enter the information about your previous employment.</p>


<div class="control-group">
    <label class='control-label' for='from'>From</label>
    <div class="controls">
            <input type='text' name='from[]' id='from_<?=$cnt?>' value="<?=$from[$cnt]?>" placeholder='2011-11-03'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='To'>To</label>
    <div class="controls">
        <input type='text' name='to[]' id='to_<?=$cnt?>' value="<?=$to[$cnt]?>" placeholder='2012-11-03'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='companyName'>Company Name</label>
    <div class="controls">
        <input type='text' name='companyName[]' id='companyName_<?=$cnt?>' value="<?=$companyName[$cnt]?>" placeholder=''>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='address'>Address</label>
    <div class="controls">
        <textarea name='companyAddress[]' id='companyAddress_<?=$cnt?>'><?=$companyAddress[$cnt]?></textarea>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='Phone'>Phone</label>
    <div class="controls">
        <input type='text' name='companyPhone[]' id='companyPhone_<?=$cnt?>' value="<?=$companyPhone[$cnt]?>" placeholder='(702) 555-1212'>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='salary'>Salary</label>
    <div class="controls">
        <input type='text' name='salary[]' id='salary_<?=$cnt?>' value="<?=$salary[$cnt]?>" placeholder=''>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='position'>Position</label>
    <div class="controls">
        <input type='text' name='position[]' id='position_<?=$cnt?>' value="<?=$position[$cnt]?>" placeholder=''>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='reason'>Reason for Leaving</label>
    <div class="controls">
        <textarea name='reason[]' id='reason_<?=$cnt?>' class='input-block-level'><?=$reason[$cnt]?></textarea>
    </div>
</div>

</div> <!-- .well -->

<div id='job-display-<?=($cnt + 1)?>'></div>
