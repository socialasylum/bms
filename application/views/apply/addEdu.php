<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
    $type = $this->session->userdata('type');
    $desc = $this->session->userdata('desc');
    $years = $this->session->userdata('years');
    $grad = $this->session->userdata('grad');
    $studied = $this->session->userdata('studied');


    $checked[$grad[$cnt]] ='checked';

?>

<div class='well form-horizontal' id='edu-wrapper-<?=$cnt?>'>

    <button type='button' class='close' onclick="apply.removeEdu(<?=$cnt?>);">&times;</button>
<h4>Education Information</h4>

<p>Please enter the information about this educational institution.</p>


<div class="control-group">

    <label class='control-label' for='type'>Type</label>
    <div class="controls">

            <select name="type[]" id="type_<?=$cnt?>">
                <option value=""></option>
<?php
if (!empty($types))
{
    foreach ($types as $k => $r)
    {
        $sel = ($r->code == $type[$cnt]) ? 'selected' : null;

        echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
            </select>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for='desc'>Name &amp; Location</label>
    <div class="controls">
        <textarea name='desc[]' id='desc_<?=$cnt?>' class='input-block-level' placehoder="Name &amp; Location"><?=$desc[$cnt]?></textarea>
    </div>
</div>

<div class="control-group">
    <label class='control-label' for='years'># Years</label>
    <div class="controls">
    <input type='text' class='input-mini' name='years[]' id='years_<?=$cnt?>' value="<?=$years[$cnt]?>" placeholder='4'>
    </div>
</div>


<div class="control-group">
    <label class='control-label' for='grad'>Graduate?</label>
    <div class="controls">
        <label class='radio'>
            <input type='radio' name='grad[]' id='grad_<?=$cnt?>_1' value="1" <?=$checked[1]?>> Yes
        </label>
        <label class='radio'>
            <input type='radio' name='grad[]' id='grad_<?=$cnt?>_0' value="0" <?=$checked[0]?>> No
        </label>


    </div>
</div>


<div class="control-group">
    <label class='control-label' for='subjects'>Subjects Studied</label>
    <div class="controls">
        <input type='text' name='studied[]' id='studied_<?=$cnt?>' class='input-block-level' value="<?=$studied[$cnt]?>" placeholder='General Education'>
    </div>
</div>
    <div class='clearfix'></div>
</div> <!-- .well -->


<div id='edu-display-<?=($cnt + 1)?>'></div>
