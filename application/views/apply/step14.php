<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
    $jobCnt = $this->session->userdata('jobCnt');

    $jobCnt = (empty($jobCnt)) ? '0' : $jobCnt;
?>

<h1>Employment Application</h1>

<h4>Step 14 of 16</h4>

<p class='lead'>Please list below your last 3 years of employment history, starting with the most recent first. </p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<input type='hidden' name='jobCnt' id='jobCnt' value='<?=$jobCnt?>'>

<?php
if ($jobCnt > 0)
{
    for ($i = 0; $i <= ($jobCnt - 1); $i++)
    {
        $data = array
            (
                'cnt' => $i
            );

        echo $this->load->view('apply/addJob', $data, true);
    }
}
?>

<div id='job-display-1'></div>

<div class='form-actions'>
    <button type='button' class='btn' onclick="apply.addJob();"><i class='icon-plus'></i> Add Employment History</button>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>


</form>
