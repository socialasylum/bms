<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row'>
    <div class='col-12'>

<h1>Employment Application</h1>

<h4>Step 1 of 16</h4>

<p class='lead'>Please enter your full name.</p>


<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open('/apply/save', $attr);
?>

<input type='hidden' name='id' id='id' value='<?=$id?>'>

<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>


<div class="form-group">
    <label class='col-3 control-label' for='firstName'>First Name</label>
    <div class="col-9 controls">
        <input type='text' name='firstName' id='firstName' class='form-control' value='<?=$this->session->userdata('firstName')?>'>
    </div>
</div>

<div class="form-group">
    <label class='col-3 control-label' for='lastName'>Last Name</label>
    <div class="col-9 controls">
            <input type='text' class='form-control' name='lastName' id='lastName' value='<?=$this->session->userdata('lastName')?>'>
    </div>
</div>


<div class="form-group">
    <label class='col-3 control-label' for='MI'>Middle Inital</label>
    <div class="col-9 controls">
        <input type='text' class='form-control' name='MI' id='MI' value='<?=$this->session->userdata('MI')?>' maxlength='1'>
    </div>
</div>

    </div>

</div> <!-- .row -->

<div class='row'>
<div class='col-12 form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>
</div> <!-- row -->



