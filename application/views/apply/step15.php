<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
    $refName = $this->session->userdata('refName');
    $refPhone = $this->session->userdata('refPhone');
    $refOcc = $this->session->userdata('refOcc');
    $refYear = $this->session->userdata('refYear');
?>

<h1>Employment Application</h1>

<h4>Step 15 of 16</h4>

<p class='lead'>Please give the names of three persons not related to you, whom you have known at least one year.</p>

<?php
    $attr = array
        (
            'name' => 'appForm',
            'id' => 'appForm',
            'method' => 'post'
        );

echo form_open('/apply/save', $attr);
?>
<input type='hidden' name='id' id='id' value='<?=$id?>'>
<input type='hidden' name='page' id='page' value='<?=$page?>'>
<input type='hidden' name='edit' id='edit' value='<?=$edit?>'>

<table class='table table-bordered'>
    <thead>
        <tr>
            <th>Name</th>
            <th>Phone Number </th>
            <th>Business/Occupation</th>
            <th>Years Acquainted</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><input type='text' class='textinput' name='refName[0]' value='<?=$refName[0]?>'></td>
            <td><input type='text' class='textinput' name='refPhone[0]' value='<?=$refPhone[0]?>'></td>
            <td><input type='text' class='textinput' name='refOcc[0]' value='<?=$refOcc[0]?>'></td>
            <td><input type='text' class='textinput' name='refYear[0]' style="width:50px;" value='<?=$refYear[0]?>'></td>
        </tr>
        <tr>
            <td><input type='text' class='textinput' name='refName[1]' value='<?=$refName[1]?>'></td>
            <td><input type='text' class='textinput' name='refPhone[1]' value='<?=$refPhone[1]?>'></td>
            <td><input type='text' class='textinput' name='refOcc[1]' value='<?=$refOcc[1]?>'></td>
            <td><input type='text' class='textinput' name='refYear[1]' style="width:50px;" value='<?=$refYear[1]?>'></td>
        </tr>
        <tr>
            <td><input type='text' class='textinput' name='refName[2]' value='<?=$refName[2]?>'></td>
            <td><input type='text' class='textinput' name='refPhone[2]' value='<?=$refPhone[2]?>'></td>
            <td><input type='text' class='textinput' name='refOcc[2]' value='<?=$refOcc[2]?>'></td>
            <td><input type='text' class='textinput' name='refYear[2]' style="width:50px;" value='<?=$refYear[2]?>'></td>
        </tr>
    </tbody>
</table>


<div class='form-actions'>
    <button type='submit' class='btn btn-primary' onclick="this.form.submit();$(this).attr('disabled', 'disabled');">Continue &raquo;</button>
</div>





</form>
