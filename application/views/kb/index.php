<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-question-circle'></i> Knowledge Base</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><a href='/kb/edit'><i class='fa fa-file-text'></i> New Article</a></li>
        <li><a href='javascript:void(0);' id="createFolderBtn"><i class='fa fa-folder-open'></i> New Folder</a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<input type='hidden' id='rootFolder' value='<?=$rootFolder?>'>
<input type='hidden' id='folder' value='<?=$folder?>'>

<div id='kbArticle-container'>
<?php
if (empty($content))
{
    echo $this->alerts->info("This folder is currently empty and contains no knowledge base articles!");
}
else
{

    // echo "<div class='row-fluid'>\n";


    echo "<ol id='fileList'>\n";

    if (!empty($folder))
    {
        try
        {
            if (!empty($parentFolder)) $parentFolderName = $this->folders->getTableValue('name', $parentFolder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "\t<li class='ui-state-default folder' value='{$parentFolder}' itemType='1' id='folder_{$parentFolder}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>../{$parentFolderName}</div></li>\n";
    }


    $rcnt = 1;
    foreach ($content as $r)
    {
        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        if ($r->type == 1)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} folder' value='{$r->id}' itemType='1' id='folder_{$r->id}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>{$name}</div></li>\n";
        }
        elseif ($r->type == 2)
        {
            // $docType = $r->policyType;

            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} kbArticle' value='{$r->id}' itemType='2' id='item_{$r->id}' tbl='kbArticle' col='articleId'><i class='fa fa-file-text drag-icon'></i><div class='docName'>{$name}</div></li>\n";
        }

    }

    echo "</ol>\n";

}
?>
</div> <!-- #doc-container -->
