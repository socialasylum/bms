<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><?=$headerIcon?><?=$info->title?></h1>

<div class='col-lg-12 col-m-12 col-s-12 col-xs-12'>
    
<?php
if (!empty($sections))
{
    echo "<div class='row'><div class='col-lg-3 col-m-3 col-s-3 col-xs-12 well'>" . PHP_EOL;
        
    foreach ($sections as $r)
    {
        echo "<p><a href='#section_{$r->id}'>{$r->sectionHeading}</a></p>";
    }
    echo "</div> <!-- .well -->" . PHP_EOL;
    echo "</div> <!-- .row -->" . PHP_EOL;

    foreach ($sections as $r)
    {
        echo "<div class='row'>" . PHP_EOL;
        echo "<a name='section_{$r->id}'></a>" . PHP_EOL;
        echo "<div class='page-header'><h3>{$r->sectionHeading}</h3></div>";

        echo $r->body;

        echo "</div>" . PHP_EOL;
    }
}

if (!empty($info->module))
{
    try
    {
        $moduleName = $this->modules->getTableValue('name', $info->module);

        $moduleUrl = $this->modules->getTableValue('url', $info->module);

        echo "<hr>";
        echo "<a href='{$moduleUrl}' class='btn btn-info'>Return to {$moduleName}</a>";
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

}

?>

</div> <!-- col-12 -->
