<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Knowledge Base Article</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
            <li><a href="javascript:void(0);" id='addSectionBtn'><i class='fa fa-plus'></i> Add Section</a></li>
            <li><a href="javascript:void(0);" id='settingsBtn'><i class='fa fa-cog'></i> Settings</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm'
        );

echo form_open('#', $attr);


if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

?>

<input type='hidden' name='folder' id='folder' value='<?=$folder?>'>


<div class='row announce-subject'>
    <div class='col-lg-12 col-m-12 col-s-12 col-xs-12'>
        <input type='text' class='form-control' name='title' id='title' value="<?=$info->title?>" placeholder='Title'>
    </div> <!-- .col-12 -->
</div> <!-- .row -->


<ul id='section-container' class='formSort'>
<!-- <div id='section-container'> -->

<?php

    $cnt = count($sections);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++) 
    {
?>

<li>
<div class='row' id='section-wrapper-<?=$i?>'>
    <div class='col-lg-12 col-m-12 col-s-12 col-xs-12'>
        <div class='panel panel-default'>
            <div class='panel-heading'>
                <input type='hidden' name='sectionCnt[]' id='sectionCnt' value='<?=$i?>'>
                <input type='hidden' name='sectionId[]' id='sectionId' value='<?=(empty($sections[$i]->id)) ? 0 : $sections[$i]->id?>'>
                <input type='hidden' name='sectionHeading[]' id='sectionHeading' value="<?=$sections[$i]->sectionHeading?>">
                <h3 class='panel-title'><?=(empty($sections[$i]->sectionHeading)) ? 'Section Heading' : $sections[$i]->sectionHeading?></h3>
            </div> <!-- .panel-heading -->
            
            <div class='panel-body'>
<?php

        include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php';

        echo <<< EOS
    <ul class="nav navbar-nav">
        <li><a href="javascript:void(0);" id='sectionSettingsBtn' onclick="kb.getSectionHeadingProperties(this);"><i class='fa fa-cog'></i> Section Settings</a></li>
    </ul>


    <ul class="nav navbar-nav pull-right">
            <li><a href="javascript:void(0);" id='deleteHeaderBtn' value='{$r->id}' onclick="kb.deleteSection(this);"><i class='fa fa-trash-o danger'></i> Delete Section</a></li>
    </ul>

EOS;

        include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php';
?>


                <textarea class='form-control' rows='5' name='body[]' id='body_<?=$i?>'><?=$sections[$i]->body?></textarea>
            </div> <!-- .panel-body -->
        </div> <!-- .panel -->

    </div> <!-- .col-12 -->
</div> <!-- .row -->
</li>
<?php
} // close of for loop
?>

</ul>
<!--</div>--> <!-- #section-container -->

<!-- main settings modal //-->
<div id='mainSettingsModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-gear'></i> Article Settings</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='settingsAlert'></div>

        <p class='lead'>Below are the main settings for the article.</p>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='active'>Status</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <select name='active' id='active' class='form-control'>
        <?php
            foreach($status as $code => $display)
            {
                // new documents, sets to active by default
                if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                else $sel = ($info->active == $code) ? 'selected' : null;

                echo "<option {$sel} value='{$code}'>{$display}</option>\n";
            }
?>
        </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


    <input type='hidden' name='module' id='module' value='0'>

<?php if ($this->users->isAdmin() === true) : ?>
        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='module'>Module</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="module" id="module" class='form-control'>
                    <option value="0"></option>
<?php
if (!empty($modules))
{
    foreach ($modules as $r)
    {
        if ($r->id !== $info->module)
        {
            try
            {
                $assigned = $this->kb->checkModuleAssigned($r->id);

                // module is assigned, cannot assign to another kb aritcle, continues to next
                if ($assigned == true) continue;
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
        }

        $sel = ($r->id == $info->module) ? "selected='selected'" : null;

        echo "<option {$sel} value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
    }
}
?>
                </select>
                <span class='help-block'>You can link this article as the main article for a paticular module.</span>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='globalArticle'>Global Article</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <label class='checkbox'>
                    <input type='hidden' name='globalArticle' value='0'>
<?php
$globalArticleChecked = ($info->globalArticle == 1) ? "checked='checked'" : null;
?>
                    <input type='checkbox' name='globalArticle' id='globalArticle' value='1' <?=$globalArticleChecked?>>
                </label>

                <?php if ($this->session->userdata('company') == 1) : ?>
                    <span class='help-block'><small>Will post on main page in <strong>Documentation</strong></small></span>
                <?php endif; ?>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

<?php endif; ?>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-info' data-dismiss='modal' aria-hidden='true'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



</form>


<div id='sectionPropModal' class='modal fade'></div>
