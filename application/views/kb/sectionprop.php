<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-cog'></i> Section Settings</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='secSettingsAlert'></div>

        <p class='lead'>Below are the main settings for the article.</p>


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='sectionPropHeading'>Heading</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='sectionPropHeading' id='sectionPropHeading' value="<?=$heading?>">
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-info' data-dismiss='modal' aria-hidden='true'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
