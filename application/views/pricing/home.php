<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <h1>Select Modules</h1>

    <p class='lead'><strong>Step 1</strong> select which modules you want to include in your Business Management System.</p>


    <input type='hidden' name='users' id='users' value='5'>

<?php

echo "<div id='mod-container-list'>" . PHP_EOL;
if (!empty($modules))
{
    $rcnt = 1;

    foreach ($modules as $r)
    {

        if ($rcnt == 1) echo "<div class='row'>" . PHP_EOL;


        echo "<div class='col-lg-3 col-m-3 col-s-3 col-xs-3'>";
        echo "<div class='well package-sections' id='module_{$r->id}' ppu='{$r->price}' value='{$r->id}'  onclick=\"pricing.selectModule($(this), {$r->id});\">";

        echo "<h2 class='text-info'>";

        if (!empty($r->icon)) echo "<i class='{$r->icon} drag-icon'></i><br>";

        echo "{$r->name}</h2>" . PHP_EOL;

        if (!empty($r->description)) echo $r->description;


        // echo "<hr>";


        echo "<div align='center'>";
        // echo "<h3>$" . number_format($r->price, 2) . "/user</h3>";

        // echo "<button class='btn  btn-large'>Select</button>";

        echo "</div>";


        echo "</div>"; // .well
        echo "</div>" . PHP_EOL; // .span3

        if ($rcnt >= 4)
        {
            echo "</div>" . PHP_EOL;
            // echo "<div class='row'>" . PHP_EOL;

            $rcnt = 1;
        }
        else
        {
            $rcnt++;
        }
    }

    if ($rcnt > 1 && $rcnt < 4) echo "</div>" . PHP_EOL; // .row if ends with less than 4 modules
}
echo "</div>";
?>


    <hr>

    <h1>Select Number of Users</h1>

    <p class='lead'><strong>Step 2</strong> select how many users your Business Management System will require.</p>


    <div class="row">

    <div class="col-lg-12">


<h3><span id='numUsers'>5</span> Users</h3>

        <!-- <p>Select amount of users you will require.</p> -->
        <div id="slider"></div><p>for more than 100 users, please contact us</p>
    </div>
</div> <!-- /.row -->

<div class='row' align='center'><h1>$<span id='est-display'>100.00</span> / Month </h1></div>

<hr>


<div align='center'>
    <button type='button' class='btn btn-primary btn-lg' name='signUpBtn' id='signUpBtn'>Continue &amp; Sign up &raquo;</button>
    <!-- <a href='/signup' class='btn btn-primary btn-large input-large' id='betaBtn'>Sign up for Beta!</a> -->
</div>





<div class='panel panel-default' style="margin-top:20px;">
    <div class='panel-heading'>Included Modules</div>

    <div class='panel-body'>
    <p class='lead'>Below is a list of all the included modules that every company receives when signing up.</p>

    <p><label class='label label-info'>Bonus</label> All companies will automatically receive any new included modules when they are released!</p>


<?php

echo "<div class='btn-controls' id='unvModContainer'>" . PHP_EOL;
if (!empty($unvModules))
{
    $rcnt = 1;

    foreach ($unvModules as $r)
    {

        $content = $title = null;

        if ($rcnt == 1) echo "<div class='row-fluid'>" . PHP_EOL;

        $shortDesc = null;

        if (!empty($r->shortDesc)) $shortDesc = "<p>" . nl2br($r->shortDesc) . "</p>";

        $content = "{$shortDesc}<a href='#' class='btn btn-xs btn-warning'><i class='fa fa-exclamation-circle'></i> More Information</a>";

        $title = ((empty($r->icon)) ? null : "<i class='{$r->icon}'></i> ") . $r->name;

        echo "<a href='javascript:void(0);' id='popover_{$r->id}' class='btn btn-info btn-medium btn-module' data-toggle=\"popover\" title=\"{$title}\" data-content=\"{$content}\" role='button'><i class='{$r->icon}'></i><b> {$r->name}</b></a>";

        if ($rcnt >= 6)
        {
            echo "</div>" . PHP_EOL;

            $rcnt = 1;
        }
        else
        {
            $rcnt++;
        }
    }

        if ($rcnt > 1 && $rcnt < 6) echo "</div>" . PHP_EOL; // .row if ends with less than 6 modules
}
echo "</div>";
?>
    </div> <!-- .panel-body -->

</div> <!-- .panel -->


