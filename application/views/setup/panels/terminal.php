<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='panel panel-default setupProgress'>
	<div class='panel-heading'><h3 class='panel-title'><i class='fa fa-terminal'></i> Terminal</h3></div>
	<div class='panel-body'>
		<div id='console-output'></div>
	</div>
	
	<div class='panel-footer'>
		<div class='input-group'>
			<span class='input-group-addon'>$</span>
			<input type='text' class='form-control' id='consoleInput' name='consoleInput' value=''>
		</div>
	</div> <!-- /.panel-body -->
</div> <!-- /.panel -->