<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='panel panel-default' id='infoPanel'>
	<div class='panel-heading'><h3 class='panel-title'><i class='fa fa-user'></i> Admin User &amp; Company Information</h3></div>
	<div class='panel-body'>

		<div class='form-group'>
			<label class='control-label'>First Name</label>
			<input type='text' class='form-control' name='firstName' id='firstName' value='' required data-msg='Please enter your first name' placeholder="Audrey">
		</div>
		
		<div class='form-group'>
			<label class='control-label'>Last Name</label>
			<input type='text' class='form-control' name='lastName' id='lastName' value='' required data-msg='Please enter your last name' placeholder="Smith">
		</div>
		
		<div class='form-group'>
			<label class='control-label'>Position</label>
			<input type='text' class='form-control' name='position' id='position' value='' required data-msg='Please enter your position' placeholder="CXO">
		</div>	
		
		<div class='form-group'>
			<label class='control-label'>Timezone</label>
			<select name='timezone' id='timezone' class='form-control' data-width='100%' required data-msg="Please select your timezone!">
					<option value=''></option>
		            <?php
		            if (!empty($timezones))
		            {
		                foreach ($timezones as $zone => $abb)
		                {
		                    $sel = ($info->timezone == $zone) ? 'selected' : null;
		
		                    echo "<option {$sel} value=\"{$zone}\">{$abb}</option>" . PHP_EOL;
		                }
		            }
		            ?>
			</select>
		</div>
		
	</div> <!-- /.panel-body -->
</div> <!-- /.panel -->