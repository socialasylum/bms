<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='panel panel-default' id='dbPanel'>
	<div class='panel-heading'><h3 class='panel-title'><i class='fa fa-database'></i> Database</h3></div>
	<div class='panel-body'>
		<p>Enter your database credentials here. Note that the system will scan table names only (not structure). 
		If there is a match, the table will <b>not</b> be dropped and re-created.</p>

		<div class='form-group'>
			<label class='control-label'>Host</label>
			<input type='text' class='form-control' name='host' id='host' value='' required data-msg='Please enter the database host server.' placeholder="localhost">
		</div>
		
		<div class='form-group'>
			<label class='control-label'>Database</label>
			<input type='text' class='form-control' name='database' id='database' value='' required data-msg='Please enter the name of the database' placeholder="BMS">
		
			<span class='help-block'>
				<div class='checkbox'>
					<label>
						<input type='checkbox' name='createDB' id='createDB' value='1' checked='checked'> Attempt to create database if it does not exist
					</label>
				</div>
			</span>
		</div>
		
		<div class='form-group'>
			<label class='control-label'>Username</label>
			<input type='text' class='form-control' name='username' id='username' value='' requried data-msg='Please enter the database username.' placeholder="root">
		</div>
		
		<div class='form-group'>
			<label class='control-label'>Password</label>
			<input type='password' class='form-control' name='password' id='password' value='' required data-msg='Please enter the database password.' placeholder="">
		</div>
	</div>
</div>
