<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-wrench'></i> System Setup</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><button type='button' id='runBtn' class='btn btn-link navbar-btn'><i class='fa fa-cog'></i> Run Setup</button></li>

    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<p class="lead">Please enter the information below to setup the system and database.</p>

<?php
	$attr = array(
		'id' => 'setupForm',
		'name' => 'setupForm'
	);
	
	echo form_open('#', $attr);
?>

	<div class='row'>
	
		<div class='col-xs-12 col-sm-6'>
			<?php include_once 'panels/database.php'; ?>
			<?php include_once 'panels/info.php'; ?>
		</div> <!-- /.col -->
	
		<div class='col-xs-12 col-sm-6'>
			<?php include_once 'panels/terminal.php'; ?>
		</div> <!-- /.col -->
		
	</div> <!-- /.row -->

</form>


<div id='consoleOutputHtml' style='display:none'>
	<div class='input-group'>
		<span class='input-group-addon'>$</span>
		<div class='consoleOutput'></div>
	</div>
</div> <!-- /#consoleOutputHtml -->
