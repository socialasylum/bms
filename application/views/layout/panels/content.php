<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="panel min panel-default" data-selector='.panel-body'>
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#contentPanel">Content</a></h3>
	</div> <!-- /.panel-heading -->

	<div id="contentPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
	<div class='row'>
	<div class='col-xs-12'>

		<div class="form-group">
		    <label for="" class="control-label">Color</label>
			<input type='text' class='form-control' id='contentBgColor' name='contentBgColor' value="<?=$values['contentBgColor']?>">
		</div> <!-- .form-group -->


		<div class="form-group">
			    <label for="" class="control-label">Opacity <span id='contentOpacityPercent'></span></label>

	            <input type='hidden' name='contentBGOpacity' id='contentBGOpacity' value='<?=$values['contentBGOpacity']?>'>
	            <div id='contentBGOpacitySlide'></div>
		</div> <!-- .form-group -->		

<?php
/*

		<div class="form-group">
				<label for="" class="control-label">Background Image</label>
				<div class='input-group layout-bg-input-group'>
					<input type='text' class='form-control' id='contentBgImg' name='contentBgImg' value='<?=$values['contentBgImg']?>' placeholder="backgroundImage.png">
					<span class='input-group-btn'>
						<button type='button' class='btn btn-danger' id='delBGImgBtn' <?php if (empty($values['contentBgImg'])) echo "disabled='disabled'"; ?>><i class='fa fa-trash-o'></i></button>
					</span>
				</div>
					
				<select name='contentBGRepeat' id='contentBGRepeat' class='form-control'>
				<?php
				if (!empty($bgFills))
				{
					foreach ($bgFills as $k => $v)
					{
						echo "<option value='{$k}'>{$v}</option>" . PHP_EOL;
					}
				}					
				?>
				</select>
					
				<input type='file' name='contentBG' id='contentBG'>
		</div> <!-- .form-group -->		
*/
?>
		
	</div> <!-- /.col-xs-12 -->
	</div> <!-- /.row -->		
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->