<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


	<div class="panel min panel-default" data-selector='.wrapper'>
		<div class="panel-heading">
			
			<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#bgPanel">Body</a></h3>
		</div> <!-- /.panel-heading -->

		<div id="bgPanel" class="panel-collapse collapse in">
		<div class="panel-body">
		
		<div class='row'>
			<div class='col-xs-12'>
	
				<div class="form-group">
				    <label for="" class="control-label">Background Color</label>
					<input type='text' class='form-control' id='background-color' name='background-color' value="<?=$values['background-color']?>">
				</div> <!-- .form-group -->
				
				<div class="form-group">
					    <label for="" class="control-label">Background Opacity <span id='opacityPercent'><?=($values['bgOpacity'] * 100)?>%</span></label>
	
			            <input type='hidden' name='bgOpacity' id='bgOpacity' value='<?=$values['bgOpacity']?>'>
			            <div id='bgOpacitySlide'></div>
				</div> <!-- .form-group -->		
				
				<div class='form-group'>

				</div> <!-- /.form-group -->
	
	
				<div class="form-group">
					<label for="" class="control-label">Background Image</label>
					<div class='input-group layout-bg-input-group'>
						<input type='text' class='form-control' id='background-image' name='background-image' value='<?=$values['background-image']?>' placeholder="backgroundImage.png">
						<span class='input-group-btn'>
							<button type='button' class='btn btn-danger' id='delBGImgBtn' <?php if (empty($values['background-image'])) echo "disabled='disabled'"; ?>><i class='fa fa-trash-o'></i></button>
						</span>
					</div>
					
 
					<select name='bgRepeat' id='bgRepeat' class='form-control'>
					<?php
					if (!empty($bgFills))
					{
						foreach ($bgFills as $k => $v)
						{
							$sel = ($values['bgRepeat'] == $k) ? "selected='selected'" : null;
							echo "<option {$sel} value='{$k}'>{$v}</option>" . PHP_EOL;
						}
					}					
					?>
					</select>
 
					<input type='file' id='bgImg' name='bgImg'>
				</div> <!-- .form-group -->		
				
				
			</div> <!-- /.col -->
		</div> <!-- /.row -->
		</div> <!-- /.panel-body -->
		</div>
	</div> <!-- /.panel -->