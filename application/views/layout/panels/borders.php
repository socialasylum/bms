<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="panel min panel-default" data-selector='.panel, .navbar, input, select, button'>
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#borderPanel">Borders</a></h3>	
	</div> <!-- /.panel-heading -->
	<div id="borderPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
		<div class='row'>
			<div class='col-xs-12'>
	
				<div class="form-group">
				    <label for="" class="control-label">Color</label>
					<input type='text' class='form-control' id='borderColor' name='borderColor' value="<?=$values['borderColor']?>">
				</div> <!-- .form-group -->		
	
				<div class="form-group">
				    <label for="" class="control-label">Width</label>
					<input type='text' class='form-control' id='borderThickness' name='borderThickness' value='<?=$values['borderThickness']?>'>
				</div> <!-- .form-group -->
				
				<div class="form-group">
					    <label for="" class="control-label">Radius <span id='contentRadiusDisplay'>4px</span></label>
	
			            <input type='hidden' name='contentBorderRadius' id='contentBorderRadius' value='<?=$values['contentBorderRadius']?>'>
			            <div id='contentBorderRadiusSlide'></div>
				</div> <!-- .form-group -->		
			
				<div class='form-group'>
				
					<div class='checkbox'>
						<label>
							<?php if (!empty($values['footerBorder'])) $footerBorderchecked = "checked='checked'"; ?>
							<input type='checkbox' name='footerBorder' id='footerBorder' <?=$footerBorderchecked?> value='1'> Footer Border
						</label>
					</div>
					
					<span class='help-block'><small>Some modules such as messaging will still use a fluid width</small></span>
				</div>

			</div> <!-- /.col-xs-12 -->	
		</div> <!-- /.row -->
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->