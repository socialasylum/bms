<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="panel min panel-default"  data-selector='#main-container'>
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#containerPanel">Container</a></h3>
	</div> <!-- /.panel-heading -->

	<div id="containerPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
	<div class='row'>
	<div class='col-xs-12'>

		<div class="form-group">
		    <label for="" class="control-label">Background Color</label>
			<input type='text' class='form-control' id='containerBGColor' name='containerBGColor' value="<?=$values['containerBGColor']?>" placeholder='transparent'>
		</div> <!-- .form-group -->

		<div class="form-group">
			    <label for="" class="control-label">Background Opacity <span id='containerOpacityPercent'></span></label>

	            <input type='hidden' name='containerBGOpacity' id='containerBGOpacity' value='<?=$values['containerBGOpacity']?>'>
	            <div id='containerBGOpacitySlide'></div>
		</div> <!-- .form-group -->		


	</div> <!-- /.col-xs-12 -->
	</div> <!-- /.row -->		
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->