<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="panel min panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#miscPanel">Misc</a></h3>
	</div> <!-- /.panel-heading -->

	<div id="miscPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
	<div class='row'>
	<div class='col-xs-12'>
		<input type='hidden' name='bootstrapTheme' value='0'>
					
		<div class="form-group">
			<div class='checkbox'>
				<label>
					<?php if (!empty($values['bootstrapTheme'])) $bSTchecked = "checked='checked'"; ?>
					<input type='checkbox' name='bootstrapTheme' id='bootstrapTheme' <?=$bSTchecked?> value='1'> Enable Bootstrap Theme.
				</label>
			</div>


		</div> <!-- .form-group -->
		
		<div class='form-group'>
			<input type='hidden' name='fixedWidth' value='0'>
							
			<div class='checkbox'>
				<label>
					<?php if (!empty($values['fixedWidth'])) $FWchecked = "checked='checked'"; ?>
					<input type='checkbox' name='fixedWidth' id='fixedWidth' <?=$FWchecked?> value='1'> Fixed Width
				</label>
			</div>
			
			<span class='help-block'><small>Some modules such as messaging will still use a fluid width</small></span>
		</div>


	</div> <!-- /.col-xs-12 -->
	</div> <!-- /.row -->		
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->