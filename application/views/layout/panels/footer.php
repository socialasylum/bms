<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	
<div class="panel min panel-default" data-selector='.footer'>
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#footerPanel">Footer</a></h3>
	</div> <!-- /.panel-heading -->

	<div id="footerPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
	<div class='row'>
	<div class='col-xs-12'>

		<div class="form-group">
		    <label for="" class="control-label">Background Color</label>
			<input type='text' class='form-control' id='footerBgColor' name='footerBgColor' value="<?=$values['footerBgColor']?>">
		</div> <!-- .form-group -->


	</div> <!-- /.col-xs-12 -->
	</div> <!-- /.row -->		
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->