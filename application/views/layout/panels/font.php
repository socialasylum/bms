<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="panel min panel-default" data-selector='p, span, label'>
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#fontPanel">Font</a></h3>	
	</div> <!-- /.panel-heading -->
	<div id="fontPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
		<div class='row'>
			<div class='col-xs-12'>
	
				<div class="form-group">
				    <label for="" class="control-label">Color</label>
					<input type='text' class='form-control' id='fontColor' name='fontColor' value="<?=$values['fontColor']?>">
				</div> <!-- .form-group -->		
					
				<div class="form-group">
				    <label for="" class="control-label">Weight</label>
					<input type='text' class='form-control' id='fontWeight' name='fontWeight' value='<?=$values['fontWeight']?>'>
				</div> <!-- .form-group -->	


				
			</div> <!-- /.col-xs-12 -->	
		</div> <!-- /.row -->
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->
