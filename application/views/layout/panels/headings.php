<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="panel min panel-default" data-selector='h1, h2, h3, h4, h5, h6'>
	<div class="panel-heading">
		<h3 class="panel-title"><a data-toggle="collapse" data-parent="#layout-panels" href="#headingPanel">Headings</a></h3>	
	</div> <!-- /.panel-heading -->
	<div id="headingPanel" class="panel-collapse collapse">
	<div class="panel-body">
	
		<div class='row'>
			<div class='col-xs-12'>
	
				<div class="form-group">
				    <label for="" class="control-label">Color</label>
					<input type='text' class='form-control' id='headingColor' name='headingColor' value="<?=$values['headingColor']?>">
				</div> <!-- .form-group -->		
	
				<div class="form-group">
				    <label for="" class="control-label">Weight</label>
					<input type='text' class='form-control' id='headingWeight' name='headingWeight' value='<?=$values['headingWeight']?>'>
				</div> <!-- .form-group -->
				
			</div> <!-- /.col-xs-12 -->	
		</div> <!-- /.row -->
	</div> <!-- /.panel-body -->
	</div>
</div> <!-- /.panel -->