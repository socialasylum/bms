<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><?=$this->modules->getIcon(true)?>Layout</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . 'application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><button type='button' class='btn btn-link navbar-btn' id='saveBtn'><i class='fa fa-save success'></i> Save &amp; Preview</button></li>
            <li><button type='button' class='btn btn-link navbar-btn' id='compileBtn'><i class='fa fa-cog info'></i> Compile</button></li>

        </ul>

        <ul class="nav navbar-nav pull-right">

            <li><button type='button' class='btn btn-link navbar-btn' id='resetBtn'><i class='fa fa-history danger'></i> Reset to Default</button></li>
			<!--
            <li><a href='javascript:void(0);' id='helpBtn' class='helpBtn'><i class='fa fa-question-circle info'></i></a></li>

            <li><a href='javascript:void(0);' onclick="users.cancelEdit(this);" id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
            -->
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . 'application/views/template/navbar_bottom.php'; ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li class="active"><a href="#styleTab" role="tab" data-toggle="tab"><i class='fa fa-tint'></i> Site Styles</a></li>
	<li><a href="#cssTab" role="tab" data-toggle="tab"><i class='fa fa-css3'></i> CSS</a></li>
</ul>

<?php

    $attr = array
        (
            'name' => 'layoutForm',
            'id' => 'layoutForm'
        );

echo form_open('#', $attr);
?>

<input type='hidden' id='company' value='<?=$this->session->userdata('company')?>'>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane fade in active" id="styleTab">

	<div class='row'>
		<div class='col-xs-12 col-sm-3'>
		
		<div class='panel-group' id='layout-panels'>
			<?php 
				include_once 'panels/body.php';
				include_once 'panels/container.php';
				include_once 'panels/content.php';
				include_once 'panels/font.php';
				include_once 'panels/headings.php';
				include_once 'panels/borders.php';
				include_once 'panels/footer.php';
				include_once 'panels/misc.php';
			?>
	</div> <!-- /.layout-panels -->

		</div> <!-- /.col -->
		
				
			<div class='col-sm-9'>
			<iframe src='/layout/preview?preview' id='preview-panel' scrolling='yes' seamless='seamless' class='layout-preview'></iframe>
			<?php //include_once 'preview.php'; ?>
			</div> <!-- /.col-sm-9 -->
		
	
	</div> <!-- /.row -->

  </div> <!-- /#styleTab -->
  <div class="tab-pane fade" id="cssTab">
  	<p class='lead'>
  		Here you can add your own CSS tags (for advanced users).
  	</p>
  
	  <textarea class='form-control' name='css' id='css' rows='15' placeholder="body{ background:white; } ... "><?=$values['css']?></textarea>
  </div> <!-- /#cssTab -->
</div>

</form>