<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<script type='text/javascript'>
(function(){
	//$('select').selectpicker();
});
</script>

<div class="panel panel-default" id='preview-panel'>
	<div class="panel-heading">
		<h3 class="panel-title">Preview</h3>	
	</div> <!-- /.panel-heading -->

	<div class="panel-body" id='preview-body'>
		<h1><i class='fa fa-eye'></i> Preview</h1>

		<?php include $_SERVER['DOCUMENT_ROOT'] . 'application/views/template/navbar_top.php'; ?>

        <ul class="nav navbar-nav">
            <li><button type='button' class='btn btn-link navbar-btn'><i class='fa fa-save success'></i> Save</button></li>
        </ul>
      
		<div class="navbar-form navbar-left" role="search">
		  <div class="form-group">
		    <input type="text" class="form-control" placeholder="Search">
		  </div>
		  <button type="button" class="btn btn-default">Submit</button>
		</div>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

		<?php include $_SERVER['DOCUMENT_ROOT'] . 'application/views/template/navbar_bottom.php'; ?>			

		<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="#home" role="tab" data-toggle="tab">Home</a></li>
  <li><a href="#profile" role="tab" data-toggle="tab">Headings</a></li>
  <li><a href="#messages" role="tab" data-toggle="tab">Form</a></li>
  <li><a href="#settings" role="tab" data-toggle="tab">Settings</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content well-white">
  <div class="tab-pane active" id="home">
	  <p class='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque vitae turpis id mattis. Nulla elementum neque est, ut porta lacus ornare in.</p>
  
	  <p>
	  Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent hendrerit leo ut fringilla auctor. Morbi iaculis, metus ut sodales mattis, dui leo iaculis tortor, eu vehicula velit orci in metus. Nam turpis nisi, eleifend vitae ante at, hendrerit porttitor diam. Praesent auctor urna sit amet nulla malesuada, nec bibendum dolor consectetur. Maecenas fringilla nisl ut dui fringilla ultricies. Nullam iaculis vitae sem vulputate interdum. Maecenas imperdiet lectus at lectus tristique consectetur. Nulla a cursus nibh, vel eleifend purus. Sed eu magna urna. Nunc a adipiscing risus. Mauris id nibh blandit, iaculis orci rhoncus, rhoncus nibh. Nam aliquet lobortis volutpat.
	  </p>
  
  </div>
  
  <div class="tab-pane" id="profile">
  	  <h1>Heading h1</h1>
	  <h2>Heading h2</h2>
	  <h3>Heading h3</h3>
	  <h4>Heading h4</h4>
	  <h5>Heading h5</h5>
	  <h6>Heading h6</h6>
  </div>
  <div class="tab-pane" id="messages">
  
  	<?php $this->load->view('users/edit/email_new'); ?>

  </div>
  <div class="tab-pane" id="settings">...</div>
</div>
	
		

		
		

	</div> <!-- /.panel-body -->
	
</div> <!-- /.panel -->