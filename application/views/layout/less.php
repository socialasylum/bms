<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	try
	{
		$hideFooter = $this->companies->getTableValue('hideFooter', $company);
	}
	catch (Exception $e)
	{
		PHPFunctions::sendStackTrace($e);
	}

?>

@import "<?=$_SERVER['DOCUMENT_ROOT']?>public/less/mixins.less";

// body
<?=$this->style->css($company, '@layoutBGColor', '*', 'background-color')?>
<?=$this->style->css($company, '@layoutBGOpacity', null, 'bgOpacity')?>
<?=$this->style->css($company, '@layoutBGImg', "url('/{$this->path}*')", 'background-image')?>

// container
<?=$this->style->css($company, '@containerBGColor', "*", 'containerBGColor')?>
<?=$this->style->css($company, '@containerBGOpacity', null, 'containerBGOpacity')?>

//content
<?=$this->style->css($company, '@layoutContentBGColor', '*', 'contentBgColor')?>
<?=$this->style->css($company, '@layoutContentBGOpacity', null, 'contentBGOpacity')?>
<?=$this->style->css($company, '@layoutContentBGImg', "url('/{$this->path}*')", 'contentBgImg')?>

// heading colors
<?=$this->style->css($company, '@layoutHeadingColor', '*', 'headingColor')?>

//font
<?=$this->style->css($company, '@layoutWeight', null, 'fontWeight')?>
<?=$this->style->css($company, '@layoutFontColor', '*', 'fontColor')?>

// borders
<?=$this->style->css($company, '@layoutBorderRadius', '*px', 'contentBorderRadius')?>
<?=$this->style->css($company, '@layoutBorderColor', '*', 'borderColor')?>
<?=$this->style->css($company, '@layoutBorderWidth', '*px', 'borderThickness')?>


// footer
<?=$this->style->css($company, '@layoutFooterBGColor', '*', 'footerBgColor')?>

<?php
//$wrapper = (!$preview) ? ".wrapper" : "#preview-body";
?>

body
{
	color:@layoutFontColor;
	
	<?php
		if ($hideFooter)
		{
			$rgb = PHPFunctions::hex2rgb($this->style->getFinalVal($company, 'background-color'));
		
			echo "background-color: rgba({$rgb[0]}, {$rgb[1]}, {$rgb[2]}, @layoutBGOpacity);" . PHP_EOL;

			if ((int) $values['bgRepeat'] !== 4) echo "background-image:@layoutBGImg;" . PHP_EOL;
		
			if ((int) $values['bgRepeat'] == 1) echo "background-repeat:repeat;" . PHP_EOL;
			if ((int) $values['bgRepeat'] == 5) echo ".bgSize(cover);" . PHP_EOL;
		}
		else
		{
			echo 'background-color: @layoutFooterBGColor;';
		}
	?>


};


	.wrapper
	{
		//width:100%;
		//height:auto;
			
		<?php
		if ($hideFooter)
		{
			echo "background:none;" . PHP_EOL;
		}
		else
		{
			$rgb = PHPFunctions::hex2rgb($this->style->getFinalVal($company, 'background-color'));
		
			echo "background-color: rgba({$rgb[0]}, {$rgb[1]}, {$rgb[2]}, @layoutBGOpacity);" . PHP_EOL;
			
			if ((int) $values['bgRepeat'] == '4') echo 'background:none;';
			else  echo "background-image:@layoutBGImg;" . PHP_EOL;
		}

		if ((int) $values['bgRepeat'] == 1) echo "background-repeat:repeat;" . PHP_EOL;
		if ((int) $values['bgRepeat'] == 5) echo ".bgSize(cover);" . PHP_EOL;
		?>
		
	

		<?php
			if ((int) $values['footerBorder'] == 1)
			{
				echo 'border-bottom:@layoutBorderWidth solid @layoutBorderColor;' . PHP_EOL;
			}
			else
			{
				echo 'border-bottom:none;' . PHP_EOL;
				echo 'box-shadow:none;' . PHP_EOL;
			}
		?>

		//display: inline-block;
		//position: relative;
		
	};
	
<?php if (!empty($values['containerBGColor'])) :?>

#main-container
, .dashboard-widget .display
, .msg-folders
, #msg-list-display
, #msg-preview .inner
{
	
	<?php
	$rgb = PHPFunctions::hex2rgb($this->style->getFinalVal($company, 'containerBGColor'));
	?>
	
	background-color: rgba(<?=$rgb[0]?>, <?=$rgb[1]?>, <?=$rgb[2]?>, @containerBGOpacity);
};

<?php endif; ?>

	<?php /* if ($values['bgOpacity'] < 100) : ?>

	.wrapper::after
	{
		content: "";
		position:absolute;
		background-color:@layoutBGColor;
		background-image:@layoutBGImg;
			  
		  -webkit-background-size: cover;
		  -moz-background-size: cover;
		  -o-background-size: cover;
		  background-size: cover;
			  
			  
		opacity:@layoutBGOpacity;
		background-repeat:repeat;
		top:0;
		left:0;
		bottom:0;
		right:0;
		z-index:-1;
	};
	<?php endif; */ ?>

	.well-white
	, .panel
	, blockquote
	{
		display: block;
		//position: relative;

		<?php
		if (PHPFunctions::isHexColor($this->style->getFinalVal($company, 'contentBgColor')))
		{
			$rgb = PHPFunctions::hex2rgb($this->style->getFinalVal($company, 'contentBgColor'));
			echo "background-color: rgba({$rgb[0]}, {$rgb[1]}, {$rgb[2]}, @layoutContentBGOpacity);" . PHP_EOL;
		}
		else
		{
			echo "background-color: @layoutContentBGColor;" . PHP_EOL;
		}
		
		?>

		
		//background-color: @layoutContentBGColor;
		background-image:@layoutContentBGImg;
		color:@layoutFontColor;
	};
	
	.footer
	{
		background-color:@layoutFooterBGColor;
	};
	
	// full borderRadius
	.btn
	, .form-control
	, .navbar.subnav
	, .breadcrumb
	, .panel
	, .alert
	, #main-container.container
	, .msg-folders
	, #msg-list-display
	, #msg-preview .inner
	, #editor .placeholder
	{
		.borderRadius(@layoutBorderRadius, @layoutBorderRadius, @layoutBorderRadius, @layoutBorderRadius) !important;
	};
	
	.menu-panel .navbar.subnav
	{
		.borderRadius(0, 0, 0, 0);
	};

	.cke_chrome
	{
		overflow:hidden;
	}

	iframe
	, .well
	, .well-white
	, .panel-group .panel
	, .panel-group
	, .navbar .nav > li > .dropdown-menu li > a
	, .uploader-dropzone
	, .img-thumbnail
	, .img-responsive
	, .label
	, .modal-content
	, .dropdown-menu
	, .uploader-dropzone button
	, blockquote
	
	{
		.borderRadius(@layoutBorderRadius, @layoutBorderRadius, @layoutBorderRadius, @layoutBorderRadius) !important;
	};
	
	// top only
	.nav-tabs > li > a
	, iframe.layout-preview #main-top-nav
	, .panel-heading
	{
		.borderRadius(@layoutBorderRadius, 0, 0, @layoutBorderRadius);
	};
	
	// bottom
	.module-drop-zone
	, .panel-footer
	{
		.borderRadius(0, @layoutBorderRadius, @layoutBorderRadius, 0) !important;
	};
	
	// left only
	.pagination > li:first-child > a, .pagination > li:first-child > span
	{
		.borderRadius(@layoutBorderRadius, @layoutBorderRadius, 0, 0);
	};
	
	// right only
	.pagination > li:last-child > a, .pagination > li:last-child > span
	{
		.borderRadius(0, 0, @layoutBorderRadius, @layoutBorderRadius);
	};
	
	.navbar .nav > li > .dropdown-menu
	{
		.borderRadius(0, @layoutBorderRadius, @layoutBorderRadius, @layoutBorderRadius);
	};
	
	<?php 
	// if border radius is zero, sets everything to no radius to be sureee
	if (empty($values['contentBorderRadius'])) echo " *{ .borderRadius(0, 0, 0, 0); } " . PHP_EOL;
	?>
	h1, h2, h3, h4, h5, h6
	{
		color:@layoutHeadingColor;
	
	};
	
	<?php 
	
	if (!empty($values['css'])) echo $values['css'];
	
	//if ($preview) echo "\n\t};\n";
	?>



