<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

        <!-- <h1><i class='fa fa-calendar'></i> Calendar</h1> -->

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav pull-right">
            <li><a href='/calendar/index/<?=date("n/Y", mktime(0, 0, 0, ($month - 1), 1, $year))?>' name='prevBtn' id='prevBtn'><i class='fa fa-chevron-left'></i></a></li>
             <li><a href='/calendar/index/<?=date("n/Y", mktime(0, 0, 0, ($month + 1), 1, $year))?>' name='nextBtn' id='nextBtn'><i class='fa fa-chevron-right'></i></a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>



<?php
/*
    <hr>

    <div class='col-6'>
        <h4><?=date("F Y", mktime(0, 0, 0, $month, 1, $year))?></h4>
    </div>

    <div class='col-6'>
        <div class='btn-group pull-right'>
            <a href='/calendar/index/<?=date("n/Y", mktime(0, 0, 0, ($month - 1), 1, $year))?>' class='btn btn-info' name='prevBtn' id='prevBtn'><i class='fa fa-chevron-left'></i></a>
            <a href='/calendar/index/<?=date("n/Y", mktime(0, 0, 0, ($month + 1), 1, $year))?>' class='btn btn-info' name='nextBtn' id='nextBtn'><i class='fa fa-chevron-right'></i></a>
        </div> <!-- .btn-group -->

    </div> <!-- .col-6 -->
*/
?>

<table class='calendar table table-bordered'>
    <thead>
        <tr>
            <th>Sunday</th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
            <th>Saturday</th>
        </tr>
    </thead>
    <tbody>
<?php

$first = date("w", mktime(0, 0, 0, $month, 1, $year));

// echo "FIRST:{$first}<BR>";

$startDate = mktime(0, 0, 0, $month, (1 - $first), $year);

$endDate = mktime(0, 0, 0, ($month + 1), (1-1), $year);

$end = date('n', $endDate);

// echo date("m/d/Y l | N | z", $startDate) . "<BR>";
// echo date("m/d/Y l | N | z", $endDate) . "<BR>";

$diff =  (int) date("z", $endDate) - (int) date("z", $startDate);
// echo $diff;

    if ($diff < 0) $diff += 365;

$extend = 7 - (int) date("N", $endDate);

// echo "EXTEND: {$extend}<BR>";

$rcnt = 1;


$total = $diff + $extend;

    try
    {
        $events = $this->calendar->getEvents($startDate, $endDate);

        // print_r($events);
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }


for ($i = 1; $i <= $total; $i++)
{

    $class = $tdClass = null;

    $utDay = $startDate + (($i - 1) * 86400);

    $day = date("d", $utDay);
    // $day = date("m/d/Y g:i A", $utDay);
    
    $date = date("Y-m-d", $utDay);

    if ($i > $first && $i <= ($total - $extend)) $class = 'label-success';
    else $tdClass = 'unavail-day';

    if ($rcnt == 1) echo "<tr>" . PHP_EOL;

    echo "\t<td class='{$tdClass}'><div class='day-container'>";

    // echo "<div class='label {$class} calDay'>{$day}</div>";

    echo "<a href='/calendar/edit/?date=" . urlencode($date) . "&month={$month}&year={$year}' class='btn btn-info btn-xs calDay' title='{$date}'><i class='icon-pencil'></i> {$day}</a>";


    // goes through events of which ones to display

    // echo "UTDAY: {$utDay}";

    if (!empty($events))
    {
        foreach ($events as $r)
        {
            $color = null;

            $tomorrow = $utDay + 86400;

            $fdut = strtotime($r->fromDate);
            $tdut = strtotime($r->toDate);

            // $Fstart = date("g:i A", $fdut);
            // $Fend = date("g:i A", $tdut);

            $Fstart = $this->users->convertTimezone($this->session->userdata('userid'), date("Y-m-d g:i A", $fdut), "g:i A");
            $Fend = $this->users->convertTimezone($this->session->userdata('userid'), date("Y-m-d g:i A", $tdut), "g:i A");


            // $color = "background-color:#" . (empty($r->color) ? 'F00' : $r->color );

            if (!empty($r->color)) $color = "background-color:#{$r->color}";

            if ($r->allDay == 1)
            {
                if ($fdut >= $utDay && $tdut < $tomorrow)
                // if ($utDay >= $fdut && $utDay <= $tdut)
                {
                    echo "<label class='label label-default calendarEvent' onclick=\"cal.loadEventModal({$r->id});\" style=\"{$color}\">{$r->name}</label>" . PHP_EOL;
                }
            }
            else
            {
                if ($fdut >= $utDay && $tdut < $tomorrow)
                {
                    // check for non all day events
                    // echo "<label class='label label-default'>{$r->name} : {$fdut} : {$tdut}</label>" . PHP_EOL;
                    echo "<label class='label label-default calendarEvent' onclick=\"cal.loadEventModal({$r->id});\" style=\"{$color}\">{$r->name} {$Fstart}-{$Fend}</label>" . PHP_EOL;
                }
            }
        }
    }


    echo "</div></td>" . PHP_EOL;

    if ($rcnt >= 7)
    {
        echo "</tr>" . PHP_EOL;
        $rcnt = 1;
    }
    else
    {
        $rcnt++;
    }
}


?>
    </tbody>
</table>

<div id='eventModal' class='modal fade'></div>
