<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>

<style type='text/css'>
.modal-backdrop
{
	background-color: #FFF;
}

</style>



<h1><?=(empty($id)) ? 'Create' : 'Edit'?> Event</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <button type='button' class='btn btn-link navbar-btn' id='saveBtn'><i class='fa fa-save success'></i> Save</button>

		<button type='button' class='btn btn-link navbar-btn pull-right' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</button>
		<?php if (!empty($id)) : ?>
		<button type='button' class='btn btn-link navbar-btn pull-right' name='deleteBtn' id='deleteBtn'><i class='fa fa-trash-o'></i></button>
		<?php endif; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

$attr = array
    (
        'id' => 'repeatEventForm',
        'name' => 'repeatEventForm'
    );


echo form_open_multipart('/calendar/save', $attr);

if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>";
?>

    <input type='hidden' name='month' id='month' value='<?=$month?>'>
    <input type='hidden' name='year' id='year' value='<?=$year?>'>


<div class='col-lg-12 col-m-12 col-s-12 col-xs-12 form-horizontal'>
    <div class='row'>
        <div class='col-lg-6 col-m-6 col-s-12 col-xs-12'>

            <h2><i class='fa fa-plus-square'></i> Add Event</h2>
				
				<input type='text' class='form-control' name='title' id='title' value="<?=$info->name?>" placeholder="Event Title">

					<div class='row'>
		  
						  <div class='col-md-12 timedash'>-</div>
						  
						  <div class='col-md-3'><input type='text' class='form-control' name='startDate' id='startDate' value="<?=$fromDate?>" placeholder='<?=date("Y-m-d")?>'></div>
						  <div class='col-md-3'><input type='text' class='form-control' name='startTime' id='startTime' value="<?=$fromTime?>" placeholder='10:00AM'></div>
						  

						  <div class='col-md-3'><input type='text' class='form-control' name='endTime' id='endTime' value="<?=$toTime?>" placeholder='5:30PM'></div>
						  <div class='col-md-3'><input type='text' class='form-control' name='endDate' id='endDate' value="<?=$toDate?>" placeholder='<?=date("Y-m-d")?>'></div>
						  
					</div> <!-- /.row -->
        
					<div class='row'>
						<div class='col-md-12 eventCheckboxes'>
							<div class='checkbox'>
								<label>
									<input type='hidden' name='allDay' value='0'>
									<?php
									$allDayChecked = (!empty($info->allDay)) ? "checked='checked'" : null;
									?>
									<input type='checkbox' name='allDay' id='allDay' value='1' <?=$allDayChecked?>> All Day
								</label>
							</div>
							
							<input type='hidden' name='repeatEvent' value='0'>
							
							<div class='checkbox'>
								<label>
								<?php
								$repeatChecked = (!empty($info->repeat)) ? "checked='checked'" : null;
								?>
									<input type='checkbox' name='repeatEvent' id='repeatEvent' value='1' <?=$repeatChecked?>> 
								</label>
								
								<span id='repeatCheck'>Repeat...</span>
							</div>

							
						</div>
					</div>



					<h3>Upload Attachments</h3>
		  
				  <div class='row eventInputMargin'>
				  	<div class='col-md-12'>
				  		<button type='button' class='btn btn-link' id='videoUploadBtn'><i class='fa fa-plus'></i> Videos</button>
				  		<button type='button' class='btn btn-link' id='photoUploadBtn'><i class='fa fa-plus'></i> Photos</button>
				  		<button type='button' class='btn btn-link' id='fileUploadBtn'><i class='fa fa-plus'></i> Attachments</button>
				  	</div>
				  </div>


				  <div id='uploadContainer'>
					  <?php
					  	if (!empty($videos))
					  	{
					  		//print_r($videos);
					  	
						  	foreach ($videos as $r)
						  	{
						  		$body = array();
						  		
						  		$body['url'] = $r->url;
						  		$body['id'] = $r->id;
						  		
							  	$this->load->view('calendar/video_upload', $body);
						  	}
					  	}
					  	
					  	if (!empty($files))
					  	{
					  		foreach ($files as $r)
					  		{
						  		$path = "/public/uploads/events/{$id}/";


							  	echo <<< EOS
							  	<div class='fileUploadContainer'>
								  	<div class='row eventInputMargin'>
								  		<div class='col-md-6'>
								  			<a href='{$path}{$r->fileName}' target='_blank'>{$r->orgFileName}</a>
								  		</div> <!-- /.col-md-6 -->
								  		
								  		<div class='col-md-6'>
								  			<button type='button' class='btn btn-danger pull-right' onclick="cal.removeFileUploaded(this, {$id}, {$r->id});"><i class='fa fa-trash-o'></i></button>
								  		</div> <!-- /.col-md-6 -->
								  	</div> <!-- /.row -->
							  	</div> <!-- /.fileUploadContainer -->
EOS;
							}
					  	}
					  ?>
				  </div>
        
		<?php if (empty($location)) : ?>
				  <div class='row eventInputMargin'>
				  	<div class='col-md-12'>
				  		<select class='form-control' id='eventLocationSel' name='eventLocationSel'>
				  			<option value=''>SELECT A LOCATION</option>
				  			<?php
							if (!empty($locations))
							{
							    foreach ($locations as $r)
							    {
							        $sel = ($r->id == $info->location) ? "selected='selected'" : null;
							
							        echo "<option {$sel} value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
							    }
							}
							
							    $otherSelected = (!empty($info->otherLocation)) ? "selected='selected'" : null;
							
							?>
				  			<option value='OTHER'>OTHER LOCATION</option>
				  		</select>
				  	</div> <!-- /.col-12 -->
				  </div> <!-- /.row -->
		
				  <div class='row eventInputMargin' id='otherLocRow' style='display:none;'>
				  	<div class='col-md-12'>
				  		<input type='text' class='form-control' name='otherLocation' id='otherLocation' value="" placeholder="Location">
				  	</div> <!-- /.col-12 -->
				  </div> <!-- /.row -->
		
		<?php endif; ?>
        
				  <div class='row eventInputMargin'>
				  	<div class='col-md-12'>
				  		<select class='form-control' name='expLvl' id='expLvl'>
				  			<option value=''>EXPERIENCE LEVEL</option>
				  			<?php
				  			if (!empty($expLvls))
				  			{
					  			foreach ($expLvls as $r)
					  			{
					  				$sel = ($info->expLvl == $r->code) ? "selected='selected'" : null;
					  			
						  			echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
					  			}
				  			}
				  			?>
				  		</select>
				  	</div> <!-- /.col-12 -->
				  </div> <!-- /.row -->
		
				  <div class='row eventInputMargin'>
				  	<div class='col-md-12'>
				  		<div class="input-group">
						  <span class="input-group-addon"><i class='fa fa-dollar'></i></span>
						  <input type="text" class="form-control" name='price' id-'price' value="<?=$info->price?>" placeholder="PRICE OF EVENT">
						  <span class="input-group-addon">.00</span>
						</div>
				  	</div> <!-- /.col-12 -->
				  </div> <!-- /.row -->



				  <div class='row eventInputMargin'>
				  	<div class='col-md-12'>
				  		<div class="input-group">
						  <span class="input-group-addon"><i class='fa fa-pencil-square-o'></i></span>
						  <?php
						    $color = (empty($id)) ? 'FF0000' : $info->color;
						?>
							<input type='text' name='color' id='color' value='<?=$color?>' hex='<?=$color?>' style="background-color:#<?=$color?>;" class='form-control color-picker' placeholder="Select a color">
						</div>
				  	</div> <!-- /.col-12 -->
				  </div> <!-- /.row -->



<?php
/*
            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='reminder'>Reminder</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' class='form-control'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
*/
?>
        </div> <!-- .col-6 -->

        <div class='col-lg-6 col-m-6 col-s-12 col-xs-12'>
            <h2><i class='fa fa-pencil-square'></i> Description</h2>
            <textarea class='form-control' rows='5' name='description' id='description'><?=$info->description?></textarea>
        </div> <!-- .col-6 -->

    </div> <!-- .row -->

</div> <!-- col-12 -->

</form>

<?php
	$body = array();
	$body['repeatTypes'] = $repeatTypes;
	$body['location'] = $location;
	$body['info'] = $info;
	$body['id'] = $id;
	
	$this->load->view('calendar/repeat_modal', $body);
?>


<div id='videoUploadHtml' class='hide'>
<?php
	$body = array();
	$body['url'] = '';
	$this->load->view('calendar/video_upload', $body);
?>
</div> <!-- /#videoUploadHtml -->

<div id='fileUploadHtml' class='hide'>
	<div class='fileUploadContainer'>
		<div class='row eventInputMargin'>
			<div class='col-md-6'>
				<input type='file' name='file[]'>
			</div> <!-- /.col-md-6 -->
			<div class='col-md-6'>
				<button type='button' class='btn btn-danger pull-right' onclick="cal.removeFileUpload(this);"><i class='fa fa-trash-o'></i></button>
			</div> <!-- /.col-md-6 -->
			
		</div> <!-- /.row -->
	</div> <!-- /.fileUploadContainer -->
</div> <!-- /#fileUploadHtml -->