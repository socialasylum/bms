<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Promotion</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save Promotion</a></li>
            <!-- <li><a href='#settingsModal' data-toggle='modal'><i class='fa fa-cog'></i> Settings</a></li> -->

        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
        <?php if (!empty($id)) : ?>
            <li><a href='javascript:void(0);' class='danger' onclick="formbuilder.deleteForm(<?=$id?>);"><i class='fa fa-trash-o danger'></i> Delete</a></li>
        <?php endif; ?>

            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>


        </ul>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'method' => 'post'
        );

echo form_open_multipart('/promo/save', $attr);

    if (!empty($id))
    {
        echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
        
        echo "<input type='hidden' name='currentBannerImg' id='currentBannerImg' value=\"{$info->bannerImg}\">";
    }

?>

<div class='tabbable'>
    <ul class='nav nav-tabs' id='userTabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab"><i class='fa fa-cog'></i> Settings</a></li>
        <li><a href='#tabItems' data-toggle="tab"><i class='fa fa-shopping-cart'></i> Items</a></li>
    </ul>

    <div class="tab-content">

        <div id="tabSettings" class='tab-pane form-horizontal active'>
            <div class='row'>
                <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>

                    <div class="form-group">
                        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='name'>Name</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                            <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>">
                        </div> <!-- .controls -->
                    </div> <!-- .form-group -->

                    <div class="form-group">
                        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='active'>Status</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                            <select name="active" id="acitve" class='form-control'>
                                <?php
                                    foreach($status as $code => $display)
                                    {
                                        // new blogs, sets to active by default
                                        if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                                        else $sel = ($info->active == $code) ? 'selected' : null;

                                        echo "<option {$sel} value='{$code}'>{$display}</option>\n";
                                    }
                                ?>
                            </select>
                        </div> <!-- .controls -->
                    </div> <!-- .form-group -->

                    <div class="form-group">
                        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='activeatedBy'>Activated By</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                            <select name="activeatedBy" id="activeatedBy" class='form-control'>
<?php
if (!empty($actTypes))
{
    foreach ($actTypes as $r)
    {
        echo "<option value='{$r->code}'>{$r->display}</option>". PHP_EOL;
    }
}
?>
                            </select>
                        </div> <!-- .controls -->
                    </div> <!-- .form-group -->

                </div> <!-- col-6 -->
                
                <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
 
                    <div class="form-group">
                        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='startDate'>Start Date</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                            <input type='text' class='form-control' name='startDate' id='startDate' value="<?=date("Y-m-d", strtotime($info->startDate))?>">
                        </div> <!-- .controls -->
                    </div> <!-- .form-group -->

                    <div class="form-group">
                        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='endDate'>End Date</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                            <input type='text' class='form-control' name='endDate' id='endDate' value="<?=date("Y-m-d", strtotime($info->endDate))?>">
                        </div> <!-- .controls -->
                    </div> <!-- .form-group -->
                   


                </div> <!-- col-6 -->
            </div> <!-- .row -->

            <hr>
                <h2><i class='fa fa-picture-o'></i> Banner Image</h2>

                <p><input type='file' name='bannerImg'></p>

                <?php
                    if (empty($info->bannerImg))
                    {
                        echo $this->alerts->info("No banner image has been uploaded!");
                    }
                    else
                    {
                        echo "<img src='/public/uploads/promoimgs/{$this->session->userdata('company')}/{$info->bannerImg}' class='img-responsive'>" . PHP_EOL;
                    }
                ?>

            <hr>

            <h2><i class='fa fa-exclamation-circle'></i> Description</h2>

            <textarea class='form-control' rows='5' name='description' id='description'><?=$info->description?></textarea>

        </div> <!-- #tabSettings -->

        <div id="tabItems" class='tab-pane'>

<?php
// print_r($promoItems);
if (empty($items))
{
    echo $this->alerts->info("No items have been created");
}
else
{
echo <<< EOS
    <table class='table table-hover table-bordered'>
        <thead>
            <tr>
                <th width='50px'><input type='checkbox' onclick="global.tblChkboxSelectAllToggle(this, '#promoItemsTblBody');"></th>
                <th>Item</th>
                <th>Cost</th>
                <th>Retail Price</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody id='promoItemsTblBody'>
EOS;

    foreach ($items as $r)
    {
        $status = (empty($r->active)) ? "<label class='label label-danger'>Deactived</label>" : "<label class='label label-success'>Active</label>";

        $checked = (in_array($r->id, $promoItems)) ? "checked='checked'" : null;

        echo "<tr>" . PHP_EOL;

        echo "\t<td width='50px'><input type='checkbox' name='item[]' id='item_{$r->id}' value='{$r->id}' {$checked} ></td>" . PHP_EOL;
        echo "\t<td>{$r->name}</td>" . PHP_EOL;
        echo "\t<td>\${$r->cost}</td>" . PHP_EOL;
        echo "\t<td>\${$r->retailPrice}</td>" . PHP_EOL;
        echo "\t<td>{$status}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>

        </div> <!-- tabitems -->
    </div> <!-- tab-content -->

</div> <!-- .tabbable -->
</form>
