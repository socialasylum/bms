<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-star-o'></i> Promotions</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
<ul class="nav navbar-nav">
    <li><a href='/promo/edit'><i class='fa fa-star'></i> New Promotion</a></li>
</ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php
if (empty($promos))
{
    echo $this->alerts->info("No promotions have been created!");
}
else
{
echo <<< EOS
    <table class='table table-striped table-condensed' id='promoTbl'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Promo ID</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($promos as $r)
    {
        try
        {
            $startDate = date("Y-m-d", strtotime($r->startDate));
            $endDate = date("Y-m-d", strtotime($r->endDate));
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "\t<td>{$r->name}</td>" . PHP_EOL;
        echo "\t<td>{$r->id}</td>" . PHP_EOL;
        echo "\t<td>{$startDate}</td>" . PHP_EOL;
        echo "\t<td>{$endDate}</td>" . PHP_EOL;
        echo "\t<td><button type='button' class='btn btn-info btn-xs pull-right' onclick=\"promo.edit(this, $r->id);\"><i class='fa fa-edit'></i></button></td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;

}
?>
