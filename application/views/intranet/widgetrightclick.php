<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='dropdown'>
    <ul class='dropdown-menu'>
    <!-- <li><a tabindex='-1' href="javascript:void(0);"><i class='icon-trash'></i> Delete</a></li> -->
    <li class='dropdown-submenu'>
        <a tabindex='-1' href="javascript:void(0);" onclick=""><i class='icon-plus'></i> Add Widget</a>
            <ul class='dropdown-menu'>
<?php
if (!empty($widgets))
{
    foreach ($widgets as $k => $r)
    {
        echo "<li><a href=\"javascript:void(0);\" onclick=\"intranet.loadWidget('{$r->url}', {$r->id});\">{$r->name}</a></li>" . PHP_EOL;
    }
}
?>
            </ul>
    </li>
    <li class='divider'></li>
    </ul>
</div>
