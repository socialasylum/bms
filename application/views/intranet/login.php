<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>



<div class='row'>

<div class='col-xs-12 col-sm-offset-3 col-sm-6'>

        <div align='center'>

<?php
/*
	if (empty($compLogo)) echo "<h1>" . ((empty($compName)) ? 'Business Management System' : $compName) . '</h1>' . PHP_EOL;
	else echo "<img class='img-responsive login-logo' src='/public/uploads/logos/{$company}/{$compLogo}'>" . PHP_EOL;
 */
?>
<img class="img-responsive login-logo" src="/public/images/greennvy_logo_whisper.png" />
        </div>

<div class='panel panel-default'>
<div class='panel-heading'><i class='fa fa-sign-in'></i> Login</div>

<div class='panel-body'>

<?php if(!empty($msg)) : ?>
<div class='alert'><?=$msg?></div>
<?php endif; ?>


<?php

	$remClass = 'fa-square-o';
	
    if ($this->session->userdata('rememberMe')) $remClass = "fa-check-square-o";


    $attr = array
        (
            'name' => 'loginForm',
            'id' => 'loginForm',
            'method' => 'post',
            'class' => 'login',
            'onSubmit' => 'intranet.loginSubmit();'
        );

echo form_open('/intranet/login', $attr);
?>
<input type='hidden' name='ref' value='<?=$_GET['ref']?>'>
<input type='hidden' name='company' value='<?=$company?>'>


<div class="form-group">
    <label class='control-label' for='email'>E-mail</label>

	<div class='input-group'>
		<span class='input-group-addon'><i class='fa fa-envelope'></i></span>
        <input type='text' class='form-control' id='email' tabindex='1' name='email' autocomplete="off" placeholder="E-Mail" value="<?=$email?>">
	</div>
</div> <!-- .form-group -->


<div class="form-group">
    <label class='control-label' for='password'>Password</label>

	<div class='input-group'>
		<span class='input-group-addon'><i class='fa fa-lock'></i></span>
        <input type='password' id='password' tabindex='2'  name='password' class='form-control' autocomplete="off" placeholder='Password'>
	</div>
		<span class='help-text'><a href='javascript:void(0);' data-toggle='modal' data-target='#passwordModal'>Forgot Password?</a></span>
</div> <!-- .form-group -->

</div> <!-- .panel-body -->

    <div class='panel-footer'>

        <button type='submit' id='loginBtn' class='btn btn-primary'><i class='fa fa-sign-in'></i> Login</button>

			<input type='hidden' name='rememberMe' id='rememberMe' value='<?=(($this->session->userdata('rememberMe')) ? 1 : 0)?>'>

		<button type='button' id='rememberMeBtn' class='btn btn-link pull-right'><i class='fa <?=$remClass?>'></i> Remember Me</button>
    </div>


</form>

</div> <!-- panel -->
<a id="regLink" href="/signup"><h2>Register</h2></a>
</div> <!-- col -->
</div> <!-- .row -->

<?php

	try
	{
		if (!empty($this->session->userdata('company'))) $company = $this->session->userdata('company');

		if (!empty($company)) $fbSync = $this->companies->getTableValue('fbSync', $company);
		
		$fb = (!empty($fbSync)) ? true : false;
	}
	catch (Exception $e)
	{
		PHPFunctions::sendStackTrace($e);
	}

	if ($fb) :
?>
<div class='fbLoginBtnContainer'>
     <a href='javascript:void(0);' class='fbLoginBtn' onclick="fb.login(this, false, false);"><img src='/public/images/fbLogin.png'></a>
</div>
<?php endif; ?>

<!-- forgot password modal //-->
<div id='passwordModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4><i class='fa fa-question'></i> Forgot Password</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='passwordAlert'></div>

        <p class='lead'>Forgot your password? Please enter your e-mail address below.</p>

        <input type='text' class='form-control' name='fpEmail' id='fpEmail' value="" placeholder="email@domain.com">

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelFPBtn'>Cancel</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='submitFPBtn'>Submit</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



