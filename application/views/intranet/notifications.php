<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<a href='#' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-exclamation-circle msg-not-icon'></i> <span class='label label-danger'><?=$cnt?></span>  <span class="caret"></span></a>

<ul class="dropdown-menu not-menu-container">
<?php
// print_r($notifications);

if (empty($notifications))
{
    echo "<li class='notLI'><a href='javascript:void(0);'><h2><i class='fa fa-ban'></i></h2> No Notifications!</a></li>";
}
else
{
    foreach ($notifications as $r)
    {
        echo "<li class='notLI'>";

        echo "<a href='/intranet/gotonot/{$r->id}'>";

        if (!empty($r->icon)) echo "<h2><i class='{$r->icon}'></i></h2> ";

        echo "{$r->msg}</a>";


        echo "</li>" . PHP_EOL;
    }
}

?>
</ul>
