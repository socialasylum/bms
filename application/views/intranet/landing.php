<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>


<h1><i class='fa fa-dashboard'></i> Dashboard</h1>

<?php
/*
          <div class="jumbotron">
            <h1>Welcome!</h1>
            <p>To your company Intranet!</p>
            <p><a href="#" class="btn btn-primary btn-large">Learn more &raquo;</a></p>
          </div>
 */
?>

<div class='row'>
    <ul id='portSort'>

<?php
    $ports = 6;

    for ($i = 1; $i <= $ports; $i++)
    {

    $url = null;

        try
        {
            $winfo = $this->widgets->getPortletWidget($i);

            if (!empty($winfo->widget))
            {
                $url = $this->widgets->getTableValue('url', $winfo->widget);
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<li class='col-lg-6 col-md-6 col-sm-12 col-xs-12 widget' id='widget_{$i}' value='{$i}' widget='0'>" . PHP_EOL;

        echo "\t<input type='hidden' name='portlets[]' id='portlet_{$i}' portlet='{$i}' value='{$winfo->widget}' url=\"{$url}\" idCol='{$winfo->id}'> " . PHP_EOL;

        echo "</li>" . PHP_EOL . PHP_EOL;
    }
?>
    </ul>
</div>


<?php
/*
<menu id="html5menu" type="context" style="display:none" class="showcase">
  <menu label="<i class='icon-plus'></i> Add Widget">
    <?php
    if (!empty($widgets))
    {
        foreach ($widgets as $k => $r)
        {

            // echo "<command label=\"{$r->name}\" onclick=\"intranet.loadWidget('', '');\" id='{$r->id}'></command>";
            echo "<command label=\"{$r->name}\" onclick=\"\" id='{$r->id}'></command>";
        }
    }
    ?>

  </menu>
</menu>
 */
?>
<div id='noWidgetHtml' class='hide'>
    <div class='no-widget'><p><i class='fa fa-ban' style="font-size:90px;line-height:150px"></i></p><p><a href='#' class='btn'>Right click</a> to add a widget.</p></div>
</div>


