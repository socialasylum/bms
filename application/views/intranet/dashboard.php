<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>


<div id='dashboard-grid' class='dashboard-grid'>
	<ul id='gridUL'>
	
	<?php
	if (!empty($ports))
	{
		foreach ($ports as $r)
		{
				$attr = array
				(
					'row' => $r->row,
					'col' => $r->col,
					'dataSizeX' => $r->x,
					'dataSizeY' => $r->y,
					'dataUrl' => $r->url
				);
				
			$this->load->view('intranet/widget_html', $attr);	
		}
	}
	/*
	$ports = 1;
	$cols = 4; // number of widgets that fit in a row
	$rows = 3; // number of rows
	
	for ($i = 0; $i < $ports; $i++)
	{
		$col = floor($i / $rows) + 1;
		
		$row = ($i - (($col - 1) * $rows)) + 1;

		//$row = floor($i / $cols) + 1;
		//$col =  ($i - (($row * $cols) - $cols)) + 1;

		
		$attr = array
		(
			'row' => $row,
			'col' => $col,
			'dataSizeX' => 2,
			'dataSizeY' => 2,
			'dataUrl' => '',
			'cnt' => $i
		);
		
		$this->load->view('intranet/widget_html', $attr);
		
		if (($i + 1) >= ($cols * $rows)) break;
	}
	*/
	?>
		

	</ul>

</div>


<div id='widgetHtml' style='display:none'>
<?php
		$attr = array
		(
			'row' => 1,
			'col' => 1,
			'dataSizeX' => 1,
			'dataSizeY' => 1,
			'dataUrl' => '',
			'cnt' => 0
		);
		
	$this->load->view('intranet/widget_html', $attr);
?>
</div> <!-- /#widgetHtml -->