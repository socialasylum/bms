<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<h1><i class='fa fa-plane'></i> Welcome</h1>

<div class='row'>
	<div class='col-sm-8'>
	



			<p class='lead'>Welcome to your Business Management System</p>
		

			


	</div> <!-- /.col -->

	<div class='col-sm-4'>
	
		<?php if ($access) : ?>
		<div class='panel panel-default'>
			<div class='panel-heading'><h3 class='panel-title'><i class='<?=$menu->icon?>'></i> <?=$menu->name?></h3></div>
			
			<div class='panel-body'>
				<p><button type='button' class='btn btn-info btn-block' onclick="intranet.editMenu('<?=$menu->url?>');"><i class='<?=$menu->icon?>'></i> Edit <?=$menu->name?></button></p>
			
				<p><span class='text-muted'>Click the menu button to make adjustments to the menu.</span></p>	
			</div> <!-- /.panel-body -->
		
		</div> <!-- /.panel -->
		<?php endif; ?>

	</div> <!-- /.col -->


</div> <!-- /.row -->


