<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<ul class='rightclick'>

    <li><i class='fa fa-plus' id='addWidget'></i> Add Widget <span class='pull-right'>&raquo;</span>

<?php
if (!empty($widgets))
{
    echo "<ul class='submenu'>";
    foreach ($widgets as $r)
    {
        $icon = null;

        try
        {
            $icon = $this->modules->getTableValue('icon', $r->module);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<li id='{$r->id}' url=\"{$r->url}\"><i class='{$icon}'></i> {$r->name}</li>";
    }
    echo "</ul>";
}
?>
    </li>

<?php
    try
    {
        $reports = $this->functions->getReportList();
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

    if (!empty($reports)) : 
?>
    <li><i class='fa fa-bar-chart-o'></i> Add Report <span class='pull-right'>&raquo;</span>
        <ul class='submenu'>
        <?php
        foreach ($reports as $r)
        {
            echo "<li>{$r->name}</li>" . PHP_EOL;
        }
        ?>
        </ul>
    </li>
    <?php endif; ?>
    <li action='DELETE'><i class='fa fa-trash-o'></i> Delete Widget</li>
</ul>
