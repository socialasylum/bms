<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<div class='panel panel-default'>
<div class='panel-heading inline'><i class='fa fa-user'></i> Users <button data-dismiss="alert" class="close" type="button">&times;</button></div>
<div class='panel-body widget-body'>

<div id='widget-content'>
<?php
if (!empty($users))
{

echo <<< EOS
    <table class='table table-striped table-bordered' id='usersWidgetTable'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($users as $r)
    {
        $statusDisplay = null;

        try
        {

            // $company = $this->companies->getHomeLocation($r->id, true);

            $assigned = $this->companies->userAssignedToCompany($r->id, $this->session->userdata('company'));

            // home location is not assigned
            if ($assigned == false) continue;

            // only checks if the user being checked is not them
            if ((int) $r->id !== (int) $this->session->userdata('userid'))
            {
                // checks if they are in the current users downline
                $inDownline = $this->users->inDownline($r->id);

                // skips if they are not in downline
                if ($inDownline == false) continue;
            }


            $info = $this->users->getRow($r->id);


            $statusDisplay = $this->functions->codeDisplay(7, $info->status);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr id='userRow{$r->id}'>\n";

        echo "<td>{$r->id}</td>";
        echo "<td>{$info->firstName} {$info->lastName}</td>";
        echo "<td>{$info->email}</td>";
        echo "<td>{$statusDisplay}</td>";

        echo "<td>
            <a href='/user/edit/{$r->id}' class='pull-right'><i class='fa fa-pencil'></i></a>
        </td>";

        echo "</tr>\n";

    }

    echo "</tbody>\n";
    echo "</table>\n";

}
?>



</div> <!-- #widget-content -->

</div> <!-- .widget-body -->

</div> <!-- .panel -->


<div class='clearfix'></div>
