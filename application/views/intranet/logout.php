<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h2 class='modal-title'><i class='icon-time'></i> Currently Clocked In</h2>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='logoutAlert'></div>

        <p class='lead'>Please select how you wish to continue.</p>

<?php
$attr = array
    (
        'name' => 'punchForm',
        'id' => 'punchForm'
    );

echo form_open('#', $attr);
?>
        <input type='hidden' name='addPunch' value='1'>

        </form>


    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='logoutCancelBtn'>Cancel</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='logoutClockOutBtn'>Clockout &amp; Continue</button>
        <button type='button' class='btn btn-warning' aria-hidden='true' id='logoutAnyBtn'><i class='icon-signout'></i> Logout Anyways</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
