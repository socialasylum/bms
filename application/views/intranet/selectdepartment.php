<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <!-- <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> -->
        <h2 class='modal-title'><i class='fa fa-sitemap'></i> Select Department</h2>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='selDepAlert'></div>

        <p class='lead'>Please select which department you want to be assigned to.</p>

        <p><small>*Note: this prompt will only display during this intial setup process</small></p>
<?php
$attr = array
    (
        'name' => 'selDepForm',
        'id' => 'selDepForm',
        'class' => 'form-horizontal'
    );

echo form_open('#', $attr);
?>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='department'>Department</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <select name="department" id="department" class='form-control'>
            <option value=""></option>
<?php
if (!empty($departments))
{
    foreach ($departments as $r)
    {
        echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
    }
}
?>
        </select>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


        </form>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='selDepBtn' onclick="intranet.checkDepartmentSelect(this);">Save &amp; Continue</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
