<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>



	<li class='' data-row="<?=(int) $row?>" data-col='<?=(int) $col?>' data-sizex='<?=$dataSizeX?>' data-sizey='<?=$dataSizeY?>' data-url="<?=$dataUrl?>" data-type='portlet'>
		<div class='dashboard-widget'>
		<div class='dashboard-widget-header'><span class='widget-name'></span> <button class='close-widget' onclick="intranet.deleteWidget(this);">&times</button></div>

		<div class='dashboard-widget-body'>
		
		<div class='dashboard-widget-toolbar' style="height:0px;opacity:0;">
			<button type='button' class='widget-btn'><i class='fa fa-arrow-left'></i></button>
			<button type='button' class='widget-btn' disabled='disabled'><i class='fa fa-arrow-right'></i></button>
					
			<input type='text' name='widgetUrl' id='widgetUrl' class='widgetUrl' placeholder="http://">
			<button type='button' id='delReBtn' class='widget-btn'><i class='fa fa-refresh'></i></button>
			
			<button type='button' id='expandBtn' class='widget-btn pull-right' onclick="intranet.expandWidget(this);"><i class='fa fa-expand'></i></button>
								
		</div> <!-- /.toolbar -->
		

			<div class='display-scroll'>
				<div class='display'>
					
				</div> <!-- /.display -->
			</div> <!-- /.display-container -->
		</div> <!-- #dashboard-widget-body -->
		
		<div class='dashboard-widget-footer'>
			<div class="progress" style="opacity:0;">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
					<span class="sr-only">0%</span>
				</div>
			</div>
		</div> <!-- /.footer -->
		</div> <!-- /.widget -->
	</li>

