<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Content Management System</h1>

<?php
$attr = array('id' => 'cmsForm', 'name' => 'cmsForm');

echo form_open_multipart('#', $attr);
?>

<div class='row'>



	<div class='col-sm-9'>
		<div class='well-white'>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><button type='button' class='btn btn-link navbar-btn' id='saveBtn'><i class='fa fa-save success'></i> Save</button></li>
            <li><button type='button' class='btn btn-link navbar-btn' id='saveBtn'><i class='fa fa-folder-open info'></i> New Folder</button></li>
        </ul>
        
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

		
			<div id='editor'>
				<div class='placeholder'>
					<span><i class='fa fa-file-code-o'></i>Open a file via sitemap, or drop a file here to upload</span>
				</div>
			</div> <!-- /editor -->
	

			
		</div>
	</div> <!-- /.col-8 -->

	<div class='col-sm-3'>
		
		<div class='panel panel-default'>
			<div class='panel-heading'><h3 class='panel-title'><i class='fa fa-cloud-upload'></i> File Upload</h3></div>
			<div class='panel-body'>
				<input type='file' id='upload' name='file' data-path='/'>
			</div> <!-- /.panel-body -->
			
			<div class='panel-footer path-lbl-container'>
				<span class='label labe-info'>&nbsp;</span>
			</div>
		</div> <!-- /.panel -->
		
		<div class='panel panel-default'>
			<div class='panel-heading'><h3 class='panel-title'><i class='fa fa-sitemap'></i> Site Map</h3></div>
			
			<div class='panel-body'>
				<div id='sitemap'><?=$sitemapUL?></div>	
			</div> <!-- /.panel-body -->
			

		</div> <!-- /.panel -->
	</div> <!-- /.col -->

</div> <!-- /.row -->

</form>



<div id='editorPlaceHolderHtml' style='display:none;'>
	<div class='placeholder'>
		<span><i class='fa fa-file-code-o'></i>Open a file via sitemap, or drop a file here to upload</span>
	</div>
</div> <!-- /#editorPlaceHolderHtml -->