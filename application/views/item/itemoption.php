<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>
<!-- HTML for item options -->

<?php if ($show !== true) : ?>
<div id='itemOption' style="display:none;">
<?php endif; ?>

<h4>Custom Item Option</h4>

<!-- <button class='btn btn-danger pull-right'><i class='icon-trash'></i></button> -->


        <input type='hidden' name='option[]' id='option' value=''>

<p>Enter the details of the custom option below. For example color, or size.</p>

    <div class='form-horizontal'>
        <div class="form-group">
            <label class='col-lg-2 control-label' for='name'>Option Name</label>
            <div class="col-lg-10 controls">
                <input type='text' name='optionName' id='optionName' optionCnt='' class='form-control doc-title' value="<?=$name?>" placeholder=''>
            </div>
        </div>


        <div class="form-group">
            <label class='col-lg-2 control-label' for='type'>Selection Type</label>
            <div class="col-lg-10 controls">
                <label class='radio inline'>
                    <input type='radio' name='type' id='type_0' value='0' checked> Can only select <strong>one</strong> option.
                </label>

                <label class='radio inline'>
                    <input type='radio' name='type' id='type_1' value='1'> Can select <strong>multiple</strong> options.
                </label>
            </div>
        </div>

        <!-- <h5>Options</h5> -->

<?php

    $cnt = count($choices);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++) 
    {
        if ($i == 0) echo "<div id='master-option'>";

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        echo <<< EOS
        <div class="form-group">
            <label class='col-lg-2 control-label' for='email'>Option</label>
            <div class="col-lg-10 controls">
                <div class='input-group'>
                <input type='text' class='form-control' name='optionChoice[]' id='optionChoice' value="{$choices[$i]->name}" placeholder=''>
                    <span class='input-group-btn'>
                        <button type='button' class='btn btn-danger' id='delBtn' onclick="item.deleteOption(this);" {$disabled}><i class='fa fa-trash-o'></i></button>
                    </span>
                </div> <!-- .input-group -->
            </div> <!-- col-lg-9 -->
        </div> <!-- .form-group -->


EOS;


        if ($i == 0) echo "</div> <!-- #master-option -->";
    }

?>

        <!-- div that holds dynamically added options -->
        <div id='options-container'></div>

    <div class='col-lg-12'>
        <button type='button' class='btn btn-info pull-right' id='addItemBtn' onclick="item.addOption(this);"><i class='fa fa-plus'></i> Add Option</button>
    </div> <!-- .col-lg-9 .col-lg-offset-2 -->




    </div> <!-- .form-horizontal -->

<?php if ($show !== true) : ?>
</div> <!-- #itemOption -->
<?php endif; ?>
