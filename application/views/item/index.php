<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-shopping-cart'></i> Item Management</h1>

    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='rootFolder' value='<?=$rootFolder?>'>
    <input type='hidden' id='admin' value='<?=(int) $admin?>'>
    <input type='hidden' id='folder' value='<?=$folder?>'>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
<ul class="nav navbar-nav">
    <li><a href='/item/edit/0/<?=$folder?>'><i class='fa fa-plus'></i> New Item</a></li>
    <li><a href='javascript:void(0);' id="createCatBtn"><i class='fa fa-folder-open'></i> New Category</a></li>
</ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<div id='item-container'>
<?php
    if (empty($content) && empty($folder))
    {
        echo $this->alerts->info('No items have been created!');
    }
    else
    {

    // echo "<div class='row-fluid'>\n";


    echo "<ol id='fileList'>\n";

    if (!empty($folder))
    {
        try
        {
            if (!empty($parentFolder)) $parentFolderName = $this->folders->getTableValue('name', $parentFolder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "\t<li class='ui-state-default folder' value='{$parentFolder}' itemType='1' id='folder_{$parentFolder}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>../{$parentFolderName}</div></li>\n";
    }

    $rcnt = 1;
    foreach ($content as $r)
    {
        $img = null;

        #if ($rcnt == 1) echo "<div class='row-fluid'>\n";

        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        if ($r->type == 1)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} folder' value='{$r->id}' itemType='1' id='folder_{$r->id}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>{$name}</div></li>\n";
        }
        elseif ($r->type == 2)
        {

            try
            {
                $img = $this->item->getItemImages($r->id, 1);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $img = null;
            }

            $class = ($r->active == 1) ? null : ' hidden-doc';


            if (empty($img[0]->imageName)) $icon = "<i class='fa fa-tag drag-icon'></i>";
            else $icon = "<img src='/genimg/render/55?img=" . urlencode($img[0]->imageName) . "&path=" . urlencode("uploader/{$this->session->userdata('company')}") . "' class=''>";

            echo "\t<li class='ui-state-default{$class} item' value='{$r->id}' itemType='2' id='item_{$r->id}' itemID='{$r->id}' tbl='item'>{$icon}<div class='docName'>{$name}</div></li>\n";
        }

    }

    echo "</ol>\n";

    // if ($rcnt <= 12) echo "</div>\n <!-- .row-fluid (outside) //-->"; // closes row if not 12 docs


    }
?>
</div>
