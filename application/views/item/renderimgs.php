<?php if(!defined('BASEPATH')) die('Direct access not allowed'); 

if (empty($pictures))
{
    echo $this->alerts->info("No images have been uploaded for this item");
}
else
{

    echo "<ul id='imgSortList' class='formSort'>" . PHP_EOL;

    foreach ($pictures as $k => $file)
    {

        $class = ($k == 0) ? 'yt-primary' : null;

        echo "<li fileName='{$file}'>" . PHP_EOL;

        echo "<img src='/genimg/render/150?img=" . urlencode($file) . "&path=" . urlencode("uploader/{$this->session->userdata('company')}") . "' class='img-thumbnail'>";

        echo "</li>" . PHP_EOL;
    }


    echo "</ul>" . PHP_EOL;
}
