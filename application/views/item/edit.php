<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Item</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save Item</a></li>
            <!-- <li><a href='#settingsModal' data-toggle='modal'><i class='fa fa-cog'></i> Settings</a></li> -->

        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
        <?php if (!empty($id)) : ?>
            <li><a href='javascript:void(0);' class='danger' onclick="item.deleteItem(this, <?=$id?>);"><i class='fa fa-trash-o danger'></i> Delete</a></li>
        <?php endif; ?>

            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>


        </ul>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'method' => 'post'
        );

echo form_open_multipart('#', $attr);

    if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

?>


    <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>

<div class='row form-horizontal'>

    <div class='col-lg-6'>
        <div class="form-group">
            <label class='col-lg-3 control-label' for='name'>Item Name</label>
            <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>" placeholder=''>
            </div>
        </div>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='brand'>Brand</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='brand' id='brand' value="<?=$info->brand?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='model'>Model</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='model' id='model' value="<?=$info->model?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='sku'>SKU #</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='sku' id='sku' value="<?=$info->sku?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

    </div> <!-- .col-lg-6 -->

    <div class='col-lg-6'>

        <div class="form-group">
            <label class='col-lg-3 control-label' for='itemID'>Item ID</label>
            <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='itemID' id='itemID' value="<?=$info->itemID?>" placeholder=''>
            </div>
        </div>

        <div class="form-group">
            <label class='col-lg-3 control-label' for='cost'>Cost</label>
            <div class="col-lg-9 controls">
                <div class='input-group'>
                    <span class='input-group-addon'>$</span>
                    <input type='text' class='form-control' name='cost' id='cost' value="<?=$info->cost?>" placeholder='4.99'>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class='col-lg-3 control-label' for='retailPrice'>Retail Price</label>
            <div class="col-lg-9 controls">
                <div class='input-group'>
                    <span class='input-group-addon'>$</span>
                    <input type='text' class='form-control' name='retailPrice' id='retailPrice' value="<?=$info->retailPrice?>" placeholder='9.99'>
                </div>
            </div>
        </div>



    </div> <!-- .col-lg-6 -->



</div> <!-- .row -->


<div class='row'>

    <div class='col-lg-12'>

        <h2><i class='fa fa-info'></i> Description</h2>

        <textarea name='description' id='description' class='form-control' rows='5'><?=$info->description?></textarea>

    </div> <!-- .col-lg-12 -->

</div> <!-- .row -->

<hr>

<div class='row'>

    <div class='col-lg-12'>

        <h2><i class='fa fa-info'></i> Short Description</h2>
        <p class='lead'>Primarily used during item browsing.</p>

        <textarea name='shortDescription' id='shortDescription' class='form-control' rows='5'><?=$info->shortDescription?></textarea>

    </div> <!-- .col-lg-12 -->

</div> <!-- .row -->






<div class='row'>

    <div class='col-lg-12'>

        <hr>

        <h2><i class='fa fa-gears'></i> Item Options</h2>


    <ul class='nav nav-tabs' id='itemOptionsNav'>
        <li class='active'><a href='#tabPictures' data-toggle="tab"><i class='fa fa-picture-o'></i> Pictures</a></li>
        <li><a href='#tabPromo' data-toggle="tab"><i class='fa fa-star'></i> Promotions</a></li>
        <li><a href='#tabDiscount' data-toggle="tab"><i class='fa fa-tag'></i> Discounts</a></li>
<?php
if (!empty($id))
{
    if (!empty($options))
    {
        $cnt = 1;
        foreach ($options as $r)
        {
            echo "<li><a href='#optionTab{$cnt}' data-toggle='tab'><i class='fa fa-cog'></i> {$r->name}</a></li>" . PHP_EOL;

        $cnt++;
        }
    }
}
?>

        <li><a href='javascript:void(0);' id='addItemOption'><i class='fa fa-plus'></i> Add Item Option</a></li>
    </ul>

<div class='tab-content crm-notes' id='tab-content'>
    <div class='tab-pane form-horizontal active' id='tabPictures'>

    <div class='row'>
            <div class='col-lg-12'>
                    <h3><i class='fa fa-picture-o'></i> Upload Image</h3>
                    <p class='lead'>Browse to upload an image of this item.</p>

<div class='row'>
<div id='uploadedImgDisplay' class='col-lg-12'>
</div> <!-- #uploadedImgDisplay -->
</div>

<div id='uploadedImgs'>
<?php
if (!empty($images))
{
    foreach ($images as $r)
    {
        echo "<input type='hidden' name='img[]' id='img' value='{$r->imageName}'>" . PHP_EOL;
    }
}
?>
</div> <!-- #uploadedImgs -->


<div id='deleteDropZone' class='delete-drop-zone danger' style='display:none;'><i class='fa fa-trash-o'></i> Drop here to delete image.</div>

<hr>
<button type='button' id='uploadImgBtn' name='uploadImgBtn' class='btn btn-info'><i class='fa fa-upload'></i> Browse ...</button>
<?php
/*
<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='image'>Image</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <button type='button' id='uploadImgBtn' name='uploadImgBtn' class='btn btn-info'><i class='fa fa-upload'></i> Browse ...</button>
    </div> <!-- .controls -->
</div> <!-- .form-group -->
 */
?>
            </div> <!-- .col-lg-6 -->
    </div><!-- .row -->




    </div> <!-- #tabPictures -->

    <div class='tab-pane' id='tabPromo'>
        <h2><i class='fa fa-star'></i> Promotions</h2>

    <p class='lead'>Below is a list of all active promotions to which you can assign this item to.</p>
<?php
if (empty($promos))
{
    echo $this->alerts->info("No promotions have been created!");
}
else
{

    foreach ($promos as $r)
    {
        $checked = null;

        try
        {
            $desc = $this->promo->getPromoDesc($r->id);

            $assigned = $this->promo->checkItemPromoAssigned($id, $r->id);
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $checked = ($assigned == true) ? "checked='checked'" : null;

        echo "<div class='well'>" . PHP_EOL;
        echo "<h4><input type='checkbox' name='promo[]' id='promo_{$r->id}' value='{$r->id}' {$checked}> <label for='promo_{$r->id}'>{$r->name}</label></h4>";

        echo $desc;

        echo "</div>" . PHP_EOL;

        // echo "<hr>";
    }
}
?>


    </div> <!-- #tabPromo -->

    <div class='tab-pane' id='tabDiscount'>
        <h2><i class='fa fa-tag'></i> Discounts</h2>

    </div> <!-- #tabDiscount -->

<?php
if (!empty($id))
{
    if (!empty($options))
    {
        $cnt = 1;
        foreach ($options as $r)
        {
            // loads view for tab pane

            try
            {
                // gets item option choices
                $attr['choices'] = $this->item->getItemOptionChoices($r->id);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

            $attr['show'] = true;
            $attr['name'] = $r->name;


            echo "<div class='tab-pane' id='optionTab{$cnt}'>" . PHP_EOL;
            $this->load->view('item/itemoption', $attr);
            echo "</div>" . PHP_EOL;
        
        $cnt++;
        }
    }
}

    unset($attr); //unsets attr variable
?>


</div> <!-- .tab-content -->



    </div> <!-- .col-lg-12 -->

</div> <!-- .row -->



<?php
/*
<div class='row'>

    <div class='col-lg-12 form-actions'>
        <button type='button' class='btn btn-primary' id='saveBtn'><i class='fa fa-save'></i> Save</button>
        <button type='button' class='btn btn-default' id='cancelBtn'><i class='fa fa-ban'></i> Cancel</button>
    </div> <!-- .col-lg-12 -->

</div> <!-- .row -->
*/
?>


</form>


<?php include 'itemoption.php'; ?>
