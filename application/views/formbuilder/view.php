<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>

<div class='row'>
    <div class='col-lg-8 form-horizontal'>
    <h1><?=$info->title?></h1>

    <?=$info->briefIns?>
<?php
    $attr = array
        (
            'name' => 'formbuilderForm',
            'id' => 'formbuilderForm'
        );

    echo form_open('#', $attr);


    echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

    //  echo $this->formbuilder->buildForm($info->formHtml);

    try
    {
        $html = $info->formHtml;

        $html = $this->formbuilder->stripSortTags($html);

        echo $html;
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

?>
<hr>

            <button type='button' class='btn btn-primary' name='saveCustFormBtn' id='saveCustFormBtn'>Submit</button>
            <button type='button' class='btn' name='resetBtn' id='resetBtn'>Reset</button>
</form>

    </div> <!-- .col-8 -->

    <div class='col-lg-4'>

        <div class='panel panel-default'>
            <div class='panel-heading inline'>Instructions</div>
            <div class='panel-body'>
                <?=$info->instructions?>
            </div> <!-- .panel-body -->
        </div> <!-- .panel -->

    </div>
</div>
 
