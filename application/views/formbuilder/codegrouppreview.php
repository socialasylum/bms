<?php if(!defined('BASEPATH')) die('Direct access not allowed');

if (empty($codes))
{
    echo $this->alerts->info('There are no codes in this group for you to preview.');
}
else
{
    echo <<< EOS
    <hr>
    <h4>Code Group Preview</h4>

    <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th>Value</th>
                <th>Display</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($codes as $r)
    {
        echo "<tr>" . PHP_EOL;

        echo "\t<td>{$r->code}</td>" . PHP_EOL;
        echo "\t<td>{$r->display}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
