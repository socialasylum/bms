<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<ul class='rightclick'>
    <li action='EDIT'><i class='fa fa-edit'></i> Edit</li>
    <li action='DELETE'><i class='fa fa-trash-o'></i> Delete</li>
</ul>
