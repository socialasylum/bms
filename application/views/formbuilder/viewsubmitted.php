<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-code'></i> Forms</h1>

    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>
    <input type='hidden' name='formID' id='formID' value='<?=$info->form?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>




<div class='row'>
    <div class='col-lg-8 col-m-8 col-s-12 col-xs-12 form-horizontal'>
        <h1><?=$info->formTitle?></h1>

    <?=$formInfo->briefIns?>
<?php

    echo $info->formHtml;
?>


    </div> <!-- col-8/12 -->

    <div class='col-lg-4 col-m-4 col-s-12 col-xs-12'>

        <div class='panel panel-default'>
            <div class='panel-heading inline'><i class='fa fa-exclamation-circle'></i> Form Information</div>
            <div class='panel-body'>
                <dl class='dl-horizontal'>

                    <dt>Submitted By</dt>
                    <dd><?=$submitName?></dd>

                    <dt>Date Received</dt>
                    <dd><?=$date?></dd>

                    <dt>Status</dt>
                    <dd><label class='label label-info'>Pending Approval Decision</label></dd>
                </dl>

            </div> <!-- .panel-body -->

            <div class='panel-footer'>
                <a href='/formbuilder' class='btn btn-primary btn-sm'>&laquo; Return to Forms</a>
            </div>
        </div> <!-- .panel -->




        <div class='panel panel-default'>
            <div class='panel-heading'>Approvals</div>
            <div class='panel-body'>
                <h3>Required Approvals: <label class='label label-info'><?=$formInfo->approvals?></label></h3>
<?php

$sigCnt = 0;

if (empty($formInfo->approvals))
{
    echo "<label class='label labe-info'>Form does not require any level of approvals</label>" . PHP_EOL;
}
else
{
    for ($i = 1; $i <= $formInfo->approvals; $i++)
    {
        $signName = null;

        try
        {
            // will check if there is a signature
            $sigInfo = $this->formbuilder->getSigLevelInfo($id, $i);

            if (empty($sigInfo))
            {
                //will now check if user has permission to sign this level
                $canSign = $this->formbuilder->canSignlvl($info->form, $i);

                if ($canSign == false)
                {
                    $disabled = "disabled='disabled'";
                }
            }
            else
            {
                // there is signature data
                $signName = $this->users->getName($sigInfo->userid);
                
                $date = $this->users->convertTimezone($this->session->userdata('userid'), $sigInfo->datestamp, "m/d/Y g:i A");
            }
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            break;
        }

echo <<< EOS
    <div class='row approvalLvl'>
        <div class='col-lg-5 col-m-5 col-s-5 col-xs-5'>
            <strong>Level {$i}</strong>
        </div>

        <div class='col-lg-7 col-m-7 col-s-7 col-xs-7'>
EOS;

            if (!empty($sigInfo))
            {
                echo "<p><strong>{$signName}</strong> {$date}</p>";
                $sigCnt++;
            }
            else
            {
                echo "<button class='btn btn-success btn-sm' onclick=\"formbuilder.approveLvl({$id}, {$i}, 0);\" {$disabled}><i class='fa fa-thumbs-up'></i> Approve</button> ";
                echo "<button class='btn btn-danger btn-sm' onclick=\"formbuilder.denyLvl({$id}, {$i}, 0);\" {$disabled}><i class='fa fa-thumbs-down'></i> Deny</button>";
            }
            echo "</div>
    </div>";

        // ends at the level is has no signature data for
        if (empty($sigInfo)) break;
    }
}

    if  ($sigCnt == $formInfo->approvals)
    {

        if ($final === false)
        {
        $finalCnt = $sigCnt + 1;

echo <<< EOS
    <hr>
    <div class='row approvalLvl'>
        <div class='col-lg-5 col-m-5 col-s-5 col-xs-5'>
            <strong>Process</strong>
        </div>

        <div class='col-lg-7 col-m-7 col-s-7 col-xs-7'>
            <button class='btn btn-success btn-sm' onclick="formbuilder.approveLvl({$id}, {$finalCnt}, 1);" {$disabled}><i class='fa fa-thumbs-up'></i> Approve</button>
            <button class='btn btn-danger btn-sm' onclick="formbuilder.denyLvl({$id}, {$finalCnt}, 1);" {$disabled}><i class='fa fa-thumbs-down'></i> Deny</button>
        </div>
    </div>
EOS;
        }
        else
        {
            try
            {
                // there is signature data
                $signName = $this->users->getName($final->userid);

                $date = $this->users->convertTimezone($this->session->userdata('userid'), $final->datestamp, "m/d/Y g:i A");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

echo <<< EOS
    <hr>
    <div class='row approvalLvl'>
        <div class='col-lg-5 col-m-5 col-s-5 col-xs-5'>
            <strong>Process</strong>
        </div>

        <div class='col-lg-7 col-m-7 col-s-7 col-xs-7'>
            <p><strong>{$signName}</strong> {$date}</p>
        </div>
    </div>
EOS;
        }
    }


                // echo $this->alerts->info("This form does not require any approvals");
?>

<!--
<div align='center'>
    <button class='btn btn-success'><i class='fa fa-thumbs-up'></i> Approve</button>
    <button class='btn btn-danger'><i class='fa fa-thumbs-down'></i> Deny</button>
</div>
-->

            </div> <!-- .panel-body -->
        </div> <!-- .panel -->


    </div>
</div><!-- .row -->

<div id='formPostData' style="display:none"><?=$info->postdata?></div>
