<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>


<h1><i class='fa fa-code'></i> Form Builder</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save Form</a></li>
            <li><a href='#settingsModal' data-toggle='modal'><i class='fa fa-cog'></i> Settings</a></li>
            <li><a href='#approvalsModal' data-toggle='modal'><i class='fa fa-group'></i> Approvals</a></li>
            <li><a href='javascript:void(0);' onclick="formbuilder.clearFormHtml();"><i class='fa fa-eraser'></i> Clear Form</a></li>

        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' onclick="formbuilder.cancelForm();"><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>


        <?php if (!empty($id)) : ?>
        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' class='danger' onclick="formbuilder.deleteForm(<?=$id?>);"><i class='fa fa-trash-o danger'></i> Delete</a></li>
        </ul>
        <?php endif; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<p class='lead'>Below you can drag and drop elements to create your own custom form.</p>


<hr>

<div class='row'>
    <div id='dynamic-form' class='col-lg-7'>

        <div id='formTitle' contenteditable='true'>
<?php
    if (empty($id)) echo "<h1>Form Name</h1>" . PHP_EOL;
    else echo "<h1>{$info->title}</h1>" . PHP_EOL;
?>
        </div>


        <div id='bInst' contenteditable='true'>
<?php
    if (empty($id) OR empty($info->briefIns)) :
?>
    <p class='lead'>Brief Instructions <small>(Optional - click to Edit)</small></p>
<?php
    else :
        echo $info->briefIns;
    endif;
?>

        </div>


        <div id='form-content'></div>

            <div class='col-lg-12 module-drop-zone' id='formDropZone'>Drop Elements Here</div>

        <hr>

        <div class='row'>
            <div class='col-lg-12'>
                <button class='btn btn-primary' disabled='disabled'>Submit</button>
                <button class='btn btn-default' disabled='disabled'>Reset</button>
            </div>
        </div>

    </div> <!-- #dynamic-form -->

    <div class='col-lg-5'>

    <div class='panel panel-default'>
        <div class='panel-heading inline'>Form Elements</div>
        <div class='panel-body form-horizontal' id='form-elements'>

        <p class='lead'>Below are the elements you can drag into your custom form.</p>

        <p>Simply click and drag which elements you wish to add to your form.</p>

        <hr>



    <ol id='elementList'>
        <li class='ui-state-default' value='1' id='1' file="">
            <div class="form-group" options='0'>
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>Text Field</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                        <input type='text' class='form-control' name="" id="" placeholder="">
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
        </li>

        <li class='ui-state-default' value='2' id='2' file="">
            <input type='hidden' name='optionType[]' id='optionType' value='0'>
            <input type='hidden' name='optionTypeValue[]' id='optionTypeValue' value='0'>
            
            <div class="form-group" options='1'>
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>Select</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <select class='form-control' name="" id="">
                    </select>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
        </li>

        <li class='ui-state-default' value='3' id='3' file="">
            <div class="form-group" options='0'>
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>Textarea</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <textarea class='form-control' name="" id=""></textarea>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
        </li>

        <li class='ui-state-default' value='4' id='4' file="">
            <div class="form-group" options='0'>
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>Password</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                        <input type='password' class='form-control' placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
        </li>

        <li class='ui-state-default' value='5' id='5' file="">
            <input type='hidden' name='optionType[]' id='optionType' value='0'>
            <input type='hidden' name='optionTypeValue[]' id='optionTypeValue' value='0'>
            <div class="form-group" options='1'>
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>Radio</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                
                	<div class='radio'>
                		<label>
		                    <input type='radio' name='' id='' value=''> Option 1
                		</label>
                	</div>
                	
                	<div class='radio'>
                		<label>
							<input type='radio' name='' id='' value=''> Option 2
	              		</label>
                	</div>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
        </li>


        <li class='ui-state-default' value='6' id='6' file="">
            <input type='hidden' name='optionType[]' id='optionType' value='0'>
            <input type='hidden' name='optionTypeValue[]' id='optionTypeValue' value='0'>
            <div class="form-group" options='1'>
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>Checkbox</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='checkbox' name='' id='' value=''> Option 1
                    <input type='checkbox' name='' id='' value=''> Option 2
                </div> <!-- .controls -->
            </div> <!-- .form-group -->
        </li>
    </ol>


        </div> <!-- .panel-body -->

        </div> <!-- .panel -->

    </div> <!-- col-lg-4 -->

</div> <!-- .row -->


<?php

$attr = array
(
    'name' => 'formForm',
    'id' => 'formForm',
);

echo form_open('#', $attr);
?>


    <?php if (!empty($id)) : ?>
        <input type='hidden' name='id' id='id' value='<?=$id?>'>
    <?php endif; ?>

<!-- settings modal //-->
<div id='settingsModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-cog'></i> Settings</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='settingsAlert'></div>


<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='active'>Status</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <select class='form-control' name='active' id='active'>
        <?php
            foreach($status as $code => $display)
            {
                // new documents, sets to active by default
                if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                else $sel = ($info->active == $code) ? 'selected' : null;

                echo "<option {$sel} value='{$code}'>{$display}</option>\n";
            }
        ?>
        </select>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='instructions'>Instructions</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <textarea id='instructions' name='instructions' class='form-control'><?=$info->instructions?></textarea>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->




<!-- approvals modal //-->
<div id='approvalsModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-group'></i> Approvals</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='approvalAlert'></div>

    <p class='lead'>Here you can select how many levels of approvals the form will require before it's approved.</p>

    <input type='hidden' name='applvl' id='applvl' value='0'>


<div class='row'>
<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='approvals'>Approval Levels</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <select class='form-control' name="approvals" id="approvals">
            <?php
for ($i = 0; $i <= 5; $i++)
{
    $sel = ($info->approvals == $i) ? 'selected' : null;

    echo "<option {$sel} value='{$i}'>{$i}</option>" . PHP_EOL;
}

            ?>
        </select>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

</div>

<hr>

    <div id='approval-display'></div>


    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->


</form>



<div id='formPropPanel' style="display:none;"></div>
