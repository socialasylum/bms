<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
if (empty($cnt)) :
    echo $this->alerts->info("This form does not require any approval");
else :

    for ($i = 1; $i <= $cnt; $i++)
    {
?>

<div class='panel panel-default'>
    <div class='panel-heading'>Approval Level <?=$i?></div>
    <div class='panel-body'>


<?php
if (!empty($positions))
{
    foreach ($positions as $r)
    {

        $checked = null;

        // checks if position has approval for this lvl
        if (!empty($approvals))
        {
            foreach ($approvals as $ar)
            {
                if ((int) $ar->lvl == (int) $i)
                {
                    // position to be checked is for this lvl
                    if ($r->id == $ar->position)
                    {
                        $checked = "checked='checked'";
                        break;
                    }
                }

            }
        }


        echo "<label class='checkbox'>";

        echo "<input type='checkbox' name='position[{$i}][]' id='position_{$i}_{$r->id}' value='{$r->id}' {$checked}> {$r->name}";

        echo "</label>" . PHP_EOL;
    }
}
?>
        </div> <!-- .panel-body -->

        </div> <!-- .panel -->


<?php
    }
endif; ?>
