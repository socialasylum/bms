<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
if (empty($formHtml))
{
    echo $this->alerts->info("This form is empty!");
}
else
{


    echo "<div id='formHtmlContainer' class='form-horizontal'>" . PHP_EOL;

    echo "<ul id='formSort'>" . PHP_EOL;

    echo $formHtml;

    echo "</ul>" . PHP_EOL;

    echo "</div>" . PHP_EOL;

}
?>
