<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='form-horizontal'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' onclick="formbuilder.closeRightClick();">&times;</button>
        <h3 class='modal-title'>Field Properties</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='approvalAlert'></div>


<div class='tabbable'>
    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabGen' data-toggle="tab"><i class='fa fa-cog'></i> General</a></li>

        <?php if ($options == true) : ?>
                <li><a href='#tabOptions' data-toggle="tab"><i class='fa fa-wrench'></i> Options</a></li>
        <?php endif; ?>

        <li><a href='#tabAdv' data-toggle="tab"><i class='fa fa-cogs'></i> Advanced</a></li>
    </ul>



<div class="tab-content">

    <div id="tabGen" class='tab-pane active'>

    <p class='lead'>Below are some basic settings about this input field.</p>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='lbl'>Label</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='lbl' id='lbl' value="" placeholder=''>
    </div> <!-- .controls -->
</div> <!-- .form-group -->



<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='name'>Name</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='name' id='name' value="" placeholder=''>
    </div> <!-- .controls -->
</div> <!-- .form-group -->



<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='id'>ID</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='id' id='id' value="" placeholder=''>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='placeholder'>Placeholder</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='placeholder' id='placeholder' value="" placeholder=''>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='helpTxt'>Help Txt</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='helpTxt' id='helpTxt' value="">
    </div> <!-- .controls -->
</div> <!-- .form-group -->


        </div> <!-- #tabGen -->

<?php if ($options == true) : ?>

        <div id='tabOptions' class='tab-pane'>


        <input type='hidden' name='optionType' id='optionType' value='1'>

            <p class='lead'>Below you can edit the options for this element.</p>

<div class='panel-group' id='optionAcc'>

    <div class='panel panel-default'>
        <div class='panel-heading'>
            <h4 class='panel-title'><a data-toggle='collapse' data-parent='#optionAcc' href='#option1Tab' onclick="formbuilder.setOptionType(1);"><i class='fa fa-list'></i> Codes</a></h4>
        </div> <!-- .panel-heading -->


        <div id='option1Tab' class='panel-collapse collapse in' value='1'>
            <div class='panel-body'>
            <p>Use site codes database table</p>

        <select name='codeGroup' id='codeGroup' class='form-control'>
            <option value=''></option>
<?php
if (!empty($groups))
{
    foreach ($groups as $r)
    {
        echo "<option value='{$r->group}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
        </select>

                <div id='codeGroupPreview'></div>

            </div> <!-- .panel-body -->
        </div> <!-- #option1Tab -->
    </div> <!-- .panel -->

    <div class='panel panel-default'>
        <div class='panel-heading'>
            <h4 class='panel-title'><a data-toggle='collapse' data-parent='#optionAcc' href='#option2Tab' onclick="formbuilder.setOptionType(2);"><i class='fa fa-hdd'></i> Datasets</a></h4>
        </div> <!-- .panel-heading -->


        <div id='option2Tab' class='panel-collapse collapse' value='2'>
            <div class='panel-body'>
            <p>Use a Dataset that was defined in the reporting module.</p>

        <select name='dataset' id='dataset' class='form-control'>
            <option value=''></option>
<?php
if (!empty($datasets))
{
    foreach ($datasets as $r)
    {
        echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
    }
}
?>
        </select>


            </div> <!-- .panel-body -->
        </div> <!-- #option2Tab -->
    </div> <!-- .panel -->

    <div class='panel panel-default'>
        <div class='panel-heading'>
            <h4 class='panel-title'><a data-toggle='collapse' data-parent='#optionAcc' href='#option3Tab' onclick="formbuilder.setOptionType(3);"><i class='fa fa-edit'></i> Custom</a></h4>
        </div> <!-- .panel-heading -->


        <div id='option3Tab' class='panel-collapse collapse' value='3'>
            <div class='panel-body'>

            <div id='customOptionsContainer'>
                <ul id='optionsSortList' class='formSort'>
                    <li>
                    <div class='row'>
                        <div class='col-lg-5 col-m-5 col-s-5 col-xs-5'>
                            <input type='text' name='key[]' id='key' class='form-control' value="" placeholder='Value'>
                        </div>
                        <div class='col-lg-5 col-m-5 col-s-5 col-xs-5'>
                            <input type='text' name='value[]' id='value' class='form-control' value="" placeholder='Display'>
                        </div>
                        <div class='col-lg-2 col-m-2 col-s-2 col-xs-2'>
                            <button type='button' class='btn btn-danger' name='removeOptionBtn' id='removeOptionBtn' disabled='disabled' onclick="formbuilder.removeCustomOption(this);"><i class='fa fa-trash-o'></i></button>
                        </div>

                    </div> <!-- .row -->
                    </li>
                </ul>
            </div> <!-- #customOptionsContainer -->

            <hr>
                <button type='button' class='btn btn-info' name='addCustomBtn' id='addCustomBtn'><i class='fa fa-plus'></i> Add Option</button>

            </div> <!-- .panel-body -->
        </div> <!-- #option3Tab -->
    </div> <!-- .panel -->





</div> <!-- .panel-group -->

<?php
/*

    <div class='well'>

                <label for='option_1'>
                    <input type='radio' name='option[]' id='option_1' value='1'> Option 1
                </label>
            <p>Use site codes database table</p>

        <select name='codeGroup' id='codeGroup' class='form-control'>
            <option value=''></option>
<?php
if (!empty($groups))
{
    foreach ($groups as $r)
    {
        echo "<option value='{$r->group}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
        </select>

    </div> <!-- .well -->

    <div class='well'>
                <label for='option_2'>
                    <input type='radio' name='option[]' id='option_2' value='2'> Option 2
                </label>
            <p>Use a Dataset</p>

        <select name='dataset' id='dataset' class='form-control'>
            <option value=''></option>
<?php
if (!empty($datasets))
{
    foreach ($datasets as $r)
    {
        echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
    }
}
?>
        </select>


    </div> <!-- .well -->
        <fieldset>
            <legend>
                <label for='option_3'>
                    <input type='radio' name='option[]' id='option_3' value='3'> Option 3
                </label>
                </legend>
            <p>Custom</p>
        </fieldset>

*/
?>
        </div> <!-- #tabOptions -->
<?php endif; ?>

        <div id='tabAdv' class='tab-pane'>

            <p class='lead'>Below are some more advanced settings about this input field.</p>

<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='class'>Class</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='class' id='class' value="" placeholder='form-control'>
    </div> <!-- .controls -->
</div> <!-- .form-group -->



        </div> <!-- #tabAdv -->

        </div> <!-- .tab-content -->
        </div> <!-- .tabbable -->

        </div> <!-- .modal-body -->

    <div class='modal-footer'>

        <button class='btn btn-danger pull-left' id='delBtn'><i class='fa fa-trash-o'></i></button>

        <button class='btn btn-default' id='closeBtn' onclick="formbuilder.closeRightClick();">Cancel</button>
        <button class='btn btn-primary' id='saveBtn'>Save</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->


</div> <!-- .form-horizontal -->

