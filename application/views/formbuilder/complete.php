<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-thumbs-o-up'></i> Form Received</h1>

<p class='lead'>The <?=$title?> form your have submitted has been received!</p>
<hr>

<div align='center'>
    <button type='button' class='btn btn-primary btn-lg' name='returnBtn' id='returnBtn'>Return to Forms</button>
</div>
