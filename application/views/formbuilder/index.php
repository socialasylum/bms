<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<h1><i class='fa fa-code'></i> Forms</h1>

    <input type='hidden' id='rootFolder' value='<?=$rootFolder?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='/formbuilder/edit'><i class='fa fa-file'></i> Create new form</a></li>
            <li><a href='javascript:void(0);' id='newFolderBtn'><i class='fa fa-folder-open'></i> New folder</a></li>
        </ul>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<div class='tabbable'>
    <ul class='nav nav-tabs' id='userTabs'>
        <li class='active'><a href='#tabForms' data-toggle="tab"><i class='fa fa-file'></i> Forms</a></li>
        <li><a href='#tabSubmittedForms' data-toggle="tab"><i class='fa fa-upload'></i> Submitted Forms</a></li>

    </ul>



<div class="tab-content">

    <div id="tabForms" class='tab-pane active'>


<input type='hidden' id='folder' value='<?=$folder?>'>

<div id='form-container'>
<?php
    if (empty($content))
    {
        echo $this->alerts->info('No forms have been created!');
    }
    else
    {

    // echo "<div class='row'>\n";


    echo "<ol id='fileList'>\n";

    $rcnt = 1;
    foreach ($content as $r)
    {

        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        if ($r->type == 1)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} folder' value='{$r->id}' itemType='1' id='folder_{$r->id}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>{$name}</div></li>\n";
        }
        elseif ($r->type == 2)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class} form' value='{$r->id}' itemType='2' id='doc_{$r->id}'><i class='fa fa-file drag-icon'></i><div class='docName'>{$name}</div></li>\n";
        }

    }

    echo "</ol>\n";

    }
?>
</div> <!-- #form-container -->


        </div> <!-- tabForms -->

        <div id="tabSubmittedForms" class='tab-pane'>
            <h2><i class='fa fa-upload'></i> Submitted Forms</h2>

<?php
if (empty($submittedForms))
{
    echo $this->alerts->info("No forms have been filled out and submitted.");
}
else
{
    echo <<< EOS
     <table class='table table-hover' id='submittedFormsTbl'>
        <thead>
            <tr>
                <th>Form</th>
                <th>User</th>
                <th>Date</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
EOS;


    foreach ($submittedForms as $r)
    {
        $userName = $date = null;

        try
        {
            $userName = $this->users->getName($r->userid);

            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "m/d/Y g:i A");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr onclick=\"formbuilder.viewSubmittedForm({$r->id});\">" . PHP_EOL;

        echo "\t<td>{$r->formTitle}</td>" . PHP_EOL;
        echo "\t<td>{$userName}</td>" . PHP_EOL;
        echo "\t<td>{$date}</td>" . PHP_EOL;
        echo "\t<td>Pending</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }


    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>


        </div> <!-- #tabSubmittedForms -->

    </div> <!-- .tab-contents //-->
</div> <!-- .tabbable //-->



