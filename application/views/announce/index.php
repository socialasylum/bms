<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php if ($widget == true) : ?>
<div class='panel panel-default'>
    <div class='panel-heading inline'><i class='fa fa-bullhorn'></i> Announcements <button onclick="" class="close" type="button">&times;</button></div>
    <div class='panel-body widget-body'>

    <div id='widget-content'>
<?php else : ?>

<h1><i class='fa fa-bullhorn'></i> Announcements</h1>
<?php endif; ?>

<input type='hidden' id='<?=$this->security->get_csrf_token_name()?>' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' name='module' id='module' value='<?=$modId?>'>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <?php if ($editPerm == true) : ?>
    <ul class="nav navbar-nav">
        <li><a href='/announce/edit'><i class='fa fa-plus'></i> New Announcement</a></li>
    </ul>
    <?php endif; ?>

    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php if ($widget !== true) include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php
if (empty($announcements))
{
    echo $this->alerts->info("There are no Announcements!");
}
else
{

    if ($editPerm === true) $editCol = "<th>&nbsp;</th>" . PHP_EOL;

echo <<< EOS
    <table class='table' id='annTbl'>
        <thead>
            <tr>
                <th>Title</th>
                <th>From</th>
                <th>Publish Date</th>
                <th>Start Date</th>
                <th>End Date</th>
                {$editCol}
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($announcements as $r)
    {
        $fromNamee = $startDate = $endDate = $date = null;

        try
        {
            $fromName = $this->users->getName($r->userid);

            $startDate = $this->users->convertTimezone($this->session->userdata('userid'), $r->startDate, "Y-m-d");
            $endDate = $this->users->convertTimezone($this->session->userdata('userid'), $r->endDate, "Y-m-d");
            
            $fdate = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "Y-m-d g:i A");


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "<td onclick=\"announce.view({$r->id});\">{$r->title}</td>";
        echo "<td onclick=\"announce.view({$r->id});\">{$fromName}</td>";
        echo "<td onclick=\"announce.view({$r->id});\">{$fdate}</td>";
        echo "<td onclick=\"announce.view({$r->id});\">{$startDate}</td>";
        echo "<td onclick=\"announce.view({$r->id});\">{$endDate}</td>";

        if ($editPerm === true)
        {
            echo "<td>
             <a href='/announce/edit/{$r->id}' alt='Edit Announcement' class='pull-right'><i class='fa fa-edit'></i></a>
             <!-- <a href='javascript:void(0);' alt='View Announcement Properties' class='pull-right'><i class='fa fa-cog'></i></a> -->
             </td>";
        }
        echo "</tr>" . PHP_EOL;

    }


echo "</tbody>";
echo "</table>";
}

?>

<?php if ($widget == true) : ?>
    </div> <!-- #widget-content -->
    </div> <!-- .widget-body -->
    </div> <!-- .panel -->
<?php endif; ?>
