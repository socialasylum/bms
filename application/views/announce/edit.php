<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Announcement</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' name='module' id='module' value='<?=$modId?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
            <li><a href="javascript:void(0);" id='settingsBtn'><i class='fa fa-cog'></i> Settings</a></li>
            <li><a href="javascript:void(0);" id='posAccessBtn'><i class='fa fa-group'></i> Position Access</a></li>
            <li><a href="javascript:void(0);" id='locAccessBtn'><i class='fa fa-building'></i> Location Access</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm'
        );

echo form_open('#', $attr);


if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

?>


<div id='posAccessInputs'>
<?php
if (!empty($id) && !empty($posAccess))
{
    foreach ($posAccess as $r)
    {
        echo "<input type='hidden' name='position[]' id='position_{$r->position}' value='{$r->position}'>" . PHP_EOL;
    }
}
?>
</div> <!-- #posAccessInputs -->

<div id='terAccessInputs'>
<?php
if (!empty($id) && !empty($terAccess))
{
    foreach ($terAccess as $r)
    {
        echo "<input type='hidden' name='territory[]' id='territory_{$r->territory}' value='{$r->territory}'>" . PHP_EOL;
    }
}
?>
</div>  <!-- #terAccessInputs -->

<div id='locAccessInputs'>
<?php
if (!empty($id) && !empty($locAccess))
{
    foreach ($locAccess as $r)
    {
        echo "<input type='hidden' name='location[]' id='location_{$r->location}' value='{$r->location}'>" . PHP_EOL;
    }
}
?>
</div> <!-- #locAccessInputs -->

<div class='row announce-subject'>
    <div class='col-lg-12 col-m-12 col-s-12 col-xs-12'>
        <input type='text' class='form-control' name='title' id='title' value="<?=$info->title?>" placeholder='Subject'>
    </div> <!-- .col-12 -->
</div> <!-- .row -->


<div class='row'>
    <div class='col-lg-12 col-m-12 col-s-12 col-xs-12'>
        <textarea class='form-control' rows='5' name='announcement' id='announcement'><?=$info->body?></textarea>
    </div> <!-- .col-12 -->
</div> <!-- .row -->



<!-- Settings modal //-->
<div id='settingsModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-cog'></i> Settings</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='settingsAlert'></div>

        <p class='lead'>Here you can adjust the general settings of this announcement.</p>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='active'>Status</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select class='form-control' name='active' id='active'>
                <?php
                    foreach($status as $code => $display)
                    {
                        // new documents, sets to active by default
                        if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                        else $sel = ($info->active == $code) ? 'selected' : null;

                        echo "<option {$sel} value='{$code}'>{$display}</option>\n";
                    }
                ?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='startDate'>Start Date</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <?php
            try
            {
                if(!empty($id)) $startDate = $this->users->convertTimezone($this->session->userdata('userid'), $info->startDate, "Y-m-d");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
            ?>
            <input type='text' class='form-control' name='startDate' id='startDate' value="<?=(empty($id)) ? date("Y-m-d") : $startDate?>" placeholder='<?=date("Y-m-d")?>'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='endDate'>End Date</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <?php
            try
            {
                if(!empty($id)) $endDate = $this->users->convertTimezone($this->session->userdata('userid'), $info->endDate, "Y-m-d");
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
            ?>
                <input type='text' class='form-control' name='endDate' id='endDate' value="<?=(empty($id)) ? date("Y-m-d", strtotime("+ 1 month")) : $endDate?>" placeholder='<?=date("Y-m-d", strtotime("+ 1 month"))?>'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->




    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' data-dismiss='modal' class='btn btn-info' aria-hidden='true' id='saveBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->





<!-- Position Access modal //-->
<div id='posAccessModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-group'></i> Position Access</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body' id='posAccessBody'>
        <div id='posAccessAlert'></div>

        <p class='lead'>Here you can select which positions have access to view this announcement.</p>
<?php
// print_r($positions);
if (!empty($positions))
{
    foreach ($positions as $r)
    {
        echo "<button type='button' class='btn btn-default btn-checkbox' onclick=\"global.toggleCheckboxBtn(this);\" id='posAccessBtn_{$r->id}' value='{$r->id}'>{$r->shortName}</button>" . PHP_EOL;
    }
}
?>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' data-dismiss='modal' class='btn btn-info' aria-hidden='true' id='saveBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



<!-- Location Access modal //-->
<div id='locAccessModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-building'></i> Location Access</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='locAccessAlert'></div>

        <p class='lead'>Here you can select which locations have access to view this announcement.</p>

        <p><strong>Step 1:</strong> select the coverage type for the announcement.</p>

        
        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='sendToType'>Send To</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="sendToType" id="sendToType" class='form-control'>
                    <option value=""></option>
<?php
if (!empty($sendToTypes))
{
    foreach ($sendToTypes as $r)
    {
        $sel = ($info->sendToType == $r->code) ? "selected='selected'" : null;

        echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div id='sendToTypeDisplay'><?php if (!empty($id)) echo $sendToHTML; ?></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' data-dismiss='modal' class='btn btn-info' aria-hidden='true' id='saveBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->




</form>
