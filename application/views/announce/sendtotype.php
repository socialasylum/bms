<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<hr>

<?php
if (!empty($locations))
{
    echo "<p><strong>Step 2:</strong> Select one or more locations you want to have access to this announcement.</p>";

    echo "<div id='locAccessBody'>" . PHP_EOL;

    foreach ($locations as $r)
    {
        echo "<button type='button' class='btn btn-default btn-checkbox' onclick=\"global.toggleCheckboxBtn(this);\" id='locAccessBtn_{$r->id}' value='{$r->id}'>{$r->name}</button>" . PHP_EOL;
    }

    echo "</div>" . PHP_EOL;
}

if (!empty($territories))
{
    echo "<p><strong>Step 2:</strong> Select one or more territories you want to have access to this announcement.</p>";

    echo "<div id='terAccessBody'>" . PHP_EOL;

    foreach ($territories as $r)
    {
        echo "<button type='button' class='btn btn-default btn-checkbox' onclick=\"global.toggleCheckboxBtn(this);\" id='terAccessBtn_{$r->id}' value='{$r->id}'>{$r->name}</button>" . PHP_EOL;
    }
    
    echo "</div>" . PHP_EOL;
}

?>
