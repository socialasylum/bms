<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='row'>
    <div class='col-lg-6 col-m-6 col-s-12 col-xs-12'>
        <h1><?=$info->title?></h1>
    </div>
    <div class='col-lg-6 col-m-6 col-s-12 col-xs-12'>
        <p class='text-muted pull-right'><strong>William Gallios</strong> &bull; <?=$date?></p>
    </div>

</div> <!-- .row -->


<?=$info->body?>

<hr>

<button type='button' class='btn btn-info' id='returnBtn'>&laquo; Return to Announcements</button>
