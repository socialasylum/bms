<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-money'></i> Leads</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='/leads/edit'><i class='fa fa-plus'></i> New  Lead</a></li>
        </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<p class='lead'>
Below are all the leads assoicated with your account and assigned chapters.
</p>


<div class='tabbable'>
    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabLeadsMe' data-toggle="tab"><i class='fa fa-reply'></i> Sent To me</a></li>
        <li><a href='#tabLeadsOthers' data-toggle="tab"><i class='fa fa-share'></i> Sent To Others</a></li>
        <li><a href='#tabChapterLeads' data-toggle="tab"><i class='fa fa-group'></i> Chapter Leads</a></li>
    </ul>



<div class="tab-content">

    <div id="tabLeadsMe" class='tab-pane active'>
<?php
if (empty($received)) echo $this->alerts->info("There are no leads sent to me.");
else
{
    echo "<div class='col-sm-12 col-xs-12 territoryEditMap' id='sentToMeMap'></div>";

    echo "<div id='savedReceivedMarkers'>" . PHP_EOL;

    foreach ($received as $r)
    {
        $addresses = null;
        try
        {
            $addresses = $this->leads->getAddresses($r->id);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
            continue;
        }

        if (!empty($addresses))
        {
            foreach ($addresses as $ar)
            {
                if (!empty($ar->lat) && !empty($ar->lng))
                {
                    echo "<input type='hidden' lat='{$ar->lat}' lng='{$ar->lng}'>" . PHP_EOL;
                }
            }
        }
    }
    echo "</div> <!-- #savedReceivedMarkers -->" . PHP_EOL;




    echo <<< EOS
<table id='receivedTbl' class='table table-hover table-condensed'>
    <thead>
        <tr>
            <th>Company</th>
            <th>Contact</th>
            <th>Date</th>
            <th>From</th>
            <th>Contact Method</th>
            <th>Phone</th>
            <th>E-mail</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
EOS;

    foreach ($received as $r)
    {
        $info = $contactMethod = $date = $phone = $email = $address = $from = null;

        try
        {
            $info = $this->leads->getInfo($r->id);

            if (!empty($info->contactType)) $contactMethod = $this->functions->codeDisplay(16, $info->contactType);

            if (!empty($info->leadDate)) $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->leadDate, "m/d/Y");

            if (!empty($info->assignedFrom)) $from = $this->users->getName($info->assignedFrom);

            $phone = $this->leads->getMainPhone($r->id);
            $email = $this->leads->getMainEmail($r->id);
            $address = $this->leads->getMainAddress($r->id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips lead if there is an error getting data
        }

        echo "<tr onclick=\"leads.edit({$r->id});\">" . PHP_EOL;

        echo "\t<td>{$info->companyName}</td>" . PHP_EOL;
        echo "\t<td>{$info->contactName}</td>" . PHP_EOL;
        echo "\t<td>{$date}</td>" . PHP_EOL;
        echo "\t<td>{$from}</td>" . PHP_EOL;
        echo "\t<td>{$contactMethod}</td>" . PHP_EOL;
        echo "\t<td>{$phone}</td>" . PHP_EOL;
        echo "\t<td>{$email}</td>" . PHP_EOL;
        echo "\t<td>{$address->address} {$address->city}, {$address->state} {$address->postalCode}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;

    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>

    </div> <!-- #tabLeadsMe -->


    <div id='tabLeadsOthers' class='tab-pane'>
<?php
if (empty($sent)) echo $this->alerts->info("You have not sent any leads");
else
{
    echo <<< EOS
<table id='sentTbl' class='table table-hover table-condensed'>
    <thead>
        <tr>
            <th>Company</th>
            <th>Contact</th>
            <th>Date</th>
            <th>To</th>
            <th>Contact Method</th>
            <th>Phone</th>
            <th>E-mail</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
EOS;

    foreach ($sent as $r)
    {
        $info = $contactMethod = $date = $phone = $email = $address = $to = null;

        try
        {
            $info = $this->leads->getInfo($r->id);

            if (!empty($info->contactType)) $contactMethod = $this->functions->codeDisplay(16, $info->contactType);

            if (!empty($info->leadDate)) $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->leadDate, "m/d/Y");

            if (!empty($info->assignedTo)) $to = $this->users->getName($info->assignedTo);

            $phone = $this->leads->getMainPhone($r->id);
            $email = $this->leads->getMainEmail($r->id);
            $address = $this->leads->getMainAddress($r->id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips lead if there is an error getting data
        }

        echo "<tr onclick=\"leads.edit({$r->id});\">" . PHP_EOL;

        echo "\t<td>{$info->companyName}</td>" . PHP_EOL;
        echo "\t<td>{$info->contactName}</td>" . PHP_EOL;
        echo "\t<td>{$date}</td>" . PHP_EOL;
        echo "\t<td>{$to}</td>" . PHP_EOL;
        echo "\t<td>{$contactMethod}</td>" . PHP_EOL;
        echo "\t<td>{$phone}</td>" . PHP_EOL;
        echo "\t<td>{$email}</td>" . PHP_EOL;
        echo "\t<td>{$address->address} {$address->city}, {$address->state} {$address->postalCode}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;

    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>
    </div> <!-- #tabLeadsOthers -->


    <div id='tabChapterLeads' class='tab-pane'>
<?php
if (empty($chapter)) echo $this->alerts->info("There are no chapter leads available");
else
{
    echo <<< EOS
<table id='chapterTbl' class='table table-hover table-condensed'>
    <thead>
        <tr>
            <th>Company</th>
            <th>Contact</th>
            <th>Date</th>
            <th>Contact Method</th>
            <th>Phone</th>
            <th>E-mail</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
EOS;

    foreach ($chapter as $r)
    {
        $info = $contactMethod = $date = $phone = $email = $address = $to = null;

        try
        {
            $info = $this->leads->getInfo($r->id);

            if (!empty($info->contactType)) $contactMethod = $this->functions->codeDisplay(16, $info->contactType);

            if (!empty($info->leadDate)) $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->leadDate, "m/d/Y");

            // if (!empty($info->assignedTo)) $to = $this->users->getName($info->assignedTo);

            $phone = $this->leads->getMainPhone($r->id);
            $email = $this->leads->getMainEmail($r->id);
            $address = $this->leads->getMainAddress($r->id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips lead if there is an error getting data
        }

        echo "<tr onclick=\"leads.edit({$r->id});\">" . PHP_EOL;

        echo "\t<td>{$info->companyName}</td>" . PHP_EOL;
        echo "\t<td>{$info->contactName}</td>" . PHP_EOL;
        echo "\t<td>{$date}</td>" . PHP_EOL;
        echo "\t<td>{$contactMethod}</td>" . PHP_EOL;
        echo "\t<td>{$phone}</td>" . PHP_EOL;
        echo "\t<td>{$email}</td>" . PHP_EOL;
        echo "\t<td>{$address->address} {$address->city}, {$address->state} {$address->postalCode}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;

    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>
    </div> <!-- #tabChapterLeads -->

</div> <!-- .tab-content -->

</div> <!-- .tabbable -->
