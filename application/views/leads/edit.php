<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
$attr = array
    (
        'name' => 'leadForm',
        'id' => 'leadForm'
    );


echo form_open('#', $attr);
if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>";
?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Lead</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <?php if (empty($id)) : ?>
                <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-share'></i> Send Lead</a></li>
            <?php else :
                    if (!empty($id) && empty($info->assignedTo))
                    {
                        echo "<li><a href=\"javascript:void(0);\" id='saveBtn'><i class='fa fa-save'></i> Save & <strong>Claim</strong> Lead</a></li>" . PHP_EOL;
                    }
                    else
                    {
                        echo "<li><a href=\"javascript:void(0);\" id='saveBtn'><i class='fa fa-save'></i> Save Lead</a></li>" . PHP_EOL;
                    }
    

            endif; ?>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>


        <?php if (!empty($id)) : ?>
        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' class='danger' onclick="formbuilder.deleteForm(<?=$id?>);"><i class='fa fa-trash-o danger'></i> Delete</a></li>
        </ul>
        <?php endif; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<p class='lead'>Below you can create and edit leads.</p>

<div class='row form-horizontal'>

    <div class='col-lg-6'>
    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyName'>Company Name</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='companyName' id='companyName' value="<?=$info->companyName?>" placeholder="">
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='leadDate'>Date</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='leadDate' id='leadDate' value="<?=(empty($id)) ? date("Y-m-d") : $info->leadDate?>" placeholder='<?=date("Y-m-d")?>'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='from'>From</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='hidden' name='from' value='<?=(empty($id)) ? $this->session->userdata('userid') : $info->assignedFrom?>'>
            <select name="from" id="from" class='form-control' disabled>
                <option value=""></option>
<?php
if (!empty($users))
{
    foreach ($users as $k => $user)
    {
        $name = null;

        try
        {
            $name = $this->users->getName($user);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips row if can't find name
        }

        if (empty($id))
        {
            $sel = ($user == $this->session->userdata('userid')) ? "selected='selected'" : null;
        }
        else
        {
            $sel = ($user == $info->assignedFrom) ? "selected='selected'" : null;
        }

        echo "<option {$sel} value='{$user}'>{$name}</option>" . PHP_EOL;
    }
}
?>
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='to'>To</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <select name="to" id="to" class='form-control'>
                <option value=""></option>
<?php
if (!empty($users))
{
    foreach ($users as $k => $user)
    {
        $name = null;

        // skips the user -- cannot send to themselves
        // if (($r->userid == $this->session->userdata('userid')) && ($info->assignedFrom !== $this->session->userdata('userid'))) continue;

        try
        {
            $name = $this->users->getName($user);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips row if can't find name
        }
    
        if (!empty($id) && empty($info->assignedTo))
        {
            $sel = ($user == $this->session->userdata('userid')) ? "selected='selected'" : null;
        }
        else
        {
            $sel = ($info->assignedTo == $user) ? "selected='selected'" : null;
        }
        echo "<option {$sel} value='{$user}'>{$name}</option>" . PHP_EOL;
    }
}
?>
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    </div> <!-- .col-lg-6 -->

    <div class='col-lg-6'>


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='contactName'>Contact Name</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='contactName' id='contactName' value="<?=$info->contactName?>" placeholder="">
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='amount'>Amount</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            	<div class='input-group'>
                <span class='input-group-addon'>$</span>
                <input type='text' class='form-control' id='amount' name='amount' value="<?=$info->amount?>">
            	</div> <!-- /.input-group -->
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='contacType'>Method of Contact</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="contactType" id="contactType" class='form-control'>
                    <option value=""></option>
<?php
if (!empty($contactTypes))
{
    foreach ($contactTypes as $r)
    {
        $sel = ($info->contactType == $r->code) ? "selected='selected'" : null;

        echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

    </div> <!-- .col-lg-6 -->

</div> <!-- .row .form-horizontal -->



<hr>
<h2><i class='fa fa-building'></i> Addresses</h2>
<ul id='addSortList' class='formSort'>

<?php

    $cnt = count($addresses);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++)
    {

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        // if ($i == 0) echo "<div id='master-address'>";

        echo <<< EOS
<li>
<div class='panel panel-default'>
    <div class='panel-heading'>Address <button type='button' onclick="leads.deleteAddress(this);" class='pull-right deleteLink' {$disabled} ><i class='fa fa-trash-o'></i></button></div>

    <div class='panel-body form-horizontal'>
        
    <div class='row'>
        <div class='col-lg-12'>

        <div class="form-group">
             <label class='col-lg-2 control-label' for='description'>Description</label>
             <div class="col-lg-10 controls">
                 <input type='text' class='form-control' name='address[description][]' id='address_description' value="{$addresses[$i]->description}">
             </div>
         </div>

            <div class="form-group">
                <label class='col-lg-2 control-label' for='notes'>Notes</label>
                <div class="col-lg-10 controls">
                    <textarea name='address[notes][]' id='address_notes' class='form-control' rows='4'>{$addresses[$i]->notes}</textarea>
                </div>
            </div>

        </div> <!-- .col-lg-12 -->
    </div> <!-- .row -->


        <div class='row'>
            <div class='col-lg-6'>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='address'>Address</label>
        <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='address[addressLine1][]' id='address_line_1' value="{$addresses[$i]->address}" placeholder=''>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='address2'>Address 2</label>
        <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='address[addressLine2][]' id='address_line_2' value="{$addresses[$i]->address2}" placeholder=''>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='address3'>Address 3</label>
        <div class="col-lg-9 controls">
                <input type='text' class='form-control' name='address[addressLine3][]' id='address_line_3' value="{$addresses[$i]->address3}" placeholder=''>
        </div>
    </div>


            </div>
            <div class='col-lg-6'>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='city'>City</label>
        <div class="col-lg-9 controls">
            <input type='text' class='form-control' name='address[city][]' id='city' value="{$addresses[$i]->city}" placeholder='Las Vegas'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='state'>State</label>
        <div class="col-lg-9 controls">
        <select class='form-control' name="address[state][]" id="state_1">
            <option value=""></option>
EOS;

    if (!empty($states))
    {
        foreach ($states as $abb => $name)
        {
            $sel = ($abb == $addresses[$i]->state) ? 'selected' : null;
            echo "<option {$sel} value='{$abb}'>{$name}</option>" . PHP_EOL;
        }
    }

echo <<< EOS
        </select>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='postalCode'>Postal Code</label>
        <div class="col-lg-9 controls">
            <input type='text' class='form-control' name='address[postalCode][]' id='postalCode' value="{$addresses[$i]->postalCode}" placeholder='89101'>
        </div>
    </div>


            </div>


        </div> <!-- .row -->

        </div> <!-- .panel-body -->
    </div> <!-- .panel -->
    </li>
EOS;


        // if ($i == 0) echo "</div>";
    }
?>

    <!-- div that holds dynamically added addresses -->
    <!-- <div id='address-container'></div> -->
    </ul>

    <div class='row'>
        <div class='col-lg-12'>
            <button type='button' class='btn btn-info' name='addAddressBtn' id='addAddressBtn'><i class='fa fa-plus'></i> Add Address</button>
        </div>
    </div>




<hr>
<h2><i class='fa fa-phone'></i> Phone &amp; Fax</h2>


    <p class='lead'>Here you can add various phone numbers with this contact.</p>


<ul id='phoneSortList' class='formSort'>
<?php

    $cnt = count($phones);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++) 
    {

        $phoneNumber = $phones[$i]->phoneNumber;
        $type = $phones[$i]->type;

        // if ($i == 0) echo "<div id='master-phone'>";

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        echo <<< EOS
        <li>
        <div class='row' style="margin-bottom:15px;">
            <div class='col-lg-3'>
                <select class='form-control' name="phone[type][]" id="phone_type_1">
                    <option value=""></option>
EOS;

    if (!empty($phoneTypes))
    {
        foreach ($phoneTypes as $r)
        {
            $sel = ($r->code == $type) ? 'selected' : null;

            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . EL;
        }
    }

echo <<< EOS
                </select>
            </div> <!-- .col-lg-3 -->

            <div class='col-lg-9'>

                <div class='input-group'>

                <input type='text' class='form-control' name='phone[number][]' id='phone_number_1' value="{$phoneNumber}" placeholder='888-555-1212'>
                    <span class='input-group-btn'>
                        <button type='button' class='btn btn-danger' id='delBtn' {$disabled} onclick="leads.deletePhone(this);"><i class='fa fa-trash-o'></i></button>
                    </span>
                </div> <!-- .input-group -->
            </div> <!-- col-lg-9 -->

        </div> <!-- .row -->
        </li>
EOS;

        // if ($i == 0) echo "</div>";

    }

?>
    </ul>

    <div class='row'>
        <div class='col-lg-12'>
            <button type='button' class='btn btn-info' name='addPhoneBtn' id='addPhoneBtn'><i class='fa fa-plus'></i> Add Number</button>
        </div>
    </div>


<hr>

<h2><i class='fa fa-envelope'></i> E-mail Addresses</h2>

<p class='lead'>Below is a list of all e-mail addresses associated with this contact</p>

<div class='row form-horizontal'>
    <div class='col-lg-12'>

<ul id='emailSortList' class='formSort'>

<?php

    // $cnt = (empty($id)) ? 1 : count($emails);

    $cnt = count($emails);

    if (empty($cnt)) $cnt = 1;

    for ($i = 0; $i < $cnt; $i++) 
    {

        $email = $emails[$i]->email;

        $disabled = ($i == 0) ? "disabled='disabled'" : null;

        echo <<< EOS

        <li>
        <div class="form-group">
            <label class='col-lg-3 control-label' for='email'>E-mail</label>
            <div class="col-lg-9 controls">
                <div class='input-group'>
                <input type='text' class='form-control' name='email[]' id='email' value="{$email}" placeholder=''>
                    <span class='input-group-btn'>
                        <button type='button' class='btn btn-danger' id='delBtn' onclick="leads.deleteEmail(this);" {$disabled} ><i class='fa fa-trash-o'></i></button>
                    </span>
                </div> <!-- .input-group -->
            </div> <!-- col-lg-9 -->
        </div> <!-- .form-group -->
        </li>
EOS;

    }

?>

</ul>

        </div> <!-- .col-lg-12 -->

    <div class='col-lg-12'>
        <button type='button' class='btn btn-info' id='addEmailBtn'><i class='fa fa-plus'></i> Add E-mail</button>
    </div> <!-- .col-lg-9 .col-lg-offset-2 -->

</div> <!-- .row -->







<hr>



        <h2><i class='fa fa-info'></i> Lead Details</h2>

        <p class='lead'>Please enter any details about the products or services this lead is looking for.</p>

        <textarea name='details' id='details' class='form-control' rows='5'><?=$info->details?></textarea>



</form>
