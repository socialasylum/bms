<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1>Create Location</h1>

<p class='lead'>Here you can create a new location for your company.</p>

<?php

    $attr = array
        (
            'name' => 'createForm',
            'id' => 'createForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>
<div class='row-fluid'>

    <div class='span6'>


<div class="form-group">
    <label class='col-lg-2 control-label' for='StoreID'>Store ID</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='StoreID' id='StoreID' value="" placeholder='123'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='name'>Location Name</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='name' id='name' value="" placeholder='Location Name'>
    </div>
</div>

<?php if (!empty($territories)) : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='territory'>Territory</label>
    <div class='col-lg-10 controls'>
        <select class='form-control' name="territory" id="territory">
            <option value=""></option>
            <?php
            foreach ($territories as $r)
            {
                echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
            }
            ?>
        </select>
    </div>
</div>
<?php endif; ?>

<div class="form-group">
    <label class='col-lg-2 control-label' for='openDate'>Open Date</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='openDate' id='openDate' value="" placeholder='YYYY-MM-DD'>
    </div>
</div>

    </div> <!-- .span6 -->

    <div class='span6'>

<div class="form-group">
    <label class='col-lg-2 control-label' for='address'>Address</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='address' id='address' value="" placeholder='101 Tropicana Ave.'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='address2'>Address 2</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='address2' id='address2' value="" placeholder=''>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='address3'>Address 3</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='address3' id='address3' value="" placeholder=''>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='city'>City</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='city' id='city' value="" placeholder='Las Vegas'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='state'>State</label>
    <div class='col-lg-10 controls'>
        <select class='form-control' name="state" id="state">
            <option value=""></option>
<?php
    if (!empty($states))
    {
        foreach ($states as $abb => $name)
        {
            echo "<option value='{$abb}'>{$name}</option>" . PHP_EOL;
        }
    }
?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='postalCode'>Postal Code</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='postalCode' id='postalCode' value="" placeholder='89011'>
    </div>
</div>

    </div> <!-- .span6 -->

        

</div> <!-- .row-fluid -->

<div class='form-actions'>
    <button type='button' class='btn btn-primary' id='createBtn' name='createBtn'>Create Location</button>
    <button type='button' class='btn' id='cancelBtn' name='cancelBtn'>Cancel</button>
</div>

</form>


