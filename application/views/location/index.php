<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-building'></i> Locations</h1>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='/location/edit'><i class='fa fa-map-marker'></i> New Location</a></li>
        </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<div class='col-lg-12 col-m-12 col-s-12 col-xs-12 territoryEditMap img-thumbnail' id='locGoogMap'></div>

<?php
if (empty($locations)): echo $this->alerts->info("No locations have been created!");
else :

    echo "<div id='savedMarkers'>" . PHP_EOL;

    foreach ($locations as $r)
    {
        if (!empty($r->lat) && !empty($r->lng))
        {
            echo "<input type='hidden' lat='{$r->lat}' lng='{$r->lng}'>" . PHP_EOL;
        }
    }

    echo "</div> <!-- #savedMarkers -->" . PHP_EOL;




?>
<table class='table table-striped table-condensed' id='locations'>
    <thead>
        <tr>
            <th>Store ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php
    foreach ($locations as $r)
    {
        echo "<tr>" . PHP_EOL;

        echo "\t<td>{$r->StoreID}</td>" . PHP_EOL;
        echo "\t<td>{$r->name}</td>" . PHP_EOL;
        echo "\t<td>{$r->address}</td>" . PHP_EOL;
        echo "\t<td>{$r->city}</td>" . PHP_EOL;
        echo "\t<td>{$r->state}</td>" . PHP_EOL;
        echo "\t<td>{$r->postalCode}</td>" . PHP_EOL;
        echo "\t<td><a href='/location/edit/{$r->id}' class='btn btn-info btn-xs pull-right'><i class='fa fa-pencil'></i></a></td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }
?>
    </tbody>
</table>

<?php endif; ?>
