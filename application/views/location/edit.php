<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit' ?> Location</h1>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'method' => 'post',
            'class' => 'form-horizontal'
        );

echo form_open_multipart('/location/save', $attr);


?>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>

        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>


        <?php if (!empty($id)) : ?>
        <!-- <ul class="nav navbar-nav pull-right"> -->
            <!-- <li><a href='javascript:void(0);' class='danger' onclick="formbuilder.deleteForm(<?=$id?>);"><i class='fa fa-trash-o danger'></i> Delete</a></li> -->
        <!-- </ul> -->
        <?php endif; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>



    <input type="hidden" name="MAX_FILE_SIZE" value="104857600" />

    <?php if (!empty($id)) : ?>
        <input type='hidden' name='id' id='id' value='<?=$id?>'>
    <?php endif; ?>

    <input type='hidden' name='department' value='0'>

<div class='tabbable'>
    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab"><i class='fa fa-cog'></i> Settings</a></li>
        <li><a href='#tabDesc' data-toggle="tab"><i class='fa fa-pencil-square'></i> Description</a></li>
        <li><a href='#tabPics' data-toggle="tab"><i class='fa fa-picture-o'></i> Pictures</a></li>
        <li><a href='#tabVideos' data-toggle="tab"><i class='fa fa-youtube-square'></i> YouTube Videos</a></li>
        <li><a href='#tabEmps' data-toggle="tab"><i class='fa fa-group'></i> Users</a></li>
        <li><a href='#tabAcct' data-toggle="tab"><i class='fa fa-dollar'></i> Account Info</a></li>
    </ul>



<div class="tab-content">

    <div id="tabSettings" class='tab-pane active'>

<p class='lead'>Here you can edit the general settings about the location.</p>

<div class='row'>

    <div class='col-lg-6 col-m-6 col-s-6 col-xs-6'>


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='StoreID'>Store ID</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='StoreID' id='StoreID' value="<?=$info->StoreID?>" placeholder='123' <?php if (!empty($id)) echo 'readonly'; ?>>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='name'>Location Name</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>" placeholder='Location Name'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


<?php if (!empty($territories)) : ?>
        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='territory'>Territory</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <select class='form-control' name="territory" id="territory">
            <option value=""></option>
            <?php
            foreach ($territories as $r)
            {
                $sel = ($r->id == $info->territory) ? 'selected' : null;
                echo "<option {$sel} value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
            }
            ?>
        </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

<?php endif; ?>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='openDate'>Open Date</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' class='form-control' name='openDate' id='openDate' value="<?=$info->openDate?>" placeholder='YYYY-MM-DD'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='lat'>Latitude</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' class='form-control' name='lat' id='lat' value="<?=$info->lat?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='lng'>Longitude</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' class='form-control' name='lng' id='lng' value="<?=$info->lng?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


		<div class="form-group">
		    <label for="active" class="col-sm-3 control-label">Active</label>
			<div class="col-sm-9">
				<select class='form-control' name='active' id='active'>
				<?php
					if (!empty($yesno))
					{
						foreach($yesno as $k => $v)
						{
							if (empty($id)) $sel = ($k == 1) ? 'selected' : null;
							else $sel = ($k == $info->active) ? 'selected' : null;
						
							echo "<option {$sel} value='{$k}'>{$v}</option>" . PHP_EOL;
						}
					}
				?>
				</select>
		    </div> <!-- col-9 -->
		</div> <!-- .form-group -->

		<div class="form-group">
		    <label for="allowUpdate" class="col-sm-3 control-label">Allow Update</label>
			<div class="col-sm-9">
				<select class='form-control' name='allowUpdate' id='allowUpdate'>
				<?php
					if (!empty($yesno))
					{
						foreach($yesno as $k => $v)
						{
							if (empty($id)) $sel = ($k == 1) ? 'selected' : null;
							else $sel = ($k == $info->allowUpdate) ? 'selected' : null;
						
							echo "<option {$sel} value='{$k}'>{$v}</option>" . PHP_EOL;
						}
					}
				?>
				</select>
		    </div> <!-- col-9 -->
		</div> <!-- .form-group -->
		


    </div> <!-- .col-6 -->

    <div class='col-lg-6 col-m-6 col-s-6 col-xs-6'>

`        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='address'>Address</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='address' id='address' value="<?=$info->address?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='address2'>Address 2</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='address2' id='address2' value="<?=$info->address2?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->




        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='address3'>Address 3</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='address3' id='address3' value="<?=$info->address3?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='city'>City</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='city' id='city' value="<?=$info->city?>" placeholder='Las Vegas'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='state'>State</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <select class='form-control' name="state" id="state">
            <option value=""></option>
<?php
    if (!empty($states))
    {
        foreach ($states as $abb => $name)
        {
            $sel = ($abb == $info->state) ? 'selected' : null;
            echo "<option {$sel} value='{$abb}'>{$name}</option>" . PHP_EOL;
        }
    }
?>
        </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='postalCode'>Postal Code</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='postalCode' id='postalCode' value="<?=$info->postalCode?>" placeholder='89011'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

    
        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='phone'>Phone</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='phone' id='phone' value="<?=$info->phone?>" placeholder='(888) 444-9350'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='fax'>Fax</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='fax' id='fax' value="<?=$info->fax?>" placeholder=''>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='websiteUrl'>Website URL</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='form-control' name='websiteUrl' id='websiteUrl' value="<?=$info->websiteUrl?>" placeholder='http://cgisolution.com'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->


    </div> <!-- .span6 -->

        

</div> <!-- .row-fluid -->


    </div> <!-- #tabSettings -->

    <div id="tabDesc" class='tab-pane'>
        <h2><i class='fa fa-pencil-square'></i> Description</h2>
        <p class='lead'>Enter a description about this location.</p>

        <textarea class='form-control' name='description' id='description' rows='5'><?=$info->description?></textarea>

        <hr>

        <h2><i class='fa fa-plus-square'></i> Additional Description</h2>

        <p class='lead'>Should you need more.</p>

        <textarea class='form-control' name='addDescription' id='addDescription' rows='5'><?=$info->addDescription?></textarea>


    </div> <!-- #tabDesc -->

    <div id='tabPics' class='tab-pane'>

        <h2><i class='fa fa-picture-o'></i> Pictures</h2>

        <p class='lead'>Upload pictures for this location.</p>

        <input type='file' name='img' id='img'>

        <button class='btn btn-primary' type='button' id='uploadPictureBtn'><i class='fa fa-cloud-upload'></i> Upload Picture</button>

    <hr>


        <div id='pictureDisplay'></div>

    </div> <!-- #tabPics -->

    <div id='tabVideos' class='tab-pane'>

        <h2><i class='fa fa-youtube-square'></i> Youtube Videos</h2>

        <p class='lead'>Enter any youtube Url's you wish to share with this location.</p>


        <div class='input-group'>

        <input type='text' class='form-control' name='youTubeUrl' id='youTubeUrl' value="" placeholder='http://youtu.be/ff3TBEVc_ko'>
            <div class='input-group-btn'>
                <?php
                $YTAddDis = (empty($id)) ? 'disabled' : null;
                ?>
                    <button type='button' class='btn btn-primary' id='addYTBtn' name='addYTBtn' <?=$YTAddDis?>><i class='fa fa-plus'></i> Add</button>
            </div>


        </div>
    <hr>


        <div id='YTVideoDisplay'></div>

    </div> <!-- #tabVideos -->

    <div id="tabEmps" class='tab-pane'>

        <p class='lead'>Below is a list of all employee's assigned to this location.</p>
<?php
if (empty($emps))
{
    echo $this->alerts->info("This company has no active employees");
}
else
{
    echo <<< EOS
<table class='table table-striped table-condensed' id='empsTbl'>
    <thead>
        <tr>
            <th><input type='checkbox'> Assigned</th>
            <th>Name</th>
            <th>E-mail</th>
            <th><input type='checkbox'> <i class='fa fa-home'></i> Home</th>
            <th>ID</th>
        </tr>
    </thead>
    <tbody>
EOS;

    foreach ($emps as $r)
    {

        $email = $assigned = $checked = $homeChecked = null;

        try
        {

            $assigned = $this->companies->userAssignedToCompany($r->id, $this->session->userdata('company'));

            // if user is not assigned to that company, skips them
            if ($assigned == false) continue;

            $email = $this->users->getTableValue('email', $r->id);

            $assigned = $this->location->getUserAssigned($r->id, $id);


            if ($assigned->location == $id) $checked = 'checked';
            if (!empty($assigned->homeLocation)) $homeChecked = 'checked';

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            $email = "[Database Error]";
        }

        echo "<tr>";

        echo "<td><input type='checkbox' name='assigned[]' id='assigned_{$r->id}' value='{$r->id}' {$checked}></td>";
        echo "<td>{$r->firstName} {$r->lastName}</td>";
        echo "<td>{$email}</td>";
        echo "<td>
            <input type='hidden' name='home[{$r->id}]' value='0'>
            <input type='checkbox' name='home[{$r->id}]' id='home_{$r->id}' value='1' {$homeChecked}></td>";
        echo "<td>{$r->id}</td>";


        echo "</tr>" . PHP_EOL;

    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;

}
?>

    </div> <!-- #tabEmps -->

    <div id="tabAcct" class='tab-pane for-horizontal'>

        <p class='lead'>Enter the monthly cost information for this location as it will be utilized in calculating your company break even.</p>


        <div class='row'>

            <div class='col-lg-6'>
                <div class="form-group">
                    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='rent'>Monthly Rent</label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">

                        <div class='input-group'>
                            <span class='input-group-addon'><i class='fa fa-dollar'></i></span>
                                <input type='text' name='rent' id='rent' class='form-control' value="<?=$info->rent?>" placeholder='3,000'>
                        </div> <!-- .input-group -->
                    </div> <!-- .controls -->
                </div> <!-- .form-group -->



            </div> <!-- .col-lg-6 -->

            <div class='col-lg-6'>

                <div class="form-group">
                    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='utilities'>Monthly Utilities</label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">

                        <div class='input-group'>
                            <span class='input-group-addon'><i class='fa fa-dollar'></i></span>
                                <input type='text' name='utilities' id='utilities' class='form-control' value="<?=$info->utilities?>" placeholder='500'>
                        </div> <!-- .input-group -->
                    </div> <!-- .controls -->
                </div> <!-- .form-group -->


            </div> <!-- col-6 -->

        </div> <!-- .row -->

        <hr>
        <h2><i class='fa fa-male'></i> Sales Tax</h2>
            <p class='lead'>If you are making sales either online or through the sales terminal, here you can set the tax per location.</p>

        <div class='row'>
            <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>



                <div class="form-group">
                    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='salesTax'>Sales Tax</label>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                        <div class='input-group'>
                            <input type='text' class='form-control' name='salesTax' id='salesTax' value="<?=$info->salesTax?>" placeholder="8.725">
                            <span class='input-group-addon'>%</span>
                        </div> <!-- .input-group -->
                    </div> <!-- .controls -->
                </div> <!-- .form-group -->
            
            </div> <!-- col-6 -->

            <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
                
            </div> <!-- col-6 -->
        </div> <!-- row -->



    </div> <!-- #tabAcct -->

</div> <!-- .tab-content -->

</div> <!-- .tabbable -->


