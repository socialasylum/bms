<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
if (empty($images))
{
    echo $this->alerts->info("No image have been uploaded.");
}
else
{
    echo "<ul id='picSortList' class='formSort'>" . PHP_EOL;

    foreach ($images as $r)
    {
        echo "<li>";
        // echo $r->fileName;

        echo "<img class='img-thumbnail img-sort' src=\"/genimg/render/150?img=" . urlencode($r->fileName) . "&path=" . urlencode('locationimgs') . "\">";

        echo "</li>" . PHP_EOL;
    }

    echo "</ul>" . PHP_EOL;
}
?>
