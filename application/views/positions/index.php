<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-group'></i> Positions</h1>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

        <ul class="nav navbar-nav">
            <li><a href='/position/edit'><i class='fa fa-plus'></i> Create Position</a></li>
        </ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>



<?php
if (empty($positions)) :
    echo $this->alerts->info('No positions have been created!');
else :
?>
    <table class='table table-striped table-condensed' id='positionsTable'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Short Name</th>
                <th>Default Position</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach ($positions as $r)
    {
        $companyName = $departmentName = null;

        echo "<tr>\n";

        try
        {
            $default = ($r->default == 1) ? "<i class='fa fa-check'></i>" : null;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "\t<td>{$r->name}</td>\n";
        echo "\t<td>{$r->shortName}</td>\n";
        echo "\t<td>{$default}</td>\n";
        echo "\t<td><button type='button' onclick=\"positions.editPosition(this);\" data-id='{$r->id}' class='btn btn-info btn-xs pull-right'><i class='fa fa-pencil icon-white'></i></a></td>\n";


        echo "</tr>\n";
    }
?>
        </tbody>
    </table>


<?php endif; ?>

