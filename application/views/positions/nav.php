<?php if(!defined('BASEPATH')) die('Direct access not allowed');

$a[$nav] = 'active';

?>
<div class='sidebar'>
    <ul class='widget widget-menu unstyled'>
        <li class='<?=$a['index']?>'><a href='/position' class='<?=$a['index']?>'><i class='menu-icon icon-dashboard'></i>All Positions</a></li>
        <li class='<?=$a['create']?>'><a href='/position/create' class='<?=$a['create']?>'><i class='menu-icon icon-bar-chart'></i>Create Position</a></li>

        <!-- <li class=''><a href='#'><i class='menu-icon icon-group'></i>PCR <b class='label green pull-right'>132</b></a></li> -->
        <!-- <li class=''><a href='#'><i class='menu-icon icon-tasks'></i>New Hires <b class='label green pull-right'>0</b></a></li> -->
        <!-- <li class=''><a href='#'><i class='menu-icon icon-file-alt'></i>Eval/Vists <b class='label green pull-right'>8</b></a></li> -->

    </ul>
</div>


