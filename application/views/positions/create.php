<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Create Position</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


        <p class='lead'>Here you can create new positions for any companies you are assigned to.</p>

<?php

    $attr = array
        (
            'name' => 'postitionForm',
            'id' => 'positionForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>


<!--
            <div class="form-group">
            <label class="col-lg-2 control-label" for='company'>Company</label>
                <div class='col-lg-10 controls'>
                    <select class='form-control' name='company' id='company'>
                        <option value=''></option>
                    <?php
                    if (!empty($companies))
                    {
                        foreach ($companies as $r)
                        {
                            echo "<option value='{$r->id}'>{$r->companyName}</option>\n";
                        }
                    }
                    ?>
                    </select>
                </div>
            </div>
//-->
            <div class="form-group">
            <label class="col-lg-2 control-label" for='department'>Department</label>
                <div class='col-lg-10 controls'>
                    <select class='form-control' name='department' id='department'>
                        <option value=''></option>
<?php
                    if (!empty($departments))
                    {
                        foreach ($departments as $r)
                        {
                            echo "<option value='{$r->id}'>{$r->name}</option>\n";
                        }
                    }
?>
                    </select>
                </div>
            </div>

            <div class="form-group">
            <label class="col-lg-2 control-label" for='name'>Name</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='name' id='name' placeholder='Chief Awesome Officer'>
                </div>
            </div>


            <div class="form-group">
            <label class="col-lg-2 control-label" for='shortName'>Short Name</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='shortName' id='shortName' placeholder='CAO'>
                </div>
            </div>

            <div class="form-group">
                <label class='col-lg-2 control-label' for='description'>Description</label>
                <div class='col-lg-10 controls'>
                    <textarea name='description' id='description'></textarea>
                </div>
            </div>


        <div class='form-actions'>
            <button type='button' class='btn btn-primary' id='createBtn'>Create</button>
            <button type='button' class='btn' name='cancelBtn' id='cancelBtn'>Cancel</button>
        </div>

        </form>

