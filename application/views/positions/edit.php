<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit' ?> Position</h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm'
        );

echo form_open('#', $attr);
?>

<?php if (!empty($id)) : ?>
    <input type='hidden' name='id' id='id' value='<?=$id?>'>
<?php endif; ?>

<div class='tabbable'>
    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab">Settings</a></li>
        <li class=''><a href='#tabChild' data-toggle="tab">Subordinate Positions</a></li>
    </ul>



<div class="tab-content">

    <div id="tabSettings" class='tab-pane form-horizontal active'>

            <div class="form-group">
            <label class="col-lg-2 control-label" for='name'>Name</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='name' id='name' placeholder='Chief Awesome Officer' value="<?=$info->name?>">
                </div>
            </div>


            <div class="form-group">
            <label class="col-lg-2 control-label" for='shortName'>Short Name</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='shortName' id='shortName' placeholder='CAO' value="<?=$info->shortName?>">
                </div>
            </div>




            <div class="form-group">
                <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='default'>Default?</label>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
                   <input type='hidden' name='default' value='0'>
                	                        
                	<div class='checkbox'>
	                    <label>
	                        <?php if ($info->default == 1) $defaultChecked = "checked='checked'"; ?>
	                        <input type='checkbox' name='default' id='default' value='1' <?=$defaultChecked?>> Default Position
	
	                    </label>
                	</div>

                        <span class='help-block'>This position will be assigned when a user registers.</span>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->



            <div class="form-group">
                <label class='col-lg-2 control-label' for='description'>Description</label>
                <div class='col-lg-10 controls'>
                    <textarea name='description' id='description' class='form-control'><?=$info->description?></textarea>
                </div>
            </div>



    </div>


    <div id="tabChild" class='tab-pane'>
<?php
if (!empty($positions))
{
    foreach ($positions as $r)
    {
        if ($r->id !== $id)
        {
            // resets checked variable just in case check function throws an exception
            $checked = null;

            try
            {
                $check = $this->position->checkIsChildPosition($id, $r->id);

                $checked = ($check == true) ? "checked='checked'" : null;
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }


            echo <<< EOS
            <div class='checkbox'>
				<label>
				    <input type="checkbox" name='childPosition[]' id='childPosition_{$r->id}' value="{$r->id}" {$checked}> {$r->name}
				</label>
		    </div>
EOS;
        }

    }
}
?>
    </div> <!-- #tabChild //-->


</div> <!-- .tab-contents //-->
</div> <!-- .tabbable //-->

</form>

