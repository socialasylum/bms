<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row-fluid'>

    <div class='span3'>
        <?php include_once 'nav.php'; ?>
    </div>

    <div class='span9'>
        <h1>Manage Structure</h1>

        <p class='lead'>Here you can manage the organizational hierarchical structures.</p>

    </div>

</div>


