<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <button type="button" class="btn btn-primary btn-circle" id="step-1">1</button>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle" id="step-2">2</button>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle" id="step-3">3</button>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle" id="step-4">4</button>
        </div>
    </div>
</div>
<div id="sliderOuter">
    <div class="inner-nav">
        <form id="signupform">
            <?php $this->load->view('partials/signupslide1'); ?>
            <?php $this->load->view('partials/signupslide2'); ?>
            <?php $this->load->view('partials/signupslide3'); ?>
            <?php $this->load->view('partials/signupslide4'); ?>
            <input type="hidden" name="address" id="address" value="" />
            <input type="hidden" name="address2" id="address2" value="" />
            <input type="hidden" name="address3" id="address3" value="" />
            <input type="hidden" name="city" id="city" value="" />
            <input type="hidden" name="state" id="state" value="" />
            <input type="hidden" name="postalCode" id="postalCode" value="" />
            <input type="hidden" name="industry" id="industry" value="0" />
            <input type="hidden" name="cgi_token" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        </form>
    </div>
</div>
<div class="container">
    <button class="pull-right btn btn-green nextbutton navbuttons">NEXT</button>
    <button class="pull-right btn btn-red backbutton navbuttons hide">BACK</button>
    <button class="pull-right btn btn-green submitbutton navbuttons hide">SUBMIT</button>
</div>
