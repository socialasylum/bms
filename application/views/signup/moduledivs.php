<div class='well'>
<?php if ($type == "appPro") : ?>

<div id="appPro">
    <h2>Application Processing</h2>
    <p>We know that handling all those applications flooding in can be stressful and overwhelming. We have created a revolutionary system to help you deal with this project. Wether you recieve 10 applications a month or 10,000 a month, our Applications Processing System can help you manage all those new possible recruits.</p>
</div>

<?php elseif ($type == "empOn") : ?>

<div id='empOn'>
    <h2>Employee Onboarding</h2>
    <p>Once your recruits have made it past the application phase and have been selected for hiring, our onboarding process will make sure that they review every one of your policies and procedures as well electronically sign them. This process is 100% customizable by you in regards to what documents are presented to them and the order in which they will be reviewed.</p>
</div>

<?php elseif ($type == "docMan") : ?>

<div id='docMan'>
    <h2>Document Management</h2>
    <p>Document Management allows you to list all your documents on your company intranet and allow all employees to access them dependent on their location and position. We know you may have one policy for Canada with slight differences for US employees, or your Team Members don't need to sign what your Store Managers need to. CGI Solution offers you a unique way to ensure all your employees can access/sign the documents they need to, without any extras.</p>
</div>

<?php elseif ($type == "repSer") : ?>

<div id='repSer'>
    <h2>Reporting Services</h2>
    <p>We offer a range of basic reports that work right away with your new intranet system. If you desire more indepth reporting, we can make that happen as well! Just ask one of our team members how to get your own report writer via CGI Solution.</p>
</div>

<?php elseif ($type == "tecSup") : ?>

<div id='tecSup'>
    <h2>24/7 Tech Support</h2>
    <p>We offer 24/7 help desk ticketing services as well as help desk support. We are partnered with Total Tech to make sure that all your tech questions stayed answered.</p>
</div>

<?php elseif ($type == "locMan") : ?>

<div id='locMan'>
    <h2>Location Management</h2>
    <p>Location Mangement will allow you to manage all your locations and will give you a glance at how they are doing. We can set specific KPI's for your locations and provide you with a summary of how each location is doing.</p>
</div>

<?php endif; ?>
</div>
