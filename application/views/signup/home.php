<?php include_once $_SERVER['DOCUMENT_ROOT'] . 'application/views/template/alert.php'; ?>

<div class='panel panel-default'>
    <div class='panel-heading'>Sign up</div>

    <div class='panel-body'>
<div class="row">
    <div class="col-lg-6">


<?php
//<form name='loginForm' id='loginForm' method='post' action='/intranet/login'>

    $attr = array
        (
            'name' => 'signupForm',
            'id' => 'signupForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);


echo "<div id='mod-hidden-list'>" . PHP_EOL;

if (!empty($_GET['module']))
{
    foreach ($_GET['module'] as $module)
    {
        $ppu = 0;

        try
        {
            $info = $this->modules->moduleInfo($module);

            $ppu = $info->price;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "\t<input type='hidden' name='module[]' id='module_{$module}' ppu=\"{$ppu}\" value='{$module}'>" . PHP_EOL;
    }
}

echo "</div>" . PHP_EOL; // #mod-hidden-list

?>

    <input type='hidden' name='users' id='users' value='<?=$users?>'>


<fieldset>
    <legend>Company Information</legend>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='company'>Company Name</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='companyName' id='companyName' value="" placeholder=''>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label" for="firstName">First Name</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="firstName" id="firstName" placeholder="John">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="lastName">Last Name</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="lastName" id="lastName" placeholder="Smith">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="password">Admin Password</label>
        <div class="col-lg-9">
        <input type="password" class='form-control' name="password" id="password" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
    </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="passwordConf">Confirm Admin Password</label>
        <div class="col-lg-9">
        <input type="password" class='form-control' name="confirmPassword" id="confirmPassword" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="address">Address</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="address" id="address" placeholder="123 Main St.">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="address2">Address 2</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="address2" id="address2" placeholder="Suite #101">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="city">City</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="city" id="city" placeholder="Las Vegas">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="state">State</label>
        <div class="col-lg-9">
             <select name='state' id='state' class='form-control'>
                <option value=''></option>
<?php
if (!empty($states))
{
    foreach ($states as $abb => $stateName)
    {
        echo "<option value='{$abb}'>{$stateName}</option>\n";
    }
}
?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="postalCode">Postal Code</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="postalCode" id="postalCode" placeholder="89011">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="email">Email</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="email" id="email" placeholder="someone@domain.com">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="phone">Phone</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="phone" id="phone" placeholder="(702) 555 - 0123">
        </div>
    </div>


    <div class="form-group">
        <label class="col-lg-3 control-label" for="industry">Industry?</label>
        <div class="col-lg-9">
        <select name="industry" id='industry' class='form-control'>
            <option value=''></option>
            <?php
            if (!empty($industry))
            {
                foreach ($industry as $r)
                {
                    echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
                }
            }
            ?>
            <option value='0'>Other</option>
        </select>
        </div>
    </div>

    <div class="form-group" style="display:none;" id='otherIndustryFormGroup'>
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='otherIndustry'>Other industry</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='otherIndustry' id='otherIndustry' value=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    </fieldset>
    <fieldset>
        <legend>Billing Information</legend>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='ccName'>Name on Card</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='ccName' id='ccName' value="" placeholder='John Smith'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='ccNumber'>Card Number</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='ccNumber' id='ccNumber' value="" placeholder='0000-1234-5678-9999'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccExp'>Card Expiration</label>
        <div class="col-lg-9">
            <select name="ccExpM" id="ccExpM" class='input-small form-control' style="width:100px;float:left;">
                <option value="">MM</option>
<?php
for ($i = 1; $i <= 12; $i++)
{

    $display = str_pad($i, 2, '0', STR_PAD_LEFT);

    echo "<option value='{$display}'>{$display}</option>" . PHP_EOL;
}
?>
            </select>
 <div style='float:left; font-size:24px;margin:0 10px;'>/</div>
            <select name="ccExpY" id="ccExpY" class='input-small form-control' style="width:100px;float:left;">
                <option value="">YYYY</option>
<?php
for ($i = (int) date("Y"); $i <= ((int) date("Y") + 10); $i++) 
{
    echo "<option value='{$i}'>{$i}</option>" . PHP_EOL;
}
?>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccCvv'>CVV [?]</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='ccCvv' id='ccCvv' value="" placeholder='000'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

<?php
/*
    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='billingZip'>Billing Postal Code</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='billingZip' id='billingZip' class='input-medium' value="" placeholder='89011'>
        </div>
    </div>
*/
?>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <label class='checkbox'>
                <input type='hidden' name='agree' value='0'>
                <input type='checkbox' name='agree' id='agree' value='1'> By clicking this checkbox you agree to the sites Terms of Service, Privacy Policy, and Return &amp; Refunds Policy.
            </label>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    </fieldset>

</form>
</div> <!-- /.col-lg-6 -->
<div class="col-lg-6">


<div class='well form-horizontal'>

        <h3 class='text-info'>Package Details</h3>

        <p class='lead'>Below is a detailed list of the modules, number of users, and total monthly cost.</p>

    <hr>

        <dl class='dl-horizontal'>
            <dt>Number of Users</dt>
            <dd><?=$users?></dd>

            <dt>Modules</dt>
            <dd>
<?php
if (empty($_GET['module']))
{
    echo "<span class='label label-info'>No modules have been selected. <br>Click the <strong>Edit Package</strong> to purchase extra modules.</span>" . PHP_EOL;
}
else
{
    foreach ($_GET['module'] as $module)
    {
        $moduleName = "[Database Error]";

        try
        {
            $info = $this->modules->moduleInfo($module);

            $moduleName = $info->name;

            if (!empty($info->icon)) $moduleName = "<i class='{$info->icon}'></i> " . $moduleName;

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<p>{$moduleName}</p>";
    }
}
?>

            </dd>


            <dt>Included Packages</dt>

            <dd>
<?php
if (!empty($unvModules))
{
    foreach ($unvModules as $r)
    {
        echo "<p><i class='{$r->icon}'></i> {$r->name}</p>";
    }
}
?>
            </dd>

            <dt>Monthly Cost</dt>
            <dd>$<span id='totalDisplay'>0</span> / month</dd>

        </dl>

    <hr>

    <div align='center'>
        <button type='button' class='btn btn-default' name='editPlanBtn' id='editPlanBtn'><i class='icon-pencil'></i> Edit Package</button>
    </div>
</div> <!-- .module -->


    <div class='panel panel-default'>
        <div class='panel-heading'>Promo Code</div>
        <div class='panel-body'>
            <p class='lead'>Do you have a promo code? if so, enter it here.</p>

            <input type='text' class='form-control' name='promoCode' id='promoCode' value=''>

        </div>  <!-- .panel-body -->

        <div class='panel-footer'>
            <button type='button' class='btn btn-success'>Apply Promo Code</button>
        </div>

    </div> <!-- .panel -->


</div> <!-- /.col-lg-6 -->
</div> <!-- /.row -->


    </div> <!-- .panel-body -->



    <div class="panel-footer">
            <button type="button" class='btn btn-primary' name="submitBtn" id='submitBtn'>Submit</button>
            <button type="reset" name="resetBtn" id='resetBtn' class='btn btn-default'>Clear</button>
    </div>


</div> <!-- .panel -->

