<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<?php if (!isset($_GET['dynamic'])) : ?>
    </div><!--/.container-->

    </div> <!-- .wrapper -->
	<?php
	
	$containerClass = 'container';
	
	$companyName = 'GreenNvy';
	try
	{
		if (class_exists('CI_DB'))
		{
			if (!empty($this->session->userdata('company'))) $company = $this->session->userdata('company');
		
			if (!empty($company))
			{
				$hideFooter = $this->companies->getTableValue('hideFooter', $company);
				
				if ($this->style->getVal($company, 'fixedWidth') == 1) $containerClass = 'container';

				// gets company name
				$companyName = $this->companies->getCompanyName($company);
			}
			
			if (!empty($hideFooter)) $hide = true;
		}
	}
	catch (Exception $e)
	{
		PHPFunctions::sendStackTrace($e);
	}

	// container class was specified in controller
	if (!empty($container)) $containerClass = $container;
	?>

        <div class='footer intranet'<?=($hide) ? " style='display:none;'" : null?>>
        <div class='<?=$containerClass?>'><span>©</span> greenNVY 2014 | <a href="#">Investor Relations</a> | <a href="mailto:support@greennvy.com?Subject=Support%20Question" target="_blank">Support</a></div>
        </div> <!-- /.footer -->

		</div> <!-- /#site-wrapper -->

<?php if ($this->session->userdata('logged_in') && !isset($_GET['window'])) : ?>

<div id='site-modal-container'>
	<!-- logout modal -->
	<div id='logoutModal' class='modal fade'></div>
	
	<!-- modal which handles user inputs for company setup -->
	<div id='reqcheckModal' class='modal fade'></div>

	<!-- div for document modal -->
	<!-- <div id='docAckModal'></div> -->

	<?php $this->load->view('template/modals/logout_warn'); ?>
	<?php $this->load->view('template/modals/lock'); ?>
	<?php $this->load->view('template/modals/search'); ?>
		
	<?php
	if (class_exists('CI_DB'))
	{
		if ($this->users->isAdmin($this->session->userdata('userid'))) $this->load->view('template/modals/createcomp');
	}	
	?>

</div>

<?php endif; ?>

  </body>
</html>
<?php endif; ?>
