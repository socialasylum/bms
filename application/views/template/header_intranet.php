<?php if(!defined('BASEPATH')) die('Direct access not allowed');

if (!isset($_GET['dynamic'])) :

include_once 'headinclude.php';
?>

    <?=$headscript?>

	
	<style type='text/css'>
		<?php
		
		if (isset($_GET['dynamic'])) echo "body{ padding-top: 0px; } ". PHP_EOL;
		
		if (isset($container_padding)) echo "#main-container.container{ padding: {$container_padding} }";
		
		if (isset($css)) echo $css . PHP_EOL;
		?>
		<?php if (isset($bgcolor)) : ?>
		body
		{
			background-color: <?=(isset($bgcolor))? $bgcolor : 'white'?>;
		}
		<?php endif; ?>
		
		<?php if (isset($wrapperbg)) : ?>
		.wrapper{ background-color: <?=$wrapperbg?>; }
		<?php endif; ?>
	</style>

  </head>

  <body<?=(empty($onload)) ? null : " data-init=\"{$onload}\""?> data-token="<?=$this->security->get_csrf_hash()?>" <?php
  	echo " data-company='{$this->session->userdata('company')}'";
  	
  	if ($this->session->userdata('logged_in'))
  	{
  		echo " data-userid='{$this->session->userdata('userid')}'";
  	}
  	
  	
  	if (!empty($this->functions->minscripts))
  	{
	  	echo " data-javascript='" . json_encode($this->functions->minscripts) . "'";
  	}
  	
  	if (!empty($this->functions->mincss))
  	{
	  	echo " data-css='" . json_encode($this->functions->mincss) . "'";
  	}
  	
  	if (class_exists('CI_DB') && !empty($company))
  	{
  		if ($this->style->getVal($company, 'bgRepeat') == '4')
  		{
  			echo " data-bg='/public/uploads/layout/" . $company . '/' . $this->style->getVal($company, 'background-image') . "'";
  		}
  		
  		$hideFooter = $this->companies->getTableValue('hideFooter', $company);
  		
  		echo " data-hideFooter='{$hideFooter}'" . PHP_EOL;
  	}
 
  	
   ?>>

<div id='site-wrapper'>

<?php if (!isset($_GET['window'])) : ?>

<?php if ($hide) echo "<style>#site-wrapper{ padding-top:0px; } .wrapper .container{ padding-top:0; }</style>" . PHP_EOL; ?>



<div class="navbar navbar-default navbar-fixed-top" id='main-top-nav' role='navigation'<?=($hide) ? " style='display:none;'" : null?>>
    <div class='container-fluid'>
        <div class="navbar-header">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavCollapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

        <?=$this->functions->headerLogo()?>

    </div> <!-- .navbar-header -->
      <!-- Everything you want hidden at 940px or less, place within here -->
    <div class="collapse navbar-collapse navbar-ex1-collapse" id='mainNavCollapse'>

            <ul id='main-nav' class="nav navbar-nav">
            
<?php
    try
    {
		$landingUrl = '/msg';
    }
    catch(Exception $e)
    {
       PHPFunctions::sendStackTrace($e);
    }
    

?>
		        <li class=""><a href="/msg" id='navHomeBtn'><i class='fa fa-home'></i></a></li>

<?php
if ($this->users->isAdmin())
{
    echo "<li class='dropdown'>" . PHP_EOL;

    echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Admin<b class='caret'></b></a>" . PHP_EOL;

    echo "<ul class='dropdown-menu' role='menu'>" . PHP_EOL;

    echo "<li><a href=\"/user\" class=\"fa fa-user\"> User</a></li>";

    echo "</ul>" . PHP_EOL;

    echo "</li>" . PHP_EOL;
}
?>
            <li><a  href='#' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-user'></i>  <span class="caret"></span></a>
  <ul class="dropdown-menu">
    	
    	<?php //dropdown menu links ?>
        <li><a href='/user/edit/<?=$this->session->userdata('userid')?>'><i class='fa fa-cog'></i> <?=$this->users->getName($this->session->userdata('userid'))?></a></li>
        <li class='divider'></li>
        <li><a href='/intranet/logout'><i class='fa fa-sign-out'></i> Log Out</a></li>

<?php
// lists out all available companies user has access to
// so they can switche between companies
    try
    {
        $companies = $this->companies->getCompanies();
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

    if  ((!empty($companies)) && (count($companies) > 1))
    {
        echo "<li class='divider'></li>" . PHP_EOL;

        foreach ($companies as $r)
        {
            echo "<li><a href='/intranet/switchcompany/{$r->id}'>{$r->companyName}</a></li>";
        }
    }
?>
  </ul>
            </li>

            <?php // general notifications ?>
            <!--
            <li id='notificationLi' class='notification'><a  href='javascript:void(0);'><i class='fa fa-exclamation-circle'></i></a></li>
			-->
            <?php // msg notifications ?>
            <li id='msgNotLi' class='notification'><a href='javascript:void(0);'><i class='fa fa-envelope'></i></a></li>

    </ul>

        <input type='hidden' name='namespace' value='<?=$this->router->fetch_class()?>'>

        </div>
    </div>
    </div>
	<?php endif; // endif window ?>


	
    <div class='wrapper'<?php if (isset($wrapper_style)) echo " style=\"{$wrapper_style}\""; ?>>

	<?php
	
	$containerClass = 'container-fluid';
	
	try
	{
		if ($this->style->getVal($company, 'fixedWidth') == 1) $containerClass = 'container';
	}
	catch (Exception $e)
	{
		PHPFunctions::sendStackTrace($e);
	}

	// container class was specified in controller
	if (!empty($container)) $containerClass = $container;

	//if (!isset($container_style)) $container_style = 'margin-top:15px;';
	
	if ($hide) $container_style = '';
	?>
    <div <?php if (!isset($_GET['dynamic'])) echo "id='main-container'"; ?> class="<?=$containerClass?>" style="<?=$container_style?><?=(isset($opacity)) ? "opacity:{$opacity};" : null?>">

<?php 
	//endif; // endif for window
else:

	include_once 'head_libs.php';
	/*
	echo "<script>" . PHP_EOL;
	
	
	echo "\tvar pageHeader = " . json_encode($_ci_vars) . PHP_EOL;
	
	if (!empty($title)) echo "\tvar pageTitle = '{$title}';"  . EOL;
	
	echo "</script>";
	*/
	$meta = array
	(
		'pageTitle' => $title,
		'civars' => $_ci_vars,
		'headlibs' => $this->functions->minscripts,
		'headcss' => $this->functions->mincss,
		'containerClass' => $containerClass,
		'containerStyle' => $container_style
	);

	echo json_encode($meta);
	
	echo EOL . EOL . "<!-- HTML_BEGIN -->" . EOL;
// endif for dyanmic
endif; 


?>



<?php include_once 'alert.php' ?>

