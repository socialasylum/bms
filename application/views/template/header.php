<?php
if (!defined('BASEPATH'))
    die('Direct access not allowed');

$a[$nav] = 'active';
?><?php include_once 'headinclude.php'; ?>

<?= $headscript ?>

</head>

<body<?= (empty($onload)) ? null : " data-init=\"{$onload}\"" ?><?php
if (!empty($this->functions->minscripts)) {
    echo " data-javascript='" . json_encode($this->functions->minscripts) . "'";
}

if (class_exists('CI_DB') && !empty($company)) {
    if ($this->style->getVal($company, 'bgRepeat') == '4') {
        echo " data-bg='/public/uploads/layout/" . $company . '/' . $this->style->getVal($company, 'background-image') . "'";
    }

    $hideFooter = $this->companies->getTableValue('hideFooter', $company);

    echo " data-hideFooter='{$hideFooter}'" . PHP_EOL;
}
?> data-token="<?= $this->security->get_csrf_hash() ?>">

    <style>#site-wrapper { padding-top: 0px; margin: 0 auto -60px; height: 100%; min-height: 100%; } .wrapper .container { padding-top:0; }</style>
    <div id='site-wrapper'>
        <div class="navbar navbar-default" role='navigation'<?= ($hide) ? " style='display:none;'" : null ?>>
            <div class="container">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="title" id="navLogo" href="/intranet/login"><img src="/public/images/greennvy_logo_whisper.png" width="378" height="82" /></a>
                </div> <!-- .navbar-header -->
                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">



                    <ul class="nav navbar-nav pull-right">

<?php if ($this->session->userdata('logged_in') && class_exists('CI_DB')) : ?>
                            <?php
                            try {
                                $name = $this->users->getName($this->session->userdata('userid'));
                            } catch (Exception $e) {
                                $this->functions->sendStackTrace($e);
                            }
                            ?>
                            <li class='navbar-form' id="logButton"><button type='button' class='btn btn-default' name='navLoggedInBtn' id='navLoggedInBtn'><i class='fa fa-user'></i> <?= $name ?></button></li>
                        <?php else : ?>
                            <li class='navbar-form' id="logButton"><button type='button' class='btn btn-default' name='navLoginBtn' id='navLoginBtn'><i class='fa fa-sign-in'></i> Login</button></li>
                        <?php endif; ?>
                    </ul>


                </div><!--/.nav-collapse -->
            </div>
        </div>


        <div class='wrapper'>
<?php
$containerClass = 'container-fluid';
// container class was specified in controller
if (!empty($container))
    $containerClass = $container;
?>
            <div id='main-container' class='<?= $containerClass ?>' style="">


            <?php include_once 'alert.php' ?>
