<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<ul class='breadcrumb'>
    <li><a href='/intranet/landing'><i class='fa fa-home'></i></a> 
    <?php
    if (isset($this->bcControllerUrl) AND isset($this->bcControllerText))
    {
        // echo "<span class='divider'>/</span></li>" . PHP_EOL;
        echo "<li><a href='{$this->bcControllerUrl}'>{$this->bcControllerText}</a> " . PHP_EOL;
    }


    if  (!empty($breadCrumbs))
    {
        foreach ($breadCrumbs as $k => $v)
        {
            try
            {
                $name = $this->folders->getTableValue('name', $v);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
                $name = "[Database Error]";
            }

            echo "<li><a href='/" . $this->router->fetch_class() . "/index/{$v}/0'><i class='fa fa-folder-open-o'></i> {$name}</a>" . PHP_EOL;
        }
        // print_r($breadCrumbs);
    }

    if (isset($this->bcViewText))
    {
        // echo "<span class='divider'>/</span></li>" . PHP_EOL;
        echo "<li>{$this->bcViewText}" . PHP_EOL;
    }
    ?>
    </li>
</ul>
