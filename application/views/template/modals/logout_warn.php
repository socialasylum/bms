<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="modal fade" id='logoutWarnModal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Logout</h4>
      </div>
      <div class="modal-body">
        <p class='lead'>Your session is about to expire. If you wish to stay logged in, please click the "Stay Logged In" button below.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="global.logout();">Log Out</button>
        <button type="button" class="btn btn-primary" onclick="global.stayLoggedIn();">Stay Logged In</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->