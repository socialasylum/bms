<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<!-- lock modal //-->
<div id='lockModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <h2 class='modal-title'><i class='fa fa-lock'></i> Site Locked</h2>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='lockAlert'></div>

        <p class='lead'>The site has been locked. Please enter your password to resume your session, or logout.</p>


<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='email'>E-mail</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='email' id='email' readonly autocomplete="off" placeholder='email@domain.com' value="<?=$this->session->userdata('email')?>">
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='password'>Password</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='password'  name='password' class='form-control' autocomplete="off" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
    </div> <!-- .controls -->
</div> <!-- .form-group -->


    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <a href='/intranet/logout/0/1' class='btn btn-default'><i class='fa fa-logout'></i> Log Out</a>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createDocBtn'><i class='fa fa-login'></i> Login</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->
