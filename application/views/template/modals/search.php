<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-- search modal //-->
<div id='searchModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <h3 class='modal-title'><i class='icon-search'></i> Search</h3>
            </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='searchAlert'></div>
<?php
    $attr = array
        (
            'name' => 'modalSearchForm',
            'id' => 'modalSearchForm',
            'method' => 'get'
        );

echo form_open('/search', $attr);
?>

    <div class='input-group'>

        <input type='text' class='form-control' name='q' id='q'>

        <span class='input-group-btn'>
            <button type='submit' class='btn btn-primary' aria-hidden='true' id='modalSearchBtn'><i class='fa fa-search'></i></button>
        </span>

    </div> <!-- .input-group -->

        </form>
    </div> <!-- .modal-body //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->