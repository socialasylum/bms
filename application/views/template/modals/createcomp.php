<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class="modal fade" id='createCompModal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class='fa fa-plus'></i> Create New Company</h4>
      </div>
 
      <div class="modal-body">
      
      	<div id='createCompAlert'></div>
		<?php
		
		$attr = array
		(
			'name' => 'createCompForm',
			'id' => 'createCompForm'	
		);
		
		echo form_open('#', $attr);
		?>
      
      	<p>To create a new company, complete the form below. Enter the company name, and the default position and department.</p>
      	
		  <div class="form-group">
		  	  <label for='companyName'>Company Name</label>
		      <input type='text' class='form-control' name='companyName' id='companyName'>
		  </div> <!-- .form-group -->
        
		  <div class="form-group">
		  	  <label for='posName'>Default Position</label>
		      <input type='text' class='form-control' name='posName' id='posName' placeholder="User">
		  </div> <!-- .form-group -->


		  <div class="form-group">
		  	  <label for='depName'>Default Department</label>
		      <input type='text' class='form-control' name='depName' id='depName' placeholder="Corporate">
		  </div> <!-- .form-group -->

		  <div class='form-group'>
		  	<div class='checkbox'>
		  		<label>
		  			<input type='checkbox' name='addUser' id='addUser' value='1'> Add me to this company.
		  		</label>
		  	</div> <!-- /.checkbox -->
		  </div> <!-- /.form-group -->

		  </form>
      
      </div> <!-- /.modal-body -->

      <div class="modal-footer">
        <button type="button" id='createCompCancelBtn' class="btn btn-default" data-dismiss="modal"><i class='fa fa-times-circle'></i> Cancel</button>
        <button type="button" id='createCompSaveBtn' class="btn btn-primary" onclick="global.checkCreateCompForm(this);"><i class='fa fa-save'></i> Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->