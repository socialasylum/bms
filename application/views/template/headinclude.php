<?php if(!defined('BASEPATH')) die('Direct access not allowed');
	
	$companyName = null;
	
	if (!empty($this->session->userdata('company'))) $company = $this->session->userdata('company');
	
	if (!empty($company) && class_exists('CI_DB'))
	{
		try
		{
			$bootstrapTheme = $this->style->getVal($company, 'bootstrapTheme');

			// gets company name
			$companyName = $this->companies->getCompanyName($company);
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
	}

	
?><!DOCTYPE html>
<html lang="en">
    <head data-minv='<?=$this->config->item('min_version')?>'<?=(!empty($this->config->item('min_debug'))) ? " data-mindebug='true'" : null?>>

    <title><?php if (!empty($companyName)) echo "{$companyName} - "; if(!empty($title)) echo "{$title} - "; ?>Business Management System : Sports Asylum</title>
    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
        { (i[r].q=i[r].q||[]).push(arguments)}
        ,i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-56375842-2', 'auto');
        ga('send', 'pageview');
    </script>

<?php
	//if (ENVIRONMENT == 'development') echo "<script> less = { env: 'development' }; </script>" . PHP_EOL;
	// require js
	$files = array
	(
		'require.js',
		//'domReady.js',
		//'cs.js',
		//'i18n.js',
		//'text.js'
	);
	
	//echo $this->functions->minFiles($files, 'public/js/');

	?>

	<script type='text/javascript' data-main="/public/js/loader"  src='/public/js/requirejs-async/lib/require.js'></script>
    
    <?php if ($signup): ?>
    <script src="/public/js/signup.js"></script>
    <?php endif; ?>
    
    <?php if ($slider): ?>
    <script data-main="/public/js/bootstrap-slider/js/bootstrap-slider" src='/public/js/requirejs-async/lib/require.js'></script>
    <link href="/public/js/bootstrap-slider/css3/bootstrap-slider.css" rel="stylesheet" />
    <script>
        require(['domReady!'], function (doc) {
            new Slider('#numusers');
        });
    </script> 
    <?php endif; ?>
    <?php if ($login): ?>
    <style type="text/css">
        .btn-primary {
            background: #7c9f03 !important;
            border: 0px !important;
        }
        .btn-primary:hover, .btn-primary:focus {
            background:#c4db3b !important;
            border: 0px !important;
        }
        a
        {
            color: #7C9F03 !important;
        }
        .btn-link,
        .btn-link:hover,
        .btn-link:focus {
            color: #7C9F03 !important;
        }
    </style>
    <?php endif; ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css' />
    
    <?php //$this->functions->jsScript('jquery.loader.js')?>
    
<!--[if lt IE 9]>
		<?=$this->functions->jsScript('html5shiv-printshiv.js html5shiv.js', 'public/js/html5shiv/src/')?>
<![endif]-->

<?php 

require_once 'head_libs.php';

if ($this->config->item('live') == true) : ?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-45844313-1']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    // ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
<?php else: ?>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<?php endif; ?>


<!-- banner that displays if someone is using IE 8 or less -->
<!--[if lt IE 9]> 
  <div style='clear:both; text-align:center; position:relative;'><a href="https://google.com/chrome"><img src="/public/images/updateBrowser.jpg" border="0" height="344" width="640" style="margin-top:100px;z-index:99999;" alt="You are using an outdated browser. For a  faster, safer browsing experience, upgrade for free today." /></a></div>
 <![endif]-->


<?php


	$cssFiles[] = '../js/bootstrap-select/bootstrap-select.css';
	$cssFiles[] = '../font-awesome-4.1.0/css/font-awesome.css';
	$cssFiles[] = '../bootstrap/dist/css/bootstrap.css';



	if (!empty($company) && class_exists('CI_DB'))
	{

		
		// if bootstrap theme is enabled
		if ($bootstrapTheme == 1) $cssFiles[] = '../bootstrap/dist/css/bootstrap-theme.css';
	}
	
	//$cssFiles[] = '../js/jquery-ui-1.11.0.bms/jquery-ui.css';
	$cssFiles[] = '../js/jquery-ui-1.11.0/jquery-ui.css';
	$cssFiles[] = 'main_org.css';
	$cssFiles[] = 'media.css';
	$cssFiles[] = '../less/main.css';


	$cssMinSrc = $this->functions->minSrc(implode(',', $cssFiles));

	echo "<link type='text/css' rel='stylesheet' href='/min/?b=public/css&amp;f=" . $cssMinSrc . "' />";

	// if user is logged in
	if (!empty($company) && class_exists('CI_DB'))
	{
		try
		{
			$previewFile = $this->style->styleFileExists($company, 'preview.css');
			$layoutFile = $this->style->styleFileExists($company);
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
		}
		
		if (isset($_GET['preview']) && $previewFile)
		{
			echo $this->functions->cssScript('preview.css', "public/uploads/layout/{$company}/");
		}
		// checks if file exists
		elseif ($layoutFile)
		{
			echo $this->functions->cssScript('layout.css', "public/uploads/layout/{$company}/");
		}
	}
