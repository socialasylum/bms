<?php if(!defined('BASEPATH')) die('Direct access not allowed'); 

//echo $this->functions->jsScript('jquery.websocket.js', 'application/third_party/bms/js/', 'text/javascript', true, 'gen');

//echo $this->functions->jsScript('msg_socket.js');
//echo $this->functions->jsScript('less/dist/less-1.7.3.min.js', '/public/js/', 'text/javascript', false);

//echo $this->functions->cssScript('jquery.functions.less', '/public/js/jquery.functions.js/less/', false, 'f', 'stylesheet/less');

	$files = array
	(
		'facebook.js',
		'bootstrap-select/bootstrap-select.js',
		'../classyloader/js/jquery.classyloader.min.js'
	);

echo $this->functions->jsScript($files);


//echo $this->functions->cssScript('bootstrap-select/bootstrap-select.css', '/public/js/');

//echo $this->functions->cssScript('jquery-ui.min.css', '/public/js/jquery-ui-1.11.0/', false);

if ($jqueryui)
{
	//echo $this->functions->jsScript('jquery-ui.min.js', '/public/js/jquery-ui-1.11.0/', 'text/javascript', false);
}

if ($strength)
{
	echo $this->functions->jsScript('strength.js', '/public/js/strength/src/');
	echo $this->functions->cssScript('strength.css', '/public/js/strength/src/');
}

if ($fancytree)
{
	echo $this->functions->jsScript('fancytree/dist/jquery.fancytree-all.js');
	echo $this->functions->cssScript('ui.fancytree.css', 'public/js/fancytree/dist/skin-lion/');
}

if ($contextmenu)
{
	echo $this->functions->jsScript('jquery.functions.js/jquery.contextmenu.js');
//	echo $this->functions->cssScript('contextMenu.less', '/public/js/jquery.functions.js/less/', false, 'f', 'stylesheet/less');
}

if ($inputSpinner)
{
	echo $this->functions->jsScript('jquery.functions.js/jquery.inputSpinner.js');
}

if ($uploader)
{
	echo $this->functions->jsScript('jquery.functions.js/jquery.uploader.js');
//	echo $this->functions->cssScript('uploader.less', '/public/js/jquery.functions.js/less/', false, 'f', 'stylesheet/less');
}

if ($datatables)
{
	//echo $this->functions->jsScript('datatables/media/js/jquery.dataTables.js', '/public/js/', 'text/javascript', false);
	echo $this->functions->cssScript('jquery.dataTables.css', 'public/js/datatables/media/css/');
	
	echo $this->functions->cssScript('dataTables.bootstrap.css', '/public/js/datatables.plugins/integration/bootstrap/3/');
	echo $this->functions->jsScript('dataTables.bootstrap.js', '/public/js/datatables.plugins/integration/bootstrap/3/');
} 

if ($prettycheck)
{
	echo $this->functions->jsScript('prettyCheckable.min.js', '/public/js/prettycheck/dist/', 'text/javascript', false);
	echo $this->functions->cssScript('prettyCheckable.css', '/public/js/prettycheck/dist/');
}

if ($bootbox)	echo $this->functions->jsScript('bootbox/bootbox.js');

 /*
if (!$perfectscroll) 
{
	$slimscroll = true;
	
	//if ($slimscroll) echo $this->functions->jsScript('jquery.slimscroll.js', '/public/jQuery-slimScroll-1.3.0/');
}
*/
if ($perfectscroll || $slimscroll)
{
	$files = array
	(
		'jquery.mousewheel.js',
		'perfect-scrollbar.js'
	);
	
	echo $this->functions->jsScript($files, 'public/js/perfect-scrollbar/src/', '');
	echo $this->functions->cssScript('perfect-scrollbar.css', 'public/js/perfect-scrollbar/src/');	
}

if ($ckeditor)
{
	//echo $this->functions->jsScript('jquery.js', '/public/ckeditor4.2.2/adapters/', '', false);
	//echo $this->functions->jsScript('ckeditor.js adapters/jquery.js', '/public/ckeditor4.2.2/', '', false);
	//echo $this->functions->jsScript('ckeditor.js', '/public/ckeditor4.2.2/', '', false);
	
}

/*
if ($formbuilder) : ?>
<script data-main="/public/formbuilder/js/main-built.js" src="/public/formbuilder/js/lib/require.js" ></script>
*/

if ($autosize) echo $this->functions->jsScript('autosize/jquery.autosize.js');



//if ($rightlick) echo $this->functions->jsScript('rcmenu.js');

if ($inputedit)  echo $this->functions->jsScript('inputedit.js');

//if ($typeahead) echo $this->functions->jsScript('typeahead.0.9.3.min.js', '/public/js/', 'text/javascript', false);

if ($timepicker)
{
	echo $this->functions->jsScript('jquery.browser.js');
	echo $this->functions->jsScript('jquery.timePicker.js', 'public/timepicker/');
	echo $this->functions->cssScript('timePicker.css', 'public/timepicker/');
}

if ($googleMaps)
{
	//echo $this->functions->jsScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyAbQLHbC3h7BuowGR0C1i1n-F6rDjcFL3s&sensor=true', '', 'text/javascript', false);
	
	echo $this->functions->jsScript('googleMaps.js');
}

if ($colorpicker) echo $this->functions->minFiles('css/colorpicker.css js/colorpicker.js', 'public/colorpicker23.05.2009/');

if ($timeentry) echo $this->functions->minFiles('jquery.timeentry.css jquery.plugin.js jquery.timeentry.js', 'public/timeentry2/');

if ($jcrop) echo $this->functions->minFiles('js/jquery.Jcrop.js css/jquery.Jcrop.css', 'public/jcrop0.9.12/');

if ($nailthumb) echo $this->functions->minFiles('jquery.nailthumb.1.1.js jquery.nailthumb.1.1.css', 'public/js/nailthumb1.1/');

if ($justgal) echo $this->functions->minFiles('css/justifiedGallery.min.css js/jquery.justifiedGallery.min.js', 'public/js/justified-gallery/dist/');

//if ($masonry) echo $this->functions->jsScript('masonry.pkgd.min.js', '/public/js/', '', false);

if ($gridster)
{
	echo $this->functions->jsScript('gridster/dist/jquery.gridster.with-extras.js');
	echo $this->functions->cssScript('gridster/dist/jquery.gridster.css', 'public/js/');
	//echo $this->functions->cssScript('dashboard/jquery.dashboard.css', 'public/js/');
}



//if (DYNAMIC) echo EOL . "<script>" . EOL . "\tvar headlibs = " . json_encode($this->functions->minscripts) . ";" . PHP_EOL . "</script>" . PHP_EOL;

