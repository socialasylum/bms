<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>
<?php
$title = (empty($id)) ? urldecode($_GET['title']) : $info->title;


$attr = array
(
    'name' => 'docForm',
    'id' => 'docForm',
);

echo form_open('#', $attr);
?>


<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Documents</h1>


    <div id='doc-container'>
    <input type='hidden' name='id' id='id' value='<?=$id?>'>

    <?php if (!empty($id)) : ?>
        <input type='hidden' name='version' id='version' value='<?=$info->version?>'>
    <?php endif; ?>

<?php
    /*
    if (!empty($info->fileName)) echo "<input type='hidden' name='type' id='type' value='11'>"; // pdf
    elseif (!empty($info->video)) echo "<input type='hidden' name='type' id='type' value='3'>"; // video
    else echo "<input type='hidden' name='type' id='type' value='{$info->type}'>";
    */
?>

    <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>

    <?php if (!empty($info->file)) : ?>
        <input type='hidden' name='file' id='file' value='<?=$info->file?>'>
    <?php endif; ?>

    <?php if (!empty($info->video)) : ?>
        <input type='hidden' name='video' id='video' value='<?=$info->video?>'>
    <?php endif; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

<ul class="nav navbar-nav">
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">File <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <!-- <li><a tabindex="-1" role='button' href="#createDocModal" data-toggle='modal'><i class='icon-file'></i> New Document</a></li> -->
            <!-- <li class="divider"></li> -->
            <li><a tabindex="-1" role='button' id='saveBtn' href="#createDocModal"><i class='fa fa-save'></i> Save</a></li>

            <?php if ($info->type == 1) : ?>
            <!-- <li><a tabindex="-1" role='button' href="#createDocModal"><i class='icon-zoom-in'></i> Print Preview</a></li> -->
            <!-- <li><a tabindex="-1" role='button' href="#createDocModal"><i class='icon-print'></i> Print</a></li> -->
            <!-- <li><a tabindex="-1" role='button' href="#createDocModal"><i class='icon-file'></i> Export as PDF</a></li> -->
            <?php endif; ?>
            <li class="divider"></li>
            <li><a tabindex="-1"  role='button' id='exitBtn'><i class='fa fa-times-circle'></i> Exit</a></li>
        </ul>
  </li>

    <li><a tabindex="-1" role='button' href='#settingsModal' data-toggle='modal'><i class='fa fa-cog'></i> Settings</a></li>

    <li><a tabindex="-1" role='button' href='#positionModal' data-toggle='modal'><i class='fa fa-group'></i> Positions</a></li>
    <li><a tabindex="-1" role='button' href='#locationModal' data-toggle='modal'><i class='fa fa-map-marker'></i> Locations</a></li>

</ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

  
<div class='row announce-subject'>
    <div class='col-lg-12'>
    <input type='text' class='input-block-level doc-title form-control' id='title' name='title' value="<?=$title?>" placeholder="Document Title">
    </div>
</div>



<?php
if ($info->type == 11 OR !empty($info->fileName)) // PDF
{

    echo "<div id='pdf-container' class='doc-container'>\n";
    echo "<iframe src='/public/uploads/{$info->fileName}' style=\"width:100%; height:100%;\" frameborder='0'></iframe>";
    echo "</div>\n";
}
else if ($info->type == 3) // video
{
    $video = urlencode($info->video);

echo <<< EOS
    <a href="/public/training_videos/$video"
       style="display:block;width:575px;height:418px;margin:0 auto;"
       id="player">
    </a>
EOS;
}
else
{
    // echo "<div id='ckeditor-container' class='doc-container'>\n";
    echo "<textarea name='body' id='body' class='input-block-level'>{$info->body}</textarea>";
    // echo "</div>";
}
?>

    </div> <!-- #doc-container-->


<!-- position access modal //-->
<div id='positionModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Position Access</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='docAlert'></div>

        <p class='lead'>Select which position's will have access to this document.</p>

        <div class='row-fluid' style="margin-bottom:20px;">
	        <div class='checkbox'>
		        <label>
		            <input type='checkbox' id='selectAll' value='1'> Select all positions
				</label>
	        </div>
        </div>

        <div id='policyPositions'>
        <?php
            if(!empty($positions))
            {
                foreach ($positions as $r)
                {
                    // resets checked variable just in case check function throws an exception
                    $checked = null;

                    try
                    {
                        $check = $this->docs->checkPositionAssigned($id, $r->id);

                        $checked = ($check == true) ? "checked='checked'" : null;
                    }
                    catch(Exception $e)
                    {
                        $this->functions->sendStackTrace($e);
                    }


                    echo <<< EOS
        <div class='checkbox'>
        <label>
            <input type="checkbox" name='position[]' id='position_{$r->id}' value="{$r->id}" {$checked}> {$r->name}
        </label>
        </div> <!-- /.checkbox -->
EOS;

                }

            }
        ?>
        </div> <!-- #policyPositions //-->




    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



<!-- settings modal //-->
<div id='settingsModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Settings</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='docAlert'></div>

<div class="form-group">
    <label class="col-lg-3 control-label" for="active">Status</label>
    <div class='col-lg-9 controls'>
        <select class='form-control' name='active' id='active'>
        <?php
            foreach($status as $code => $display)
            {
                // new documents, sets to active by default
                if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                else $sel = ($info->active == $code) ? 'selected' : null;

                echo "<option {$sel} value='{$code}'>{$display}</option>\n";
            }
        ?>
        </select>

    </div>
</div>



<div class="form-group">
    <label class="col-lg-3 control-label" for="ack">Acknowledgement</label>
    <div class='col-lg-9 controls'>
        <select class='form-control' name='ack' id='ack'>
        <option value='0'></option>
        <?php

            if (empty($info->ack))
            {
                if ($info->reqSign == 1) $info->ack = 2;
                elseif ($info->reqView == 1) $info->ack = 1;
            }

        foreach($docack as $r)
        {
            $sel = ($info->ack == $r->code) ? "selected" : null;

            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>\n";
        }
        ?>
        </select>

    </div>
</div>

<?php

?>
<div class="form-group">
    <label class="col-lg-3 control-label" for="startDate">Start Date [<a href="javascript:void(0);" onclick="docs.startDateHelp();">?</a>]</label>
    <div class='col-lg-9 controls'>
        <input type='text' class='form-control' name='startDate' id='startDate' value="<?=$info->startDate?>">
    </div>
</div>



<div class="form-group">
    <label class="col-lg-3 control-label" for="endDate">End Date [<a href="javascript:void(0);" onclick="docs.endDateHelp();">?</a>]</label>
    <div class='col-lg-9 controls'>
        <input type='text' class='form-control' name='endDate' id='endDate' value="<?=$info->endDate?>">
    </div>
</div>


<div class="form-group">
    <label class="col-lg-3 control-label" for="newhire">Newhire Document</label>
    <div class='col-lg-9 controls'>
        <?php
        if(empty($id)) $hireCheck[0] = 'checked';
        else $hireCheck[$info->trainingDocument] = 'checked';
        ?>
        <div class='radio'>
	        <label>
	            <input type='radio' value='0' id='newhire_no' name='newhire' <?=$hireCheck[0]?>> No
	        </label>
        </div>

		<div class='radio'>        
	        <label>
	            <input type='radio' value='1' id='newhire_yes' name='newhire' <?=$hireCheck[1]?>> Yes 
	        </label>
		</div>
    </div>
</div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->





<!-- locations modal //-->
<div id='locationModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Location Access</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='locationAlert'></div>


    <div class="form-group">
        <label class='control-label' for=''>Access To</label>

            <select class='form-control' name="sendToType" id="sendToType">
                <option value=""></option>
                <!-- options -->
<?php
    if (!empty($sendTypes))
    {
        foreach ($sendTypes as $r)
        {
            $sel = ($r->code == $info->accessType) ? 'selected' : null;

            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
        }
    }
?>

            </select>

    </div>

    <hr>


    <div id='sendTo-display'></div>


    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->






</form>
