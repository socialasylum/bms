<?php if(!defined('BASEPATH')) die('Direct access not allowed');


if  ($code == 3)
{
    echo $this->alerts->info("All users will have access.");
    exit;
}


if (empty($locations))
{
    echo $this->alerts->info("No locations to send to");
}
else
{
    echo "<div id='location-container'>" . PHP_EOL;

    foreach ($locations as $k => $r)
    {
        $name = $checked = null;

        try
        {
            // send to a location
            if ($code == 1)
            {
                $checked = $this->docs->checkLocationAssigned($id, $r->id);

                $name = $r->name;
            }

            // send to a country
            if ($code == 2)
            {
                $checked = $this->docs->checkCountryAssigned($id, $r->code);

                $name = $r->display;
            }

            $id = ($code == 1) ? $r->id : $r->code;

            $checked = ($checked == true) ? 'checked' : null;
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

		echo "<div class='checkbox'>";
        echo "<label>";
        echo "<input type='checkbox' name='location[{$id}]' value='{$id}' {$checked}> {$name}";
        echo "</label>";
        echo "</div>" . PHP_EOL;
    }

    echo "</div>" . PHP_EOL;
}
