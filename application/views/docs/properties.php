<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3><?=$info->title?></h3>
    </div> <!-- .modal-header -->

    <div class='modal-body'>
    
    <div id='propAlert'></div>



<div class='tabbable'>
    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab">Settings</a></li>
        <li><a href='#tabPrev' data-toggle="tab">Previous Versions</a></li>
    </ul>

    <div class="tab-content">

        <div id="tabSettings" class='tab-pane active'>

<?php
/*
            <dl class='dl-horizontal'>
                <dt>Group</dt>
                <dd><?=$info->groupDisplay?></dd>
            </dl>
 */
?>
            <dl class='dl-horizontal'>
                <dt>Status</dt>
                <dd><?=$status[$info->active]?></dd>
                <!--
                <dd><?=($info->status == 1) ? 'Active' : 'Deactived'?></dd>
                //-->
            </dl>


            <dl class='dl-horizontal'>
                <dt>Acknowledgement</dt>
                <dd><?=(empty($ackDisplay)) ? "<span class='muted'><i>Unavailable (Update Document)</i></span>" : $ackDisplay?></dd>
            </dl>



            <?php if (!empty($info->startDate)) : ?>
            <dl class='dl-horizontal'>
                <dt>Start Date</dt>
                <dd><?=$info->startDate?></dd>
            </dl>
            <?php endif; ?>


            <?php if (!empty($info->endDate)) : ?>
            <dl class='dl-horizontal'>
                <dt>End Date</dt>
                <dd><?=$info->endDate?></dd>
            </dl>
            <?php endif; ?>

            <?php if ($info->type == 1 && !empty($info->idocFileName)) : ?>
            <dl class='dl-horizontal'>
                <dt>iDoc File</dt>
                <dd><?=$info->idocFileName?></dd>
            </dl>
            <?php endif; ?>

            <dl class='dl-horizontal'>
                <dt>Training Document</dt>
                <dd><?=($info->trainingDocument == 1) ? 'Yes' : 'No'?></dd>
            </dl>


        </div> <!-- #tabSettings //-->


        <div id="tabPrev" class='tab-pane'>

<?php
if (!empty($previousVersions))
{
echo <<< EOS
            <table class='table table-bordered table-striped' id='docVersionTbl'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Employee</th>
                        <th>Position</th>
                        <th>Version</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
EOS;


    $cnt = 1;
    foreach ($previousVersions as $r)
    {

        try
        {
            // $time = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "g:i A");

            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "m/d/Y g:i A");

            $name = $this->users->getName($r->userid);

            $position = $this->users->getTableValue('position', $r->userid);

            $positionName = $this->positions->getName($position);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>\n";

        $ast = ($cnt == 1) ? '*' : null;

        echo "\t<td>{$date}</td>\n";
        echo "\t<td>{$name}</td>\n";
        echo "\t<td>{$positionName}</td>\n";
        echo "\t<td>{$r->version}{$ast}</td>\n";
        echo "\t<td><a href='/docs/view/{$id}/0/0/{$r->version}' class='btn btn-default pull-right' target='_blank'><i class='fa fa-file-text'></i></a></td>\n";

        echo "</tr>\n";

    $cnt++;
    }

    echo "\t</tbody>\n";
    echo "</table>\n";
}
?>
        <p><small>* Current version</small></p>

        </div>

    </div> <!-- .tab-content //-->
</div> <!-- .tabbable //-->

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelPropBtn'>Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->


