<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <!-- <div class='module-head'><h3><?=$info->title?></h3></div> -->
    <h1><?=$info->title?></h1>


    <div class='docBody'>
    <?=$info->body?>
    </div>


<?php
if (empty($signatureData))
{
    $attr = array
        (
            'name' => 'docViewForm',
            'id' => 'docViewForm'
        );

echo form_open('#', $attr);
?>

    <input type='hidden' name='id' id='id' value='<?=$id?>'>
    <input type='hidden' name='folder' id='folder' value='<?=$folder?>'>
    <input type='hidden' name='rootFolder' id='rootFolder' value='<?=$rootFolder?>'>
    <input type='hidden' name='version' id='version' value='<?=$info->version?>'>
    <input type='hidden' name='ack' id='ack' value='<?=$info->ack?>'>

<div class='row'>
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-3'>
        
    <div class='panel panel-default'>
            <?php if ($info->ack == 1) : ?>
            <div class='panel-heading'>Signature Required</div>

            <div class='panel-body form-horizontal'>
                <div id='ackAlert'></div>

                <p class='lead'><?=$userName?> this document requires your signature. Please enter your password to sign this document.</p>

                <div class="form-group">
                    <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='password'>Password</label>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
                        <input type='password' class='form-control' name='password' id='password' value="" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
                    </div> <!-- .controls -->
                </div> <!-- .form-group -->

                <div class="form-group">
                    <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='date'>Date</label>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
                    <label><?=$date?></label>
                    </div> <!-- .controls -->
                </div> <!-- .form-group -->


            </div> <!-- .panel-body -->
            <?php elseif ($info->ack == 2) : ?>
                <div class='panel-heading'>Please read &amp; acknowledge</div>
                <div class='panel-body form-horizontal'>
                    <p class='lead'><?=$userName?> this document requires that you read and acknowledge you understand it.</p>

                </div> <!-- .panel-body -->
            <?php endif; ?>

        <div class='panel-footer'>
<?php
    if ($info->ack == 1)
    {
        // requires signature
        echo "<button type='button' class='btn btn-primary' id='signDocBtn'>Sign Document</button>";
    }
    else if($info->ack == 2)
    {
        // requires at least acknowledment of document
        echo "<button type='button' class='btn btn-primary' id='ackDocBtn'>I have read and understand this document.</button>";
    }
    else
    {
        // no acknowledgement required
        echo "<button type='button' id='returnBtn' class='btn btn-info'>&laquo; Return to Documents</button>";
    }
?>
        </div>

    </div>
    </div> <!-- col6 -->
    </div> <!-- .row -->

</form>
<?php
}
else
{

    try
    {
        $date = $this->users->convertTimezone($user, $signatureData->datestamp, "m/d/Y g:i A");
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

    if ($info->ack == 1)
    {
        echo "<div class='well'>{$userName} signed this document on {$date}</div>";
    }
    elseif ($info->ack == 2)
    {

        echo "<div class='well'>{$userName} read and acknowledged this document on {$date}</div>";
    }

        echo "<button type='button' id='returnBtn' class='btn btn-info'>&laquo; Return to Documents</button>";
}
?>
<!--

    <div class='form-actions'>
        <button type='button' id='returnBtn' class='btn btn-info'>&laquo; Return to Documents</button>

        <div class='pull-right'>
            <button type='button' id='printBtn' class='btn'><i class='icon-print'></i></button>
        </div>

    </div>
//-->
