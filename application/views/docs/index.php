<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <h1><i class='fa fa-file-text'></i> Documents</h1>

    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>


    <input type='hidden' id='admin' value='<?=(int) $admin?>'>

    <input type='hidden' id='rootFolder' value='<?=$rootFolder?>'>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
<ul class="nav navbar-nav">
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">File <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a tabindex="-1" role='button' href="#createDocModal" data-toggle='modal'><i class='fa fa-file'></i> New Document</a></li>
            <li><a role='button' href="javascript:void(0);" id='createFolderBtn'><i class='fa fa-folder-open'></i> New Folder</a></li>
            <!-- <li><a tabindex="-1" role='button' href="#createVideoModal" data-toggle='modal'><i class='fa fa-facetime-video'></i> New Video</a></li> -->
            <li><a tabindex="-1" role='button' href="#createPDFModal" data-toggle='modal'><i class='fa fa-upload'></i> Upload PDF</a></li>
        </ul>
  </li>

</ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<input type='hidden' id='folder' value='<?=$folder?>'>

<div id='doc-container'>
<?php
if (empty($content) && empty($folder))
{
    echo $this->alerts->info("This folder is currently empty!");
}
else
{


    echo "<ol id='fileList'>\n";

    if (!empty($folder))
    {
        try
        {
            if (!empty($parentFolder)) $parentFolderName = $this->folders->getTableValue('name', $parentFolder);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "\t<li class='ui-state-default folder' value='{$parentFolder}' itemType='1' id='folder_{$parentFolder}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>../{$parentFolderName}</div></li>\n";
    }


    foreach ($content as $r)
    {
        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        if ($r->type == 1)
        {
            $class = ($r->active == 1) ? null : ' hidden-doc';
            
            echo "\t<li class='ui-state-default{$class} folder' value='{$r->id}' itemType='1' id='folder_{$r->id}'><img src='/public/images/windows_folder_icon_trans.png'><div class='folderName'>{$name}</div></li>\n";
        }
        elseif ($r->type == 2)
        {
            if ($r->policyType == 3) $img = 'movie-icon.png'; // video
            elseif ($r->policyType == 11) $img = 'pdf-icon.png'; // pdf
            else $img = 'doc_blue_trans.png';

            $class = ($r->active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class}' value='{$r->id}' itemType='2' id='item_{$r->id}' tbl='doc'><i class='fa fa-file-text drag-icon'></i><div class='docName'>{$name}</div></li>\n";
        }

    }

    echo "</ol>\n";
}
?>
</div> <!-- #form-container -->

<!-- new document modal //-->
<div id='createDocModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Create a New Document</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='docAlert'></div>

        <p class='lead'><img src='/public/images/doc_blue.png'> Enter the name of your new document</p>

        <form name='createDocForm' id='createDocForm'>
        <p><input type='text' class='form-control' name='docName' id='docName'  placeholder='Document Name'></p>
        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createDocBtn'>Create Document</button>
    </div> <!-- .modal-footer //-->
        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->




<!-- new PDF modal //-->
<div id='createPDFModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3>Upload a PDF</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='pdfAlert'></div>

        <p class='lead'><img src='/public/images/pdf-icon.png'> Enter the title of your PDF and upload the .pdf file from your computer.</p>

        <form name='uploadPDFForm' id='uploadPDFForm' method='post' action='/docs/upload' enctype="multipart/form-data">
        <input type='hidden' name='type' value='11'>
        <input type='hidden' name='folder' value='<?=$folder?>'>
        <p><input type='text' class='form-control' name='pdfName' id='pdfName' placeholder='Document Title'></p>
        <p><input type='file' class='input-large' name='pdfFile' id='pdfFile'></p>
        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelUploadPDFBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='uploadPDFBtn'>Upload PDF</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->


<!-- Properties Modal //-->
<div id='propertiesModal' class='modal fade'></div>


