<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-puzzle-piece'></i> Modules</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><a href='/module/edit'><i class='fa fa-plus'></i> Create Module</a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php
if (empty($modules))
{
    echo $this->alerts->info("No modules available =(");
}
else
{
echo <<< EOS
<table class='table table-striped table-condensed' id='modTbl'>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Url</th>
            <th>Namespace</th>
            <th>Sellable?</th>
            <th>Univ.</th>
            <th>PPU</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
EOS;

    foreach ($modules as $r)
    {

        // $sellable = ($r->sellable == 1) ? 'Sellable' : 'Not selling';
        $sellable = ($r->sellable == 1) ? "<i class='icon-dollar'></i>" : '&nbsp;';
        $unvPackage = ($r->unvPackage == 1) ? "<i class='icon-globe'></i>" : '&nbsp;';

        $price = ($r->sellable == 1) ?  '$' . number_format($r->price, 2) : '&nbsp;';

        echo "<tr>";

        echo "<td>{$r->id}</td>";
        echo "<td>{$r->name}</td>";
        echo "<td>{$r->url}</td>";
        echo "<td>{$r->namespace}</td>";
        echo "<td>{$sellable}</td>";
        echo "<td>{$unvPackage}</td>";
        echo "<td>{$price}</td>";
        echo "<td>";

        echo "<button type='button' onclick=\"module.editModule(this, {$r->id});\" class='btn btn-info btn-xs pull-right' title=\"Edit {$r->name} module\"><i class='fa fa-pencil'></i></button>";

        echo "</td>";

        echo "</tr>" . PHP_EOL;

    }

echo "</tbody>";
echo "</table>";
}
?>
