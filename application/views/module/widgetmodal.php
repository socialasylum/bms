<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <div class='modal-dialog'>
        <div class='modal-content'>



    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'><?=(empty($id)) ? 'Create' : 'Edit'?> Widget</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='widgetAlert'></div>

<?php

    $attr = array
        (
            'name' => 'widgetForm',
            'id' => 'widgetForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);

    if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

?>

    <input type='hidden' name='module' id='module' value='<?=$module?>'>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='name'>Name</label>
        <div class="col-lg-9 controls">
            <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>" placeholder='Name'>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='url'>Url</label>
        <div class="col-lg-9 controls">
            <input type='text' class='form-control' name='url' id='url' value="<?=$info->url?>" placeholder='/controller/method'>
        </div>
    </div>


    <div class="form-group">
        <label class="col-lg-3 control-label" for="active">Status</label>
        <div class="col-lg-9 controls">
            <select class='form-control' name='active' id='active'>
            <?php
                foreach($status as $code => $display)
                {
                    // new documents, sets to active by default
                    if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                    else $sel = ($info->active == $code) ? 'selected' : null;

                    echo "<option {$sel} value='{$code}'>{$display}</option>\n";
                }
            ?>
            </select>

        </div>
    </div>



        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='saveWidgetBtn' onclick="module.saveWidget();">Save</button>
    </div> <!-- .modal-footer //-->


        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
