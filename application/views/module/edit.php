<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Module</h1>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>

<?php if (!empty($id)) : ?>
        <li><a href='javascript:void(0);' id='newWidgetBtn'><i class='fa fa-plus'></i> New Widget</a></li>
<?php endif; ?>
    </ul>

    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
    </ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>




    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm'
        );

echo form_open('#', $attr);

if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
?>




<ul class='nav nav-tabs'>
    <li class='active'><a href='#tabSettings' data-toggle="tab"><i class='fa fa-cog'></i> Settings</a></li>
    <li><a href='#tabComps' data-toggle="tab"><i class='fa fa-building'></i> Assigned Companies</a></li>
    <li><a href='#tabWidgets' data-toggle="tab"><i class='fa fa-gears'></i> Widgets</a></li>
    <li><a href='#tabLibs' data-toggle="tab"><i class='fa fa-file-text'></i> Libraries</a></li>
</ul>

<div class="tab-content">

    <div id="tabSettings" class='tab-pane active form-horizontal'>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='name'>Name</label>
        <div class='col-lg-10 controls'>
            <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>" placeholder='Module Name'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='url'>URL</label>
        <div class='col-lg-10 controls'>
            <input type='text' class='form-control' name='url' id='url' value="<?=$info->url?>">
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='namespace'>Namespace</label>
        <div class='col-lg-10 controls'>
            <input type='text' class='form-control' name='namespace' id='namespace' value="<?=$info->namespace?>">
        </div>
    </div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='active'>Status</label>
    <div class='col-lg-10 controls'>
        <select class='form-control' name='active' id='active'>
        <?php
            foreach($status as $code => $display)
            {
                // new documents, sets to active by default
                if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                else $sel = ($info->active == $code) ? 'selected' : null;

                echo "<option {$sel} value='{$code}'>{$display}</option>\n";
            }
        ?>
        </select>


    </div>
</div>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='price'>Price</label>
        <div class='col-lg-10 controls'>
            <div class='input-group'>
            <span class='input-group-addon'>$</span>
            <input type='text' class='form-control' name='price' id='price' value="<?=$info->price?>">
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-2 control-label' for='icon'>Icon</label>
        <div class='col-lg-10 controls'>
            <input type='text' class='form-control' name='icon' id='icon' value="<?=$info->icon?>">

            <span class='help-block'><a href='http://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>Click here for icons</a></span>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-2 control-label' for='usesFolders'><i class='fa fa-folder-open'></i> Folders</label>
        <div class='col-lg-10 controls'>
                <input type='hidden' name='sellable' id='sellable' value='0'>
                
			<div class='checkbox'>
            <label>

<?php
    $folderChecked = ($info->usesFolders == 1) ? "checked='checked'" : null;
?>
                <input type='checkbox' name='usesFolders' id='usesFolders' value='1' <?=$folderChecked?>> Modules utilizes folders.
            </label>
			</div>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-2 control-label' for='sellable'>Sellable</label>
        <div class='col-lg-10 controls'>
			
			<div class='checkbox'>
            <label>
                <input type='hidden' name='sellable' id='sellable' value='0'>
<?php
    $sellChecked = ($info->sellable == 1) ? "checked='checked'" : null;
?>
                <input type='checkbox' name='sellable' id='sellable' value='1' <?=$sellChecked?>> List as a purchasable module
            </label>
			</div>
			
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-2 control-label' for='unvPackage'>Universal Package</label>
        <div class='col-lg-10 controls'>

        <div class='checkbox'>
            <label>
                <input type='hidden' name='unvPackage' id='unvPackage' value='0'>
<?php
    $unvChecked = ($info->unvPackage == 1) ? "checked='checked'" : null;
?>
                <input type='checkbox' name='unvPackage' id='unvPackage' value='1' <?=$unvChecked?>> All companies have access to this module
            </label>
        </div>
        
        </div>
    </div>


<div class="form-group">
    <label class='col-lg-2 col-md-2 col-sm-2 control-label' for='description'>Description</label>
    <div class="col-lg-10 col-md-10 col-sm-10 controls">
        <textarea id='description' name='description' class='form-control' rows='5'><?=$info->description?></textarea>
    </div> <!-- .controls -->
</div> <!-- .form-group -->



<div class="form-group">
    <label class='col-lg-2 col-md-2 col-sm-2 control-label' for='shortDesc'>Short Description</label>
    <div class="col-lg-10 col-md-10 col-sm-10 controls">
        <textarea name='shortDesc' id='shortDesc' rows='4' class='form-control'><?=$info->shortDesc?></textarea>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


    </div> <!-- .tabSettings //-->

    <div id="tabComps" class='tab-pane'>
<?php
if (empty($companies))
{
    echo $this->alerts->info("No companies have been created");
}
else
{
echo <<< EOS
    <table class='table table-striped table-condensed' id='compTbl'>
        <thead>
            <tr>
                <th><input type='checkbox'></th>
                <th>ID</th>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Postal Code</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($companies as $r)
    {

        $checked = null;

        $status = ($r->active == 1) ? 'Active' : 'Inactive';

        if  (!empty($id))
        {
            if (!empty($modComps))
            {
                foreach ($modComps as $mcr)
                {
                    if ($mcr->company == $r->id)
                    {
                        $checked = "checked='checked'";
                        break;
                    }
                }
            }
        }

        echo "<tr>";

        echo "<td><input type='checkbox' name='company[]' id='company_{$r->id}' value='{$r->id}' {$checked}></td>";
        echo "<td>{$r->id}</td>";
        echo "<td>{$r->companyName}</td>";
        echo "<td>{$r->addressLine1}</td>";
        echo "<td>{$r->city}</td>";
        echo "<td>{$r->state}</td>";
        echo "<td>{$r->postalCode}</td>";
        echo "<td>{$status}</td>";

        echo "</tr>";

    }

echo "</tbody>";
echo "</table>";
}
?>


    </div> <!-- #tabComps -->

    <div id="tabWidgets" class='tab-pane'>


<p class='lead'>Below is a list of all widgets for this module. Here you can edit and create new widget rows.</p>

<?php
if (empty($widgets))
{
    echo $this->alerts->info("There are no widgets for this module!");
}
else
{
echo <<< EOS
<table class='table table-stripped table-bordered' id='widgetTbl'>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>URL</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>

EOS;

    foreach ($widgets as $r)
    {

        echo "<tr>";

        echo "<td>{$r->id}</td>";
        echo "<td>{$r->name}</td>";
        echo "<td>{$r->url}</td>";
        echo "<td><a href='javascript:void(0);' class='btn btn-link pull-right' onclick=\"module.loadWidgetModal({$r->id});\"><i class='fa fa-pencil'></i></a></td>";

        echo "</tr>";

    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>
    </div> <!-- #tabWidgets -->
    

	<div id="tabLibs" class='tab-pane'>

        <div class='col-md-4' id='tree'>
        <?php
        echo $ul;
        ?>
        
        </div> <!-- /.col-8 -->
        
        <div class='col-md-8'>
        	<h2>Libraries</h2>
        	
        	<p class='lead'>Select which libraries this module will utilize.</p>
        	
        	<div id='selectedModules'>
        	
        	</div>
        </div> <!-- col-8 -->
	</div> <!-- tablibs -->    

</div> <!-- .tabContent //-->


<?php
/*
<div class='row'>
    <div class='col-lg-12'>

    <button type='button' class='btn btn-primary' name='saveBtn' id='saveBtn'>Save</button>
    <button type='button' class='btn' name='cancelBtn' id='cancelBtn'>Cancel</button>

    </div>
</div>

 */
?>

</form>

<!-- widget modal //-->
<div id='widgetModal' class='modal fade'></div>
