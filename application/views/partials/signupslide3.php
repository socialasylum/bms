<div class="container clearfix" id="payment">
    <div class="signupcontainer">
        <div class="signupheadercontainer">
            <div class="col-md-6 signupheader">
                SUBSCRIPTIONS INFO
            </div>
            <div class="col-md-6 signupheader">
                ENTER BILLING INFO
            </div>
        </div>
        <div>
            <div class="col-md-6 left-panel">
                <div>
                    Select Total Users
                </div>
                <div>
                    <label><span id="numusersspan">10</span> Users</label>
                    <input type="text" name="numusers" id="numusers" value="10" />
                    <input type="hidden" name="price" id="price" value="50" />
                </div>
                <div>
                    Total Per Month ($5.00 per user)
                </div>
                <div>
                    $<span id="totalprice">50</span> MO
                </div>
            </div>
            <div class="col-md-6 right-panel">
                <div class="inputfield">
                    <label>Name on Card</label>
                    <input type="text" name="cardname" id="cardname" />
                </div>
                <div class="inputfield">
                    <label>Card Number</label>
                    <input type="text" name="cardnum" id="cardnum" />
                </div>
                <div class="inputfield">
                    <label>Card Expiration</label>
                    <input type="text" name="ccexpm" id="ccexpm" placeholder="MM" size="2" />
                    <input type="text" name="ccexpy" id="ccexpy" placeholder="YY" size="2" />
                </div>
                <div class="inputfield">
                    <label>CVV</label>
                    <input type="text" name="cvv" id="cvv" />
                </div>
            </div>
        </div>
    </div>
</div>
