<div class="container clearfix" id="about">
    <div class="signupcontainer">
        <div class="signupheadercontainer">
            <div class="col-md-12 signupheader">
                ABOUT YOUR BUSINESS
            </div>
        </div>
        <div>
            <div class="col-md-6 col-md-offset-3">
                <div class="inputfield">
                    <label>Company Name</label>
                    <input type="text" name="companyName" id="cname" />
                </div>
                <div class="inputfield">
                    <label>Company Phone Number</label>
                    <input type="text" name="phone" id="phone" />
                </div>
                <div class="inputfield">
                    <label>Email Domain Name</label>
                    <input type="text" name="emaildomain" id="emaildomain" />
                </div>
                <div class="inputfield">
                    <input type="checkbox" name="terms" id="terms" /> Agree to terms and conditions. <a href="/terms" target="_blank">View</a>
                </div>
            </div>
        </div>
    </div>
</div>