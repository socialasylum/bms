<div class="container clearfix" id="newusers">
    <div class="signupcontainer">
        <div class="signupheadercontainer">
            <div class="col-md-12 signupheader">
                Your Information
            </div>
        </div>
        <div>
            <div class="col-md-6 col-md-offset-3">
                <div class="inputfield">
                    <label>Your New Email</label>
                    <div class="input-group">
                        <input type="text" name="email" id="email" /><span class="input-group-addon" id="newemaildomain"></span>
                    </div>
                    <label>First Name</label>
                    <input type="text" name="firstName" id="firstName" />
                    <label>Last Name</label>
                    <input type="text" name="lastName" id="lastName" />
                    <label>Password</label>
                    <input type="password" name="password" id="password" />
                    <label>Confirm Password</label>
                    <input type="password" name="password2" id="password2" />
                </div>
            </div>
        </div>
    </div>
</div>
