<div class="container clearfix" id="confirm">
    <div class="signupcontainer">
        <div class="signupheadercontainer">
            <div class="col-md-12 signupheader">
                To complete your setup, please point your MX records to the following domains
            </div>
        </div>
        <div class="emailinfo">
            <h3>mail.greennvy.com</h3>
            <h3>smtp.greennvy.com</h3>
        </div>
        <div class="signupheadercontainer">
            <div class="col-md-12 signupheader">
                In an effort to better serve your users, please add the following A records to your DNS
            </div>
        </div>
        <div class="emailinfo">
            <h3>autoconfig.<span class="domainname"></span> to 107.170.228.79</h3>
            <h3>autodiscover.<span class="domainname"></span> to 107.170.228.79</h3>
        </div>
    </div>
</div>