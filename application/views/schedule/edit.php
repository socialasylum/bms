<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='clearfix'>
<h1 class='pull-left'><i class='icon-edit'></i> Edit Schedule</h1>

<h3 class='pull-right'><?=date("l, F j, Y", strtotime($date))?> <label class='label label-info'><?=$this->locations->getTableValue('name', $location)?></label></h3>


    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>





</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<p class='lead'>Select an employee and select their shift start and end times</p>

<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>


    <input type='hidden' name='location' id='location' value='<?=$location?>'>
    <input type='hidden' name='date' id='date' value='<?=$date?>'>

<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='user'>User</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        <select name="user" id="user" class='form-control'>
            <option value=""></option>
<?php
if (!empty($users))
{
    foreach ($users as $r)
    {
        $name = null;

        try
        {
            $name = $this->users->getName($r->userid);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<option value='{$r->userid}'>{$name}</option>" . PHP_EOL;
    }
}
?>
        </select>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='shiftStart'>Shift Start</label>
    <div class="col-lg-3 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' id='shiftStart' name='shiftStart' value="">
    </div> <!-- .controls -->
</div> <!-- .form-group -->



<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for='shiftEnd'>Shift End</label>
    <div class="col-lg-3 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' id='shiftEnd' name='shiftEnd' value="">
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-2 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 controls">
        
        <button type='button' class='btn btn-primary' name='saveBtn' id='saveBtn'>Add Shift</button>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

</form>

<hr>

<h2><i class='icon-calendar'></i> Current Day Schedule</h2>
<p class='lead'>Below is a list of all employee's scheduled to work on this day.</p>

<?php
if (empty($schedule))
{
    echo $this->alerts->info("There are no employees scheduled to work on this day.");
}
else
{
    echo <<< EOS
    <table class='table table-bordered'>
        <thead>
            <tr>
                <th>User</th>
                <th>Scheduled By</th>
                <th>Shift Start</th>
                <th>Shift End</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($schedule as $r)
    {
        $name = $submitName = null;

        try
        {
            $name = $this->users->getName($r->userid);
            $submitName = $this->users->getName($r->submittedBy);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        echo "<tr>" . PHP_EOL;

        echo "<td>{$name}</td>";
        echo "<td>{$submitName}</td>";
        echo "<td>{$r->shiftStart}</td>";
        echo "<td>{$r->shiftEnd}</td>";
        echo "<td><button type='button' class='btn pull-right' onclick=\"schedule.deleteDaySchedule(this, {$r->id});\"><i class='icon-trash'></i></button></td>";

        echo "</tr>" . PHP_EOL;
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>
