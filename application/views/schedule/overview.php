<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>



<hr>


<?php
if (empty($location))
{
    echo $this->alerts->alert("Please select a location!");
    exit;
}
?>

<div class='row'>
    <div class='col-lg-6'>
        <h4><?=date("F Y", mktime(0, 0, 0, $month, 1, $year))?></h4>
    </div>

    <div class='col-lg-6'>
        <div class='btn-group pull-right'>
            <a href="javascript:schedule.loadStoreSchedule(<?=date("n, Y", mktime(0, 0, 0, ($month - 1), 1, $year))?>);" class='btn btn-info' name='prevBtn' id='prevBtn'><i class='fa fa-chevron-left'></i></a>
            <a href="javascript:schedule.loadStoreSchedule(<?=date("n, Y", mktime(0, 0, 0, ($month + 1), 1, $year))?>);" class='btn btn-info' name='nextBtn' id='nextBtn'><i class='fa fa-chevron-right'></i></a>
        </div> <!-- .btn-group -->

    </div> <!-- .col-6 -->
</div> <!-- .row -->


<div class='row'>
    <div class='col-lg-12'>

<table class='calendar table table-bordered'>
    <thead>
        <tr>
            <th>Sunday</th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
            <th>Saturday</th>
        </tr>
    </thead>
    <tbody>
<?php

$first = date("w", mktime(0, 0, 0, $month, 1, $year));


$startDate = mktime(0, 0, 0, $month, (1 - $first), $year);

$endDate = mktime(0, 0, 0, ($month + 1), (1-1), $year);

$end = date('n', $endDate);


$diff =  (int) date("z", $endDate) - (int) date("z", $startDate);
// echo $diff;

    if ($diff < 0) $diff += 365;

$extend = 7 - (int) date("N", $endDate);

// echo "EXTEND: {$extend}<BR>";

$rcnt = 1;

$total = $diff + $extend;

for ($i = 1; $i <= $total; $i++)
{


    $class = $tdClass = null;

    $utDay = $startDate + (($i - 1) * 86400);

    $day = date("d", $utDay);

    $date = date("Y-m-d", $utDay);

    if ($i > $first && $i <= ($total - $extend)) $class = 'label-success';
    else $tdClass = 'unavail-day';

    try
    {
        $schedule = $this->schedule->getDaySchedule($location, $date);
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }


    if ($rcnt == 1) echo "<tr>" . PHP_EOL;

    echo "\t<td class='{$tdClass}'><div class='day-container'>";

    if (strtotime('now') <= ($utDay + 86400))
    {
        echo "<a href='/schedule/edit/{$location}?date=" . urlencode($date) . "' class='btn btn-info btn-xs calDay'><i class='icon-pencil'></i> {$day}</a>";
    }
    else
    {
        echo "<div class='label label-default calDay'>{$day} </div>";
    }


    // displays current schedule for
    if (empty($schedule))
    {
    }
    else
    {
        foreach ($schedule as $r)
        {
            $empName = null;

            try
            {
                $empName = $this->users->getName($r->userid);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

            echo "<span class='label label-warning label-schedule'>{$empName}<br>{$r->shiftStart} - {$r->shiftEnd}</span>";
        }
    }


    echo "</div></td>" . PHP_EOL;

    if ($rcnt >= 7)
    {
        echo "</tr>" . PHP_EOL;
        $rcnt = 1;
    }
    else
    {
        $rcnt++;
    }
}


?>
    </tbody>
</table>


    </div>
</div> <!-- .row -->
