<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='icon-calendar'></i> Schedule</h1>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<p class='lead'>Select a location to view and manage the schedule.</p>

<div class='row'>
    <div class='col-lg-12 form-horizontal'>

<div class="form-group">
    <label class='col-lg-2 control-label' for='ID'>Location</label>
    <div class="col-lg-10 controls">
        <select name="location" id="location" class='form-control'>
            <option value=""></option>
<?php
if (!empty($locations))
{
    foreach ($locations as $r)
    {
        $name = null;

        $sel = ($r->homeLocation == 1) ? "selected='selected'" : null;

        try
        {
            $name = $this->locations->getTableValue('name', $r->location);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<option {$sel} value='{$r->location}'>{$name}</option>" . PHP_EOL;
    }
}
?>
        </select>
    </div>
</div>

    </div>
</div>


<div id='schedule-display'></div>
