<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-refresh'></i> Compatibility</h1>

<hr>
<h2><i class='fa fa-desktop'></i> Browser Compatibility</h2>
<p class='lead'>Currently the Business Management System is designed to run optimally in the latest versions <a href='http://www.mozilla.org/en-US/firefox/new/' target='_blank'>FireFox</a> and <a href='http://google.com/chrome' target='_blank'>Google Chrome</a>.</p>

<p class='mainTxt'>The Business Management System is designed to utilize the latest in HTML 5 and CSS 3 as well as other technologies which are greatly supported by both FireFox and Google Chrome. Because of this, we recommend using the system in either of those browsers for a better browsing experience.</p>

<p>The system is tested in Internet Explorer<sup>&reg;</sup> 9+, however using Internet Explorer<sup>&reg;</sup> is <strong>not recommended</strong>.</p>
