<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<input type='hidden' name='namespace' id='namespace' value='<?=$namespace?>'>

<div class="row">
<div class="col-lg-3">
    <?php include_once 'nav.php'; ?>
</div>

<div class="col-lg-9">

    <h1><i class='fa fa-search'></i> Search</h1>

        <p class='lead'>Search results for <strong><?=$_GET['q']?></strong></p>


    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>


    <input type='hidden' id='admin' value='<?=(int) $admin?>'>




<div class="tab-content">


    <div id="tabKBArticles" class='tab-pane active'>

<?php

$totalKBCnt = 0;

if (empty($kbResults['matches']))
{
    echo $this->alerts->info('No knowledge base articles found!');
}
else
{

    echo "<div id='kbArticle-container'>";

    echo "<div class='row'>\n";
    echo "<ol id='fileList'>\n";


    foreach ($kbResults['matches'] as $k => $v)
    {

        try
        {
            $company = $this->knowledgebase->getTableValue('company', $v['id']);
        
            if (empty($company) || $company == $this->session->userdata('company'))
            {
                // do nothing
            }
            else
            {
                // article is not 0 or set for this company
                continue;
            }


            $name = $this->knowledgebase->getTableValue('title', $v['id']);
            $active = $this->documents->getTableValue('active', $v['id']);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $name = (strlen($name) > 25) ? substr($name, 0, 22) . '...' : $name;

        if ($type == 3) $img = 'movie-icon.png'; // video
        elseif ($type == 11) $img = 'pdf-icon.png'; // pdf
        else $img = 'doc_blue_trans.png';

        $docType = $type;

        $class = ($active == 1) ? null : ' hidden-doc';

        echo "\t<li class='ui-state-default{$class} kbArticle' value='{$r->id}' itemType='2' id='kb_{$r->id}' onclick=\"search.viewKBArticle({$v['id']});\"><i class='fa fa-file-text drag-icon'></i><div class='docName'>{$name}</div></li>\n";

        $totalKBCnt++;

    }

    echo "</ol>\n";

    echo "</div> <!-- .row -->" . PHP_EOL;
    echo "</div> <!-- #bkArticle-container -->" . PHP_EOL;
}
?>

    <input type='hidden' name='totalKBCnt' id='totalKBCnt' value='<?=$totalKBCnt?>'>

    </div> <!-- #tabKBArticles -->

    <div id="tabCRMContacts" class='tab-pane'>

<?php

$totalCRMCnt = 0;

if (empty($crmResults['matches']))
{
    echo $this->alerts->info('No CRM contacts found!');
}
else
{
    echo <<< EOS
    <table id='crmTblSearch' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>Company Name</th>
                <th>Name</th>
                <th>Title</th>
                <th>Email</th>
                <th>Type</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
                <th>Phone</th>
            </tr>
        </thead>
        <tbody>

EOS;

    foreach ($crmResults['matches'] as $k => $v)
    {
        $type = $status = $address = $phone = $email = null;
        $contactCompany = $info = null;
        try
        {
            // first checks company assigned
            $contactCompany = $this->crm->getContactCompany($v['id']);

            // skips row if now this companies crm contact
            if ((int) $contactCompany  !== (int) $this->session->userdata('company')) continue;

            // gets contact info
            $info = $this->crm->getContactInfo($v['id']);

            if (!empty($info->type)) $type = $this->functions->codeDisplay(8, $info->type);

            $email = $this->crm->getContactEmail($v['id']);

            $address = $this->crm->getContactAddress($v['id']);
            $phone = $this->crm->getContactPhone($v['id'], 1);
            $fax = $this->crm->getContactPhone($v['id'], 3);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
            continue; // skips row if there was a sql error
        }

        echo "<tr onclick=\"search.viewCrmContact({$v['id']})\">" . PHP_EOL;

        echo "\t<td>{$info->companyName}</td>";
        echo "\t<td>{$info->name}</td>";
        echo "\t<td>{$info->title}</td>";
        echo "\t<td>{$email}</td>" . PHP_EOL;
        echo "\t<td>{$type}</td>" . PHP_EOL;
        echo "\t<td>{$address->address}</td>";
        echo "\t<td>{$address->city}</td>";
        echo "\t<td>{$address->state}</td>";
        echo "\t<td>{$address->postalCode}</td>";
        echo "\t<td>{$phone}</td>";
        echo "</tr>" . PHP_EOL;

        $totalCRMCnt++;

    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>


    <input type='hidden' name='totalCRMCnt' id='totalCRMCnt' value='<?=$totalCRMCnt?>'>

    </div> <!-- #tabCRMContacts -->

    <div id="tabUsers" class='tab-pane'>

<?php
$totalUserCnt = 0;

$userAlert = $this->alerts->info('No users found!');


if (empty($userResults['matches']))
{
    echo $userAlert;
}
else
{
    echo "<table id='userSearchTbl' class='table table-hover table-bordered'>
        <thead>
            <tr>
                <th>Name</th>
                <th>ID</th>
                <th>Email</th>
                <th>Position</th>
            </tr>
        </thead>
        <tbody>";

    foreach ($userResults['matches'] as $k => $v)
    {
        try
        {
            $company = $this->companies->userAssignedToCompany($v['id'], $this->session->userdata('company'));

            // skips user if they are not assigned to this current company
            if ($company == false) continue;


            $name = $this->users->getName($v['id']);
            $email = $this->users->getTableValue('email', $v['id']);

            $position = $this->users->getPosition($v['id']);

            $positionDisplay = $this->positions->getName($position);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr onclick=\"search.userClick({$v['id']});\">";

        echo "<td>{$name}</td>";
        echo "<td>{$v['id']}</td>";
        echo "<td>{$email}</td>";
        echo "<td>{$positionDisplay}</td>";
        // echo "<td><a href='/user/edit/{$v['id']}' class='pull-right'><i class='fa fa-eye'></i></a></td>";

        echo "</tr>" . PHP_EOL;

        $totalUserCnt++;
    }
            echo "</tbody></table>";

    if (empty($totalUserCnt)) echo $userAlert;
}


#print_r($results);
?>

    <input type='hidden' name='totalUserCnt' id='totalUserCnt' value='<?=$totalUserCnt?>'>

    </div> <!-- #tabUsers -->


    <div id="tabMsg" class='tab-pane'>

<?php

$totalMsgCnt = 0;

$msgAlert = $this->alerts->info('No messages found!');

if (empty($msgResults['matches']))
{
    echo $msgAlert;
}
else
{
    echo "<table id='msgSearchTbl' class='table table-hover table-bordered'>
        <thead>
            <tr>
                <th>From</th>
                <th>Date</th>
                <th>Subject</th>
            </tr>
        </thead>
        <tbody>";

    foreach ($msgResults['matches'] as $k => $v)
    {

        try
        {

            $assigned = $this->msgs->checkUserAssignedToMessage($this->session->userdata('userid'), $v['id']);

            // skips message if not assigned
            if ($assigned === false) continue;


            $subject = $this->msgs->getTableValue('subject', $v['id']);
            $from = $this->msgs->getTableValue('from', $v['id']);
            $datestamp = $this->msgs->getTableValue('datestamp', $v['id']);

            $fromDisplay = $this->users->getName($from);

            $date = $this->users->convertTimezone($this->session->userdata('userid'), $datestamp, "m/d/Y g:i A");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>";

        echo "<td>{$fromDisplay}</td>";
        echo "<td>{$date}</td>";
        echo "<td>{$subject}</td>";

        echo "</tr>" . PHP_EOL;
    
        $totalMsgCnt++;
    }

    echo "</tbody></table>";


    if (empty($totalMsgCnt)) echo $msgAlert;

    
}
?>

    <input type='hidden' name='totalMsgCnt' id='totalMsgCnt' value='<?=$totalMsgCnt?>'>

    </div> <!-- #tabMsg -->

    <div id="tabDoc" class='tab-pane'>

<div id='doc-container'>
<?php

$totalDocCnt = 0;

$docAlert = $this->alerts->info('No documents found!');



if (empty($docResults['matches']))
{
    echo $docAlert;
}
else
{


    echo "<div class='row'>\n";
    echo "<ol id='fileList'>\n";

    foreach ($docResults['matches'] as $k => $v)
    {

        try
        {
            $company = $this->documents->getTableValue('company', $v['id']);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        // only checks documents that are assigned to current company
        if ((int) $company == (int) $this->session->userdata('company'))
        {

            try
            {
                $name = $this->documents->getTableValue('title', $v['id']);
                $type = $this->documents->getTableValue('type', $v['id']);
                $active = $this->documents->getTableValue('active', $v['id']);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

            $name = (strlen($name) > 25) ? substr($name, 0, 22) . '...' : $name;

            if ($type == 3) $img = 'movie-icon.png'; // video
            elseif ($type == 11) $img = 'pdf-icon.png'; // pdf
            else $img = 'doc_blue_trans.png';

            $docType = $type;

            $class = ($active == 1) ? null : ' hidden-doc';

            echo "\t<li class='ui-state-default{$class}' value='{$v['id']}' itemType='2' documentType='{$docType}' id='doc_{$v['id']}'><img src='/public/images/{$img}'><div class='docName'>{$name}</div></li>\n";

            $totalDocCnt++;
        }
    }

    echo "</ol>\n";
    
    echo "</div>\n"; // .row



}
?>
    </div> <!-- #doc-container -->


    <input type='hidden' name='totalDocCnt' id='totalDocCnt' value='<?=$totalDocCnt?>'>

    </div> <!-- #tabDoc -->


    <div id="tabForms" class='tab-pane'>
<?php
$totalFormCnt = 0;

if (empty($formResults))
{
    echo $this->alerts->info("No forms were found based upon your search!");
}
else
{

    echo "<div id='forms-container'>";

    echo "<div class='row'>" . PHP_EOL;
    echo "<ol id='fileList'>" . PHP_EOL;


    foreach ($formResults['matches'] as $k => $v)
    {
        $active = false;

        try
        {
            $company = $this->formbuilder->getFormCompany($v['id']);
        
            if (empty($company) || $company == $this->session->userdata('company'))
            {
                // do nothing
            }
            else
            {
                // article is not 0 or set for this company
                continue;
            }

            $name = $this->formbuilder->getFormTitle($v['id']);
            $active = $this->formbuilder->active($v['id']);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $name = (strlen($name) > 25) ? substr($name, 0, 22) . '...' : $name;

        $class = ($active == true) ? null : ' hidden-doc';

        echo "\t<li class='ui-state-default{$class} form' value='{$r->id}' itemType='2' id='form_{$r->id}' onclick=\"search.viewForm({$v['id']});\"><i class='fa fa-file drag-icon'></i><div class='docName'>{$name}</div></li>\n";

        $totalFormCnt++;

    }

    echo "</ol>\n";

    echo "</div> <!-- .row -->" . PHP_EOL;
    echo "</div> <!-- #bkArticle-container -->" . PHP_EOL;

}

?>

    <input type='hidden' name='totalFormCnt' id='totalFormCnt' value='<?=$totalFormCnt?>'>

    </div> <!-- #tabForms -->


    <div id="tabVideos" class='tab-pane'>
<?php
$totalVideoCnt = 0;
// print_r($videoResults);
if (empty($videoResults))
{
    echo $this->alerts->info("No videos were found based upon your search!");
}
else
{

    echo "<div id='videos-container'>";

    echo "<div class='row'>" . PHP_EOL;
    echo "<ol id='fileList'>" . PHP_EOL;


    foreach ($videoResults['matches'] as $k => $v)
    {
        $active = false;

        try
        {
            $company = $this->video->getCompany($v['id']);
        
            if (empty($company) || $company == $this->session->userdata('company'))
            {
                // do nothing
            }
            else
            {
                // article is not 0 or set for this company
                continue;
            }

            $info = $this->video->getInfo($v['id']);

            $name = $info->title;
            $active = $info->active;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $name = (strlen($name) > 25) ? substr($name, 0, 22) . '...' : $name;

        $class = ($active == true) ? null : ' hidden-doc';
        
        $icon = "<i class='fa fa-film drag-icon'></i>";


        if (!empty($info->youtubeUrl) && !empty($info->thumbnail)) $icon = "<img src='{$info->thumbnail}' height='50px'>";


        echo "\t<li class='ui-state-default{$class} form' value='{$r->id}' itemType='2' id='form_{$r->id}' onclick=\"search.viewVideo({$v['id']});\">{$icon}<div class='docName'>{$name}</div></li>\n";

        $totalVideoCnt++;

    }

    echo "</ol>\n";

    echo "</div> <!-- .row -->" . PHP_EOL;
    echo "</div> <!-- #videos-container -->" . PHP_EOL;

}

?>

        <input type='hidden' name='totalVideoCnt' id='totalVideoCnt' value='<?=$totalVideoCnt?>'>
    </div> <!-- #tabVideos -->
</div>


</div> <!-- .span9 -->
</div> <!-- .row-fluid -->
