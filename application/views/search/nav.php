<?php if(!defined('BASEPATH')) die('Direct access not allowed');

$a[$nav] = 'active';

?>

<!-- <h2><i class='fa fa-search'></i> Search</h2> -->

    <ul class='list-group'>
        <li class='list-group-item active'><a href='#tabKBArticles' data-toggle='tab'><i class='menu-icon fa fa-user'></i> Knowledge Base <span class='badge pull-right' id='KBCntDisplay'>0</span></a></li>
        <li class='list-group-item'><a href='#tabCRMContacts' data-toggle='tab'><i class='menu-icon fa fa-rocket'></i> CRM Contacts <span class='badge pull-right' id='CRMCntDisplay'>0</span></a></li>
        <li class='list-group-item'><a href='#tabUsers' data-toggle='tab'><i class='menu-icon fa fa-user'></i> Users <span class='badge pull-right' id='userCntDisplay'>0</span></a></li>
        <li class='list-group-item'><a href='#tabMsg' data-toggle='tab'><i class='menu-icon fa fa-envelope'></i> Messages <span class='badge pull-right' id='msgCntDisplay'>0</span></a></li>
        <li class='list-group-item'><a href='#tabDoc' data-toggle='tab'><i class='menu-icon fa fa-file'></i> Documents <span class='badge pull-right' id='docCntDisplay'>0</span></a></li>
        <li class='list-group-item'><a href='#tabForms' data-toggle='tab'><i class='menu-icon fa fa-code'></i> Forms <span class='badge pull-right' id='formCntDisplay'>0</span></a></li>
        <li class='list-group-item'><a href='#tabVideos' data-toggle='tab'><i class='menu-icon fa fa-film'></i> Videos <span class='badge pull-right' id='videoCntDisplay'>0</span></a></li>
        <!-- <li><a href='#'><i class='menu-icon icon-group'></i>Applicants <b class='badge pull-right'>0</b></a></li> -->


    </ul>


