<h1><i class="icon icon-ticket"></i> Submit A Ticket</h1>
<div class="row">

<div class="col-lg-12">
    <?php include_once 'nav.php'; ?>
<!-- <form class="form-horizontal"> -->
<?php
$company = $this->session->userdata('company');
    $attr = array
        (
            'name' => 'createForm',
            'id' => 'createForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>
    <input type='hidden' id='company' name='company' value='<?=$company?>'>
<!-- <legend>Submit Helpdesk Ticket</legend> -->

    <?php if (!empty($locations)) : ?>

    <div class="form-group">
        <label class="col-lg-3 control-label" for="location">Select Location</label>
        <div class="col-lg-9">
            <select name="location" id="location">
                <option value=''></option>
            <?php
                foreach ($locations as $r)
                {
                    echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                }
            ?>
            </select>
        </div>
    </div> <!-- /.control-group -->

    <?php endif; ?>

    <?php if (!empty($departments)) : ?>

            <div class="form-group">
            <label class="col-lg-3 control-label" for='department'>Department</label>
                <div class="col-lg-9">
                    <select class="form-control" name='department' id='department'>
                        <option value=''></option>
                    <?php
                        foreach ($departments as $r)
                        {
                            // $sel = ($r->id == $id) ? 'selected' : null;

                            echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                        }
                    ?>
                    </select>
                </div>
            </div> <!-- /.control-group -->

    <?php endif; ?>

            <div id='departmentTechsDisplay' name='departmentTechsDisplay' class='departmentTechsDisplay'></div>

    <!-- <div class="control-group"> -->
    <!--     <label class="control-label" for="inputProblemType">Problem Type</label> -->
    <!--     <div class="controls"> -->
    <!--         <select id="inputProblemType"> -->
    <!--             <option>Problem 1</option> -->
    <!--             <option>Problem 2</option> -->
    <!--         </select> -->
    <!--     </div> -->
    <!-- </div> --> <!-- /.control-group -->


    <div class="form-group">
        <label class="col-lg-3 control-label" for="datepicker">Challenge experienced on:</label>
        <div class="col-lg-9">
            <input class="form-control" type="text" name="datepicker" id="datepicker">
        </div>
    </div> <!-- /.control-group -->

    <div class="form-group">
        <label class="col-lg-3 control-label" for="priority">Priority</label>
        <div class="col-lg-9">
            <select class="form-control" name="priority" id="priority">
                <option value="6">New Ticket</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label" for="status">Status</label>
        <div class="col-lg-9">
            <select class="form-control" name="status" id="status" value='4'>
                <option value="4">New Ticket</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label" for="subject">Subject</label>
        <div class="col-lg-9">
            <input class="form-control" type="text" name="subject" id="subject" placeholder="Subject">
        </div>
    </div> <!-- /.control-group -->

    <div class="form-group">
        <label class="col-lg-3 control-label" for="issue">Explain your difficulty</label>
        <div class="col-lg-9">
            <textarea class="form-control" name="issue" id="issue" rows="5" placeholder="Explain your difficulty in detail here..."></textarea>
        </div>
    </div> <!-- /.control-group -->

    <div class="form-group">
        <label class="col-lg-3 control-label" for="submitBtn"></label>
        <div class="col-lg-9">
            <button type="button" class="btn btn-primary" id="submitBtn"><i class="menu-icon icon-ok"></i> Submit</button>
        </div> <!--  /.col-lg-7 -->
    </div> <!-- /.form-group -->

</form>
</div> <!-- /.span9 -->
</div> <!-- /.row-fluid -->
