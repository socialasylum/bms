<h1><i class="icon icon-tasks"></i> My Tech Tickets</h1>
<div class="row">
<div class="col-lg-12">
    <?php include_once 'nav.php'; ?>
        <table class='table table-hover table-striped table-bordered' id='myTechTicketsTable'>
            <thead>
                <tr>
                    <th>Ticket ID</th>
                    <th>Company</th>
                    <th>Date Reported</th>
                    <th>Submitted By</th>
                    <th>Updated</th>
                    <th>Status</th>
                    <th>Problem Description</th>
                </tr>
            </thead>
            <tbody>
<?php
    foreach ($tickets as $r)
    {
        $date = null;
        try
        {
            $info = $this->help->getTicketInfo($r->ticketid);
            $compName = $this->companies->getCompanyName($info->company);
            $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->issueDate, "m/d/Y");
            $reported = $this->users->convertTimezone($this->session->userdata('userid'), $info->datestamp, "m/d/Y");
            $status = $this->functions->codeDisplay("11", "{$info->status}");
            $submittedBy = $this->users->getName($info->userid);
            $updated = $this->help->getUpdateTime($r->ticketid);
            $updated = $updated->datestamp;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
        echo"<tr onclick=\"window.location='/helpdesk/editTicket/{$r->ticketid}';\">\n";
            echo"<td>{$r->ticketid}</td>\n";
            echo"<td>{$compName}</td>\n";
            echo"<td>{$reported}</td>\n";
            echo"<td>{$submittedBy}</td>\n";
            echo"<td>{$updated}</td>\n";
            echo"<td>{$status}</td>\n";
            echo"<td>{$info->issue}</td>\n";
        echo"</tr>\n";

    }
?>
            </tbody>
        </table>
</div> <!-- /.span9 -->
</div> <!-- /.row-fluid -->
