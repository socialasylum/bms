<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ($colSize == 2) 
{
    $labelCol = 'col-lg-2';
    $divCol = 'col-lg-10';
}
elseif ($colSize == 3)
{
    $labelCol = 'col-lg-3';
    $divCol = 'col-lg-9';
}

if (!empty($departmentTechs))
{
    echo "<div class='form-group'>";
    echo "<label class='{$labelCol} control-label' for='departmentTechs'>Techs:</label>";
    echo "<div class='{$divCol}'>";
    echo "<input type='hidden' name='departmentTechs' id='departmentTechs' value=''>";
    echo "<select multiple class='{$divCol} form-control' name='departmentTechs' id='departmentTechs'>\n";

    echo "\t<option value=''><b>Random Tech</b></option>\n";

    foreach ($departmentTechs as $r)
    {
        $techName = $this->users->getName($r->id);
        echo "\t<option value='{$r->id}'>{$techName}</option>\n";
    }
    echo "</select>\n";
}
else
{
    echo $this->alerts->info('No Techs Available For This Department');
}


