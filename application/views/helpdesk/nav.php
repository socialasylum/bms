<?php if(!defined('BASEPATH')) die('Direct access not allowed');

$a[$nav] = 'active';

?>

<?php 
if ($myTicketCount > 10) $mtc = 'label-danger';
elseif ($myTicketCount <= 5) $mtc = 'label-success';
else $mtc = 'label-warning';
?>
<?php 
if ($depTicketCount > 20) $dtc = 'label-danger';
elseif ($depTicketCount <= 10) $dtc = 'label-success';
else $dtc = 'label-warning';
?>
<?php 
if ($myTechTicketCount > 10) $mttc = 'label-danger';
elseif ($myTechTicketCount <= 5) $mttc = 'label-success';
else $mttc = 'label-warning';
?>
 <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

  <ul class='nav navbar-nav'>
        <li class='<?=$a['index']?>'><a href='/helpdesk'><i class='menu-icon icon-ticket'></i>    Submit Helpdesk Ticket</a></li>
        <li class='<?=$a['myTickets']?>'><a href='/helpdesk/myTickets'><i class='menu-icon icon-bar-chart'></i>     My Tickets<span class='label ticket-number <?=$mtc?> pull-right'><?=$myTicketCount?></span></a></li>
        <li class='<?=$a['departmentTickets']?>'><a href='/helpdesk/depTickets'><i class='menu-icon icon-bar-chart'></i>     Department Tickets<span class='label ticket-number <?=$dtc?> pull-right'><?=$depTicketCount?></span></a></li>
        <li class='<?=$a['myTechTickets']?>'><a href='/helpdesk/myTechTickets'><i class='menu-icon icon-tasks'></i>     My Tech Tickets<span class='label ticket-number <?=$mttc?> pull-right'><?=$myTechTicketCount?></span></a></li>
    </ul>


 <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

 
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>
