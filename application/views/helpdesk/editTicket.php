<h1>Edit Ticket</h1>
<div class="row">
<div class="col-lg-12">
    <?php include_once 'nav.php'; ?>

<?php
    $attr = array
        (
            'name' => 'editTicket',
            'id' => 'editTicket',
            'class' => 'form-horizontal'
        );

echo form_open('/help/addNote', $attr);
?>
    <input type="hidden" id="id" name="id" value="<?=$id?>">
<?php if(strtotime($tickets->issueDate) > (strtotime(DATESTAMP) - (86400*7)))
                {
                    $alert = 'alert-success';
                }
                elseif (strtotime($tickets->issueDate) > (strtotime(DATESTAMP) - (86400*14)) AND (strtotime($tickets->issueDate) < (strtotime(DATESTAMP) - (86400*7))))
                {
                    $alert = 'alert-warning';
                }
                else
                {
                    $alert = 'alert-danger';
                }
?>
<div class='alert <?=$alert?>'>
        <dl class="dl-horizontal">
            <dt>Challenge experienced:</dt>
            <dd><?=$this->users->convertTimezone($this->session->userdata('userid'), $tickets->issueDate, "m/d/Y");?></dd>
        </dl>
    </div>
<dl class='dl-horizontal'>
    <dt>Submitted By</dt>
    <dd><?=$this->users->getName($tickets->userid)?></dd>
</dl>

    <?php if (!empty($locations)) : ?>

        <dl class="dl-horizontal">
            <dt>Location:</dt>
            <dd></dd>
        </dl>

    <?php endif; ?>

    <?php if (!empty($departments)) : ?>
<?php
    if($techTicket == True)
    {
        echo "<div class='form-group'>";
            echo "<label class='col-lg-2 control-label' for='department'>Department:</label>";
            echo "<div class='col-lg-10'>";
                echo "<select id='department' name='department' class='form-control'>";
            foreach($departments as $r)
                {
                        $sel = ($r->id == $tickets->department) ? 'selected' : null;
                               echo "<option {$sel} value='{$r->id}'>{$r->name}</option>";
                }
                            echo "</select>";
                        echo "</div>";
                    echo "</div>";
    }
    else
    {
        echo "<dl class='dl-horizontal'>";
        echo "<dt>Department:</dt>";
            echo "<dd>";
                if(!empty($tickets->department)) :
                    echo $this->departments->getName($tickets->department);
                endif;

        echo "</dd>";
        echo "</dl>";
    }
    endif;
?>



    <?php endif; ?>

 
    <!-- <div class="control-group"> -->
    <!--     <label class="control-label" for="inputProblemType">Problem Type</label> -->
    <!--     <div class="controls"> -->
    <!--         <select id="inputProblemType"> -->
    <!--             <option>Problem 1</option> -->
    <!--             <option>Problem 2</option> -->
    <!--         </select> -->
    <!--     </div> -->
    <!-- </div> --> <!-- /.control-group -->

<?php 
    if($techTicket == True) :
        echo "<div class='form-group'>";
            echo "<label class='col-lg-2 control-label' for='status'>Status:</label>";
            echo "<div class='col-lg-10'>";
                echo "<select id='status' name='status' class='form-control'>";
     
            foreach($status as $r)
                {
                    $codeDisplay = $this->functions->codeDisplay('11', $r->code);
                        $sel = ($r->code == $tickets->status) ? 'selected' : null;
                               echo "<option {$sel} value='{$r->code}'>{$codeDisplay}</option>";
                }
                            echo "</select>";
                        echo "</div>";
                    echo "</div>";

    else :
        echo "<dl class='dl-horizontal'>";
        echo "<dt>Status</dt>";
            echo "<dd>";
                if(!empty($tickets->status)) :
                echo $this->functions->codeDisplay('11', $tickets->status);

                else :
                echo $this->functions->codeDisplay("11", "1");
                endif;

                echo "</dd>";
        echo "</dl>";
    endif;
?>
    
<?php if($techTicket == True) :
    echo "<div id='departmentTechsDisplay' name='departmentTechsDisplay' class='departmentTechsDisplay'>";
        echo "<div class='form-group'>";
            echo "<label class='col-lg-2 control-label' for='depTechs'>Techs:</label>";
            echo "<div class='col-lg-10'>";
                echo "<select multiple id='depTechs' name='depTechs' class='form-control'>";

            foreach($depTechs as $r)
            {
                $techName = $this->users->getName($r->id);
                foreach ($tech as $t)
                    {
                        if($r->id == $t->userid) :
                        {
                            $sel = "selected";
                        }
                        else :
                        {
                            null;
                        }
                        endif;
                    }
                echo "<option {$sel} value='{$r->id}'>$techName</option>";
                $sel = null;
            }
                echo "</select>";
            echo "</div>";
        echo "</div>";
    echo "</div>";
    else :

        echo "<dl class='dl-horizontal'>";
            echo "<dt>Tech Assigned:</dt>";
            echo "<dd>";

        foreach ($tech as $r) 
        {
            try
            {
                $techName = $this->users->getName($r->userid);
                echo "$techName <br>";
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

        }

    endif;
?>
            </dd>
        </dl>
        <dl class="dl-horizontal">
<?php 
    if($techTicket == True) :
        echo "<div class='form-group'>";
            echo "<label class='col-lg-2 control-label' for='priority'>Priority:</label>";
            echo "<div class='col-lg-10'>";
                echo "<select id='priority' name='priority' class='form-control'>";
     
            foreach($priorities as $r)
                {
                    $codeDisplay = $this->functions->codeDisplay('10', $r->code);
                        $sel = ($r->code == $tickets->priority) ? 'selected' : null;
                               echo "<option {$sel} value='{$r->code}'>{$codeDisplay}</option>";
                }
                echo "</select>";
            echo "</div>";
        echo "</div>";

    else :
        echo "<dt>Priority</dt>";
            echo "<dd>";
                if(!empty($tickets->priority)) :
                echo $this->functions->codeDisplay('10', $tickets->priority);

                else :
                echo $this->functions->codeDisplay("10", "6");
                endif;

            echo "</dd>";
    endif;
?>
        </dl>

        <dl class="dl-horizontal">
            <dt>Subject:</dt>
            <dd><?=$tickets->subject?></dd>
        </dl>

        <dl class="dl-horizontal">
            <dt>Your challenge:</dt>
            <dd><?=nl2br($tickets->issue)?></dd>
        </dl>

        <dl class="dl-horizontal">
<?php foreach ($notes as $r)
        {
            try
            {
                $name = $this->users->getName($r->userid);
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }
            echo "<dt>{$name} <br> on <br> {$r->datestamp}</dt>";
            echo "<dd><div class='well'>{$r->note}</div></dd>";
        }
?>

        </dl>

    <div class="form-group">
        <label class="col-lg-2 control-label" for="issue">Add Note:</label>
        <div class="col-lg-10">
            <textarea class="form-control" name="note" id="note" rows="5" placeholder="Anything you would like to add to help your tech can be added here:"></textarea>
        </div>
    </div> <!-- /.control-group -->


    <div class="form-actions">
        <button type="button" class="btn btn-primary" id="updateBtn" name="updateBtn">Update</button>
    </div> <!-- /.form-actions -->
</form>

</div> <!-- /.span9 -->
</div> <!-- /.row-fluid -->
