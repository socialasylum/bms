<h1><i class="icon-bar-chart"></i> My Tickets</h1>
<div class="row">
<!-- <div class="col-lg-3"> -->
<div class="col-lg-12">
    <?php include_once 'nav.php'; ?>
<!-- </div>  /.span3 -->
    <!-- <div class="panel"> -->
        <!-- <div class="panel-heading"><h3 class="panel-title">My Tickets</h3></div> -->
        <table class='table table-hover table-striped table-bordered' id='myTicketsTable'>
            <thead>
                <tr>
                    <th>Ticket ID</th>
                    <th>Date of Issue</th>
                    <th>Date Reported</th>
                    <th>Updated</th>
                    <th>Status</th>
                    <th>Tech Assigned</th>
                    <th>Problem Description</th>
                </tr>
            </thead>
            <tbody>
<?php
    foreach ($tickets as $r)
    {
        try
        {
            $tech = $this->help->getTech($r->id);
            $dateSubmitted = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "m/d/Y");
            $issueDate = $this->users->convertTimezone($this->session->userdata('userid'), $r->issueDate, "m/d/Y");
            $status = $this->functions->codeDisplay("11", "{$r->status}");
            $updated = $this->help->getUpdateTime($r->id);
            $updated = $updated->datestamp;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
        echo"<tr onclick=\"window.location='/helpdesk/editTicket/{$r->id}';\">\n";
            echo"<td>{$r->id}</td>\n";
            echo"<td>{$issueDate}</td>\n";
            echo"<td>{$dateSubmitted}</td>\n";
            echo"<td>{$updated}</td>\n";
            echo"<td>{$status}</td>\n";
            echo"<td>\n";
            foreach ($tech as $t)
            {
                try
                {
                    echo "{$this->users->getName($t->userid)} </br>";
                }
                catch(Exception $e)
                {
                    $this->functions->sendStackTrace($e);
                }
            }
            echo"</td>\n";
            echo"<td>{$r->issue}</td>\n";
        echo"</tr>\n";
    }
?>
            </tbody>
        </table>
        </div> <!-- /.module-body -->
    </div> <!-- /.module -->
</div> <!-- /.span9 -->
</div> <!-- /.row-fluid -->
