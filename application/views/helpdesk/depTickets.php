<h1><i class="icon icon-bar-chart"></i> Department Tickets</h1>
<div class="row">
<div class="col-lg-12">
    <?php include_once 'nav.php'; ?>
        <table class='table table-hover table-striped table-bordered' id='depTicketsTable'>
            <thead>
                <tr>
                    <th>Ticket ID</th>
                    <th>Date of Issue</th>
                    <th>Date Reported</th>
                    <th>Updated</th>
                    <th>Status</th>
                    <th>Tech Assigned</th>
                    <th>Problem Description</th>
                </tr>
            </thead>
            <tbody>
<?php

    foreach ($tickets as $r)
    {
        try
        {
            $info = $this->help->getTicketInfo($r->id);
            $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->issueDate, "m/d/Y");
            $reported = $this->users->convertTimezone($this->session->userdata('userid'), $info->datestamp, "m/d/Y");
            $tech = $this->help->getTech($r->id);
            $status = $this->functions->codeDisplay("11", "{$info->status}");
            $updated = $this->help->getUpdateTime($r->id);
            $updated = $updated->datestamp;
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }
        echo "<tr onclick=\"window.location='/helpdesk/editTicket/{$r->id}';\">\n";
            echo "<td>{$r->id}</td>\n";
            echo "<td>{$date}</td>\n";
            echo "<td>{$reported}</td>\n";
            echo "<td>{$updated}</td>\n";
            echo "<td>{$status}</td>\n";
            echo "<td>\n";
            foreach ($tech as $t)
            {
                try
                {
                    echo "{$this->users->getName($t->userid)} </br>";
                }
                catch(Exception $e)
                {
                    $this->functions->sendStackTrace($e);
                }
            }
            echo "</td>\n";

            echo "<td>{$info->issue}</td>\n";
        echo "</tr>\n";
    }
?>
            </tbody>
        </table>
    </div> <!-- /.module -->
</div> <!-- /.span9 -->
</div> <!-- /.row-fluid -->
