<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
/*
<div class='panel panel-default'>
    <div class='panel-heading'>Welcome</div>

    <div class='panel-body'>
 */
?>

<div id='main-carousel' class='carousel slide'>
    <ol class='carousel-indicators'>
        <li data-target='#main-carousel' data-slide-to='0' class='active'></li>
        <li data-target='#main-carousel' data-slide-to='1'></li>
        <li data-target='#main-carousel' data-slide-to='2'></li>
        <li data-target='#main-carousel' data-slide-to='3'></li>
    </ol>

    <!-- wrapper for slides -->
    <div class='carousel-inner'>

        <div class='item active'>
            <img src='/public/images/businessStartBanner3.png' alt=''>
        </div> <!-- .item .active -->

        <div class='item'>
            <img src='/public/images/dashboard_crop.png' alt=''>
                <div class='carousel-caption'>
                    <h2><i class='fa fa-dashboard'></i> Custom Dashboard</h2>
                    <p>Portlets allow users to create a customized dashboard to optimize their performance.</p>
                </div>
        </div> <!-- .item -->

        <div class='item'>
            <img src='/public/images/crmcrop.png' alt=''>
                <div class='carousel-caption'>
                    <h2><i class='fa fa-rocket'></i> CRM</h2>
                    <p>Built in CRM that interfaces directly with any new or existing website!</p>
                </div>
        </div> <!-- .item -->



        <div class='item'>
            <img src='/public/images/datasets.png' alt=''>
                <div class='carousel-caption'>
                    <h2><i class='fa fa-bar-chart-o'></i> Reporting</h2>
                    <p>Create custom datasets and reports in order to see your business data how you need it.</p>
                </div>
        </div> <!-- .item -->
    </div> <!-- carousel-inner -->


    <!-- controls -->
    <a class='left carousel-control' href='#main-carousel' data-slide='prev'>
        </span class='icon-prev'></span>
    </a>
    <a class='right carousel-control' href='#main-carousel' data-slide='next'>
        </span class='icon-next'></span>
    </a>

</div> <!-- .carousel -->






      <div class="jumbotron">
        <h2>Business management made easy!</h2>
        <p class="lead">Don't re-invent the wheel, we made the systems so you don't have to!</p>
        <p>CGI Solutions Business Management System allows business professionals to optimize their performance, and allow them to get more done in less time. Rather than hiring more employee's, utilizng systems can be a great way to assist your current work force.</p>


        <a class="btn btn-large btn-success" href="/pricing">Sign up today</a>
      </div>
      <hr>

      <div class="row marketing">
        <div class="col-lg-6">
          <h4>Application & Onboarding Automation</h4>
          <p>We have several systems to streamline employee inprocessing. From accepting applications to employee policy compliance, we have the tools to assist you company.</p>

          <h4>Location Management</h4>
          <p>Systems available to help you manage multiple locations from 2 - 20,000. Keep track of all important tracking info in one place.</p>

          <h4>Help Desk Ticketing System &amp; 24/7 Tech Support</h4>
          <p>We have a full help desk ticketing system that can be used plus the cost of the 24/7 tech services that we offer.</p>
        </div>

        <div class="col-lg-6">
          <h4>Reporting Services</h4>
          <p>We have a full reporting service that is plug and play for basic reports. More custom, indepth reports can be purchased on an "as needed" basis.</p>

          <h4>Employee/Location Messaging &amp; Announcements</h4>
          <p>We can impliment an employee messaging &amp;/or location announcements for easier location communication.</p>

          <h4>Just Some Extras</h4>
          <p>No matter what Modules you decide to use, the following will be included for free:</p>
            <ul>
                <li>Hierarchy Controllability</li>
                <li>LDAP Integration</li>
                <li>Built in Text Editor (CMS)</li>
            </ul>
        </div>
      </div>

    <div class='clearfix'></div>

    </div> <!-- .panel-body -->

</div> <!-- .panel -->
