<div class="panel panel-default">
    <div class="panel-heading">

        <h3 class='panel-title'><i class='fa fa-usd'></i> Payment Receipt</h3>

    </div> <!-- /.panel-heading -->
    <div class='panel-body'>

<div class='row'>
    <div class='col-lg-4 col-m-4 col-s-4 col-xs-4'>

<?php
    ?>

    </div> <!-- /. col4 -->
    <div class='col-lg-8 col-m-8 col-s-8 col-xs-8'>
    </div> <!-- /. col8 -->
</div> <!-- /.row -->
<div class='row'>
    <div class='col-lg-4 col-m-4 col-s-4 col-xs-4'>
        <legend>Sold To:</legend>
<?php

    echo "<p><strong>{$customer['name']}</strong></p>";
    if (!empty($customer['address'])) echo "<p>{$customer['address']}</p>";
    if (!empty($customer['address2'])) echo "<p>{$customer['address2']}</p>";

    echo "<p>";
    if (!empty($customer['city'])) echo "{$customer['city']}, ";
    if (!empty($customer['state'])) echo "{$customer['state']} ";
    if (!empty($customer['postal'])) echo $customer['postal'];
    echo "</p>";
?>
    </div> <!-- /.col4 -->
    <div class='col-lg-4 col-m-4 col-s-4 col-xs-4'>
        <legend>Sold By:</legend>
<?php
    echo "<p><strong>{$company['name']}</strong></p>";
    echo "<p>{$company['address']}</p>";
    if (!empty($company['address2'])) echo "<p>{$company['address2']}</p>";

    echo "<p>";
    if (!empty($company['city'])) echo "{$company['city']}, ";
    if (!empty($company['state'])) echo "{$company['state']} ";
    if (!empty($company['postal'])) echo $company['postal'];
    echo "</p>";

    if (!empty($company['phone'])) echo $company['phone'];

?>
    </div> <!-- /.col4 -->
    <div class='col-lg-4 col-m-4 col-s-4 col-xs-4'>
        <legend>Transaction Info:</legend>
<?php
    try
    {

        $tranAttr = $tranInfo->_attributes;

        // print_r($tranAttr);

        $tranid = $tranAttr['id'];
        $tranUpdated = $tranAttr['updatedAt'];
        $tranStatus = $tranAttr['status'];
        $subscription = $tranAttr['subscription'];

        $billEndDate = $subscription['billingPeriodEndDate'];
        $billEndDate = $billEndDate->format('M-d-Y H:m:s');
        $billEndDatef = substr($billEndDate, 0, 11);

        $billStartDate = $subscription['billingPeriodStartDate'];
        $billStartDate = $billStartDate->format('M-d-Y H:m:s');
        $billStartDatef = substr($billStartDate, 0, 11);

        $date = $tranUpdated->format('M-d-Y H:i:s');
        $datef = substr($date, 0, 11);
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

    echo "<p>Transaction ID: <strong>{$tranid}</strong></p>";
    echo "<p>Transaction Date: <strong>{$datef}</strong></p>";
    echo "<p>Transaction Status: <strong>{$tranStatus}</strong></p>";
    echo "<p>Billing Cycle: <strong>{$billStartDatef} - {$billEndDatef}</strong></p>";

?>
    </div>
</div> <!-- /.row -->

<?php
echo <<< EOS
    <table id='itemTbl' class='table'>
        <thead>
            <tr>
                <th>Plan ID</th>
                <th>Description</th>
                <th>Unit Price</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
EOS;

$plans = $this->payment->getPlans();

if (!empty($plans))
{

    foreach ($plans as $p)
    {
        $finalPlans = $p->_attributes;

            // print_r("<br>id: " . $finalPlans['id']);
            // print_r("<br>planId: " . $tranAttr['planId']);
        if ($finalPlans['id'] == $tranAttr['planId'])
        {
            echo "<tr>";

            echo "<td>{$finalPlans['id']}</td>";
            echo "<td>{$finalPlans['description']}</td>";
            echo "<td>$ {$finalPlans['price']}</td>";
            echo "<td>$ {$finalPlans['price']}</td>";

            echo "<tr>";
        }

    }

}

$addOns = $tranAttr['addOns'];

if (!empty($addOns))
{
    foreach ($addOns as $r)
    {
        $finalAddOns = $r->_attributes;

        echo "<tr>";

        echo "<td>{$finalAddOns['id']}</td>";
        echo "<td>";

        if (!empty($finalAddOns['name'])) echo $finalAddOns['name'];
        else  echo "Admin Fee";

        echo "</td>";
        echo "<td>$ {$finalAddOns['amount']}</td>";
        echo "<td>$ {$finalAddOns['amount']}</td>";

        echo "</tr>";

    }
}

if (!empty($discount))
{
    foreach ($discount as $r)
    {
        $finalDiscount = $r->_attributes;

        echo "<tr>";

        echo "<td>{$finalDiscount['id']}</td>";
        echo "<td>{$finalDiscount['description']}</td>";
        echo "<td>{$finalDiscount['amount']}</td>";
        echo "<td></td>";

        echo "<tr>";

    }
}

?>

        </tbody> 
    </table>
    </div> <!-- /.panel-body -->
</div> <!-- /.panel-default -->
