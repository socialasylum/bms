<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<?php
$attr = array
(
    'name' => 'editPermForm',
    'id' => 'editPermForm',
    'class' => 'form-horizontal'
);

echo form_open('#', $attr);

if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
?>

    <input type='hidden' name='module' id='module' value='<?=$module?>'>
    <input type='hidden' name='admin' id='admin' value='0'>


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='bit'>Bit</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' name='bit' id='bit' class='form-control' value="<?=$bit?>" placeholder='1'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='title'>Title</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='title' id='title' value="<?=$info->title?>" placeholder='Permission Title'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='description'>Description</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <textarea class='form-control' rows='5' name='description' id='description' rows='5'><?=$info->description?></textarea>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


<?php if ($this->session->userdata('admin') === true) : ?>
    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='admin'>Admin</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
<?php
    $adminChecked = ($info->admin == 1) ? "checked='checked'" : null;
?>

		<div class='checkbox'>
	        <label>
		        <input type='checkbox' name='admin' id='admin' value='1' <?=$adminChecked?>> Administrator Permission
	        </label>
		</div>
		
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


<?php endif; ?>

</form>
