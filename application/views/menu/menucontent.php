<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>

<input type='hidden' id='company' value='<?=$this->session->userdata('company')?>'>

<h1><i class='fa fa-list-ul'></i> Menu</h1>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><a href="javascript:void(0);" id='menuHeaderBtn' value='<?=$id?>'><i class='fa fa-edit'></i> Create Menu Header</a></li>
        <!-- <li><a href="javascript:void(0);" id='menuSettingsBtn'><i class='fa fa-cog'></i> Settings</a></li> -->
    </ul>



<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<div class='row'>
    
    <div class='col-lg-8'>

<!-- <h3><?=$companyName?></h3> -->

<div id='headers-container'>
<?php
if (empty($headers)) : echo $this->alerts->info("No menu's have been created for {$companyName}");
else :

    echo "<ul id='menu-li'>" . PHP_EOL;

    foreach ($headers as $r)
    {

        echo "<li id='{$r->id}' value='{$r->id}'>" . PHP_EOL;

        try
        {
            $menu = $this->menu->getMenu($id, false, $r->id);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }


		echo "<div class='panel panel-danger menu-panel'>" . PHP_EOL;
		
        //echo "<div class='row-fluid menu-header'>" . PHP_EOL;

        //echo "<div class='col-lg-12 well'>" . PHP_EOL;

        echo "<div class='panel-heading menu-header'><h3 class='panel-title'>{$r->name}</h3></div>";


        include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php';

        echo <<< EOS
    <ul class="nav navbar-nav">
        <li><button type='button' id='menuLinkBtn' class='btn btn-link navbar-btn' parentmenu='{$r->id}'><i class='fa fa-link'></i> Create Link</button></li>
        <li><button type='button' id='menuPageBtn' class='btn btn-link navbar-btn' parentmenu='{$r->id}'><i class='fa fa-edit'></i> Create Page</button></li>
    </ul>


    <ul class="nav navbar-nav pull-right">
        <li><button type='button' class='btn btn-link navbar-btn' id='deleteHeaderBtn' value='{$r->id}' onclick="menu.deleteMenuHeader({$r->id});"><i class='fa fa-trash-o danger'></i></button></li>
    </ul>

EOS;

        include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php';

        if (empty($menu)) echo $this->alerts->info("No menu items have been created for {$r->name}");
        else
        {
            echo <<< EOS
            <table class='table table-condensed table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Url</th>
                        <!-- <th>Icon</th> -->
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>

EOS;

            foreach ($menu as $mr)
            {
                if (!empty($mr->module))
                {
                    try
                    {
                        $modInfo = $this->modules->moduleInfo($mr->module);
                    }
                    catch(Exception $e)
                    {
                        PHPFunctions::sendStackTrace($e);
                    }

                    $name = $modInfo->name;
                    $url = $modInfo->url;

                }
                else
                {
                    $name = $mr->name;
                    $url = $mr->url;

                    $del = "";
                }
				
                $del = "<button type='button' class='btn btn-xs btn-danger' data-toggle='tooltip' data-placement='top' title='Remove Module' onclick=\"menu.deleteMenuItem({$mr->id}, {$id});\"><i class='fa fa-trash-o'></i></button>" . PHP_EOL;
                $del .= "<button type='button' class='btn btn-xs btn-info' data-toggle='tooltip' data-placement='top' title='Access' onclick=\"menu.loadAccessModal({$mr->id});\"><i class='fa fa-group'></i></button>" . PHP_EOL;

                if (!empty($mr->module))
                {
                    $del .= "<button type='button' class='btn btn-xs btn-warning' data-toggle='tooltip' data-placement='top' title='Permission' onclick=\"menu.loadModulePermissions({$mr->module});\"><i class='fa fa-unlock'></i></button>" . PHP_EOL;
                }

                echo "<tr>" . PHP_EOL;

                echo "\t<td>" . (empty($modInfo->icon) ? null : "<i class='{$modInfo->icon}'></i>") . " {$name}</td>" . PHP_EOL;
                echo "\t<td><a href='{$url}'>{$url}</a></td>" . PHP_EOL;
                //echo "\t<td>" . (empty($modInfo->icon) ? null : "<i class='{$modInfo->icon}'></i>") . "</td>" . PHP_EOL;
                echo "\t<td><div class='btn-group pull-right'>{$del}</div></td>" . PHP_EOL;

                echo "</tr>" . PHP_EOL;
            }

            echo "</tbody>" . PHP_EOL;

            echo "</table>" . PHP_EOL;

        }

        echo <<< EOS

		<div class='panel-footer'>
            <div class='module-drop-zone' value='{$r->id}'>Drop Modules Here</div>
		</div>
EOS;

        //echo "</div>" . PHP_EOL; // .span12 well-white

        echo "</div>" . PHP_EOL; // .panel

        echo "</li>" . PHP_EOL;
    }

    echo "</ul>" . PHP_EOL;
?>

<?php endif; ?>



        </div> <!-- #headers-container //-->

    </div> <!-- .span8 //-->

    <div class='col-lg-4'>


    <div class='panel panel-default'>
        <div class='panel-heading inline'>Module Reservoir</div>
        <div class='panel-body'>




        <p>Below is a list of all modules associated with the company account.</p>
<?php
if (empty($modules)) echo $this->alerts->info('No modules available.');
else
{

    echo "<div class='row'>\n";

    echo "<ol id='fileList' class='fileList'>\n";

    foreach ($modules as $r)
    {
        $name = (strlen($r->name) > 25) ? substr($r->name, 0, 22) . '...' : $r->name;

        $img = "/public/images/module-icon.png";

        try
        {
            $assigned = $this->menu->checkModuleAssigned($id, $r->id);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        if ($assigned == false)
        {

            echo "\t<li class='ui-state-default{$class}' value='{$r->module}' id='mod{$r->id}' file=\"{$r->name}\"><i class='{$r->icon} drag-icon'></i><div class='docName'>{$name}</div></li>\n";

        // echo "\t<li class='ui-state-default{$class}' value='{$r->module}' id='mod{$r->id}' file=\"{$r->name}\"><img src='{$img}' class='img-polaroid'><div class='docName'>{$name}</div></li>\n";
        }
    }

    echo "</ol>\n";
    echo "</div>" . PHP_EOL; // .row-fluid
}
?>

        </div> <!-- .panel-body -->

    </div> <!-- .panel -->


    </div> <!-- .col-lg-4 //-->
</div> <!-- .row //-->



<!-- new menu header modal //-->
<?php $this->load->view('menu/modals/menuheader'); ?>

<!-- create/edit menu link //-->
<?php $this->load->view('menu/modals/menulink'); ?>

<!-- new menu page modal //-->
<?php $this->load->view('menu/modals/menupage'); ?>

<!-- page position access  modal //-->
<?php $this->load->view('menu/modals/access'); ?>

<!-- module permissions modal //-->
<?php $this->load->view('menu/modals/permission'); ?>

<!-- module permissions modal //-->
<?php $this->load->view('menu/modals/editpermission'); ?>