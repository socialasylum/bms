<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div id='editPermModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Create Permissions</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='editPermAlert'></div>

        <div id='editPermDisplay'></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelEditPermBtn'>Cancel</button>
        <button class='btn btn-primary' id='saveEditPermBtn'>Save</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->