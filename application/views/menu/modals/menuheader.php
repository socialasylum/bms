<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div id='createMenuHeaderModal' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Create Menu Header</h4>
      </div>
      <div class="modal-body">
      
        <div id='menuHeaderAlert'></div>

        <p class='lead'>Enter the name of the menu header you want to create.</p>

        <?php
        $attr = array
        (
            'name' => 'menuHeaderForm',
            'id' => 'menuHeaderForm',
        );

        echo form_open('#', $attr);
        ?>
        <input type='hidden' name='id' id='id' value=''>
        <input type='hidden' name='header' id='header' value='1'>
        <p><input type='text' class='form-control' name='name' id='name' placeholder='Menu Header Name'></p>
        </form>
        

      </div>
      <div class="modal-footer">
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelMenuHeaderBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createMenuHeaderBtn'>Create Menu Header</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->