<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div id='createMenuLinkModal' class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Create Link</h4>
      </div>
      <div class="modal-body">
      
<div id='menuLinkAlert'></div>

        <p class='lead'>Enter the information about your link below.</p>

        <?php
        $attr = array
        (
            'name' => 'menuLinkForm',
            'id' => 'menuLinkForm',
            'class' => 'form-horizontal'
        );

        echo form_open('#', $attr);
        ?>
        <!-- <form name='menuLinkForm' id='menuLinkForm' class='form-horizontal' > -->
        <input type='hidden' name='id' id='id' value=''>
        <input type='hidden' name='parentMenu' id='parentMenu' value=''>
        <input type='hidden' name='header' id='header' value='0'>

            <div class="control-group">
                <label class='control-label' for='name'>Name</label>
                <div class="controls">
                    <input type='text' name='name' id='name' value="" placeholder=''>
                </div>
            </div>


            <div class="control-group">
                <label class='control-label' for='link'>Link</label>
                <div class="controls">
                    <input type='text' name='url' id='url' value="" placeholder='http://cgisolution.com'>
                </div>
            </div>

            <div class="control-group">
                <label class='control-label' for='target'>Target</label>
                <div class="controls">
                    <select name='target' id='target'>
                    <?php
                    if (!empty($linkTargets))
                    {
                        foreach ($linkTargets as $name => $value)
                        {
                            echo "<option value='{$value}'>{$name}</option>";
                        }
                    }
                    ?>
                    </select>
                </div>
            </div>


        </form>
        

      </div>
      <div class="modal-footer">
        <button type='button' class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelMenuHeaderBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createMenuLinkBtn'><i class='fa fa-save'></i> Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->