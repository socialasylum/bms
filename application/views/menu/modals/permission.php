<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div id='modPermModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Module Permissions</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='modPermAlert'></div>

        <div id='modPermDisplay'></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
<?php if ($this->session->userdata('admin') === true) : ?>
        <button type='button' class='btn btn-default' name='newModPermBtn' id='newModPermBtn'><i class='fa fa-plus'></i> New Permission</button>
<?php endif; ?>
        <button class='btn btn-info' data-dismiss='modal' aria-hidden='true' id='cancelModPermBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->