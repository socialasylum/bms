<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>
<div id='accessModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'><i class='fa fa-group'></i> Position Access</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='accessAlert'></div>

        <p class='lead'>Select which positions will have access to this menu.</p>

        <div id='access-display'></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-info' data-dismiss='modal' aria-hidden='true' id='cancelMenuPageBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->
