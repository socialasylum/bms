<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<?php
$attr = array
(
    'name' => 'modPermForm',
    'id' => 'modPermForm'
);

echo form_open('#', $attr);

echo "<input type='hidden' name='module' id='module' value='{$module}'>" . PHP_EOL;

if (empty($permissions))
{
    echo $this->alerts->info("This module has no permissions to assign");
}
else
{
    foreach ($permissions as $r)
    {

        $editBtn = null;

        if ($this->session->userdata('admin') === true)
        {
            $editBtn = " <button type='button' onclick=\"menu.loadEditPermModule({$module}, {$r->id});\" class='btn btn-default btn-xs pull-right' {$disabled} ><i class='fa fa-pencil'></i></button>";
        }

        echo <<< EOS
<div class='panel panel-default'>
    <div class='panel-heading'>{$r->title} : {$r->bit}{$editBtn}</div>
    <div class='panel-body'>
EOS;

        if (!empty($r->description)) echo "<p class='lead'>" . nl2br($r->description) . "</p>" . PHP_EOL;

        if (empty ($positions))
        {
            echo $this->alerts->info("No positiosn available to assign this module to!");
        }
        else
        {
            foreach ($positions as $pr)
            {
                echo "<div class='checkbox'><label>";

                $checked = null;

                if (!empty($access))
                {
                    foreach ($access as $ar)
                    {
                        if ($ar->position == $pr->id)
                        {
                            if ($ar->permissions & $r->bit)
                            {
                                $checked = "checked='checked'";
                                break;
                            }
                        }
                    }
                }

                echo "<input type='checkbox' name='position[{$pr->id}][{$r->bit}]' id='position_{$pr->id}_{$r->bit}' value='{$pr->id}' {$checked} > {$pr->name}";

                echo "</label></div>";
            }
        }

        
        echo "</div>"; // .panel-body
        echo "</div>"; // .panel

    }
}
?>

</form>
