<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>


<?php
$attr = array
(
    'name' => 'accessForm',
    'id' => 'accessForm',
    'class' => 'form-horizontal'
);

echo form_open('#', $attr);
?>
<input type='hidden' name='id' id='id' value='<?=$id?>'>
<?php

if (empty($positions))
{
    echo $this->alerts->info("No positions to choose from!");
}
else
{
    foreach ($positions as $r)
    {
        $checked = null;

        try
        {
            $access = $this->functions->checkMenuAccess($id, $r->id);

            if ($access == true) $checked = "checked='checked'";
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        echo "<div class='checkbox'><label>";

        echo "<input type='checkbox' name='position[]' id='position_{$r->id}' value='{$r->id}' {$checked}> {$r->name}";

        echo "</label></div>" . PHP_EOL;
    }
}
?>
</form>
