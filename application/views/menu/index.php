<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-list-ul'></i> Menu</h1>


<p class='lead'>Here you can edit the menu's for your company.</p>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>

<input type='hidden' id='company' value='<?=$this->session->userdata('company')?>'>


<div class='tabbable tabs-left'>
    <ul class='nav nav-tabs nav-tab-menu'>
<?php
if (!empty($companies))
{
    $cnt = 0;
    foreach ($companies as $r)
    {
        $class = ($cnt == 0) ? 'active' : null;

        echo "<li class='{$class}'><a href='#tab{$r->id}' data-toggle='tab'>{$r->companyName}</a></li>" . PHP_EOL;

    $cnt++;
    }
}
?>
    </ul>

    <div class='tab-content'>

<?php
if (!empty($companies))
{
    $cnt = 0;
    
    foreach ($companies as $r)
    {
        $class = ($cnt == 0) ? ' active' : null;

        echo "<div id='tab{$r->id}' class='tab-pane{$class}' value='{$r->id}'>";

        echo "<div id='tab{$r->id}-display'></div>";

        echo "</div>" . PHP_EOL;

		$cnt++;
    }
}
?>

    </div> <!-- .tab-contnet //-->

</div> <!-- .tabbable .tabs-left //-->



<!-- new menu header modal //-->
<div id='createMenuHeaderModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>
    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3>Create Menu Header</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='menuHeaderAlert'></div>

        <p class='lead'>Enter the name of the menu header you want to create.</p>

        <?php
        $attr = array
        (
            'name' => 'menuHeaderForm',
            'id' => 'menuHeaderForm',
        );

        echo form_open('#', $attr);
        ?>
        <input type='hidden' name='id' id='id' value=''>
        <input type='hidden' name='header' id='header' value='1'>
        <p><input type='text' class='input-block-level' name='name' id='name' placeholder='Menu Header Name'></p>
        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true' id='cancelMenuHeaderBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createMenuHeaderBtn'>Create Menu Header</button>
    </div> <!-- .modal-footer //-->


        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



<!-- create/edit menu link //-->
<div id='createMenuLinkModal' class='modal hide fade'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3>Menu Link</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='menuLinkAlert'></div>

        <p class='lead'>Enter the information about your link below.</p>

        <?php
        $attr = array
        (
            'name' => 'menuLinkForm',
            'id' => 'menuLinkForm',
            'class' => 'form-horizontal'
        );

        echo form_open('#', $attr);
        ?>
        <!-- <form name='menuLinkForm' id='menuLinkForm' class='form-horizontal' > -->
        <input type='hidden' name='id' id='id' value=''>
        <input type='hidden' name='parentMenu' id='parentMenu' value=''>
        <input type='hidden' name='header' id='header' value='0'>

            <div class="control-group">
                <label class='control-label' for='name'>Name</label>
                <div class="controls">
                    <input type='text' name='name' id='name' value="" placeholder=''>
                </div>
            </div>


            <div class="control-group">
                <label class='control-label' for='link'>Link</label>
                <div class="controls">
                    <input type='text' name='url' id='url' value="" placeholder='http://cgisolution.com'>
                </div>
            </div>

            <div class="control-group">
                <label class='control-label' for='target'>Target</label>
                <div class="controls">
                    <select name='target' id='target'>
                    <?php
                    if (!empty($linkTargets))
                    {
                        foreach ($linkTargets as $name => $value)
                        {
                            echo "<option value='{$value}'>{$name}</option>";
                        }
                    }
                    ?>
                    </select>
                </div>
            </div>


        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true' id='cancelMenuLinkBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createMenuLinkBtn'>Save</button>
    </div> <!-- .modal-footer //-->

</div>


<!-- new menu page modal //-->
<div id='createMenuPageModal' class='modal hide fade'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3>Create Page</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='menuPageAlert'></div>

        <p class='lead'>Enter the name of the page you want to create.</p>

        <form name='menuPageForm' id='menuPageForm' >
        <input type='hidden' name='id' id='id' value=''>
        <input type='hidden' name='parentMenu' id='parentMenu' value=''>
        <input type='hidden' name='header' id='header' value='0'>
        <p><input type='text' class='input-block-level' name='name' id='name' placeholder='Page Name'></p>
        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true' id='cancelMenuPageBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createMenuPageBtn'>Create Page</button>
    </div> <!-- .modal-footer //-->

</div>



<!-- page position access  modal //-->
<div id='accessModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Position Access</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='accessAlert'></div>

        <p class='lead'>Select which positions will have access to this menu.</p>

        <div id='access-display'></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-info' data-dismiss='modal' aria-hidden='true' id='cancelMenuPageBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



<!-- module permissions modal //-->
<div id='modPermModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Module Permissions</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='modPermAlert'></div>

        <div id='modPermDisplay'></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
<?php if ($this->session->userdata('admin') === true) : ?>
        <button type='button' class='btn' name='newModPermBtn' id='newModPermBtn'><i class='icon-plus'></i> New Permission</button>
<?php endif; ?>
        <button class='btn btn-info' data-dismiss='modal' aria-hidden='true' id='cancelModPermBtn'>Save &amp; Close</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



<!-- module permissions modal //-->
<div id='editPermModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title'>Create Permissions</h4>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='editPermAlert'></div>

        <div id='editPermDisplay'></div>

    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelEditPermBtn'>Cancel</button>
        <button class='btn btn-primary' id='saveEditPermBtn'>Save</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->
