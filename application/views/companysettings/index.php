<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-gears'></i> Company Settings</h1>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>
<?php
if (empty($companies))
{
	echo $this->alerts->alert("There are no companies! Not sure how you are seeing this though.");
}
else
{

echo <<< EOS
    <table class='table table-hover table-condensed' id='compTbl'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Postal Code</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
EOS;

	foreach ($companies as $r)
	{
		$info = $status = null;
		$compAdmin = false;
		
		try
		{
			$compAdmin = $this->users->isCompanyAdmin($this->session->userdata('userid'), $r->id);
			
			// skips if they are not an admin of that company
			if (!$compAdmin) continue;
			
			$info = $this->companysettings->getInfo($r->id);
			$status = ($info->active == 1) ? 'Active' : 'Inactive';
			
			$statusLbl = ($info->active == 1) ? 'success' : 'danger';
		}
		catch (Exception $e)
		{
			PHPFunctions::sendStackTrace($e);
			continue;
		}
	
		echo "<tr data-id='{$r->id}'>" . PHP_EOL;

		echo "<td>{$r->id}</td>" . PHP_EOL;
		echo "<td>{$r->companyName}</td>" . PHP_EOL;
		echo "<td>{$r->addressLine1}</td>" . PHP_EOL;
		echo "<td>{$info->city}</td>" . PHP_EOL;
		echo "<td>{$info->state}</td>" . PHP_EOL;
		echo "<td>{$info->postalCode}</td>" . PHP_EOL;
		echo "<td><div class='label label-{$statusLbl}'>{$status}</div></td>" . PHP_EOL;
		echo "<td><button type='button' class='btn btn-info btn-xs pull-right' onclick=\"compsettings.index.edit(this, {$r->id});\"><i class='fa fa-edit'></i></button></td>" . PHP_EOL;
		echo "</tr>" . PHP_EOL;
	}
	
	echo "</tbody>" . PHP_EOL;
	echo "</table>" . PHP_EOL;
}