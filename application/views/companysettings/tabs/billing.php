<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<div id='tabBilling' class='tab-pane form-horizontal'>

<input type='hidden' name='exempt' value='0'>

<?php if ($this->session->userdata('admin') === true) : ?>

    <h2><i class='fa fa-star-o'></i> Billing Exempt</h2>

    <p class='lead'>
 Is this company billing exempt? Meaning they can have access to as many users and modules as they want without charge. If so click the <strong>Billing Exempt</strong> checkbox below.</p>


 	<div class='checkbox'>
 		<label>
                <?php
                    $exemptChecked = ($info->billingExempt == 1) ? 'checked' : null;
                ?>
	        <input type='checkbox' name='exempt' id='exempt' value='1' <?=$exemptChecked?>> Company is Billing Exempt
		</label>
	</div>


    <hr>
<?php endif; ?>





        <div class='col-xs-12 col-sm-6'>

        <h2><i class='fa fa-exclamation'></i> Billing Information</h2>

			<div class='col-xs-12'>
            <div class="form-group">
                <label for='billingFirstName'>First Name</label>
                <input type='text' class='form-control' name='billingFirstName' id='billingFirstName' value="<?=$info->billingFirstName?>" placeholder="John">
            </div> <!-- .form-group -->


            <div class="form-group">
                <label for='billingLastName'>Last Name</label>
                    <input type='text' class='form-control' name='billingLastName' id='billingLastName' value="<?=$info->billingLastName?>" placeholder="Smith">
            </div> <!-- .form-group -->
			</div> <!-- col-12 -->

            <dl class='dl-horizontal'>
                <dt>Monthly Total</dt>
                <dd><span id='billingTotal'>$<?=(empty($info->subscriptionPrice)) ? '0.00' : $info->subscriptionPrice?></span></dd>

                <dt>Users</dt>
                <dd><span id='billingUsers'><?=(empty($info->users)) ? $this->config->item('min_users') : $info->users?></span></dd>

                <?php if (!empty($info->BrainTreeSubscriptionID)) : ?>
                <dt>Subscription ID</dt>
                <dd><?=$info->BrainTreeSubscriptionID?></dd>
                <?php endif; ?>


                <dt>Subscription Status</dt>
                <?php
                    $statusLbl = 'label-info';

                    if ($subStatus == 'CANCELED') $statusLbl = 'label-danger';
                    if ($subStatus == 'ACTIVE') $statusLbl = 'label-success';

                ?>
                    <dd><label class='label <?=$statusLbl?>'><?=$subStatus?></label></dd>


                </dl>

            


        </div> <!-- col-6 -->


        <div class='col-xs-12 col-sm-6'>


            <div class='panel panel-default'>
                <div class='panel-heading'>
                <h3 class='panel-title'><i class='fa fa-credit-card'></i> Card on File</h3>
                </div> <!-- .panel-heading -->

                <div class='panel-body'>
<?php
if (empty($cards))
{
        echo $this->alerts->info("You currently have no cards on file.");
}
else
{
echo <<< EOS
    <table id='cardTbl' class='table table-hover'>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Type</th>
                <th>Last 4 Digits</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($cards as $r)
    {
        $typeDisplay = null;

        try
        {
            $typeDisplay = $this->functions->codeDisplay(18, $r->cardType);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "<td><input type='radio' name='previousCard' id='previousCard_{$r->id}' value='{$r->id}'></td>";
        echo "<td>{$typeDisplay}</td>";
        echo "<td>{$r->lastFour}</td>";

        echo "</tr>" . PHP_EOL;
    }


echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>


                </div> <!-- .panel-body -->


            </div> <!-- .panel -->



            <div class='panel panel-default'>
                <div class='panel-heading'>
                <h3 class='panel-title'><i class='fa fa-credit-card'></i> New Card</h3>
                </div> <!-- .panel-heading -->

                <div class='panel-body'>
					<div class='col-xs-12'>
                    <div class="form-group">
                        <label for='ccName'>Name on Card</label>
                        <input type='text' class='form-control' name='ccName' id='ccName' value="" placeholder='John Smith'>
                    </div>

                    <div class="form-group">
                        <label for='ccNumber'>Card Number</label>
                        <input type='text' class='form-control' name='ccNumber' id='ccNumber' value="" placeholder='0000-1234-5678-9999'>
                    </div>

                    <div class="form-group">
                        <label for='ccExp'>Card Expiration</label>

						<div class='row'>
							<div class='col-xs-6'>
								<select name="ccExpM" id="ccExpM" class='form-control'>
	                                <option value="">Month</option>
					                <?php
					                for ($i = 1; $i <= 12; $i++)
					                {
					
					                    $display = str_pad($i, 2, '0', STR_PAD_LEFT);
					
					                    echo "<option value='{$display}'>{$display}</option>" . PHP_EOL;
					                }
					                ?>
	                            </select>
							</div>
							
							<div class='col-xs-6'>
		                       <select name="ccExpY" id="ccExpY" class='form-control'>
	                                <option value="">Year</option>
					                <?php
					                for ($i = (int) date("Y"); $i <= ((int) date("Y") + 10); $i++) 
					                {
					                    echo "<option value='{$i}'>{$i}</option>" . PHP_EOL;
					                }
					                ?>
		                        </select>
							</div>
						
						</div> <!-- /.row -->
                    </div> <!-- /.form-group -->


                    <div class="form-group">
                        <label for='ccCvv'>CVV</label>
                        <input type='text' class='form-control' name='ccCvv' id='ccCvv' value="" placeholder='000'>
                    </div> <!-- .form-group -->


                </div> <!-- .panel-body -->

                </div>
            </div> <!-- .panel -->


        </div> <!-- .col-6 -->



<?php if (!empty($info->BrainTreeSubscriptionID) && $subStatus == 'ACTIVE') : ?>

    <hr>

    <h2><i class='fa fa-ban'></i> Cancel Subscription</h2>
    
    <p class='lead'>If you want to cancel the subscription associated with this company. Click the <span class='label label-danger'>Cancel Subscription button</span> bellow.</p>

    <p>Note: By cancelling your subscription will lockout all users from the system.</p>

    <button type='button' class='btn btn-danger btn-m' id='cancelSubBtn'><i class='fa fa-ban'></i> Cancel Subscription</button>

<?php endif; ?>

</div> <!-- #tabBilling -->
