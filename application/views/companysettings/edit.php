<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-gears'></i> <?=$info->companyName?></h1>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><button type='button' id='saveBtn' class='btn btn-link navbar-btn'><i class='fa fa-save success'></i> Save</button></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li class='muted disabled'><button id='cancelBtn' type='button' class='btn btn-link navbar-btn'><i class='fa fa-ban danger'></i> Cancel</button></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php

    $attr = array
        (
            'name' => 'companyForm',
            'id' => 'companyForm',
            'class' => 'form-horizontal'
        );

echo form_open_multipart('/companysettings/update', $attr);
?>

    <input type='hidden' name='id' id='id' value='<?=$info->id?>'>
    <input type='hidden' name='users' id='users' value='<?=(empty($info->users)) ? $this->config->item('min_users') : $info->users?>'>

    <input type='hidden' name='orgPrice' id='orgPrice' value='<?=$info->subscriptionPrice?>'>

    <input type='hidden' name='calcPrice' id='calcPrice' value='0.00'>

    <input type='hidden' name='subStatus' id='subStatus' value='<?=$subStatus?>'>

    <input type='hidden' name='BrainTreeSubscriptionID' id='BrainTreeSubscriptionID' value='<?=$info->BrainTreeSubscriptionID?>'>
    <input type='hidden' name='BrainTreeCustomerID' id='BrainTreeCustomerID' value='<?=$info->BrainTreeCustomerID?>'>

<!-- holds serizlied hidden inputs of modules selected -->
<div id='moduleHiddenContainer'></div>


<div class='tabbable'>
    <ul class='nav nav-tabs' id='compSettingsTabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab"><i class='fa fa-cog'></i> Settings</a></li>
        <li><a href='#tabBilling' data-toggle="tab"><i class='fa fa-money'></i> Billing Information</a></li>
        <li><a href='#tabModules' data-toggle="tab"><i class='fa fa-puzzle-piece'></i> Modules &amp; Users</a></li>
        <li><a href='#tabReg' data-toggle="tab"><i class='fa fa-pencil'></i> Registration</a></li>
        <li><a href='#tabUsers' data-toggle="tab"><i class='fa fa-user'></i> Users</a></li>
    </ul>



<div class="tab-content">

    <div id="tabSettings" class='tab-pane active'>

<p class='lead'>Here you can adjust your company settings and upload a logo.</p>



    <div class="col-xs-12" id='logo-display'>
        <?php
            if (!empty($info->logo)) echo "<img class='img-responsive' id='logoPreview' src='/public/uploads/logos/{$info->id}/{$info->logo}'>";
        ?>
	


        <input type='file' name='file' id='file'>

    </div>

<div class='col-xs-12'>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyName'>Company Name</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='companyName' id='companyName' value="<?=$info->companyName?>">
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-sm-3 control-label' for='logo'>Logo</label>
    <div class="col-sm-9 controls">
       	<div class='input-group layout-bg-input-group'>
			<input type='text' class='form-control' id='logo' name='logo' value='<?=$info->logo?>' placeholder="company_logo.png">
		
			<span class='input-group-btn'>
				<button type='button' class='btn btn-danger' id='delLogoBtn' <?php if (empty($info->logo)) echo "disabled='disabled'"; ?>><i class='fa fa-trash-o'></i></button>
			</span>
		</div> <!-- /.input-group -->
    </div> <!-- .controls -->
</div> <!-- .form-group -->



<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='addressLine1'>Address</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='addressLine1' id='addressLine1' value="<?=$info->addressLine1?>" >
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='addressLine2'>Address 2</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='addressLine2' id='addressLine2' value="<?=$info->addressLine2?>" >
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='addressLine3'>Address 3</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='addressLine3' id='addressLine3' value="<?=$info->addressLine3?>" >
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='city'>City</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='city' id='city' value="<?=$info->city?>" placeholder='Las Vegas' >
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='state'>State</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <select name="state" id="state" class='form-control'>
            <option value=""></option>
<?php
if (!empty($states))
{
    foreach ($states as $abb => $stateName)
    {
        $sel = ($info->state == $abb) ? 'selected' : null;

        echo "<option {$sel} value='{$abb}'>{$stateName}</option>\n";
    }
}
?>
        </select>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='postalCode'>Postal Code</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='postalCode' id='postalCode' value="<?=$info->postalCode?>" placeholder='89011'>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='phone'>Phone</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='phone' id='phone' value="<?=$info->phone?>" placeholder='(888) 555-1212'>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='email'>E-mail</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='email' id='email' value="<?=$info->email?>" placeholder='email@address.com'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


	<div class="form-group">
	    <label for="hideFooter" class="col-sm-3 control-label">Footer</label>
		<div class="col-sm-9">
			<div class='checkbox'>
				<label>
					<input type='hidden' name='hideFooter' value='0'>
					
					<?php
					if ($info->hideFooter == '1') $footerChecked = "checked='checked'";
					?>
					
					<input type='checkbox' name='hideFooter' id='hideFooter' value='1' <?=$footerChecked?>> Hide Footer
				</label>
			</div>
								<span class="help-block">If checked, the footer on the bottom of the page for this company will be completely hidden.</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-3 control-label">Landing URL</label>
		<div class="col-sm-9">
			<input type='text' class='form-control' name='landingUrl' id='landingUrl' placeholder="<?=$this->config->item('defaultLandingUrl')?>" value="<?=$info->landingUrl?>">
			<span class="help-block">Leave blank to use system default landing url: <?=$this->config->item('defaultLandingUrl')?></span>
	    </div> <!-- col-9 -->
	    
	    <!--
	    <div class='col-sm-5'>
	    	<span class='muted'>Leave blank to use system default landing page: <?=$this->config->item('defaultLandingUrl')?></span>
	    </div>
	    -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="indexUrl" class="col-sm-3 control-label">Index Url</label>
		<div class="col-sm-9">
			<input type='text' class='form-control' name='indexUrl' id='indexUrl' placeholder="<?=$this->config->item('defaultIndexUrl')?>" value="<?=$info->indexUrl?>">
			<span class="help-block">Leave blank to use system default index url: <?=$this->config->item('defaultIndexUrl')?></span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='email'>Domains</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        	<div class='input-group'>
	            <input type='text' class='form-control' name='domain' id='domain' value="" placeholder='domain.com'>
		            <span class="input-group-btn">
						<button type='button' class='btn btn-default' id='addDomainBtn'><i class='fa fa-plus'></i></button>
		            </span>
        	</div> <!-- /.input-group -->

            <span class="help-block" id=''>Enter any domains associated with this company as it will detect the company based upon the domain.</span>
            
            <div id='domains'><?php
	            if (!empty($domains))
	            {
		            foreach ($domains as $r)
		            {
			            echo "<div class='label label-info msg-to-lbl' data-key='{$r->id}'>";
			            echo "<label>{$r->domain}</label>";
			            echo "<button type='button' class='close' onclick=\"compsettings.removeDomain(this);\">&times;</button>";
			            echo "</div>" . PHP_EOL;
		            }
	            }
            ?></div>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

		</div>

</div> <!-- #tabSettings -->

<?php
	include_once 'tabs/billing.php';
?>

<div id='tabModules' class='tab-pane'>



<div class='row' align='center'><h1>$<span id='est-display'>100.00</span> / Month </h1></div>
<hr>

<!--
    <h2><i class='fa fa-puzzle-piece'></i> Modules &amp; Users</h2>

    <p class='lead'>Below you can adjust the modules purchased for this comapny as well as adjust the amount of users your company requires.</p>

<hr>
-->
<h2>Select Number of Users</h2>

<p class='lead'>Select how many users your Business Management System will require.</p>

<h3><i class='fa fa-user'></i> <span id='numUsers'><?=(empty($info->users)) ? $this->config->item('min_users') : $info->users?></span> Users</h3>

        <!-- <p>Select amount of users you will require.</p> -->
        <div id="slider"></div>
<hr>

    <h2><i class='fa fa-puzzle-piece'></i> Select Modules</h2>

    <p class='lead'>Select which modules you want to include in your Business Management System.</p>

<?php

echo "<div id='mod-container-list'>" . PHP_EOL;
if (!empty($modules))
{
    $rcnt = 1;

    foreach ($modules as $r)
    {
        $assigned = false;

        // checks if moduel is assigned to company already or not
        $assigned = $this->modules->checkModuleAssigned($this->session->userdata('company'), $r->id);

        if ($rcnt == 1) echo "<div class='row'>" . PHP_EOL;

        $selectedClass = ($assigned == true) ? 'package-selected' : null;

        echo "<div class='col-lg-3 col-m-3 col-s-3 col-xs-3'>";
        echo "<div class='well package-sections {$selectedClass}' id='module_{$r->id}' ppu='{$r->price}' value='{$r->id}'  onclick=\"compsettings.selectModule($(this), {$r->id});\">";

        echo "<h2 class='text-info'>";

        if (!empty($r->icon)) echo "<i class='{$r->icon} drag-icon'></i><br>";

        echo "{$r->name}</h2>" . PHP_EOL;

        if (!empty($r->description)) echo $r->description;


        echo "<hr>";


        echo "<div align='center'>";
        echo "<h3>$" . number_format($r->price, 2) . "/user</h3>";

        // echo "<button class='btn  btn-large'>Select</button>";

        echo "</div>";


        echo "</div>"; // .well
        echo "</div>" . PHP_EOL; // .span3

        if ($rcnt >= 4)
        {
            echo "</div>" . PHP_EOL;
            // echo "<div class='row'>" . PHP_EOL;

            $rcnt = 1;
        }
        else
        {
            $rcnt++;
        }
    }

    if ($rcnt > 1 && $rcnt < 4) echo "</div>" . PHP_EOL; // .row if ends with less than 4 modules
}
echo "</div>";
?>




</div> <!-- #tabModules -->

<div id='tabReg' class='tab-pane form-horizontal'>


    <h2><i class='fa fa-pencil'></i> Registration</h2>

    <p class='lead'>Here you can manage settings about users registering to your company.</p>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='allowReg'>Allow Registration</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">

        <?php
        $allowRegChecked[$info->allowReg] = "checked='checked'";
        ?>
        	<div class='radio'><label><input type='radio' name='allowReg' value='1' <?=$allowRegChecked[1]?>> Yes</label></div>
            <div class='radio'><label><input type='radio' name='allowReg' value='0' <?=$allowRegChecked[0]?>> No <small><span class='text-muted'>(Users can only be added through the users process internally.)</span></small></label></div>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='allowFBreg'>Allow <i class='fa fa-facebook-square'></i> Facebook Registration</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">

        <?php
        $allowFBregChecked[$info->allowFBreg] = "checked='checked'";
        ?>
            <div class='radio'><label><input type='radio' name='allowFBreg' value='1' <?=$allowFBregChecked[1]?>> Yes</label></div>
            <div class='radio'><label><input type='radio' name='allowFBreg' value='0' <?=$allowFBregChecked[0]?>> No </label></div>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

<hr>

<h3>Becoming a Member</h3>

<p class='lead'>The text below displays on the registration page and is a good place to add key points of why you want users to register.</p>

<textarea class='form-control' name='becomeMemberTxt' id='becomeMemberTxt' rows='5'><?=$info->becomeMemberTxt?></textarea>

	</div> <!-- #tabReg -->

	<div id='tabUsers' class='tab-pane form-horizontal'>
		<h2><i class='fa fa-user'></i> Users</h2>
		
		<p class='lead'>Here you can add/remove features for users.</p>
		
		<div class='checkbox'>
			<label>
				<input type='hidden' name='fbSync' value='0'>
				<input type='checkbox' id='fbSync' name='fbSync' value='1' <?php if ($info->fbSync > 0) echo "checked='checked'"; ?>> Allow users to sync account with Facebook<sup>&reg;</sup>
			</label>
		</div> <!-- ./checkbox -->


		<div class='checkbox'>
			<label>
				<input type='hidden' name='ytUpload' value='0'>
				<input type='checkbox' id='ytUpload' name='ytUpload' value='1' <?php if ($info->ytUpload > 0) echo "checked='checked'"; ?>> Upload YouTube Videos
			</label>
		</div> <!-- ./checkbox -->

	
	</div> <!-- /.#tabUsers -->

</div> <!-- .tab-contents //-->
</div> <!-- .tabbable //-->


</form>
