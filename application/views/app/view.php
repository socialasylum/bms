<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h2><?=$info->firstName?> <?=$info->MI?> <?=$info->lastName?></h2>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>SSN</th>
            <th>Are you 18 or older?</th>
            <th>Are you eligible to work in US?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$info->SSN?></td>
            <td><?php echo ($info->is18 == 1) ? "Yes" : "No"; ?></td>
            <td><?php echo ($info->workUsa == 1) ? "Yes" : "No"; ?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$info->address?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>City</th>
            <th>State</th>
            <th>Postal Code</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$info->city?></td>
            <td><?=$info->state?></td>
            <td><?=$info->postalCode?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Home Phone</th>
            <th>Mobile Number</th>
            <th>E-mail Address</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$info->phone?></td>
            <td><?=$info->mobile?></td>
            <td><?=$info->email?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Referred By</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$reasonDisplay?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Related to/knows someone who works for <?=$companyName?>?</th>
            <th>Who</th>
            <th>Where</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo ($info->related == 1) ? "Yes" : "No"; ?></td>
            <td><?=$info->relatedWho?></td>
            <td><?=$info->relatedWhere?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Have you previously worked for <?=$companyName?>?</th>
            <th>From</th>
            <th>To</th>
            <th>Reason For Leaving?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo ($info->previous == 1) ? "Yes" : "No"; ?></td>
            <td><?=$info->previousFrom?></td>
            <td><?=$info->previousTo?></td>
            <td><?=$info->previousReason?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Desired State</th>
            <th>Desired Location</th>
            <th>Currently Employed</th>
            <th>Desired Position</th>
            <th>Date you can start</th>
            <th>Salary Desired</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$info->desiredState?></td>
            <td><?=$locationInfo->Description?></td>
            <td><?php echo ($info->employed == 1) ? "Yes" : "No"; ?></td>
            <td><?=$positionDisplay?></td>
            <td><?=$info->startDate?></td>
            <td><?=$info->desiredSalary?></td>
        </tr>
    </tbody>
</table>

<hr>
<h3>Availability</h3>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
            <th>Saturday</th>
            <th>Sunday</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><strong>AM</strong></td>
            <td><?php if($info->monAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->tueAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->wedAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->thuAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->friAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->satAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->sunAM == 1) echo "<i class='icon-ok'></i>"; ?></td>
        </tr>
        <tr>
            <td><strong>PM</strong></td>
            <td><?php if($info->monPM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->tuePM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->wedPM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->thuPM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->friPM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->satPM == 1) echo "<i class='icon-ok'></i>"; ?></td>
            <td><?php if($info->sunPM == 1) echo "<i class='icon-ok'></i>"; ?></td>
        </tr>
    </tbody>
</table>

<hr>

<h3>Education</h3>

<?php

if (empty($edu))
{
    echo $this->alerts->info("Applicant did not enter any education");
}
else
{

    echo <<< EOS
<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Type</th>
            <th>Name &amp; Location</th>
            <th># of years</th>
            <th>Graduate?</th>
            <th>Subjects Studied</th>
        </tr>
    </thead>
    <tbody>
EOS;

    foreach ($edu as $r)
    {
        $gradDisplay = ($r->grad == 1)? "Yes" : "No";

        $typeDisplay = $this->functions->codeDisplay(4, $r->type);

        echo "\n<tr>\n";

        echo "\t<td valign='top'>{$typeDisplay}</td>\n";
        echo "\t<td valign='top'>".nl2br($r->desc)."</td>\n";
        echo "\t<td valign='top'>{$r->years}</td>\n";
        echo "\t<td valign='top'>{$gradDisplay}</td>\n";
        echo "\t<td valign='top'>{$r->studied}</td>\n";

        echo "\n</tr>\n";
    }


    echo "</tbody>\n";
    echo "</table>\n";

}

?>

<hr>


<h3>Professional Experience</h3>

<?php

if (empty($jobs))
{
    echo $this->alerts->info("Applicant did not enter any professional experience");
}
else
{
    echo <<< EOS
    <table class='table table-bordered table-striped'>
        <thead>
            <tr>
                <th>From</th>
                <th>To</th>
                <th>Name, Address &amp; Phone Number</th>
                <th>Salary</th>
                <th>Position</th>
                <th>Reason for leaving</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($jobs as $r)
    {
        echo "\n<tr>\n";

        echo "\t<td valign='top'>{$r->from}</td>\n";
        echo "\t<td valign='top'>{$r->to}</td>\n";
        echo "\t<td valign='top'>{$r->companyName}<br>".nl2br($r->companyAddress)."<br>{$r->companyPhone}</td>\n";
        echo "\t<td valign='top'>{$r->salary}</td>\n";
        echo "\t<td valign='top'>{$r->position}</td>\n";
        echo "\t<td valign='top'>{$r->reason}</td>\n";

        echo "\n</tr>\n";
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>\n";
}
?>


<hr>

<h3>References</h3>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Business/Occupation</th>
            <th>Years Acquainted</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$info->refName1?></td>
            <td><?=$info->refPhone1?></td>
            <td><?=$info->refOcc1?></td>
            <td><?=$info->refYear1?></td>
        </tr>
        <tr>
            <td><?=$info->refName2?></td>
            <td><?=$info->refPhone2?></td>
            <td><?=$info->refOcc2?></td>
            <td><?=$info->refYear2?></td>
        </tr>
        <tr>
            <td><?=$info->refName3?></td>
            <td><?=$info->refPhone3?></td>
            <td><?=$info->refOcc3?></td>
            <td><?=$info->refYear3?></td>
        </tr>

        </tbody>
</table>

<hr>

<h3>Miscellaneous</h3>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Special Skills</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=nl2br($info->skills)?></td>
        </tr>
    </tbody>
</table>

<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>US Military or Naval Service</th>
            <th>Rank</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
            <?php
                $milService = $info->milService;

                echo (empty($milService)) ? '&nbsp;' : $milService;
            ?>
            </td>
            <td><?=$info->rank?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Present Membership in National Guard or Reserves</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php
                $guard = $infoguard;

                echo (empty($guard)) ? '&nbsp;' : $guard;
                ?></td>
        </tr>
    </tbody>
</table>


<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>Have you been convicted of a crime other than a traffic offense?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo ($info->convict == 1) ? "Yes" : "No"; ?></td>
        </tr>
    </tbody>
</table>

<?php if ($info->convict == 1) : ?>
<table class='table table-bordered table-striped'>
    <thead>
        <tr>
            <th>When, where and nature of offense?</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=nl2br($info->offense)?></td>
        </tr>
    </tbody>
</table>
<?php endif; ?>

<?php if ($processApplications === true) : ?>
<hr>

<h1>Application Processing</h1>
<?php endif; ?>



