<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h2>Applications</h2>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><a href='/app/postposition'><i class='fa fa-file'></i> Post Open Position</a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<div class='tabbable'>
    <ul class='nav nav-tabs'>
        <li class='active'><a href='#tabApps' data-toggle="tab">Available Applicants</a></li>
        <li><a href='#tabOP' data-toggle="tab">Open Positions <span class='badge'><?=$numOP?></span></a></li>
    </ul>

<div class="tab-content">

    <div id="tabApps" class='tab-pane active'>



<?php
if (empty($applications)) : echo $this->alerts->info('No one has submitted an application.');
else :
?>

<table class='table table-stripped table-bordered' id='appTbl'>
    <thead>
        <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Location</th>
            <th>Position</th>
            <th>Status</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php

    foreach ($applications as $r)
    {

        try
        {
            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "m/d/Y g:i A");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>";
    
        echo "<td>{$r->firstName} {$r->lastName}</td>";
        echo "<td>{$date}</td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td>[Status]</td>";
        echo "<td>
            
                <a href=\"/app/view/{$r->id}\" class='btn btn-link pull-right'><i class='fa fa-plus-square'></i></a>
        </td>";

        echo "</tr>" . PHP_EOL;

    }

?>

    </tbody>
</table>


<?php endif;?>
    </div> <!-- .tabApps //-->

    <div id="tabOP" class='tab-pane'>

<?php
if (empty($openPositions)) : echo $this->alerts->info('There are no posted positions');
else :
?>

<table class='table table-stripped table-bordered' id='openTbl'>
    <thead>
        <tr>
            <th>Position</th>
            <th>Location</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php
    foreach ($openPositions as $k => $r)
    {

        $positionDisplay = $locationDisplay = null;

        try
        {
            if (empty($r->positionId)) $positionDisplay = $r->title;
            else $positionDisplay = $this->positions->getName($r->positionId);

            if (empty($r->locationId)) $locationDisplay = $r->locationTxt;
            else $locationDisplay = $this->locations->getTableValue('name', $r->locationId);


            $startDate = $this->users->convertTimezone($this->session->userdata('userid'), $r->startDate, "m/d/Y");
            $endDate = $this->users->convertTimezone($this->session->userdata('userid'), $r->endDate, "m/d/Y");


        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }


        
        echo "<tr>" . PHP_EOL;

        echo "<td>{$positionDisplay}</td>" . PHP_EOL;
        echo "<td>{$locationDisplay}</td>" . PHP_EOL;
        echo "<td>{$startDate}</td>" . PHP_EOL;
        echo "<td>{$endDate}</td>" . PHP_EOL;
        echo "<td>

            <button class='btn btn-link pull-right' value='{$r->id}' onclick=\"users.editUser($(this));\" id='editUserBtn{$r->id}'><i class='fa fa-pencil icon-white'></i></button>   <button class='btn btn-link pull-right' value='{$r->id}' onclick=\"users.editUser($(this));\" id='editUserBtn{$r->id}'><i class='fa fa-trash-o icon-white'></i></button>

            </td>";
        echo "</tr>" . PHP_EOL;
    }
?>
    </tbody>
</table>


<?php endif;?>


    </div> <!-- .tabOP //-->

</div> <!-- .tabContent //-->

</div> <!-- .tabbable -->

