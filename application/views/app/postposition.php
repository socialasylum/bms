<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Post Open Position</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<p class='lead'>Here you can post a new available position you are looking to recruit for.</p>

<?php
    $attr = array
        (
            'name' => 'createForm',
            'id' => 'createForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>

<?php if (!empty($positions)) : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='position'>Position</label>
    <div class='col-lg-10 controls'>
        <select class='form-control' name="position" id="position">
            <option value=""></option>
                <?php
                    foreach ($positions as $r)
                    {
                        // $sel = ($r->id == $id) ? 'selected' : null;

                        echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                    }
                ?>
        </select>
    </div>
</div>
<?php else: ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='title'>Job Title</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='title' id='title' value="" placeholder='Job Title'>
    </div>
</div>
<?php endif; ?>

<?php if (empty($locations)) : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='location'>Location</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='locationTxt' id='locationTxt' value="" placeholder='Location'>
    </div>
</div>
<?php else : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='locationId'>Location</label>
    <div class='col-lg-10 controls'>
        <select class='form-control' name="locationId" id="locationId">
            <option value=""></option>
                <?php
                    foreach ($locations as $r)
                    {
                        // $sel = ($r->id == $id) ? 'selected' : null;

                        echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                    }
                ?>
        </select>
    </div>
</div>
<?php endif;  ?>


<div class="form-group">
    <label class='col-lg-2 control-label' for='startDate'>Start Date</label>
    <div class='col-lg-10 controls'>
            <input type='text' class='form-control' name='startDate' id='startDate' value="" placeholder='YYYY-MM-DD'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='endDate'>End Date</label>
    <div class='col-lg-10 controls'>
            <input type='text' class='form-control' name='endDate' id='endDate' value="" placeholder='YYYY-MM-DD'>
    </div>
</div>

<?php if (!empty($positions)) : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for=''>Description</label>
    <div class='col-lg-10 controls'>
        <div id='position-description'></div>
    </div>
</div>
<?php endif; ?>

<div class="form-group">
    <label class='col-lg-2 control-label' for='description'><?php if (!empty($positions)) echo 'Additional<br>'; ?>Description</label>
    <div class='col-lg-10 controls'>
        <textarea name='description' id='description'></textarea>
    </div>
</div>

<div class='row'>
    <div class='col-lg-10 col-offset-2'>
        <button type='button' class='btn btn-primary' id='postBtn'>Post Position</button>
        <button type='button' class='btn' id='cancelBtn'>Cancel</button>
    </div>
</div>




</form>
