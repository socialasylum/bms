<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1 class='text-danger'><i class='fa fa-ban'></i> You Do Not Have Access</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

    <p class='lead'>It appears you do not have access to the <?php if (!empty($moduleName)) echo $moduleName . ' '; ?>module. Check with your system administrator if you require access.</p>
