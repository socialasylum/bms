<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit'?> Territory</h1>


<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>
<?php

    $attr = array
        (
            'name' => 'editForm',
            'id' => 'editForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);

if (!empty($id))
{
    echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
    echo "<input type='hidden' name='orgName' id='orgName' value=\"{$info->name}\">" . PHP_EOL;


    echo "<input type='hidden' name='mapCenterLat' id='mapCenterLat' value='{$info->mapCenterLat}'>" . PHP_EOL;
    echo "<input type='hidden' name='mapCenterLng' id='mapCenterLng' value='{$info->mapCenterLng}'>" . PHP_EOL;
    echo "<input type='hidden' name='mapZoom' id='mapZoom' value='{$info->mapZoom}'>" . PHP_EOL;
}

// print_r($markers);

if (!empty($markers))
{
    echo "<div id='savedMarkers'>" . PHP_EOL;

    foreach ($markers as $r)
    {
        echo "<input type='hidden' lat='{$r->lat}' lng='{$r->lng}' radius='{$r->radius}' color='{$r->color}' opacity='{$r->opacity}' >" . PHP_EOL;
    }

    echo "</div> <!-- #savedMarkers -->" . PHP_EOL;
}

?>

    <div class='col-lg-12 col-m-12 col-s-12 col-xs-12 territoryEditMap img-thumbnail' id='map'></div>

<div class="form-group">
    <label class='col-lg-2 control-label' for='name'>Territory Name</label>
    <div class="col-lg-10 controls">
        <input type='text' class='form-control' name='name' id='name' value="<?=$info->name?>" placeholder='Location Name'>
    </div>
</div>

<?php if (!empty($positions)) : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='ownerPosition'>Position Owner</label>
    <div class="col-lg-10 controls">
        <select class='form-control' name="positionOwner" id="positionOwner" class='form-control'>
            <option value=""></option>
<?php
    if (!empty($positions))
    {
        foreach ($positions as $r)
        {
            $sel = ($r->id == $info->positionOwner) ? 'selected' : null;

            echo "<option {$sel} value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
        }
    }
?>
        </select>
    </div>
</div>
<?php endif; ?>


    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='googleMapCircleColor'>Color</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
<?php
    $color = (empty($id)) ? 'FF0000' : $color;
?>
        <input type='text' name='googleMapCircleColor' id='googleMapCircleColor' value='<?=$color?>' hex='<?=$color?>' style="background-color:#<?=$color?>;" class='form-control color-picker'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='googleMapCircleOpacity'>Opacity <span id='opacityPercent'>80%</span></label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <input type='hidden' name='googleMapCircleOpacity' id='googleMapCircleOpacity' value='<?=(empty($id)) ? '80' : $info->opacity?>'>
            <div id='googleMapCircleOpacitySlider'></div>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->



</form>

