<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1>Create Territory</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<p class='lead'>Here you can create a new location for your company.</p>

<?php

    $attr = array
        (
            'name' => 'createForm',
            'id' => 'createForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>


<div class="form-group">
    <label class='col-lg-2 control-label' for='name'>Territory Name</label>
    <div class='col-lg-10 controls'>
        <input type='text' class='form-control' name='name' id='name' value="" placeholder='Location Name'>
    </div>
</div>

<?php if (!empty($positions)) : ?>
<div class="form-group">
    <label class='col-lg-2 control-label' for='ownerPosition'>Position Owner</label>
    <div class='col-lg-10 controls'>
        <select class='form-control' name="positionOwner" id="positionOwner">
            <option value=""></option>
<?php
    if (!empty($positions))
    {
        foreach ($positions as $r)
        {
            echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
        }
    }
?>
        </select>
    </div>
</div>
<?php endif; ?>



<div class='row'>
    <div class='col-lg-10 col-offset-2'>
    <button type='button' class='btn btn-primary' id='saveBtn' name='saveBtn'>Save</button>
    <button type='button' class='btn' id='cancelBtn' name='cancelBtn'>Cancel</button>

    </div>
</div>



</form>


