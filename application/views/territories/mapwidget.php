<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<div class='panel panel-default'>
<div class='panel-heading inline'><i class='fa fa-globe'></i> Territory Map 
    <!-- <button onclick="" class="close" type="button">&times;</button> -->
</div>
<div class='panel-body widget-body'>

<div id='widget-content'>

<?php

    echo "<div id='savedMarkers'>" . PHP_EOL;

    foreach ($territories as $r)
    {
        $markers = null;

        try
        {
            $markers = $this->territory->getMarkers($r->id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        if (!empty($markers))
        {
            foreach ($markers as $mr)
            {
                echo "<input type='hidden' lat='{$mr->lat}' lng='{$mr->lng}' radius='{$mr->radius}' color='#{$mr->color}' opacity='{$mr->opacity}'>" . PHP_EOL;
            }
        }
        
    }

    echo "</div> <!-- #savedMarkers -->" . PHP_EOL;

?>

    <div id='widgetMap'></div>

</div> <!-- #widget-content -->

</div> <!-- .widget-body -->

</div> <!-- .panel -->


<div class='clearfix'></div>
