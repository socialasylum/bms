<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-globe'></i> Territories</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='/territories/edit'><i class='fa fa-globe'></i> New Territory</a></li>
        </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<div class='col-lg-12 col-m-12 col-s-12 col-xs-12 territoryEditMap img-thumbnail' id='map'></div>

<?php
if (empty($territories)): echo $this->alerts->info("No territories have been created!");
else :


    echo "<div id='savedMarkers'>" . PHP_EOL;

    foreach ($territories as $r)
    {
        $markers = null;

        try
        {
            $markers = $this->territory->getMarkers($r->id);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        if (!empty($markers))
        {
            foreach ($markers as $mr)
            {
                echo "<input type='hidden' lat='{$mr->lat}' lng='{$mr->lng}' radius='{$mr->radius}' color='#{$mr->color}' opacity='{$mr->opacity}'>" . PHP_EOL;
            }
        }
        
    }

    echo "</div> <!-- #savedMarkers -->" . PHP_EOL;


?>
<table class='table table-striped table-condensed' id='territories'>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Position Owner</th>
            <th>Assigned Locations</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php
    foreach ($territories as $r)
    {
        $owner = $color = $locCount = null;

        try
        {
            $owner = $this->positions->getName($r->positionOwner);
            $locCount = $this->territories->getTerritoryLocationCount($r->id);

            $color = $this->territories->getMarkersColor($r->id);

            if (!empty($color)) $color = "background-color:#{$color};";

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;
        echo "\t<td>{$r->id}</td>" . PHP_EOL;
        echo "\t<td>{$r->name}</td>" . PHP_EOL;
        echo "\t<td>{$owner}</td>" . PHP_EOL;
        echo "\t<td>{$locCount}</td>" . PHP_EOL;
        echo "\t<td><button type='button' class='btn btn-default btn-xs pull-right' data-id='{$r->id}' style=\"{$color}\" name='editBtn' id='editBtn' onclick=\"territories.editTerritory(this);\"><i class='fa fa-pencil'></i></button></td>" . PHP_EOL;
        echo "</tr>" . PHP_EOL;
    }
?>

    </tbody>
</table>
<?php endif; ?>
