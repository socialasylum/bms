<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	header('Content-Type: application/json');
	
#echo "<ul id='file-tree-ul'>" . PHP_EOL;

$el = array();


if (empty($folders))
{
    $this->alerts->alert('There are no folders!');
}
else
{
    foreach ($folders as $id => $r)
    {
		
		$accountFolders = array();
		
		$editable = ($r->deletable == 1) ? 'true' : 'false';
		
        $total = $unread = 0;
        
        try
        {
			$unread = $this->msg->getFolderTotal($id, $this->session->userdata('userid'), true);
            $unreadDisplay = (empty($unread)) ? null : " ({$unread})";
            $class = (in_array(strtolower($r), array('inbox', 'junk', 'outbox', 'sent', 'trash', 'drafts'))) ? strtolower($r) . '-folder' : 'folder-folder';
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

		$el[] = array
		(
			'key' => $r,
			'name' => $r,
			'title' => $r . $unreadDisplay,
			'editable' => false,
			'extraClasses' => $class
		);
    }
	
}

	echo json_encode($el);

#echo "</ul>" . PHP_EOL;

