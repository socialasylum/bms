<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php
if (empty($info)): echo $this->alerts->info('There is no message information!');
else :
?>

<input type='hidden' id='id' value='<?=$id?>'>

<?php
	if (!empty($account)) echo "<input type='hidden' id='account' value='{$account}'>" . PHP_EOL;
?>

<?php if (isset($_GET['window'])) :
	echo "<input type='hidden' id='parsed' value='{$info->parsed}'>" . PHP_EOL;

	echo "<div class='preview-scroll msg-window'>" . PHP_EOL;
	echo "<div id='msg-preview-content'>" . PHP_EOL;

	include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
            <ul class="nav navbar-nav" id='msg-preview-actions'>
            <?php
				include_once 'actions.php';
			?>
            </ul>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php endif; ?>

<div class='well-white'>
<span class='datestamp'><?=$time?></span>

<?php
$from = str_replace(array('<', '>'), array('&lt;', '&gt;'), $from);

echo "From: <h4 class='from'>{$from}</h4>" . PHP_EOL;

$to = str_replace(array('<', '>'), array('&lt;', '&gt;'), $to);

echo "To: <div class='msg-to-row'>$to</div>" . PHP_EOL;

$subject = (!empty($info->subject)) ? "<h4 class='subject'>{$info->subject}</h4>" : "<span class='text-muted'>No Subject</span>";
?>

<?=$subject?>

<hr>

<div id='email-body'>
<?php
	if (!isset($_GET['window'])) echo $body;
?>
</div> <!-- /#email-body -->

<?php endif; ?>
</div>

<?php
if (isset($_GET['window'])) echo "</div>\n</div>"; // scroll - content