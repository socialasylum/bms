<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<!-- temporary hardcoded reference to script -->
	<script language="javascript" src="/public/js/msg.js" > </script>

<!-- <h1><i class='fa fa-edit'></i> Compose Message</h1> -->


<?php
    $attr = array
        (
            'name' => 'composeForm',
            'id' => 'composeForm',
            'class' => 'composeForm',
            'method' => 'post'
        );

echo form_open('#', $attr);
?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

<ul class="nav navbar-nav">
    <li><button type='button' id='sendBtn' class='btn btn-link navbar-btn'><i class='fa fa-envelope success'></i> Send Message</button></li>
</ul>

<ul class="nav navbar-nav pull-right">
    <li id="saveDraftTxt" style="color: green; font-weight: bold; display: none; padding-top: 6px;">Draft Saved.</li>
    <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
</ul>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<div id='msg-to-container' class='row msg-to-container' >
    <div class='col-xs-12'>
		<div id='msg-to-display'></div>
    </div>
</div> <!-- /.row -->

<div class='row' id='compose-controls'>
    <div class='col-xs-12 form-horizontal'>

        <div class="form-group">
            <label class=' col-xs-2 col-sm-1 control-label msg-to' for='sendType'>
                <select class='form-control' id='sendType' name='sendType'>
<?php
if (!empty($sendTypes))
{
    foreach  ($sendTypes as $r)
    {
        echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
                </select>
            </label>

            <div class="col-xs-10 col-sm-11">
                <div class='input-group'>
                    <input type='text' id='add' name='add' class='form-control' placeholder="">

                    <div class='input-group-btn'>
                        <button type='button' class='btn btn-primary' id='addBtn'><i class='fa fa-plus'></i> Add</button>
                    </div>
                </div>
            </div>
        </div>

<?php if (!empty($accounts)) : ?>
        <div class="form-group">
            <label class='col-xs-2 col-sm-1 control-label' for='account'>From</label>
            <div class="col-xs-10 col-sm-11 controls">
                <select name="account" id="account" class='form-control'>
                    <!-- <option value=""></option> -->
<?php
    foreach ($accounts as $r)
    {
        echo "<option value='{$r->userid}'>{$r->alias} ({$r->emailAddress})</option>" . PHP_EOL;
    }
?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->

<?php endif; ?>



        <div class="form-group">
            <label class='col-xs-2 col-sm-1 control-label' for='subject'>Subject</label>
            <div class="col-xs-10 col-sm-11">
                <input type='text' class='form-control' name='subject' id='subject' value="<?=$subject?>">
            </div>
        </div>

    </div> <!-- .span12 form-horizontal -->
</div> <!-- .row-fluid -->




<textarea id='msgBody' name='body' style="visibility:hidden;"><?=$orgmsg?></textarea>
<input type="hidden" name="msgid" id="msgid" value="0" />

</form>

