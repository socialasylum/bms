<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-cog'></i> Message Settings</h1>

<?php
$attr = array
(
    'name' => 'msgSettingsForm',
    'id' => 'msgSettingsForm',
);

echo form_open('#', $attr);
?>



<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
            <li><a href='/msg/addaccount'><i class='fa fa-plus'></i> Add Account</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<div class='tabbable'>
    <ul class='nav nav-tabs' id='userTabs'>
        <li class='active'><a href='#tabSettings' data-toggle="tab"><i class='fa fa-cog'></i> General Settings</a></li>
        <li><a href='#tabSig' data-toggle="tab"><i class='fa fa-pencil-square'></i> Signature</a></li>
        <li><a href='#tabAccounts' data-toggle="tab"><i class='fa fa-envelope-o'></i> Additional Accounts</a></li>
        <li><a href='#tabSMTP' data-toggle="tab"><i class='fa fa-upload'></i> SMTP Servers</a></li>
    </ul>

<div class="tab-content">

    <div id="tabSettings" class='tab-pane active'>
        <h2><i class='fa fa-cog'></i> General Settings</h2>
            <p class='lead'>Below are general settings for your email messaging client.</p>

            <div class='form-horizontal'>
                <div class="form-group">
                    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='syncRate'>Sync Rate (Min)</label>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 controls">
                        <input type='text' class='form-control' id='syncRate' name='syncRate' value="<?=$syncRate?>" placeholder='10'>
                        <p class='help-block'>How many minutes to check for new messages</p>
                    </div> <!-- .controls -->
                </div> <!-- .form-group -->

            </div> <!-- .form-horizontal -->

    </div> <!-- #tabSettings -->

    <div id="tabSig" class='tab-pane'>
        <h2><i class='fa fa-pencil-square'></i> Signature</h2>
        <p class='lead'>Here you can customize your message signature.</p>


<?php
if (!empty($sig->id)) echo "<input type='hidden' name='sigId' id='sigId' value='{$sig->id}'>" . PHP_EOL;
?>
        <textarea name='sig' id='sig' class='form-control' rows='5'><?=$sig->signature?></textarea>

    </div> <!-- #tabSig -->


    <div id="tabAccounts" class='tab-pane'>
        <h2><i class='fa fa-envelope-o'></i> Additional Accounts</h2>

        <p class='lead'>Below is a list of any additional e-mail accounts that have been added to your user account.</p>
<?php
if (empty($accounts))
{
    echo $this->alerts->info("No additional e-mail accounts have been added.");
}
else
{
echo <<< EOS
    <table class='table table-bordered table-hover' id='accountsTbl'>
        <thead>
            <tr>
                <th>Type</th>
                <th>E-mail Address</th>
                <th>Account Name</th>
                <th>Name</th>
                <th>Server</th>
                <th>Port</th>
                <th>Username</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($accounts as $r)
    {
        $type = null;

        try
        {
            $type = $this->functions->codeDisplay(20, $r->type);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "\t<td>{$type}</td>" . PHP_EOL;
        echo "\t<td>{$r->emailAddress}</td>" . PHP_EOL;
        echo "\t<td>{$r->alias}</td>" . PHP_EOL;
        echo "\t<td>{$r->yourname}</td>" . PHP_EOL;
        echo "\t<td>{$r->mailbox}</td>" . PHP_EOL;
        echo "\t<td>{$r->port}</td>" . PHP_EOL;
        echo "\t<td>{$r->username}</td>" . PHP_EOL;
        echo "\t<td>";
        echo "<button type='button' class='btn btn-link btn-sm pull-right' onclick=\"msg.deleteExternalAccount(this, {$r->id});\"><i class='fa fa-trash-o danger'></i></button>";                
        echo "<button type='button' class='btn btn-link btn-sm pull-right' onclick=\"msg.editAccount({$r->id}, this);\"><i class='fa fa-pencil'></i></button>";
        
        echo "</td>" . PHP_EOL;
        
        echo "</tr>" . PHP_EOL;
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>

    </div> <!-- #tabAccounts -->

    <div id="tabSMTP" class='tab-pane'>
        <h2><i class='fa fa-upload'></i> SMTP Servers</h2>
            <p class='lead'>Here you can manage your SMTP servers which are used to sending outgoing external e-mail.</p>
<?php
if (empty($smtpAccounts))
{
    echo $this->alerts->info("No SMTP Servers have been added!");
}
else
{
echo <<< EOS
    <table class='table table-bordered table-hover' id='smtpTbl'>
        <thead>
            <tr>
                <th>Type</th>
                <th>Account Name</th>
                <th>Server</th>
                <th>Port</th>
                <th>Username</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($smtpAccounts as $r)
    {
        $type = null;

        try
        {
            $type = $this->functions->codeDisplay(20, $r->type);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "\t<td>{$type}</td>" . PHP_EOL;
        echo "\t<td>{$r->alias}</td>" . PHP_EOL;
        echo "\t<td>{$r->mailbox}</td>" . PHP_EOL;
        echo "\t<td>{$r->port}</td>" . PHP_EOL;
        echo "\t<td>{$r->username}</td>" . PHP_EOL;
        echo "\t<td>";
        
        echo "<button type='button' class='btn btn-link btn-sm pull-right' onclick=\"msg.deleteExternalAccount(this, {$r->id});\"><i class='fa fa-trash-o danger'></i></button>";
        echo "<button type='button' class='btn btn-link btn-sm pull-right' onclick=\"msg.editAccount({$r->id}, this);\"><i class='fa fa-pencil'></i></button>";
        
        echo "</td>" . PHP_EOL;
        
        echo "</tr>" . PHP_EOL;
    }

    echo "</tbody>" . PHP_EOL;
    echo "</table>" . PHP_EOL;
}
?>

    </div> <!-- #tabSMTP -->
</div> <!-- .tab-content -->

</div> <!-- .tabbable -->

</form>
