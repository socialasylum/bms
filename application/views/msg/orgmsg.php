<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

    try
    {
        // gets local time msg was sent
        $date = $this->users->convertTimezone($this->session->userdata('userid'), $info->emailDate, "m/d/Y g:m A T");   
    }
    catch(Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

    echo "<p></p>";


    if (!empty($sig->signature)) echo $sig->signature;

    echo "<blockquote>
        <b>From:</b> {$from}<br>\n";

        if (!empty($sendTypes))
        {
            foreach ($sendTypes as $type)
            {
                $names = array();

                if ($type->code == 3) continue; // skips sent

                try
                {
                    if (empty($account)) $to = $this->msg->getMessageTo($id, $type->code);
                }
                catch(Exception $e)
                {
                    PHPFunctions::sendStackTrace($e);
                }

                if (!empty($to))
                {
                    echo "<b>{$type->display}:</b> " . PHP_EOL;

                    foreach ($to as $ti)
                    {
                    	$name = null;
                    	
                    	try
                    	{
	    					if (!empty($ti->to)) $name = $this->users->getName($ti->to);
							if (!empty($ti->emailAddress)) $name = $ti->emailAddress;
							
                            $names[] = "<span class='label label-default msg-to-lbl'>{$name}</span>";
                    	}
                    	catch (Exception $e)
                    	{
                    		PHPFunctions::sendStackTrace($e);
                    	}
                    }

                    echo implode(', ', $names);

                    echo "<br>" . PHP_EOL;
                }

            }
        }


    echo "<br>\n
        <b>Sent:</b> {$date}<br>\n
        <b>Subject:</b> {$info->subject}<br>\n" . $info->parsed;
    echo "</blockquote>" . PHP_EOL;
