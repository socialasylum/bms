<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>



<?php

if (empty($sendTypes))
{
    $this->alerts->alert("There are no send types available! =[ (this is not good)");
}
else
{
    foreach ($sendTypes as $r)
    {

        // only displays "None" for To. Skips the rest unless there people assigned to that send type
        if (empty($toArray[$r->code]) AND $r->code > 1) continue;

        echo "<div class='row msg-to-row'>" . PHP_EOL;

        echo "\t<div class='col-xs-2 col-sm-1 msg-to-col'>{$r->display}</div>" . PHP_EOL;


        echo "\t<div class='col-xs-10 col-sm-11' >" . PHP_EOL;

		// displays none
		if (empty($toArray[$r->code]))
		{
			echo "<span class='text-muted'>None</span>" . PHP_EOL;
		}

        foreach ($toArray[$r->code] as $key => $to)
        {
            $name = null;

            try
            {
                if (is_numeric($to)) $name = $this->users->getName($to);
                else $name = "<span class='unknownEmail'>&lt;{$to}&gt;</span>";
            }
            catch(Exception $e)
            {
                $this->functions->sendStackTrace($e);
            }

            echo "\t\t\t<span class='label label-default msg-to-lbl'>{$name}<button class='close'  type='button' onclick=\"msg.getMsgTo(undefined, {$key}, {$r->code});\">&times;</button></span>" . PHP_EOL;
        }

            echo "\t</div>" . PHP_EOL;
            echo "\t\t<div class='clearfix'></div>" . PHP_EOL;
            echo "\t</div>" . PHP_EOL; // row
        

    }


}

?>

<div class='clearfix'></div>
