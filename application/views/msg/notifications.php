<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<a href='#' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-envelope msg-not-icon'></i> <span class='label label-danger' id='menuInboxCnt'><?=$cnt?></span>  <span class="caret"></span></a>
  <ul class="dropdown-menu not-menu-container">
  	<li>
  		<div class='not-header-container'>
  			<div class='not-header'>
  				<h4>Messages</h4>
  			</div>
  		
  			<div class='not-actions'>
  				<div class='col-sm-6'>
  					<span class='bold'>Inbox</span> <span id='inboxCnt'><?=$cnt?></span>
  					&bull;
  					<span class='bold'>Outbox</span> <span id='outboxCnt'><?=$outboxCnt?></span>
  					
  				</div>

  				<div class='col-sm-6'>
  					<button type='button' onclick="global.composeMsg();" class='btn btn-primary pull-right btn-sm'><i class='fa fa-envelope'></i> New Message</button>
  				</div>
  				
  			</div>
  		</div>
  	</li>
    <!-- dropdown menu links -->
<?php
if (empty($messages))
{
    echo "<li>";
    echo $this->alerts->info("No Messages");
    echo "</li>" . PHP_EOL;
}
else
{
    foreach ($messages as $r)
    {

		$r = (object) $r;

        $time = $name = null;

		$account = 0;
        $ext = 'false';

        try
        {
        	$i = $this->msg->getMessageInfo($r->msgId);
        	
        	if (!empty($r->datestamp))
        	{
	            $time = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "g:i A");
				$date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "M j, Y");
        	}
            
			if (!empty($r->account))
			{
				$ext = 'true';
				$name = $r->emailAddress;
			}
			else $name = $this->users->getName($i->from);
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

		$unreadClass = (empty($r->read)) ? ' unread' : null;

		$dread = (empty($r->read)) ? 'false' : 'true';

		$subject = (!empty($i->subject)) ? $i->subject : "<span class='text-muted'>No Subject</span>";

        echo "<li>";

        echo <<< EOS
            <div class='notification-container{$unreadClass}' onclick="global.viewMsg(this, {$i->id})" data-read='{$dread}' data-parsed='{$i->parsed}'>
                
                    <img src='/user/profileimg/60/{$r->from}' class='img-responsive'>
                

                <div class='prev-col'>
                	<span class='date'>{$date} {$time}</span>
                	
                	<span class='from'>{$name}</span>

                    <!--<span class='time'>{$time}</span>-->
                    
                    <span class='subject'>{$subject}</span>
                
                   

                    <!--<span classs='body'>{$preview}</span>-->
                </div>

            </div> <!-- .notification-container -->
EOS;


        echo "</li>" . PHP_EOL;
    }
}

?>


    <li><button type='button' class='btn btn-link bold' onclick="global.allMsgs(this);">See All</button>
</ul>
