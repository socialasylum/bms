<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>


<div class='panel panel-default'>
<div class='panel-heading inline'><a href='/msg'><i class='fa fa-envelope'></i> Messaging</a> <button data-dismiss="alert" class="close" type="button">&times;</button></div>
<div class='panel-body widget-body'>

<div id='widget-content'>

<div class='panel-body' id='widget-content'>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='javascript:void(0);' onclick="global.composeNewMsg();"><i class='fa fa-envelope icon-white'></i> New Message</a></li>

       <li class='dropdown-submenu'>
        <a href="javascript:void(0);" onclick="" class='dropdown-toggle' data-toggle="dropdown"><i class='fa fa-folder'></i> <span class='folderName'>Inbox</span> <span class='caret'></span></a>
            <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
                <!-- <li><a href='#'>Trash</a></li> -->
<?php
    if (!empty($folders))
    {
        foreach ($folders as $r)
        {
            // does not allow to move msgs to the current folder they are already in
            if ($r->id !== $id) echo "<li><a href='javascript:void(0);' onclick=\"msg.moveMsgsToFolder({$r->id})\">{$r->name}</a></li>" . PHP_EOL;
        }
    }
?>
            </ul>
    </li>



        </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?=$msgDisplay?>
</div> <!-- .panel-body #widget-content -->


</div> <!-- .widget-body -->

</div> <!-- .panel -->




<div class='clearfix'></div>
