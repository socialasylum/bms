<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-plus'></i> Add E-mail Account</h1>
    
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>


<?php
    $attr = array
        (
            'name' => 'accountForm',
            'id' => 'accountForm',
            'class' => 'form-horizontal'
        );

    echo form_open('#', $attr);

    if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

?>

<p class='lead'><strong>Step 1:</strong> Select the type off account you wish to add.</p>


    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='type'>Type</label>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 controls">
            <select name="type" id="type" class='form-control'>
                <option value=""></option>
<?php
    if (!empty($types))
    {
        foreach ($types as $r)
        {
            $sel = ($r->code == $info->type) ? "selected='selected'" : null;

            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
        }
    }
?>
                <!-- options -->
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->



    <div id='typeSettingsContainer'>

    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='alias'>Account Name</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <input type='text' class='form-control' name='alias' id='alias' value="<?=$info->alias?>" placeholder="">
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group" id='yournameFormGroup' style='display:none;'>
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='yourname'>Your Name</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <input type='text' class='form-control' name='yourname' id='yourname' value="<?=$info->yourname?>" placeholder="John Smith">
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group" id='emailAddressFormGroup' style='display:none;'>
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='emailAddress'>E-mail Address</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <input type='text' class='form-control' name='emailAddress' id='emailAddress' value="<?=$info->emailAddress?>" placeholder="email@domain.com">
        </div> <!-- .controls -->
    </div> <!-- .form-group -->



     <div class="form-group">
         <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='mailbox'>Server</label>
         <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
             <input type='text' class='form-control' name='mailbox' id='mailbox' value="<?=$info->mailbox?>" placeholder="">
         </div> <!-- .controls -->
     </div> <!-- .form-group -->


     <div class="form-group">
         <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='port'>Port</label>
         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 controls">
             <input type='text' class='form-control' name='port' id='port' value="<?=$info->port?>" placeholder="">
         </div> <!-- .controls -->
     </div> <!-- .form-group -->

     <div class="form-group">
         <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='username'>Username</label>
         <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
             <input type='text' class='form-control' name='username' id='username' value="<?=$info->username?>" placeholder="">
         </div> <!-- .controls -->
     </div> <!-- .form-group -->


     <div class="form-group">
         <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='password'>Password</label>
         <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
             <input type='password' class='form-control' name='password' id='password' value="<?=$this->encrypt->decode($info->passwd)?>" placeholder="">
         </div> <!-- .controls -->
     </div> <!-- .form-group -->


    <div class="form-group" id='smtpServerFormGroup' style='display:none;'>
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='smtpServer'>Outgoing SMTP Server</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <select name="smtpServer" id="smtpServer" class='form-control'>
                <option value=""></option>
<?php
    if (!empty($smtpAccounts))
    {
        foreach ($smtpAccounts as $r)
        {
            $sel = ($info->outgoingServer == $r->id) ? "selected='selected'" : null;

            echo "<option {$sel} value='{$r->id}'>{$r->alias} ($r->mailbox)</option>" . PHP_EOL;
        }
    }
?>
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    </div> <!-- #typeSettingsContainer -->


</form>
