<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- <h1><i class='fa fa-envelope'></i> Messaging</h1> -->

	<input type='hidden' id='noPasswd' value="<?=json_encode($noPasswdAccounts)?>">
    <input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>
    <input type='hidden' id='token_name' value='<?=$this->security->get_csrf_token_name()?>'>

	<input type='hidden' id='trash' value='<?=$trash?>'>
	<input type='hidden' id='inbox' value='<?=$inbox?>'>
    <input type='hidden' name='syncRate' id='syncRate' value='<?=$syncRate?>'>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>

<!-- <a href='javascript:void(0);' class='navbar-brand'><i class='fa fa-envelope'></i> Messaging</a> -->

<ul class="nav navbar-nav">
<!--<li><a href='javascript:void(0);' onclick="msg.compose();"><i class='fa fa-envelope icon-white'></i> New Message</a></li> -->
    <li><a href='javascript:void(0);' id='refreshBtn'><i class='fa fa-refresh' id='refreshIcon'></i></a></li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">File <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a tabindex="-1" role='button' href='javascript:void(0);' id='createFolderBtn'><i class='fa fa-folder-open'></i> New Folder</a></li>
            <li><a tabindex="-1" role='button' href='javascript:void(0);' onclick="msg.emptyTrash();"><i class='fa fa-trash-o'></i> Empty Trash</a></li>
        </ul>
  </li>
</ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<div class='msg-container'>
    <div class='col-xs-4 col-sm-2' id='folder-tree-display'>
    
    	<div class='msg-folders'>
			<button class='btn btn-primary btn-block composeBtn' onclick='global.composeMsg();'><i class='fa fa-envelope'></i> Compose New</button>
		    
		    <div id='tree-scroll'>
		    	<div id='folder-tree'></div>
		    </div> <!-- /#tree-scroll -->
		    
    	</div> <!-- /.msg-folders -->
    </div> <!-- span3 //-->

    <div class='col-xs-8 col-sm-4' id='msg-list-display'>
		<div id='folder-content-scroll'>
			<div id='msg-folder-content'></div>
		</div>
    </div> <!-- span4 //-->

    <div class='col-xs-12 col-sm-6' id='msg-preview'>
        <nav class="navbar navbar-default subnav" role="navigation">
            <ul class="nav navbar-nav">
                <?php $this->load->view('msg/actions'); ?>
            </ul>
        </nav>
    	<div class='inner'>
			<div id='msg-preview-scroll'>
				<div id='msg-preview-content'>
				</div>
			</div>
    	</div>
    </div> <!-- span5 //-->

</div>

</div>



<div id='msgRightClickDisplay'>
<div class='dropdown'>
    <ul class='dropdown-menu'>
    <li><a tabindex='-1' role='button' href="javascript:void(0);" onclick="msg.moveMsgsToFolder(<?=$this->config->item('trash_folder')?>);"><i class='icon-trash'></i> Delete</a></li>

    <li class='divider'></li>
    <li class='dropdown-submenu'>
        <a href="javascript:void(0);" onclick="" class='dropdown-toggle' data-toggle="dropdown"><i class='icon-plus'></i> Move To</a>
            <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
                <!-- <li><a href='#'>Trash</a></li> -->
<?php
    if (!empty($folders))
    {
        foreach ($folders as $r)
        {
            // does not allow to move msgs to the current folder they are already in
            if ($r->id !== $id) echo "<li><a href='javascript:void(0);' onclick=\"msg.moveMsgsToFolder({$r->id})\">{$r->name}</a></li>" . PHP_EOL;
        }
    }
?>
            </ul>
    </li>
    </ul>
</div>


<?php
/* // old create folder modal
<!-- new folder modal //-->
<div id='createFolderModal' class='modal fade' data-backdrop=''>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3>Create a New Folder</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='folderAlert'></div>

        <p class='lead'><img src='/public/images/windows_folder_icon.png'> Enter the name of your new folder</p>

        <?php
        //<form name='createFolderForm' id='createFolderForm'>

        $attr = array
        (
            'name' => 'createFolderForm',
            'id' => 'createFolderForm',
        );

        echo form_open('#', $attr);
        ?>
        <input type='hidden' name='parentFolder' value='<?=$folder?>'>
        <p><input type='text' class='input-block-level' name='folderName' id='folderName' placeholder='Folder Name'></p>
        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='createFolderBtn'>Create Folder</button>
    </div> <!-- .modal-footer //-->

</div>
*/
?>
