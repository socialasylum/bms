<?php if(!defined('BASEPATH')) die('Direct access not allowed');

header('Content-Type: application/json');

$el = array();

$el[] = array
	(
		'name' => 'Delete',
		'action' => 'delete',
		'iconClass' => 'fa fa-trash-o'
	);

	if (!empty($folders))
	{
		$c = array();
				
		foreach ($folders as $r)
		{
			$class = 'fa fa-folder';

			if ($r->inbox > 0) $class='fa fa-inbox';
			if ($r->junk > 0) $class='fa fa-bomb';
			if ($r->outbox > 0) $class='fa fa-pencil-square-o';
			if ($r->drafts > 0) $class='fa fa-pencil-square-o';
			if ($r->sent > 0) $class='fa fa-rocket';
			if ($r->trash > 0) $class='fa fa-trash-o';
			
			$c[] = array
			(
				'name' => $r->name,
				'action' => 'folder',
				'id' => $r->id,
				'iconClass' => $class
			);
		}


		$el[] = array
			(
				'name' => 'Move To',
				'action' => 'move',
				'iconClass' => 'fa fa-folder',
				'children' => $c
			);
	}

	
echo json_encode($el);