<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (empty($messages)) {
    //echo "<alert data-type='info' data-msg='This folder is empty' data-options='{\"clearTimeoutSeconds\":\"0\", \"zindex\":1}'></alert>" . EOL;
} else {
    $prevDate = null;

    foreach ($messages as $k => $r) {
        if ($r->deleted == 0)
        {
            $blockClass = $msgId = null;

            // ensures element is an object	
            $r = (object) $r;

            $time = null;

            $account = 0;
            $ext = 'false';

            try {
                // gets messsage info
                //$i = $this->msg->getMessageInfo($r->msgId);

                if (!empty($r->udate)) {
                    $time = $this->users->convertTimezone($this->session->userdata('userid'), $r->udate, "g:i A");
                    $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->udate, "M j, Y");
                }

                $ext = 'true';
                $name = $r->from;
                
            } catch (Exception $e) {
                PHPFunctions::sendStackTrace($e);
            }

            if ($date !== $prevDate) {
                echo "<div class='well msg-date-header'><h5>{$date}</h5></div>" . EOL . PHP_EOL;
            }

            $class = ($r->seen == 0) ? 'unread' : null;

            if ($r->seen == 0) {
                $blockClass = "unread";
            }

            $subject = (!empty($r->subject)) ? imap_mime_header_decode($r->subject)[0]->text : "<span class='text-muted'>No Subject</span>";

            echo "<blockquote class='msg {$blockClass}' data-id='{$r->msgno}' data-extMsgID='{$r->extMsgID}' read='{$r->seen}' value='{$r->msgno}' data-external='{$ext}' data-account='{$r->account}' data-parsed='{$r->parsed}' id='msg_{$r->msgno}'>" . PHP_EOL;

            echo "\t<div class='col-xs-8'>";

            echo "<h4>{$name}</h4>" . PHP_EOL;

            echo "\t<p id='subject{$r->msgno}' class='{$class}'>{$subject}</p>";

            echo "</div>" . PHP_EOL; // .span12

            echo "\t<div class='col-xs-4'><span class='pull-right'>{$date} {$time}</span></div>" . PHP_EOL;

            echo "\t<div class='clearfix'></div>" . PHP_EOL;


            echo "</blockquote>" . PHP_EOL . PHP_EOL;

            $prevDate = $date;
        }
    }
}
