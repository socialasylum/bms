<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php
	$style = (isset($_GET['window'])) ? null : "display:none;";
?>

<li><button type='button' id='replyBtn' class='btn btn-link' onclick='msg.reply();' style='<?=$style?>'><i class='fa fa-mail-reply'></i> Reply</button></li>
<li><button type='button' id='forwardBtn' class='btn btn-link' onclick='msg.forward();' style='<?=$style?>'><i class='fa fa-mail-forward'></i> Forward</button></li>