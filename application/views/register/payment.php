    <h1>Billing Information <small>for <?=$userName?></h1>
    <hr>

<?php

    $userName = $this->users->getName($userid);

    $attr = array
        (
            'name' => 'billingInfo',
            'id' => 'billingInfo',
            'class' => 'form-horizontal',
            'method' => 'POST'
        );

    echo form_open('/register/saveBilling', $attr);

    $rcnt = 1;

    foreach ($plans as $r)
    {
        if ($rcnt == 1) echo "<div class='row'>";

        echo "<div class='col-lg-4 col-m-4 col-s-4 col-xs-4'>";
        echo "<h2 class='title'>{$r->name}</h2>";
        echo "<div class='radio'>";

        echo "<label>";

        echo "<input type='radio' name='packages' id='packages_{$r->id}' value='{$r->id}'>";
        echo "<p>Select Package</p>";

        echo "</label>";

        echo "</div> <!-- /.radio -->";
        echo "</div> <!-- /.col4 -->";
        if ($rcnt >= 3)
        {
            echo "</div>";
            $rcnt = 1;
        }
        else
        {
            $rcnt++;
        }
    }

    if ($rcnt <= 3 && $rcnt > 1) echo "</div>";
?>

    <div class='row package'>
    </div>

<input type='hidden' name='userid' id='userid' value='<?=$userid?>'>
<input type='hidden' name='BTcustomerID' id='BTcustomerID' value='<?=$userid?>'>

<div class='row'>
    <div class='col-lg-8 col-m-8 col-s-8 col-xs-8'>

            <div class='panel panel-default'>
                <div class='panel-heading'>
                <h3 class='panel-title'><i class='fa fa-credit-card'></i> New Card Information</h3>
                </div> <!-- .panel-heading -->

                <div class='panel-body'>
    <!-- <h3>New Card Information</h3> -->
    <!-- <hr> -->

<div class="form-group">
    <label class='col-lg-3 control-label' for='ccName'>Name on Card</label>
    <div class="col-lg-9">
        <input type='text' class='form-control' name='ccName' id='ccName' value="" placeholder='<?=$userName?>'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-3 control-label' for='ccNumber'>Card Number</label>
    <div class="col-lg-9">
        <input type='text' class='form-control' name='ccNumber' id='ccNumber' value="" placeholder='0000-1234-5678-9999'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccExp'>Card Expiration</label>
    <div class="col-lg-9">
        <select name="ccExpM" id="ccExpM" class='input-small form-control' style="width:100px;float:left;">
            <option value="">MM</option>
<?php
for ($i = 1; $i <= 12; $i++)
{

    $display = str_pad($i, 2, '0', STR_PAD_LEFT);

    echo "<option value='{$display}'>{$display}</option>" . PHP_EOL;
}
?>
        </select>
 <div style='float:left; font-size:24px;margin:0 10px;'>/</div>
            <select name="ccExpY" id="ccExpY" class='input-small form-control' style="width:100px;float:left;">
                <option value="">YYYY</option>
<?php
for ($i = (int) date("Y"); $i <= ((int) date("Y") + 10); $i++) 
{
    echo "<option value='{$i}'>{$i}</option>" . PHP_EOL;
}
?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccCvv'>CVV [?]</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='ccCvv' id='ccCvv' value="" placeholder='000'>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <label class='checkbox'>
            <input type='hidden' name='agree' value='0'>
            <input type='checkbox' name='agree' id='agree' value='1'> By clicking this checkbox you agree to the sites Terms of Service, Privacy Policy, and Return &amp; Refunds Policy.
        </label>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

<!-- </fieldset> -->
                </div> <!-- .panel-body -->

            </div> <!-- .panel -->


<button type='button' id='submitBtn' name='submitBtn' class='btn btn-primary'>Submit</button>
    </div> <!-- col-8 -->
    <div class='col-lg-4 col-m-4 col-s-4 col-xs-4'>


            <div class='panel panel-default'>
                <div class='panel-heading'>
                <h3 class='panel-title'><i class='fa fa-credit-card'></i> Card on File</h3>
                </div> <!-- .panel-heading -->

                <div class='panel-body'>
<?php
if (empty($cards))
{
        echo $this->alerts->info("You currently have no cards on file.");
}
else
{
echo <<< EOS
    <table id='cardTbl' class='table table-stripped'>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Type</th>
                <th>Last 4 Digits</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($cards as $r)
    {
        $typeDisplay = null;

        try
        {
            $typeDisplay = $this->functions->codeDisplay(18, $r->cardType);
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "<td><label class='radio'><input type='radio' name='previousCard' id='previousCard_{$r->id}' value='{$r->id}'></label></td>";
        echo "<td>{$typeDisplay}</td>";
        echo "<td>{$r->lastFour}</td>";

        echo "</tr>" . PHP_EOL;
    }


echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>


                </div> <!-- .panel-body -->

            </div> <!-- .panel -->

    </div> <!-- col-4 -->

</div> <!-- .row -->

</form>
