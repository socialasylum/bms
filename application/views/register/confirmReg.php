<div class='row'>
    <div class='col-sm-12'>
        <center><h1 class='title'><?=$userName?> would like to join <?=$location?> under the <strong><?=$industry?></strong> industry.</h1></center>
        <p>Please ensure there isn't currently a member under this industry for this location before approving. By approving, an email will be sent to this user with info to fill out their billing information. Once the billing info is filled out, their account will be switched to live and their public profile will appear on the promoteleads.com</p>
        <button class='btn btn-success' name='approveBtn' id='approveBtn' value='<?=$userid?>'>Approved!!</button>
        <button class='btn btn-danger pull-right' name='denyBtn' id='denyBtn' value='<?=$userid?>'>Denied!</button>
    </div> <!-- /.col12 -->
</div> <!-- /.row -->
