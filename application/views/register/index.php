<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<div class='col-lg-8'>
<h1><i class='fa fa-user'></i> Register</h1>

<p class='lead'>Sign up and become a member of <strong><?=$companyName?></strong>.</p>

<?php

    $attr = array
        (
            'name' => 'regForm',
            'id' => 'regForm',
        );

echo form_open('#', $attr);

if (!empty($type)) echo "<input type='hidden' name='accountType' id='accountType' value='{$type}'>" . EOL;
?>

    <input type='hidden' name='company' id='company' value='<?=$company?>'>
<?php

    if (!empty($defaultPosition)) echo "<input type='hidden' name='position' id='position' value='{$defaultPosition}'>" . PHP_EOL;
    if (!empty($defaultDepartment)) echo "<input type='hidden' name='department' id='department' value='{$defaultDepartment}'>" . PHP_EOL;

    if (!empty($utm_campaign))
    {
        echo "\n\n<input type='hidden' name='utm_campaign' id='utm_campaign' value=\"{$utm_campaign}\">" . PHP_EOL;
    }

?>

<div class='form-horizontal'>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyName'>Company Name</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='companyName' id='companyName' value="" placeholder=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='firstName'>First Name</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='firstName' id='firstName' value="" placeholder=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='lastName'>Last Name</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='lastName' id='lastName' value="" placeholder=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='email'>E-mail</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='email' id='email' value="" placeholder=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="password">Password</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
        <input type="password" class='form-control' name="password" id="password" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
    </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="passwordConf">Confirm Password</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
        <input type="password" class='form-control' name="confirmPassword" id="confirmPassword" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">
        </div>
    </div>

<?php if ((int) $company !== 35) : ?>
    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='location'>Location</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <select name="location" id="location" class='form-control'>
                <option value=""></option>
<?php
if (!empty($locations))
{
    foreach ($locations as $r)
    {
        echo "<option value='{$r->id}'>{$r->name}</option>\n";
    }
}
?>
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->
<?php endif; ?>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='industry'>Industry</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <select name="industry" id="industry" class='form-control'>
                <option value=""></option> <!-- options -->
<?php
if (!empty($industry))
{
    foreach ($industry as $r)
    {
        echo "<option value='{$r->code}'>{$r->display}</option>\n";
    }
}
?>
                <option value="0">Other</option>
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group" id='otherIndustryFormGroup' style="display:none;">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='industryOther'>Other Industry</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='industryOther' id='industryOther' value="" placeholder=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <hr>

    <h3><i class='fa fa-building'></i> Address Information</h3>

    <div class="form-group">
        <label class="col-lg-3 control-label" for="address">Address</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="address" id="address" placeholder="123 Main St.">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="address2">Address 2</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="address2" id="address2" placeholder="Suite #101">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="city">City</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="city" id="city" placeholder="Las Vegas">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="state">State</label>
        <div class="col-lg-9">
             <select name='state' id='state' class='form-control'>
                <option value=''></option>
<?php
if (!empty($states))
{
    foreach ($states as $abb => $stateName)
    {
        echo "<option value='{$abb}'>{$stateName}</option>\n";
    }
}
?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label" for="postalCode">Postal Code</label>
        <div class="col-lg-9">
        <input type='text' class='form-control' name="postalCode" id="postalCode" placeholder="89011">
        </div>
    </div>
<?php
if($company == 6)
{

}
else
{

    echo "<fieldset>";

    if ($showBilling == true)
    {
        echo "<legend>Billing Information</legend>";

    if ($utm_campaign == 'facebookft') $specialTxt = " <span class='specialTxt'>(Billed after 14 Day free trial period)</span> ";

echo <<< EOS
    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='priceLbl'>Price</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <label class='priceLbl'>$9<span class='cents'>.99</span> / Month{$specialTxt}</label>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

EOS;

    echo "<div class='form-group'>";
        echo "<label class='col-lg-3 control-label' for='ccName'>Name on Card</label>";
        echo "<div class='col-lg-9'>";
            echo "<input type='text' class='form-control' name='ccName' id='ccName' value='' placeholder='John Smith'>";
        echo "</div>";
    echo "</div>";

    echo "<div class='form-group'>";
        echo "<label class='col-lg-3 control-label' for='ccNumber'>Card Number</label>";
        echo "<div class='col-lg-9'>";
            echo "<input type='text' class='form-control' name='ccNumber' id='ccNumber' value='' placeholder='0000-1234-5678-9999'>";
        echo "</div>";
    echo "</div>";

    echo "<div class='form-group'>";
        echo "<label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccExp'>Card Expiration</label>";
        echo "<div class='col-lg-9'>";
            echo "<select name='ccExpM' id='ccExpM' class='input-small form-control' style='width:100px;float:left;'>";
                echo "<option value=''>MM</option>";

for ($i = 1; $i <= 12; $i++)
{

    $display = str_pad($i, 2, '0', STR_PAD_LEFT);

    echo "<option value='{$display}'>{$display}</option>" . PHP_EOL;
}

            echo "</select>";
 echo "<div style='float:left; font-size:24px;margin:0 10px;'>/</div>";
            echo "<select name='ccExpY' id='ccExpY' class='input-small form-control' style='width:100px;float:left;'>";
                echo "<option value=''>YYYY</option>";

for ($i = (int) date("Y"); $i <= ((int) date("Y") + 10); $i++) 
{
    echo "<option value='{$i}'>{$i}</option>" . PHP_EOL;
}

            echo "</select>";
        echo "</div>";
    echo "</div>";


    echo "<div class='form-group'>";
        echo "<label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccCvv'>CVV [?]</label>";
        echo "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 controls'>";
            echo "<input type='text' class='form-control' name='ccCvv' id='ccCvv' value='' placeholder='000'>";
        echo "</div> <!-- .controls -->";
    echo "</div> <!-- .form-group -->";

/*
    echo "<div class='form-group'>";
        echo "<label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='billingZip'>Billing Postal Code</label>";
        echo "<div class='col-lg-9'>";
            echo "<input type='text' class='form-control' name='billingZip' id='billingZip' class='input-medium' value='' placeholder='89011'>";
        echo "</div>";
    echo "</div>";
 */
    }
}
?>
<?php
if($company == 6)
{

}
else
{
    if ($showBilling == true)
    {
        echo "<div class='form-group'>";
            echo "<label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>";
            echo "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 controls'>";
                echo "<label class='checkbox'>";
                    echo "<input type='hidden' name='agree' value='0'>";
                    echo "<input type='checkbox' name='agree' checked='checked' id='agree' value='1'> Billing Address the same as address above.";
                echo "</label>";
            echo "</div> <!-- .controls -->";
        echo "</div> <!-- .form-group -->";
    }
}
?>


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='hidden' name='agree' value='0'>

                <label class='checkbox cursorPointer' for='agree'>
                <input type='checkbox' name='agree' id='agree' value='1'> By clicking this checkbox you agree to the sites Terms of Service, Privacy Policy, and Return &amp; Refunds Policy.
            </label>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->





    </fieldset>


</div> <!-- .form-horizontal -->


<hr>

<button type='button' id='submitBtn' class='btn btn-primary'>Submit</button>
<button type='button' id='resetFormBtn' class='btn btn-default'>Reset</button>



</form>


</div> <!-- col-lg-8 -->

<div class='col-lg-4'>

<div class='panel panel-default'>
    <div class='panel-heading'><strong>Becoming a member</strong></div>

    <div class='panel-body'>

        <?php 
            if(!empty($logo))
            {
                echo "<div align='center' class='regLogo'>";
                    echo "<img class='img-responsive' src='/public/uploads/logos/{$company}/{$logo}'>";
                echo "</div>";
            }
            else
            {
                echo "<h2 class='regH2'>{$companyName}</h2>";
            }

            if (!empty($becomeMemberTxt)) echo "<hr>{$becomeMemberTxt}";
        ?>

    </div> <!-- .panel-body -->
</div> <!-- .panel -->

<?php if ($allowFBreg > 0) : ?>
<hr>

<a href='javascript:void(0);' onclick="fb.login(this, true, false);"><img src='/public/images/regFBbtn.png' class='img-responsive facebookReg'></a>
<?php endif; ?>



<?php if ($utm_campaign == 'facebookft') : ?>

<hr>
<div align='center'>
<img src='/public/images/trialsquare.png' alt='14 Free Trial!'>
</div>

<?php endif; ?>



</div>
