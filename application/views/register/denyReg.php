<div class='row'>
    <div class='col-sm-12'>
        <center><h1 class='title'>You have decided to deny <?=$userName?></h1></center>
<?php
    $attr = array(
        'name' => 'denyReason',
        'id' => 'denyReason',
        'method' => 'POST',
        'class' => 'form-horizontal'
    );

    echo form_open('/register/denyReg', $attr);
?>
          <div class="form-group">
            <label for="reason" class="col-sm-2 control-label">Reason:</label>
            <div class="col-sm-10">
              <textarea class="form-control" rows="4" name="reason" id="reason" placeholder="Please explain why you have denied the user and add any suggestions here (choose different category, etc)"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary" name="submitBtn" id="submitBtn">Submit</button>
            </div>
          </div>
        </form>
    </div>
</div>
