<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='jumbotron'>
    <h1>Thank you for registering!</h1>
    <p>Thank you for registering to <?=$companyName?>.</p>
</div>

<hr>
<h3>You have successfully completed your inital user registration. 
Below you can enter more information to complete your user profile.</h4>

<?php

    $attr = array
        (
            'name' => 'regForm',
            'id' => 'regForm'
        );

echo form_open_multipart('/register/completereg', $attr);
?>

<input type='hidden' name='company' id='company' value='<?=$company?>'>

<div class='form-horizontal'>
    <h2><i class='fa fa-rss'></i> Subscription</h2>

    <p class='lead'>Below you can select your subscription and enter your billing information. Without a subscription, you may not able to take full advantage of the business management system.</p>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='ccName'>Name on Card</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='ccName' id='ccName' value="" placeholder='John Smith'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='ccNumber'>Card Number</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='ccNumber' id='ccNumber' value="" placeholder='0000-1234-5678-9999'>
        </div>
    </div>

    <div class="form-group">
        <label class='col-lg-3 control-label' for='ccExp'>Card Expiration</label>
        <div class="col-lg-9">
            <select name="ccExpM" id="ccExpM" class='input-small form-control' style="width:100px;float:left;">
                <option value="">MM</option>
                <!-- options -->
            </select>
 <div style='float:left; font-size:24px;margin:0 10px;'>/</div>
            <select name="ccExpY" id="ccExpY" class='input-small form-control' style="width:100px;float:left;">
                <option value="">YYYY</option>
                <!-- options -->
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class='col-lg-3 control-label' for='billingZip'>Billing Postal Code</label>
        <div class="col-lg-9">
            <input type='text' class='form-control' name='billingZip' id='billingZip' class='input-medium' value="" placeholder='89011'>
        </div>
    </div>



<hr>

    <h2><i class='fa fa-group'></i> Social Media</h2>


    <p class='lead'>Enter any social media links you wish to be associated with your user profile.</p>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='facebookUrl'><i class='fa fa-facebook-square'></i> Facebook</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' name='facebookUrl' id='facebookUrl' class='form-control' value="" placeholder='http://facebook.com'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='twitterUrl'><i class='fa fa-twitter-square'></i> Twitter</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' name='twitterUrl' id='twitterUrl' class='form-control' value="" placeholder='http://twitter.com'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='linkedInUrl'><i class='fa fa-linkedin-square'></i> LinkedIn</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' name='linkedInUrl' id='linkedInUrl' class='form-control' value="" placeholder='http://linkedIn.com'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='youtubeUrl'><i class='fa fa-youtube-square'></i> Youtube</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' name='youtubeUrl' id='youtubeUrl' class='form-control' value="" placeholder='http://youtube.com'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='profileImg'><i class='fa fa-user'></i> Profile Image</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='file' name='profileImg' class=''>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->



</div> <!-- .form-horizontal -->




<hr>

    <button type='button' id='submitBtn' class='btn btn-primary'><i class='fa fa-sign-in'></i> Continue &amp; Login</button>


</form>
