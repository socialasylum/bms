<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="modal fade photo-modal" id='photo-modal'>
  <div class="modal-dialog">
    <div class="modal-content">
   
      <div class="modal-body">
      
      	<input type='hidden' id='modalPhotoID' value=''>
      
		  <div class='row photo-content'>
		  	<div class='col-xs-6 col-sm-8 col-md-9 img-bg'>
		  		<img id='photo-img' class='img-responsive' src=''>
		  		
		  		<div class='photo-actions' style="opacity:0;">
		  			<button type='button' class='btn btn-link pull-left' id='albumBtn'></button>

		  			<div class='dropdown'>
			  			<button type='button' class='btn btn-link pull-right' id='photoSettingsBtn' data-toggle="dropdown"><i class='fa fa-cog'></i> <span class="caret"></span></button>
				  			<ul class="dropdown-menu photo-settings" aria-labelledby="photoSettingsBtn">
					  			<li><a href="javascript:void(0);" id='photoDLLink' target='_blank'><i class='fa fa-download'></i> Download</a></li>
					  			<li><a href="#"><i class='fa fa-user'></i> Make Default</a></li>
					  			<li><a href="#"><i class='fa fa-share-alt'></i> Share</a></li>
				  			</ul>
		  			</div> <!-- /.dropdown -->
		  		</div> <!-- /.photo-actions -->

	  			<div class='actions-bg' style="opacity:0;"></div>
		  		
		  	</div>
		  	
		  	<div class='col-xs-6 col-sm-4 col-md-3 info-col'>
		  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		  		<div id='photo-info-display'></div>
		  		
		  	</div> 
		  </div> <!-- /.row -->
      </div> <!-- /.model-body -->
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
