<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>

<?php

$attr = array
    (
        'name' => 'blogForm',
        'id' => 'blogForm'
    );

echo form_open_multipart('/blog/save', $attr);

if (!empty($id)) echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
?>

    <input type='hidden' name='page' id='page' value='<?=$page?>'>
    <input type='hidden' name='category' id='category' value='<?=(empty($id)) ? '0' : $info->category?>'>

    <h1><i class='fa fa-edit'></i> <?=(empty($id)) ? 'Create' : 'Edit' ?> Blog</h1>
<div class='row'>

    <div class='col-lg-9'>

        <div class='form-horizontal'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
            <li><a tabindex="-1" role='button' href="#blogSettingsModal" data-toggle='modal'><i class='fa fa-cog'></i> Settings</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>



<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<div class="form-group announce-subject">
    <label class='col-lg-1 control-label' for='title'>Title</label>
    <div class="col-lg-11">
        <input type='text' name='title' id='title' value="<?=$info->title?>" class='form-control' placeholder='Title'>
    </div>
    </div>

<div class="form-group">
    <label class='col-lg-1 control-label' for='body'>Body</label>
    <div class="col-lg-11">
        <textarea name='body' id='body' class='form-control input-block-level' rows='5'><?=$info->body?></textarea>
    </div>
</div>

<hr>

<div class="form-group">
    <label class='col-lg-1 control-label' for='excerpt'>Excerpt</label>
    <div class="col-lg-11">
        <textarea name='excerpt' id='excerpt' class='form-control input-block-level' rows='5'><?=$info->excerpt?></textarea>
    </div>
</div>

<hr>

<div class="form-group">
    <label for="" class="col-sm-1 control-label">Featured Image</label>
	<div class="col-sm-11">
		<?php
			if (empty($info->featuredImg))
			{
				echo $this->alerts->info("This blog currently does not have a featured image!");
			}
			else
			{
				echo "<img src='/public/uploads/blog/{$info->featuredImg}' class='img-responsive'>" . PHP_EOL;
			
				echo "<input type='hidden' name='currentFeaturedImg' id='currentFeaturedImg' value=\"{$info->featuredImg}\">" . PHP_EOL;
			}
		?>
		<input type='file' name='featuredImg' id='featuredImg'>
    </div> <!-- col-9 -->
</div> <!-- .form-group -->

<hr>

	<div class="form-group">
	    <label for="" class="col-sm-1 control-label">Comments</label>
		<div class="col-sm-11">
			<?php
			if (empty($comments))
			{
				echo $this->alerts->info("This blog has no comments!");
			}
			else
			{
				
			}
			?>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


        </div> <!-- /.form-horizontal -->
    </div> <!-- /.col-lg-9 -->
    <div class='col-lg-3'>

    <div class='panel panel-default'>
        <div class='panel-heading'><h3 class='panel-title'><i class='icon-cog'></i> Settings</h3></div>

        <div class='panel-body'>
        <div class='form-horizontal'>
            <div class="form-group">
                <label class='col-lg-3 control-label' for='status'>Status</label>
                <div class="col-lg-9">

            <select class='form-control textinput' name='active' id='active'>
            <?php
                foreach($status as $code => $display)
                {
                    // new blogs, sets to active by default
                    if (empty($id)) $sel = ($code == 1) ? 'selected' : null;
                    else $sel = ($info->active == $code) ? 'selected' : null;

                    echo "<option {$sel} value='{$code}'>{$display}</option>\n";
                }
            ?>
            </select>

                </div>
            </div>


        <div class="form-group">
            <label class='col-lg-3 control-label' for='publishDate'>Publish Date</label>
            <div class="col-lg-9">
                <input class='form-control' type='text' name='publishDate' id='publishDate' value="<?=(empty($id)) ? null : $publishDate?>" placeholder='<?=date("Y-m-d")?>'>
            </div>
        </div>


        <div class="form-group">
            <label class='col-lg-3 control-label' for='user'>User</label>
            <div class="col-lg-9">
                <select class='form-control' name="user" id="user">
    <?php
                if (!empty($users))
                {
                    foreach ($users as $r)
                    {
                    	// skips user if they are not assigned to this company
                    	if (!$this->companies->userAssignedToCompany($r->id, $this->session->userdata('company'))) continue;
                    
                        if (empty($id)) $sel = ($r->id == $this->session->userdata('userid')) ? 'selected' : null;
                        else $sel = ($info->userid == $r->id) ? 'selected' : null;

                        echo "<option {$sel} value='{$r->id}'>{$r->firstName} {$r->lastName}</option>" . PHP_EOL;
                    }
                }
    ?>
                </select>
            </div>
        </div>
    </div> <!-- /.form-horizontal -->
        </div> <!-- .panel-body -->
    </div> <!-- /.panel -->

        <div class='panel panel-default'>
            <div class='panel-heading'><h3 class='panel-title'><i class='icon-tag'></i> Tags</h3></div>

            <div class='panel-body'>

                <div id='tagAlert'></div>

                <div class='input-group'>

                <input type='text' class='form-control' name='tag' id='tag' value="" placeholder='Books'>
                    <span class='input-group-btn'>
                        <button type='button' class='btn btn-primary' id='addTagBtn'><i class='fa fa-plus'></i></button>
                    </span>
                </div> <!-- .input-group -->

                <div id='tagDisplay'></div>

                <div id='tagInputContainer' style="display:none;">
                <?php
                if (!empty($id) && !empty($tags))
                {
                    foreach ($tags as $r)
                    {
                        echo "<input type='hidden' name='tag[]' id='tag' value=\"{$r->tag}\">" . PHP_EOL;
                    }
                }

?>
                </div> <!-- #tagInputContainer -->


            </div> <!-- .panel-body -->
        </div> <!-- .panel -->


        <div class='panel panel-default'>
            <div class='panel-heading'><h3 class='panel-title'><i class='icon-list'></i> Categories</h3></div>

            <div class='panel-body' id='folderTree'>
            </div> <!-- .panel-body -->
        </div> <!-- .panel -->
</div> <!-- col-lg-3 -->


    </div> <!-- .row -->



<!-- settings modal -->
<div id='blogSettingsModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <h4 class='modal-title'><i class='fa fa-cog'></i> Blog Settings</h4>
            </div> <!-- .modal-header //-->

            <div class='modal-body form-horizontal'>
                <div id='blogSettingsAlert'></div>

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='allowComments'>Allow Comments</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <select name="allowComments" id="allowComments" class='form-control'>
                    <?php
                    if (!empty($allowComments))
                    {
                        foreach ($allowComments as $k => $display)
                        {
                            // new blogs, sets to allow comments by default
                            if (empty($id)) $sel = ($k == 1) ? 'selected' : null;
                            else $sel = ($info->allowComments == $k) ? 'selected' : null;

                            echo "<option {$sel} value='{$k}'>{$display}</option>" . PHP_EOL;
                        }
                    }
                    ?>
                    </select>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->


            </div> <!-- .modal-body //-->

            <div class='modal-footer'>
                <button class='btn btn-info' data-dismiss='modal' aria-hidden='true'>Save &amp; Close</button>
            </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->





</form>
