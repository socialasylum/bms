<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<input type='hidden' id='token' value='<?=$this->security->get_csrf_hash()?>'>



<div class='blog-body well-white'>
	
<h1><?=$info->title?></h1>
        
<?=$info->body?>
</div>

<div class='row'>
    <div class='col-lg-12'>
        <?php if (strtoupper($namespace) == 'ARTICLES') : ?>
            <button type='button' class='btn btn-default' id='returnBtn'><i class='fa fa-arrow-left'></i> Return to Articles</button>
        <?php else: ?>
            <button type='button' class='btn btn-default' id='returnBtn'><i class='fa fa-arrow-left'></i> Return to Blogs</button>
        <?php endif; ?>
        <!-- <a href='/<?=$this->router->fetch_class()?>' class='btn btn-default'>&laquo; Return to blog</a> -->
    </div>
</div>


<?php if ($info->allowComments == 1) : ?>

<hr>

<?php

$attr = array
    (
        'name' => 'commentForm',
        'id' => 'commentForm'
    );

echo form_open('#', $attr);

echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;
?>

    <input type='hidden' name='page' id='page' value='<?=$page?>'>
    <input type='hidden' name='userid' id='userid' value='<?=($this->session->userdata('logged_in') == true) ? $this->session->userdata('userid') : 0?>'>

<?php
if (empty($comments))
{
    echo $this->alerts->info("This post has no comments.");
}
else
{
    echo '<h2>Comments</h2>';

    foreach ($comments as $r)
    {
        $comment = $name = $date = $src = null;
        try
        {
            $comment = $this->blog->getCommentBody($r->id);

            $name = (empty($r->userid)) ? $r->name : $this->users->getName($r->userid);

            $src = (empty($r->userid)) ? "/genimg/render/130?img=".urlencode('dude.gif').'&path='.urlencode('../images/') : "/user/profileimg/130/{$r->userid}";

            if ($this->router->fetch_class() == 'blog')
            {
                $user = $this->session->userdata('userid');
            }
            else
            {
                if (empty($r->userid)) $user = 1; // patch for now
                else $user = $r->userid;
            }

            $date = $this->users->convertTimezone($user, $r->datestamp, "Y-m-d");

        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo <<< EOS

    <div class='panel panel-default'>
        <div class='panel-heading inline'><strong>{$name}</strong> <span class='pull-right'>{$date}</span></div>
EOS;

    echo "<div class='panel-body'>" . PHP_EOL;

    echo "<dl class='dl-horizontal'>";

    echo "<dt><img src=\"{$src}\" class='blog-preview-img img-thumbnail'></dt>";

    echo "<dd>";
    echo "<blockquote>{$comment}</blockquote>";
    echo "</dd>";

    echo "</dl>";

    echo "<div class='clearfix'></div>";

    echo "</div>"; // panel-body

    if ($editComments == true)
    {
        echo "<div class='panel-footer'>" . PHP_EOL;

        echo "<button type='button' class='btn btn-danger' onclick=\"blog.deleteComment({$r->id});\"><i class='fa fa-trash-o'></i> Delete</button>" . PHP_EOL;
        echo "<button type='button' class='btn btn-warning' onclick=\"blog.setSpamComment({$r->id});\"><i class='fa fa-ban'></i> Spam</button>" . PHP_EOL;

        echo "</div> <!-- .panel-footer -->" . PHP_EOL;
    }

    echo "</div>"; // panel

    }
}
?>

<hr>

<div class='form-horizontal'>
<h2><i class='fa fa-comment'></i> Leave a comment</h2>

<p class='lead'>Please complete the form below to submit a comment.</p>

<div id='commentAlert'></div>

<?php

try
{
    if ($this->session->userdata('logged_in') == true)
    {
        $name = $this->users->getName($this->session->userdata('userid'));
        $email = $this->users->getTableValue('email', $this->session->userdata('userid'));

        $comDis = "disabled='disabled'";
    }
}
catch(Exception $e)
{
    $this->functions->sendStackTrace($e);
}

/*
<h3><i class='fa fa-user'></i> <?=$this->users->getName($this->session->userdata('userid'))?></h3>
 */
?>

    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='name'>Name</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <input type='text' class='form-control' name='name' id='name' value="<?=$name?>" placeholder='Dwayne Smith' <?=$comDis?>>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='email'>E-mail</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <input type='text' class='form-control' name='email' id='email' value="<?=$email?>" placeholder='email@domain.com' <?=$comDis?>>
            <span class='help-block'><strong>*Will not be published</strong></span>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-2 col-md-2 col-sm-2 col-xs-2 control-label' for='comment'>Comment</label>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 controls">
            <textarea name='comment' id='comment' class='form-control' rows='5'></textarea>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->





<hr>

<button type='button' class='btn btn-primary' id='submitCmtBtn'>Submit Comment</button>


</div> <!-- .form-horizontal -->
</form>

<?php
endif; // end of allow comments if
?>



