<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h1><i class='fa fa-quote-left'></i> Blog</h1>

    <input type='hidden' name='page' id='page' value='<?=$page?>'>

<?php if ($editBlog == true) : ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href='/blog/edit'><i class='fa fa-plus'></i> New Blog Entry</a></li>
        </ul>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php endif; ?>

<div class='row'>

    <div class='col-lg-12'>
<?php
if (empty($blogs))
{
    echo $this->alerts->info("There are no blog entries");
}
else
{

    foreach ($blogs as $k => $r)
    {
        $excerpt = $date = $name = null;

        try
        {
            $excerpt = $this->blogs->getTableValue('excerpt', $r->id);

            $name = $this->users->getName($r->userid);

            $user = ($this->router->fetch_class() == 'blog') ? $this->session->userdata('userid') : $r->userid;

            $date = $this->users->convertTimezone($user, $r->publishDate, "Y-m-d");
        }
        catch(Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        $class = (empty($k)) ? ' unread' : null;

        // echo "<img src='/user/profileimg/100/{$r->userid}' class='blog-preview-img img-polaroid'>";
        echo EOL . "<blockquote class='blog{$class}'><h3>{$r->title}</h3><div class='blogDate'>{$name} &bull; {$date}</div>" . EOL;

        $onclick = (strtoupper($namespace) == 'ARTICLES') ? "articles.readMore(this);" : "blog.readMore(this);";

        echo "<div class='blogContent' onclick=\"{$onclick}\" articleID='{$r->id}'>" . PHP_EOL;
        echo "<img src='/user/profileimg/100/{$r->userid}' class='blog-preview-img img-polaroid'>" . EOL;

        echo nl2br($excerpt) . EOL;

        echo "</div> <!-- .blogContent -->";
        echo "<hr>" . EOL;

        $onclick = (strtoupper($namespace) == 'ARTICLES') ? "articles.readMore(this);" : "blog.readMore(this);";

        echo "<button type='button' id='readMoreBtn{$r->id}' class='btn btn-default btn-sm' onclick=\"{$onclick}\" articleID='{$r->id}'>Read More <i class='fa fa-arrow-right'></i></button>" . PHP_EOL;


        if ($editBlog == true)
        {
            echo "<button type='button' class='btn btn-warning btn-sm' onclick=\"blog.editBlog(this);\" blogID='{$r->id}'><i class='fa fa-edit'></i> Edit</button>" . EOL;
        }


        // only displays active tab in blogs view
        if (strtoupper($namespace) == 'BLOG')
        {
            if ($r->active == 1)
            {
                echo "<label class='label label-success blogStatus pull-right'>Active</label>" . PHP_EOL;
            }
            else
            {
                echo "<label class='label label-danger blogStatus pull-right'>Deactived</label>" . PHP_EOL;
            }
        }

        echo "<div class='clearfix'></div>" . EOL;
        echo "</blockquote>" . PHP_EOL;

    }

}
?>
    </div> <!-- .col12 -->


</div> <!-- .row-fluid -->

<ul class='pagination'>
    <li class='<?=($page == 1) ? 'disabled' : null?>'><a href='/<?=$this->router->fetch_class()?>'>&laquo;</a></li>
<?php
for ($i = 1; $i <= $totalPages; $i++)
{
    $class = ($i == $page) ? 'active' : null;

    echo "<li class='{$class}'><a href='/" . $this->router->fetch_class(). "/index/{$i}'>{$i}</a></li>" . PHP_EOL;
}
?>
    <li class='<?=($page == $totalPages) ? 'disabled' : null;?>'><a href='/<?=$this->router->fetch_class()?>/index/<?=$totalPages?>'>&raquo;</a></li>
</ul>

