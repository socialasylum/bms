<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h2>Outgoing Servers</h2>

<p>Select an outgoing server</p>

<?php if (empty($outgoingServers)) : echo $this->alerts->info("There are currently no outgoing servers.");
else : 
?>

<select id='outgoingServer' id='outgoingServer' data-width="100%">
	<option value=''>Select server</option>
	<?php
		foreach ($outgoingServers as $r)
		{
			//$sel = ($r->id == $info->outgoingServer) ? "selected='selected'" : null;
			
			$box = strtoupper($r->mailbox);
			
			echo "<option {$sel} value='{$r->id}'>{$box}:{$r->port}</option>" . PHP_EOL;
		}
	
	?>
</select>
<?php endif; ?>


<div id='outgoing-settings-display' class='form-horizontal' style="opacity:0;display:none">

	<input type='hidden' name='id' id='SMTPAccountID' value='0'>

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Description</label>
		<div class="col-sm-10">
			<input type='text' name='alias' id='alias' class='form-control'>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Server Hostname</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' id='mailbox' name='mailbox' value="" placeholder="smtp.example.com" required data-msg='Please enter the outgoing server host'>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-xs-2 col-sm-2 control-label">Port</label>
		<div class="col-xs-10 col-sm-10">
			<input type='text' class='form-control' name='port' id='port' value="" placeholder="25" style="width:100px;">
			<span class="help-block">System will use port <b>25</b> if left blank.</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->	
	
	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">SSL</label>
		<div class="col-sm-10">
			<select name='ssl' id='ssl'>
				<option value='0'>None</option>
			<?php
			if (!empty($ssl))
			{
				foreach ($ssl as $r)
				{
					echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Authentication</label>
		<div class="col-sm-10">
			<select name='auth' id='auth'>
				<option value='0'>Normal Password</option>
			<?php
			if (!empty($auth))
			{
				foreach ($auth as $r)
				{
					echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->
	

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='username' id='username' placeholder='' required data-msg="Please enter the username">
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Password</label>
		<div class="col-sm-10">
			<input type='password' class='form-control' name='passwd' id='passwd' placeholder="Password" required data-msg="Please enter the e-mail password">

			<span class="help-block">
				<div class='checkbox'>
					<input type='hidden' name='remPasswd' value='0'>
					<label><input type='checkbox' name='remPasswd' id='remPasswd' value='1'> Remember Password</label>
				</div>
			</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	
	
</div>