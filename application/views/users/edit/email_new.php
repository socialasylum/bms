<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h3>Add E-mail Account</h3>

<div class='form-horizontal'>
	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Your Name</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='yourname' id='yourname' required data-msg="Please enter your name">
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">E-mail Address</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='emailAddress' id='emailAddress'  placeholder="email@example.com" required data-msg="Please enter the e-mail address">
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='username' id='username' value="" placeholder='' required data-msg="Please enter the username">
			<span class="help-block">If blank, will use E-mail address as username</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Password</label>
		<div class="col-sm-10">
			<input type='password' class='form-control' name='passwd' id='passwd' placeholder="Password" required data-msg="Please enter the e-mail password">

			<span class="help-block">
				<div class='checkbox'>
					<input type='hidden' name='remPasswd' value='0'>
					<label><input type='checkbox' name='remPasswd' value='1'> Remember Password</label>
				</div>
			</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<h4>Incoming</h4>

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Type</label>
		<div class="col-sm-10">
			<select class='form-control' name='type' id='type'>
				<?php
				if (!empty($types))
				{
					foreach ($types as $r)
					{
						echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
					}
				}
				?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Server Hostname</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' id='incomingHost' name='incomingHost' value="" placeholder="mail.example.com" required data-msg='Please enter the incoming server host'>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->
	
	<div class="form-group">
	    <label for="" class="col-xs-2 col-sm-2 control-label">Port</label>
		<div class="col-xs-10 col-sm-10">
			<input type='text' class='form-control' name='incomingPort' id='incomingPort' placeholder="587" style="width:100px;">
			<span class="help-block">System will use default ports if left blank. IMAP <b>143</b> POP <b>110</b></span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->	
	
	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">SSL</label>
		<div class="col-sm-10">
			<select class='form-control' name='incomingSSL' id='incomingSSL'>
				<option value=''>None</option>
			<?php
			if (!empty($ssl))
			{
				foreach ($ssl as $r)
				{
					echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Authentication</label>
		<div class="col-sm-10">
			<select class='form-control' name='incomingAuth' id='incomingAuth'>
				<option value=''>Normal Password</option>
			<?php
			if (!empty($auth))
			{
				foreach ($auth as $r)
				{
					echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<h4>Outgoing (SMTP)</h4>
	
	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Server Hostname</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' id='outgoingHost' name='outgoingHost' value="" placeholder="smtp.example.com" required data-msg='Please enter the outgoing server host'>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-xs-2 col-sm-2 control-label">Port</label>
		<div class="col-xs-10 col-sm-10">
			<input type='text' class='form-control' name='outgoingPort' id='outgoingPort' value="" placeholder="25" style="width:100px;">
			<span class="help-block">System will use port <b>25</b> if left blank.</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->	
	
	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">SSL</label>
		<div class="col-sm-10">
			<select class='form-control' name='outgoingSSL' id='outgoingSSL'>
				<option value=''>None</option>
			<?php
			if (!empty($ssl))
			{
				foreach ($ssl as $r)
				{
					echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Authentication</label>
		<div class="col-sm-10">
			<select class='form-control' name='outgoingAuth' id='outgoingAuth'>
				<option value=''>Normal Password</option>
			<?php
			if (!empty($auth))
			{
				foreach ($auth as $r)
				{
					echo "<option value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

</div>