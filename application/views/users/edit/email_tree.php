<?php if(!defined('BASEPATH')) die('Direct access not allowed');

	header('Content-Type: application/json');

	$el = array();

	$el[] = array
	(
		'key' => 0,
		'title' => 'Signature',
		'page' => 'email_sig',
		'extraClasses' => 'signature-icon',
	);

	
	if (!empty($emailAccounts))
	{
		foreach ($emailAccounts as $r)
		{
			$accountActions = array
				(
					array('title' => 'Server Settings', 'key' => $r->id, 'page' => 'email_server_settings', 'extraClasses' => 'server-folder')
				);
				
			$el[] = array
			(
				'key' => $r->id,
				'title' => $r->emailAddress,
				'page' => 'email_account', 
				'extraClasses' => 'email-folder',
				'children' => $accountActions
			);
		}
	}
	
	
	if (!empty($outCnt))
	{
		$el[] = array
		(
			'key' => 0,
			'title' => 'Outgoing Servers (SMTP)',
			'page' => 'email_outgoing_servers',
			'extraClasses' => 'smtp-folder',
		);
	}
	
	echo json_encode($el);