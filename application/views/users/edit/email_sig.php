<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<h3><i class='fa fa-pencil'></i> Signature</h3>

<input type='hidden' name='signatureID' id='signatureID' value='<?=$sig->id?>'>

<textarea id='signature' name='signature' class='form-control' rows='5'><?=$sig->signature?></textarea>
