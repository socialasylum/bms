<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='form-horizontal'>

	<input type='hidden' name='id' value='<?=$info->id?>'>


	<div class="form-group">
	    <label for='' class='col-sm-2 control-label'>Type</label>
		<div class='col-sm-10'>
			<select name='type' id='type' class='form-control' data-width='200px'>
				<?php
				if (!empty($types))
				{
					foreach ($types as $r)
					{
						$sel = ($r->code == $info->type) ? "selected='selected'" : null;
					
						echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
					}
				}
				?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Server Hostname</label>
		<div class="col-sm-10">
			<input type='text' name='mailbox' id='mailbox' class='form-control' value="<?=$info->mailbox?>" placeholder="mail.example.com" required data-msg='Please enter the server hostname'>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-xs-2 col-sm-2 control-label">Port</label>
		<div class="col-xs-10 col-sm-10">
			<input type='text' class='form-control' name='port' id='port' value="<?=$info->port?>" placeholder="587" style="width:100px;">
			<span class="help-block">System will use default ports if left blank. IMAP <b>143</b> POP <b>110</b></span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->	
	
	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">SSL</label>
		<div class="col-sm-10">
			<select name='ssl' id='ssl'>
				<option value='0'>None</option>
			<?php
			if (!empty($ssl))
			{
				foreach ($ssl as $r)
				{
					$sel = ($r->code == $info->ssl) ? "selected='selected'" : null;
					
					echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Authentication</label>
		<div class="col-sm-10">
			<select name='auth' id='auth'>
				<option value='0'>Normal Password</option>
			<?php
			if (!empty($auth))
			{
				foreach ($auth as $r)
				{
					$sel = ($r->code == $info->auth) ? "selected='selected'" : null;
				
					echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
				}
			}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Username</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='username' id='username' value="<?=$info->username?>" placeholder='' required data-msg="Please enter the username">
			<span class="help-block">If blank, will use E-mail address as username</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Password</label>
		<div class="col-sm-10">
			<input type='password' class='form-control' name='passwd' id='passwd' value="<?=$info->passwd?>" placeholder="Password" required data-msg="Please enter the e-mail password">

			<span class="help-block">
				<div class='checkbox'>
					<input type='hidden' name='remPasswd' value='0'>
					<label><input type='checkbox' name='remPasswd' value='1' <?php if (!empty($info->passwd)) echo "checked='checked' "; ?>> Remember Password</label>
				</div>
			</span>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


</div> <!-- /.div-horizontal -->