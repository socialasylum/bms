<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='form-horizontal'>
	<input type='hidden' name='id' value='<?=$info->id?>'>

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Account Name</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='alias' id='alias' value="<?=$info->alias?>">
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Your Name</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='yourname' id='yourname' value="<?=$info->yourname?>" required data-msg="Please enter your name">
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->


	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">E-mail Address</label>
		<div class="col-sm-10">
			<input type='text' class='form-control' name='emailAddress' id='emailAddress' value="<?=$info->emailAddress?>" placeholder="email@example.com" required data-msg="Please enter the e-mail address">
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	<div class="form-group">
	    <label for="" class="col-sm-2 control-label">Outgoing Server</label>
		<div class="col-sm-10">
			<select name='outgoingServer' id='outgoingServer' data-width='100%'>
			<?php
				if (!empty($outgoingServers))
				{
					foreach ($outgoingServers as $r)
					{
						$sel = ($r->id == $info->outgoingServer) ? "selected='selected'" : null;
						
						echo "<option {$sel} value='{$r->id}'>{$r->mailbox}:{$r->port}</option>" . PHP_EOL;
					}
				}
			?>
			</select>
	    </div> <!-- col-9 -->
	</div> <!-- .form-group -->

	

</div> <!-- /.form-horizontal -->