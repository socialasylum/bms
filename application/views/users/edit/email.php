<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div id="tabEmail" class='tab-pane' data-tab='email'>
	<h2><i class='fa fa-envelope'></i> E-mail</h2>
	
	<p class='lead'>Here you can add, edit, and delete external outgoing e-mail accounts.</p>
	

	<div class='col-sm-3'>
			<div id='email-tree'></div>
	</div>
	
	<div class='col-sm-9' id='email-edit-display'></div>

</div> <!-- /#tabEmail -->