<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
echo <<< EOS
    <table id='cardTbl' class='table package'>
        <thead>
            <tr>
                <th>Default</th>
                <th>Type</th>
                <th>Name on Card</th>
                <th>Card Number</th>
                <th>Exp.</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
EOS;

foreach ($token as $id=>$t) 
{
    try
    {
        $cc = $this->payment->getCreditCards($t->token);
    }
    catch (Exception $e)
    {
        $this->functions->sendStackTrace($e);
    }

    if (empty($cc))
    {
        echo $this->alerts->info("You currently have no cards on file.");
    }
    else
    {

        foreach ($cc as $id => $r)
        {
            $ccid = $r['token'];

            $default = null;
            if (!empty($r['default'])) $default = "checked='checked'";

            echo "<tr>" . PHP_EOL;

            echo "<td><input type='radio' onclick=\"users.toggleCardSelect();\" name='defaultCard' id='defaultCard_{$ccid}' value='{$ccid}' ccName=\"{$r['cardholderName']}\" ccNumber=\"{$r['maskedNumber']}\" ccExp=\"{$r['expirationDate']}\" {$default}></td>";

            echo "<td><img src=\"{$r['imageUrl']}\"></td>";
            echo "<td>{$r['cardholderName']}</td>";
            echo "<td>{$r['maskedNumber']}</td>";
            echo "<td>{$r['expirationDate']}</td>";
            echo "<td><button id='delBtn' class='btn btn-danger btn-xs' onclick=\"users.deleteCard('{$ccid}', {$userid});\" type='button'><i class='fa fa-trash-o'></i></button></td>";

            echo "</tr>" . PHP_EOL;

        }

    }

}
echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
?>

