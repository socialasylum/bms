<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
    $attr = array
        (
            'name' => 'compBillingForm',
            'id' => 'compBillingForm'
        );

echo form_open('#', $attr);
?>

    <div class='modal-dialog'>
        <div class='modal-content'>


    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3 class='modal-title'><i class='fa fa-dollar'></i> Complete Billing</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body form-horizontal'>
        <div id='billingAlert'></div>

        <p class='lead'>Welcome <strong><?=$name?></strong>, thank you for registering with <?=$companyName?>. 
        To complete your registration, please complete the form below.</p>

        <h4><strong>Step 1:</strong> Select your plan</h4>
<?php 

//print_r($plans); 

if (empty($plans))
{
    echo $this->alerts->info("No plans to select from");
}
else
{
echo <<< EOS
    <table class='table table-hover table-bordered' id='planTbl'>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Plan</th>
                <th>Cost</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($plans as $k => $v)
    {
        // if allowed plans have been set to be restricted
        if (!empty($allowedPlans))
        {
            // if plan is not in allowed plans, will skip
            if (!in_array($v->id, $allowedPlans)) continue;
        }


        $planName = $v->name;
        $cost = number_format($v->price, 2);


        echo "<tr>" . PHP_EOL;

        echo "\t<td><input type='radio' name='planID' id='PlanID_{$v->id}' value='{$v->id}'></td>" . PHP_EOL;
        echo "\t<td>{$planName}</td>" . PHP_EOL;
        echo "\t<td>\${$cost}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>
        <h4><strong>Step 2:</strong> Enter Card Information</h4>

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccName'>Name on card</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='ccName' id='ccName' value='' placeholder='John Smith'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccNumber'>Card Number</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='ccNumber' id='ccNumber' value='' placeholder='0000-1234-5678-9999'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccExp'>Card Expiration</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <select name='ccExpM' id='ccExpM' class='input-small form-control' style='width:100px;float:left;'>
                <option value="">MM</option>
<?php
for ($i = 1; $i <= 12; $i++)
{

    $display = str_pad($i, 2, '0', STR_PAD_LEFT);

    echo "<option value='{$display}'>{$display}</option>" . PHP_EOL;
}
?>
            </select> 
                <div style='float:left; font-size:24px;margin:0 10px;'>/</div>
            <select name='ccExpY' id='ccExpY' class='input-small form-control' style='width:100px;float:left;'>
                <option value="">YYYY</option>
<?php
for ($i = (int) date("Y"); $i <= ((int) date("Y") + 10); $i++) 
{
    echo "<option value='{$i}'>{$i}</option>" . PHP_EOL;
}
?>
            </select>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->


    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccCvv'>CVV</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <input type='text' class='form-control' name='ccCvv' id='ccCvv' value='' placeholder='000'>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->

    <div class="form-group">
        <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
            <label class='checkbox'>
                <input type='hidden' name='agree' value='0'>
                <input type='checkbox' name='agree' id='agree' value='1'> By clicking this checkbox you agree to the sites Terms of Service, Privacy Policy, and Return &amp; Refunds Policy.
            </label>
        </div> <!-- .controls -->
    </div> <!-- .form-group -->



    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-danger btn-xs pull-left' id='cancelRegBtn' data-dismiss='modal' aria-hidden='true'><i class='fa fa-ban'></i> Cancel &amp; Do no Subscribe</button>
        <button type='button' class='btn btn-success' id='completeRegBtn' onclick="users.completeFBReg(this);">Complete Registration <i class='fa fa-arrow-right'></i></button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->

</form>
