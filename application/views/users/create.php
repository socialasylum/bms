<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1>Create User</h1>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>
<?php
    $attr = array
        (
            'name' => 'createForm',
            'id' => 'createForm',
            'class' => 'form-horizontal'
        );

echo form_open('#', $attr);
?>

            <div class="form-group">
            <label class="col-lg-2 control-label" for='firstName'>First Name</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='firstName' id='firstName' placeholder='David'>
                </div>
            </div>


            <div class="form-group">
            <label class="col-lg-2 control-label" for='lastName'>Last Name</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='lastName' id='lastName' placeholder='Smith'>
                </div>
            </div>


            <div class="form-group">
            <label class="col-lg-2 control-label" for='email'>E-mail</label>
                <div class='col-lg-10 controls'>
                    <input type='text' class='form-control' name='email' id='email' placeholder='email.address@domain.com'>
                </div>
            </div>

<?php if (!empty($positions)) : ?>

            <div class="form-group">
            <label class="col-lg-2 control-label" for='position'>Position</label>
                <div class='col-lg-10 controls'>
                    <select class='form-control' name='position' id='position'>
                        <option value=''></option>
                    <?php
                        foreach ($positions as $r)
                        {
                            // $sel = ($r->id == $id) ? 'selected' : null;

                            echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                        }
                    ?>
                    </select>
                </div>
            </div>

<?php endif; ?>

<?php if (!empty($departments)) : ?>

            <div class="form-group">
            <label class="col-lg-2 control-label" for='department'>Department</label>
                <div class='col-lg-10 controls'>
                    <select class='form-control' name='department' id='department'>
                        <option value=''></option>
                    <?php
                        foreach ($departments as $r)
                        {
                            // $sel = ($r->id == $id) ? 'selected' : null;

                            echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                        }
                    ?>
                    </select>
                </div>
            </div>

<?php endif; ?>



            <div class="form-group">
            <label class="col-lg-2 control-label" for='password'>Password</label>
                <div class='col-lg-10 controls'>
                    <input type='password' class='form-control' name='password' id='password' placeholder='&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;'>
                </div>
            </div>

            <div class="form-group">
            <label class="col-lg-2 control-label" for='confirmPassword'>Confirm Password</label>
                <div class='col-lg-10 controls'>
                    <input type='password' class='form-control' name='confirmPassword' id='confirmPassword' placeholder='&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;'>
                </div>
            </div>





        </form>

        <div class='form-actions'>
            <button type='button' class='btn btn-primary' id='createBtn'>Create</button>
            <button type='button' class='btn' name='cancelBtn' id='cancelBtn'>Cancel</button>
        </div>


