<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php
if (empty($youtubeVideos))
{
    echo $this->alerts->info("There are no Youtube Videos.<br>Add Some =)");
}
else
{
    echo "<ul id='ytVideoSortList' class='formSort'>" . PHP_EOL;

    foreach ($youtubeVideos as $k => $r)
    {

        $ytid = $this->functions->getYoutubeVideoID($r->url);

        if ($ytid === false) continue;

        $class = ($k == 0) ? 'yt-primary' : null;

        echo "<li id='videos-{$r->id}'>";

        // youtube embed iframe
        echo <<< EOS
            <div class='ytContainer {$class}'>
                <div class='yt-body'>
                <iframe class='img-thumbnail' width="252" height="142" src="//www.youtube.com/embed/{$ytid}" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class='yt-footer'>
                    <span class='muted'>[Click here to drag]</span>
                <button type='button' onclick="users.deleteYTVideo({$r->id});" class='btn btn-default pull-right'><i class='fa fa-trash-o'></i></button>
        </div>
    </div>
EOS;



        echo "</li>" . PHP_EOL;
    }

    echo "</ul>" . PHP_EOL;
}
?>
