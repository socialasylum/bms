<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-user'></i> Users</h1>

<input type='hidden' id='<?=$this->security->get_csrf_token_name()?>' value='<?=$this->security->get_csrf_hash()?>'>
<input type='hidden' name='module' id='module' value='<?=$modId?>'>


<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><a href='/user/edit'><i class='fa fa-globe'></i> Create new User</a></li>
        <li><a href='#invUserModal' tabindex="-1" role='button' data-toggle='modal'><i class='fa fa-user'></i> Invite User</a></li>
    </ul>

    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='helpBtn' class='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php
if (!empty($users))
{
echo <<< EOS
    <table class='table table-hover table-condensed' id='usersTable'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <!--<th>&nbsp;</th> -->
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($users as $r)
    {
        $statusDisplay = null;

        try
        {
            // $company = $this->companies->getHomeLocation($r->id, true);

            $assigned = $this->companies->userAssignedToCompany($r->id, $this->session->userdata('company'));

            // home location is not assigned
            if ($assigned == false) continue;

            // only checks if the user being checked is not them
            if (((int) $r->id !== (int) $this->session->userdata('userid')) && !$admin)
            {
            	
            	
                // checks if they are in the current users downline
                $inDownline = $this->users->inDownline($r->id);

                // skips if they are not in downline
                if ($inDownline == false) continue;
            }
            $info = $this->users->getRow($r->id);


            $statusDisplay = $this->functions->codeDisplay(7, $info->status);
            
            $statusLbl = ($info->active == 1) ? 'success' : 'danger';
            			
        }
        catch(Exception $e)
        {
            PHPFunctions::sendStackTrace($e);
        }

        echo "<tr onclick=\"users.editUser({$r->id})\">\n";

        echo "<td>{$r->id}</td>";
        echo "<td>{$info->firstName} {$info->lastName}</td>";
        echo "<td>{$info->email}</td>";
        echo "<td><div class='label label-{$statusLbl}'>{$statusDisplay}</div></td>";

		/*
        echo "<td>
            <a href=\"/user/edit/{$r->id}\" class='btn btn-warning btn-xs pull-right' value='{$r->id}'><i class='fa fa-edit'></i></a>
        </td>";
		*/
        echo "</tr>\n";

    }

    echo "</tbody>\n";
    echo "</table>\n";

}
?>


<!-- invite User modal //-->
<div id='invUserModal' class='modal fade'>
    <div class='modal-dialog'>
        <div class='modal-content'>

    <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h3><i class='fa fa-plus'></i> Invite User</h3>
    </div> <!-- .modal-header //-->

    <div class='modal-body'>
        <div id='invUserAlert'></div>

        <p class='lead'>Enter the e-mail address of the user you wish to invite to this company.</p>

<?php

        $attr = array
            (
                'name' => 'invUserForm',
                'id' => 'invUserForm',
                'class' => 'form-horizontal'
            );

        echo form_open('#', $attr);
?>

        <input type='hidden' name='addedUser' id='addedUser' value=''>

        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='email'>E-mail Address</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <input type='text' class='typeahead form-control' name='email' id='email' autocomplete='off' placeholder='email.address@domain.com'>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->



        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='position'>Position</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="position" id="position" class='form-control'>
                    <option value=""></option>
        <?php
        if (!empty($positions))
        {
            foreach ($positions as $r)
            {
                echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
            }
        }
        ?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->



        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='department'>Department</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <select name="department" id="department" class='form-control'>
            <option value=""></option>
<?php
if (!empty($departments))
{
    foreach ($departments as $r)
    {
        echo "<option value='{$r->id}'>{$r->name}</option>" . PHP_EOL;
    }
}
?>
        </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->





        </form>
    </div> <!-- .modal-body //-->

    <div class='modal-footer'>
        <button class='btn btn-default' data-dismiss='modal' aria-hidden='true' id='cancelUploadPDFBtn'>Close</button>
        <button type='button' class='btn btn-primary' aria-hidden='true' id='invUserBtn' disabled='disabled'>Invite User</button>
    </div> <!-- .modal-footer //-->

        </div> <!-- .modal-content -->
    </div> <!-- .modal-dialog -->
</div> <!-- .modal -->



