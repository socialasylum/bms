<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>
<?php

$attr = array
        (
            'name' => 'newCardForm',
            'id' => 'newCardForm',
            'class' => 'form-horizontal'
        );

echo form_open_multipart('#', $attr);

	if (!empty($userid)) $userName = $this->users->getName($userid);
?>

<input type='hidden' name='userid' id='userid' value=''>

<?php
/*
    <input type='hidden' name='BTcustomerID' id='BTcustomerID' value='<?=$info->BrainTreeCustomerID?>'>
 */
?>

<div class="form-group">
    <label class='col-lg-3 control-label' for='ccName'>Name on Card</label>
    <div class="col-lg-9">
        <input type='text' class='form-control' name='ccName' id='ccName' value="" placeholder='<?=$userName?>'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-3 control-label' for='ccNumber'>Card Number</label>
    <div class="col-lg-9">
        <input type='text' class='form-control' name='ccNumber' id='ccNumber' value="" placeholder='0000-1234-5678-9999'>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccExp'>Card Expiration</label>
    <div class="col-lg-9">
        <select name="ccExpM" id="ccExpM" class='input-small form-control' style="width:100px;float:left;">
            <option value="">MM</option>
<?php
for ($i = 1; $i <= 12; $i++)
{

    $display = str_pad($i, 2, '0', STR_PAD_LEFT);

    echo "<option value='{$display}'>{$display}</option>" . PHP_EOL;
}
?>
        </select>
 <div style='float:left; font-size:24px;margin:0 10px;'>/</div>
            <select name="ccExpY" id="ccExpY" class='input-small form-control' style="width:100px;float:left;">
                <option value="">YYYY</option>
<?php
for ($i = (int) date("Y"); $i <= ((int) date("Y") + 10); $i++) 
{
    echo "<option value='{$i}'>{$i}</option>" . PHP_EOL;
}
?>
        </select>
    </div>
</div>

<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ccCvv'>CVV [?]</label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <input type='text' class='form-control' name='ccCvv' id='ccCvv' value="" placeholder='000'>
    </div> <!-- .controls -->
</div> <!-- .form-group -->


<div class="form-group">
    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='ID'>
        <input type='hidden' name='makeDefault' value='0'>
        <input type='checkbox' id='makeDefault' name='makeDefault' value='1'>
    </label>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
        <label for='makeDefault'>
        Set card as Default? (If this is your first card, it will be your default even if you don't check the box)
        </label>
    </div> <!-- .controls -->
</div> <!-- .form-group -->

</form>

<div class='clearfix'></div>

