<?php if (!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-edit'></i> <?= (empty($id)) ? 'Create' : 'Edit' ?> User</h1>

<input type='hidden' id='token' value='<?= $this->security->get_csrf_hash() ?>'>
<input type='hidden' id='tab' value='<?= $tab ?>'>
<input type='hidden' name='module' id='module' value='<?= $modId ?>'>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
<ul class="nav navbar-nav">
    <li><button type='button' class='btn btn-link navbar-btn' id='saveBtn'><i class='fa fa-save success'></i> Save</button></li>

    <?php if ($emailPerm) : ?>

        <li class='dropdown' id='emailDD' style='display:none;opacity:0;'>
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class='fa fa-envelope'></i> E-mail <span class="caret"></span></a>
            <ul class='dropdown-menu' role='menu'>
                <li><a href='javascript:void(0)' id='newAcctBtn'><i class='fa fa-plus'></i> Add Account</a></li>
                <li><button type='button' class='btn btn-link dropdown-btn' id='testAcctBtn'><i class='fa fa-circle-o-notch'></i> Test</button></li>
                <li><button type='button' class='btn btn-link dropdown-btn' id='delAcctBtn' disabled='disabled'><i class='fa fa-trash-o'></i> Delete Account</button></li>
            </ul>
        </li>

    <?php endif; ?>

</ul>

<ul class="nav navbar-nav pull-right">
    <li><a href='javascript:void(0);' id='helpBtn' class='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
    <?php if ($this->users->isCompanyAdmin($this->session->userdata('userid'), $this->session->userdata('company')) && !empty($id)) : ?>
        <li><button type='button' id='deleteUserBtn' class='btn btn-link navbar-btn'><i class='fa fa-trash-o danger'></i> Delete User</button></li>
    <?php endif; ?>

    <li><a href='javascript:void(0);' onclick="users.cancelEdit(this);" id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
</ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php
$attr = array
    (
    'name' => 'editForm',
    'id' => 'editForm'
);

echo form_open_multipart('/user/update', $attr);


if (!empty($id))
    echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

if (!empty($completeBilling))
    echo "<input type='hidden' name='completeBilling' id='completeBilling' value='{$completeBilling}'>" . PHP_EOL;
?>
<input type='hidden' name='company' id='company' value='<?php echo $this->session->userdata('company'); ?>'>
<input type="hidden" name="MAX_FILE_SIZE" value="104857600" />
<input type='hidden' name='uploadedProfileImg' id='uploadedProfileImg' value=''>
<input type='hidden' name='admin' value='<?= (empty($id)) ? '0' : $info->admin ?>'>
<input type='hidden' name='industry' value='<?= $info->industry ?>'>
<input type='hidden' name='otherIndustry' value='<?= $info->otherIndustry ?>'>
<input type='hidden' name='emaildomain' value='<?php echo $domain; ?>' />

<div class='tabbable'>

    <div class="tab-content">

        <div id="tabSettings" class='tab-pane active'>

            <div class='row'>
                <div class='col-lg-6 form-horizontal'>

                    <fieldset>

                        <h2><i class='fa fa-cog'></i> Settings</h2>

                        <p class='lead'>Please enter basic information about this user.</p>

                        <div class="form-group">
                            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='firstName'>First Name</label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                <input type='text' class='form-control' name='firstName' id='firstName' placeholder='David' value="<?= $info->firstName ?>" required data-msg='Please enter a first name!' />
                            </div> <!-- .controls -->
                        </div> <!-- .form-group -->


                        <div class="form-group">
                            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='lastName'>Last Name</label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                <input type='text' class='form-control' name='lastName' id='lastName' placeholder='Smith' value="<?= $info->lastName ?>" required data-msg='Please enter a last name!' />
                            </div> <!-- .controls -->
                        </div> <!-- .form-group -->

                        <div class="form-group">
                            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='email'>E-mail</label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                <div class='input-group'>
                                    <input type='text' class='form-control' name='email' id='email' placeholder='email.address' value="<?php echo explode('@', $info->email)[0]; ?>" required data-msg='Please enter an e-mail address!' /><span class="input-group-addon" id="newemaildomain">@<?php echo $domain; ?></span>
                                </div>
                            </div> <!-- .controls -->
                        </div> <!-- .form-group -->

                        <div class="form-group">
                            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='status'>Status</label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                <select name="status" id="status" class='form-control'>
                                    <!-- <option value=""></option> -->
                                    <?php
                                    if (!empty($statuses)) {
                                        foreach ($statuses as $r) {

                                            $sel = ($r->code == $info->status) ? "selected='selected'" : null;

                                            echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
                                        }
                                    }
                                    ?>
                                </select>
                            </div> <!-- .controls -->
                        </div> <!-- .form-group -->


                        <?php if (!empty($positions)) : ?>
                            <div class="form-group">
                                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='position'>Position</label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                    <select name='position' id='position' class='form-control' required data-msg='Please select a position!'>
                                        <option value=''></option>
                                        <?php
                                        try {
                                            if (!empty($id))
                                                $loggedInPosition = $this->users->getPosition();
                                        } catch (Exception $e) {
                                            PHPFunctions::sendStackTrace($e);
                                        }


                                        foreach ($positions as $r) {

                                            if (!$this->users->isCompanyAdmin($this->session->userdata('userid'), $this->session->userdata('company'))) {

                                                if (!empty($id) && !empty($loggedInPosition)) {
                                                    // skips check if current position
                                                    if ($r->id !== $position) {
                                                        $childPosition = $this->positions->isChildPosition($loggedInPosition, $r->id);

                                                        // skips if not a childPosition
                                                        if ($childPosition == false)
                                                            continue;
                                                    }
                                                }
                                            }
                                            $sel = ($position == $r->id) ? 'selected' : null;

                                            echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                                        }
                                        ?>
                                    </select>
                                </div> <!-- .controls -->
                            </div> <!-- .form-group -->

                                    <?php endif; ?>


                                    <?php if (!empty($departments)) : ?>

                            <div class="form-group">
                                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='department'>Department</label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                    <select name='department' id='department' class='form-control'>
                                        <option value='0'></option>
                            <?php
                            foreach ($departments as $r) {
                                // $sel = ($info->department == $r->id) ? 'selected' : null;
                                $sel = ($department == $r->id) ? 'selected' : null;

                                echo "<option {$sel} value='{$r->id}'>{$r->name}</option>\n";
                            }
                            ?>
                                    </select>
                                </div> <!-- .controls -->
                            </div> <!-- .form-group -->
                                    <?php endif; ?>

                        <div class="form-group">
                            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='timezone'>Timezone</label>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                <select id='timezone' name='timezone' class='form-control' required data-msg='Please select a timezone!'>
                                    <option value=''></option>
<?php
if (!empty($timezones)) {
    foreach ($timezones as $zone => $abb) {
        $sel = ($info->timezone == $zone) ? 'selected' : null;

        echo "<option {$sel} value=\"{$zone}\">{$abb}</option>" . PHP_EOL;
    }
}
?>
                                </select>
                            </div> <!-- .controls -->
                        </div> <!-- .form-group -->



                                    <?php /* if (!empty($id)) : ?>

                                      <div class="form-group">
                                      <label class="col-sm-3 control-label" for='password'>Current Password</label>
                                      <div class="col-sm-9">
                                      <input type='password' class='form-control' name='currentPassword' id='currentPassword'>
                                      <span class='help-block'>Leave password fields blank if you do not wish to update password.</span>
                                      </div>
                                      </div>
                                      <?php endif; */ ?>

                        <div class="form-group" id='passwordFG'>
                            <label class="col-sm-3 control-label" for='password'>Password</label>
                            <div class="col-sm-9">
                                <input type='password' class='form-control' name='password' id='password' <?= ((empty($id)) ? 'required' : 'optional') ?> data-match='confirmPassword' data-msg="Please enter a password!">
                            </div>
                        </div>

                        <div class="form-group" id='confirmPWFG' style="display: none;">
                            <label class="col-sm-3 control-label" for='confirmPassword'>Confirm Password</label>
                            <div class="col-sm-9" style='overflow:hidden;'>
                                <input type='password' class='form-control strength' name='confirmPassword' id='confirmPassword' data-msg='Passwords did not match!'>
                            </div>
                        </div>


<?php if ($this->session->userdata('admin') === true OR $companyAdmin === true) : ?>

                            <h2><i class='fa fa-lock'></i> Permissions</h2>

                            <div class="form-group">
                                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyAdmin'>Company Admin</label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                    <div class='checkbox'>
                                        <label>
    <?php
    $compAdminChecked = ($companyAdmin == 1) ? "checked='checked'" : null;
    ?>
                                            <input type='checkbox' name='companyAdmin' id='companyAdmin' value='1' <?= $compAdminChecked ?> > Check if you user is an adminstrator for the company. This will give them full access to this company.
                                        </label>
                                    </div>
                                </div> <!-- .controls -->
                            </div> <!-- .form-group -->




                            <div class="form-group">
                                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyEditor'>Site Editor</label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                    <div class='checkbox'>
                                        <label>
    <?php
    $editorChecked = ($companyEditor == 1) ? "checked='checked'" : null;
    ?>
                                            <input type='checkbox' name='companyEditor' id='companyEditor' value='1' <?= $editorChecked ?> > Check if you user has the ability to change content on the site for this company.
                                        </label>
                                    </div>
                                </div> <!-- .controls -->
                            </div> <!-- .form-group -->
    <?php
else :
    echo "<input type='hidden' name='companyAdmin' value='" . ((empty($id)) ? '0' : $companyAdmin) . "'>";
    echo "<input type='hidden' name='companyEditor' value='" . ((empty($id)) ? '0' : $companyEditor) . "'>";
endif;

if ($this->session->userdata('admin') === true) :
    ?>

                            <hr>

                            <div class="form-group">
                                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='admin'>Administrator</label>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                    <div class='checkbox'>
                                        <label>
                            <?php
                            $adminChecked = ($info->admin == 1) ? "checked='checked'" : null;
                            ?>
                                            <input type='checkbox' name='admin' id='admin' value='1' <?= $adminChecked ?> > Site Administrator. <span class='label label-danger'>FULL ACCESS TO ALL COMPANIES</span>
                                        </label>
                                    </div>
                                </div> <!-- .controls -->
                            </div> <!-- .form-group -->


    <?php if ((int) $id !== (int) $this->session->userdata('userid') AND ! empty($id)) : ?>
                                <hr>
                                <div class="form-group">
                                    <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for=''>&nbsp;</label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                                        <button type='button' id='loginAsBtn' class='btn btn-info'>Login as <?= $info->firstName ?> <?= $info->lastName ?></button>
                                    </div> <!-- .controls -->
                                </div> <!-- .form-group -->
    <?php endif; ?>
<h3><i class='fa fa-pencil'></i> Signature</h3>

<input type='hidden' name='signatureID' id='signatureID' value='<?=$sig->id?>'>

<textarea id='signature' name='signature' class='form-control' rows='5'><?=$sig->signature?></textarea>
<?php endif; ?>




                    </fieldset>

                </div>

                <div class='col-lg-6 form-horizontal'>

                    <fieldset>

                        <h2><i class='fa fa-picture-o'></i> Profile Picture</h2>

                        <div class='row-fluid' style="margin-bottom:10px;" id='profilePicContainer'>
<?php
if (empty($info->profileimg))
    echo "<img src='/public/images/dude.gif' class='img-responsive img-thumbnail'>";
else
    echo "<img src='/user/profileimg/400/{$id}' class='img-responsive img-thumbnail'>";
?>
                        </div>

                        <p>
                            To upload your image you will need to access this page from a computer where you have your pictures and not from the Kiosk. Click to browse your computer for the image, then simply click the <b>Upload</b> button. The image size should be 300 X 300 pixels or larger in .jpg, .gif or .png file formats only. 
                        </p>

                        <p><input type='file' name='profileimg' id='profileimg' <?= $btnDis ?>></p>

                            <?php
                            if (!empty($info->facebookID)) {
                                echo "<hr>" . PHP_EOL;

                                echo "<button type='button' class='btn btn-info' onclick=\"fb.syncPhoto(this);\"><i class='fa fa-refresh'></i> Sync With <i class='fa fa-facebook-square'></i> Facebook Profile Image</button>";
                            }
                            ?>


                    </fieldset>


                </div>

            </div>



        </div> <!-- #tabSettings //-->

        <div id="tabPhotos" class='tab-pane'>

<?php
if (empty($albums))
    echo $this->alerts->info("This user has no albums!");
else {

    echo "<div class='album-container'>" . PHP_EOL;

    echo "<h2><i class='fa fa-photo'></i> Photo Albums</h2>" . PHP_EOL;

    foreach ($albums as $r) {

        $imgDisplay = null;

        try {
            $previewPhoto = $this->user->getAlbumPhotos($r->id, 1);

            // http://dev.bms.socialasylum.com/user/albumphoto/1/f9837500764419219183ec6d4d5da19c.jpg/250
            if (!empty($previewPhoto))
                $imgDisplay = "<div class='nt-container'><img src='/user/albumphoto/{$id}/{$previewPhoto[0]->fileName}/250'></div>";
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }


        echo <<< EOS

		<div class='album shadow'>
			<div class='album-preview' onclick="users.viewAlbum({$r->id});">
				{$imgDisplay}
			</div> <!-- /.album-preview -->
			
			<div class='album-name'><a href='javascript:users.viewAlbum({$r->id});'>{$r->name}</a></div>
			
		</div> <!-- /.album -->
EOS;
    }

    echo "</div> <!-- /.album-container -->" . PHP_EOL;

    // closes row if not 3 albums in the row
    //if ($rcnt < 2) echo "</div> <!-- /.row -->" . PHP_EOL;
}
?>

            <div id='photos-display'></div>
        </div> <!-- /#tabPhotos -->




        <div id='tabContact' class='tab-pane form-horizontal'>
            <h2><i class='fa fa-info-circle'></i> Contact Information</h2>

            <p class='lead'>Below you can enter the physical mailing address for this user.</p>


            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyName'>Company Name</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' class='form-control' name="companyName" id="companyName" value="<?= $info->companyName ?>" placeholder="">
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

            <div class="form-group">
                <label class="col-lg-3 control-label" for="address">Address</label>
                <div class="col-lg-9">
                    <input type='text' class='form-control' name="address" id="address" value="<?= $info->addressLine1 ?>" placeholder="123 Main St.">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label" for="address2">Address 2</label>
                <div class="col-lg-9">
                    <input type='text' class='form-control' name="address2" id="address2" value="<?= $info->addressLine2 ?>" placeholder="Suite #101">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label" for="address2">Address 3</label>
                <div class="col-lg-9">
                    <input type='text' class='form-control' name="address3" id="address3" value="<?= $info->addressLine3 ?>" placeholder="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label" for="city">City</label>
                <div class="col-lg-9">
                    <input type='text' class='form-control' name="city" id="city" value="<?= $info->city ?>" placeholder="Las Vegas">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label" for="state">State</label>
                <div class="col-lg-9">
                    <select name='state' id='state' class='form-control'>
                        <option value=''></option>
<?php
if (!empty($states)) {
    foreach ($states as $abb => $stateName) {
        $sel = ($info->state == $abb) ? "selected='selected'" : null;

        echo "<option {$sel} value='{$abb}'>{$stateName}</option>\n";
    }
}
?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label" for="postalCode">Postal Code</label>
                <div class="col-lg-9">
                    <input type='text' class='form-control' name="postalCode" id="postalCode" value="<?= $info->postalCode ?>" placeholder="89011">
                </div>
            </div>

            <hr>
            <h2><i class='fa fa-phone'></i> Phone</h2>

            <p class='lead'>Enter the phone, mobile, and or fax number to contact this user.</p>

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='phone'>Phone</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='phone' id='phone' class='form-control' value="<?= $info->phone ?>" placeholder='(888) 555-1212'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='mobile'>Mobile</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='mobile' id='mobile' class='form-control' value="<?= $info->mobile ?>" placeholder='(888) 555-1212'>
                </div> <!-- .controls -->
            </div> <!-- .form-group --> 


            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='fax'>Fax</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='fax' id='fax' class='form-control' value="<?= $info->fax ?>" placeholder='(888) 555-1212'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

            <hr>
            <h2><i class='fa fa-facebook-square'></i> Social Media</h2>


            <p class='lead'>Enter any social media links you wish to be associated with your user profile.</p>

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='facebookUrl'><i class='fa fa-facebook-square'></i> Facebook</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='facebookUrl' id='facebookUrl' class='form-control' value="<?= $info->facebookUrl ?>" placeholder='http://facebook.com'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->


            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='twitterUrl'><i class='fa fa-twitter-square'></i> Twitter</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='twitterUrl' id='twitterUrl' class='form-control' value="<?= $info->twitterUrl ?>" placeholder='http://twitter.com'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->


            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='linkedInUrl'><i class='fa fa-linkedin-square'></i> LinkedIn</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='linkedInUrl' id='linkedInUrl' class='form-control' value="<?= $info->linkedInUrl ?>" placeholder='http://linkedIn.com'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='youtubeUrl'><i class='fa fa-youtube-square'></i> Youtube Channel</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='youtubeUrl' id='youtubeUrl' class='form-control' value="<?= $info->youtubeUrl ?>" placeholder='http://youtube.com'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='googlePlusUrl'><i class='fa fa-google-plus-square'></i> Google Plus</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' name='googlePlusUrl' id='googlePlusUrl' class='form-control' value="<?= $info->googlePlusUrl ?>" placeholder='http://plus.google.com'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

            <div class="form-group">
                <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='companyWebsiteUrl'>Company Website</label>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                    <input type='text' class='form-control' name='companyWebsiteUrl' id='companyWebsiteUrl' value="<?= $info->companyWebsiteUrl ?>" placeholder='http://'>
                </div> <!-- .controls -->
            </div> <!-- .form-group -->

        </div> <!-- #tabContact -->



        <div id='tabWall' class='tab-pane'>


<?php
if (empty($streamPosts))
    echo $this->alerts->info('No activity for this user.');
else {
    echo "<div id='masonry-container'>" . PHP_EOL;

    foreach ($streamPosts as $r) {
        $body = $likes = $comments = $date = $usersName = $photos = $displayLikes = null;
        $likes = 0;
        $setDim = 'false';
        try {
            $usersName = $this->users->getName($r->postingUser);
            $body = $this->stream->getPostBody($r->id);
            $likes = $this->stream->getLikeCnt($r->id);
            $comments = $this->stream->getPosts($userid, $r->id);

            $photos = $this->stream->getPostPhotos($r->id);


            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "F j Y g:iA");
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            continue;
        }

        $body = nl2br($body);

        if ($likes == 1) {
            $likeTxt = "<a href='javascript:void(0);'><i class='fa fa-thumbs-up'></i> {$likes} person</a> likes this";
        } else {
            $likeTxt = "<a href='javascript:void(0);'><i class='fa fa-thumbs-up'></i> {$likes} people</a> like this";
        }

        if (empty($likes))
            $displayLikes = "display:none;";

        echo "<div class='item'>" . PHP_EOL;

        echo "<div class='wallpost-container'>" . PHP_EOL;

        echo <<< EOS
			<div class='wallpost'>
				<div class='row'>
					<div class='col-md-12'>
						<img class='img-responsive post-img' src='/user/profileimg/40/{$r->postingUser}'>
						
						<div class='wallpost-name'><a href='/user/edit/{$r->postingUser}'>{$usersName}</a></div> <!-- /.wallpost-name -->
					
						<div class='wallpost-time text-muted'>{$date}</div>
EOS;

        if (!empty($photos)) {
            $class = (count($photos) == 1) ? 'post-photo-container-single' : 'post-photo-container';

            if (count($photos) == 1)
                $setDim = 'true';

            echo "<div class='{$class}'>" . PHP_EOL;

            foreach ($photos as $pr) {
                echo "<a href=\"javascript:photos.viewPhoto({$pr->id}, {$userid}, '{$pr->fileName}', {$setDim});\" id='img-preview-link-{$pr->id}'><img id='img-preview-{$pr->id}' src='/user/albumphoto/{$userid}/{$pr->fileName}/300' class='img-responsive'></a>" . PHP_EOL;
            }

            echo "</div> <!-- /.post-photo-container -->" . PHP_EOL;
        }
        echo <<< EOS
					</div>
				</div> <!-- /.row -->
				
				<div class='post-body'>{$body}</div>
				
				<div class='wallpost-actions'>
					{$likeBtn}
				</div> <!-- /.wallpost-actions -->
				
				<div class='clearfix'></div>
			</div> <!-- ./userTabContent -->
			
			<div class='like-container' style="{$displayLikes}">
			{$likeTxt}
			</div> <!-- /.like-container -->
EOS;


        if (!empty($comments)) {
            foreach ($comments as $cr) {

                try {
                    $commentBody = $this->stream->getPostBody($cr->id);
                    $usersName = $this->users->getName($cr->postingUser);

                    //$tzUserid = ($this->session->userdata('logged_in') == true) ? $this->session->userdata('userid') : $cr->postingUser;

                    $date = $this->users->convertTimezone($this->session->userdata('userid'), $cr->datestamp, "F j Y g:iA");
                } catch (Exception $e) {
                    $this->functions->sendStackTrace($e);
                    continue;
                }

                $commentBody = nl2br($commentBody);

                echo <<< EOS
					<div class='comment-container'>
						<img class='img-responsive post-img' src='/user/profileimg/40/{$cr->postingUser}'>
						
						<div class='wallpost-name'><a href='/user/index/{$cr->postingUser}'>{$usersName}</a></div> <!-- /.wallpost-name -->
					
						<div class='wallpost-time text-muted'>{$date}</div>
						
						<div class='comment-body'>{$commentBody}</div>
					</div>				
EOS;
            }
        }

        echo "</div> <!-- /.wallpost=container -->" . PHP_EOL;
        echo "</div> <!-- /.item -->" . PHP_EOL;
    }

    echo "</div> <!-- /#masonry-container -->" . PHP_EOL;
}
?>


        </div> <!-- /.#tabWall -->


            <?php if ((int) $id == (int) $this->session->userdata('userid')) : ?>
            <div id='tabFacebook' class='tab-pane form-horizontal'>
                <h2><i class='fa fa-facebook-square'></i> Facebook</h2>
                <?php
                if (empty($info->facebookID)) :
                    echo $this->alerts->alert("Your account is currently not associated with your Facebook account. Click the button below to connect your account.");
                    ?>

                    <div class='fbBtnContainer'>
                        <a href='javascript:void(0);' onclick="fb.login(this, false, true);"><img src='/public/images/fbConnectBtn.png'></a>
                    </div>
                <?php
                else:
                    echo $this->alerts->success("Your account has been successfully linked with Facebook!");
                    ?>
                    <p class='lead'>Now that your account is linked with your Facebook account. You can login to the BMS simply by clicking the &quot;Sign in with Facebook&quot; button at the login screen.</p>

                    <hr>
                    <p>If you wish to disconnect your linked Facebook account, you can do so by clicking the button below.</p>

                    <button class='btn btn-danger btn-sm' onclick="fb.unlink(this, <?= $id ?>);"><i class='fa fa-facebook-square'></i> Unlink Facebook Account</button>

                <?php endif; ?>

            </div> <!-- #tabFacebook -->
<?php endif; ?>

        <hr>

        <?php if ($this->functions->promoteLeadVersion() OR $this->session->userdata('company') == 35) : ?>

        <div id="tabBilling" class='tab-pane'>

            <h2><i class='fa fa-credit-card'></i> Billing</h2>

            <p class='lead'>Here you view your transaction history, as well as manage any subscriptions pertaining to your company.</p>

            <div class='row'>
                <div class='col-lg-7 col-md-7 col-sm-7 col-xs-12'>
                    <div class='panel panel-default'>
                        <div class='panel-heading'>
                            <h3 class='panel-title'><i class=''></i> Transaction History</h3>
                        </div> <!-- .panel-heading -->

                        <div class='panel-body'>

            <?php
            if (empty($transData)) {
                echo $this->alerts->info("There is no transaction information!");
            } else {
                echo "<table id='tranTbl' class='table table-stripped table-hover'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Transaction ID</th>";
                echo "<th>Credit Card</th>";
                echo "<th>Date</th>";
                echo "<th>Amount</th>";
                echo "<th>Status</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                EOS;

                foreach ($transData as $id => $t) {
                    try {
                        $transid = $t->id;
                        $tranAmount = $t->amount;
                        $tranStatus = $t->status;
                        $trancc = $t->creditCardDetails->maskedNumber;
                        $updated = $t->_attributes['updatedAt'];
                        $tranIso = $t->_attributes['currencyIsoCode'];

                        $date = $updated->format('M-d-Y H:i:s');

                        $datef = substr($date, 0, 11);
                    } catch (Exception $e) {
                        $this->functions->sendStackTrace($e);
                    }

                    echo "<tr onclick=\"users.getInvoice('{$transid}', {$info->id});\">" . PHP_EOL;

                    echo "<td>{$transid}</td>";
                    echo "<td>{$trancc}</td>";
                    echo "<td>{$datef}</td>";
                    echo "<td>$ {$tranAmount} {$tranIso}</td>";
                    echo "<td>{$tranStatus}</td>";

                    echo "</tr>" . PHP_EOL;
                }
                echo "</tbody>" . PHP_EOL;
                echo "</table>" . PHP_EOL;
            }
            ?>

                        </div> <!-- .panel-body -->
                    </div> <!-- .panel -->


                    <div class='' name='invoiceDisplay' id='invoiceDisplay'></div>

                </div> <!-- /.col7 -->

                <div class='col-lg-5 col-md-5 col-sm-5 col-xs-12'>
                            <?php
// if (empty($info->BrainTreeSubscriptionID))
// {
                            try {
                                $plans = $this->payment->getPlans();
                            } catch (Exception $e) {
                                $this->functions->sendStackTrace($e);
                            }
                            ?>
                    <div class='panel panel-default' id='createSubPanel'>
                        <div class='panel-heading'>

                            <h3 class='panel-title'><i class='fa fa-credit-card'></i> Subscription</h3>
                        </div> <!-- .panel-heading -->

                        <div class='panel-body'>

                            <p><b>Step 1:</b> Select which plan you would like to purchase.</p>
                            <?php
                            if (!empty($plans)) {
                                echo <<< EOS
    <table class='table table-hover table-bordered' id='subscriptionTbl'>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Plan</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
EOS;

                                // print_r($plans);
                                foreach ($plans as $k => $v) {
                                    if (in_array($v->id, $subs)) {
                                        echo "<tr>" . PHP_EOL;

                                        echo "\t<td><input type='radio' name='subPlan' id='subplan_{$v->id}' value=\"{$v->id}\" planPrice=\"{$v->price}\" onclick=\"users.toggleCardSelect();\"></td>" . EOL;
                                        echo "\t<td>{$v->name}</td>" . PHP_EOL;
                                        echo "\t<td>\${$v->price}" . PHP_EOL;

                                        echo "</tr>" . PHP_EOL;
                                    }
                                }

                                echo "</tbody>" . PHP_EOL;
                                echo "</table>" . PHP_EOL;
                                // }
                                ?>

                                <p><b>Step 2:</b> To create or upgrade a subscription please  add or select a new credit card below to which this subscription will be billed to.</p>

                                <div id='cardDisplay'>
                        <?= $this->alerts->info("No card has been selected!") ?>
                                </div>

                                <p class='text-muted'><strong>*Note:</strong> You may only downgrade your plan once every billing cycle.</p>
                            </div>

                            <div class='panel-footer'>
                                <button type='button' class='btn btn-info' id='createSubBtn' disabled='disabled'><i class='fa fa-refresh'></i> Save Subscription</button>
                            </div>
                        </div> <!-- .panel -->
                    <?php } ?>

                    <div class='panel panel-default'>
                        <div class='panel-heading'>

                            <h3 class='panel-title'><i class='fa fa-credit-card'></i> Cards on File</h3>
                        </div> <!-- .panel-heading -->

                        <div class='panel-body' id='cardOnFileBody'>
    <?php
    $this->load->view('users/cardsOnFile', $b);
    ?>
                        </div> <!-- .panel-body -->
                        <div class='panel-footer'>
                            <button type='button' name='addCard' id='addCard' class='btn btn-success'><i class='fa fa-plus'></i> Add New Card</button>
                        </div><!-- .panel-footer -->
                    </div> <!-- .panel -->

                            <?php if ($this->users->isAdmin() == true) : ?>
                        <div class='panel panel-default'>
                            <div class='panel-heading'>
                                <h3 class='panel-title'>Admin Billing Data</h3>
                            </div> <!-- .panel-heading -->
                            <div class='panel-body form-horizontal'>
                                <p><strong>ADMIN USE ONLY</strong> This is only used for making bacend subscription changes. Mostly all subscription changes should be done through the system.</p>

                                <div class="form-group">
                                    <label class='col-lg-5 col-md-5 col-sm-5 col-xs-5 control-label' for='BrainTreeCustomerID'>BrainTree Customer ID</label>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 controls">
                                        <input type='text' class='form-control' name='BrainTreeCustomerID' id='BrainTreeCustomerID' value="<?= $info->BrainTreeCustomerID ?>" placeholder=''>
                                    </div> <!-- .controls -->
                                </div> <!-- .form-group -->

                                <div class="form-group">
                                    <label class='col-lg-5 col-md-5 col-sm-5 col-xs-5 control-label' for='BrainTreeSubscriptionID'>BrainTree Subscription ID</label>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 controls">
                                        <input type='text' class='form-control' name='BrainTreeSubscriptionID' id='BrainTreeSubscriptionID' value="<?= $info->BrainTreeSubscriptionID ?>" placeholder=''>
                                    </div> <!-- .controls -->
                                </div> <!-- .form-group -->


                            </div> <!-- .panel-body -->
                        </div> <!-- .panel -->
                            <?php endif; ?>



                            <?php
// checks if there is a subsription
                            /*
                              if (!empty($info->BrainTreeSubscriptionID))
                              { */
                            $display = (empty($info->BrainTreeSubscriptionID)) ? 'display:none;' : null;

                            echo "<div id='cancelSubDisplay' style=\"{$display}\">" . PHP_EOL;
                            echo "<hr>";

                            echo "<p class='lead'>If you wish to cancel your subscription, please click the button below.</p>";

                            echo "<button type='button' class='btn btn-danger' id='cancelSubBtn'>Cancel Subscription</button>" . PHP_EOL;

                            echo "</div> <!-- #cancelSubDisplay -->" . PHP_EOL;
// }
                            ?>

                </div> <!-- col-5 -->
            </div> <!-- .row -->

    <?php
    $user = $this->session->userdata('userid');
    $company = $this->session->userdata('company');

    if ($this->users->isCompanyAdmin($user, $company)) :
        ?>

                <hr>
                <h2><i class='fa fa-star-o'></i> Billing Exempt</h2>

                <p class='lead'>
                    Is this user billing exempt? If so click the <strong>Billing Exempt</strong> button below.</p>

                <input type='hidden' name='exempt' id='exempt' value='<?= (int) $exempt ?>'>
        <?php
        if ((bool) $exempt == true)
            echo "<button type='button' class='btn btn-success btn-lg' id='exemptBtn'><i class='fa fa-check'></i> Exempt</button>";
        else
            echo "<button type='button' class='btn btn-danger btn-lg' id='exemptBtn'><i class='fa fa-ban'></i> Non-Exempt</button>";
        ?>



    <?php endif; ?>




        </div> <!-- #tabBilling -->



    <?php
else:
    echo "<input type='hidden' name='BrainTreeCustomerID' value='{$info->BrainTreeCustomerID}'>" . PHP_EOL;
    echo "<input type='hidden' name='BrainTreeSubscriptionID' value='{$info->BrainTreeSubscriptionID}'>" . PHP_EOL;
endif;


if ($emailPerm && !empty($id))
    include_once 'edit/email.php';
?>

</div> <!-- .tab-contents //-->
</div> <!-- .tabbable //-->

</form>

</div>
</div>

<!-- Modal -->
<div class="modal fade" id="newCardModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class='fa fa-credit-card'></i> Add New Card</h4>
            </div>
            <div class="modal-body">
                <div id='newCardAlert'></div>
        <?php include_once 'newCard.php'; ?>
            </div> <!-- modal-body -->

            <div class="modal-footer">
                <button type="button" class="btn btn-default" id='closeCardModelBtn'><i class='fa fa-times'></i> Close</button>
                <button type='button' id='NCsubmitBtn' name='NCsubmitBtn' class='btn btn-primary'><i class='fa fa-save'></i> Submit</button>
                </form>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- holds HTML for step 2 in creating a subscription
displays once a card is chosen -->
<div id='cardDataHtml' style="display:none">
    <hr>
    <dl class='dl-horizontal pricedl'>
        <dt>Price</dt>
        <dd id='createPriceDisplay'>$0.00</dd>

        <dt>Card Holders Name</dt>
        <dd id='createHolderNameDisplay'><span class='text-muted'>Unselected</span></dd>

        <dt>Card Selected</dt>
        <dd id='createCardSelectDisplay'><span class='text-muted'>Unselected</span></dd>

        <dt>Card Expiration Date</dt>
        <dd id='createCardExpDisplay'><span class='text-muted'>Unselected</span></dd>
    </dl>

</div>


<div id='completeBillingModal' class='modal fade'></div>

    <?php
    $this->load->view('photos/modal');
    ?>