<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<div class='row album-row'>
	<div class='col-md-9'>
		<h2><i class='fa fa-photo'></i> <?=$info->name?></h2>	
	</div>
	
	<div class='col-md-3'>
		<button type='button' class='btn btn-info pull-right' onclick="users.viewAlbums();"><i class='fa fa-chevron-left'></i> Back To Albums</button>
	</div> <!-- col-md-3 -->
</div> <!-- /.row -->



<?php
if (empty($photos))
{
	echo $this->alerts->alert("This album has no photo's.");
}
else
{
	echo "<div id='album-photo-container'>";
	foreach ($photos as $r)
	{
		echo "<a href=\"javascript:photos.viewPhoto({$r->id}, {$userid}, '{$r->fileName}', true);\"><img class='' src='/user/albumphoto/{$userid}/{$r->fileName}/400'></a>" . PHP_EOL;
	}
	echo "</div> <!-- /#album-photo-container -->" . PHP_EOL;
}
?>