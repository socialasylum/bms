<?php if(!defined('BASEPATH')) die('Direct access not allowed');

$a[$nav] = 'active';

?>


<div class='sidebar'>
    <ul class='widget widget-menu unstyled'>
        <li class='<?=$a['index']?>'><a href='/user'><i class='menu-icon icon-dashboard'></i>All Users</a></li>
        <li class='<?=$a['create']?>'><a href='/user/create'><i class='menu-icon icon-bar-chart'></i>Create User</a></li>

        <!-- <li class=''><a href='#'><i class='menu-icon icon-group'></i>PCR <b class='label green pull-right'>132</b></a></li> -->
        <!-- <li class=''><a href='#'><i class='menu-icon icon-tasks'></i>New Hires <b class='label green pull-right'>0</b></a></li> -->
        <!-- <li class=''><a href='#'><i class='menu-icon icon-file-alt'></i>Eval/Vists <b class='label green pull-right'>8</b></a></li> -->

    </ul>
</div>


<?php
/*

<div class="well sidebar-nav">
    <ul class='nav nav-list'>
        <li class="nav-header">Users</li>
        <li class='<?=$a['index']?>'><a href='/user'>All Users</a></li>
        <li class='<?=$a['create']?>'><a href='/user/create'>Create User</a></li>
    </ul>
    </div>
*/
?>
