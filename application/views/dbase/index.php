<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>



<div class='row'>
	<div class='col-sm-3'>
		<div class='well-white'>
			<h3><i class='fa fa-table'></i> Tables </h3>
		</div>
	</div> <!-- /.col-3 -->

	<div class='col-sm-9'>
		<div class='well-white'>
	
			<h1><i class='fa fa-database'></i> Database</h1>
		
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav">
        <li><button type='button' class='btn btn-link navbar-btn' id='saveBtn'><i class='fa fa-save success'></i> Save</button></li>
        <li><button type='button' class='btn btn-link navbar-btn' id='tblBtn'><i class='fa fa-table info'></i> New Table</button></li>
        <li><button type='button' class='btn btn-link navbar-btn' id='backupBtn'><i class='fa fa-hdd-o'></i> Backup</button></li>
        <li><button type='button' class='btn btn-link navbar-btn' id='restoreBtn'><i class='fa fa-cloud-upload'></i> Restore</button></li>
    </ul>
        
<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

		</div> <!-- /.well-white -->
		
	</div> <!-- /.col-9 -->
</div> <!-- /.row -->