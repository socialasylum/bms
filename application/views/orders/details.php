<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-list-alt'></i> Order Details</h1>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
        <ul class="nav navbar-nav">
            <li><a href="javascript:void(0);" id='saveBtn'><i class='fa fa-save'></i> Save</a></li>
        </ul>

        <ul class="nav navbar-nav pull-right">
            <li><a href='javascript:void(0);' id='cancelBtn'><i class='fa fa-ban danger'></i> Cancel</a></li>
        </ul>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php
if (empty($items))
{
    echo $this->alerts->error("There are no items associated with this order! This should not be.");
}
else
{

    $attr = array
        (
            'name' => 'detailsForm',
            'id' => 'detailsForm',
            'onSubmit' => 'return false;'
        );

    echo form_open('#', $attr);

    echo "<input type='hidden' name='id' id='id' value='{$id}'>" . PHP_EOL;

    echo <<< EOS
    <table class='table table-hover table-bordered'>
        <thead>
            <tr>
                <th>Image</th>
                <th>Item</th>
                <th>Qty</th>
                <th>Options</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($items as $r)
    {
        $itemInfo = null;

        try
        {
            $itemInfo = $this->item->getInfo($r->item);

            $shortDesc = nl2br($itemInfo->shortDescription);
        
            $img = $this->item->getItemMainImage($r->item);

            if (empty($img)) $imgDisplay = "<i class='fa fa-tag item-blank-img drop-icon'></i>";
            else $imgDisplay = "<img src='{$this->config->item('liveUrl')}genimg/render/80?img=" . urlencode($img) . "&path=" . urlencode("uploader/{$this->config->item('company')}") . "' class='img-responsive'>";

        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr>" . PHP_EOL;

        echo "\t<td valign='middle' align='middle'>{$imgDisplay}</td>" . PHP_EOL;
        echo "\t<td>
            <input type='hidden' name='item[]' id='item' value='{$itemInfo->id}'>
            <span class='cartItemName'>{$itemInfo->name}</span><p>{$shortDesc}</p></td>" . PHP_EOL;
        echo "\t<td>{$r->qty}</td>" . PHP_EOL;
        echo "\t<td><p class='text-muted'>(None)</p></td>" . PHP_EOL;
        echo "\t<td>{$r->retailPrice}</td>" . PHP_EOL;

        echo "</tr>" . PHP_EOL;
    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>

<div class='row'>

    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
        <fieldset>
            <legend>Shipping Address</legend>
<?php
if (empty($shippingAddress))
{
    echo $this->alerts->error("Shipping Address not available!");
}
else
{

    $name = (empty($crmContact->name)) ? null : "<strong>{$crmContact->name}</strong><br>";
    $companyName = (empty($crmContact->companyName)) ? null : "<strong>{$crmContact->companyName}</strong><br>";

echo <<< EOS
    <address>
        {$companyName}
        {$name}
        {$shippingAddress->address}<br>
        {$shippingAddress->city}, {$shippingAddress->state} {$shippingAddress->postalCode}
    </address>
EOS;
?>
        </fieldset>
    </div>

    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
        <fieldset>
            <legend>Billing Address</legend>
<?php
if (empty($billingAddress))
{
    echo $this->alerts->error("Billing Address not available!");
}
else
{

    $name = (empty($crmContact->name)) ? null : "<strong>{$crmContact->name}</strong><br>";
    $companyName = (empty($crmContact->companyName)) ? null : "<strong>{$crmContact->companyName}</strong><br>";

echo <<< EOS
    <address>
        {$companyName}
        {$name}
        {$billingAddress->address}<br>
        {$billingAddress->city}, {$billingAddress->state} {$billingAddress->postalCode}
    </address>

EOS;
}
?>
        </fieldset>
    </div>


    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>
        <fieldset>
            <legend>Payment Information</legend>
    
        </fieldset>
    </div>

    <div class='col-lg-3 col-md-3 col-sm-6 col-xs-12'>

        <fieldset>
            <legend><i class='fa fa-money'></i> Total</legend>

 <table class='table table-hover table-bordered grandTotalTbl pull-right'>
    <tbody>

        <tr>
            <td>Subtotal<input type='hidden' id='subTotal' value='0.00'></td>
            <td id='subtTotalTD'>$0.00</td>
        </tr>

        <tr>
            <td>Shipping</td>
            <td>$0.00</td>
        </tr>

        <tr>
            <td>Tax</td>
            <td>$0.00 (8.5%)</td>
        </tr>

        <tr>
            <td>Grand Total</td>
            <td id='grandTotalTD'>$0.00</td>
        </tr>

    </tbody>
</table>

        </fieldset>
    </div> <!-- col-4 -->

</div> <!-- .row -->

<hr>
<h2><i class='fa fa-edit'></i> Notes</h2>
<p class='lead'>Here you can view notes related to this order. If you wish to leave a note, simply enter it below.</p>

<?php
if (empty($notes))
{
    echo $this->alerts->info("There are no notes associated with this order");
}
else
{
    foreach ($notes as $r)
    {
        $name = $date = null;

        try
        {
            $name = $this->users->getName($r->userid);
            
            $date = $this->users->convertTimezone($this->session->userdata('userid'), $r->datestamp, "Y-m-d g:i A");
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
            echo $this->alerts->error('Database Error!');
        }

echo <<< EOS
    <div class='row'>
    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>

            <div class='clearfix'>
            <img src='/user/profileimg/130/{$r->userid}' class='img-responsive img-thumbnail noteImg'>

        {$r->note}
            </div>
            <div class='noteDetails'>
                <p>{$name} <span class='text-muted2'>&bull; {$date}</span></p>
                
            </div>
        </div>
    </div> <!-- .row -->
EOS;
    }
}
?>

<textarea class='form-control' id='note' name='note' rows='5'></textarea>

<hr>

<h2><i class='fa fa-refresh'></i> Order Processing</h2>

<p class='lead'>Below you can change details about the processing of this order.</p>

<div class='row'>
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12 form-horizontal'>
        <div class="form-group">
            <label class='col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label' for='status'>Status</label>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 controls">
                <select name="status" id="status" class='form-control'>
<?php
if (!empty($status))
{
    foreach ($status as $r)
    {
        $sel = ($info->status == $r->code) ? "selected='selected'" : null;

        echo "<option {$sel} value='{$r->code}'>{$r->display}</option>" . PHP_EOL;
    }
}
?>
                </select>
            </div> <!-- .controls -->
        </div> <!-- .form-group -->
    </div>
    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
        
    </div>
</div> <!-- .row -->

</form>

<?php } ?>
