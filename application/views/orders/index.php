<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

<h1><i class='fa fa-clipboard'></i> Orders</h1>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_top.php'; ?>
    <ul class="nav navbar-nav pull-right">
        <li><a href='javascript:void(0);' id='helpBtn'><i class='fa fa-question-circle info'></i></a></li>
    </ul>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/navbar_bottom.php'; ?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/application/views/template/breadcrumbs.php'; ?>

<?php
if (empty($orders))
{
    echo $this->alerts->info("No orders have been placed!");
}
else
{
echo <<< EOS
    <table class='table table-hover table-bordered' id='ordersTbl'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Company Name</th>
                <th>Amount</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
EOS;

    foreach ($orders as $r)
    {
        $status = null;
        $total = 0;

        try
        {
            $info = $this->crm->getContactInfo($r->crmContact);

            $total = $this->orders->calculateOrderAmount($r->id);

            $companyName = (empty($info->companyName)) ? "<span class='text-muted'>(None)</span>" : $info->companyName;

            $status = $this->functions->codeDisplay(22, $r->status);
        }
        catch (Exception $e)
        {
            $this->functions->sendStackTrace($e);
        }

        echo "<tr onclick=\"orders.viewDetails({$r->id});\">". PHP_EOL;

        echo "\t<td>{$info->name}</td>" . PHP_EOL;
        echo "\t<td>{$companyName}</td>" . PHP_EOL;
        echo "\t<td>\${$total}</td>" . PHP_EOL;
        echo "\t<td>{$status}</td>" . PHP_EOL;
        
        echo "</tr>". PHP_EOL;

    }

echo "</tbody>" . PHP_EOL;
echo "</table>" . PHP_EOL;
}
?>
