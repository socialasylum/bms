<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cms']['folders'] = array('assets', 'css', 'html', 'js');

$config['cms']['extensions'] = array('js', 'css', 'html', 'htm', 'less', 'jpg', 'jpeg', 'png', 'gif');