<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

include 'config.php';

/*
$hook['pre_system'] = array(
                    'class'    => 'bms_pre_system',
                    'function' => 'initialize',
                    'filename' => 'bms_pre_system.php',
                    'filepath' => 'third_party/bms/hooks',
                    'params'   => array()
                    );
*/

$hook['pre_system'] = array(
                    'class'    => 'CI_less_pre_sys',
                    'function' => 'initialize',
                    'filename' => 'CI_less_pre_sys.php',
                    'filepath' => 'third_party/phpfunctions/hooks',
                    'params'   => array('public/less/', array('main.less' => 'main.css'), ENVIRONMENT, $config['min_version'])
                    );

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */