<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
$config['memcached'] = array(
    'server_1' => array(
    'host' => '192.168.0.52',
    'port'        => 11211,
    'weight'    => 1
    ),
    'server_2' => array(
    'host' => '192.168.0.50',
    'port'        => 11211,
    'weight'    => 2
    )

); 
 */

$config['memcached'] = array
(
    'hostname' => 'localhost',
    'port' => '11211',
    'weight' => '1'
);
