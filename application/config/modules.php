<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['modules'] = array();

$config['modules'][] = array('name' => 'Announcements', 'url' => '/announce', 'namespace' => 'announce', 'icon' => 'fa fa-bullhorn', 'sellable' => true, 'unv' => false, 'folders' => false, 'desc' => "<p>Allow for company wide announcements.</p>  <p>Able to be integrated into public website.</p>");

$config['modules'][] = array('name' => 'Applications', 'url' => '/app', 'namespace' => 'app', 'icon' => 'fa fa-copy', 'sellable' => true, 'unv' => false, 'folders' => false, 'desc' => "<p>Receive online applications from potential applicants.</p>");

//$config['modules'][] = array('name' => '', 'url' => '', 'namespace' => 'announce', 'icon' => 'fa fa-', 'sellable' => true, 'unv' => false, 'folders' => false, 'desc' => "");