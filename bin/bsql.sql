SHOW DATABASES;

SHOW TABLES;

UPDATE users SET passwd = SHA1('rainbow0020') WHERE id = 5;


EXPLAIN users;
EXPLAIN modules;
EXPLAIN companyModules;
EXPLAIN tickets;
EXPLAIN notes;

EXPLAIN docFolderAssign
EXPLAIN documents;

EXPLAIN menu;

SELECT * FROM companyBilling;
SELECT * FROM contactUs;
SELECT * FROM menu;
SELECT * FROM userYouTubeVideos;
SELECT * FROM users WHERE id = 84;

SELECT * FROM codes WHERE `group` = 7;
DELETE FROM menu WHERE id = 6;

UPDATE menu SET `target` = NULL;

DELETE FROM userYouTubeVideos WHERE id = 31;

DELETE FROM announcements WHERE id = 2;

SELECT id, firstName, lastName, BrainTreeSubscriptionID FROM users WHERE id = 85;

SELECT * FROM userTokens;

EXPLAIN childPositions;
EXPLAIN positions;

DELETE FROM users where id = 62;

EXPLAIN companies;
SELECT * FROM announcements;
SELECT * FROM companies;
SELECT * FROM tickets;
SELECT * FROM users;
SELECT * FROM positions;
SELECT * FROM leads;
SELECT * FROM departments;
SELECT * FROM permissions;
SELECT * FROM notes;
SELECT * FROM assignedTech;
SELECT * FROM userCompanies;
SELECT * FROM userCompanyDepartments;
SELECT * FROM userCompanyPositions;
SELECT name FROM departments WHERE id = 3;
SELECT * FROM locations;
EXPLAIN users;
-- TRUNCATE TABLE menu;

SELECT * FROM menu WHERE company = 1;

SELECT * FROM codes WHERE code = 0;

SELECT * FROM modules;

SELECT * FROM documents;
SELECT * FROM documentHistory;

SELECT id, company, title, active, deleted FROM documents;

SELECT * FROM msgFolders;

UPDATE users SET status = 3 WHERE id = 53;

-- TRUNCATE TABLE documentHistory;
-- TRUNCATE TABLE documents;


SELECT * FROM childPositions;

-- inserts into notes
INSERT INTO notes SET
    ticketID = 1,
    userid = 2,
    datestamp = NOW(),
    note = 'I am testing this with Note 3 but 2 for this ticket';

-- inserts into assignedTech
INSERT INTO assignedTech SET
    ticketid = 1,
    userid = 1;

INSERT INTO userCompanies SET
    userid = 2,
    company = 6;

INSERT INTO userCompanyDepartments SET
    userid = 5,
    company = 6,
    department = 15;

INSERT INTO userCompanyPositions SET
    userid = 15,
    company = 1,
    position = 1;

-- inserts into tickets
INSERT INTO tickets SET
    status = 2,
    techAssigned = 1,
    priority = 1
WHERE id = 3;

-- inserts a new department
INSERT INTO departments SET
    datestamp = NOW(),
    createdBy = 2,
    company = 6,
    name = 'Accounting',
    active = 1;

-- inserts a new user
INSERT INTO users SET
    datestamp = NOW(),
    email = 'georgewburroughs@gmail.com',
    passwd = SHA1('rainbow0020'),
    firstName = 'George',
    lastName = 'Burroughs',
    status = 1,
    permissions = 0;

INSERT INTO companies SET
    datestamp = NOW(),
    companyName = 'Promote Leads',
    active = 1;

-- inserts new module

INSERT INTO modules SET
    datestamp = NOW(),
    name = 'Messaging',
    namespace = 'msg',
    price = 0.0,
    url = '/msg',
    active = 1


SELECT * FROM modules;

-- inserts company modle
-- 1: CGI Solution, 2: LVE
INSERT INTO companyModules
SELECT NULL, 1, id FROM modules WHERE id = 7;

SELECT * FROM companyModules;
SELECT NULL, 1, id FROM modules WHERE id = 6;

SELECT * FROM codes;

--inserts codes 
INSERT INTO codes SET
    `group` = 10,
    `code` = 6,
    display = 'New Ticket',
    active = 1,
    company = 1;

SELECT * FROM codes WHERE code = 0;
SELECT * FROM companies;
SELECT * FROM departments;
SELECT * FROM tickets;

select * FROM users;

UPDATE users SET tech = 1 WHERE id = 4;


ALTER TABLE users ADD COLUMN tech BINARY DEFAULT 0;
ALTER TABLE users ADD COLUMN position INT(10) UNSIGNED DEFAULT 0;
ALTER TABLE tickets ADD COLUMN techAssigned int DEFAULT NULL;
ALTER TABLE positions ADD COLUMN `deleted` BINARY DEFAULT 0;

-- renames and changes a columns type
ALTER TABLE menu CHANGE `window` `target` VARCHAR(50);

CREATE DATABASE intranet;

CREATE TABLE `dbUpgradeFiles` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `userid` INT(10) UNSIGNED DEFAULT 0,
    `filename` VARCHAR(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;




CREATE UNIQUE INDEX emailIndex ON users (email);

ALTER TABLE users DROP INDEX emailCompanyIndex;



SELECT COUNT(id) AS Count FROM tickets WHERE userid = "2";


SELECT * FROM tickets;
EXPLAIN assignedTech;

SELECT * FROM users;


explain leads;

SELECT * FROM userCompanies WHERE userid = 1;
